//
// AbstractScheduleValidator.java
//
//     Validates a Schedule.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.calendaring.schedule.spi;


/**
 *  Validates a Schedule.
 */

public abstract class AbstractScheduleValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractScheduleValidator</code>.
     */

    protected AbstractScheduleValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractScheduleValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractScheduleValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Schedule.
     *
     *  @param schedule a schedule to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.calendaring.Schedule schedule) {
        super.validate(schedule);

        testNestedObject(schedule, "getScheduleSlot");

        testConditionalMethod(schedule, "getTimePeriodId", schedule.hasTimePeriod(), "hasTimePeriod");
        testConditionalMethod(schedule, "getTimePeriod", schedule.hasTimePeriod(), "hasTimePeriod");
        if (schedule.hasTimePeriod()) {
            testNestedObject(schedule, "getTimePeriod");
        }

        test(schedule.getScheduleStart(), "getScheduleStart()");
        test(schedule.getScheduleEnd(), "getScheduleEnd()");

        testConditionalMethod(schedule, "getLimit", schedule.hasLimit(), "hasLimit");
        if (schedule.hasLimit()) {
            testCardinal(schedule.getLimit(), "getLimit()");
        }

        test(schedule.getLocationDescription(), "getLocationDescription()");

        testConditionalMethod(schedule, "getLocationId", schedule.hasLocation(), "hasLocation");        
        testConditionalMethod(schedule, "getLocation", schedule.hasLocation(), "hasLocation");        
        if (schedule.hasLocation()) {
            testNestedObject(schedule, "getLocation");
        }

        test(schedule.getTotalDuration(), "getTotalDuration()");

        return;
    }
}
