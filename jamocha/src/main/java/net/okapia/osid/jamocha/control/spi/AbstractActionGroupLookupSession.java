//
// AbstractActionGroupLookupSession.java
//
//    A starter implementation framework for providing an ActionGroup
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an ActionGroup
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getActionGroups(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractActionGroupLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.control.ActionGroupLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();
    

    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>ActionGroup</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActionGroups() {
        return (true);
    }


    /**
     *  A complete view of the <code>ActionGroup</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActionGroupView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ActionGroup</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActionGroupView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include action groups in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>ActionGroup</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActionGroup</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ActionGroup</code> and
     *  retained for compatibility.
     *
     *  @param  actionGroupId <code>Id</code> of the
     *          <code>ActionGroup</code>
     *  @return the action group
     *  @throws org.osid.NotFoundException <code>actionGroupId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>actionGroupId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroup getActionGroup(org.osid.id.Id actionGroupId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.control.ActionGroupList actionGroups = getActionGroups()) {
            while (actionGroups.hasNext()) {
                org.osid.control.ActionGroup actionGroup = actionGroups.getNextActionGroup();
                if (actionGroup.getId().equals(actionGroupId)) {
                    return (actionGroup);
                }
            }
        } 

        throw new org.osid.NotFoundException(actionGroupId + " not found");
    }


    /**
     *  Gets an <code>ActionGroupList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  actionGroups specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ActionGroups</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getActionGroups()</code>.
     *
     *  @param  actionGroupIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActionGroup</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByIds(org.osid.id.IdList actionGroupIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.control.ActionGroup> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = actionGroupIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getActionGroup(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("action group " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.control.actiongroup.LinkedActionGroupList(ret));
    }


    /**
     *  Gets an <code>ActionGroupList</code> corresponding to the given
     *  action group genus <code>Type</code> which does not include
     *  action groups of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getActionGroups()</code>.
     *
     *  @param  actionGroupGenusType an actionGroup genus type 
     *  @return the returned <code>ActionGroup</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByGenusType(org.osid.type.Type actionGroupGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.actiongroup.ActionGroupGenusFilterList(getActionGroups(), actionGroupGenusType));
    }


    /**
     *  Gets an <code>ActionGroupList</code> corresponding to the given
     *  action group genus <code>Type</code> and include any additional
     *  action groups with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActionGroups()</code>.
     *
     *  @param  actionGroupGenusType an actionGroup genus type 
     *  @return the returned <code>ActionGroup</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByParentGenusType(org.osid.type.Type actionGroupGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getActionGroupsByGenusType(actionGroupGenusType));
    }


    /**
     *  Gets an <code>ActionGroupList</code> containing the given
     *  action group record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActionGroups()</code>.
     *
     *  @param  actionGroupRecordType an actionGroup record type 
     *  @return the returned <code>ActionGroup</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByRecordType(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.actiongroup.ActionGroupRecordFilterList(getActionGroups(), actionGroupRecordType));
    }


    /**
     *  Gets an <code> ActionGroupList </code> containing of the given <code>
     *  Action. </code>
     *
     *  In plenary mode, the returned list contains all known action
     *  groups or an error results. Otherwise, the returned list may
     *  contain only those action groups that are accessible through
     *  this session.
     *
     *  @param  actionId an action <code> Id </code>
     *  @return the returned <code> ActionGroup </code> list
     *  @throws org.osid.NullArgumentException <code> actionId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByAction(org.osid.id.Id actionId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.control.actiongroup.ActionGroupFilterList(new ActionFilter(actionId), getActionGroups()));
    }


    /**
     *  Gets all <code>ActionGroups</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  action groups or an error results. Otherwise, the returned list
     *  may contain only those action groups that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ActionGroups</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.control.ActionGroupList getActionGroups()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the action group list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of action groups
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.control.ActionGroupList filterActionGroupsOnViews(org.osid.control.ActionGroupList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class ActionFilter
        implements net.okapia.osid.jamocha.inline.filter.control.actiongroup.ActionGroupFilter {
         
        private final org.osid.id.Id actionId;
         
         
        /**
         *  Constructs a new <code>ActionFilter</code>.
         *
         *  @param actionId the action to filter
         *  @throws org.osid.NullArgumentException
         *          <code>actionId</code> is <code>null</code>
         */
        
        public ActionFilter(org.osid.id.Id actionId) {
            nullarg(actionId, "action Id");
            this.actionId = actionId;
            return;
        }

         
        /**
         *  Used by the ActionGroupFilterList to filter the action group list based
         *  on action.
         *
         *  @param actionGroup the action group
         *  @return <code>true</code> to pass the action group,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.control.ActionGroup actionGroup) {
            try (org.osid.id.IdList actionIds = actionGroup.getActionIds()) {
                while (actionIds.hasNext()) {
                    try {
                        if (actionIds.getNextId().equals(this.actionId)) {
                            return (true);
                        }
                    } catch (org.osid.OperationFailedException ofe) {}
                }
            }
            
            return (false);
        }
    }    
}
