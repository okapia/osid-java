//
// QualifierElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.qualifier.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class QualifierElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the QualifierElement Id.
     *
     *  @return the qualifier element Id
     */

    public static org.osid.id.Id getQualifierEntityId() {
        return (makeEntityId("osid.authorization.Qualifier"));
    }


    /**
     *  Gets the QualifierHierarchyId element Id.
     *
     *  @return the QualifierHierarchyId element Id
     */

    public static org.osid.id.Id getQualifierHierarchyId() {
        return (makeQueryElementId("osid.authorization.qualifier.QualifierHierarchyId"));
    }


    /**
     *  Gets the QualifierHierarchy element Id.
     *
     *  @return the QualifierHierarchy element Id
     */

    public static org.osid.id.Id getQualifierHierarchy() {
        return (makeQueryElementId("osid.authorization.qualifier.QualifierHierarchy"));
    }


    /**
     *  Gets the AuthorizationId element Id.
     *
     *  @return the AuthorizationId element Id
     */

    public static org.osid.id.Id getAuthorizationId() {
        return (makeQueryElementId("osid.authorization.qualifier.AuthorizationId"));
    }


    /**
     *  Gets the Authorization element Id.
     *
     *  @return the Authorization element Id
     */

    public static org.osid.id.Id getAuthorization() {
        return (makeQueryElementId("osid.authorization.qualifier.Authorization"));
    }


    /**
     *  Gets the AncestorQualifierId element Id.
     *
     *  @return the AncestorQualifierId element Id
     */

    public static org.osid.id.Id getAncestorQualifierId() {
        return (makeQueryElementId("osid.authorization.qualifier.AncestorQualifierId"));
    }


    /**
     *  Gets the AncestorQualifier element Id.
     *
     *  @return the AncestorQualifier element Id
     */

    public static org.osid.id.Id getAncestorQualifier() {
        return (makeQueryElementId("osid.authorization.qualifier.AncestorQualifier"));
    }


    /**
     *  Gets the DescendantQualifierId element Id.
     *
     *  @return the DescendantQualifierId element Id
     */

    public static org.osid.id.Id getDescendantQualifierId() {
        return (makeQueryElementId("osid.authorization.qualifier.DescendantQualifierId"));
    }


    /**
     *  Gets the DescendantQualifier element Id.
     *
     *  @return the DescendantQualifier element Id
     */

    public static org.osid.id.Id getDescendantQualifier() {
        return (makeQueryElementId("osid.authorization.qualifier.DescendantQualifier"));
    }


    /**
     *  Gets the VaultId element Id.
     *
     *  @return the VaultId element Id
     */

    public static org.osid.id.Id getVaultId() {
        return (makeQueryElementId("osid.authorization.qualifier.VaultId"));
    }


    /**
     *  Gets the Vault element Id.
     *
     *  @return the Vault element Id
     */

    public static org.osid.id.Id getVault() {
        return (makeQueryElementId("osid.authorization.qualifier.Vault"));
    }
}
