//
// RecurringEventValidator.java
//
//     Validates a RecurringEvent.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.calendaring.recurringevent;


/**
 *  Validates a RecurringEvent.
 */

public final class RecurringEventValidator
    extends net.okapia.osid.jamocha.builder.validator.calendaring.recurringevent.spi.AbstractRecurringEventValidator {


    /**
     *  Constructs a new <code>RecurringEventValidator</code>.
     */

    public RecurringEventValidator() {
        return;
    }


    /**
     *  Constructs a new <code>RecurringEventValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public RecurringEventValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a RecurringEvent with a default validation.
     *
     *  @param recurringEvent a recurring event to validate
     *  @return the recurring event
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>recurringEvent</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.calendaring.RecurringEvent validateRecurringEvent(org.osid.calendaring.RecurringEvent recurringEvent) {
        RecurringEventValidator validator = new RecurringEventValidator();
        validator.validate(recurringEvent);
        return (recurringEvent);
    }


    /**
     *  Validates a RecurringEvent for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param recurringEvent a recurring event to validate
     *  @return the recurring event
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>recurringEvent</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.calendaring.RecurringEvent validateRecurringEvent(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.calendaring.RecurringEvent recurringEvent) {

        RecurringEventValidator validator = new RecurringEventValidator(validation);
        validator.validate(recurringEvent);
        return (recurringEvent);
    }
}
