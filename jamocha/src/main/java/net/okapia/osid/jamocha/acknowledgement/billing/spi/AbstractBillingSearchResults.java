//
// AbstractBillingSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBillingSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.acknowledgement.BillingSearchResults {

    private org.osid.acknowledgement.BillingList billings;
    private final org.osid.acknowledgement.BillingQueryInspector inspector;
    private final java.util.Collection<org.osid.acknowledgement.records.BillingSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBillingSearchResults.
     *
     *  @param billings the result set
     *  @param billingQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>billings</code>
     *          or <code>billingQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBillingSearchResults(org.osid.acknowledgement.BillingList billings,
                                            org.osid.acknowledgement.BillingQueryInspector billingQueryInspector) {
        nullarg(billings, "billings");
        nullarg(billingQueryInspector, "billing query inspectpr");

        this.billings = billings;
        this.inspector = billingQueryInspector;

        return;
    }


    /**
     *  Gets the billing list resulting from a search.
     *
     *  @return a billing list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillings() {
        if (this.billings == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.acknowledgement.BillingList billings = this.billings;
        this.billings = null;
	return (billings);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.acknowledgement.BillingQueryInspector getBillingQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  billing search record <code> Type. </code> This method must
     *  be used to retrieve a billing implementing the requested
     *  record.
     *
     *  @param billingSearchRecordType a billing search 
     *         record type 
     *  @return the billing search
     *  @throws org.osid.NullArgumentException
     *          <code>billingSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(billingSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.acknowledgement.records.BillingSearchResultsRecord getBillingSearchResultsRecord(org.osid.type.Type billingSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.acknowledgement.records.BillingSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(billingSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(billingSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record billing search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBillingRecord(org.osid.acknowledgement.records.BillingSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "billing record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
