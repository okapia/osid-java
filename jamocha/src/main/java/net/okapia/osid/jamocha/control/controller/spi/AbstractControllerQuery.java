//
// AbstractControllerQuery.java
//
//     A template for making a Controller Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.controller.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for controllers.
 */

public abstract class AbstractControllerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQuery
    implements org.osid.control.ControllerQuery {

    private final java.util.Collection<org.osid.control.records.ControllerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Mathes an address. 
     *
     *  @param  address an address 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> address </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchAddress(String address, 
                             org.osid.type.Type stringMatchType, boolean match) {
        return;
    }


    /**
     *  Clears the address query terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        return;
    }


    /**
     *  Sets the model <code> Id </code> for this query. 
     *
     *  @param  modelId the model <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> modelId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModelId(org.osid.id.Id modelId, boolean match) {
        return;
    }


    /**
     *  Clears the model <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearModelIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ModelQuery </code> is available. 
     *
     *  @return <code> true </code> if a model query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Model. </code> Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the model query 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuery getModelQuery() {
        throw new org.osid.UnimplementedException("supportsModelQuery() is false");
    }


    /**
     *  Matches any models. 
     *
     *  @param  match <code> true </code> to match controllers with models, 
     *          <code> false </code> to match controllers with no model 
     *          defined 
     */

    @OSID @Override
    public void matchAnyModel(boolean match) {
        return;
    }


    /**
     *  Clears the model query terms. 
     */

    @OSID @Override
    public void clearModelTerms() {
        return;
    }


    /**
     *  Sets the version for this query. 
     *
     *  @param  version the version 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> version </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVersion(org.osid.installation.Version version, 
                             boolean match) {
        return;
    }


    /**
     *  Matches controllers with any version. 
     *
     *  @param  match <code> true </code> to match controllers with versions, 
     *          <code> false </code> to match controllers with no version 
     *          defined 
     */

    @OSID @Override
    public void matchAnyVersion(boolean match) {
        return;
    }


    /**
     *  Clears the version query terms. 
     */

    @OSID @Override
    public void clearVersionTerms() {
        return;
    }


    /**
     *  Matches controllers with versions including and more recent than the 
     *  given version. 
     *
     *  @param  version the version 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> version </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVersionSince(org.osid.installation.Version version, 
                                  boolean match) {
        return;
    }


    /**
     *  Clears the version since query terms. 
     */

    @OSID @Override
    public void clearVersionSinceTerms() {
        return;
    }


    /**
     *  Matches toggleable controllers. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchToggleable(boolean match) {
        return;
    }


    /**
     *  Clears the toggleable query terms. 
     */

    @OSID @Override
    public void clearToggleableTerms() {
        return;
    }


    /**
     *  Matches variable controllers. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchVariable(boolean match) {
        return;
    }


    /**
     *  Clears the variable query terms. 
     */

    @OSID @Override
    public void clearVariableTerms() {
        return;
    }


    /**
     *  Matches variable by percentage controllers. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchVariableByPercentage(boolean match) {
        return;
    }


    /**
     *  Clears the variable query terms. 
     */

    @OSID @Override
    public void clearVariableByPercentageTerms() {
        return;
    }


    /**
     *  Matches variable minimums between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVariableMinimum(java.math.BigDecimal start, 
                                     java.math.BigDecimal end, boolean match) {
        return;
    }


    /**
     *  Matches any variable minimums. 
     *
     *  @param  match <code> true </code> to match controllers with variable 
     *          minimums, <code> false </code> to match controllers with no 
     *          variable minimums 
     */

    @OSID @Override
    public void matchAnyVariableMinimum(boolean match) {
        return;
    }


    /**
     *  Clears the variable minimum query terms. 
     */

    @OSID @Override
    public void clearVariableMinimumTerms() {
        return;
    }


    /**
     *  Matches variable maximums between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVariableMaximum(java.math.BigDecimal start, 
                                     java.math.BigDecimal end, boolean match) {
        return;
    }


    /**
     *  Matches any variable maximums. 
     *
     *  @param  match <code> true </code> to match controllers with variable 
     *          maximums, <code> false </code> to match controllers with no 
     *          variable maximums 
     */

    @OSID @Override
    public void matchAnyVariableMaximum(boolean match) {
        return;
    }


    /**
     *  Clears the variable maximum query terms. 
     */

    @OSID @Override
    public void clearVariableMaximumTerms() {
        return;
    }


    /**
     *  Matches discreet states controllers. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchDiscreetStates(boolean match) {
        return;
    }


    /**
     *  Clears the discreet states query terms. 
     */

    @OSID @Override
    public void clearDiscreetStatesTerms() {
        return;
    }


    /**
     *  Sets the state <code> Id </code> for this query. 
     *
     *  @param  stateId the state <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDiscreetStateId(org.osid.id.Id stateId, boolean match) {
        return;
    }


    /**
     *  Clears the state <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StateQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> State. </code> Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDiscreetStateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getDiscreetStateQuery() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateQuery() is false");
    }


    /**
     *  Matches any discreet states. 
     *
     *  @param  match <code> true </code> to match controllers with discreet 
     *          states, <code> false </code> to match controllers with no 
     *          discreet states 
     */

    @OSID @Override
    public void matchAnyDiscreetState(boolean match) {
        return;
    }


    /**
     *  Clears the state query terms. 
     */

    @OSID @Override
    public void clearDiscreetStateTerms() {
        return;
    }


    /**
     *  Sets the system <code> Id </code> for this query. 
     *
     *  @param  systemId the system <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given controller query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a controller implementing the requested record.
     *
     *  @param controllerRecordType a controller record type
     *  @return the controller query record
     *  @throws org.osid.NullArgumentException
     *          <code>controllerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(controllerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ControllerQueryRecord getControllerQueryRecord(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ControllerQueryRecord record : this.records) {
            if (record.implementsRecordType(controllerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(controllerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this controller query. 
     *
     *  @param controllerQueryRecord controller query record
     *  @param controllerRecordType controller record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addControllerQueryRecord(org.osid.control.records.ControllerQueryRecord controllerQueryRecord, 
                                          org.osid.type.Type controllerRecordType) {

        addRecordType(controllerRecordType);
        nullarg(controllerQueryRecord, "controller query record");
        this.records.add(controllerQueryRecord);        
        return;
    }
}
