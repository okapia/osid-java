//
// AbstractQueryAuctionConstrainerLookupSession.java
//
//    An inline adapter that maps an AuctionConstrainerLookupSession to
//    an AuctionConstrainerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AuctionConstrainerLookupSession to
 *  an AuctionConstrainerQuerySession.
 */

public abstract class AbstractQueryAuctionConstrainerLookupSession
    extends net.okapia.osid.jamocha.bidding.rules.spi.AbstractAuctionConstrainerLookupSession
    implements org.osid.bidding.rules.AuctionConstrainerLookupSession {

    private boolean activeonly    = false;
    private final org.osid.bidding.rules.AuctionConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAuctionConstrainerLookupSession.
     *
     *  @param querySession the underlying auction constrainer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAuctionConstrainerLookupSession(org.osid.bidding.rules.AuctionConstrainerQuerySession querySession) {
        nullarg(querySession, "auction constrainer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform <code>AuctionConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionConstrainers() {
        return (this.session.canSearchAuctionConstrainers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction constrainers in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only active auction constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive auction constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>AuctionConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AuctionConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @param  auctionConstrainerId <code>Id</code> of the
     *          <code>AuctionConstrainer</code>
     *  @return the auction constrainer
     *  @throws org.osid.NotFoundException <code>auctionConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainer getAuctionConstrainer(org.osid.id.Id auctionConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerQuery query = getQuery();
        query.matchId(auctionConstrainerId, true);
        org.osid.bidding.rules.AuctionConstrainerList auctionConstrainers = this.session.getAuctionConstrainersByQuery(query);
        if (auctionConstrainers.hasNext()) {
            return (auctionConstrainers.getNextAuctionConstrainer());
        } 
        
        throw new org.osid.NotFoundException(auctionConstrainerId + " not found");
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionConstrainers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AuctionConstrainers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @param  auctionConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByIds(org.osid.id.IdList auctionConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerQuery query = getQuery();

        try (org.osid.id.IdList ids = auctionConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAuctionConstrainersByQuery(query));
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> corresponding to the given
     *  auction constrainer genus <code>Type</code> which does not include
     *  auction constrainers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  auction constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @param  auctionConstrainerGenusType an auctionConstrainer genus type 
     *  @return the returned <code>AuctionConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByGenusType(org.osid.type.Type auctionConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerQuery query = getQuery();
        query.matchGenusType(auctionConstrainerGenusType, true);
        return (this.session.getAuctionConstrainersByQuery(query));
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> corresponding to the given
     *  auction constrainer genus <code>Type</code> and include any additional
     *  auction constrainers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  auction constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @param  auctionConstrainerGenusType an auctionConstrainer genus type 
     *  @return the returned <code>AuctionConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByParentGenusType(org.osid.type.Type auctionConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerQuery query = getQuery();
        query.matchParentGenusType(auctionConstrainerGenusType, true);
        return (this.session.getAuctionConstrainersByQuery(query));
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> containing the given
     *  auction constrainer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auction constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @param  auctionConstrainerRecordType an auctionConstrainer record type 
     *  @return the returned <code>AuctionConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByRecordType(org.osid.type.Type auctionConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerQuery query = getQuery();
        query.matchRecordType(auctionConstrainerRecordType, true);
        return (this.session.getAuctionConstrainersByQuery(query));
    }

    
    /**
     *  Gets all <code>AuctionConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction constrainers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainers
     *  are returned.
     *
     *  @return a list of <code>AuctionConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAuctionConstrainersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.bidding.rules.AuctionConstrainerQuery getQuery() {
        org.osid.bidding.rules.AuctionConstrainerQuery query = this.session.getAuctionConstrainerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
