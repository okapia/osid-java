//
// AbstractImmutableProvision.java
//
//     Wraps a mutable Provision to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.provision.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Provision</code> to hide modifiers. This
 *  wrapper provides an immutized Provision from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying provision whose state changes are visible.
 */

public abstract class AbstractImmutableProvision
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.provisioning.Provision {

    private final org.osid.provisioning.Provision provision;


    /**
     *  Constructs a new <code>AbstractImmutableProvision</code>.
     *
     *  @param provision the provision to immutablize
     *  @throws org.osid.NullArgumentException <code>provision</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProvision(org.osid.provisioning.Provision provision) {
        super(provision);
        this.provision = provision;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the broker. 
     *
     *  @return the broker <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBrokerId() {
        return (this.provision.getBrokerId());
    }


    /**
     *  Gets the broker. 
     *
     *  @return the broker 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker()
        throws org.osid.OperationFailedException {

        return (this.provision.getBroker());
    }


    /**
     *  Gets the <code> Id </code> of the provisionable. 
     *
     *  @return the provisionable <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProvisionableId() {
        return (this.provision.getProvisionableId());
    }


    /**
     *  Gets the provisionable. 
     *
     *  @return the provisionable 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Provisionable getProvisionable()
        throws org.osid.OperationFailedException {

        return (this.provision.getProvisionable());
    }


    /**
     *  Gets the <code> Id </code> of the recipient. 
     *
     *  @return the recipient <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRecipientId() {
        return (this.provision.getRecipientId());
    }


    /**
     *  Gets the recipient. 
     *
     *  @return the recipient 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getRecipient()
        throws org.osid.OperationFailedException {

        return (this.provision.getRecipient());
    }


    /**
     *  Tests if a request is available.
     *
     *  @return <code>true</code> if a request is available,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean provisionedByRequest() {
        return (this.provision.provisionedByRequest());
    }

    
    /**
     *  Gets the <code> Id </code> of the request. 
     *
     *  @return the request <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRequestId() {
        return (this.provision.getRequestId());
    }


    /**
     *  Gets the request. 
     *
     *  @return the request 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Request getRequest()
        throws org.osid.OperationFailedException {

        return (this.provision.getRequest());
    }


    /**
     *  Gets the date this was provisioned. The provision date may differ from 
     *  the effective dates of this provision. 
     *
     *  @return the provision date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getProvisionDate() {
        return (this.provision.getProvisionDate());
    }


    /**
     *  Tests if this provision is temporary. The lease ends when the 
     *  provision expires or is returned. 
     *
     *  @return <code> true </code> if this is a lease, false if the provision 
     *          is permanent 
     */

    @OSID @Override
    public boolean isLeased() {
        return (this.provision.isLeased());
    }


    /**
     *  Tests if this provision is must be returned. 
     *
     *  @return <code> true </code> if this is must be returned, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> isLeased() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean mustReturn() {
        return (this.provision.mustReturn());
    }


    /**
     *  Tests if this provision is must be returned and has a due date. 
     *
     *  @return <code> true </code> if this is must be returned, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> mustReturn() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean hasDueDate() {
        return (this.provision.hasDueDate());
    }


    /**
     *  Gets the due date for the return. 
     *
     *  @return the due date 
     *  @throws org.osid.IllegalStateException <code> hasDueDate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDueDate() {
        return (this.provision.getDueDate());
    }


    /**
     *  Tests if this provision has a total cost. If this is a lease with an 
     *  expected return than it is the cost for the time period. 
     *
     *  @return <code> true </code> if there is a cost, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean hasCost() {
        return (this.provision.hasCost());
    }


    /**
     *  Gets the cost. 
     *
     *  @return the cost 
     *  @throws org.osid.IllegalStateException <code> hasCost() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getCost() {
        return (this.provision.getCost());
    }


    /**
     *  Tests if this provision has a rate per time period. 
     *
     *  @return <code> true </code> if there is a rate, <code> false </code> 
     *          otherwise 
     *  @throws org.osid.IllegalStateException <code> hasRate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean hasRate() {
        return (this.provision.hasRate());
    }


    /**
     *  Gets the rate amount. 
     *
     *  @return the rate amount 
     *  @throws org.osid.IllegalStateException <code> hasRate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getRateAmount() {
        return (this.provision.getRateAmount());
    }


    /**
     *  Gets the rate period. 
     *
     *  @return the time period 
     *  @throws org.osid.IllegalStateException <code> hasRate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getRatePeriod() {
        return (this.provision.getRatePeriod());
    }


    /**
     *  Tests if this provision has been returned. 
     *
     *  @return <code> true </code> if this provision has been returned, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isReturned() {
        return (this.provision.isReturned());
    }


    /**
     *  Gets the provision return that has any data that may have been 
     *  captured in the return process. 
     *
     *  @return the return 
     *  @throws org.osid.IllegalStateException <code> isReturned() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturn getProvisionReturn() {
        return (this.provision.getProvisionReturn());
    }


    /**
     *  Gets the provision record corresponding to the given <code> Provision 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  provisionRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(provisionRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  provisionRecordType the type of provision record to retrieve 
     *  @return the provision record 
     *  @throws org.osid.NullArgumentException <code> provisionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(provisionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.records.ProvisionRecord getProvisionRecord(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.provision.getProvisionRecord(provisionRecordType));
    }
}

