//
// AbstractChainSearch.java
//
//     A template for making a Chain Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.chain.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing chain searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractChainSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.sequencing.ChainSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.sequencing.records.ChainSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.sequencing.ChainSearchOrder chainSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of chains. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  chainIds list of chains
     *  @throws org.osid.NullArgumentException
     *          <code>chainIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongChains(org.osid.id.IdList chainIds) {
        while (chainIds.hasNext()) {
            try {
                this.ids.add(chainIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongChains</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of chain Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getChainIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  chainSearchOrder chain search order 
     *  @throws org.osid.NullArgumentException
     *          <code>chainSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>chainSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderChainResults(org.osid.sequencing.ChainSearchOrder chainSearchOrder) {
	this.chainSearchOrder = chainSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.sequencing.ChainSearchOrder getChainSearchOrder() {
	return (this.chainSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given chain search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a chain implementing the requested record.
     *
     *  @param chainSearchRecordType a chain search record
     *         type
     *  @return the chain search record
     *  @throws org.osid.NullArgumentException
     *          <code>chainSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(chainSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainSearchRecord getChainSearchRecord(org.osid.type.Type chainSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.sequencing.records.ChainSearchRecord record : this.records) {
            if (record.implementsRecordType(chainSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(chainSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this chain search. 
     *
     *  @param chainSearchRecord chain search record
     *  @param chainSearchRecordType chain search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addChainSearchRecord(org.osid.sequencing.records.ChainSearchRecord chainSearchRecord, 
                                           org.osid.type.Type chainSearchRecordType) {

        addRecordType(chainSearchRecordType);
        this.records.add(chainSearchRecord);        
        return;
    }
}
