//
// AbstractIndexedMapTimePeriodLookupSession.java
//
//    A simple framework for providing a TimePeriod lookup service
//    backed by a fixed collection of time periods with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a TimePeriod lookup service backed by a
 *  fixed collection of time periods. The time periods are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some time periods may be compatible
 *  with more types than are indicated through these time period
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>TimePeriods</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapTimePeriodLookupSession
    extends AbstractMapTimePeriodLookupSession
    implements org.osid.calendaring.TimePeriodLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.TimePeriod> timePeriodsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.TimePeriod>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.TimePeriod> timePeriodsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.TimePeriod>());


    /**
     *  Makes a <code>TimePeriod</code> available in this session.
     *
     *  @param  timePeriod a time period
     *  @throws org.osid.NullArgumentException <code>timePeriod<code> is
     *          <code>null</code>
     */

    @Override
    protected void putTimePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        super.putTimePeriod(timePeriod);

        this.timePeriodsByGenus.put(timePeriod.getGenusType(), timePeriod);
        
        try (org.osid.type.TypeList types = timePeriod.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.timePeriodsByRecord.put(types.getNextType(), timePeriod);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a time period from this session.
     *
     *  @param timePeriodId the <code>Id</code> of the time period
     *  @throws org.osid.NullArgumentException <code>timePeriodId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeTimePeriod(org.osid.id.Id timePeriodId) {
        org.osid.calendaring.TimePeriod timePeriod;
        try {
            timePeriod = getTimePeriod(timePeriodId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.timePeriodsByGenus.remove(timePeriod.getGenusType());

        try (org.osid.type.TypeList types = timePeriod.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.timePeriodsByRecord.remove(types.getNextType(), timePeriod);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeTimePeriod(timePeriodId);
        return;
    }


    /**
     *  Gets a <code>TimePeriodList</code> corresponding to the given
     *  time period genus <code>Type</code> which does not include
     *  time periods of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known time periods or an error results. Otherwise,
     *  the returned list may contain only those time periods that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  timePeriodGenusType a time period genus type 
     *  @return the returned <code>TimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByGenusType(org.osid.type.Type timePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.timeperiod.ArrayTimePeriodList(this.timePeriodsByGenus.get(timePeriodGenusType)));
    }


    /**
     *  Gets a <code>TimePeriodList</code> containing the given
     *  time period record <code>Type</code>. In plenary mode, the
     *  returned list contains all known time periods or an error
     *  results. Otherwise, the returned list may contain only those
     *  time periods that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  timePeriodRecordType a time period record type 
     *  @return the returned <code>timePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriodsByRecordType(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.timeperiod.ArrayTimePeriodList(this.timePeriodsByRecord.get(timePeriodRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.timePeriodsByGenus.clear();
        this.timePeriodsByRecord.clear();

        super.close();

        return;
    }
}
