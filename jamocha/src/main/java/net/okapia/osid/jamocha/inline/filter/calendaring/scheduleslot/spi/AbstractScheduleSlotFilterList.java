//
// AbstractScheduleSlotList
//
//     Implements a filter for a ScheduleSlotList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.calendaring.scheduleslot.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a ScheduleSlotList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedScheduleSlotList
 *  to improve performance.
 */

public abstract class AbstractScheduleSlotFilterList
    extends net.okapia.osid.jamocha.calendaring.scheduleslot.spi.AbstractScheduleSlotList
    implements org.osid.calendaring.ScheduleSlotList,
               net.okapia.osid.jamocha.inline.filter.calendaring.scheduleslot.ScheduleSlotFilter {

    private org.osid.calendaring.ScheduleSlot scheduleSlot;
    private final org.osid.calendaring.ScheduleSlotList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractScheduleSlotFilterList</code>.
     *
     *  @param scheduleSlotList a <code>ScheduleSlotList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleSlotList</code> is <code>null</code>
     */

    protected AbstractScheduleSlotFilterList(org.osid.calendaring.ScheduleSlotList scheduleSlotList) {
        nullarg(scheduleSlotList, "schedule slot list");
        this.list = scheduleSlotList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.scheduleSlot == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> ScheduleSlot </code> in this list. 
     *
     *  @return the next <code> ScheduleSlot </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> ScheduleSlot </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlot getNextScheduleSlot()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.calendaring.ScheduleSlot scheduleSlot = this.scheduleSlot;
            this.scheduleSlot = null;
            return (scheduleSlot);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in schedule slot list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.scheduleSlot = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters ScheduleSlots.
     *
     *  @param scheduleSlot the schedule slot to filter
     *  @return <code>true</code> if the schedule slot passes the filter,
     *          <code>false</code> if the schedule slot should be filtered
     */

    public abstract boolean pass(org.osid.calendaring.ScheduleSlot scheduleSlot);


    protected void prime() {
        if (this.scheduleSlot != null) {
            return;
        }

        org.osid.calendaring.ScheduleSlot scheduleSlot = null;

        while (this.list.hasNext()) {
            try {
                scheduleSlot = this.list.getNextScheduleSlot();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(scheduleSlot)) {
                this.scheduleSlot = scheduleSlot;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
