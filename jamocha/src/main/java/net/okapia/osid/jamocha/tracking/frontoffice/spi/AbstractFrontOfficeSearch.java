//
// AbstractFrontOfficeSearch.java
//
//     A template for making a FrontOffice Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.frontoffice.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing front office searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractFrontOfficeSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.tracking.FrontOfficeSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.tracking.records.FrontOfficeSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.tracking.FrontOfficeSearchOrder frontOfficeSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of front offices. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  frontOfficeIds list of front offices
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongFrontOffices(org.osid.id.IdList frontOfficeIds) {
        while (frontOfficeIds.hasNext()) {
            try {
                this.ids.add(frontOfficeIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongFrontOffices</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of front office Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getFrontOfficeIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  frontOfficeSearchOrder front office search order 
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>frontOfficeSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderFrontOfficeResults(org.osid.tracking.FrontOfficeSearchOrder frontOfficeSearchOrder) {
	this.frontOfficeSearchOrder = frontOfficeSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.tracking.FrontOfficeSearchOrder getFrontOfficeSearchOrder() {
	return (this.frontOfficeSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given front office search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a front office implementing the requested record.
     *
     *  @param frontOfficeSearchRecordType a front office search record
     *         type
     *  @return the front office search record
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(frontOfficeSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.FrontOfficeSearchRecord getFrontOfficeSearchRecord(org.osid.type.Type frontOfficeSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.tracking.records.FrontOfficeSearchRecord record : this.records) {
            if (record.implementsRecordType(frontOfficeSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(frontOfficeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this front office search. 
     *
     *  @param frontOfficeSearchRecord front office search record
     *  @param frontOfficeSearchRecordType frontOffice search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFrontOfficeSearchRecord(org.osid.tracking.records.FrontOfficeSearchRecord frontOfficeSearchRecord, 
                                           org.osid.type.Type frontOfficeSearchRecordType) {

        addRecordType(frontOfficeSearchRecordType);
        this.records.add(frontOfficeSearchRecord);        
        return;
    }
}
