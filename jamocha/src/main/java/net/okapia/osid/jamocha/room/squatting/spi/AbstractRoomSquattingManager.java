//
// AbstractRoomSquattingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRoomSquattingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.room.squatting.RoomSquattingManager,
               org.osid.room.squatting.RoomSquattingProxyManager {

    private final Types leaseRecordTypes                   = new TypeRefSet();
    private final Types leaseSearchRecordTypes             = new TypeRefSet();

    private final Types deedRecordTypes                    = new TypeRefSet();
    private final Types deedSearchRecordTypes              = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRoomSquattingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRoomSquattingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any deed federation is exposed. Federation is exposed when a 
     *  specific deed may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of deeds 
     *  appears as a single deed. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of an lease lookup service. 
     *
     *  @return <code> true </code> if lease lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseLookup() {
        return (false);
    }


    /**
     *  Tests if querying leasees is available. 
     *
     *  @return <code> true </code> if lease query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseQuery() {
        return (false);
    }


    /**
     *  Tests if searching for leasees is available. 
     *
     *  @return <code> true </code> if lease search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a lease administrative service for 
     *  creating and deleting leasees. 
     *
     *  @return <code> true </code> if lease administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a lease notification service. 
     *
     *  @return <code> true </code> if lease notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseNotification() {
        return (false);
    }


    /**
     *  Tests if a lease to campus lookup session is available. 
     *
     *  @return <code> true </code> if lease campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseCampus() {
        return (false);
    }


    /**
     *  Tests if a lease to campus assignment session is available. 
     *
     *  @return <code> true </code> if lease campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseCampusAssignment() {
        return (false);
    }


    /**
     *  Tests if a lease smart campus session is available. 
     *
     *  @return <code> true </code> if lease smart campus is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLeaseSmartCampus() {
        return (false);
    }


    /**
     *  Tests for the availability of an deed lookup service. 
     *
     *  @return <code> true </code> if deed lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedLookup() {
        return (false);
    }


    /**
     *  Tests if querying deeds is available. 
     *
     *  @return <code> true </code> if deed query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedQuery() {
        return (false);
    }


    /**
     *  Tests if searching for deeds is available. 
     *
     *  @return <code> true </code> if deed search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a deed administrative service for 
     *  creating and deleting deeds. 
     *
     *  @return <code> true </code> if deed administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a deed notification service. 
     *
     *  @return <code> true </code> if deed notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedNotification() {
        return (false);
    }


    /**
     *  Tests if a deed to campus lookup session is available. 
     *
     *  @return <code> true </code> if deed campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedCampus() {
        return (false);
    }


    /**
     *  Tests if a deed to campus assignment session is available. 
     *
     *  @return <code> true </code> if deed campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedCampusAssignment() {
        return (false);
    }


    /**
     *  Tests if a deed smart campus session is available. 
     *
     *  @return <code> true </code> if deed smart campus is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeedSmartCampus() {
        return (false);
    }


    /**
     *  Tests if a service to manage squatters in bulk is available. 
     *
     *  @return <code> true </code> if a room batch squatting service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSquattingBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Lease </code> record types. 
     *
     *  @return a list containing the supported lease record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLeaseRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.leaseRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Lease </code> record type is supported. 
     *
     *  @param  leaseRecordType a <code> Type </code> indicating a <code> 
     *          Lease </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> leaseRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLeaseRecordType(org.osid.type.Type leaseRecordType) {
        return (this.leaseRecordTypes.contains(leaseRecordType));
    }


    /**
     *  Adds support for a lease record type.
     *
     *  @param leaseRecordType a lease record type
     *  @throws org.osid.NullArgumentException
     *  <code>leaseRecordType</code> is <code>null</code>
     */

    protected void addLeaseRecordType(org.osid.type.Type leaseRecordType) {
        this.leaseRecordTypes.add(leaseRecordType);
        return;
    }


    /**
     *  Removes support for a lease record type.
     *
     *  @param leaseRecordType a lease record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>leaseRecordType</code> is <code>null</code>
     */

    protected void removeLeaseRecordType(org.osid.type.Type leaseRecordType) {
        this.leaseRecordTypes.remove(leaseRecordType);
        return;
    }


    /**
     *  Gets the supported lease search record types. 
     *
     *  @return a list containing the supported lease search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLeaseSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.leaseSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given lease search record type is supported. 
     *
     *  @param  leaseSearchRecordType a <code> Type </code> indicating a lease 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> leaseSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLeaseSearchRecordType(org.osid.type.Type leaseSearchRecordType) {
        return (this.leaseSearchRecordTypes.contains(leaseSearchRecordType));
    }


    /**
     *  Adds support for a lease search record type.
     *
     *  @param leaseSearchRecordType a lease search record type
     *  @throws org.osid.NullArgumentException
     *  <code>leaseSearchRecordType</code> is <code>null</code>
     */

    protected void addLeaseSearchRecordType(org.osid.type.Type leaseSearchRecordType) {
        this.leaseSearchRecordTypes.add(leaseSearchRecordType);
        return;
    }


    /**
     *  Removes support for a lease search record type.
     *
     *  @param leaseSearchRecordType a lease search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>leaseSearchRecordType</code> is <code>null</code>
     */

    protected void removeLeaseSearchRecordType(org.osid.type.Type leaseSearchRecordType) {
        this.leaseSearchRecordTypes.remove(leaseSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Deed </code> record types. 
     *
     *  @return a list containing the supported deed record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeedRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.deedRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Deed </code> record type is supported. 
     *
     *  @param  deedRecordType a <code> Type </code> indicating a <code> Deed 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deedRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeedRecordType(org.osid.type.Type deedRecordType) {
        return (this.deedRecordTypes.contains(deedRecordType));
    }


    /**
     *  Adds support for a deed record type.
     *
     *  @param deedRecordType a deed record type
     *  @throws org.osid.NullArgumentException
     *  <code>deedRecordType</code> is <code>null</code>
     */

    protected void addDeedRecordType(org.osid.type.Type deedRecordType) {
        this.deedRecordTypes.add(deedRecordType);
        return;
    }


    /**
     *  Removes support for a deed record type.
     *
     *  @param deedRecordType a deed record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>deedRecordType</code> is <code>null</code>
     */

    protected void removeDeedRecordType(org.osid.type.Type deedRecordType) {
        this.deedRecordTypes.remove(deedRecordType);
        return;
    }


    /**
     *  Gets the supported deed search record types. 
     *
     *  @return a list containing the supported deed search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeedSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.deedSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given deed search record type is supported. 
     *
     *  @param  deedSearchRecordType a <code> Type </code> indicating a deed 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deedSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeedSearchRecordType(org.osid.type.Type deedSearchRecordType) {
        return (this.deedSearchRecordTypes.contains(deedSearchRecordType));
    }


    /**
     *  Adds support for a deed search record type.
     *
     *  @param deedSearchRecordType a deed search record type
     *  @throws org.osid.NullArgumentException
     *  <code>deedSearchRecordType</code> is <code>null</code>
     */

    protected void addDeedSearchRecordType(org.osid.type.Type deedSearchRecordType) {
        this.deedSearchRecordTypes.add(deedSearchRecordType);
        return;
    }


    /**
     *  Removes support for a deed search record type.
     *
     *  @param deedSearchRecordType a deed search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>deedSearchRecordType</code> is <code>null</code>
     */

    protected void removeDeedSearchRecordType(org.osid.type.Type deedSearchRecordType) {
        this.deedSearchRecordTypes.remove(deedSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease lookup 
     *  service. 
     *
     *  @return a <code> LeaseLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseLookupSession getLeaseLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LeaseLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseLookupSession getLeaseLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Lease </code> 
     *  @return a <code> LeaseLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseLookupSession getLeaseLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LeaseLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseLookupSession getLeaseLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease query 
     *  service. 
     *
     *  @return a <code> LeaseQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseQuerySession getLeaseQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LeaseQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseQuerySession getLeaseQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Lease </code> 
     *  @return a <code> LeaseQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseQuerySession getLeaseQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LeaseQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Lease </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseQuerySession getLeaseQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease search 
     *  service. 
     *
     *  @return a <code> LeaseSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseSearchSession getLeaseSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LeaseSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseSearchSession getLeaseSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Lease </code> 
     *  @return a <code> LeaseSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseSearchSession getLeaseSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LeaseSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Lease </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseSearchSession getLeaseSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  administrative service. 
     *
     *  @return a <code> LeaseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseAdminSession getLeaseAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LeaseAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseAdminSession getLeaseAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Lease </code> 
     *  @return a <code> LeaseAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseAdminSession getLeaseAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LeaseAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Lease </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseAdminSession getLeaseAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  notification service. 
     *
     *  @param  leaseReceiver the receiver 
     *  @return a <code> LeaseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> leaseReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseNotificationSession getLeaseNotificationSession(org.osid.room.squatting.LeaseReceiver leaseReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  notification service. 
     *
     *  @param  leaseReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> LeaseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> leaseReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseNotificationSession getLeaseNotificationSession(org.osid.room.squatting.LeaseReceiver leaseReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  notification service for the given campus. 
     *
     *  @param  leaseReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> LeaseNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> leaseReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseNotificationSession getLeaseNotificationSessionForCampus(org.osid.room.squatting.LeaseReceiver leaseReceiver, 
                                                                                                 org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the lease 
     *  notification service for the given campus. 
     *
     *  @param  leaseReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LeaseNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Lease </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> leaseReceiver, campusId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseNotificationSession getLeaseNotificationSessionForCampus(org.osid.room.squatting.LeaseReceiver leaseReceiver, 
                                                                                                 org.osid.id.Id campusId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the session for retrieving lease to campus mappings. 
     *
     *  @return a <code> LeaseCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseCampusSession getLeaseCampusSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseCampusSession not implemented");
    }


    /**
     *  Gets the session for retrieving lease to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LeaseCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLeaseCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseCampusSession getLeaseCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseCampusSession not implemented");
    }


    /**
     *  Gets the session for assigning lease to campus mappings. 
     *
     *  @return a <code> LeaseCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseCampusAssignmentSession getLeaseCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning lease to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LeaseCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseCampusAssignmentSession getLeaseCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the lease smart campus for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> LeaseSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseSmartCampusSession getLeaseSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getLeaseSmartCampusSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic lease campuses for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of a campus 
     *  @param  proxy a proxy 
     *  @return a <code> LeaseSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLeaseSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseSmartCampusSession getLeaseSmartCampusSession(org.osid.id.Id campusId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getLeaseSmartCampusSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed lookup 
     *  service. 
     *
     *  @return a <code> DeedLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedLookupSession getDeedLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeedLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedLookupSession getDeedLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Deed </code> 
     *  @return a <code> DeedLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedLookupSession getDeedLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeedLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedLookupSession getDeedLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed query 
     *  service. 
     *
     *  @return a <code> DeedQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedQuerySession getDeedQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeedQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedQuerySession getDeedQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Deed </code> 
     *  @return a <code> DeedQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedQuerySession getDeedQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeedQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Deed </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedQuerySession getDeedQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed search 
     *  service. 
     *
     *  @return a <code> DeedSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedSearchSession getDeedSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeedSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedSearchSession getDeedSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Deed </code> 
     *  @return a <code> DeedSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedSearchSession getDeedSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeedSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Deed </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedSearchSession getDeedSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  administrative service. 
     *
     *  @return a <code> DeedAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedAdminSession getDeedAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeedAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedAdminSession getDeedAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Deed </code> 
     *  @return a <code> DeedAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedAdminSession getDeedAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeedAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Deed </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedAdminSession getDeedAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  notification service. 
     *
     *  @param  DeedReceiver the receiver 
     *  @return a <code> DeedNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> DeedReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedNotificationSession getDeedNotificationSession(org.osid.room.squatting.DeedReceiver DeedReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  notification service. 
     *
     *  @param  DeedReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> DeedNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> DeedReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedNotificationSession getDeedNotificationSession(org.osid.room.squatting.DeedReceiver DeedReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  notification service for the given campus. 
     *
     *  @param  DeedReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> DeedNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> DeedReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedNotificationSession getDeedNotificationSessionForCampus(org.osid.room.squatting.DeedReceiver DeedReceiver, 
                                                                                               org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the deed 
     *  notification service for the given campus. 
     *
     *  @param  DeedReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeedNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Deed </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> DeedReceiver, campusId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedNotificationSession getDeedNotificationSessionForCampus(org.osid.room.squatting.DeedReceiver DeedReceiver, 
                                                                                               org.osid.id.Id campusId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the session for retrieving deed to campus mappings. 
     *
     *  @return a <code> DeedCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedCampusSession getDeedCampusSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedCampusSession not implemented");
    }


    /**
     *  Gets the session for retrieving deed to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeedCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeedCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedCampusSession getDeedCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedCampusSession not implemented");
    }


    /**
     *  Gets the session for assigning deed to campus mappings. 
     *
     *  @return a <code> DeedCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedCampusAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedCampusAssignmentSession getDeedCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning deed to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeedCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedCampusAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedCampusAssignmentSession getDeedCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the deed smart campus for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> DeedSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedSmartCampusSession getDeedSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getDeedSmartCampusSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic deed campuses for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of a campus 
     *  @param  proxy a proxy 
     *  @return a <code> DeedSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeedSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedSmartCampusSession getDeedSmartCampusSession(org.osid.id.Id campusId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getDeedSmartCampusSession not implemented");
    }


    /**
     *  Gets a <code> RoomSquattingBatchManager. </code> 
     *
     *  @return a <code> RoomSquattingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomSquattingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.batch.RoomSquattingBatchManager getRoomSquattingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingManager.getRoomSquattingBatchManager not implemented");
    }


    /**
     *  Gets a <code> RoomSquattingBatchProxyManager. </code> 
     *
     *  @return a <code> RoomSquattingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomSquattingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.batch.RoomSquattingBatchProxyManager getRoomSquattingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.squatting.RoomSquattingProxyManager.getRoomSquattingBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.leaseRecordTypes.clear();
        this.leaseRecordTypes.clear();

        this.leaseSearchRecordTypes.clear();
        this.leaseSearchRecordTypes.clear();

        this.deedRecordTypes.clear();
        this.deedRecordTypes.clear();

        this.deedSearchRecordTypes.clear();
        this.deedSearchRecordTypes.clear();

        return;
    }
}
