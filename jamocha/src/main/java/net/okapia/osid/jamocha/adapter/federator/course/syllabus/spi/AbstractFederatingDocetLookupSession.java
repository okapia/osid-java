//
// AbstractFederatingDocetLookupSession.java
//
//     An abstract federating adapter for a DocetLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  DocetLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingDocetLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.course.syllabus.DocetLookupSession>
    implements org.osid.course.syllabus.DocetLookupSession {

    private boolean parallel = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();


    /**
     *  Constructs a new <code>AbstractFederatingDocetLookupSession</code>.
     */

    protected AbstractFederatingDocetLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.course.syllabus.DocetLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Docet</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDocets() {
        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            if (session.canLookupDocets()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Docet</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDocetView() {
        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            session.useComparativeDocetView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Docet</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDocetView() {
        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            session.usePlenaryDocetView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include docets in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            session.useFederatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            session.useIsolatedCourseCatalogView();
        }

        return;
    }


    /**
     *  Only docets whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveDocetView() {
        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            session.useEffectiveDocetView();
        }

        return;
    }


    /**
     *  All docets of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveDocetView() {
        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            session.useAnyEffectiveDocetView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Docet</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Docet</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Docet</code> and
     *  retained for compatibility.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param  docetId <code>Id</code> of the
     *          <code>Docet</code>
     *  @return the docet
     *  @throws org.osid.NotFoundException <code>docetId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>docetId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Docet getDocet(org.osid.id.Id docetId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            try {
                return (session.getDocet(docetId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(docetId + " not found");
    }


    /**
     *  Gets a <code>DocetList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  docets specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Docets</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, docets are returned that are currently effective.
     *  In any effective mode, effective docets and those currently expired
     *  are returned.
     *
     *  @param  docetIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Docet</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>docetIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByIds(org.osid.id.IdList docetIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.course.syllabus.docet.MutableDocetList ret = new net.okapia.osid.jamocha.course.syllabus.docet.MutableDocetList();

        try (org.osid.id.IdList ids = docetIds) {
            while (ids.hasNext()) {
                ret.addDocet(getDocet(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>DocetList</code> corresponding to the given
     *  docet genus <code>Type</code> which does not include
     *  docets of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently effective.
     *  In any effective mode, effective docets and those currently expired
     *  are returned.
     *
     *  @param  docetGenusType a docet genus type 
     *  @return the returned <code>Docet</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>docetGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByGenusType(org.osid.type.Type docetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsByGenusType(docetGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DocetList</code> corresponding to the given
     *  docet genus <code>Type</code> and include any additional
     *  docets with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param  docetGenusType a docet genus type 
     *  @return the returned <code>Docet</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>docetGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByParentGenusType(org.osid.type.Type docetGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsByParentGenusType(docetGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DocetList</code> containing the given
     *  docet record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @param  docetRecordType a docet record type 
     *  @return the returned <code>Docet</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>docetRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsByRecordType(org.osid.type.Type docetRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsByRecordType(docetRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DocetList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *  
     *  In active mode, docets are returned that are currently
     *  active. In any status mode, active and inactive docets
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Docet</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of docets corresponding to a module
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  moduleId the <code>Id</code> of the module
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>moduleId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.syllabus.DocetList getDocetsForModule(org.osid.id.Id moduleId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsForModule(moduleId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of docets corresponding to a module
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  moduleId the <code>Id</code> of the module
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>moduleId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModuleOnDate(org.osid.id.Id moduleId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsForModuleOnDate(moduleId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of docets corresponding to an activity unit
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  activityUnitId the <code>Id</code> of the activity unit
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>activityUnitId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.syllabus.DocetList getDocetsForActivityUnit(org.osid.id.Id activityUnitId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsForActivityUnit(activityUnitId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of docets corresponding to an activity unit
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  activityUnitId the <code>Id</code> of the activity unit
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>activityUnitId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForActivityUnitOnDate(org.osid.id.Id activityUnitId,
                                                                             org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsForActivityUnitOnDate(activityUnitId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of docets corresponding to module and activity unit
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  moduleId the <code>Id</code> of the module
     *  @param  activityUnitId the <code>Id</code> of the activity unit
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>moduleId</code>,
     *          <code>activityUnitId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModuleAndActivityUnit(org.osid.id.Id moduleId,
                                                                                org.osid.id.Id activityUnitId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsForModuleAndActivityUnit(moduleId, activityUnitId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of docets corresponding to module and activity unit
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible
     *  through this session.
     *
     *  In effective mode, docets are returned that are
     *  currently effective.  In any effective mode, effective
     *  docets and those currently expired are returned.
     *
     *  @param  activityUnitId the <code>Id</code> of the activity unit
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>DocetList</code>
     *  @throws org.osid.NullArgumentException <code>moduleId</code>,
     *          <code>activityUnitId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocetsForModuleAndActivityUnitOnDate(org.osid.id.Id moduleId,
                                                                                      org.osid.id.Id activityUnitId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocetsForModuleAndActivityUnitOnDate(moduleId, activityUnitId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Docets</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  docets or an error results. Otherwise, the returned list
     *  may contain only those docets that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, docets are returned that are currently
     *  effective.  In any effective mode, effective docets and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Docets</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.DocetList getDocets()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList ret = getDocetList();

        for (org.osid.course.syllabus.DocetLookupSession session : getSessions()) {
            ret.addDocetList(session.getDocets());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.FederatingDocetList getDocetList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.ParallelDocetList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.course.syllabus.docet.CompositeDocetList());
        }
    }
}
