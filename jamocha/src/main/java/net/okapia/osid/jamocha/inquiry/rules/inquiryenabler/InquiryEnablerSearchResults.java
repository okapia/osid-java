//
// InquiryEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.rules.inquiryenabler;


/**
 *  A template for implementing a search results.
 */

public final class InquiryEnablerSearchResults
    extends net.okapia.osid.jamocha.inquiry.rules.inquiryenabler.spi.AbstractInquiryEnablerSearchResults
    implements org.osid.inquiry.rules.InquiryEnablerSearchResults {


    /**
     *  Constructs a new <code>InquiryEnablerSearchResults.
     *
     *  @param inquiryEnablers the result set
     *  @param inquiryEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>inquiryEnablers</code>
     *          or <code>inquiryEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    public InquiryEnablerSearchResults(org.osid.inquiry.rules.InquiryEnablerList inquiryEnablers,
                                 org.osid.inquiry.rules.InquiryEnablerQueryInspector inquiryEnablerQueryInspector) {
        super(inquiryEnablers, inquiryEnablerQueryInspector);
        return;
    }
}
