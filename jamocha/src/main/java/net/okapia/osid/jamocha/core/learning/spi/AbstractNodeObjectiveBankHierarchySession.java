//
// AbstractNodeObjectiveBankHierarchySession.java
//
//     Defines an ObjectiveBank hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an objective bank hierarchy session for delivering a hierarchy
 *  of objective banks using the ObjectiveBankNode interface.
 */

public abstract class AbstractNodeObjectiveBankHierarchySession
    extends net.okapia.osid.jamocha.learning.spi.AbstractObjectiveBankHierarchySession
    implements org.osid.learning.ObjectiveBankHierarchySession {

    private java.util.Collection<org.osid.learning.ObjectiveBankNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root objective bank <code> Ids </code> in this hierarchy.
     *
     *  @return the root objective bank <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootObjectiveBankIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivebanknode.ObjectiveBankNodeToIdList(this.roots));
    }


    /**
     *  Gets the root objective banks in the objective bank hierarchy. A node
     *  with no parents is an orphan. While all objective bank <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root objective banks 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getRootObjectiveBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivebanknode.ObjectiveBankNodeToObjectiveBankList(new net.okapia.osid.jamocha.learning.objectivebanknode.ArrayObjectiveBankNodeList(this.roots)));
    }


    /**
     *  Adds a root objective bank node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootObjectiveBank(org.osid.learning.ObjectiveBankNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root objective bank nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootObjectiveBanks(java.util.Collection<org.osid.learning.ObjectiveBankNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root objective bank node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootObjectiveBank(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.learning.ObjectiveBankNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> ObjectiveBank </code> has any parents. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @return <code> true </code> if the objective bank has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentObjectiveBanks(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getObjectiveBankNode(objectiveBankId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  objective bank.
     *
     *  @param  id an <code> Id </code> 
     *  @param  objectiveBankId the <code> Id </code> of an objective bank 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> objectiveBankId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> objectiveBankId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfObjectiveBank(org.osid.id.Id id, org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.learning.ObjectiveBankNodeList parents = getObjectiveBankNode(objectiveBankId).getParentObjectiveBankNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextObjectiveBankNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given objective bank. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @return the parent <code> Ids </code> of the objective bank 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentObjectiveBankIds(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivebank.ObjectiveBankToIdList(getParentObjectiveBanks(objectiveBankId)));
    }


    /**
     *  Gets the parents of the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> to query 
     *  @return the parents of the objective bank 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getParentObjectiveBanks(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivebanknode.ObjectiveBankNodeToObjectiveBankList(getObjectiveBankNode(objectiveBankId).getParentObjectiveBankNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  objective bank.
     *
     *  @param  id an <code> Id </code> 
     *  @param  objectiveBankId the Id of an objective bank 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> objectiveBankId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfObjectiveBank(org.osid.id.Id id, org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfObjectiveBank(id, objectiveBankId)) {
            return (true);
        }

        try (org.osid.learning.ObjectiveBankList parents = getParentObjectiveBanks(objectiveBankId)) {
            while (parents.hasNext()) {
                if (isAncestorOfObjectiveBank(id, parents.getNextObjectiveBank().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an objective bank has any children. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @return <code> true </code> if the <code> objectiveBankId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildObjectiveBanks(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getObjectiveBankNode(objectiveBankId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  objective bank.
     *
     *  @param  id an <code> Id </code> 
     *  @param objectiveBankId the <code> Id </code> of an 
     *         objective bank
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> objectiveBankId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> objectiveBankId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfObjectiveBank(org.osid.id.Id id, org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfObjectiveBank(objectiveBankId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  objective bank.
     *
     *  @param  objectiveBankId the <code> Id </code> to query 
     *  @return the children of the objective bank 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildObjectiveBankIds(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivebank.ObjectiveBankToIdList(getChildObjectiveBanks(objectiveBankId)));
    }


    /**
     *  Gets the children of the given objective bank. 
     *
     *  @param  objectiveBankId the <code> Id </code> to query 
     *  @return the children of the objective bank 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getChildObjectiveBanks(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivebanknode.ObjectiveBankNodeToObjectiveBankList(getObjectiveBankNode(objectiveBankId).getChildObjectiveBankNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  objective bank.
     *
     *  @param  id an <code> Id </code> 
     *  @param objectiveBankId the <code> Id </code> of an 
     *         objective bank
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> objectiveBankId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfObjectiveBank(org.osid.id.Id id, org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfObjectiveBank(objectiveBankId, id)) {
            return (true);
        }

        try (org.osid.learning.ObjectiveBankList children = getChildObjectiveBanks(objectiveBankId)) {
            while (children.hasNext()) {
                if (isDescendantOfObjectiveBank(id, children.getNextObjectiveBank().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  objective bank.
     *
     *  @param  objectiveBankId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified objective bank node 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getObjectiveBankNodeIds(org.osid.id.Id objectiveBankId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.learning.objectivebanknode.ObjectiveBankNodeToNode(getObjectiveBankNode(objectiveBankId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given objective bank.
     *
     *  @param  objectiveBankId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified objective bank node 
     *  @throws org.osid.NotFoundException <code> objectiveBankId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankNode getObjectiveBankNodes(org.osid.id.Id objectiveBankId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getObjectiveBankNode(objectiveBankId));
    }


    /**
     *  Closes this <code>ObjectiveBankHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an objective bank node.
     *
     *  @param objectiveBankId the id of the objective bank node
     *  @throws org.osid.NotFoundException <code>objectiveBankId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>objectiveBankId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.learning.ObjectiveBankNode getObjectiveBankNode(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(objectiveBankId, "objective bank Id");
        for (org.osid.learning.ObjectiveBankNode objectiveBank : this.roots) {
            if (objectiveBank.getId().equals(objectiveBankId)) {
                return (objectiveBank);
            }

            org.osid.learning.ObjectiveBankNode r = findObjectiveBank(objectiveBank, objectiveBankId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(objectiveBankId + " is not found");
    }


    protected org.osid.learning.ObjectiveBankNode findObjectiveBank(org.osid.learning.ObjectiveBankNode node, 
                                                                    org.osid.id.Id objectiveBankId) 
	throws org.osid.OperationFailedException {

        try (org.osid.learning.ObjectiveBankNodeList children = node.getChildObjectiveBankNodes()) {
            while (children.hasNext()) {
                org.osid.learning.ObjectiveBankNode objectiveBank = children.getNextObjectiveBankNode();
                if (objectiveBank.getId().equals(objectiveBankId)) {
                    return (objectiveBank);
                }
                
                objectiveBank = findObjectiveBank(objectiveBank, objectiveBankId);
                if (objectiveBank != null) {
                    return (objectiveBank);
                }
            }
        }

        return (null);
    }
}
