//
// MutableIndexedMapPathLookupSession
//
//    Implements a Path lookup service backed by a collection of
//    paths indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Path lookup service backed by a collection of
 *  paths. The paths are indexed by <code>Id</code>, genus
 *  and record types.</p>
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some paths may be compatible
 *  with more types than are indicated through these path
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The collection of paths can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapPathLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractIndexedMapPathLookupSession
    implements org.osid.mapping.path.PathLookupSession {


    /**
     *  Constructs a new
     *  <code>MutableIndexedMapPathLookupSession</code> with no
     *  paths.
     */

    public MutableIndexedMapPathLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableIndexedMapPathLookupSession</code> with a
     *  single path.
     *  
     *  @param  path a single path
     *  @throws org.osid.NullArgumentException <code>path</code>
     *          is <code>null</code>
     */

    public MutableIndexedMapPathLookupSession(org.osid.mapping.path.Path path) {
        putPath(path);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableIndexedMapPathLookupSession</code> using an
     *  array of paths.
     *
     *  @param  paths an array of paths
     *  @throws org.osid.NullArgumentException <code>paths</code>
     *          is <code>null</code>
     */

    public MutableIndexedMapPathLookupSession(org.osid.mapping.path.Path[] paths) {
        putPaths(paths);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableIndexedMapPathLookupSession</code> using a
     *  collection of paths.
     *
     *  @param  paths a collection of paths
     *  @throws org.osid.NullArgumentException <code>paths</code> is
     *          <code>null</code>
     */

    public MutableIndexedMapPathLookupSession(java.util.Collection<? extends org.osid.mapping.path.Path> paths) {
        putPaths(paths);
        return;
    }

    
    /**
     *  Makes a <code>Path</code> available in this session.
     *
     *  @param  path a path
     *  @throws org.osid.NullArgumentException <code>path<code> is
     *          <code>null</code>
     */

    @Override
    public void putPath(org.osid.mapping.path.Path path) {
        super.putPath(path);
        return;
    }


    /**
     *  Makes an array of paths available in this session.
     *
     *  @param  paths an array of paths
     *  @throws org.osid.NullArgumentException <code>paths<code>
     *          is <code>null</code>
     */

    @Override
    public void putPaths(org.osid.mapping.path.Path[] paths) {
        super.putPaths(paths);
        return;
    }


    /**
     *  Makes collection of paths available in this session.
     *
     *  @param  paths a collection of paths
     *  @throws org.osid.NullArgumentException <code>path<code> is
     *          <code>null</code>
     */

    @Override
    public void putPaths(java.util.Collection<? extends org.osid.mapping.path.Path> paths) {
        super.putPaths(paths);
        return;
    }


    /**
     *  Removes a Path from this session.
     *
     *  @param pathId the <code>Id</code> of the path
     *  @throws org.osid.NullArgumentException <code>pathId<code> is
     *          <code>null</code>
     */

    @Override
    public void removePath(org.osid.id.Id pathId) {
        super.removePath(pathId);
        return;
    }    
}
