//
// AbstractControlRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractControlRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.control.rules.ControlRulesManager,
               org.osid.control.rules.ControlRulesProxyManager {

    private final Types deviceEnablerRecordTypes           = new TypeRefSet();
    private final Types deviceEnablerSearchRecordTypes     = new TypeRefSet();

    private final Types inputEnablerRecordTypes            = new TypeRefSet();
    private final Types inputEnablerSearchRecordTypes      = new TypeRefSet();

    private final Types triggerEnablerRecordTypes          = new TypeRefSet();
    private final Types triggerEnablerSearchRecordTypes    = new TypeRefSet();

    private final Types actionEnablerRecordTypes           = new TypeRefSet();
    private final Types actionEnablerSearchRecordTypes     = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractControlRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractControlRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up device enablers is supported. 
     *
     *  @return <code> true </code> if device enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying device enablers is supported. 
     *
     *  @return <code> true </code> if device enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching device enablers is supported. 
     *
     *  @return <code> true </code> if device enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a device enabler administrative service is supported. 
     *
     *  @return <code> true </code> if device enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a device enabler notification service is supported. 
     *
     *  @return <code> true </code> if device enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a device enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a device enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSystem() {
        return (false);
    }


    /**
     *  Tests if a device enabler system service is supported. 
     *
     *  @return <code> true </code> if device enabler system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if a device enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a device enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSmartSystem() {
        return (false);
    }


    /**
     *  Tests if looking up input enablers is supported. 
     *
     *  @return <code> true </code> if input enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying input enablers is supported. 
     *
     *  @return <code> true </code> if input enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching input enablers is supported. 
     *
     *  @return <code> true </code> if input enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an input enabler administrative service is supported. 
     *
     *  @return <code> true </code> if input enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an input enabler notification service is supported. 
     *
     *  @return <code> true </code> if input enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an input enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an input enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSystem() {
        return (false);
    }


    /**
     *  Tests if an input enabler system service is supported. 
     *
     *  @return <code> true </code> if input enabler system assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if an input enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an input enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerSmartSystem() {
        return (false);
    }


    /**
     *  Tests if an input enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an input enabler rule application service is supported. 
     *
     *  @return <code> true </code> if input enabler rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up trigger enablers is supported. 
     *
     *  @return <code> true </code> if trigger enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying trigger enablers is supported. 
     *
     *  @return <code> true </code> if trigger enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching trigger enablers is supported. 
     *
     *  @return <code> true </code> if trigger enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a trigger enabler administrative service is supported. 
     *
     *  @return <code> true </code> if trigger enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a trigger enabler notification service is supported. 
     *
     *  @return <code> true </code> if trigger enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a trigger enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSystem() {
        return (false);
    }


    /**
     *  Tests if a trigger enabler system service is supported. 
     *
     *  @return <code> true </code> if trigger enabler system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if a trigger enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSmartSystem() {
        return (false);
    }


    /**
     *  Tests if a trigger enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a trigger enabler rule application service is supported. 
     *
     *  @return <code> true </code> if trigger enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up action enablers is supported. 
     *
     *  @return <code> true </code> if action enabler lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying action enablers is supported. 
     *
     *  @return <code> true </code> if action enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching action enablers is supported. 
     *
     *  @return <code> true </code> if action enabler search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an action enabler administrative service is supported. 
     *
     *  @return <code> true </code> if action enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an action enabler notification service is supported. 
     *
     *  @return <code> true </code> if action enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an action enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an action enabler system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSystem() {
        return (false);
    }


    /**
     *  Tests if an action enabler system service is supported. 
     *
     *  @return <code> true </code> if action enabler system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSystemAssignment() {
        return (false);
    }


    /**
     *  Tests if an action enabler system lookup service is supported. 
     *
     *  @return <code> true </code> if an action enabler system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionEnablerSmartSystem() {
        return (false);
    }


    /**
     *  Gets the supported <code> DeviceEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> DeviceEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.deviceEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> DeviceEnabler </code> record type is 
     *  supported. 
     *
     *  @param  deviceEnablerRecordType a <code> Type </code> indicating a 
     *          <code> DeviceEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerRecordType(org.osid.type.Type deviceEnablerRecordType) {
        return (this.deviceEnablerRecordTypes.contains(deviceEnablerRecordType));
    }


    /**
     *  Adds support for a device enabler record type.
     *
     *  @param deviceEnablerRecordType a device enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>deviceEnablerRecordType</code> is <code>null</code>
     */

    protected void addDeviceEnablerRecordType(org.osid.type.Type deviceEnablerRecordType) {
        this.deviceEnablerRecordTypes.add(deviceEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a device enabler record type.
     *
     *  @param deviceEnablerRecordType a device enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>deviceEnablerRecordType</code> is <code>null</code>
     */

    protected void removeDeviceEnablerRecordType(org.osid.type.Type deviceEnablerRecordType) {
        this.deviceEnablerRecordTypes.remove(deviceEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> DeviceEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> DeviceEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.deviceEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> DeviceEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  deviceEnablerSearchRecordType a <code> Type </code> indicating 
     *          a <code> DeviceEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          deviceEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceEnablerSearchRecordType(org.osid.type.Type deviceEnablerSearchRecordType) {
        return (this.deviceEnablerSearchRecordTypes.contains(deviceEnablerSearchRecordType));
    }


    /**
     *  Adds support for a device enabler search record type.
     *
     *  @param deviceEnablerSearchRecordType a device enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>deviceEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addDeviceEnablerSearchRecordType(org.osid.type.Type deviceEnablerSearchRecordType) {
        this.deviceEnablerSearchRecordTypes.add(deviceEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a device enabler search record type.
     *
     *  @param deviceEnablerSearchRecordType a device enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>deviceEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeDeviceEnablerSearchRecordType(org.osid.type.Type deviceEnablerSearchRecordType) {
        this.deviceEnablerSearchRecordTypes.remove(deviceEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> InputEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> InputEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inputEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> InputEnabler </code> record type is 
     *  supported. 
     *
     *  @param  inputEnablerRecordType a <code> Type </code> indicating an 
     *          <code> InputEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inputEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputEnablerRecordType(org.osid.type.Type inputEnablerRecordType) {
        return (this.inputEnablerRecordTypes.contains(inputEnablerRecordType));
    }


    /**
     *  Adds support for an input enabler record type.
     *
     *  @param inputEnablerRecordType an input enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>inputEnablerRecordType</code> is <code>null</code>
     */

    protected void addInputEnablerRecordType(org.osid.type.Type inputEnablerRecordType) {
        this.inputEnablerRecordTypes.add(inputEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an input enabler record type.
     *
     *  @param inputEnablerRecordType an input enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inputEnablerRecordType</code> is <code>null</code>
     */

    protected void removeInputEnablerRecordType(org.osid.type.Type inputEnablerRecordType) {
        this.inputEnablerRecordTypes.remove(inputEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> InputEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> InputEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.inputEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> InputEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  inputEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> InputEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          inputEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputEnablerSearchRecordType(org.osid.type.Type inputEnablerSearchRecordType) {
        return (this.inputEnablerSearchRecordTypes.contains(inputEnablerSearchRecordType));
    }


    /**
     *  Adds support for an input enabler search record type.
     *
     *  @param inputEnablerSearchRecordType an input enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>inputEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addInputEnablerSearchRecordType(org.osid.type.Type inputEnablerSearchRecordType) {
        this.inputEnablerSearchRecordTypes.add(inputEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an input enabler search record type.
     *
     *  @param inputEnablerSearchRecordType an input enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>inputEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeInputEnablerSearchRecordType(org.osid.type.Type inputEnablerSearchRecordType) {
        this.inputEnablerSearchRecordTypes.remove(inputEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> TriggerEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> TriggerEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.triggerEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> TriggerEnabler </code> record type is 
     *  supported. 
     *
     *  @param  triggerEnablerRecordType a <code> Type </code> indicating a 
     *          <code> TriggerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerRecordType(org.osid.type.Type triggerEnablerRecordType) {
        return (this.triggerEnablerRecordTypes.contains(triggerEnablerRecordType));
    }


    /**
     *  Adds support for a trigger enabler record type.
     *
     *  @param triggerEnablerRecordType a trigger enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>triggerEnablerRecordType</code> is <code>null</code>
     */

    protected void addTriggerEnablerRecordType(org.osid.type.Type triggerEnablerRecordType) {
        this.triggerEnablerRecordTypes.add(triggerEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a trigger enabler record type.
     *
     *  @param triggerEnablerRecordType a trigger enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>triggerEnablerRecordType</code> is <code>null</code>
     */

    protected void removeTriggerEnablerRecordType(org.osid.type.Type triggerEnablerRecordType) {
        this.triggerEnablerRecordTypes.remove(triggerEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> TriggerEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> TriggerEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.triggerEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> TriggerEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  triggerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> TriggerEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          triggerEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerEnablerSearchRecordType(org.osid.type.Type triggerEnablerSearchRecordType) {
        return (this.triggerEnablerSearchRecordTypes.contains(triggerEnablerSearchRecordType));
    }


    /**
     *  Adds support for a trigger enabler search record type.
     *
     *  @param triggerEnablerSearchRecordType a trigger enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>triggerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addTriggerEnablerSearchRecordType(org.osid.type.Type triggerEnablerSearchRecordType) {
        this.triggerEnablerSearchRecordTypes.add(triggerEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a trigger enabler search record type.
     *
     *  @param triggerEnablerSearchRecordType a trigger enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>triggerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeTriggerEnablerSearchRecordType(org.osid.type.Type triggerEnablerSearchRecordType) {
        this.triggerEnablerSearchRecordTypes.remove(triggerEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ActionEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> ActionEnabler </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.actionEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActionEnabler </code> record type is 
     *  supported. 
     *
     *  @param  actionEnablerRecordType a <code> Type </code> indicating an 
     *          <code> ActionEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> actionEnablerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionEnablerRecordType(org.osid.type.Type actionEnablerRecordType) {
        return (this.actionEnablerRecordTypes.contains(actionEnablerRecordType));
    }


    /**
     *  Adds support for an action enabler record type.
     *
     *  @param actionEnablerRecordType an action enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>actionEnablerRecordType</code> is <code>null</code>
     */

    protected void addActionEnablerRecordType(org.osid.type.Type actionEnablerRecordType) {
        this.actionEnablerRecordTypes.add(actionEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an action enabler record type.
     *
     *  @param actionEnablerRecordType an action enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>actionEnablerRecordType</code> is <code>null</code>
     */

    protected void removeActionEnablerRecordType(org.osid.type.Type actionEnablerRecordType) {
        this.actionEnablerRecordTypes.remove(actionEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ActionEnabler </code> search record types. 
     *
     *  @return a list containing the supported <code> ActionEnabler </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.actionEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ActionEnabler </code> search record type is 
     *  supported. 
     *
     *  @param  actionEnablerSearchRecordType a <code> Type </code> indicating 
     *          an <code> ActionEnabler </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          actionEnablerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionEnablerSearchRecordType(org.osid.type.Type actionEnablerSearchRecordType) {
        return (this.actionEnablerSearchRecordTypes.contains(actionEnablerSearchRecordType));
    }


    /**
     *  Adds support for an action enabler search record type.
     *
     *  @param actionEnablerSearchRecordType an action enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>actionEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addActionEnablerSearchRecordType(org.osid.type.Type actionEnablerSearchRecordType) {
        this.actionEnablerSearchRecordTypes.add(actionEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an action enabler search record type.
     *
     *  @param actionEnablerSearchRecordType an action enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>actionEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeActionEnablerSearchRecordType(org.osid.type.Type actionEnablerSearchRecordType) {
        this.actionEnablerSearchRecordTypes.remove(actionEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  lookup service. 
     *
     *  @return a <code> DeviceEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerLookupSession getDeviceEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerLookupSession getDeviceEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerLookupSession getDeviceEnablerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerLookupSession getDeviceEnablerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  query service. 
     *
     *  @return a <code> DeviceEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerQuerySession getDeviceEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerQuerySession getDeviceEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  query service for the given System. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerQuerySession getDeviceEnablerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerQuerySession getDeviceEnablerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  search service. 
     *
     *  @return a <code> DeviceEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSearchSession getDeviceEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSearchSession getDeviceEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSearchSession getDeviceEnablerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSearchSession getDeviceEnablerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  administration service. 
     *
     *  @return a <code> DeviceEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerAdminSession getDeviceEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerAdminSession getDeviceEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerAdminSession getDeviceEnablerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerAdminSession getDeviceEnablerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  notification service. 
     *
     *  @param  deviceEnablerReceiver the notification callback 
     *  @return a <code> DeviceEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerNotificationSession getDeviceEnablerNotificationSession(org.osid.control.rules.DeviceEnablerReceiver deviceEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  notification service. 
     *
     *  @param  deviceEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerNotificationSession getDeviceEnablerNotificationSession(org.osid.control.rules.DeviceEnablerReceiver deviceEnablerReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  notification service for the given system. 
     *
     *  @param  deviceEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerNotificationSession getDeviceEnablerNotificationSessionForSystem(org.osid.control.rules.DeviceEnablerReceiver deviceEnablerReceiver, 
                                                                                                                org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  notification service for the given system. 
     *
     *  @param  deviceEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> deviceEnablerReceiver, 
     *          systemId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerNotificationSession getDeviceEnablerNotificationSessionForSystem(org.osid.control.rules.DeviceEnablerReceiver deviceEnablerReceiver, 
                                                                                                                org.osid.id.Id systemId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup device enabler/system 
     *  mappings for device enablers. 
     *
     *  @return a <code> DeviceEnablerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSystemSession getDeviceEnablerSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup device enabler/system 
     *  mappings for device enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSystemSession getDeviceEnablerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning device 
     *  enablers to systems. 
     *
     *  @return a <code> DeviceEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSystemAssignmentSession getDeviceEnablerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning device 
     *  enablers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSystemAssignmentSession getDeviceEnablerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage device enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSmartSystemSession getDeviceEnablerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage device enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerSmartSystemSession getDeviceEnablerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  mapping lookup service. 
     *
     *  @return a <code> DeviceEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleLookupSession getDeviceEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleLookupSession getDeviceEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleLookupSession getDeviceEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerRuleLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleLookupSession getDeviceEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerRuleLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  assignment service. 
     *
     *  @return a <code> DeviceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleApplicationSession getDeviceEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleApplicationSession getDeviceEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> DeviceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleApplicationSession getDeviceEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getDeviceEnablerRuleApplicationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.DeviceEnablerRuleApplicationSession getDeviceEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getDeviceEnablerRuleApplicationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  lookup service. 
     *
     *  @return an <code> InputEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerLookupSession getInputEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerLookupSession getInputEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerLookupSession getInputEnablerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerLookupSession getInputEnablerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  query service. 
     *
     *  @return an <code> InputEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerQuerySession getInputEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerQuerySession getInputEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerQuerySession getInputEnablerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerQuerySession getInputEnablerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  search service. 
     *
     *  @return an <code> InputEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSearchSession getInputEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSearchSession getInputEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enablers 
     *  earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSearchSession getInputEnablerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enablers 
     *  earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSearchSession getInputEnablerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  administration service. 
     *
     *  @return an <code> InputEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerAdminSession getInputEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerAdminSession getInputEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerAdminSession getInputEnablerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerAdminSession getInputEnablerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  notification service. 
     *
     *  @param  inputEnablerReceiver the notification callback 
     *  @return an <code> InputEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inputEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerNotificationSession getInputEnablerNotificationSession(org.osid.control.rules.InputEnablerReceiver inputEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  notification service. 
     *
     *  @param  deviceProcessoEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          deviceProcessoEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerNotificationSession getInputEnablerNotificationSession(org.osid.control.rules.InputEnablerReceiver deviceProcessoEnablerReceiver, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  notification service for the given system. 
     *
     *  @param  inputEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> inputEnablerReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerNotificationSession getInputEnablerNotificationSessionForSystem(org.osid.control.rules.InputEnablerReceiver inputEnablerReceiver, 
                                                                                                              org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  notification service for the given system. 
     *
     *  @param  deviceProcessoEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          deviceProcessoEnablerReceiver, systemId, </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerNotificationSession getInputEnablerNotificationSessionForSystem(org.osid.control.rules.InputEnablerReceiver deviceProcessoEnablerReceiver, 
                                                                                                              org.osid.id.Id systemId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup input enabler/system 
     *  mappings for input enablers. 
     *
     *  @return an <code> InputEnablerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSystemSession getInputEnablerSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup input enabler/system 
     *  mappings for input enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSystemSession getInputEnablerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning input 
     *  enablers to systems. 
     *
     *  @return an <code> InputEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSystemAssignmentSession getInputEnablerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning input 
     *  enablers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSystemAssignmentSession getInputEnablerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage input enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSmartSystemSession getInputEnablerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage input enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerSmartSystemSession getInputEnablerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  mapping lookup service. 
     *
     *  @return an <code> InputEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleLookupSession getInputEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleLookupSession getInputEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  mapping lookup service. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleLookupSession getInputEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerRuleLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleLookupSession getInputEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerRuleLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  assignment service. 
     *
     *  @return an <code> InputEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleApplicationSession getInputEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleApplicationSession getInputEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> InputEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleApplicationSession getInputEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getInputEnablerRuleApplicationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.InputEnablerRuleApplicationSession getInputEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getInputEnablerRuleApplicationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler lookup service. 
     *
     *  @return a <code> TriggerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerLookupSession getTriggerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerLookupSession getTriggerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerLookupSession getTriggerEnablerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerLookupSession getTriggerEnablerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler query service. 
     *
     *  @return a <code> TriggerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerQuerySession getTriggerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerQuerySession getTriggerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerQuerySession getTriggerEnablerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerQuerySession getTriggerEnablerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler search service. 
     *
     *  @return a <code> TriggerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSearchSession getTriggerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSearchSession getTriggerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSearchSession getTriggerEnablerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSearchSession getTriggerEnablerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler administration service. 
     *
     *  @return a <code> TriggerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerAdminSession getTriggerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerAdminSession getTriggerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerAdminSession getTriggerEnablerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerAdminSession getTriggerEnablerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler notification service. 
     *
     *  @param  triggerEnablerReceiver the notification callback 
     *  @return a <code> TriggerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerNotificationSession getTriggerEnablerNotificationSession(org.osid.control.rules.TriggerEnablerReceiver triggerEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler notification service. 
     *
     *  @param  triggerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerNotificationSession getTriggerEnablerNotificationSession(org.osid.control.rules.TriggerEnablerReceiver triggerEnablerReceiver, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler notification service for the given system. 
     *
     *  @param  triggerEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerNotificationSession getTriggerEnablerNotificationSessionForSystem(org.osid.control.rules.TriggerEnablerReceiver triggerEnablerReceiver, 
                                                                                                                  org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler notification service for the given system. 
     *
     *  @param  triggerEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> triggerEnablerReceiver, 
     *          systemId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerNotificationSession getTriggerEnablerNotificationSessionForSystem(org.osid.control.rules.TriggerEnablerReceiver triggerEnablerReceiver, 
                                                                                                                  org.osid.id.Id systemId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup trigger enabler/system 
     *  mappings for trigger enablers. 
     *
     *  @return a <code> TriggerEnablerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSystemSession getTriggerEnablerSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup trigger enabler/system 
     *  mappings for trigger enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSystemSession getTriggerEnablerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning trigger 
     *  enablers to systems. 
     *
     *  @return a <code> TriggerEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSystemAssignmentSession getTriggerEnablerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning trigger 
     *  enablers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSystemAssignmentSession getTriggerEnablerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage trigger enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSmartSystemSession getTriggerEnablerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage trigger enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerSmartSystemSession getTriggerEnablerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> TriggerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleLookupSession getTriggerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleLookupSession getTriggerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleLookupSession getTriggerEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerRuleLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleLookupSession getTriggerEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerRuleLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler assignment service. 
     *
     *  @return a <code> TriggerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleApplicationSession getTriggerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleApplicationSession getTriggerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return a <code> TriggerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleApplicationSession getTriggerEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getTriggerEnablerRuleApplicationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  enabler assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no System found by the given Id 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerRuleApplicationSession getTriggerEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getTriggerEnablerRuleApplicationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  lookup service. 
     *
     *  @return an <code> ActionEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerLookupSession getActionEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException an <code> 
     *          ActionEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerLookupSession getActionEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerLookupSession getActionEnablerLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerLookupSession getActionEnablerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  query service. 
     *
     *  @return an <code> ActionEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerQuerySession getActionEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerQuerySession getActionEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerQuerySession getActionEnablerQuerySessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerQuerySession getActionEnablerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerQuerySessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  search service. 
     *
     *  @return an <code> ActionEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSearchSession getActionEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSearchSession getActionEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSearchSession getActionEnablerSearchSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action 
     *  enablers earch service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSearchSession getActionEnablerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerSearchSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  administration service. 
     *
     *  @return an <code> ActionEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerAdminSession getActionEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerAdminSession getActionEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerAdminSession getActionEnablerAdminSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerAdminSession getActionEnablerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerAdminSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  notification service. 
     *
     *  @param  actionEnablerReceiver the notification callback 
     *  @return an <code> ActionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> actionEnablerReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerNotificationSession getActionEnablerNotificationSession(org.osid.control.rules.ActionEnablerReceiver actionEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  notification service. 
     *
     *  @param  actionEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> actionEnablerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerNotificationSession getActionEnablerNotificationSession(org.osid.control.rules.ActionEnablerReceiver actionEnablerReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  notification service for the given system. 
     *
     *  @param  actionEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> actionEnablerReceiver 
     *          </code> or <code> systemId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerNotificationSession getActionEnablerNotificationSessionForSystem(org.osid.control.rules.ActionEnablerReceiver actionEnablerReceiver, 
                                                                                                                org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  notification service for the given system. 
     *
     *  @param  actionEnablerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> actionEnablerReceiver, 
     *          systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerNotificationSession getActionEnablerNotificationSessionForSystem(org.osid.control.rules.ActionEnablerReceiver actionEnablerReceiver, 
                                                                                                                org.osid.id.Id systemId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerNotificationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup action enabler/system 
     *  mappings for action enablers. 
     *
     *  @return an <code> ActionEnablerSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSystemSession getActionEnablerSystemSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup action enabler/system 
     *  mappings for action enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSystemSession getActionEnablerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning action 
     *  enablers to systems. 
     *
     *  @return an <code> ActionEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSystemAssignmentSession getActionEnablerSystemAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning action 
     *  enablers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSystemAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSystemAssignmentSession getActionEnablerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerSystemAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage action enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSmartSystemSession getActionEnablerSmartSystemSession(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage action enabler smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerSmartSystemSession getActionEnablerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerSmartSystemSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  mapping lookup service. 
     *
     *  @return an <code> ActionEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleLookupSession getActionEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleLookupSession getActionEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleLookupSession getActionEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerRuleLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  mapping lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleLookupSession getActionEnablerRuleLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerRuleLookupSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  assignment service. 
     *
     *  @return an <code> ActionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleApplicationSession getActionEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleApplicationSession getActionEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @return an <code> ActionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleApplicationSession getActionEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesManager.getActionEnablerRuleApplicationSessionForSystem not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action enabler 
     *  assignment service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerRuleApplicationSession getActionEnablerRuleApplicationSessionForSystem(org.osid.id.Id systemId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.control.rules.ControlRulesProxyManager.getActionEnablerRuleApplicationSessionForSystem not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.deviceEnablerRecordTypes.clear();
        this.deviceEnablerRecordTypes.clear();

        this.deviceEnablerSearchRecordTypes.clear();
        this.deviceEnablerSearchRecordTypes.clear();

        this.inputEnablerRecordTypes.clear();
        this.inputEnablerRecordTypes.clear();

        this.inputEnablerSearchRecordTypes.clear();
        this.inputEnablerSearchRecordTypes.clear();

        this.triggerEnablerRecordTypes.clear();
        this.triggerEnablerRecordTypes.clear();

        this.triggerEnablerSearchRecordTypes.clear();
        this.triggerEnablerSearchRecordTypes.clear();

        this.actionEnablerRecordTypes.clear();
        this.actionEnablerRecordTypes.clear();

        this.actionEnablerSearchRecordTypes.clear();
        this.actionEnablerSearchRecordTypes.clear();

        return;
    }
}
