//
// AbstractAliasResponse.java
//
//     Defines an AliasResponse builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.transaction.batch.aliasresponse.spi;


/**
 *  Defines an <code>AliasResponse</code> builder.
 */

public abstract class AbstractAliasResponseBuilder<T extends AbstractAliasResponseBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.transaction.batch.aliasresponse.AliasResponseMiter aliasResponse;


    /**
     *  Constructs a new <code>AbstractAliasResponseBuilder</code>.
     *
     *  @param aliasResponse the alias response to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAliasResponseBuilder(net.okapia.osid.jamocha.builder.transaction.batch.aliasresponse.AliasResponseMiter aliasResponse) {
        this.aliasResponse = aliasResponse;
        return;
    }


    /**
     *  Builds the alias response.
     *
     *  @return the new alias response
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>aliasResponse</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.transaction.batch.AliasResponse build() {
        (new net.okapia.osid.jamocha.builder.validator.transaction.batch.aliasresponse.AliasResponseValidator(getValidations())).validate(this.aliasResponse);
        return (new net.okapia.osid.jamocha.builder.transaction.batch.aliasresponse.ImmutableAliasResponse(this.aliasResponse));
    }


    /**
     *  Gets the alias response. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new aliasResponse
     */

    @Override
    public net.okapia.osid.jamocha.builder.transaction.batch.aliasresponse.AliasResponseMiter getMiter() {
        return (this.aliasResponse);
    }


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    public T reference(org.osid.id.Id referenceId) {
        getMiter().setReferenceId(referenceId);
        return (self());
    }


    /**
     *  Sets the alias id.
     *
     *  @param aliasId an alias id
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>aliasId</code> is <code>null</code>
     */

    public T alias(org.osid.id.Id aliasId) {
        getMiter().setAliasId(aliasId);
        return (self());
    }


    /**
     *  Sets the error message.
     *
     *  @param message an error message
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public T error(org.osid.locale.DisplayText message) {
        getMiter().setErrorMessage(message);
        return (self());
    }
}       


