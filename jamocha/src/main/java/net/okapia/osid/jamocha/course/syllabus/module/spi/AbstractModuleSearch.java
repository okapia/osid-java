//
// AbstractModuleSearch.java
//
//     A template for making a Module Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.syllabus.module.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing module searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractModuleSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.syllabus.ModuleSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.syllabus.records.ModuleSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.syllabus.ModuleSearchOrder moduleSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of modules. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  moduleIds list of modules
     *  @throws org.osid.NullArgumentException
     *          <code>moduleIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongModules(org.osid.id.IdList moduleIds) {
        while (moduleIds.hasNext()) {
            try {
                this.ids.add(moduleIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongModules</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of module Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getModuleIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  moduleSearchOrder module search order 
     *  @throws org.osid.NullArgumentException
     *          <code>moduleSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>moduleSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderModuleResults(org.osid.course.syllabus.ModuleSearchOrder moduleSearchOrder) {
	this.moduleSearchOrder = moduleSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.syllabus.ModuleSearchOrder getModuleSearchOrder() {
	return (this.moduleSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given module search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a module implementing the requested record.
     *
     *  @param moduleSearchRecordType a module search record
     *         type
     *  @return the module search record
     *  @throws org.osid.NullArgumentException
     *          <code>moduleSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(moduleSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.syllabus.records.ModuleSearchRecord getModuleSearchRecord(org.osid.type.Type moduleSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.syllabus.records.ModuleSearchRecord record : this.records) {
            if (record.implementsRecordType(moduleSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(moduleSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this module search. 
     *
     *  @param moduleSearchRecord module search record
     *  @param moduleSearchRecordType module search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addModuleSearchRecord(org.osid.course.syllabus.records.ModuleSearchRecord moduleSearchRecord, 
                                           org.osid.type.Type moduleSearchRecordType) {

        addRecordType(moduleSearchRecordType);
        this.records.add(moduleSearchRecord);        
        return;
    }
}
