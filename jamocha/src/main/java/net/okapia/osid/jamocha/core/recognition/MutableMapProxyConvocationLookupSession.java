//
// MutableMapProxyConvocationLookupSession
//
//    Implements a Convocation lookup service backed by a collection of
//    convocations that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recognition;


/**
 *  Implements a Convocation lookup service backed by a collection of
 *  convocations. The convocations are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of convocations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyConvocationLookupSession
    extends net.okapia.osid.jamocha.core.recognition.spi.AbstractMapConvocationLookupSession
    implements org.osid.recognition.ConvocationLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyConvocationLookupSession}
     *  with no convocations.
     *
     *  @param academy the academy
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyConvocationLookupSession(org.osid.recognition.Academy academy,
                                                  org.osid.proxy.Proxy proxy) {
        setAcademy(academy);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyConvocationLookupSession} with a
     *  single convocation.
     *
     *  @param academy the academy
     *  @param convocation a convocation
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code convocation}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyConvocationLookupSession(org.osid.recognition.Academy academy,
                                                org.osid.recognition.Convocation convocation, org.osid.proxy.Proxy proxy) {
        this(academy, proxy);
        putConvocation(convocation);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyConvocationLookupSession} using an
     *  array of convocations.
     *
     *  @param academy the academy
     *  @param convocations an array of convocations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code convocations}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyConvocationLookupSession(org.osid.recognition.Academy academy,
                                                org.osid.recognition.Convocation[] convocations, org.osid.proxy.Proxy proxy) {
        this(academy, proxy);
        putConvocations(convocations);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyConvocationLookupSession} using a
     *  collection of convocations.
     *
     *  @param academy the academy
     *  @param convocations a collection of convocations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code academy},
     *          {@code convocations}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyConvocationLookupSession(org.osid.recognition.Academy academy,
                                                java.util.Collection<? extends org.osid.recognition.Convocation> convocations,
                                                org.osid.proxy.Proxy proxy) {
   
        this(academy, proxy);
        setSessionProxy(proxy);
        putConvocations(convocations);
        return;
    }

    
    /**
     *  Makes a {@code Convocation} available in this session.
     *
     *  @param convocation an convocation
     *  @throws org.osid.NullArgumentException {@code convocation{@code 
     *          is {@code null}
     */

    @Override
    public void putConvocation(org.osid.recognition.Convocation convocation) {
        super.putConvocation(convocation);
        return;
    }


    /**
     *  Makes an array of convocations available in this session.
     *
     *  @param convocations an array of convocations
     *  @throws org.osid.NullArgumentException {@code convocations{@code 
     *          is {@code null}
     */

    @Override
    public void putConvocations(org.osid.recognition.Convocation[] convocations) {
        super.putConvocations(convocations);
        return;
    }


    /**
     *  Makes collection of convocations available in this session.
     *
     *  @param convocations
     *  @throws org.osid.NullArgumentException {@code convocation{@code 
     *          is {@code null}
     */

    @Override
    public void putConvocations(java.util.Collection<? extends org.osid.recognition.Convocation> convocations) {
        super.putConvocations(convocations);
        return;
    }


    /**
     *  Removes a Convocation from this session.
     *
     *  @param convocationId the {@code Id} of the convocation
     *  @throws org.osid.NullArgumentException {@code convocationId{@code  is
     *          {@code null}
     */

    @Override
    public void removeConvocation(org.osid.id.Id convocationId) {
        super.removeConvocation(convocationId);
        return;
    }    
}
