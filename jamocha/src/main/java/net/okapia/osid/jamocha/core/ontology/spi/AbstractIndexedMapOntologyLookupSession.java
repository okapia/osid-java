//
// AbstractIndexedMapOntologyLookupSession.java
//
//    A simple framework for providing an Ontology lookup service
//    backed by a fixed collection of ontologies with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Ontology lookup service backed by a
 *  fixed collection of ontologies. The ontologies are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some ontologies may be compatible
 *  with more types than are indicated through these ontology
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Ontologies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapOntologyLookupSession
    extends AbstractMapOntologyLookupSession
    implements org.osid.ontology.OntologyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.ontology.Ontology> ontologiesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ontology.Ontology>());
    private final MultiMap<org.osid.type.Type, org.osid.ontology.Ontology> ontologiesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ontology.Ontology>());


    /**
     *  Makes an <code>Ontology</code> available in this session.
     *
     *  @param  ontology an ontology
     *  @throws org.osid.NullArgumentException <code>ontology<code> is
     *          <code>null</code>
     */

    @Override
    protected void putOntology(org.osid.ontology.Ontology ontology) {
        super.putOntology(ontology);

        this.ontologiesByGenus.put(ontology.getGenusType(), ontology);
        
        try (org.osid.type.TypeList types = ontology.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ontologiesByRecord.put(types.getNextType(), ontology);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an ontology from this session.
     *
     *  @param ontologyId the <code>Id</code> of the ontology
     *  @throws org.osid.NullArgumentException <code>ontologyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeOntology(org.osid.id.Id ontologyId) {
        org.osid.ontology.Ontology ontology;
        try {
            ontology = getOntology(ontologyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.ontologiesByGenus.remove(ontology.getGenusType());

        try (org.osid.type.TypeList types = ontology.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.ontologiesByRecord.remove(types.getNextType(), ontology);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeOntology(ontologyId);
        return;
    }


    /**
     *  Gets an <code>OntologyList</code> corresponding to the given
     *  ontology genus <code>Type</code> which does not include
     *  ontologies of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known ontologies or an error results. Otherwise,
     *  the returned list may contain only those ontologies that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  ontologyGenusType an ontology genus type 
     *  @return the returned <code>Ontology</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByGenusType(org.osid.type.Type ontologyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.ontology.ArrayOntologyList(this.ontologiesByGenus.get(ontologyGenusType)));
    }


    /**
     *  Gets an <code>OntologyList</code> containing the given
     *  ontology record <code>Type</code>. In plenary mode, the
     *  returned list contains all known ontologies or an error
     *  results. Otherwise, the returned list may contain only those
     *  ontologies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  ontologyRecordType an ontology record type 
     *  @return the returned <code>ontology</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByRecordType(org.osid.type.Type ontologyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ontology.ontology.ArrayOntologyList(this.ontologiesByRecord.get(ontologyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.ontologiesByGenus.clear();
        this.ontologiesByRecord.clear();

        super.close();

        return;
    }
}
