//
// AbstractProvisioningManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractProvisioningManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.provisioning.ProvisioningManager,
               org.osid.provisioning.ProvisioningProxyManager {

    private final Types provisionRecordTypes               = new TypeRefSet();
    private final Types provisionSearchRecordTypes         = new TypeRefSet();

    private final Types provisionReturnRecordTypes         = new TypeRefSet();
    private final Types queueRecordTypes                   = new TypeRefSet();
    private final Types queueSearchRecordTypes             = new TypeRefSet();

    private final Types requestRecordTypes                 = new TypeRefSet();
    private final Types requestSearchRecordTypes           = new TypeRefSet();

    private final Types requestTransactionRecordTypes      = new TypeRefSet();
    private final Types poolRecordTypes                    = new TypeRefSet();
    private final Types poolSearchRecordTypes              = new TypeRefSet();

    private final Types provisionableRecordTypes           = new TypeRefSet();
    private final Types provisionableSearchRecordTypes     = new TypeRefSet();

    private final Types brokerRecordTypes                  = new TypeRefSet();
    private final Types brokerSearchRecordTypes            = new TypeRefSet();

    private final Types distributorRecordTypes             = new TypeRefSet();
    private final Types distributorSearchRecordTypes       = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractProvisioningManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractProvisioningManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a my provision service is supported for the current agent. 
     *
     *  @return <code> true </code> if my provision is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyProvision() {
        return (false);
    }


    /**
     *  Tests if a my supplier service is supported for the current agent. 
     *
     *  @return <code> true </code> if my supplier is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMySupplier() {
        return (false);
    }


    /**
     *  Tests if a my provision notification service is supported for the 
     *  current agent. 
     *
     *  @return <code> true </code> if my provision notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyProvisionNotification() {
        return (false);
    }


    /**
     *  Tests if looking up provisions is supported. 
     *
     *  @return <code> true </code> if provision lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionLookup() {
        return (false);
    }


    /**
     *  Tests if querying provisions is supported. 
     *
     *  @return <code> true </code> if provision query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionQuery() {
        return (false);
    }


    /**
     *  Tests if searching provisions is supported. 
     *
     *  @return <code> true </code> if provision search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionSearch() {
        return (false);
    }


    /**
     *  Tests if a provision administrative service is supported. 
     *
     *  @return <code> true </code> if provision administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionAdmin() {
        return (false);
    }


    /**
     *  Tests if a provision <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if provision notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionNotification() {
        return (false);
    }


    /**
     *  Tests if a provision distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a provision distributor lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionDistributor() {
        return (false);
    }


    /**
     *  Tests if a provision distributor assignment service is supported. 
     *
     *  @return <code> true </code> if a provision to distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionDistributorAssignment() {
        return (false);
    }


    /**
     *  Tests if a provision smart distributor service is supported. 
     *
     *  @return <code> true </code> if an v smart distributor service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionSmartDistributor() {
        return (false);
    }


    /**
     *  Tests if returning provisions is supported. 
     *
     *  @return <code> true </code> if returning provisions is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionReturn() {
        return (false);
    }


    /**
     *  Tests if looking up queues is supported. 
     *
     *  @return <code> true </code> if queue lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueLookup() {
        return (false);
    }


    /**
     *  Tests if querying queues is supported. 
     *
     *  @return <code> true </code> if queue query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Tests if searching queues is supported. 
     *
     *  @return <code> true </code> if queue search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSearch() {
        return (false);
    }


    /**
     *  Tests if queue administrative service is supported. 
     *
     *  @return <code> true </code> if queue administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueAdmin() {
        return (false);
    }


    /**
     *  Tests if a queue notification service is supported. 
     *
     *  @return <code> true </code> if queue notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueNotification() {
        return (false);
    }


    /**
     *  Tests if a queue broker lookup service is supported. 
     *
     *  @return <code> true </code> if a queue broker lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueBroker() {
        return (false);
    }


    /**
     *  Tests if a queue broker service is supported. 
     *
     *  @return <code> true </code> if queue to broker assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueBrokerAssignment() {
        return (false);
    }


    /**
     *  Tests if a queue smart broker lookup service is supported. 
     *
     *  @return <code> true </code> if a queue smart broker service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSmartBroker() {
        return (false);
    }


    /**
     *  Tests if looking up requests is supported. 
     *
     *  @return <code> true </code> if request lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestLookup() {
        return (false);
    }


    /**
     *  Tests if querying requests is supported. 
     *
     *  @return <code> true </code> if request query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestQuery() {
        return (false);
    }


    /**
     *  Tests if searching requests is supported. 
     *
     *  @return <code> true </code> if request search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestSearch() {
        return (false);
    }


    /**
     *  Tests if request administrative service is supported. 
     *
     *  @return <code> true </code> if request administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestAdmin() {
        return (false);
    }


    /**
     *  Tests if a request notification service is supported. 
     *
     *  @return <code> true </code> if request notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestNotification() {
        return (false);
    }


    /**
     *  Tests if a request distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a request distributor lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestDistributor() {
        return (false);
    }


    /**
     *  Tests if a request distributor service is supported. 
     *
     *  @return <code> true </code> if request to distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestDistributorAssignment() {
        return (false);
    }


    /**
     *  Tests if a request smart distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a request smart distributor service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestSmartDistributor() {
        return (false);
    }


    /**
     *  Tests if looking up request transactions is supported. 
     *
     *  @return <code> true </code> if request transaction lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionLookup() {
        return (false);
    }


    /**
     *  Tests if request transaction administrative service is supported. 
     *
     *  @return <code> true </code> if request transaction administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionAdmin() {
        return (false);
    }


    /**
     *  Tests if exchanging provisions is supported. 
     *
     *  @return <code> true </code> if exchange is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExchange() {
        return (false);
    }


    /**
     *  Tests if looking up pools is supported. 
     *
     *  @return <code> true </code> if pool lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolLookup() {
        return (false);
    }


    /**
     *  Tests if querying pools is supported. 
     *
     *  @return <code> true </code> if pool query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (false);
    }


    /**
     *  Tests if searching pools is supported. 
     *
     *  @return <code> true </code> if pool search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolSearch() {
        return (false);
    }


    /**
     *  Tests if a pool administrative service is supported. 
     *
     *  @return <code> true </code> if pool administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolAdmin() {
        return (false);
    }


    /**
     *  Tests if a pool <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if pool notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolNotification() {
        return (false);
    }


    /**
     *  Tests if a pool distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a pool distributor lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolDistributor() {
        return (false);
    }


    /**
     *  Tests if a pool distributor assignment service is supported. 
     *
     *  @return <code> true </code> if a pool to distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolDistributorAssignment() {
        return (false);
    }


    /**
     *  Tests if a pool smart distributor service is supported. 
     *
     *  @return <code> true </code> if a smart distributor service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolSmartDistributor() {
        return (false);
    }


    /**
     *  Tests if looking up provisionables is supported. 
     *
     *  @return <code> true </code> if provisionable lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableLookup() {
        return (false);
    }


    /**
     *  Tests if querying provisionables is supported. 
     *
     *  @return <code> true </code> if provisionable query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableQuery() {
        return (false);
    }


    /**
     *  Tests if searching provisionables is supported. 
     *
     *  @return <code> true </code> if provisionable search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableSearch() {
        return (false);
    }


    /**
     *  Tests if provisionable <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if provisionable administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableAdmin() {
        return (false);
    }


    /**
     *  Tests if a provisionable <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if provisionable notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableNotification() {
        return (false);
    }


    /**
     *  Tests if a provisionable distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a provisionable distributor lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableDistributor() {
        return (false);
    }


    /**
     *  Tests if a provisionable distributor assignment service is supported. 
     *
     *  @return <code> true </code> if a provisionable to distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableDistributorAssignment() {
        return (false);
    }


    /**
     *  Tests if a provisionable smart distributor service is supported. 
     *
     *  @return <code> true </code> if a provisionable smart distributor 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableSmartDistributor() {
        return (false);
    }


    /**
     *  Tests if looking up brokers is supported. 
     *
     *  @return <code> true </code> if broker lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerLookup() {
        return (false);
    }


    /**
     *  Tests if querying brokers is supported. 
     *
     *  @return <code> true </code> if a broker query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Tests if searching brokers is supported. 
     *
     *  @return <code> true </code> if broker search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSearch() {
        return (false);
    }


    /**
     *  Tests if broker administrative service is supported. 
     *
     *  @return <code> true </code> if broker administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerAdmin() {
        return (false);
    }


    /**
     *  Tests if a broker <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if broker notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerNotification() {
        return (false);
    }


    /**
     *  Tests if a broker distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a broker distributor lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerDistributor() {
        return (false);
    }


    /**
     *  Tests if a broker distributor service is supported. 
     *
     *  @return <code> true </code> if broker to distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerDistributorAssignment() {
        return (false);
    }


    /**
     *  Tests if a broker smart distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a broker smart distributor service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSmartDistributor() {
        return (false);
    }


    /**
     *  Tests if looking up distributors is supported. 
     *
     *  @return <code> true </code> if distributor lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorLookup() {
        return (false);
    }


    /**
     *  Tests if querying distributors is supported. 
     *
     *  @return <code> true </code> if a distributor query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Tests if searching distributors is supported. 
     *
     *  @return <code> true </code> if distributor search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorSearch() {
        return (false);
    }


    /**
     *  Tests if distributor administrative service is supported. 
     *
     *  @return <code> true </code> if distributor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorAdmin() {
        return (false);
    }


    /**
     *  Tests if a distributor <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if distributor notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a distributor hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if distributor hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a distributor hierarchy design service. 
     *
     *  @return <code> true </code> if distributor hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a provisioning batch service. 
     *
     *  @return <code> true </code> if provisioning batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisioningBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a provisioning rules service. 
     *
     *  @return <code> true </code> if provisioning rules service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisioningRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Provision </code> record types. 
     *
     *  @return a list containing the supported <code> Provision </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.provisionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Provision </code> record type is supported. 
     *
     *  @param  provisionRecordType a <code> Type </code> indicating a <code> 
     *          Provision </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> provisionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionRecordType(org.osid.type.Type provisionRecordType) {
        return (this.provisionRecordTypes.contains(provisionRecordType));
    }


    /**
     *  Adds support for a provision record type.
     *
     *  @param provisionRecordType a provision record type
     *  @throws org.osid.NullArgumentException
     *  <code>provisionRecordType</code> is <code>null</code>
     */

    protected void addProvisionRecordType(org.osid.type.Type provisionRecordType) {
        this.provisionRecordTypes.add(provisionRecordType);
        return;
    }


    /**
     *  Removes support for a provision record type.
     *
     *  @param provisionRecordType a provision record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>provisionRecordType</code> is <code>null</code>
     */

    protected void removeProvisionRecordType(org.osid.type.Type provisionRecordType) {
        this.provisionRecordTypes.remove(provisionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Provision </code> search types. 
     *
     *  @return a list containing the supported <code> Provision </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.provisionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Provision </code> search type is supported. 
     *
     *  @param  provisionSearchRecordType a <code> Type </code> indicating a 
     *          <code> Provision </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> effiortSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionSearchRecordType(org.osid.type.Type provisionSearchRecordType) {
        return (this.provisionSearchRecordTypes.contains(provisionSearchRecordType));
    }


    /**
     *  Adds support for a provision search record type.
     *
     *  @param provisionSearchRecordType a provision search record type
     *  @throws org.osid.NullArgumentException
     *  <code>provisionSearchRecordType</code> is <code>null</code>
     */

    protected void addProvisionSearchRecordType(org.osid.type.Type provisionSearchRecordType) {
        this.provisionSearchRecordTypes.add(provisionSearchRecordType);
        return;
    }


    /**
     *  Removes support for a provision search record type.
     *
     *  @param provisionSearchRecordType a provision search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>provisionSearchRecordType</code> is <code>null</code>
     */

    protected void removeProvisionSearchRecordType(org.osid.type.Type provisionSearchRecordType) {
        this.provisionSearchRecordTypes.remove(provisionSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProvisionReturn </code> record types. 
     *
     *  @return a list containing the supported <code> ProvisionReturn </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionReturnRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.provisionReturnRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProvisionReturn </code> record type is 
     *  supported. 
     *
     *  @param  provisionReturnRecordType a <code> Type </code> indicating a 
     *          <code> ProvisionReturn </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          provisionReturnRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionReturnRecordType(org.osid.type.Type provisionReturnRecordType) {
        return (this.provisionReturnRecordTypes.contains(provisionReturnRecordType));
    }


    /**
     *  Adds support for a provision return record type.
     *
     *  @param provisionReturnRecordType a provision return record type
     *  @throws org.osid.NullArgumentException
     *  <code>provisionReturnRecordType</code> is <code>null</code>
     */

    protected void addProvisionReturnRecordType(org.osid.type.Type provisionReturnRecordType) {
        this.provisionReturnRecordTypes.add(provisionReturnRecordType);
        return;
    }


    /**
     *  Removes support for a provision return record type.
     *
     *  @param provisionReturnRecordType a provision return record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>provisionReturnRecordType</code> is <code>null</code>
     */

    protected void removeProvisionReturnRecordType(org.osid.type.Type provisionReturnRecordType) {
        this.provisionReturnRecordTypes.remove(provisionReturnRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Queue </code> record types. 
     *
     *  @return a list containing the supported <code> Queue </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Queue </code> record type is supported. 
     *
     *  @param  queueRecordType a <code> Type </code> indicating a <code> 
     *          Queue </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueRecordType(org.osid.type.Type queueRecordType) {
        return (this.queueRecordTypes.contains(queueRecordType));
    }


    /**
     *  Adds support for a queue record type.
     *
     *  @param queueRecordType a queue record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueRecordType</code> is <code>null</code>
     */

    protected void addQueueRecordType(org.osid.type.Type queueRecordType) {
        this.queueRecordTypes.add(queueRecordType);
        return;
    }


    /**
     *  Removes support for a queue record type.
     *
     *  @param queueRecordType a queue record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueRecordType</code> is <code>null</code>
     */

    protected void removeQueueRecordType(org.osid.type.Type queueRecordType) {
        this.queueRecordTypes.remove(queueRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Queue </code> search record types. 
     *
     *  @return a list containing the supported <code> Queue </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Queue </code> search record type is 
     *  supported. 
     *
     *  @param  queueSearchRecordType a <code> Type </code> indicating a 
     *          <code> Queue </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueSearchRecordType(org.osid.type.Type queueSearchRecordType) {
        return (this.queueSearchRecordTypes.contains(queueSearchRecordType));
    }


    /**
     *  Adds support for a queue search record type.
     *
     *  @param queueSearchRecordType a queue search record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueSearchRecordType</code> is <code>null</code>
     */

    protected void addQueueSearchRecordType(org.osid.type.Type queueSearchRecordType) {
        this.queueSearchRecordTypes.add(queueSearchRecordType);
        return;
    }


    /**
     *  Removes support for a queue search record type.
     *
     *  @param queueSearchRecordType a queue search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueSearchRecordType</code> is <code>null</code>
     */

    protected void removeQueueSearchRecordType(org.osid.type.Type queueSearchRecordType) {
        this.queueSearchRecordTypes.remove(queueSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Request </code> record types. 
     *
     *  @return a list containing the supported <code> Request </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequestRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.requestRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Request </code> record type is supported. 
     *
     *  @param  requestRecordType a <code> Type </code> indicating a <code> 
     *          Request </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> requestRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequestRecordType(org.osid.type.Type requestRecordType) {
        return (this.requestRecordTypes.contains(requestRecordType));
    }


    /**
     *  Adds support for a request record type.
     *
     *  @param requestRecordType a request record type
     *  @throws org.osid.NullArgumentException
     *  <code>requestRecordType</code> is <code>null</code>
     */

    protected void addRequestRecordType(org.osid.type.Type requestRecordType) {
        this.requestRecordTypes.add(requestRecordType);
        return;
    }


    /**
     *  Removes support for a request record type.
     *
     *  @param requestRecordType a request record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>requestRecordType</code> is <code>null</code>
     */

    protected void removeRequestRecordType(org.osid.type.Type requestRecordType) {
        this.requestRecordTypes.remove(requestRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Request </code> search record types. 
     *
     *  @return a list containing the supported <code> Request </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequestSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.requestSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Request </code> search record type is 
     *  supported. 
     *
     *  @param  requestSearchRecordType a <code> Type </code> indicating a 
     *          <code> Request </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> requestSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequestSearchRecordType(org.osid.type.Type requestSearchRecordType) {
        return (this.requestSearchRecordTypes.contains(requestSearchRecordType));
    }


    /**
     *  Adds support for a request search record type.
     *
     *  @param requestSearchRecordType a request search record type
     *  @throws org.osid.NullArgumentException
     *  <code>requestSearchRecordType</code> is <code>null</code>
     */

    protected void addRequestSearchRecordType(org.osid.type.Type requestSearchRecordType) {
        this.requestSearchRecordTypes.add(requestSearchRecordType);
        return;
    }


    /**
     *  Removes support for a request search record type.
     *
     *  @param requestSearchRecordType a request search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>requestSearchRecordType</code> is <code>null</code>
     */

    protected void removeRequestSearchRecordType(org.osid.type.Type requestSearchRecordType) {
        this.requestSearchRecordTypes.remove(requestSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RequestTransaction </code> record types. 
     *
     *  @return a list containing the supported <code> RequestTransaction 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequestTransactionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.requestTransactionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RequestTransaction </code> record type is 
     *  supported. 
     *
     *  @param  requestTransactionRecordType a <code> Type </code> indicating 
     *          a <code> RequestTransaction </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          requestTransactionRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequestTransactionRecordType(org.osid.type.Type requestTransactionRecordType) {
        return (this.requestTransactionRecordTypes.contains(requestTransactionRecordType));
    }


    /**
     *  Adds support for a request transaction record type.
     *
     *  @param requestTransactionRecordType a request transaction record type
     *  @throws org.osid.NullArgumentException
     *  <code>requestTransactionRecordType</code> is <code>null</code>
     */

    protected void addRequestTransactionRecordType(org.osid.type.Type requestTransactionRecordType) {
        this.requestTransactionRecordTypes.add(requestTransactionRecordType);
        return;
    }


    /**
     *  Removes support for a request transaction record type.
     *
     *  @param requestTransactionRecordType a request transaction record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>requestTransactionRecordType</code> is <code>null</code>
     */

    protected void removeRequestTransactionRecordType(org.osid.type.Type requestTransactionRecordType) {
        this.requestTransactionRecordTypes.remove(requestTransactionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Pool </code> record types. 
     *
     *  @return a list containing the supported <code> Pool </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.poolRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Pool </code> record type is supported. 
     *
     *  @param  poolRecordType a <code> Type </code> indicating a <code> Pool 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> poolRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPoolRecordType(org.osid.type.Type poolRecordType) {
        return (this.poolRecordTypes.contains(poolRecordType));
    }


    /**
     *  Adds support for a pool record type.
     *
     *  @param poolRecordType a pool record type
     *  @throws org.osid.NullArgumentException
     *  <code>poolRecordType</code> is <code>null</code>
     */

    protected void addPoolRecordType(org.osid.type.Type poolRecordType) {
        this.poolRecordTypes.add(poolRecordType);
        return;
    }


    /**
     *  Removes support for a pool record type.
     *
     *  @param poolRecordType a pool record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>poolRecordType</code> is <code>null</code>
     */

    protected void removePoolRecordType(org.osid.type.Type poolRecordType) {
        this.poolRecordTypes.remove(poolRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Pool </code> search types. 
     *
     *  @return a list containing the supported <code> Pool </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.poolSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Pool </code> search type is supported. 
     *
     *  @param  poolSearchRecordType a <code> Type </code> indicating a <code> 
     *          Pool </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> poolSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPoolSearchRecordType(org.osid.type.Type poolSearchRecordType) {
        return (this.poolSearchRecordTypes.contains(poolSearchRecordType));
    }


    /**
     *  Adds support for a pool search record type.
     *
     *  @param poolSearchRecordType a pool search record type
     *  @throws org.osid.NullArgumentException
     *  <code>poolSearchRecordType</code> is <code>null</code>
     */

    protected void addPoolSearchRecordType(org.osid.type.Type poolSearchRecordType) {
        this.poolSearchRecordTypes.add(poolSearchRecordType);
        return;
    }


    /**
     *  Removes support for a pool search record type.
     *
     *  @param poolSearchRecordType a pool search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>poolSearchRecordType</code> is <code>null</code>
     */

    protected void removePoolSearchRecordType(org.osid.type.Type poolSearchRecordType) {
        this.poolSearchRecordTypes.remove(poolSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Provisionable </code> record types. 
     *
     *  @return a list containing the supported <code> Provisionable </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionableRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.provisionableRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Provisionable </code> record type is 
     *  supported. 
     *
     *  @param  provisionableRecordType a <code> Type </code> indicating a 
     *          <code> Provisionable </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> provisionableRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionableRecordType(org.osid.type.Type provisionableRecordType) {
        return (this.provisionableRecordTypes.contains(provisionableRecordType));
    }


    /**
     *  Adds support for a provisionable record type.
     *
     *  @param provisionableRecordType a provisionable record type
     *  @throws org.osid.NullArgumentException
     *  <code>provisionableRecordType</code> is <code>null</code>
     */

    protected void addProvisionableRecordType(org.osid.type.Type provisionableRecordType) {
        this.provisionableRecordTypes.add(provisionableRecordType);
        return;
    }


    /**
     *  Removes support for a provisionable record type.
     *
     *  @param provisionableRecordType a provisionable record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>provisionableRecordType</code> is <code>null</code>
     */

    protected void removeProvisionableRecordType(org.osid.type.Type provisionableRecordType) {
        this.provisionableRecordTypes.remove(provisionableRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Provisionable </code> search types. 
     *
     *  @return a list containing the supported <code> Provisionable </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionableSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.provisionableSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Provisionable </code> search type is 
     *  supported. 
     *
     *  @param  provisionableSearchRecordType a <code> Type </code> indicating 
     *          a <code> Provisionable </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          provisionableSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionableSearchRecordType(org.osid.type.Type provisionableSearchRecordType) {
        return (this.provisionableSearchRecordTypes.contains(provisionableSearchRecordType));
    }


    /**
     *  Adds support for a provisionable search record type.
     *
     *  @param provisionableSearchRecordType a provisionable search record type
     *  @throws org.osid.NullArgumentException
     *  <code>provisionableSearchRecordType</code> is <code>null</code>
     */

    protected void addProvisionableSearchRecordType(org.osid.type.Type provisionableSearchRecordType) {
        this.provisionableSearchRecordTypes.add(provisionableSearchRecordType);
        return;
    }


    /**
     *  Removes support for a provisionable search record type.
     *
     *  @param provisionableSearchRecordType a provisionable search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>provisionableSearchRecordType</code> is <code>null</code>
     */

    protected void removeProvisionableSearchRecordType(org.osid.type.Type provisionableSearchRecordType) {
        this.provisionableSearchRecordTypes.remove(provisionableSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Broker </code> record types. 
     *
     *  @return a list containing the supported <code> Broker </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.brokerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Broker </code> record type is supported. 
     *
     *  @param  brokerRecordType a <code> Type </code> indicating a <code> 
     *          Broker </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> brokerRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBrokerRecordType(org.osid.type.Type brokerRecordType) {
        return (this.brokerRecordTypes.contains(brokerRecordType));
    }


    /**
     *  Adds support for a broker record type.
     *
     *  @param brokerRecordType a broker record type
     *  @throws org.osid.NullArgumentException
     *  <code>brokerRecordType</code> is <code>null</code>
     */

    protected void addBrokerRecordType(org.osid.type.Type brokerRecordType) {
        this.brokerRecordTypes.add(brokerRecordType);
        return;
    }


    /**
     *  Removes support for a broker record type.
     *
     *  @param brokerRecordType a broker record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>brokerRecordType</code> is <code>null</code>
     */

    protected void removeBrokerRecordType(org.osid.type.Type brokerRecordType) {
        this.brokerRecordTypes.remove(brokerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Broker </code> search record types. 
     *
     *  @return a list containing the supported <code> Broker </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.brokerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Broker </code> search record type is 
     *  supported. 
     *
     *  @param  brokerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Broker </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> brokerSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBrokerSearchRecordType(org.osid.type.Type brokerSearchRecordType) {
        return (this.brokerSearchRecordTypes.contains(brokerSearchRecordType));
    }


    /**
     *  Adds support for a broker search record type.
     *
     *  @param brokerSearchRecordType a broker search record type
     *  @throws org.osid.NullArgumentException
     *  <code>brokerSearchRecordType</code> is <code>null</code>
     */

    protected void addBrokerSearchRecordType(org.osid.type.Type brokerSearchRecordType) {
        this.brokerSearchRecordTypes.add(brokerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a broker search record type.
     *
     *  @param brokerSearchRecordType a broker search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>brokerSearchRecordType</code> is <code>null</code>
     */

    protected void removeBrokerSearchRecordType(org.osid.type.Type brokerSearchRecordType) {
        this.brokerSearchRecordTypes.remove(brokerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Distributor </code> record types. 
     *
     *  @return a list containing the supported <code> Distributor </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDistributorRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.distributorRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Distributor </code> record type is 
     *  supported. 
     *
     *  @param  distributorRecordType a <code> Type </code> indicating a 
     *          <code> Distributor </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> distributorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDistributorRecordType(org.osid.type.Type distributorRecordType) {
        return (this.distributorRecordTypes.contains(distributorRecordType));
    }


    /**
     *  Adds support for a distributor record type.
     *
     *  @param distributorRecordType a distributor record type
     *  @throws org.osid.NullArgumentException
     *  <code>distributorRecordType</code> is <code>null</code>
     */

    protected void addDistributorRecordType(org.osid.type.Type distributorRecordType) {
        this.distributorRecordTypes.add(distributorRecordType);
        return;
    }


    /**
     *  Removes support for a distributor record type.
     *
     *  @param distributorRecordType a distributor record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>distributorRecordType</code> is <code>null</code>
     */

    protected void removeDistributorRecordType(org.osid.type.Type distributorRecordType) {
        this.distributorRecordTypes.remove(distributorRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Distributor </code> search record types. 
     *
     *  @return a list containing the supported <code> Distributor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDistributorSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.distributorSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Distributor </code> search record type is 
     *  supported. 
     *
     *  @param  distributorSearchRecordType a <code> Type </code> indicating a 
     *          <code> Distributor </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          distributorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDistributorSearchRecordType(org.osid.type.Type distributorSearchRecordType) {
        return (this.distributorSearchRecordTypes.contains(distributorSearchRecordType));
    }


    /**
     *  Adds support for a distributor search record type.
     *
     *  @param distributorSearchRecordType a distributor search record type
     *  @throws org.osid.NullArgumentException
     *  <code>distributorSearchRecordType</code> is <code>null</code>
     */

    protected void addDistributorSearchRecordType(org.osid.type.Type distributorSearchRecordType) {
        this.distributorSearchRecordTypes.add(distributorSearchRecordType);
        return;
    }


    /**
     *  Removes support for a distributor search record type.
     *
     *  @param distributorSearchRecordType a distributor search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>distributorSearchRecordType</code> is <code>null</code>
     */

    protected void removeDistributorSearchRecordType(org.osid.type.Type distributorSearchRecordType) {
        this.distributorSearchRecordTypes.remove(distributorSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my provision 
     *  service. 
     *
     *  @return a <code> MyProvisionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyProvision() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionSession getMyProvisionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getMyProvisionSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my provision 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyProvisionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyProvision() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionSession getMyProvisionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getMyProvisionSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my provision 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distrivutor 
     *  @return a <code> MyProvisionSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distriobutor </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyProvision() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionSession getMyProvisionSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getMyProvisionSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my provision 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MyProvisionSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyProvision() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionSession getMyProvisionSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getMyProvisionSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my supplier 
     *  service. 
     *
     *  @return a <code> MySupplierSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMySupplier() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MySupplierSession getMySupplierSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getMySupplierSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my supplier 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MySupplierSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMySupplier() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MySupplierSession getMySupplierSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getMySupplierSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my Supplier 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distrivutor 
     *  @return a <code> MySupplierSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distriobutor </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMySupplier() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MySupplierSession getMySupplierSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getMySupplierSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my Supplier 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MySupplierSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMySupplier() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MySupplierSession getMySupplierSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getMySupplierSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service for resources related to the authentciated agent. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @return a <code> MyProvisionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyProvisionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionNotificationSession getMyProvisionNotificationSession(org.osid.provisioning.ProvisionReceiver provisionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getMyProvisionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my provision 
     *  notification service for resources related to the authenticated agent. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> MyProvisionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyProvisionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionNotificationSession getMyProvisionNotificationSession(org.osid.provisioning.ProvisionReceiver provisionReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getMyProvisionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service for the given distributor for resources related 
     *  to the authentciated agent. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> MyProvisionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyProvisionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionNotificationSession getMyProvisionNotificationSessionForDistributor(org.osid.provisioning.ProvisionReceiver provisionReceiver, 
                                                                                                                org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getMyProvisionNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my provision 
     *  notification service for the given distributor for resources related 
     *  to the authenticated agent. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MyProvisionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyProvisionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionNotificationSession getMyProvisionNotificationSessionForDistributor(org.osid.provisioning.ProvisionReceiver provisionReceiver, 
                                                                                                                org.osid.id.Id distributorId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getMyProvisionNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  lookup service. 
     *
     *  @return a <code> ProvisionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionLookupSession getProvisionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionLookupSession getProvisionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionLookupSession getProvisionLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionLookupSession getProvisionLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  query service. 
     *
     *  @return a <code> ProvisionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuerySession getProvisionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuerySession getProvisionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuerySession getProvisionQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuerySession getProvisionQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  search service. 
     *
     *  @return a <code> ProvisionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchSession getProvisionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchSession getProvisionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  search service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchSession getProvisionSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  search service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchSession getProvisionSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  administration service. 
     *
     *  @return a <code> ProvisionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionAdminSession getProvisionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionAdminSession getProvisionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionAdminSession getProvisionAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionAdminSession getProvisionAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @return a <code> ProvisionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionNotificationSession getProvisionNotificationSession(org.osid.provisioning.ProvisionReceiver provisionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionNotificationSession getProvisionNotificationSession(org.osid.provisioning.ProvisionReceiver provisionReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service for the given distributor. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionNotificationSession getProvisionNotificationSessionForDistributor(org.osid.provisioning.ProvisionReceiver provisionReceiver, 
                                                                                                            org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service for the given distributor. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionNotificationSession getProvisionNotificationSessionForDistributor(org.osid.provisioning.ProvisionReceiver provisionReceiver, 
                                                                                                            org.osid.id.Id distributorId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup provision/distributor 
     *  mappings. 
     *
     *  @return a <code> ProvisionDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionDistributorSession getProvisionDistributorSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup provision/distributor 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionDistributorSession getProvisionDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  provisions to distributors. 
     *
     *  @return a <code> ProvisionDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionDistributorAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionDistributorAssignmentSession getProvisionDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  provisions to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionDistributorAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionDistributorAssignmentSession getProvisionDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage provision smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionDistributorSession getProvisionSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage provision smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionDistributorSession getProvisionSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  return service. 
     *
     *  @return a <code> ProvisionReturnSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionReturn() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnSession getProvisionReturnSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionReturnSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  return service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionReturnSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionReturn() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnSession getProvisionReturnSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionReturnSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  return service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionReturnSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionReturn() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnSession getProvisionReturnSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionReturnSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  return service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionReturnSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> s <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionReturn() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnSession getProvisionReturnSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionReturnSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service. 
     *
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueLookupSession getQueueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueLookupSession getQueueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueLookupSession getQueueLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueLookupSession getQueueLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service. 
     *
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuerySession getQueueQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuerySession getQueueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuerySession getQueueQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuerySession getQueueQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service. 
     *
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSearchSession getQueueSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSearchSession getQueueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSearchSession getQueueSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSearchSession getQueueSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service. 
     *
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueAdminSession getQueueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueAdminSession getQueueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueAdminSession getQueueAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @param  proxy a proxy 
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueAdminSession getQueueAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service. 
     *
     *  @param  queueReceiver the notification callback 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueNotificationSession getQueueNotificationSession(org.osid.provisioning.QueueReceiver queueReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service. 
     *
     *  @param  queueReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueNotificationSession getQueueNotificationSession(org.osid.provisioning.QueueReceiver queueReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service for the given distributor. 
     *
     *  @param  queueReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver </code> or 
     *          <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueNotificationSession getQueueNotificationSessionForDistributor(org.osid.provisioning.QueueReceiver queueReceiver, 
                                                                                                    org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service for the given distributor. 
     *
     *  @param  queueReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueNotificationSession getQueueNotificationSessionForDistributor(org.osid.provisioning.QueueReceiver queueReceiver, 
                                                                                                    org.osid.id.Id distributorId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue/distributor 
     *  mappings. 
     *
     *  @return a <code> QueueDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueDistributorSession getQueueDistributorSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue/distributor 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueDistributorSession getQueueDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queues 
     *  to distributors. 
     *
     *  @return a <code> QueueDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueDistributorAssignmentSession getQueueDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queues 
     *  to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueDistributorAssignmentSession getQueueDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @return a <code> QueueSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSmartDistributorSession getQueueSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getQueueSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @param  proxy a proxy 
     *  @return a <code> QueueSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSmartDistributorSession getQueueSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getQueueSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request lookup 
     *  service. 
     *
     *  @return a <code> RequestLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getRequestLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getRequestLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getRequestLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getRequestLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request query 
     *  service. 
     *
     *  @return a <code> RequestQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getRequestQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getRequestQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> CRequestQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getRequestQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getRequestQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request search 
     *  service. 
     *
     *  @return a <code> RequestSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getRequestSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getRequestSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getRequestSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getRequestSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  administrative service. 
     *
     *  @return a <code> RequestAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getRequestAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getRequestAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  administrative service for the given queue. 
     *
     *  @param  queueId the <code> Id </code> of the <code> Queue </code> 
     *  @return a <code> RequestAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Queue </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getRequestAdminSessionForQueue(org.osid.id.Id queueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestAdminSessionForQueue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  administrative service for the given queue. 
     *
     *  @param  queueId the <code> Id </code> of the <code> Queue </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Queue </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getRequestAdminSessionForQueue(org.osid.id.Id queueId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestAdminSessionForQueue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  notification service. 
     *
     *  @param  requestReceiver the notification callback 
     *  @return a <code> RequestNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> requestReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getRequestNotificationSession(org.osid.provisioning.RequestReceiver requestReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  notification service. 
     *
     *  @param  requestReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RequestNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> requestReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getRequestNotificationSession(org.osid.provisioning.RequestReceiver requestReceiver, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  notification service for the given distributor. 
     *
     *  @param  requestReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> requestReceiver </code> 
     *          or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getRequestNotificationSessionForDistributor(org.osid.provisioning.RequestReceiver requestReceiver, 
                                                                                                        org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  notification service for the given distributor. 
     *
     *  @param  requestReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> requestReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getRequestNotificationSessionForDistributor(org.osid.provisioning.RequestReceiver requestReceiver, 
                                                                                                        org.osid.id.Id distributorId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup request/distributor 
     *  mappings. 
     *
     *  @return a <code> RequestDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorSession getRequestDistributorSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup request/distributor 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorSession getRequestDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queues 
     *  to distributors. 
     *
     *  @return a <code> RequestyDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorAssignmentSession getRequestDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queues 
     *  to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestyDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorAssignmentSession getRequestDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage request smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSmartDistributorSession getRequestSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage request smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSmartDistributorSession getRequestSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction lookup service. 
     *
     *  @return a <code> RequestTransactionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionLookupSession getRequestTransactionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestTransactionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestTransactionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionLookupSession getRequestTransactionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestTransactionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestTransactionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionLookupSession getRequestTransactionLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestTransactionLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RequestTransactionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionLookupSession getRequestTransactionLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestTransactionLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction administrative service. 
     *
     *  @return a <code> RequestTransactionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionAdminSession getRequestTransactionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestTransactionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RequestTransactionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionAdminSession getRequestTransactionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestTransactionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction administrative service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestTransactionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionAdminSession getRequestTransactionAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getRequestTransactionAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction administrative service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @param  proxy a proxy 
     *  @return a <code> RequestTransactionAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionAdminSession getRequestTransactionAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getRequestTransactionAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the exchange 
     *  service. 
     *
     *  @return an <code> ExchangeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsExchange() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ExchangeSession getExchangeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getExchangeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the exchange 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ExchangeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsExchange() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ExchangeSession getExchangeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getExchangeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the exchange 
     *  service for the given queue. 
     *
     *  @param  queueId the <code> Id </code> of the <code> Queue </code> 
     *  @return an <code> ExchangeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Queue </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsExchange() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ExchangeSession getExchangeSessionForQueue(org.osid.id.Id queueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getExchangeSessionForQueue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the exchange 
     *  service for the given queue. 
     *
     *  @param  queueId the <code> Id </code> of the <code> Queue </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ExchangeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Queue </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsExchange() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ExchangeSession getExchangeSessionForQueue(org.osid.id.Id queueId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getExchangeSessionForQueue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool lookup 
     *  service. 
     *
     *  @return a <code> PoolLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolLookupSession getPoolLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolLookupSession getPoolLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolLookupSession getPoolLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolLookupSession getPoolLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool query 
     *  service. 
     *
     *  @return a <code> PoolQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuerySession getPoolQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuerySession getPoolQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuerySession getPoolQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuerySession getPoolQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool search 
     *  service. 
     *
     *  @return a <code> PoolSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchSession getPoolSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchSession getPoolSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchSession getPoolSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchSession getPoolSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  administration service. 
     *
     *  @return a <code> PoolAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolAdminSession getPoolAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolAdminSession getPoolAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolAdminSession getPoolAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @param  proxy a proxy 
     *  @return a <code> PoolAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolAdminSession getPoolAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  notification service. 
     *
     *  @param  poolReceiver the notification callback 
     *  @return a <code> PoolNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> poolReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolNotificationSession getPoolNotificationSession(org.osid.provisioning.PoolReceiver poolReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  notification service. 
     *
     *  @param  poolReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PoolNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> poolReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolNotificationSession getPoolNotificationSession(org.osid.provisioning.PoolReceiver poolReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  notification service for the given distributor. 
     *
     *  @param  poolReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> poolReceiver </code> or 
     *          <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolNotificationSession getPoolNotificationSessionForDistributor(org.osid.provisioning.PoolReceiver poolReceiver, 
                                                                                                  org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  notification service for the given distributor. 
     *
     *  @param  poolReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> poolReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolNotificationSession getPoolNotificationSessionForDistributor(org.osid.provisioning.PoolReceiver poolReceiver, 
                                                                                                  org.osid.id.Id distributorId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup pool/distributor 
     *  mappings. 
     *
     *  @return a <code> PoolDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolDistributorSession getPoolDistributorSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup pool/distributor 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolDistributorSession getPoolDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning pools to 
     *  distributors. 
     *
     *  @return a <code> PoolDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolDistributorAssignmentSession getPoolDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning pools to 
     *  distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolDistributorAssignmentSession getPoolDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage pool smart distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolSmartDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSmartDistributorSession getPoolSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getPoolSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage pool smart distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolSmartDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolDistributorSession getPoolSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getPoolSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  lookup service. 
     *
     *  @return a <code> ProvisionableLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getProvisionableLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getProvisionableLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @return a <code> ProvisionableLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getProvisionableLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getProvisionableLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  query service. 
     *
     *  @return a <code> ProvisionableQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getProvisionableQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getProvisionableQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionableQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getProvisionableQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getProvisionableQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  search service. 
     *
     *  @return a <code> ProvisionableSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getProvisionableSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getProvisionableSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  search service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionableSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getProvisionableSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  search service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getProvisionableSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  administration service. 
     *
     *  @return a <code> ProvisionableAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getProvisionableAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getProvisionableAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Dostributor 
     *          </code> 
     *  @return a <code> ProvisionableAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getProvisionableAdminSessionForPool(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableAdminSessionForPool not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  administration service for the given pool. 
     *
     *  @param  poolId the <code> Id </code> of the <code> Pool </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableAdminSession </code> 
     *  @throws org.osid.NotFoundException no pool found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> poolId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getProvisionableAdminSessionForPool(org.osid.id.Id poolId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableAdminSessionForPool not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  notification service. 
     *
     *  @param  provisionableReceiver the notification callback 
     *  @return a <code> ProvisionableNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> provisionableReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getProvisionableNotificationSession(org.osid.provisioning.RequestReceiver provisionableReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  notification service. 
     *
     *  @param  provisionableReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> provisionableReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getProvisionableNotificationSession(org.osid.provisioning.RequestReceiver provisionableReceiver, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  notification service for the given distributor. 
     *
     *  @param  provisionableReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionableNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> provisionableReceiver 
     *          </code> or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getProvisionableNotificationSessionForDistributor(org.osid.provisioning.RequestReceiver provisionableReceiver, 
                                                                                                              org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  notification service for the given distributor. 
     *
     *  @param  provisionableReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> provisionableReceiver, 
     *          </code> <code> distributorId </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getProvisionableNotificationSessionForDistributor(org.osid.provisioning.RequestReceiver provisionableReceiver, 
                                                                                                              org.osid.id.Id distributorId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup 
     *  provisionable/distributor mappings. 
     *
     *  @return a <code> ProvisionableDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorSession getProvisionableDistributorSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup 
     *  provisionable/distributor mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorSession getProvisionableDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  provisionables to distributors. 
     *
     *  @return a <code> ProvisionableDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableDistributorAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorAssignmentSession getProvisionableDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  provisionables to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableDistributorAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorAssignmentSession getProvisionableDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionableSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableSmartDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSmartDistributorSession getProvisionableSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisionableSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProvisionableSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableSmartDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSmartDistributorSession getProvisionableSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisionableSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker lookup 
     *  service. 
     *
     *  @return a <code> BrokerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerLookupSession getBrokerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerLookupSession getBrokerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerLookupSession getBrokerLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerLookupSession getBrokerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerLookupSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker query 
     *  service. 
     *
     *  @return a <code> BrokerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuerySession getBrokerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuerySession getBrokerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuerySession getBrokerQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuerySession getBrokerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerQuerySessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker search 
     *  service. 
     *
     *  @return a <code> BrokerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchSession getBrokerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchSession getBrokerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchSession getBrokerSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchSession getBrokerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerSearchSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  administration service. 
     *
     *  @return a <code> BrokerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerAdminSession getBrokerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerAdminSession getBrokerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerAdminSession getBrokerAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerAdminSession getBrokerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerAdminSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  notification service. 
     *
     *  @param  brokerReceiver the notification callback 
     *  @return a <code> BrokerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> brokerReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerNotificationSession getBrokerNotificationSession(org.osid.provisioning.BrokerReceiver brokerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  notification service. 
     *
     *  @param  brokerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> brokerReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerNotificationSession getBrokerNotificationSession(org.osid.provisioning.BrokerReceiver brokerReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  notification service for the given distributor. 
     *
     *  @param  brokerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> brokerReceiver </code> 
     *          or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerNotificationSession getBrokerNotificationSessionForDistributor(org.osid.provisioning.BrokerReceiver brokerReceiver, 
                                                                                                      org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  notification service for the given distributor. 
     *
     *  @param  brokerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> brokerReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerNotificationSession getBrokerNotificationSessionForDistributor(org.osid.provisioning.BrokerReceiver brokerReceiver, 
                                                                                                      org.osid.id.Id distributorId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerNotificationSessionForDistributor not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup broker/distributor 
     *  mappings. 
     *
     *  @return a <code> BrokerDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerDistributorSession getBrokerDistributorSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup broker/distributor 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerDistributorSession getBrokerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning brokers 
     *  to distributors. 
     *
     *  @return a <code> BrokerDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerDistributorAssignmentSession getBrokerDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning brokers 
     *  to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerDistributorAssignmentSession getBrokerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerDistributorAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage broker smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSmartDistributorSession getBrokerSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getBrokerSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage broker smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSmartDistributorSession getBrokerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getBrokerSmartDistributorSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  lookup service. 
     *
     *  @return a <code> DistributorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorLookupSession getDistributorLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getDistributorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DistributorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorLookupSession getDistributorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getDistributorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  query service. 
     *
     *  @return a <code> DistributorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuerySession getDistributorQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getDistributorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DistributorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuerySession getDistributorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getDistributorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  search service. 
     *
     *  @return a <code> DistributorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorSearchSession getDistributorSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getDistributorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DistributorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorSearchSession getDistributorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getDistributorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  administrative service. 
     *
     *  @return a <code> DistributorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorAdminSession getDistributorAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getDistributorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DistributorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorAdminSession getDistributorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getDistributorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  notification service. 
     *
     *  @param  distributorReceiver the notification callback 
     *  @return a <code> DistributorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> distributorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorNotificationSession getDistributorNotificationSession(org.osid.provisioning.DistributorReceiver distributorReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getDistributorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  notification service. 
     *
     *  @param  distributorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DistributorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> distributorReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorNotificationSession getDistributorNotificationSession(org.osid.provisioning.DistributorReceiver distributorReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getDistributorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  hierarchy service. 
     *
     *  @return a <code> DistributorHierarchySession </code> for distributors 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorHierarchySession getDistributorHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getDistributorHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DistributorHierarchySession </code> for distributors 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorHierarchySession getDistributorHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getDistributorHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for distributors 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorHierarchyDesignSession getDistributorHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getDistributorHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for distributors 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorHierarchyDesignSession getDistributorHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getDistributorHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> ProvisioningBatchManager. </code> 
     *
     *  @return a <code> ProvisioningBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisioningBatchManager getProvisioningBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisioningBatchManager not implemented");
    }


    /**
     *  Gets the <code> ProvisioningBatchProxyManager. </code> 
     *
     *  @return a <code> ProvisioningBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisioningBatchProxyManager getProvisioningBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisioningBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> ProvisioningRulesManager. </code> 
     *
     *  @return a <code> ProvisioningRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.ProvisioningRulesManager getProvisioningRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningManager.getProvisioningRulesManager not implemented");
    }


    /**
     *  Gets the <code> ProvisioningRulesPorxyManager. </code> 
     *
     *  @return a <code> ProvisioningRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.ProvisioningRulesProxyManager getProvisioningRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.provisioning.ProvisioningProxyManager.getProvisioningRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.provisionRecordTypes.clear();
        this.provisionRecordTypes.clear();

        this.provisionSearchRecordTypes.clear();
        this.provisionSearchRecordTypes.clear();

        this.provisionReturnRecordTypes.clear();
        this.provisionReturnRecordTypes.clear();

        this.queueRecordTypes.clear();
        this.queueRecordTypes.clear();

        this.queueSearchRecordTypes.clear();
        this.queueSearchRecordTypes.clear();

        this.requestRecordTypes.clear();
        this.requestRecordTypes.clear();

        this.requestSearchRecordTypes.clear();
        this.requestSearchRecordTypes.clear();

        this.requestTransactionRecordTypes.clear();
        this.requestTransactionRecordTypes.clear();

        this.poolRecordTypes.clear();
        this.poolRecordTypes.clear();

        this.poolSearchRecordTypes.clear();
        this.poolSearchRecordTypes.clear();

        this.provisionableRecordTypes.clear();
        this.provisionableRecordTypes.clear();

        this.provisionableSearchRecordTypes.clear();
        this.provisionableSearchRecordTypes.clear();

        this.brokerRecordTypes.clear();
        this.brokerRecordTypes.clear();

        this.brokerSearchRecordTypes.clear();
        this.brokerSearchRecordTypes.clear();

        this.distributorRecordTypes.clear();
        this.distributorRecordTypes.clear();

        this.distributorSearchRecordTypes.clear();
        this.distributorSearchRecordTypes.clear();

        return;
    }
}
