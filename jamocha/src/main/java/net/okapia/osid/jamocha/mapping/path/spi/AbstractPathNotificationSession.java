//
// AbstractPathNotificationSession.java
//
//     A template for making PathNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Path} objects. This session is intended for
 *  consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Path} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for path entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractPathNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.mapping.path.PathNotificationSession {

    private boolean federated = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();


    /**
     *  Gets the {@code Map/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Map Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }

    
    /**
     *  Gets the {@code Map} associated with this session.
     *
     *  @return the {@code Map} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the {@code Map}.
     *
     *  @param map the map for this session
     *  @throws org.osid.NullArgumentException {@code map}
     *          is {@code null}
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can register for {@code Path}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForPathNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include paths in maps which are children of this
     *  map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@code true} if federated view, {@code false}
     *          otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new paths. {@code
     *  PathReceiver.newPath()} is invoked when a new {@code Path} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new paths through the given
     *  location.  {@code PathReceiver.newPath()} is invoked when a
     *  new {@code Path} appears in this map.
     *
     *  @param locationId the {@code Id} of the {@code Location} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code locationId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewPathsThroughLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }



    /**
     *  Registers for notification of updated paths. {@code
     *  PathReceiver.changedPath()} is invoked when a path is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated paths through the given
     *  location. {@code PathReceiver.changedPath()} is invoked when a
     *  path is changed in this map.
     *
     *  @param locationId the {@code Id} of the {@code Location} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code locationId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedPathsThroughLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated path. {@code
     *  PathReceiver.changedPath()} is invoked when the specified path
     *  is changed.
     *
     *  @param pathId the {@code Id} of the {@code Path} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code pathId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted paths. {@code
     *  PathReceiver.deletedPath()} is invoked when a path is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }

    
    /**
     *  Register for notifications of deleted paths through the given
     *  location. {@code PathReceiver.deletedPath()} is invoked when a
     *  path is removed from this map.
     *
     *  @param locationId the {@code Id} of the {@code Location} to
     *          monitor
     *  @throws org.osid.NullArgumentException {@code locationId} is
     *          {@code null}
     *          @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedPathsThroughLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted path. {@code
     *  PathReceiver.deletedPath()} is invoked when the specified path
     *  is deleted.
     *
     *  @param pathId the {@code Id} of the
     *          {@code Path} to monitor
     *  @throws org.osid.NullArgumentException {@code pathId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedPath(org.osid.id.Id pathId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
