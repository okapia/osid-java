//
// AbstractPlanSearch.java
//
//     A template for making a Plan Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.plan.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing plan searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPlanSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.course.plan.PlanSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.course.plan.records.PlanSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.course.plan.PlanSearchOrder planSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of plans. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  planIds list of plans
     *  @throws org.osid.NullArgumentException
     *          <code>planIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPlans(org.osid.id.IdList planIds) {
        while (planIds.hasNext()) {
            try {
                this.ids.add(planIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPlans</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of plan Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPlanIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  planSearchOrder plan search order 
     *  @throws org.osid.NullArgumentException
     *          <code>planSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>planSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPlanResults(org.osid.course.plan.PlanSearchOrder planSearchOrder) {
	this.planSearchOrder = planSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.course.plan.PlanSearchOrder getPlanSearchOrder() {
	return (this.planSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given plan search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a plan implementing the requested record.
     *
     *  @param planSearchRecordType a plan search record
     *         type
     *  @return the plan search record
     *  @throws org.osid.NullArgumentException
     *          <code>planSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(planSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.PlanSearchRecord getPlanSearchRecord(org.osid.type.Type planSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.course.plan.records.PlanSearchRecord record : this.records) {
            if (record.implementsRecordType(planSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(planSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this plan search. 
     *
     *  @param planSearchRecord plan search record
     *  @param planSearchRecordType plan search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPlanSearchRecord(org.osid.course.plan.records.PlanSearchRecord planSearchRecord, 
                                           org.osid.type.Type planSearchRecordType) {

        addRecordType(planSearchRecordType);
        this.records.add(planSearchRecord);        
        return;
    }
}
