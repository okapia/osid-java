//
// AbstractImmutableMessage.java
//
//     Wraps a mutable Message to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.messaging.message.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Message</code> to hide modifiers. This
 *  wrapper provides an immutized Message from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying message whose state changes are visible.
 */

public abstract class AbstractImmutableMessage
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.messaging.Message {

    private final org.osid.messaging.Message message;


    /**
     *  Constructs a new <code>AbstractImmutableMessage</code>.
     *
     *  @param message the message to immutablize
     *  @throws org.osid.NullArgumentException <code>message</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableMessage(org.osid.messaging.Message message) {
        super(message);
        this.message = message;
        return;
    }


    /**
     *  Gets the subject line of this message. 
     *
     *  @return the subject 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getSubjectLine() {
        return (this.message.getSubjectLine());
    }


    /**
     *  Gets the text of the message. 
     *
     *  @return the text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getText() {
        return (this.message.getText());
    }


    /**
     *  Tests if this message has been sent. 
     *
     *  @return <code> true </code> if this message has been sent, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isSent() {
        return (this.message.isSent());
    }


    /**
     *  Gets the time this message was sent. 
     *
     *  @return the time 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getSentTime() {
        return (this.message.getSentTime());
    }


    /**
     *  Gets the sender <code> Id </code> of this message. 
     *
     *  @return the sender agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSenderId() {
        return (this.message.getSenderId());
    }


    /**
     *  Gets the sender of this message. 
     *
     *  @return the sender 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSender()
        throws org.osid.OperationFailedException {

        return (this.message.getSender());
    }


    /**
     *  Gets the sending agent <code> Id </code> of this message. 
     *
     *  @return the sending agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSendingAgentId() {
        return (this.message.getSendingAgentId());
    }


    /**
     *  Gets the sending agent of this message. 
     *
     *  @return the sending agent 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getSendingAgent()
        throws org.osid.OperationFailedException {

        return (this.message.getSendingAgent());
    }


    /**
     *  Gets the time this message was received. 
     *
     *  @return the time 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getReceivedTime() {
        return (this.message.getReceivedTime());
    }


    /**
     *  Gets the list of all addressed recipient <code> Ids </code> of this 
     *  message. 
     *
     *  @return the recipient resource <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getRecipientIds() {
        return (this.message.getRecipientIds());
    }


    /**
     *  Gets the list of all addressed recipients of this message. 
     *
     *  @return the recpient resources 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getRecipients()
        throws org.osid.OperationFailedException {

        return (this.message.getRecipients());
    }


    /**
     *  Tests if the resource related to the authenticated agent is one of the 
     *  recipients of this message. 
     *
     *  @return <code> true </code> if this agent is a recipient, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> isSent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean isRecipient() {
        return (this.message.isRecipient());
    }


    /**
     *  Gets the receipt <code> Id </code> for this message. A receipt is 
     *  available for the receiver of this message. 
     *
     *  @return the receipt <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isRecipient() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReceiptId() {
        return (this.message.getReceiptId());
    }


    /**
     *  Gets the receipt for this message. A receipt is available for the 
     *  receiver of this message. 
     *
     *  @return the receipt 
     *  @throws org.osid.IllegalStateException <code> isRecipient() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.messaging.Receipt getReceipt()
        throws org.osid.OperationFailedException {

        return (this.message.getReceipt());
    }


    /**
     *  Gets the message record corresponding to the given <code> Message 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> messageRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(messageRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  messageRecordType the message record type 
     *  @return the message record 
     *  @throws org.osid.NullArgumentException <code> messageRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(messageRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.records.MessageRecord getMessageRecord(org.osid.type.Type messageRecordType)
        throws org.osid.OperationFailedException {

        return (this.message.getMessageRecord(messageRecordType));
    }
}

