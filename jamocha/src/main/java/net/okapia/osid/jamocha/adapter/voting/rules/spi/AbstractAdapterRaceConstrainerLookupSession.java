//
// AbstractAdapterRaceConstrainerLookupSession.java
//
//    A RaceConstrainer lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A RaceConstrainer lookup session adapter.
 */

public abstract class AbstractAdapterRaceConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.rules.RaceConstrainerLookupSession {

    private final org.osid.voting.rules.RaceConstrainerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRaceConstrainerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRaceConstrainerLookupSession(org.osid.voting.rules.RaceConstrainerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Polls/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@code RaceConstrainer} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRaceConstrainers() {
        return (this.session.canLookupRaceConstrainers());
    }


    /**
     *  A complete view of the {@code RaceConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceConstrainerView() {
        this.session.useComparativeRaceConstrainerView();
        return;
    }


    /**
     *  A complete view of the {@code RaceConstrainer} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceConstrainerView() {
        this.session.usePlenaryRaceConstrainerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race constrainers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active race constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRaceConstrainerView() {
        this.session.useActiveRaceConstrainerView();
        return;
    }


    /**
     *  Active and inactive race constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceConstrainerView() {
        this.session.useAnyStatusRaceConstrainerView();
        return;
    }
    
     
    /**
     *  Gets the {@code RaceConstrainer} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code RaceConstrainer} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code RaceConstrainer} and
     *  retained for compatibility.
     *
     *  In active mode, race constrainers are returned that are currently
     *  active. In any status mode, active and inactive race constrainers
     *  are returned.
     *
     *  @param raceConstrainerId {@code Id} of the {@code RaceConstrainer}
     *  @return the race constrainer
     *  @throws org.osid.NotFoundException {@code raceConstrainerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code raceConstrainerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainer getRaceConstrainer(org.osid.id.Id raceConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceConstrainer(raceConstrainerId));
    }


    /**
     *  Gets a {@code RaceConstrainerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  raceConstrainers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code RaceConstrainers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, race constrainers are returned that are currently
     *  active. In any status mode, active and inactive race constrainers
     *  are returned.
     *
     *  @param  raceConstrainerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code RaceConstrainer} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code raceConstrainerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByIds(org.osid.id.IdList raceConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceConstrainersByIds(raceConstrainerIds));
    }


    /**
     *  Gets a {@code RaceConstrainerList} corresponding to the given
     *  race constrainer genus {@code Type} which does not include
     *  race constrainers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  race constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race constrainers are returned that are currently
     *  active. In any status mode, active and inactive race constrainers
     *  are returned.
     *
     *  @param  raceConstrainerGenusType a raceConstrainer genus type 
     *  @return the returned {@code RaceConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceConstrainerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByGenusType(org.osid.type.Type raceConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceConstrainersByGenusType(raceConstrainerGenusType));
    }


    /**
     *  Gets a {@code RaceConstrainerList} corresponding to the given
     *  race constrainer genus {@code Type} and include any additional
     *  race constrainers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  race constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race constrainers are returned that are currently
     *  active. In any status mode, active and inactive race constrainers
     *  are returned.
     *
     *  @param  raceConstrainerGenusType a raceConstrainer genus type 
     *  @return the returned {@code RaceConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceConstrainerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByParentGenusType(org.osid.type.Type raceConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceConstrainersByParentGenusType(raceConstrainerGenusType));
    }


    /**
     *  Gets a {@code RaceConstrainerList} containing the given
     *  race constrainer record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  race constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race constrainers are returned that are currently
     *  active. In any status mode, active and inactive race constrainers
     *  are returned.
     *
     *  @param  raceConstrainerRecordType a raceConstrainer record type 
     *  @return the returned {@code RaceConstrainer} list
     *  @throws org.osid.NullArgumentException
     *          {@code raceConstrainerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainersByRecordType(org.osid.type.Type raceConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceConstrainersByRecordType(raceConstrainerRecordType));
    }


    /**
     *  Gets all {@code RaceConstrainers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  race constrainers or an error results. Otherwise, the returned list
     *  may contain only those race constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race constrainers are returned that are currently
     *  active. In any status mode, active and inactive race constrainers
     *  are returned.
     *
     *  @return a list of {@code RaceConstrainers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerList getRaceConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRaceConstrainers());
    }
}
