//
// AbstractUnknownStatistic.java
//
//     Defines an unknown Statistic.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.metering.statistic.spi;


/**
 *  Defines an unknown <code>Statistic</code>.
 */

public abstract class AbstractUnknownStatistic
    extends net.okapia.osid.jamocha.metering.statistic.spi.AbstractStatistic
    implements org.osid.metering.Statistic {

    protected static final String OBJECT = "osid.metering.Statistic";


    /**
     *  Constructs a new <code>AbstractUnknownStatistic</code>.
     */

    public AbstractUnknownStatistic() {
        setMeter(new net.okapia.osid.jamocha.nil.metering.meter.UnknownMeter());
        setMeteredObjectId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(""));

        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        
        setSum(new java.math.BigDecimal(0));
        setMean(new java.math.BigDecimal(0));
        setMedian(new java.math.BigDecimal(0));
        setMode(new java.math.BigDecimal(0));
        setStandardDeviation(new java.math.BigDecimal(0));
        setRMS(new java.math.BigDecimal(0));       
        setDelta(new java.math.BigDecimal(0)); 
        setPercentChange(new java.math.BigDecimal(0));
        setAverageRate(new java.math.BigDecimal(0), org.osid.calendaring.DateTimeResolution.INFINITY);
        
        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownStatistic</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownStatistic(boolean optional) {
        this();
        return;
    }
}
