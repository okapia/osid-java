//
// InvariantMapProxyAuditEnablerLookupSession
//
//    Implements an AuditEnabler lookup service backed by a fixed
//    collection of auditEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry.rules;


/**
 *  Implements an AuditEnabler lookup service backed by a fixed
 *  collection of audit enablers. The audit enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAuditEnablerLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.rules.spi.AbstractMapAuditEnablerLookupSession
    implements org.osid.inquiry.rules.AuditEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuditEnablerLookupSession} with no
     *  audit enablers.
     *
     *  @param inquest the inquest
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuditEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.proxy.Proxy proxy) {
        setInquest(inquest);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyAuditEnablerLookupSession} with a single
     *  audit enabler.
     *
     *  @param inquest the inquest
     *  @param auditEnabler an single audit enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code auditEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuditEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.inquiry.rules.AuditEnabler auditEnabler, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putAuditEnabler(auditEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAuditEnablerLookupSession} using
     *  an array of audit enablers.
     *
     *  @param inquest the inquest
     *  @param auditEnablers an array of audit enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code auditEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuditEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.inquiry.rules.AuditEnabler[] auditEnablers, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putAuditEnablers(auditEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuditEnablerLookupSession} using a
     *  collection of audit enablers.
     *
     *  @param inquest the inquest
     *  @param auditEnablers a collection of audit enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code auditEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuditEnablerLookupSession(org.osid.inquiry.Inquest inquest,
                                                  java.util.Collection<? extends org.osid.inquiry.rules.AuditEnabler> auditEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putAuditEnablers(auditEnablers);
        return;
    }
}
