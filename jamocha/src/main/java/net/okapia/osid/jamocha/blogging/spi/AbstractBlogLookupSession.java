//
// AbstractBlogLookupSession.java
//
//    A starter implementation framework for providing a Blog
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Blog
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBlogs(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBlogLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.blogging.BlogLookupSession {

    private boolean pedantic = false;
    private org.osid.blogging.Blog blog = new net.okapia.osid.jamocha.nil.blogging.blog.UnknownBlog();
    


    /**
     *  Tests if this user can perform <code>Blog</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBlogs() {
        return (true);
    }


    /**
     *  A complete view of the <code>Blog</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBlogView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Blog</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBlogView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Blog</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Blog</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Blog</code> and
     *  retained for compatibility.
     *
     *  @param  blogId <code>Id</code> of the
     *          <code>Blog</code>
     *  @return the blog
     *  @throws org.osid.NotFoundException <code>blogId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>blogId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Blog getBlog(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.blogging.BlogList blogs = getBlogs()) {
            while (blogs.hasNext()) {
                org.osid.blogging.Blog blog = blogs.getNextBlog();
                if (blog.getId().equals(blogId)) {
                    return (blog);
                }
            }
        } 

        throw new org.osid.NotFoundException(blogId + " not found");
    }


    /**
     *  Gets a <code>BlogList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  blogs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Blogs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBlogs()</code>.
     *
     *  @param  blogIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Blog</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>blogIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByIds(org.osid.id.IdList blogIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.blogging.Blog> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = blogIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBlog(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("blog " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.blogging.blog.LinkedBlogList(ret));
    }


    /**
     *  Gets a <code>BlogList</code> corresponding to the given
     *  blog genus <code>Type</code> which does not include
     *  blogs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBlogs()</code>.
     *
     *  @param  blogGenusType a blog genus type 
     *  @return the returned <code>Blog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blogGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByGenusType(org.osid.type.Type blogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.blogging.blog.BlogGenusFilterList(getBlogs(), blogGenusType));
    }


    /**
     *  Gets a <code>BlogList</code> corresponding to the given
     *  blog genus <code>Type</code> and include any additional
     *  blogs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBlogs()</code>.
     *
     *  @param  blogGenusType a blog genus type 
     *  @return the returned <code>Blog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blogGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByParentGenusType(org.osid.type.Type blogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBlogsByGenusType(blogGenusType));
    }


    /**
     *  Gets a <code>BlogList</code> containing the given
     *  blog record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBlogs()</code>.
     *
     *  @param  blogRecordType a blog record type 
     *  @return the returned <code>Blog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>blogRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByRecordType(org.osid.type.Type blogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.blogging.blog.BlogRecordFilterList(getBlogs(), blogRecordType));
    }


    /**
     *  Gets a <code>BlogList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known blogs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  blogs that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Blog</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.blogging.BlogList getBlogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.blogging.blog.BlogProviderFilterList(getBlogs(), resourceId));
    }


    /**
     *  Gets all <code>Blogs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  blogs or an error results. Otherwise, the returned list
     *  may contain only those blogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Blogs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.blogging.BlogList getBlogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the blog list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of blogs
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.blogging.BlogList filterBlogsOnViews(org.osid.blogging.BlogList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
