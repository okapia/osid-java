//
// AbstractActivity.java
//
//     Defines an Activity.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Activity</code>.
 */

public abstract class AbstractActivity
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.Activity {

    private org.osid.course.ActivityUnit activityUnit;
    private org.osid.course.CourseOffering courseOffering;
    private org.osid.course.Term term;

    private boolean implicit;
    private long minimumSeats;
    private long maximumSeats;
    private boolean contact = true;
    private boolean requiresRegistration = false;

    private org.osid.calendaring.Duration totalTargetEffort;
    private org.osid.calendaring.Duration totalTargetContactTime;
    private org.osid.calendaring.Duration totalTargetIndividualEffort;
    private org.osid.calendaring.Duration weeklyEffort;
    private org.osid.calendaring.Duration weeklyContactTime;
    private org.osid.calendaring.Duration weeklyIndividualEffort;

    private final java.util.Collection<org.osid.calendaring.Schedule> schedules = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.Activity> supersedingActivities = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.MeetingTime> specificMeetingTimes = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.DateTimeInterval> blackouts = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.Resource> instructors = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.ActivityRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the activity unit <code> Id </code> associated with this 
     *  activity. 
     *
     *  @return the activity unit <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityUnitId() {
        return (this.activityUnit.getId());
    }


    /**
     *  Gets the activity unit associated with this activity. 
     *
     *  @return the activity unit 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityUnit getActivityUnit()
        throws org.osid.OperationFailedException {

        return (this.activityUnit);
    }


    /**
     *  Sets the activity unit.
     *
     *  @param activityUnit an activity unit
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnit</code> is <code>null</code>
     */

    protected void setActivityUnit(org.osid.course.ActivityUnit activityUnit) {
        nullarg(activityUnit, "activity unit");
        this.activityUnit = activityUnit;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> CourseOffering
     *  </code> of this offering.
     *
     *  @return the <code> CourseOffering </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseOfferingId() {
        return (this.courseOffering.getId());
    }


    /**
     *  Gets the <code> CourseOffering </code> of this offering. 
     *
     *  @return the course offering 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.CourseOffering getCourseOffering()
        throws org.osid.OperationFailedException {

        return (this.courseOffering);
    }


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException <code>courseOffering</code> is
     *          <code>null</code>
     */

    protected void setCourseOffering(org.osid.course.CourseOffering courseOffering) {
        nullarg(courseOffering, "course offering");
        this.courseOffering = courseOffering;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term </code> of this 
     *  offering. 
     *
     *  @return the <code> Term </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        return (this.term.getId());
    }


    /**
     *  Gets the <code> Term </code> of this offering. 
     *
     *  @return the term 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        return (this.term);
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    protected void setTerm(org.osid.course.Term term) {
        nullarg(term, "term");
        this.term = term;
        return;
    }


    /**
     *  Tests if this is a an implicit or explicit activity. 
     *
     *  @return <code> true </code> if this is an implicit activity,
     *          <code> false </code> if an explicit activity
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.implicit);
    }


    /**
     *  Sets this activity as implicit.
     *
     *  @param implicit <code> true </code> if this is an implicit
     *         activity, <code> false </code> if an explicit activity
     */

    protected void setImplicit(boolean implicit) {
        this.implicit = implicit;
        return;
    }


    /**
     *  Gets the schedule <code> Ids </code> associated with this activity. 
     *
     *  @return the schedule <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getScheduleIds() {
        try {
            org.osid.calendaring.ScheduleList schedules = getSchedules();
            return (new net.okapia.osid.jamocha.adapter.converter.calendaring.schedule.ScheduleToIdList(schedules));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the schedules associated with this activity. 
     *
     *  @return the schedules 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.calendaring.schedule.ArrayScheduleList(this.schedules));
    }


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    protected void addSchedule(org.osid.calendaring.Schedule schedule) {
        nullarg(schedule, "schedule");
        this.schedules.add(schedule);
        return;
    }


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @throws org.osid.NullArgumentException
     *          <code>schedules</code> is <code>null</code>
     */

    protected void setSchedules(java.util.Collection<org.osid.calendaring.Schedule> schedules) {
        nullarg(schedules, "schedules");

        this.schedules.clear();
        this.schedules.addAll(schedules);

        return;
    }


    /**
     *  Gets the superseding activity <code> Ids </code> whose sessions 
     *  override sessions of this activity. 
     *
     *  @return the superseding activity <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSupersedingActivityIds() {
        try {
            org.osid.course.ActivityList supersedingActivities = getSupersedingActivities();
            return (new net.okapia.osid.jamocha.adapter.converter.course.activity.ActivityToIdList(supersedingActivities));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the superseding activities whose sessions that override sessions 
     *  of this activity. 
     *
     *  @return the superseding activities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.ActivityList getSupersedingActivities()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.course.activity.ArrayActivityList(this.supersedingActivities));
    }


    /**
     *  Adds a superseding activity.
     *
     *  @param activity a superseding activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected void addSupersedingActivity(org.osid.course.Activity activity) {
        nullarg(activity, "activity");
        this.supersedingActivities.add(activity);
        return;
    }


    /**
     *  Sets all the superseding activities.
     *
     *  @param activities a collection of superseding activities
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    protected void setSupersedingActivities(java.util.Collection<org.osid.course.Activity> activities) {
        nullarg(activities, "activities");

        this.supersedingActivities.clear();
        this.supersedingActivities.addAll(activities);
        return;
    }


    /**
     *  Gets the specific meeting times added to this activity. 
     *
     *  @return a list of specific meeting times 
     */

    @OSID @Override
    public org.osid.calendaring.MeetingTimeList getSpecificMeetingTimes() {
        return (new net.okapia.osid.jamocha.calendaring.meetingtime.ArrayMeetingTimeList(this.specificMeetingTimes));
    }


    /**
     *  Adds a specific meeting time.
     *
     *  @param time a specific meeting time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void addSpecificMeetingTime(org.osid.calendaring.MeetingTime time) {
        nullarg(time, "meeting time");
        this.specificMeetingTimes.add(time);
        return;
    }


    /**
     *  Sets all the specific meeting times.
     *
     *  @param times a collection of specific meeting times
     *  @throws org.osid.NullArgumentException
     *          <code>times</code> is <code>null</code>
     */

    protected void setSpecificMeetingTimes(java.util.Collection<org.osid.calendaring.MeetingTime> times) {
        nullarg(times, "meeting times");

        this.specificMeetingTimes.clear();
        this.specificMeetingTimes.addAll(times);

        return;
    }


    /**
     *  Gets the blackout dates for this activity. Activitiy sessions
     *  overlapping with the time intervals do not appear in the
     *  series.
     *
     *  @return the exceptions to the scheduled activity 
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeIntervalList getBlackouts() {
        return (new net.okapia.osid.jamocha.calendaring.datetimeinterval.ArrayDateTimeIntervalList(this.blackouts));
    }


    /**
     *  Adds a blackout.
     *
     *  @param blackout a blackout
     *  @throws org.osid.NullArgumentException
     *          <code>blackout</code> is <code>null</code>
     */

    protected void addBlackout(org.osid.calendaring.DateTimeInterval blackout) {
        nullarg(blackout, "blackout");
        this.blackouts.add(blackout);
        return;
    }


    /**
     *  Sets all the blackouts.
     *
     *  @param blackouts a collection of blackouts
     *  @throws org.osid.NullArgumentException <code>blackouts</code>
     *          is <code>null</code>
     */

    protected void setBlackouts(java.util.Collection<org.osid.calendaring.DateTimeInterval> blackouts) {
        nullarg(blackouts, "blackouts");

        this.blackouts.clear();
        this.blackouts.addAll(blackouts);

        return;
    }


    /**
     *  Gets the instructor <code> Ids. </code> If each activity has
     *  its own instructor, the headlining instructors may be
     *  returned.
     *
     *  @return the instructor <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getInstructorIds() {
        try {
            org.osid.resource.ResourceList instructors = getInstructors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(instructors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the instructors. If each activity has its own instructor, the 
     *  headlining instructors may be returned. 
     *
     *  @return the sponsors 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getInstructors()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.instructors));
    }


    /**
     *  Adds an instructor.
     *
     *  @param instructor an instructor
     *  @throws org.osid.NullArgumentException
     *          <code>instructor</code> is <code>null</code>
     */

    protected void addInstructor(org.osid.resource.Resource instructor) {
        nullarg(instructor, "instructor");
        this.instructors.add(instructor);
        return;
    }


    /**
     *  Sets all the instructors.
     *
     *  @param instructors a collection of instructors
     *  @throws org.osid.NullArgumentException
     *          <code>instructors</code> is <code>null</code>
     */

    protected void setInstructors(java.util.Collection<org.osid.resource.Resource> instructors) {
        nullarg(instructors, "instructors");

        this.instructors.clear();
        this.instructors.addAll(instructors);

        return;
    }


    /**
     *  Tests if this course offering requires advanced registration. 
     *
     *  @return <code> true </code> if this course requires advance 
     *          registration, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean requiresRegistration() {
        return (this.requiresRegistration);
    }


    /**
     *  Sets the requires registration flag.
     *
     *  @param requires <code> true </code> if this course requires
     *          advance registration, <code> false </code> otherwise
     */

    protected void setRequiresRegistration(boolean requires) {
        this.requiresRegistration = requires;
        return;
    }


    /** 
     *  Gets the minimum number of students this activity can have.
     *
     *  @return the minimum seats 
     */

    @OSID @Override
    public long getMinimumSeats() {
        return (this.minimumSeats);
    }


    /**
     *  Sets the minimum seats.
     *
     *  @param seats a minimum seats
     *  @throws org.osid.NullArgumentException <code>seats</code> is
     *          <code>null</code>
     */

    protected void setMinimumSeats(long seats) {
        cardinalarg(seats, "minimum seats");
        this.minimumSeats = seats;
        return;
    }


    /**
     *  Tests if this activity offering has limited seating. 
     *
     *  @return <code> true </code> if this has limietd seating
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isSeatingLimited() {
        return (this.maximumSeats > 0);
    }


    /**
     *  Gets the maximum number of students this activity can accommodate. 
     *
     *  @return the maximum seats 
     *  @throws org.osid.IllegalStateException <code> requiresRegistration() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getMaximumSeats() {
        if (!isSeatingLimited()) {
            throw new org.osid.IllegalStateException("requiresRegistration() is false");
        }

        return (this.maximumSeats);
    }


    /**
     *  Sets the maximum seats.
     *
     *  @param seats a maximum seats
     *  @throws org.osid.NullArgumentException <code>seats</code> is
     *          <code>null</code>
     */

    protected void setMaximumSeats(long seats) {
        cardinalarg(seats, "maximum seats");
        this.maximumSeats = seats;
        return;
    }


    /**
     *  Gets the total time required for this activity. 
     *
     *  @return the total effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetEffort() {
        return (this.totalTargetEffort);
    }


    /**
     *  Sets the total target effort.
     *
     *  @param effort a total target effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    protected void setTotalTargetEffort(org.osid.calendaring.Duration effort) {
        nullarg(effort, "effort");
        this.totalTargetEffort = effort;
        return;
    }


    /**
     *  Tests if this is a contact activity. 
     *
     *  @return <code> true </code> if this is a contact activity,
     *          <code> false </code> if an independent activity
     */

    @OSID @Override
    public boolean isContact() {
        return (this.contact);
    }


    /**
     *  Sets the contact.
     *
     *  @param contact <code> true </code> if this is a contact
     *         activity, <code> false </code> if an independent
     *         activity
     */

    protected void setContact(boolean contact) {
        this.contact = contact;
        return;
    }


    /**
     *  Gets the total contact time for this activity. 
     *
     *  @return the total effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetContactTime() {
        return (this.totalTargetContactTime);
    }


    /**
     *  Sets the total target contact time.
     *
     *  @param time a total target contact time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setTotalTargetContactTime(org.osid.calendaring.Duration time) {
        nullarg(time, "total target contact time");
        this.totalTargetContactTime = time;
        return;
    }


    /**
     *  Gets the total indivudal time required for this activity. 
     *
     *  @return the total individual effort 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalTargetIndividualEffort() {
        return (this.totalTargetIndividualEffort);
    }


    /**
     *  Sets the total target individual effort.
     *
     *  @param effort a total target individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    protected void setTotalTargetIndividualEffort(org.osid.calendaring.Duration effort) {
        nullarg(effort, "total target individual effort");
        this.totalTargetIndividualEffort = effort;
        return;
    }


    /**
     *  Tests if this activity is recurring. 
     *
     *  @return <code> true </code> if this activity is recurring,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isRecurringWeekly() {
        return ((this.weeklyEffort != null) && (this.weeklyContactTime != null) &&
                (this.weeklyIndividualEffort != null));
    }


    /**
     *  Gets the time required for this recurring effort on a weekly basis. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyEffort() {
        if (!isRecurringWeekly()) {
            throw new org.osid.IllegalStateException("isRecurringWeekly() is false");
        }

        return (this.weeklyEffort);
    }


    /**
     *  Sets the weekly effort.
     *
     *  @param effort a weekly effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    protected void setWeeklyEffort(org.osid.calendaring.Duration effort) {
        nullarg(effort, "weekly effort");
        this.weeklyEffort = effort;
        return;
    }


    /**
     *  Gets the weekly contact time for ths activity. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyContactTime() {
        if (!isRecurringWeekly()) {
            throw new org.osid.IllegalStateException("isRecurringWeekly() is false");
        }

        return (this.weeklyContactTime);
    }


    /**
     *  Sets the weekly contact time.
     *
     *  @param time a weekly contact time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void setWeeklyContactTime(org.osid.calendaring.Duration time) {
        nullarg(time, "weekly cotact time");
        this.weeklyContactTime = time;
        return;
    }


    /**
     *  Gets the weekly individual time for ths activity. 
     *
     *  @return the weekly time 
     *  @throws org.osid.IllegalStateException <code> isRecurringWeekly() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getWeeklyIndividualEffort() {
        if (!isRecurringWeekly()) {
            throw new org.osid.IllegalStateException("isRecurringWeekly() is false");
        }

        return (this.weeklyIndividualEffort);
    }


    /**
     *  Sets the weekly individual effort.
     *
     *  @param effort a weekly individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    protected void setWeeklyIndividualEffort(org.osid.calendaring.Duration effort) {
        nullarg(effort, "weekly individual effort");
        this.weeklyIndividualEffort = effort;
        return;
    }


    /**
     *  Tests if this activity supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityRecordType an activity record type 
     *  @return <code>true</code> if the activityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRecordType) {
        for (org.osid.course.records.ActivityRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Activity</code> record <code>Type</code>.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityRecord getActivityRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityRecord the activity record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecord</code> or
     *          <code>activityRecordTypeactivity</code> is
     *          <code>null</code>
     */
            
    protected void addActivityRecord(org.osid.course.records.ActivityRecord activityRecord, 
                                     org.osid.type.Type activityRecordType) {

        nullarg(activityRecord, "activity record");
        addRecordType(activityRecordType);
        this.records.add(activityRecord);
        
        return;
    }
}
