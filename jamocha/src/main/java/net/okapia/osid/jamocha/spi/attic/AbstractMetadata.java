//
// AbstractMetadata
//
//     Defines an object implementing the Metadata interface.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 June 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All Rights 
// Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.util.MethodCheck.nullarg;


/**
 *  This implementation provides a simple means for managing metadata
 *  values. Note that the OSID integers and cardinal types are bound
 *  to <code>long</code> and float is bound to <code>double</code> in
 *  Java.
 */

public abstract class AbstractMetadata 
    implements org.osid.Metadata {

    private final org.osid.id.Id elementId;
    private final org.osid.Syntax syntax;
    private String instructions;
    private boolean required;
    private boolean isset;
    private boolean readonly;
    private String units;

    /**
     *  Constructs a new <code>AbstractMetadata</code>.
     *
     *  @param elementId the Id of a form element
     *  @param syntax the syntax of this metadata
     *  @throws org.osid.NullArgumentException <code>elementId</code>
     *          or <code>synatx</code> is <code>null</code>
     */

    protected AbstractMetadata(org.osid.id.Id elementId, org.osid.Syntax syntax) {
        nullarg(elementId, "element Id");
        nullarg(synatx, "syntax");

        this.elementId = elementId;
        this.syntax    = syntax;

        return;
    }


    /**
     *  Gets instructions for updating this data. This is a human
     *  readable description of the data element or property that may
     *  include special instructions or caveats to the end-user above
     *  and beyond what this interface provides.
     *
     *  @return instructions 
     */

    @OSID @Override
    public String getInstructions() {
	return (this.instructions);
    }


    /**
     *  Sets instructions for updating this data. This is a human
     *  readable description of the data element or property that may
     *  include special instructions or caveats to the end-user above
     *  and beyond what this interface provides.
     *
     *  @param instructions
     *  @throws org.osid.NullArgumentException
     *          <code>instructions</code> is <code>null</code>
     */

    protected void setInstructions(String instructions) {
	nullarg(instructions, "instructions");
	this.instructions = instructions;
	return;
    }


    /**
     *  Tests if this data element is required for creating new
     *  objects.
     *
     *  @return <code> true </code> if this data is required, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isRequired() {
	return (this.required);
    }


    /**
     *  Sets the required flag.
     *
     *  @param required <code>true</code> if this element is required,
     *         <code>false</code> if optional
     */

    protected void required(boolean required) {
	this.required = required;
	return;
    }


    /**
     *  Tests if this data element has a value.
     *
     *  @return <code> true </code> a value has been set, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasValue() {
	return (this.isset);
    }


    /**
     *  Sets the isSet() flag to indicate a value present.
     *
     *  @param set <code>true</code> if a value is associated with
     *         this element, <code>false</code> otherwise
     */

    protected void setIsSetFlag(boolean set) {
	this.isset = set;
	return;
    }


    /**
     *  Tests if this data can be updated. This may indicate the result of a 
     *  pre-authorization but is not a guarantee that an authorization failure 
     *  will not occur when the create or update transaction is issued. 
     *
     *  @return <code> true </code> if this data is not updatable, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isReadOnly() {
	return (this.readonly);
    }


    /**
     *  Sets the readonly flag.
     *
     *  @param readonly <code>true</code> if this element cannot be modified,
     *         <code>false</code> if it can be modified
     */

    protected void readonly(boolean readonly) {
	this.readonly = readonly;
	return;
    }


    /**
     *  Tests if this data element is linked to other data in the object. 
     *  Updating linked data should refresh all metadata and revalidate object 
     *  elements. 
     *
     *  @return true if this element is linked, false if updates have no side 
     *          effect 
     */

    @OSID @Override
    public boolean isLinked() {
        return (this.linked);
    }

    
    /**
     *  Sets the linked flag.
     *
     *  @param linked <code>true</code> if this element is linekd to a
     *         value in another element, <code>false</code> otherwise
     */

    protected void setLinked(boolean linked) {
        this.linked = linked;
        return;
    }
    

    /**
     *  Gets the units of this data for display purposes ('lbs', 'gills', 
     *  'furlongs'). 
     *
     *  @return the display units of this data or an empty string if not 
     *          applicable 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getUnits() {
        return (this.units);
    }


    /**
     *  Sets the units label.
     *
     *  @param label the units label
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    protected void setUnits(String label) {
        nullarg(label, "units label");
        this.units = label;
        return;
    }
    

    /**
     *  In the case where an array or list of elements is specified in an 
     *  <code> OsidForm, </code> this specifies the minimum number of elements 
     *  that must be included. 
     *
     *  @return the minimum elements 
     */

    @OSID @Override
    public long getMinElements() {
        return (this.minElements);
    }


    /**
     *  In the case where an array or list of elements is specified in an 
     *  <code> OsidForm, </code> this specifies the maximum number of elements 
     *  that must be included. 
     *
     *  @return the miaxmum elements 
     */

    @OSID @Override
    public long getMaxElements() {
        return (this.maxElements);
    }


    /**
     *  Gets the minimum cardinal value. 
     *
     *  @return the minimum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> 
     */

    @OSID @Override
    public long getMinCardinal() {
	throw new org.osid.IllegalStateException("metadata error: not a cardinal");
    }


    /**
     *  Gets the maximum cardinal value. 
     *
     *  @return the maximum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> 
     */

    @OSID @Override
    public long getMaxCardinal() {
	throw new org.osid.IllegalStateException("metadata error: not a cardinal");
    }


    /**
     *  Gets the set of acceptable cardinal values. 
     *
     *  @return the set of values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> CARDINAL 
     *          </code> 
     */

    @OSID @Override
    public long[] getCardinalSet() {
	throw new org.osid.IllegalStateException("metadata error: not a cardinal");
    }


    /**
     *  Gets the set of acceptable record <code> Types </code> for a
     *  Coordinate.
     *
     *  @return the set of <code> Types </code> 
     *  @throws org.osid.IllegalStateException syntax is not an <code> OBJECT 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getCoordinateRecordTypes() {
	throw new org.osid.IllegalStateException("metadata error: not a coordinate");
    }


    /**
     *  Tests if the given coordinate record type is supported. 
     *
     *  @param  coordinateRecordType an object Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not an <code> OBJECT 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code>coordinateRecordType</code>
     *          is <code>null</code>
     */

    @OSID @Override
    public boolean supportsCoordinateRecordType(org.osid.type.Type coordinateRecordType) {
	throw new org.osid.IllegalStateException("metadata error: not a coordinate");
    }


    /**
     *  Gets the minimum date value. 
     *
     *  @return the minimum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DATETIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getMinDateTime() {
	throw new org.osid.IllegalStateException("metadata error: not a datetime");
    }


    /**
     *  Gets the maximum date value. 
     *
     *  @return the maximum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DATETIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getMaxDateTime() {
	throw new org.osid.IllegalStateException("metadata error: not a datetime");
    }


    /**
     *  Gets the set of acceptable date values. 
     *
     *  @return the set of values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DATETIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime[] getDateTimeSet() {
	throw new org.osid.IllegalStateException("metadata error: not a datetime");
    }


    /**
     *  Gets the resolution of the timestamp. 
     *
     *  @return the set of values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DATETIME 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeResolution getDateTimeResolution() {
	throw new org.osid.IllegalStateException("metadata error: not a datetime");
    }


    /**
     *  Gets the minimum distance value. 
     *
     *  @return the minimum value 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>DISTANCE</code>
     */

    @OSID @Override
    public org.osid.mapping.Distance getMinDistance() {
	throw new org.osid.IllegalStateException("metadata error: not a distance");
    }


    /**
     *  Gets the maximum distance value. 
     *
     *  @return the maximum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DISTANCE 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance getMaxDistance() {
	throw new org.osid.IllegalStateException("metadata error: not a distance");
    }


    /**
     *  Gets the set of acceptable distance values. 
     *
     *  @return the set of values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DISTANCE 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance[] getDistanceSet() {
	throw new org.osid.IllegalStateException("metadata error: not a distance");
    }


    /**
     *  Gets the resolution of the distance value. 
     *
     *  @return the resolution 
     *  @throws org.osid.IllegalStateException syntax is not a <code> DISTANCE 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.DistanceResolution getDistanceResolution() {
	throw new org.osid.IllegalStateException("metadata error: not a distance");
    }
    

    /**
     *  Gets the minimum float value. 
     *
     *  @return the minimum value 
     *  @throws org.osid.IllegalStateException syntax is not a <code> FLOAT 
     *          </code> 
     */

    @OSID @Override
    public double getMinFloat() {
	throw new org.osid.IllegalStateException("metadata error: not a float");
    }


    /**
     *  Gets the maximum float value. 
     *
     *  @return the maximum float 
     *  @throws org.osid.IllegalStateException syntax is not a <code> FLOAT 
     *          </code> 
     */

    @OSID @Override
    public double getMaxFloat() {
	throw new org.osid.IllegalStateException("metadata error: not a float");
    }


    /**
     *  Gets the set of acceptable float values. 
     *
     *  @return the set of values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> FLOAT 
     *          </code> 
     */

    @OSID @Override
    public double[] getFloatSet() {
	throw new org.osid.IllegalStateException("metadata error: not a float");
    }


    /**
     *  Gets the minimum integer value. 
     *
     *  @return the minimum value 
     *  @throws org.osid.IllegalStateException syntax is not an <code> INTEGER 
     *          </code> 
     */

    @OSID @Override
    public long getMinInteger() {
	throw new org.osid.IllegalStateException("metadata error: not an integer");
    }


    /**
     *  Gets the maximum integer value. 
     *
     *  @return the maximum value 
     *  @throws org.osid.IllegalStateException syntax is not an <code> INTEGER 
     *          </code> 
     */

    @OSID @Override
    public long getMaxInteger() {
	throw new org.osid.IllegalStateException("metadata error: not an integer");
    }


    /**
     *  Gets the set of acceptable integer values. 
     *
     *  @return the set of values 
     *  @throws org.osid.IllegalStateException syntax is not an <code> INTEGER 
     *          </code> 
     */

    @OSID @Override
    public long[] getIntegerSet() {
	throw new org.osid.IllegalStateException("metadata error: not an integer");
    }


    /**
     *  Gets the set of acceptable spatial unit record types. 
     *
     *  @return the set of spatial unit types 
     *  @throws org.osid.IllegalStateException syntax is not <code> 
     *          SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getSpatialUnitRecordTypes() {
	throw new org.osid.IllegalStateException("metadata error: not an integer");
    }


    /**
     *  Tests if the given spaital unit record type is supported. 
     *
     *  @param  spatialUnitRecordType
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a <code>COORDINATE
     *          </code> 
     *  @throws org.osid.NullArgumentException <code>spatialUnitRecordType</code>
     *          is <code>null</code>
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
	throw new org.osid.IllegalStateException("metadata error: not a spatial unit");
    }


    /**
     *  Gets the minimum string size. 
     *
     *  @return the minimum string length 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     */

    @OSID @Override
    public long getMinStringLength() {
	throw new org.osid.IllegalStateException("metadata error: not a string");
    }


    /**
     *  Gets the maximum string size.
     *
     *  @return the maximum string length 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     */

    @OSID @Override
    public long getMaxStringLength() {
	throw new org.osid.IllegalStateException("metadata error: not a string");
    }


    /**
     *  Gets the set of acceptable string values. 
     *
     *  @return the set of values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> STRING 
     *          </code> 
     */

    @OSID @Override
    public String[] getStringSet() {
	throw new org.osid.IllegalStateException("metadata error: not a string");
    }


    /** 
     *  Gets the set of acceptable <code> Ids. </code> 
     *          
     *  @return the set of <code> Ids </code>  
     *  @throws org.osid.IllegalStateException syntax is not an <code> ID
     *          </code>
     */

    @OSID @Override
    public org.osid.id.Id[] getIdSet() {
	throw new org.osid.IllegalStateException("metadata error: not an Id");
    }


    /**
     *  Gets the set of acceptable <code> Types. </code>
     *
     *  @return the set of <code> Types </code>
     *  @throws org.osid.IllegalStateException syntax is not an <code> TYPE
     *          </code>
     */
    
    @OSID @Override
    public org.osid.type.Type[] getTypeSet()  {
	throw new org.osid.IllegalStateException("metadata error: not an Id");
    }


    /**
     *  Gets the set of acceptable <code> Types </code> for an object <code> . 
     *  </code> 
     *
     *  @return the set of <code> Types </code> 
     *  @throws org.osid.IllegalStateException syntax is not an <code> OBJECT 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.Type[] getObjectTypes() {
	throw new org.osid.IllegalStateException("metadata error: not an object");
    }


    /**
     *  Tests if the given object type is supported. 
     *
     *  @param  objectType an object Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not an <code> OBJECT 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code>objectTypeType</code>
     *          is <code>null</code>
     */

    @OSID @Override
    public boolean supportsObjectType(org.osid.type.Type objectType) {
	throw new org.osid.IllegalStateException("metadata error: not an object");
    }
}
