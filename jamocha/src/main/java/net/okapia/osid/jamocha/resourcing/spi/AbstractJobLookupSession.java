//
// AbstractJobLookupSession.java
//
//    A starter implementation framework for providing a Job
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Job lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getJobs(), this other methods may need to be overridden for better
 *  performance.
 */

public abstract class AbstractJobLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.resourcing.JobLookupSession {

    private boolean pedantic   = false;
    private boolean activeonly = false;
    private boolean federated  = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();
    

    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this session.
     *
     *  @return the <code>Foundry</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>Job</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJobs() {
        return (true);
    }


    /**
     *  A complete view of the <code>Job</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJobView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Job</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJobView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include jobs in foundries which are children of this
     *  foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active jobs are returned by methods in this session.
     */
     
    @OSID @Override
    public void useActiveJobView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive jobs are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Job</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Job</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Job</code> and retained for
     *  compatibility.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs are
     *  returned.
     *
     *  @param jobId <code>Id</code> of the <code>Job</code>
     *  @return the job
     *  @throws org.osid.NotFoundException <code>jobId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>jobId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Job getJob(org.osid.id.Id jobId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.resourcing.JobList jobs = getJobs()) {
            while (jobs.hasNext()) {
                org.osid.resourcing.Job job = jobs.getNextJob();
                if (job.getId().equals(jobId)) {
                    return (job);
                }
            }
        } 

        throw new org.osid.NotFoundException(jobId + " not found");
    }


    /**
     *  Gets a <code>JobList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the jobs
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Jobs</code> may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getJobs()</code>.
     *
     *  @param jobIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Job</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>jobIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByIds(org.osid.id.IdList jobIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.resourcing.Job> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = jobIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getJob(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("job " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.resourcing.job.LinkedJobList(ret));
    }


    /**
     *  Gets a <code>JobList</code> corresponding to the given job
     *  genus <code>Type</code> which does not include jobs of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known jobs or
     *  an error results. Otherwise, the returned list may contain
     *  only those jobs that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getJobs()</code>.
     *
     *  @param jobGenusType a job genus type
     *  @return the returned <code>Job</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByGenusType(org.osid.type.Type jobGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.job.JobGenusFilterList(getJobs(), jobGenusType));
    }


    /**
     *  Gets a <code>JobList</code> corresponding to the given job
     *  genus <code>Type</code> and include any additional jobs with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known jobs or
     *  an error results. Otherwise, the returned list may contain
     *  only those jobs that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJobs()</code>.
     *
     *  @param jobGenusType a job genus type
     *  @return the returned <code>Job</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByParentGenusType(org.osid.type.Type jobGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getJobsByGenusType(jobGenusType));
    }


    /**
     *  Gets a <code>JobList</code> containing the given job record
     *  <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known jobs or
     *  an error results. Otherwise, the returned list may contain
     *  only those jobs that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getJobs()</code>.
     *
     *  @param  jobRecordType a job record type 
     *  @return the returned <code>Job</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByRecordType(org.osid.type.Type jobRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.resourcing.job.JobRecordFilterList(getJobs(), jobRecordType));
    }


    /**
     *  Gets a <code>JobList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known jobs or
     *  an error results. Otherwise, the returned list may contain
     *  only those jobs that are accessible through this session.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs are
     *  returned.
     *
     *  @param resourceId a resource <code>Id</code>
     *  @return the returned <code>Job</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.JobList getJobsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.resourcing.job.JobProviderFilterList(getJobs(), resourceId));
    }


    /**
     *  Gets all <code>Jobs</code>.
     *
     *  In plenary mode, the returned list contains all known jobs or
     *  an error results. Otherwise, the returned list may contain
     *  only those jobs that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  In active mode, jobs are returned that are currently
     *  active. In any status mode, active and inactive jobs are
     *  returned.
     *
     *  @return a list of <code>Jobs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.resourcing.JobList getJobs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the job list for active and effective views. Should be
     *  called by <code>getObjects()</code> if no filtering is already
     *  performed.
     *
     *  @param list the list of jobs
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.resourcing.JobList filterJobsOnViews(org.osid.resourcing.JobList list)
        throws org.osid.OperationFailedException {

        org.osid.resourcing.JobList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.resourcing.job.ActiveJobFilterList(ret);
        }

        return (ret);
    }
}
