//
// AbstractShipment.java
//
//     Defines a Shipment builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.shipment.shipment.spi;


/**
 *  Defines a <code>Shipment</code> builder.
 */

public abstract class AbstractShipmentBuilder<T extends AbstractShipmentBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.inventory.shipment.shipment.ShipmentMiter shipment;


    /**
     *  Constructs a new <code>AbstractShipmentBuilder</code>.
     *
     *  @param shipment the shipment to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractShipmentBuilder(net.okapia.osid.jamocha.builder.inventory.shipment.shipment.ShipmentMiter shipment) {
        super(shipment);
        this.shipment = shipment;
        return;
    }


    /**
     *  Builds the shipment.
     *
     *  @return the new shipment
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.inventory.shipment.Shipment build() {
        (new net.okapia.osid.jamocha.builder.validator.inventory.shipment.shipment.ShipmentValidator(getValidations())).validate(this.shipment);
        return (new net.okapia.osid.jamocha.builder.inventory.shipment.shipment.ImmutableShipment(this.shipment));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the shipment miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.inventory.shipment.shipment.ShipmentMiter getMiter() {
        return (this.shipment);
    }


    /**
     *  Sets the source.
     *
     *  @param source a source
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>source</code> is
     *          <code>null</code>
     */

    public T source(org.osid.resource.Resource source) {
        getMiter().setSource(source);
        return (self());
    }


    /**
     *  Sets the order.
     *
     *  @param order an order
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>order</code> is
     *          <code>null</code>
     */

    public T order(org.osid.ordering.Order order) {
        getMiter().setOrder(order);
        return (self());
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T date(org.osid.calendaring.DateTime date) {
        getMiter().setDate(date);
        return (self());
    }


    /**
     *  Adds an entry.
     *
     *  @param entry an entry
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>entry</code> is
     *          <code>null</code>
     */

    public T entry(org.osid.inventory.shipment.Entry entry) {
        getMiter().addEntry(entry);
        return (self());
    }


    /**
     *  Sets all the entries.
     *
     *  @param entries a collection of entries
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>entries</code> is
     *          <code>null</code>
     */

    public T entries(java.util.Collection<org.osid.inventory.shipment.Entry> entries) {
        getMiter().setEntries(entries);
        return (self());
    }


    /**
     *  Adds a Shipment record.
     *
     *  @param record a shipment record
     *  @param recordType the type of shipment record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.inventory.shipment.records.ShipmentRecord record, org.osid.type.Type recordType) {
        getMiter().addShipmentRecord(record, recordType);
        return (self());
    }
}       


