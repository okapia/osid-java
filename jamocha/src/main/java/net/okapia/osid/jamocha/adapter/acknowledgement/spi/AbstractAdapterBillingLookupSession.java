//
// AbstractAdapterBillingLookupSession.java
//
//    A Billing lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Billing lookup session adapter.
 */

public abstract class AbstractAdapterBillingLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.acknowledgement.BillingLookupSession {

    private final org.osid.acknowledgement.BillingLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBillingLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBillingLookupSession(org.osid.acknowledgement.BillingLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Billing} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBillings() {
        return (this.session.canLookupBillings());
    }


    /**
     *  A complete view of the {@code Billing} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBillingView() {
        this.session.useComparativeBillingView();
        return;
    }


    /**
     *  A complete view of the {@code Billing} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBillingView() {
        this.session.usePlenaryBillingView();
        return;
    }

     
    /**
     *  Gets the {@code Billing} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Billing} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Billing} and
     *  retained for compatibility.
     *
     *  @param billingId {@code Id} of the {@code Billing}
     *  @return the billing
     *  @throws org.osid.NotFoundException {@code billingId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code billingId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Billing getBilling(org.osid.id.Id billingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBilling(billingId));
    }


    /**
     *  Gets a {@code BillingList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  billings specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Billings} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *
     *  @param  billingIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Billing} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code billingIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByIds(org.osid.id.IdList billingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBillingsByIds(billingIds));
    }


    /**
     *  Gets a {@code BillingList} corresponding to the given
     *  billing genus {@code Type} which does not include
     *  billings of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  billingGenusType a billing genus type 
     *  @return the returned {@code Billing} list
     *  @throws org.osid.NullArgumentException
     *          {@code billingGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByGenusType(org.osid.type.Type billingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBillingsByGenusType(billingGenusType));
    }


    /**
     *  Gets a {@code BillingList} corresponding to the given
     *  billing genus {@code Type} and include any additional
     *  billings with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  billingGenusType a billing genus type 
     *  @return the returned {@code Billing} list
     *  @throws org.osid.NullArgumentException
     *          {@code billingGenusType} is {@code }
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByParentGenusType(org.osid.type.Type billingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBillingsByParentGenusType(billingGenusType));
    }


    /**
     *  Gets a {@code BillingList} containing the given
     *  billing record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *
     *  @param  billingRecordType a billing record type 
     *  @return the returned {@code Billing} list
     *  @throws org.osid.NullArgumentException
     *          {@code billingRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByRecordType(org.osid.type.Type billingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBillingsByRecordType(billingRecordType));
    }


    /**
     *  Gets a {@code BillingList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Billing} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillingsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBillingsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Billings}. 
     *
     *  In plenary mode, the returned list contains all known
     *  billings or an error results. Otherwise, the returned list
     *  may contain only those billings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Billings} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.BillingList getBillings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBillings());
    }
}
