//
// AbstractBuildingSearchOdrer.java
//
//     Defines a BuildingSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.building.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code BuildingSearchOrder}.
 */

public abstract class AbstractBuildingSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectSearchOrder
    implements org.osid.room.BuildingSearchOrder {

    private final java.util.Collection<org.osid.room.records.BuildingSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the address. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAddress(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an address order is available. 
     *
     *  @return <code> true </code> if an address search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressSearchOrder() {
        return (false);
    }


    /**
     *  Gets the address search order. 
     *
     *  @return the address search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressSearchOrder getAddressSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAddressSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the offical 
     *  name. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOfficialName(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the building 
     *  number. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByNumber(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  subdivisions. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEnclosingBuilding(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an enclosing building search order is available. 
     *
     *  @return <code> true </code> if a building search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnclosingBuildingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the enclosing building search order. 
     *
     *  @return the building search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnclosingBuildingSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchOrder getEnclosingBuildingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsEnclosingBuildingSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the gross area. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGrossArea(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  buildingRecordType a building record type 
     *  @return {@code true} if the buildingRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code buildingRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type buildingRecordType) {
        for (org.osid.room.records.BuildingSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(buildingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  buildingRecordType the building record type 
     *  @return the building search order record
     *  @throws org.osid.NullArgumentException
     *          {@code buildingRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(buildingRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.room.records.BuildingSearchOrderRecord getBuildingSearchOrderRecord(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.BuildingSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(buildingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(buildingRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this building. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param buildingRecord the building search odrer record
     *  @param buildingRecordType building record type
     *  @throws org.osid.NullArgumentException
     *          {@code buildingRecord} or
     *          {@code buildingRecordTypebuilding} is
     *          {@code null}
     */
            
    protected void addBuildingRecord(org.osid.room.records.BuildingSearchOrderRecord buildingSearchOrderRecord, 
                                     org.osid.type.Type buildingRecordType) {

        addRecordType(buildingRecordType);
        this.records.add(buildingSearchOrderRecord);
        
        return;
    }
}
