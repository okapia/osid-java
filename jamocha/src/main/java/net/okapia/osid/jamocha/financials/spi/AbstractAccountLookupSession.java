//
// AbstractAccountLookupSession.java
//
//    A starter implementation framework for providing an Account
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an Account
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAccounts(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAccountLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.financials.AccountLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.financials.Business business = new net.okapia.osid.jamocha.nil.financials.business.UnknownBusiness();
    
    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.financials.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Account</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAccounts() {
        return (true);
    }


    /**
     *  A complete view of the <code>Account</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAccountView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Account</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAccountView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include accounts in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Account</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Account</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Account</code> and
     *  retained for compatibility.
     *
     *  @param  accountId <code>Id</code> of the
     *          <code>Account</code>
     *  @return the account
     *  @throws org.osid.NotFoundException <code>accountId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>accountId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.financials.AccountList accounts = getAccounts()) {
            while (accounts.hasNext()) {
                org.osid.financials.Account account = accounts.getNextAccount();
                if (account.getId().equals(accountId)) {
                    return (account);
                }
            }
        } 

        throw new org.osid.NotFoundException(accountId + " not found");
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  accounts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Accounts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAccounts()</code>.
     *
     *  @param  accountIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>accountIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByIds(org.osid.id.IdList accountIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.financials.Account> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = accountIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAccount(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("account " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.financials.account.LinkedAccountList(ret));
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  account genus <code>Type</code> which does not include
     *  accounts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAccounts()</code>.
     *
     *  @param  accountGenusType an account genus type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByGenusType(org.osid.type.Type accountGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.account.AccountGenusFilterList(getAccounts(), accountGenusType));
    }


    /**
     *  Gets an <code>AccountList</code> corresponding to the given
     *  account genus <code>Type</code> and include any additional
     *  accounts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known accounts
     *  or an error results. Otherwise, the returned list may contain
     *  only those accounts that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAccounts()</code>.
     *
     *  @param  accountGenusType an account genus type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByParentGenusType(org.osid.type.Type accountGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAccountsByGenusType(accountGenusType));
    }


    /**
     *  Gets an <code>AccountList</code> containing the given
     *  account record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAccounts()</code>.
     *
     *  @param  accountRecordType an account record type 
     *  @return the returned <code>Account</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByRecordType(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.account.AccountRecordFilterList(getAccounts(), accountRecordType));
    }


    /**
     *  Gets an <code> AccountList </code> associated with the given
     *  code. In plenary mode, the returned list contains all
     *  known accounts or an error results. Otherwise, the returned
     *  list may contain only those accounts that are accessible
     *  through this session.
     *
     *  @param  code an account code
     *  @return the returned <code> Account </code> list 
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.AccountList getAccountsByCode(String code)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.financials.account.AccountFilterList(new CodeFilter(code), getAccounts()));
    }


    /**
     *  Gets all <code>Accounts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  accounts or an error results. Otherwise, the returned list
     *  may contain only those accounts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Accounts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.financials.AccountList getAccounts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the account list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of accounts
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.financials.AccountList filterAccountsOnViews(org.osid.financials.AccountList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class CodeFilter
        implements net.okapia.osid.jamocha.inline.filter.financials.account.AccountFilter {

        private final String code;

        
        /**
         *  Constructs a new <code>CodeFilter</code>.
         *
         *  @param code the code to filter
         *  @throws org.osid.NullArgumentException <code>code</code>
         *          is <code>null</code>
         */

        public CodeFilter(String code) {
            nullarg(code, "code");
            this.code = code;
            return;
        }


        /**
         *  Used by the AccountFilterList to filter the account list
         *  based on code.
         *
         *  @param account the account
         *  @return <code>true</code> to pass the account,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.financials.Account account) {
            return (account.getCode().equals(this.code));
        }
    }
}
