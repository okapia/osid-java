//
// AbstractActivityBundle.java
//
//     Defines an ActivityBundle builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.activitybundle.spi;


/**
 *  Defines an <code>ActivityBundle</code> builder.
 */

public abstract class AbstractActivityBundleBuilder<T extends AbstractActivityBundleBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.registration.activitybundle.ActivityBundleMiter activityBundle;


    /**
     *  Constructs a new <code>AbstractActivityBundleBuilder</code>.
     *
     *  @param activityBundle the activity bundle to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractActivityBundleBuilder(net.okapia.osid.jamocha.builder.course.registration.activitybundle.ActivityBundleMiter activityBundle) {
        super(activityBundle);
        this.activityBundle = activityBundle;
        return;
    }


    /**
     *  Builds the activity bundle.
     *
     *  @return the new activity bundle
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.registration.ActivityBundle build() {
        (new net.okapia.osid.jamocha.builder.validator.course.registration.activitybundle.ActivityBundleValidator(getValidations())).validate(this.activityBundle);
        return (new net.okapia.osid.jamocha.builder.course.registration.activitybundle.ImmutableActivityBundle(this.activityBundle));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the activity bundle miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.registration.activitybundle.ActivityBundleMiter getMiter() {
        return (this.activityBundle);
    }


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    public T courseOffering(org.osid.course.CourseOffering courseOffering) {
        getMiter().setCourseOffering(courseOffering);
        return (self());
    }


    /**
     *  Adds an activity.
     *
     *  @param activity an activity
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public T activity(org.osid.course.Activity activity) {
        getMiter().addActivity(activity);
        return (self());
    }


    /**
     *  Sets all the activities.
     *
     *  @param activities a collection of activities
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    public T activities(java.util.Collection<org.osid.course.Activity> activities) {
        getMiter().setActivities(activities);
        return (self());
    }



    /**
     *  Adds a credit option.
     *
     *  @param credits a credit option to add
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public T credits(java.math.BigDecimal credits) {
        getMiter().addCredits(credits);
        return (self());
    }


    /**
     *  Sets all the credit options.
     *
     *  @param credits the credit options
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public T credits(java.util.Collection<java.math.BigDecimal> credits) {
        getMiter().setCredits(credits);
        return (self());
    }


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public T gradingOption(org.osid.grading.GradeSystem option) {
        getMiter().addGradingOption(option);
        return (self());
    }


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public T gradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options) {
        getMiter().setGradingOptions(options);
        return (self());
    }


    /**
     *  Adds an ActivityBundle record.
     *
     *  @param record an activity bundle record
     *  @param recordType the type of activity bundle record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.registration.records.ActivityBundleRecord record, org.osid.type.Type recordType) {
        getMiter().addActivityBundleRecord(record, recordType);
        return (self());
    }
}       


