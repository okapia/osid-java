//
// AbstractAssemblyJournalEntryQuery.java
//
//     A JournalEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.journaling.journalentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A JournalEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyJournalEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.journaling.JournalEntryQuery,
               org.osid.journaling.JournalEntryQueryInspector,
               org.osid.journaling.JournalEntrySearchOrder {

    private final java.util.Collection<org.osid.journaling.records.JournalEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.journaling.records.JournalEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.journaling.records.JournalEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyJournalEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyJournalEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the branch <code> Id </code> for this query to match branches 
     *  assigned to journals. 
     *
     *  @param  branchId a branch <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> branchId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBranchId(org.osid.id.Id branchId, boolean match) {
        getAssembler().addIdTerm(getBranchIdColumn(), branchId, match);
        return;
    }


    /**
     *  Clears the branch <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBranchIdTerms() {
        getAssembler().clearTerms(getBranchIdColumn());
        return;
    }


    /**
     *  Gets the branch <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBranchIdTerms() {
        return (getAssembler().getIdTerms(getBranchIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the branch. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBranch(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBranchColumn(), style);
        return;
    }


    /**
     *  Gets the BranchId column name.
     *
     * @return the column name
     */

    protected String getBranchIdColumn() {
        return ("branch_id");
    }


    /**
     *  Tests if a branch query is available. 
     *
     *  @return <code> true </code> if a branch query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchQuery() {
        return (false);
    }


    /**
     *  Gets the query for a branch. 
     *
     *  @return the branch query 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuery getBranchQuery() {
        throw new org.osid.UnimplementedException("supportsBranchQuery() is false");
    }


    /**
     *  Clears the branch terms. 
     */

    @OSID @Override
    public void clearBranchTerms() {
        getAssembler().clearTerms(getBranchColumn());
        return;
    }


    /**
     *  Gets the branch query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.BranchQueryInspector[] getBranchTerms() {
        return (new org.osid.journaling.BranchQueryInspector[0]);
    }


    /**
     *  Tests if a branch search order is available. 
     *
     *  @return <code> true </code> if a branch order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchSearchOrder() {
        return (false);
    }


    /**
     *  Gets the branch search order. 
     *
     *  @return the branch search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSearchOrder getBranchSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBranchSearchOrder() is false");
    }


    /**
     *  Gets the Branch column name.
     *
     * @return the column name
     */

    protected String getBranchColumn() {
        return ("branch");
    }


    /**
     *  Sets the source <code> Id. </code> 
     *
     *  @param  sourceId a source <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSourceId(org.osid.id.Id sourceId, boolean match) {
        getAssembler().addIdTerm(getSourceIdColumn(), sourceId, match);
        return;
    }


    /**
     *  Clears the source <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSourceIdTerms() {
        getAssembler().clearTerms(getSourceIdColumn());
        return;
    }


    /**
     *  Gets the source <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSourceIdTerms() {
        return (getAssembler().getIdTerms(getSourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the source. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSourceIdColumn(), style);
        return;
    }


    /**
     *  Gets the SourceId column name.
     *
     * @return the column name
     */

    protected String getSourceIdColumn() {
        return ("source_id");
    }


    /**
     *  Sets the version <code> Id. </code> 
     *
     *  @param  versionId a version <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> versionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVersionId(org.osid.id.Id versionId, boolean match) {
        getAssembler().addIdTerm(getVersionIdColumn(), versionId, match);
        return;
    }


    /**
     *  Clears the version <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearVersionIdTerms() {
        getAssembler().clearTerms(getVersionIdColumn());
        return;
    }


    /**
     *  Gets the version <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getVersionIdTerms() {
        return (getAssembler().getIdTerms(getVersionIdColumn()));
    }


    /**
     *  Gets the VersionId column name.
     *
     * @return the column name
     */

    protected String getVersionIdColumn() {
        return ("version_id");
    }


    /**
     *  Matches entries falling between the given times inclusive. 
     *
     *  @param  from start time 
     *  @param  to end time 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTimestamp(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getTimestampColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the timestamp terms. 
     */

    @OSID @Override
    public void clearTimestampTerms() {
        getAssembler().clearTerms(getTimestampColumn());
        return;
    }


    /**
     *  Gets the timestamp query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getTimestampTerms() {
        return (getAssembler().getDateTimeRangeTerms(getTimestampColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the timestamp. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTimestamp(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTimestampColumn(), style);
        return;
    }


    /**
     *  Gets the Timestamp column name.
     *
     * @return the column name
     */

    protected String getTimestampColumn() {
        return ("timestamp");
    }


    /**
     *  Matches entries following the given timestamp inclusive. 
     *
     *  @param  from start time 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> from </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEntriesSince(org.osid.calendaring.DateTime from, 
                                  boolean match) {
        getAssembler().addDateTimeTerm(getEntriesSinceColumn(), from, match);
        return;
    }


    /**
     *  Clears the entries since terms. 
     */

    @OSID @Override
    public void clearEntriesSinceTerms() {
        getAssembler().clearTerms(getEntriesSinceColumn());
        return;
    }


    /**
     *  Gets the entries since query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeTerm[] getEntriesSinceTerms() {
        return (getAssembler().getDateTimeTerms(getEntriesSinceColumn()));
    }


    /**
     *  Gets the EntriesSince column name.
     *
     * @return the column name
     */

    protected String getEntriesSinceColumn() {
        return ("entries_since");
    }


    /**
     *  Sets a resource <code> Id. </code> 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getResourceIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        getAssembler().clearTerms(getResourceIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (getAssembler().getIdTerms(getResourceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getResourceColumn(), style);
        return;
    }


    /**
     *  Gets the ResourceId column name.
     *
     * @return the column name
     */

    protected String getResourceIdColumn() {
        return ("resource_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        getAssembler().clearTerms(getResourceColumn());
        return;
    }


    /**
     *  Gets the resource query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Gets the Resource column name.
     *
     * @return the column name
     */

    protected String getResourceColumn() {
        return ("resource");
    }


    /**
     *  Sets an agent <code> Id. </code> 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAgentColumn(), style);
        return;
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent order is available. 
     *
     *  @return <code> true </code> if an agent order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Sets the journal <code> Id </code> for this query to match entries 
     *  assigned to journals. 
     *
     *  @param  journalId a journal <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchJournalId(org.osid.id.Id journalId, boolean match) {
        getAssembler().addIdTerm(getJournalIdColumn(), journalId, match);
        return;
    }


    /**
     *  Clears the journal <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearJournalIdTerms() {
        getAssembler().clearTerms(getJournalIdColumn());
        return;
    }


    /**
     *  Gets the journal <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getJournalIdTerms() {
        return (getAssembler().getIdTerms(getJournalIdColumn()));
    }


    /**
     *  Gets the JournalId column name.
     *
     * @return the column name
     */

    protected String getJournalIdColumn() {
        return ("journal_id");
    }


    /**
     *  Tests if a <code> JournalQuery </code> is available. 
     *
     *  @return <code> true </code> if a journal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalQuery() {
        return (false);
    }


    /**
     *  Gets the query for a journal query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the journal query 
     *  @throws org.osid.UnimplementedException <code> supportsJournalQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalQuery getJournalQuery() {
        throw new org.osid.UnimplementedException("supportsJournalQuery() is false");
    }


    /**
     *  Clears the journal terms. 
     */

    @OSID @Override
    public void clearJournalTerms() {
        getAssembler().clearTerms(getJournalColumn());
        return;
    }


    /**
     *  Gets the journal query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.journaling.JournalQueryInspector[] getJournalTerms() {
        return (new org.osid.journaling.JournalQueryInspector[0]);
    }


    /**
     *  Gets the Journal column name.
     *
     * @return the column name
     */

    protected String getJournalColumn() {
        return ("journal");
    }


    /**
     *  Tests if this journalEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  journalEntryRecordType a journal entry record type 
     *  @return <code>true</code> if the journalEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type journalEntryRecordType) {
        for (org.osid.journaling.records.JournalEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(journalEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  journalEntryRecordType the journal entry record type 
     *  @return the journal entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalEntryQueryRecord getJournalEntryQueryRecord(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(journalEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  journalEntryRecordType the journal entry record type 
     *  @return the journal entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalEntryQueryInspectorRecord getJournalEntryQueryInspectorRecord(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(journalEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param journalEntryRecordType the journal entry record type
     *  @return the journal entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(journalEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.journaling.records.JournalEntrySearchOrderRecord getJournalEntrySearchOrderRecord(org.osid.type.Type journalEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.journaling.records.JournalEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(journalEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(journalEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this journal entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param journalEntryQueryRecord the journal entry query record
     *  @param journalEntryQueryInspectorRecord the journal entry query inspector
     *         record
     *  @param journalEntrySearchOrderRecord the journal entry search order record
     *  @param journalEntryRecordType journal entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>journalEntryQueryRecord</code>,
     *          <code>journalEntryQueryInspectorRecord</code>,
     *          <code>journalEntrySearchOrderRecord</code> or
     *          <code>journalEntryRecordTypejournalEntry</code> is
     *          <code>null</code>
     */
            
    protected void addJournalEntryRecords(org.osid.journaling.records.JournalEntryQueryRecord journalEntryQueryRecord, 
                                      org.osid.journaling.records.JournalEntryQueryInspectorRecord journalEntryQueryInspectorRecord, 
                                      org.osid.journaling.records.JournalEntrySearchOrderRecord journalEntrySearchOrderRecord, 
                                      org.osid.type.Type journalEntryRecordType) {

        addRecordType(journalEntryRecordType);

        nullarg(journalEntryQueryRecord, "journal entry query record");
        nullarg(journalEntryQueryInspectorRecord, "journal entry query inspector record");
        nullarg(journalEntrySearchOrderRecord, "journal entry search odrer record");

        this.queryRecords.add(journalEntryQueryRecord);
        this.queryInspectorRecords.add(journalEntryQueryInspectorRecord);
        this.searchOrderRecords.add(journalEntrySearchOrderRecord);
        
        return;
    }
}
