//
// AbstractProvisioningRulesProxyManager.java
//
//     An adapter for a ProvisioningRulesProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ProvisioningRulesProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterProvisioningRulesProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.provisioning.rules.ProvisioningRulesProxyManager>
    implements org.osid.provisioning.rules.ProvisioningRulesProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterProvisioningRulesProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterProvisioningRulesProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterProvisioningRulesProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterProvisioningRulesProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up queue constrainer is supported. 
     *
     *  @return <code> true </code> if queue constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerLookup() {
        return (getAdapteeManager().supportsQueueConstrainerLookup());
    }


    /**
     *  Tests if querying queue constrainer is supported. 
     *
     *  @return <code> true </code> if queue constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerQuery() {
        return (getAdapteeManager().supportsQueueConstrainerQuery());
    }


    /**
     *  Tests if searching queue constrainer is supported. 
     *
     *  @return <code> true </code> if queue constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerSearch() {
        return (getAdapteeManager().supportsQueueConstrainerSearch());
    }


    /**
     *  Tests if a queue constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if queue constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerAdmin() {
        return (getAdapteeManager().supportsQueueConstrainerAdmin());
    }


    /**
     *  Tests if a queue constrainer notification service is supported. 
     *
     *  @return <code> true </code> if queue constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerNotification() {
        return (getAdapteeManager().supportsQueueConstrainerNotification());
    }


    /**
     *  Tests if a queue constrainer distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer distributor lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerDistributor() {
        return (getAdapteeManager().supportsQueueConstrainerDistributor());
    }


    /**
     *  Tests if a queue constrainer distributor service is supported. 
     *
     *  @return <code> true </code> if queue constrainer distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerDistributorAssignment() {
        return (getAdapteeManager().supportsQueueConstrainerDistributorAssignment());
    }


    /**
     *  Tests if a queue constrainer distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer distributor service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerSmartDistributor() {
        return (getAdapteeManager().supportsQueueConstrainerSmartDistributor());
    }


    /**
     *  Tests if a queue constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerRuleLookup() {
        return (getAdapteeManager().supportsQueueConstrainerRuleLookup());
    }


    /**
     *  Tests if a queue constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerRuleApplication() {
        return (getAdapteeManager().supportsQueueConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up queue constrainer enablers is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying queue constrainer enablers is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching queue constrainer enablers is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerSearch());
    }


    /**
     *  Tests if a queue constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerAdmin());
    }


    /**
     *  Tests if a queue constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerNotification());
    }


    /**
     *  Tests if a queue constrainer enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue constrainer enabler distributor 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerDistributor() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerDistributor());
    }


    /**
     *  Tests if a queue constrainer enabler distributor service is supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerDistributorAssignment() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerDistributorAssignment());
    }


    /**
     *  Tests if a queue constrainer enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue constrainer enabler distributor 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerSmartDistributor() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerSmartDistributor());
    }


    /**
     *  Tests if a queue constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a queue constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if a queue constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsQueueConstrainerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up queue processor is supported. 
     *
     *  @return <code> true </code> if queue processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorLookup() {
        return (getAdapteeManager().supportsQueueProcessorLookup());
    }


    /**
     *  Tests if querying queue processor is supported. 
     *
     *  @return <code> true </code> if queue processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorQuery() {
        return (getAdapteeManager().supportsQueueProcessorQuery());
    }


    /**
     *  Tests if searching queue processor is supported. 
     *
     *  @return <code> true </code> if queue processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorSearch() {
        return (getAdapteeManager().supportsQueueProcessorSearch());
    }


    /**
     *  Tests if a queue processor administrative service is supported. 
     *
     *  @return <code> true </code> if queue processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorAdmin() {
        return (getAdapteeManager().supportsQueueProcessorAdmin());
    }


    /**
     *  Tests if a queue processor notification service is supported. 
     *
     *  @return <code> true </code> if queue processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorNotification() {
        return (getAdapteeManager().supportsQueueProcessorNotification());
    }


    /**
     *  Tests if a queue processor distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a queue processor distributor lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorDistributor() {
        return (getAdapteeManager().supportsQueueProcessorDistributor());
    }


    /**
     *  Tests if a queue processor distributor service is supported. 
     *
     *  @return <code> true </code> if queue processor distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorDistributorAssignment() {
        return (getAdapteeManager().supportsQueueProcessorDistributorAssignment());
    }


    /**
     *  Tests if a queue processor distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a queue processor distributor service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorSmartDistributor() {
        return (getAdapteeManager().supportsQueueProcessorSmartDistributor());
    }


    /**
     *  Tests if a queue processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a queue processor rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorRuleLookup() {
        return (getAdapteeManager().supportsQueueProcessorRuleLookup());
    }


    /**
     *  Tests if a queue processor rule application service is supported. 
     *
     *  @return <code> true </code> if queue processor rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorRuleApplication() {
        return (getAdapteeManager().supportsQueueProcessorRuleApplication());
    }


    /**
     *  Tests if looking up queue processor enablers is supported. 
     *
     *  @return <code> true </code> if queue processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerLookup() {
        return (getAdapteeManager().supportsQueueProcessorEnablerLookup());
    }


    /**
     *  Tests if querying queue processor enablers is supported. 
     *
     *  @return <code> true </code> if queue processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerQuery() {
        return (getAdapteeManager().supportsQueueProcessorEnablerQuery());
    }


    /**
     *  Tests if searching queue processor enablers is supported. 
     *
     *  @return <code> true </code> if queue processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerSearch() {
        return (getAdapteeManager().supportsQueueProcessorEnablerSearch());
    }


    /**
     *  Tests if a queue processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue processor enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsQueueProcessorEnablerAdmin());
    }


    /**
     *  Tests if a queue processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if queue processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerNotification() {
        return (getAdapteeManager().supportsQueueProcessorEnablerNotification());
    }


    /**
     *  Tests if a queue processor enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue processor enabler distributor 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerDistributor() {
        return (getAdapteeManager().supportsQueueProcessorEnablerDistributor());
    }


    /**
     *  Tests if a queue processor enabler distributor service is supported. 
     *
     *  @return <code> true </code> if queue processor enabler distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerDistributorAssignment() {
        return (getAdapteeManager().supportsQueueProcessorEnablerDistributorAssignment());
    }


    /**
     *  Tests if a queue processor enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a queue processor enabler distributor 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerSmartDistributor() {
        return (getAdapteeManager().supportsQueueProcessorEnablerSmartDistributor());
    }


    /**
     *  Tests if a queue processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsQueueProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if a queue processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if queue processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsQueueProcessorEnablerRuleApplication());
    }


    /**
     *  Tests if looking up pool constrainer is supported. 
     *
     *  @return <code> true </code> if pool constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerLookup() {
        return (getAdapteeManager().supportsPoolConstrainerLookup());
    }


    /**
     *  Tests if querying pool constrainer is supported. 
     *
     *  @return <code> true </code> if pool constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerQuery() {
        return (getAdapteeManager().supportsPoolConstrainerQuery());
    }


    /**
     *  Tests if searching pool constrainer is supported. 
     *
     *  @return <code> true </code> if pool constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerSearch() {
        return (getAdapteeManager().supportsPoolConstrainerSearch());
    }


    /**
     *  Tests if a pool constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if pool constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerAdmin() {
        return (getAdapteeManager().supportsPoolConstrainerAdmin());
    }


    /**
     *  Tests if a pool constrainer notification service is supported. 
     *
     *  @return <code> true </code> if pool constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerNotification() {
        return (getAdapteeManager().supportsPoolConstrainerNotification());
    }


    /**
     *  Tests if a pool constrainer distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a pool constrainer distributor lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerDistributor() {
        return (getAdapteeManager().supportsPoolConstrainerDistributor());
    }


    /**
     *  Tests if a pool constrainer distributor service is supported. 
     *
     *  @return <code> true </code> if pool constrainer distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerDistributorAssignment() {
        return (getAdapteeManager().supportsPoolConstrainerDistributorAssignment());
    }


    /**
     *  Tests if a pool constrainer distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a pool constrainer distributor service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerSmartDistributor() {
        return (getAdapteeManager().supportsPoolConstrainerSmartDistributor());
    }


    /**
     *  Tests if a pool constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a pool constrainer rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerRuleLookup() {
        return (getAdapteeManager().supportsPoolConstrainerRuleLookup());
    }


    /**
     *  Tests if a pool constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a pool constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerRuleApplication() {
        return (getAdapteeManager().supportsPoolConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up pool constrainer enablers is supported. 
     *
     *  @return <code> true </code> if pool constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying pool constrainer enablers is supported. 
     *
     *  @return <code> true </code> if pool constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching pool constrainer enablers is supported. 
     *
     *  @return <code> true </code> if pool constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerSearch());
    }


    /**
     *  Tests if a pool constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if pool constrainer enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerAdmin());
    }


    /**
     *  Tests if a pool constrainer enabler notification service is supported. 
     *
     *  @return <code> true </code> if pool constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerNotification());
    }


    /**
     *  Tests if a pool constrainer enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a pool constrainer enabler distributor 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerDistributor() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerDistributor());
    }


    /**
     *  Tests if a pool constrainer enabler distributor service is supported. 
     *
     *  @return <code> true </code> if pool constrainer enabler distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerDistributorAssignment() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerDistributorAssignment());
    }


    /**
     *  Tests if a pool constrainer enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a pool constrainer enabler distributor 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerSmartDistributor() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerSmartDistributor());
    }


    /**
     *  Tests if a pool constrainer enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a pool constrainer enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if a pool constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if pool constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsPoolConstrainerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up pool processor is supported. 
     *
     *  @return <code> true </code> if pool processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorLookup() {
        return (getAdapteeManager().supportsPoolProcessorLookup());
    }


    /**
     *  Tests if querying pool processor is supported. 
     *
     *  @return <code> true </code> if pool processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorQuery() {
        return (getAdapteeManager().supportsPoolProcessorQuery());
    }


    /**
     *  Tests if searching pool processor is supported. 
     *
     *  @return <code> true </code> if pool processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorSearch() {
        return (getAdapteeManager().supportsPoolProcessorSearch());
    }


    /**
     *  Tests if a pool processor administrative service is supported. 
     *
     *  @return <code> true </code> if pool processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorAdmin() {
        return (getAdapteeManager().supportsPoolProcessorAdmin());
    }


    /**
     *  Tests if a pool processor notification service is supported. 
     *
     *  @return <code> true </code> if pool processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorNotification() {
        return (getAdapteeManager().supportsPoolProcessorNotification());
    }


    /**
     *  Tests if a pool processor distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a pool processor distributor lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorDistributor() {
        return (getAdapteeManager().supportsPoolProcessorDistributor());
    }


    /**
     *  Tests if a pool processor distributor service is supported. 
     *
     *  @return <code> true </code> if pool processor distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorDistributorAssignment() {
        return (getAdapteeManager().supportsPoolProcessorDistributorAssignment());
    }


    /**
     *  Tests if a pool processor distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a pool processor distributor service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorSmartDistributor() {
        return (getAdapteeManager().supportsPoolProcessorSmartDistributor());
    }


    /**
     *  Tests if a pool processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a pool processor rule lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorRuleLookup() {
        return (getAdapteeManager().supportsPoolProcessorRuleLookup());
    }


    /**
     *  Tests if a pool processor rule application service is supported. 
     *
     *  @return <code> true </code> if pool processor rule application service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorRuleApplication() {
        return (getAdapteeManager().supportsPoolProcessorRuleApplication());
    }


    /**
     *  Tests if looking up pool processor enablers is supported. 
     *
     *  @return <code> true </code> if pool processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerLookup() {
        return (getAdapteeManager().supportsPoolProcessorEnablerLookup());
    }


    /**
     *  Tests if querying pool processor enablers is supported. 
     *
     *  @return <code> true </code> if pool processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerQuery() {
        return (getAdapteeManager().supportsPoolProcessorEnablerQuery());
    }


    /**
     *  Tests if searching pool processor enablers is supported. 
     *
     *  @return <code> true </code> if pool processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerSearch() {
        return (getAdapteeManager().supportsPoolProcessorEnablerSearch());
    }


    /**
     *  Tests if a pool processor enabler administrative service is supported. 
     *
     *  @return <code> true </code> if pool processor enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsPoolProcessorEnablerAdmin());
    }


    /**
     *  Tests if a pool processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if pool processor enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerNotification() {
        return (getAdapteeManager().supportsPoolProcessorEnablerNotification());
    }


    /**
     *  Tests if a pool processor enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a pool processor enabler distributor 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerDistributor() {
        return (getAdapteeManager().supportsPoolProcessorEnablerDistributor());
    }


    /**
     *  Tests if a pool processor enabler distributor service is supported. 
     *
     *  @return <code> true </code> if pool processor enabler distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerDistributorAssignment() {
        return (getAdapteeManager().supportsPoolProcessorEnablerDistributorAssignment());
    }


    /**
     *  Tests if a pool processor enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a pool processor enabler distributor 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerSmartDistributor() {
        return (getAdapteeManager().supportsPoolProcessorEnablerSmartDistributor());
    }


    /**
     *  Tests if a pool processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a processor enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsPoolProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if a pool processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if pool processor enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsPoolProcessorEnablerRuleApplication());
    }


    /**
     *  Tests if looking up broker constrainer is supported. 
     *
     *  @return <code> true </code> if broker constrainer lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerLookup() {
        return (getAdapteeManager().supportsBrokerConstrainerLookup());
    }


    /**
     *  Tests if querying broker constrainer is supported. 
     *
     *  @return <code> true </code> if broker constrainer query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerQuery() {
        return (getAdapteeManager().supportsBrokerConstrainerQuery());
    }


    /**
     *  Tests if searching broker constrainer is supported. 
     *
     *  @return <code> true </code> if broker constrainer search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerSearch() {
        return (getAdapteeManager().supportsBrokerConstrainerSearch());
    }


    /**
     *  Tests if a broker constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if broker constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerAdmin() {
        return (getAdapteeManager().supportsBrokerConstrainerAdmin());
    }


    /**
     *  Tests if a broker constrainer notification service is supported. 
     *
     *  @return <code> true </code> if broker constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerNotification() {
        return (getAdapteeManager().supportsBrokerConstrainerNotification());
    }


    /**
     *  Tests if a broker constrainer distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a broker constrainer distributor lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerDistributor() {
        return (getAdapteeManager().supportsBrokerConstrainerDistributor());
    }


    /**
     *  Tests if a broker constrainer distributor service is supported. 
     *
     *  @return <code> true </code> if broker constrainer distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerDistributorAssignment() {
        return (getAdapteeManager().supportsBrokerConstrainerDistributorAssignment());
    }


    /**
     *  Tests if a broker constrainer distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a broker constrainer distributor 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerSmartDistributor() {
        return (getAdapteeManager().supportsBrokerConstrainerSmartDistributor());
    }


    /**
     *  Tests if a broker constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if a broker constrainer rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerRuleLookup() {
        return (getAdapteeManager().supportsBrokerConstrainerRuleLookup());
    }


    /**
     *  Tests if a broker constrainer rule application service is supported. 
     *
     *  @return <code> true </code> if a broker constrainer rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerRuleApplication() {
        return (getAdapteeManager().supportsBrokerConstrainerRuleApplication());
    }


    /**
     *  Tests if looking up broker constrainer enablers is supported. 
     *
     *  @return <code> true </code> if broker constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerLookup() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerLookup());
    }


    /**
     *  Tests if querying broker constrainer enablers is supported. 
     *
     *  @return <code> true </code> if broker constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerQuery() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerQuery());
    }


    /**
     *  Tests if searching broker constrainer enablers is supported. 
     *
     *  @return <code> true </code> if broker constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerSearch() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerSearch());
    }


    /**
     *  Tests if a broker constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if broker constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerAdmin() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerAdmin());
    }


    /**
     *  Tests if a broker constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if broker constrainer enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerNotification() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerNotification());
    }


    /**
     *  Tests if a broker constrainer enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a broker constrainer enabler 
     *          distributor lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerDistributor() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerDistributor());
    }


    /**
     *  Tests if a broker constrainer enabler distributor service is 
     *  supported. 
     *
     *  @return <code> true </code> if broker constrainer enabler distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerDistributorAssignment() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerDistributorAssignment());
    }


    /**
     *  Tests if a broker constrainer enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a broker constrainer enabler 
     *          distributor service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerSmartDistributor() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerSmartDistributor());
    }


    /**
     *  Tests if a broker constrainer enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a broker constrainer enabler rule 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerRuleLookup() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerRuleLookup());
    }


    /**
     *  Tests if a broker constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if broker constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerRuleApplication() {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerRuleApplication());
    }


    /**
     *  Tests if looking up broker processor is supported. 
     *
     *  @return <code> true </code> if broker processor lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorLookup() {
        return (getAdapteeManager().supportsBrokerProcessorLookup());
    }


    /**
     *  Tests if querying broker processor is supported. 
     *
     *  @return <code> true </code> if broker processor query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorQuery() {
        return (getAdapteeManager().supportsBrokerProcessorQuery());
    }


    /**
     *  Tests if searching broker processor is supported. 
     *
     *  @return <code> true </code> if broker processor search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorSearch() {
        return (getAdapteeManager().supportsBrokerProcessorSearch());
    }


    /**
     *  Tests if a broker processor administrative service is supported. 
     *
     *  @return <code> true </code> if broker processor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorAdmin() {
        return (getAdapteeManager().supportsBrokerProcessorAdmin());
    }


    /**
     *  Tests if a broker processor notification service is supported. 
     *
     *  @return <code> true </code> if broker processor notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorNotification() {
        return (getAdapteeManager().supportsBrokerProcessorNotification());
    }


    /**
     *  Tests if a broker processor distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a broker processor distributor lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorDistributor() {
        return (getAdapteeManager().supportsBrokerProcessorDistributor());
    }


    /**
     *  Tests if a broker processor distributor service is supported. 
     *
     *  @return <code> true </code> if broker processor distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorDistributorAssignment() {
        return (getAdapteeManager().supportsBrokerProcessorDistributorAssignment());
    }


    /**
     *  Tests if a broker processor distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a broker processor distributor service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorSmartDistributor() {
        return (getAdapteeManager().supportsBrokerProcessorSmartDistributor());
    }


    /**
     *  Tests if a broker processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a broker processor rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorRuleLookup() {
        return (getAdapteeManager().supportsBrokerProcessorRuleLookup());
    }


    /**
     *  Tests if a broker processor rule application service is supported. 
     *
     *  @return <code> true </code> if broker processor rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorRuleApplication() {
        return (getAdapteeManager().supportsBrokerProcessorRuleApplication());
    }


    /**
     *  Tests if looking up broker processor enablers is supported. 
     *
     *  @return <code> true </code> if broker processor enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerLookup() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerLookup());
    }


    /**
     *  Tests if querying broker processor enablers is supported. 
     *
     *  @return <code> true </code> if broker processor enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerQuery() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerQuery());
    }


    /**
     *  Tests if searching broker processor enablers is supported. 
     *
     *  @return <code> true </code> if broker processor enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerSearch() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerSearch());
    }


    /**
     *  Tests if a broker processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if broker processor enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerAdmin() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerAdmin());
    }


    /**
     *  Tests if a broker processor enabler notification service is supported. 
     *
     *  @return <code> true </code> if broker processor enabler notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerNotification() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerNotification());
    }


    /**
     *  Tests if a broker processor enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a broker processor enabler distributor 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerDistributor() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerDistributor());
    }


    /**
     *  Tests if a broker processor enabler distributor service is supported. 
     *
     *  @return <code> true </code> if broker processor enabler distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerDistributorAssignment() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerDistributorAssignment());
    }


    /**
     *  Tests if a broker processor enabler distributor lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a broker processor enabler distributor 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerSmartDistributor() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerSmartDistributor());
    }


    /**
     *  Tests if a broker processor enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerRuleLookup() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerRuleLookup());
    }


    /**
     *  Tests if a broker processor enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if broker processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerRuleApplication() {
        return (getAdapteeManager().supportsBrokerProcessorEnablerRuleApplication());
    }


    /**
     *  Gets the supported <code> QueueConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> QueueConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerRecordTypes() {
        return (getAdapteeManager().getQueueConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  queueConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> QueueConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerRecordType(org.osid.type.Type queueConstrainerRecordType) {
        return (getAdapteeManager().supportsQueueConstrainerRecordType(queueConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> QueueConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> QueueConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getQueueConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  queueConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerSearchRecordType(org.osid.type.Type queueConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsQueueConstrainerSearchRecordType(queueConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> QueueConstrainerEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> QueueConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getQueueConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueConstrainerEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  queueConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> QueueConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerRecordType(org.osid.type.Type queueConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsQueueConstrainerEnablerRecordType(queueConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> QueueConstrainerEnabler </code> search 
     *  record interface types. 
     *
     *  @return a list containing the supported <code> QueueConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getQueueConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueConstrainerEnabler </code> search 
     *  record interface type is supported. 
     *
     *  @param  queueConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueConstrainerEnablerSearchRecordType(org.osid.type.Type queueConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsQueueConstrainerEnablerSearchRecordType(queueConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> QueueProcessor </code> record interface 
     *  types. 
     *
     *  @return a list containing the supported <code> QueueProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorRecordTypes() {
        return (getAdapteeManager().getQueueProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueProcessor </code> record interface type 
     *  is supported. 
     *
     *  @param  queueProcessorRecordType a <code> Type </code> indicating a 
     *          <code> QueueProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorRecordType(org.osid.type.Type queueProcessorRecordType) {
        return (getAdapteeManager().supportsQueueProcessorRecordType(queueProcessorRecordType));
    }


    /**
     *  Gets the supported <code> QueueProcessor </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> QueueProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorSearchRecordTypes() {
        return (getAdapteeManager().getQueueProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueProcessor </code> search record 
     *  interface type is supported. 
     *
     *  @param  queueProcessorSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorSearchRecordType(org.osid.type.Type queueProcessorSearchRecordType) {
        return (getAdapteeManager().supportsQueueProcessorSearchRecordType(queueProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> QueueProcessorEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> QueueProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getQueueProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueProcessorEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  queueProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> QueueProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerRecordType(org.osid.type.Type queueProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsQueueProcessorEnablerRecordType(queueProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> QueueProcessorEnabler </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> QueueProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getQueueProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> QueueProcessorEnabler </code> search record 
     *  interface type is supported. 
     *
     *  @param  queueProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> QueueProcessorEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsQueueProcessorEnablerSearchRecordType(org.osid.type.Type queueProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsQueueProcessorEnablerSearchRecordType(queueProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> PoolConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> PoolConstrainer </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolConstrainerRecordTypes() {
        return (getAdapteeManager().getPoolConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> PoolConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  poolConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> PoolConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerRecordType(org.osid.type.Type poolConstrainerRecordType) {
        return (getAdapteeManager().supportsPoolConstrainerRecordType(poolConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> PoolConstrainer </code> search record types. 
     *
     *  @return a list containing the supported <code> PoolConstrainer </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getPoolConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> PoolConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  poolConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> PoolConstrainer </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolConstrainerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerSearchRecordType(org.osid.type.Type poolConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsPoolConstrainerSearchRecordType(poolConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> PoolConstrainerEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> PoolConstrainerEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getPoolConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> PoolConstrainerEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  poolConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> PoolConstrainerEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerRecordType(org.osid.type.Type poolConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsPoolConstrainerEnablerRecordType(poolConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> PoolConstrainerEnabler </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> PoolConstrainerEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getPoolConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> PoolConstrainerEnabler </code> search record 
     *  interface type is supported. 
     *
     *  @param  poolConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> PoolConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolConstrainerEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsPoolConstrainerEnablerSearchRecordType(org.osid.type.Type poolConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsPoolConstrainerEnablerSearchRecordType(poolConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> PoolProcessor </code> record interface 
     *  types. 
     *
     *  @return a list containing the supported <code> PoolProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolProcessorRecordTypes() {
        return (getAdapteeManager().getPoolProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> PoolProcessor </code> record interface type 
     *  is supported. 
     *
     *  @param  poolProcessorRecordType a <code> Type </code> indicating a 
     *          <code> PoolProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> poolProcessorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPoolProcessorRecordType(org.osid.type.Type poolProcessorRecordType) {
        return (getAdapteeManager().supportsPoolProcessorRecordType(poolProcessorRecordType));
    }


    /**
     *  Gets the supported <code> PoolProcessor </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> PoolProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolProcessorSearchRecordTypes() {
        return (getAdapteeManager().getPoolProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> PoolProcessor </code> search record 
     *  interface type is supported. 
     *
     *  @param  poolProcessorSearchRecordType a <code> Type </code> indicating 
     *          a <code> PoolProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPoolProcessorSearchRecordType(org.osid.type.Type poolProcessorSearchRecordType) {
        return (getAdapteeManager().supportsPoolProcessorSearchRecordType(poolProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> PoolProcessorEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> PoolProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getPoolProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> PoolProcessorEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  poolProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> PoolProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolProcessorEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerRecordType(org.osid.type.Type poolProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsPoolProcessorEnablerRecordType(poolProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> PoolProcessorEnabler </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> PoolProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getPoolProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> PoolProcessorEnabler </code> search record 
     *  interface type is supported. 
     *
     *  @param  poolProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> PoolProcessorEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsPoolProcessorEnablerSearchRecordType(org.osid.type.Type poolProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsPoolProcessorEnablerSearchRecordType(poolProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> BrokerConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> BrokerConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerConstrainerRecordTypes() {
        return (getAdapteeManager().getBrokerConstrainerRecordTypes());
    }


    /**
     *  Tests if the given <code> BrokerConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  brokerConstrainerRecordType a <code> Type </code> indicating a 
     *          <code> BrokerConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerRecordType(org.osid.type.Type brokerConstrainerRecordType) {
        return (getAdapteeManager().supportsBrokerConstrainerRecordType(brokerConstrainerRecordType));
    }


    /**
     *  Gets the supported <code> BrokerConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> BrokerConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerConstrainerSearchRecordTypes() {
        return (getAdapteeManager().getBrokerConstrainerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> BrokerConstrainer </code> search record type 
     *  is supported. 
     *
     *  @param  brokerConstrainerSearchRecordType a <code> Type </code> 
     *          indicating a <code> BrokerConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerSearchRecordType(org.osid.type.Type brokerConstrainerSearchRecordType) {
        return (getAdapteeManager().supportsBrokerConstrainerSearchRecordType(brokerConstrainerSearchRecordType));
    }


    /**
     *  Gets the supported <code> BrokerConstrainerEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> 
     *          BrokerConstrainerEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerConstrainerEnablerRecordTypes() {
        return (getAdapteeManager().getBrokerConstrainerEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> BrokerConstrainerEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  brokerConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating a <code> BrokerConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerRecordType(org.osid.type.Type brokerConstrainerEnablerRecordType) {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerRecordType(brokerConstrainerEnablerRecordType));
    }


    /**
     *  Gets the supported <code> BrokerConstrainerEnabler </code> search 
     *  record interface types. 
     *
     *  @return a list containing the supported <code> 
     *          BrokerConstrainerEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerConstrainerEnablerSearchRecordTypes() {
        return (getAdapteeManager().getBrokerConstrainerEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> BrokerConstrainerEnabler </code> search 
     *  record interface type is supported. 
     *
     *  @param  brokerConstrainerEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> BrokerConstrainerEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerConstrainerEnablerSearchRecordType </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsBrokerConstrainerEnablerSearchRecordType(org.osid.type.Type brokerConstrainerEnablerSearchRecordType) {
        return (getAdapteeManager().supportsBrokerConstrainerEnablerSearchRecordType(brokerConstrainerEnablerSearchRecordType));
    }


    /**
     *  Gets the supported <code> BrokerProcessor </code> record interface 
     *  types. 
     *
     *  @return a list containing the supported <code> BrokerProcessor </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerProcessorRecordTypes() {
        return (getAdapteeManager().getBrokerProcessorRecordTypes());
    }


    /**
     *  Tests if the given <code> BrokerProcessor </code> record interface 
     *  type is supported. 
     *
     *  @param  brokerProcessorRecordType a <code> Type </code> indicating a 
     *          <code> BrokerProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerProcessorRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorRecordType(org.osid.type.Type brokerProcessorRecordType) {
        return (getAdapteeManager().supportsBrokerProcessorRecordType(brokerProcessorRecordType));
    }


    /**
     *  Gets the supported <code> BrokerProcessor </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> BrokerProcessor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerProcessorSearchRecordTypes() {
        return (getAdapteeManager().getBrokerProcessorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> BrokerProcessor </code> search record 
     *  interface type is supported. 
     *
     *  @param  brokerProcessorSearchRecordType a <code> Type </code> 
     *          indicating a <code> BrokerProcessor </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerProcessorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorSearchRecordType(org.osid.type.Type brokerProcessorSearchRecordType) {
        return (getAdapteeManager().supportsBrokerProcessorSearchRecordType(brokerProcessorSearchRecordType));
    }


    /**
     *  Gets the supported <code> BrokerProcessorEnabler </code> record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> BrokerProcessorEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerProcessorEnablerRecordTypes() {
        return (getAdapteeManager().getBrokerProcessorEnablerRecordTypes());
    }


    /**
     *  Tests if the given <code> BrokerProcessorEnabler </code> record 
     *  interface type is supported. 
     *
     *  @param  brokerProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> BrokerProcessorEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerProcessorEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerRecordType(org.osid.type.Type brokerProcessorEnablerRecordType) {
        return (getAdapteeManager().supportsBrokerProcessorEnablerRecordType(brokerProcessorEnablerRecordType));
    }


    /**
     *  Gets the supported <code> BrokerProcessorEnabler </code> search record 
     *  interface types. 
     *
     *  @return a list containing the supported <code> BrokerProcessorEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerProcessorEnablerSearchRecordTypes() {
        return (getAdapteeManager().getBrokerProcessorEnablerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> BrokerProcessorEnabler </code> search record 
     *  interface type is supported. 
     *
     *  @param  brokerProcessorEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> BrokerProcessorEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerProcessorEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsBrokerProcessorEnablerSearchRecordType(org.osid.type.Type brokerProcessorEnablerSearchRecordType) {
        return (getAdapteeManager().supportsBrokerProcessorEnablerSearchRecordType(brokerProcessorEnablerSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerLookupSession getQueueConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerLookupSession getQueueConstrainerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerQuerySession getQueueConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerQuerySession getQueueConstrainerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerSearchSession getQueueConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerSearchSession getQueueConstrainerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerAdminSession getQueueConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerAdminSession getQueueConstrainerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer notification service. 
     *
     *  @param  queueConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueConstrainerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerNotificationSession getQueueConstrainerNotificationSession(org.osid.provisioning.rules.QueueConstrainerReceiver queueConstrainerReceiver, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerNotificationSession(queueConstrainerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer notification service for the given distributor. 
     *
     *  @param  queueConstrainerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerReceiver, distributorId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerNotificationSession getQueueConstrainerNotificationSessionForDistributor(org.osid.provisioning.rules.QueueConstrainerReceiver queueConstrainerReceiver, 
                                                                                                                                org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerNotificationSessionForDistributor(queueConstrainerReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue 
     *  constrainer/distributor mappings for queue constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerDistributorSession getQueueConstrainerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  constrainer to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerDistributorAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerDistributorAssignmentSession getQueueConstrainerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue constrainer smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerSmartDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerSmartDistributorSession getQueueConstrainerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  ta queue. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerRuleLookupSession getQueueConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer mapping lookup service for the given distributor for 
     *  looking up rules applied to a qeue. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerRuleLookupSession getQueueConstrainerRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer assignment service to apply to queues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerRuleApplicationSession getQueueConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer assignment service for the given distributor to apply to 
     *  queues. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerRuleApplicationSession getQueueConstrainerRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession getQueueConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerLookupSession getQueueConstrainerEnablerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerQuerySession getQueueConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerQuerySession getQueueConstrainerEnablerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerSearchSession getQueueConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enablers earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerSearchSession getQueueConstrainerEnablerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerAdminSession getQueueConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerAdminSession getQueueConstrainerEnablerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler notification service. 
     *
     *  @param  queueConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerNotificationSession getQueueConstrainerEnablerNotificationSession(org.osid.provisioning.rules.QueueConstrainerEnablerReceiver queueConstrainerEnablerReceiver, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerNotificationSession(queueConstrainerEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler notification service for the given distributor. 
     *
     *  @param  queueConstrainerEnablerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerEnablerReceiver, distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerNotificationSession getQueueConstrainerEnablerNotificationSessionForDistributor(org.osid.provisioning.rules.QueueConstrainerEnablerReceiver queueConstrainerEnablerReceiver, 
                                                                                                                                              org.osid.id.Id distributorId, 
                                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerNotificationSessionForDistributor(queueConstrainerEnablerReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue constrainer 
     *  enabler/distributor mappings for queue constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerDistributorSession getQueueConstrainerEnablerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  constrainer enablers to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerDistributorAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerDistributorAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerDistributorAssignmentSession getQueueConstrainerEnablerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue constrainer 
     *  enabler smart distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerSmartDistributorSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerSmartDistributor() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerSmartDistributorSession getQueueConstrainerEnablerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerRuleLookupSession getQueueConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler mapping lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerRuleLookupSession getQueueConstrainerEnablerRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerRuleApplicationSession getQueueConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  constrainer enabler assignment service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueConstrainerEnablerRuleApplicationSession getQueueConstrainerEnablerRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueConstrainerEnablerRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorLookupSession getQueueProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorLookupSession getQueueProcessorLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorQuerySession getQueueProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorQuerySession getQueueProcessorQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorSearchSession getQueueProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorSearchSession getQueueProcessorSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorAdminSession getQueueProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorAdminSession getQueueProcessorAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor notification service. 
     *
     *  @param  queueProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueProcessorReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorNotificationSession getQueueProcessorNotificationSession(org.osid.provisioning.rules.QueueProcessorReceiver queueProcessorReceiver, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorNotificationSession(queueProcessorReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor notification service for the given distributor. 
     *
     *  @param  queueProcessorReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueProcessorReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorNotificationSession getQueueProcessorNotificationSessionForDistributor(org.osid.provisioning.rules.QueueProcessorReceiver queueProcessorReceiver, 
                                                                                                                            org.osid.id.Id distributorId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorNotificationSessionForDistributor(queueProcessorReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue 
     *  processor/distributor mappings for queue processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorDistributorSession getQueueProcessorDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  processor to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorDistributorAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorDistributorAssignmentSession getQueueProcessorDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue processor smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorSmartDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorSmartDistributorSession getQueueProcessorSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor mapping lookup service for looking up the rules applied to a 
     *  queue processor. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorRuleLookupSession getQueueProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor mapping lookup service for the given distributor for looking 
     *  up rules applied to a queue. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorRuleLookupSession getQueueProcessorRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor assignment service to apply to queues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorRuleApplicationSession getQueueProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor assignment service for the given distributor to apply to 
     *  queues. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorRuleApplicationSession getQueueProcessorRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerLookupSession getQueueProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerLookupSession getQueueProcessorEnablerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerQuerySession getQueueProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerQuerySession getQueueProcessorEnablerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerSearchSession getQueueProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enablers earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerSearchSession getQueueProcessorEnablerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerAdminSession getQueueProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerAdminSession getQueueProcessorEnablerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler notification service. 
     *
     *  @param  queueProcessorEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerNotificationSession getQueueProcessorEnablerNotificationSession(org.osid.provisioning.rules.QueueProcessorEnablerReceiver queueProcessorEnablerReceiver, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerNotificationSession(queueProcessorEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler notification service for the given distributor. 
     *
     *  @param  queueProcessorEnablerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueProcessorEnablerReceiver, distributorId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerNotificationSession getQueueProcessorEnablerNotificationSessionForDistributor(org.osid.provisioning.rules.QueueProcessorEnablerReceiver queueProcessorEnablerReceiver, 
                                                                                                                                          org.osid.id.Id distributorId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerNotificationSessionForDistributor(queueProcessorEnablerReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue processor 
     *  enabler/distributor mappings for queue processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerDistributorSession getQueueProcessorEnablerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queue 
     *  processor enablers to queue processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerDistributorAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerDistributorAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerDistributorAssignmentSession getQueueProcessorEnablerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue processor enabler 
     *  smart distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerSmartDistributor() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerSmartDistributorSession getQueueProcessorEnablerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerRuleLookupSession getQueueProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler mapping lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerRuleLookupSession getQueueProcessorEnablerRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerRuleApplicationSession getQueueProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  processor enabler assignment service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerRuleApplicationSession getQueueProcessorEnablerRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueProcessorEnablerRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerLookupSession getPoolConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerLookupSession getPoolConstrainerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerQuerySession getPoolConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerQuerySession getPoolConstrainerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException a <code> 
     *          PoolConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerSearchSession getPoolConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerSearchSession getPoolConstrainerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerAdminSession getPoolConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerAdminSession getPoolConstrainerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer notification service. 
     *
     *  @param  poolConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> poolConstrainerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerNotificationSession getPoolConstrainerNotificationSession(org.osid.provisioning.rules.PoolConstrainerReceiver poolConstrainerReceiver, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerNotificationSession(poolConstrainerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer notification service for the given distributor. 
     *
     *  @param  poolConstrainerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> poolConstrainerReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerNotificationSession getPoolConstrainerNotificationSessionForDistributor(org.osid.provisioning.rules.PoolConstrainerReceiver poolConstrainerReceiver, 
                                                                                                                              org.osid.id.Id distributorId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerNotificationSessionForDistributor(poolConstrainerReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup pool 
     *  constrainer/distributor mappings for pool constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerDistributorSession getPoolConstrainerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning pool 
     *  constrainer to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerDistributorAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerDistributorAssignmentSession getPoolConstrainerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage pool constrainer smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerSmartDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerSmartDistributorSession getPoolConstrainerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a pool. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerRuleLookupSession getPoolConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer mapping lookup service for the given distributor for 
     *  looking up rules applied to a pool. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerRuleLookupSession getPoolConstrainerRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer assignment service to apply to pools. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerRuleApplicationSession getPoolConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer assignment service for the given distributor to apply to 
     *  pools. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerRuleApplicationSession getPoolConstrainerRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession getPoolConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerLookupSession getPoolConstrainerEnablerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerQuerySession getPoolConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerQuerySession getPoolConstrainerEnablerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerSearchSession getPoolConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enablers earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerSearchSession getPoolConstrainerEnablerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerAdminSession getPoolConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerAdminSession getPoolConstrainerEnablerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler notification service. 
     *
     *  @param  poolConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolConstrainerEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerNotificationSession getPoolConstrainerEnablerNotificationSession(org.osid.provisioning.rules.PoolConstrainerEnablerReceiver poolConstrainerEnablerReceiver, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerNotificationSession(poolConstrainerEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler notification service for the given distributor. 
     *
     *  @param  poolConstrainerEnablerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolConstrainerEnablerReceiver, distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerNotificationSession getPoolConstrainerEnablerNotificationSessionForDistributor(org.osid.provisioning.rules.PoolConstrainerEnablerReceiver poolConstrainerEnablerReceiver, 
                                                                                                                                            org.osid.id.Id distributorId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerNotificationSessionForDistributor(poolConstrainerEnablerReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup pool constrainer 
     *  enabler/distributor mappings for pool constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerDistributorSession getPoolConstrainerEnablerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning pool 
     *  constrainer enablers to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerDistributorAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerDistributorAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerDistributorAssignmentSession getPoolConstrainerEnablerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage pool constrainer enabler 
     *  smart distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerSmartDistributor() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerSmartDistributorSession getPoolConstrainerEnablerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerRuleLookupSession getPoolConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler mapping lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerRuleLookupSession getPoolConstrainerEnablerRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerRuleApplicationSession getPoolConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  constrainer enabler assignment service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolConstrainerEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerEnablerRuleApplicationSession getPoolConstrainerEnablerRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolConstrainerEnablerRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorLookupSession getPoolProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorLookupSession getPoolProcessorLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorQuerySession getPoolProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorQuerySession getPoolProcessorQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorSearchSession getPoolProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorSearchSession getPoolProcessorSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorAdminSession getPoolProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorAdminSession getPoolProcessorAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  notification service. 
     *
     *  @param  poolProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> poolProcessorReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorNotificationSession getPoolProcessorNotificationSession(org.osid.provisioning.rules.PoolProcessorReceiver poolProcessorReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorNotificationSession(poolProcessorReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  notification service for the given distributor. 
     *
     *  @param  poolProcessorReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> poolProcessorReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorNotificationSession getPoolProcessorNotificationSessionForDistributor(org.osid.provisioning.rules.PoolProcessorReceiver poolProcessorReceiver, 
                                                                                                                          org.osid.id.Id distributorId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorNotificationSessionForDistributor(poolProcessorReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup pool 
     *  processor/distributor mappings for pool processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorDistributorSession getPoolProcessorDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning pool 
     *  processor to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorDistributorAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorDistributorAssignmentSession getPoolProcessorDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage pool processor smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorSmartDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorSmartDistributorSession getPoolProcessorSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  mapping lookup service for looking up the rules applied to a pool. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorRuleLookupSession getPoolProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  mapping lookup service for the given distributor for looking up rules 
     *  applied to a pool. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorRuleLookupSession getPoolProcessorRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  assignment service to apply to pool processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorRuleApplication() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorRuleApplicationSession getPoolProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  assignment service for the given distributor to apply to pool 
     *  processors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorRuleApplicationSession getPoolProcessorRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerLookupSession getPoolProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerLookupSession getPoolProcessorEnablerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerQuerySession getPoolProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerQuerySession getPoolProcessorEnablerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerSearchSession getPoolProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enablers earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerSearchSession getPoolProcessorEnablerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerAdminSession getPoolProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerAdminSession getPoolProcessorEnablerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler notification service. 
     *
     *  @param  poolProcessorEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolProcessorEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerNotificationSession getPoolProcessorEnablerNotificationSession(org.osid.provisioning.rules.PoolProcessorEnablerReceiver poolProcessorEnablerReceiver, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerNotificationSession(poolProcessorEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler notification service for the given distributor. 
     *
     *  @param  poolProcessorEnablerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          poolProcessorEnablerReceiver, distributorId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerNotificationSession getPoolProcessorEnablerNotificationSessionForDistributor(org.osid.provisioning.rules.PoolProcessorEnablerReceiver poolProcessorEnablerReceiver, 
                                                                                                                                        org.osid.id.Id distributorId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerNotificationSessionForDistributor(poolProcessorEnablerReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup pool processor 
     *  enabler/distributor mappings for pool processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerDistributorSession getPoolProcessorEnablerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning pool 
     *  processor enablers to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerDistributorAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerDistributorAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerDistributorAssignmentSession getPoolProcessorEnablerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage pool processor enabler 
     *  smart distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerSmartDistributor() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerSmartDistributorSession getPoolProcessorEnablerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerRuleLookupSession getPoolProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler mapping lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerRuleLookupSession getPoolProcessorEnablerRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerRuleApplicationSession getPoolProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool processor 
     *  enabler assignment service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PoolProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerRuleApplicationSession getPoolProcessorEnablerRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolProcessorEnablerRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerLookupSession getBrokerConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerLookupSession getBrokerConstrainerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerQuerySession getBrokerConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerQuerySession getBrokerConstrainerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerSearchSession getBrokerConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerSearchSession getBrokerConstrainerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerAdminSession getBrokerConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerAdminSession getBrokerConstrainerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer notification service. 
     *
     *  @param  brokerConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerConstrainerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerNotificationSession getBrokerConstrainerNotificationSession(org.osid.provisioning.rules.BrokerConstrainerReceiver brokerConstrainerReceiver, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerNotificationSession(brokerConstrainerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer notification service for the given distributor. 
     *
     *  @param  brokerConstrainerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerConstrainerReceiver, distributorId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerNotificationSession getBrokerConstrainerNotificationSessionForDistributor(org.osid.provisioning.rules.BrokerConstrainerReceiver brokerConstrainerReceiver, 
                                                                                                                                  org.osid.id.Id distributorId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerNotificationSessionForDistributor(brokerConstrainerReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup broker 
     *  constrainer/distributor mappings for broker constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerDistributorSession getBrokerConstrainerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning broker 
     *  constrainer to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerDistributorAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerDistributorAssignmentSession getBrokerConstrainerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage broker constrainer smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerSmartDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerSmartDistributorSession getBrokerConstrainerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  a broker. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerRuleLookupSession getBrokerConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer mapping lookup service for the given distributor for 
     *  looking up rules applied to a broker. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerRuleLookupSession getBrokerConstrainerRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer assignment service to apply to brokers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerRuleApplicationSession getBrokerConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer assignment service for the given distributor to apply to 
     *  brokers. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerRuleApplicationSession getBrokerConstrainerRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerLookupSession getBrokerConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerLookupSession getBrokerConstrainerEnablerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerQuerySession getBrokerConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerQuerySession getBrokerConstrainerEnablerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerSearchSession getBrokerConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enablers earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerSearchSession getBrokerConstrainerEnablerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerAdminSession getBrokerConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerAdminSession getBrokerConstrainerEnablerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler notification service. 
     *
     *  @param  brokerConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerConstrainerEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerNotificationSession getBrokerConstrainerEnablerNotificationSession(org.osid.provisioning.rules.BrokerConstrainerEnablerReceiver brokerConstrainerEnablerReceiver, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerNotificationSession(brokerConstrainerEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler notification service for the given distributor. 
     *
     *  @param  brokerConstrainerEnablerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerConstrainerEnablerReceiver, distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerNotificationSession getBrokerConstrainerEnablerNotificationSessionForDistributor(org.osid.provisioning.rules.BrokerConstrainerEnablerReceiver brokerConstrainerEnablerReceiver, 
                                                                                                                                                org.osid.id.Id distributorId, 
                                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerNotificationSessionForDistributor(brokerConstrainerEnablerReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup broker constrainer 
     *  enabler/distributor mappings for broker constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerDistributor() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerDistributorSession getBrokerConstrainerEnablerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning broker 
     *  constrainer enablers to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerDistributorAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerDistributorAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerDistributorAssignmentSession getBrokerConstrainerEnablerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage broker constrainer 
     *  enabler smart distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerSmartDistributorSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerSmartDistributor() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerSmartDistributorSession getBrokerConstrainerEnablerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerRuleLookupSession getBrokerConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler mapping lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerRuleLookupSession getBrokerConstrainerEnablerRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerRuleApplicationSession getBrokerConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  constrainer enabler assignment service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerRuleApplicationSession getBrokerConstrainerEnablerRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerConstrainerEnablerRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorLookupSession getBrokerProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorLookupSession getBrokerProcessorLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorQuerySession getBrokerProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorQuerySession getBrokerProcessorQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorSearchSession getBrokerProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorSearchSession getBrokerProcessorSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorAdminSession getBrokerProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorAdminSession getBrokerProcessorAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor notification service. 
     *
     *  @param  brokerProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> brokerProcessorReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorNotificationSession getBrokerProcessorNotificationSession(org.osid.provisioning.rules.BrokerProcessorReceiver brokerProcessorReceiver, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorNotificationSession(brokerProcessorReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor notification service for the given distributor. 
     *
     *  @param  brokerProcessorReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> brokerProcessorReceiver, 
     *          distributorId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorNotificationSession getBrokerProcessorNotificationSessionForDistributor(org.osid.provisioning.rules.BrokerProcessorReceiver brokerProcessorReceiver, 
                                                                                                                              org.osid.id.Id distributorId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorNotificationSessionForDistributor(brokerProcessorReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup broker 
     *  processor/distributor mappings for broker processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorDistributorSession getBrokerProcessorDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning broker 
     *  processor to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorDistributorAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorDistributorAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorDistributorAssignmentSession getBrokerProcessorDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage broker processor smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorSmartDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorSmartDistributorSession getBrokerProcessorSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor mapping lookup service for looking up the rules applied to a 
     *  broker. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorRuleLookupSession getBrokerProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor mapping lookup service for the given distributor for looking 
     *  up rules applied to a broker. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorRuleLookupSession getBrokerProcessorRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor assignment service to apply to brokers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorRuleApplicationSession getBrokerProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor assignment service for the given distributor to apply to 
     *  brokers. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorRuleApplicationSession getBrokerProcessorRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession getBrokerProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession getBrokerProcessorEnablerLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerQuerySession getBrokerProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerQuerySession getBrokerProcessorEnablerQuerySessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerQuerySessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerSearchSession getBrokerProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enablers earch service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerSearchSession getBrokerProcessorEnablerSearchSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerSearchSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerAdminSession getBrokerProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerAdminSession getBrokerProcessorEnablerAdminSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerAdminSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler notification service. 
     *
     *  @param  brokerProcessorEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerProcessorEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerNotificationSession getBrokerProcessorEnablerNotificationSession(org.osid.provisioning.rules.BrokerProcessorEnablerReceiver brokerProcessorEnablerReceiver, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerNotificationSession(brokerProcessorEnablerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler notification service for the given distributor. 
     *
     *  @param  brokerProcessorEnablerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          brokerProcessorEnablerReceiver, distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerNotificationSession getBrokerProcessorEnablerNotificationSessionForDistributor(org.osid.provisioning.rules.BrokerProcessorEnablerReceiver brokerProcessorEnablerReceiver, 
                                                                                                                                            org.osid.id.Id distributorId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerNotificationSessionForDistributor(brokerProcessorEnablerReceiver, distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup broker processor 
     *  enabler/distributor mappings for broker processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerDistributorSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerDistributorSession getBrokerProcessorEnablerDistributorSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerDistributorSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning broker 
     *  processor enablers to distributors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerDistributorAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerDistributorAssignment() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerDistributorAssignmentSession getBrokerProcessorEnablerDistributorAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerDistributorAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage broker processor enabler 
     *  smart distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerSmartDistributor() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerSmartDistributorSession getBrokerProcessorEnablerSmartDistributorSession(org.osid.id.Id distributorId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerSmartDistributorSession(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerRuleLookupSession getBrokerProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerRuleLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler mapping lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerRuleLookupSession getBrokerProcessorEnablerRuleLookupSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerRuleLookupSessionForDistributor(distributorId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerRuleApplicationSession getBrokerProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerRuleApplicationSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  processor enabler assignment service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BrokerProcessorEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerProcessorEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerRuleApplicationSession getBrokerProcessorEnablerRuleApplicationSessionForDistributor(org.osid.id.Id distributorId, 
                                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerProcessorEnablerRuleApplicationSessionForDistributor(distributorId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
