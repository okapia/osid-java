//
// FloorElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.floor.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class FloorElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the FloorElement Id.
     *
     *  @return the floor element Id
     */

    public static org.osid.id.Id getFloorEntityId() {
        return (makeEntityId("osid.room.Floor"));
    }


    /**
     *  Gets the BuildingId element Id.
     *
     *  @return the BuildingId element Id
     */

    public static org.osid.id.Id getBuildingId() {
        return (makeElementId("osid.room.floor.BuildingId"));
    }


    /**
     *  Gets the Building element Id.
     *
     *  @return the Building element Id
     */

    public static org.osid.id.Id getBuilding() {
        return (makeElementId("osid.room.floor.Building"));
    }


    /**
     *  Gets the Number element Id.
     *
     *  @return the Number element Id
     */

    public static org.osid.id.Id getNumber() {
        return (makeElementId("osid.room.floor.Number"));
    }


    /**
     *  Gets the GrossArea element Id.
     *
     *  @return the GrossArea element Id
     */

    public static org.osid.id.Id getGrossArea() {
        return (makeElementId("osid.room.floor.GrossArea"));
    }


    /**
     *  Gets the RoomId element Id.
     *
     *  @return the RoomId element Id
     */

    public static org.osid.id.Id getRoomId() {
        return (makeQueryElementId("osid.room.floor.RoomId"));
    }


    /**
     *  Gets the Room element Id.
     *
     *  @return the Room element Id
     */

    public static org.osid.id.Id getRoom() {
        return (makeQueryElementId("osid.room.floor.Room"));
    }


    /**
     *  Gets the CampusId element Id.
     *
     *  @return the CampusId element Id
     */

    public static org.osid.id.Id getCampusId() {
        return (makeQueryElementId("osid.room.floor.CampusId"));
    }


    /**
     *  Gets the Campus element Id.
     *
     *  @return the Campus element Id
     */

    public static org.osid.id.Id getCampus() {
        return (makeQueryElementId("osid.room.floor.Campus"));
    }
}
