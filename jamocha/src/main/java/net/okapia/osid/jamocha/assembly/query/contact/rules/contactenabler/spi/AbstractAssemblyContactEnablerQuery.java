//
// AbstractAssemblyContactEnablerQuery.java
//
//     A ContactEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.contact.rules.contactenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ContactEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyContactEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.contact.rules.ContactEnablerQuery,
               org.osid.contact.rules.ContactEnablerQueryInspector,
               org.osid.contact.rules.ContactEnablerSearchOrder {

    private final java.util.Collection<org.osid.contact.rules.records.ContactEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.contact.rules.records.ContactEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.contact.rules.records.ContactEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyContactEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyContactEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the contact. 
     *
     *  @param  contactId the contact book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> contactBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledContactId(org.osid.id.Id contactId, boolean match) {
        getAssembler().addIdTerm(getRuledContactIdColumn(), contactId, match);
        return;
    }


    /**
     *  Clears the contact <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledContactIdTerms() {
        getAssembler().clearTerms(getRuledContactIdColumn());
        return;
    }


    /**
     *  Gets the contact <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledContactIdTerms() {
        return (getAssembler().getIdTerms(getRuledContactIdColumn()));
    }


    /**
     *  Gets the RuledContactId column name.
     *
     * @return the column name
     */

    protected String getRuledContactIdColumn() {
        return ("ruled_contact_id");
    }


    /**
     *  Tests if a <code> ContactBookQuery </code> is available. 
     *
     *  @return <code> true </code> if a contact query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledContactQuery() {
        return (false);
    }


    /**
     *  Gets the query for a contact. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> supportsContactQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactQuery getRuledContactQuery() {
        throw new org.osid.UnimplementedException("supportsRuledContactQuery() is false");
    }


    /**
     *  Matches enablers mapped to any contact. 
     *
     *  @param  match <code> true </code> for enablers mapped to any contact, 
     *          <code> false </code> to match enablers mapped to no contact 
     */

    @OSID @Override
    public void matchAnyRuledContact(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledContactColumn(), match);
        return;
    }


    /**
     *  Clears the contact query terms. 
     */

    @OSID @Override
    public void clearRuledContactTerms() {
        getAssembler().clearTerms(getRuledContactColumn());
        return;
    }


    /**
     *  Gets the contact query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.contact.ContactQueryInspector[] getRuledContactTerms() {
        return (new org.osid.contact.ContactQueryInspector[0]);
    }


    /**
     *  Gets the RuledContact column name.
     *
     * @return the column name
     */

    protected String getRuledContactColumn() {
        return ("ruled_contact");
    }


    /**
     *  Matches enablers mapped to the address book. 
     *
     *  @param  addressBookId the address book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressBookId(org.osid.id.Id addressBookId, boolean match) {
        getAssembler().addIdTerm(getAddressBookIdColumn(), addressBookId, match);
        return;
    }


    /**
     *  Clears the address book <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAddressBookIdTerms() {
        getAssembler().clearTerms(getAddressBookIdColumn());
        return;
    }


    /**
     *  Gets the address book <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAddressBookIdTerms() {
        return (getAssembler().getIdTerms(getAddressBookIdColumn()));
    }


    /**
     *  Gets the AddressBookId column name.
     *
     * @return the column name
     */

    protected String getAddressBookIdColumn() {
        return ("address_book_id");
    }


    /**
     *  Tests if an <code> AddressBookQuery </code> is available. 
     *
     *  @return <code> true </code> if an address book query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address book. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address book query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAddressBookQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQuery getAddressBookQuery() {
        throw new org.osid.UnimplementedException("supportsAddressBookQuery() is false");
    }


    /**
     *  Clears the address book query terms. 
     */

    @OSID @Override
    public void clearAddressBookTerms() {
        getAssembler().clearTerms(getAddressBookColumn());
        return;
    }


    /**
     *  Gets the address book query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.contact.AddressBookQueryInspector[] getAddressBookTerms() {
        return (new org.osid.contact.AddressBookQueryInspector[0]);
    }


    /**
     *  Gets the AddressBook column name.
     *
     * @return the column name
     */

    protected String getAddressBookColumn() {
        return ("address_book");
    }


    /**
     *  Tests if this contactEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  contactEnablerRecordType a contact enabler record type 
     *  @return <code>true</code> if the contactEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type contactEnablerRecordType) {
        for (org.osid.contact.rules.records.ContactEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(contactEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  contactEnablerRecordType the contact enabler record type 
     *  @return the contact enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.rules.records.ContactEnablerQueryRecord getContactEnablerQueryRecord(org.osid.type.Type contactEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.rules.records.ContactEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(contactEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  contactEnablerRecordType the contact enabler record type 
     *  @return the contact enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.rules.records.ContactEnablerQueryInspectorRecord getContactEnablerQueryInspectorRecord(org.osid.type.Type contactEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.rules.records.ContactEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(contactEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param contactEnablerRecordType the contact enabler record type
     *  @return the contact enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(contactEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.contact.rules.records.ContactEnablerSearchOrderRecord getContactEnablerSearchOrderRecord(org.osid.type.Type contactEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.contact.rules.records.ContactEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(contactEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(contactEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this contact enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param contactEnablerQueryRecord the contact enabler query record
     *  @param contactEnablerQueryInspectorRecord the contact enabler query inspector
     *         record
     *  @param contactEnablerSearchOrderRecord the contact enabler search order record
     *  @param contactEnablerRecordType contact enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerQueryRecord</code>,
     *          <code>contactEnablerQueryInspectorRecord</code>,
     *          <code>contactEnablerSearchOrderRecord</code> or
     *          <code>contactEnablerRecordTypecontactEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addContactEnablerRecords(org.osid.contact.rules.records.ContactEnablerQueryRecord contactEnablerQueryRecord, 
                                      org.osid.contact.rules.records.ContactEnablerQueryInspectorRecord contactEnablerQueryInspectorRecord, 
                                      org.osid.contact.rules.records.ContactEnablerSearchOrderRecord contactEnablerSearchOrderRecord, 
                                      org.osid.type.Type contactEnablerRecordType) {

        addRecordType(contactEnablerRecordType);

        nullarg(contactEnablerQueryRecord, "contact enabler query record");
        nullarg(contactEnablerQueryInspectorRecord, "contact enabler query inspector record");
        nullarg(contactEnablerSearchOrderRecord, "contact enabler search odrer record");

        this.queryRecords.add(contactEnablerQueryRecord);
        this.queryInspectorRecords.add(contactEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(contactEnablerSearchOrderRecord);
        
        return;
    }
}
