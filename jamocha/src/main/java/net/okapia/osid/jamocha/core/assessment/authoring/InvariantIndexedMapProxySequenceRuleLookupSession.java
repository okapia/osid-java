//
// InvariantIndexedMapProxySequenceRuleLookupSession
//
//    Implements a SequenceRule lookup service backed by a fixed
//    collection of sequenceRules indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.authoring;


/**
 *  Implements a SequenceRule lookup service backed by a fixed
 *  collection of sequenceRules. The sequenceRules are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some sequenceRules may be compatible
 *  with more types than are indicated through these sequenceRule
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxySequenceRuleLookupSession
    extends net.okapia.osid.jamocha.core.assessment.authoring.spi.AbstractIndexedMapSequenceRuleLookupSession
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxySequenceRuleLookupSession}
     *  using an array of sequence rules.
     *
     *  @param bank the bank
     *  @param sequenceRules an array of sequence rules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRules} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxySequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                                         org.osid.assessment.authoring.SequenceRule[] sequenceRules, 
                                                         org.osid.proxy.Proxy proxy) {

        setBank(bank);
        setSessionProxy(proxy);
        putSequenceRules(sequenceRules);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxySequenceRuleLookupSession}
     *  using a collection of sequence rules.
     *
     *  @param bank the bank
     *  @param sequenceRules a collection of sequence rules
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code sequenceRules} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxySequenceRuleLookupSession(org.osid.assessment.Bank bank,
                                                         java.util.Collection<? extends org.osid.assessment.authoring.SequenceRule> sequenceRules,
                                                         org.osid.proxy.Proxy proxy) {

        setBank(bank);
        setSessionProxy(proxy);
        putSequenceRules(sequenceRules);

        return;
    }
}
