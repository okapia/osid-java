//
// AbstractSettingSearch.java
//
//     A template for making a Setting Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.setting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing setting searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractSettingSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.control.SettingSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.control.records.SettingSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.control.SettingSearchOrder settingSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of settings. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  settingIds list of settings
     *  @throws org.osid.NullArgumentException
     *          <code>settingIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongSettings(org.osid.id.IdList settingIds) {
        while (settingIds.hasNext()) {
            try {
                this.ids.add(settingIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongSettings</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of setting Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getSettingIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  settingSearchOrder setting search order 
     *  @throws org.osid.NullArgumentException
     *          <code>settingSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>settingSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderSettingResults(org.osid.control.SettingSearchOrder settingSearchOrder) {
	this.settingSearchOrder = settingSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.control.SettingSearchOrder getSettingSearchOrder() {
	return (this.settingSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given setting search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a setting implementing the requested record.
     *
     *  @param settingSearchRecordType a setting search record
     *         type
     *  @return the setting search record
     *  @throws org.osid.NullArgumentException
     *          <code>settingSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(settingSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SettingSearchRecord getSettingSearchRecord(org.osid.type.Type settingSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.control.records.SettingSearchRecord record : this.records) {
            if (record.implementsRecordType(settingSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(settingSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this setting search. 
     *
     *  @param settingSearchRecord setting search record
     *  @param settingSearchRecordType setting search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSettingSearchRecord(org.osid.control.records.SettingSearchRecord settingSearchRecord, 
                                           org.osid.type.Type settingSearchRecordType) {

        addRecordType(settingSearchRecordType);
        this.records.add(settingSearchRecord);        
        return;
    }
}
