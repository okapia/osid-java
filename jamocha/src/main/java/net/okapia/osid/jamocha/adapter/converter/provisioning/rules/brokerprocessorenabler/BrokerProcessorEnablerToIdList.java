//
// BrokerProcessorEnablerToIdList.java
//
//     Implements a BrokerProcessorEnabler IdList. 
//
//
// Tom Coppeto
// Okapia
// 15 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.provisioning.rules.brokerprocessorenabler;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  Implements an IdList converting the underlying BrokerProcessorEnablerList to a
 *  list of Ids.
 */

public final class BrokerProcessorEnablerToIdList
    extends net.okapia.osid.jamocha.adapter.id.id.spi.AbstractAdapterIdList
    implements org.osid.id.IdList {

    private final org.osid.provisioning.rules.BrokerProcessorEnablerList brokerProcessorEnablerList;


    /**
     *  Creates a new {@code BrokerProcessorEnablerToIdList}.
     *
     *  @param brokerProcessorEnablerList a {@code BrokerProcessorEnablerList}
     *  @throws org.osid.NullArgumentException {@code brokerProcessorEnablerList}
     *          is {@code null}
     */

    public BrokerProcessorEnablerToIdList(org.osid.provisioning.rules.BrokerProcessorEnablerList brokerProcessorEnablerList) {
        super(brokerProcessorEnablerList);
        this.brokerProcessorEnablerList = brokerProcessorEnablerList;
        return;
    }


    /**
     *  Creates a new {@code BrokerProcessorEnablerToIdList}.
     *
     *  @param brokerProcessorEnablers a collection of BrokerProcessorEnablers
     *  @throws org.osid.NullArgumentException {@code brokerProcessorEnablers}
     *          is {@code null}
     */

    public BrokerProcessorEnablerToIdList(java.util.Collection<org.osid.provisioning.rules.BrokerProcessorEnabler> brokerProcessorEnablers) {
        this(new net.okapia.osid.jamocha.provisioning.rules.brokerprocessorenabler.ArrayBrokerProcessorEnablerList(brokerProcessorEnablers)); 
        return;
    }


    /**
     *  Gets the next {@code Id} in this list. 
     *
     *  @return the next {@code Id} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Id} is available before calling this method.
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.Id getNextId()
        throws org.osid.OperationFailedException {

        return (this.brokerProcessorEnablerList.getNextBrokerProcessorEnabler().getId());
    }


    /**
     *  Closes this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.brokerProcessorEnablerList.close();
        return;
    }
}
