//
// GradeSystemTransformElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.transform.gradesystemtransform.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class GradeSystemTransformElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the GradeSystemTransformElement Id.
     *
     *  @return the grade system transform element Id
     */

    public static org.osid.id.Id getGradeSystemTransformEntityId() {
        return (makeEntityId("osid.grading.transform.GradeSystemTransform"));
    }


    /**
     *  Gets the SourceGradeSystemId element Id.
     *
     *  @return the SourceGradeSystemId element Id
     */

    public static org.osid.id.Id getSourceGradeSystemId() {
        return (makeElementId("osid.grading.transform.gradesystemtransform.SourceGradeSystemId"));
    }


    /**
     *  Gets the SourceGradeSystem element Id.
     *
     *  @return the SourceGradeSystem element Id
     */

    public static org.osid.id.Id getSourceGradeSystem() {
        return (makeElementId("osid.grading.transform.gradesystemtransform.SourceGradeSystem"));
    }


    /**
     *  Gets the TargetGradeSystemId element Id.
     *
     *  @return the TargetGradeSystemId element Id
     */

    public static org.osid.id.Id getTargetGradeSystemId() {
        return (makeElementId("osid.grading.transform.gradesystemtransform.TargetGradeSystemId"));
    }


    /**
     *  Gets the TargetGradeSystem element Id.
     *
     *  @return the TargetGradeSystem element Id
     */

    public static org.osid.id.Id getTargetGradeSystem() {
        return (makeElementId("osid.grading.transform.gradesystemtransform.TargetGradeSystem"));
    }


    /**
     *  Gets the GradeMaps element Id.
     *
     *  @return the GradeMaps element Id
     */

    public static org.osid.id.Id getGradeMaps() {
        return (makeElementId("osid.grading.transform.gradesystemtransform.GradeMaps"));
    }


    /**
     *  Gets the NormalizeInputScore element Id.
     *
     *  @return the NormalizeInputScore element Id
     */

    public static org.osid.id.Id getNormalizesInputScore() {
        return (makeElementId("osid.grading.transform.gradesystemtransform.NormalizeInputScore"));
    }
}
