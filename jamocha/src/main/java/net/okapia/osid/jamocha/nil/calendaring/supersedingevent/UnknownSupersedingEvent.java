//
// UnknownSupersedingEvent.java
//
//     Defines an unknown SupersedingEvent.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.calendaring.supersedingevent;


/**
 *  Defines an unknown <code>SupersedingEvent</code>.
 */

public final class UnknownSupersedingEvent
    extends net.okapia.osid.jamocha.nil.calendaring.supersedingevent.spi.AbstractUnknownSupersedingEvent
    implements org.osid.calendaring.SupersedingEvent {


    /**
     *  Constructs a new <code>UnknownSupersedingEvent</code>.
     */

    public UnknownSupersedingEvent() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownSupersedingEvent</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownSupersedingEvent(boolean optional) {
        super(optional);
        addSupersedingEventRecord(new SupersedingEventRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown SupersedingEvent.
     *
     *  @return an unknown SupersedingEvent
     */

    public static org.osid.calendaring.SupersedingEvent create() {
        return (net.okapia.osid.jamocha.builder.validator.calendaring.supersedingevent.SupersedingEventValidator.validateSupersedingEvent(new UnknownSupersedingEvent()));
    }


    public class SupersedingEventRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.calendaring.records.SupersedingEventRecord {

        
        protected SupersedingEventRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
