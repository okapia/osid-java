//
// InvariantMapRequestTransactionLookupSession
//
//    Implements a RequestTransaction lookup service backed by a fixed collection of
//    requestTransactions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning;


/**
 *  Implements a RequestTransaction lookup service backed by a fixed
 *  collection of request transactions. The request transactions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRequestTransactionLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.spi.AbstractMapRequestTransactionLookupSession
    implements org.osid.provisioning.RequestTransactionLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRequestTransactionLookupSession</code> with no
     *  request transactions.
     *  
     *  @param distributor the distributor
     *  @throws org.osid.NullArgumnetException {@code distributor} is
     *          {@code null}
     */

    public InvariantMapRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor) {
        setDistributor(distributor);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRequestTransactionLookupSession</code> with a single
     *  request transaction.
     *  
     *  @param distributor the distributor
     *  @param requestTransaction a single request transaction
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code requestTransaction} is <code>null</code>
     */

      public InvariantMapRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.RequestTransaction requestTransaction) {
        this(distributor);
        putRequestTransaction(requestTransaction);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRequestTransactionLookupSession</code> using an array
     *  of request transactions.
     *  
     *  @param distributor the distributor
     *  @param requestTransactions an array of request transactions
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code requestTransactions} is <code>null</code>
     */

      public InvariantMapRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                               org.osid.provisioning.RequestTransaction[] requestTransactions) {
        this(distributor);
        putRequestTransactions(requestTransactions);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRequestTransactionLookupSession</code> using a
     *  collection of request transactions.
     *
     *  @param distributor the distributor
     *  @param requestTransactions a collection of request transactions
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code requestTransactions} is <code>null</code>
     */

      public InvariantMapRequestTransactionLookupSession(org.osid.provisioning.Distributor distributor,
                                               java.util.Collection<? extends org.osid.provisioning.RequestTransaction> requestTransactions) {
        this(distributor);
        putRequestTransactions(requestTransactions);
        return;
    }
}
