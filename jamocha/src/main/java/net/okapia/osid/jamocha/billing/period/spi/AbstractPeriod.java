//
// AbstractPeriod.java
//
//     Defines a Period.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.period.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Period</code>.
 */

public abstract class AbstractPeriod
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.billing.Period {

    private org.osid.locale.DisplayText displayLabel;
    private org.osid.calendaring.DateTime openDate;
    private org.osid.calendaring.DateTime closeDate;
    private org.osid.calendaring.DateTime billingDate;
    private org.osid.calendaring.DateTime dueDate;

    private final java.util.Collection<org.osid.billing.records.PeriodRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets a display label for this period which may be less formal than the 
     *  display name. 
     *
     *  @return the period label 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.displayLabel);
    }


    /**
     *  Sets the display label.
     *
     *  @param label a display label
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    protected void setDisplayLabel(org.osid.locale.DisplayText label) {
        nullarg(label, "displaylabel");
        this.displayLabel = label;
        return;
    }


    /**
     *  Tests if this period has an open date. 
     *
     *  @return <code> true </code> if there is an open date
     *          associated with this period, <code> false </code>
     *          otherwise
     */

    @OSID @Override
    public boolean hasOpenDate() {
        return (this.openDate != null);
    }


    /**
     *  Gets the open date. 
     *
     *  @return the open date 
     *  @throws org.osid.IllegalStateException <code> hasOpenDate()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getOpenDate() {
        if (!hasOpenDate()) {
            throw new org.osid.IllegalStateException("hasOpenDate() is false");
        }

        return (this.openDate);
    }


    /**
     *  Sets the open date.
     *
     *  @param date an open date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setOpenDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "open date");
        this.openDate = date;
        return;
    }


    /**
     *  Tests if this period has a close date. 
     *
     *  @return <code> true </code> if there is a close date
     *          associated with this period, <code> false </code>
     *          otherwise
     */

    @OSID @Override
    public boolean hasCloseDate() {
        return (this.closeDate != null);
    }


    /**
     *  Gets the close date. 
     *
     *  @return the close date 
     *  @throws org.osid.IllegalStateException <code> hasCloseDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCloseDate() {
        if (!hasCloseDate()) {
            throw new org.osid.IllegalStateException("hasCloseDate() is false");
        }

        return (this.closeDate);
    }


    /**
     *  Sets the close date.
     *
     *  @param date a close date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setCloseDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "close date");
        this.closeDate = date;
        return;
    }


    /**
     *  Tests if this period has a billing date. 
     *
     *  @return <code> true </code> if there is a billing date associated with 
     *          this period, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasBillingDate() {
        return (this.billingDate != null);
    }


    /**
     *  Gets the billing date. 
     *
     *  @return the billing date 
     *  @throws org.osid.IllegalStateException <code> hasBillingDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getBillingDate() {
        if (!hasBillingDate()) {
            throw new org.osid.IllegalStateException("hasBillingDate() is false");
        }

        return (this.billingDate);
    }


    /**
     *  Sets the billing date.
     *
     *  @param date a billing date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setBillingDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "billing date");
        this.billingDate = date;
        return;
    }


    /**
     *  Tests if this period has a due date. 
     *
     *  @return <code> true </code> if there is a due date associated with 
     *          this period, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasDueDate() {
        return (this.dueDate != null);
    }


    /**
     *  Gets the due date. 
     *
     *  @return the due date 
     *  @throws org.osid.IllegalStateException <code> hasDueDate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDueDate() {
        if (!hasDueDate()) {
            throw new org.osid.IllegalStateException("hasDueDate() is false");
        }

        return (this.dueDate);
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setDueDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "due date");
        this.dueDate = date;
        return;
    }


    /**
     *  Tests if this period supports the given record
     *  <code>Type</code>.
     *
     *  @param  periodRecordType a period record type 
     *  @return <code>true</code> if the periodRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type periodRecordType) {
        for (org.osid.billing.records.PeriodRecord record : this.records) {
            if (record.implementsRecordType(periodRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Period</code>
     *  record <code>Type</code>.
     *
     *  @param  periodRecordType the period record type 
     *  @return the period record 
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(periodRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.PeriodRecord getPeriodRecord(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.PeriodRecord record : this.records) {
            if (record.implementsRecordType(periodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(periodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this period. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param periodRecord the period record
     *  @param periodRecordType period record type
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecord</code> or
     *          <code>periodRecordType</code> is <code>null</code>
     */
            
    protected void addPeriodRecord(org.osid.billing.records.PeriodRecord periodRecord, 
                                   org.osid.type.Type periodRecordType) {

        nullarg(periodRecord, "period record");
        addRecordType(periodRecordType);
        this.records.add(periodRecord);
        
        return;
    }
}
