//
// AbstractChainQueryInspector.java
//
//     A template for making a ChainQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.chain.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for chains.
 */

public abstract class AbstractChainQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.sequencing.ChainQueryInspector {

    private final java.util.Collection<org.osid.sequencing.records.ChainQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the fifo query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getFifoTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the element <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getElementTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the antimatroid <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAntimatroidIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the antimatroid query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQueryInspector[] getAntimatroidTerms() {
        return (new org.osid.sequencing.AntimatroidQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given chain query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a chain implementing the requested record.
     *
     *  @param chainRecordType a chain record type
     *  @return the chain query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(chainRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainQueryInspectorRecord getChainQueryInspectorRecord(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.ChainQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(chainRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(chainRecordType + " is not supported");
    }


    /**
     *  Adds a record to this chain query. 
     *
     *  @param chainQueryInspectorRecord chain query inspector
     *         record
     *  @param chainRecordType chain record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addChainQueryInspectorRecord(org.osid.sequencing.records.ChainQueryInspectorRecord chainQueryInspectorRecord, 
                                                   org.osid.type.Type chainRecordType) {

        addRecordType(chainRecordType);
        nullarg(chainRecordType, "chain record type");
        this.records.add(chainQueryInspectorRecord);        
        return;
    }
}
