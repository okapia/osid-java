//
// AbstractAuthorizationRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAuthorizationRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.authorization.rules.AuthorizationRulesManager,
               org.osid.authorization.rules.AuthorizationRulesProxyManager {

    private final Types authorizationEnablerRecordTypes    = new TypeRefSet();
    private final Types authorizationEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractAuthorizationRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAuthorizationRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up authorization enablers is supported. 
     *
     *  @return <code> true </code> if authorization enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying authorization enablers is supported. 
     *
     *  @return <code> true </code> if authorization enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching authorization enablers is supported. 
     *
     *  @return <code> true </code> if authorization enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an authorization enabler administrative service is supported. 
     *
     *  @return <code> true </code> if authorization enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an authorization enabler notification service is supported. 
     *
     *  @return <code> true </code> if authorization enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an authorization enabler vault lookup service is supported. 
     *
     *  @return <code> true </code> if an authorization enabler vault lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerVault() {
        return (false);
    }


    /**
     *  Tests if an authorization enabler vault service is supported. 
     *
     *  @return <code> true </code> if authorization enabler vault assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerVaultAssignment() {
        return (false);
    }


    /**
     *  Tests if an authorization enabler vault lookup service is supported. 
     *
     *  @return <code> true </code> if an authorization enabler vault service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerSmartVault() {
        return (false);
    }


    /**
     *  Tests if an authorization enabler authorization rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an authorization enabler authorization 
     *          rule lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an authorization enabler authorization rule application 
     *  service is supported. 
     *
     *  @return <code> true </code> if authorization enabler authorization 
     *          rule application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> AuthorizationEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> AuthorizationEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthorizationEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.authorizationEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuthorizationEnabler </code> record type is 
     *  supported. 
     *
     *  @param  authorizationEnablerRecordType a <code> Type </code> 
     *          indicating an <code> AuthorizationEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerRecordType(org.osid.type.Type authorizationEnablerRecordType) {
        return (this.authorizationEnablerRecordTypes.contains(authorizationEnablerRecordType));
    }


    /**
     *  Adds support for an authorization enabler record type.
     *
     *  @param authorizationEnablerRecordType an authorization enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationEnablerRecordType</code> is <code>null</code>
     */

    protected void addAuthorizationEnablerRecordType(org.osid.type.Type authorizationEnablerRecordType) {
        this.authorizationEnablerRecordTypes.add(authorizationEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an authorization enabler record type.
     *
     *  @param authorizationEnablerRecordType an authorization enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationEnablerRecordType</code> is <code>null</code>
     */

    protected void removeAuthorizationEnablerRecordType(org.osid.type.Type authorizationEnablerRecordType) {
        this.authorizationEnablerRecordTypes.remove(authorizationEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AuthorizationEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> AuthorizationEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthorizationEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.authorizationEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AuthorizationEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  authorizationEnablerSearchRecordType a <code> Type </code> 
     *          indicating an <code> AuthorizationEnabler </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAuthorizationEnablerSearchRecordType(org.osid.type.Type authorizationEnablerSearchRecordType) {
        return (this.authorizationEnablerSearchRecordTypes.contains(authorizationEnablerSearchRecordType));
    }


    /**
     *  Adds support for an authorization enabler search record type.
     *
     *  @param authorizationEnablerSearchRecordType an authorization enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addAuthorizationEnablerSearchRecordType(org.osid.type.Type authorizationEnablerSearchRecordType) {
        this.authorizationEnablerSearchRecordTypes.add(authorizationEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an authorization enabler search record type.
     *
     *  @param authorizationEnablerSearchRecordType an authorization enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>authorizationEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeAuthorizationEnablerSearchRecordType(org.osid.type.Type authorizationEnablerSearchRecordType) {
        this.authorizationEnablerSearchRecordTypes.remove(authorizationEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler lookup service. 
     *
     *  @return an <code> AuthorizationEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerLookupSession getAuthorizationEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerLookupSession getAuthorizationEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerLookupSession getAuthorizationEnablerLookupSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerLookupSession getAuthorizationEnablerLookupSessionForVault(org.osid.id.Id vaultId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler query service. 
     *
     *  @return an <code> AuthorizationEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerQuerySession getAuthorizationEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerQuerySession getAuthorizationEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler query service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerQuerySession getAuthorizationEnablerQuerySessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerQuerySessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler query service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerQuerySession getAuthorizationEnablerQuerySessionForVault(org.osid.id.Id vaultId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerQuerySessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler search service. 
     *
     *  @return an <code> AuthorizationEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerSearchSession getAuthorizationEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerSearchSession getAuthorizationEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enablers earch service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerSearchSession getAuthorizationEnablerSearchSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerSearchSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enablers earch service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerSearchSession getAuthorizationEnablerSearchSessionForVault(org.osid.id.Id vaultId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerSearchSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler administration service. 
     *
     *  @return an <code> AuthorizationEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerAdminSession getAuthorizationEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerAdminSession getAuthorizationEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerAdminSession getAuthorizationEnablerAdminSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler administration service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerAdminSession getAuthorizationEnablerAdminSessionForVault(org.osid.id.Id vaultId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerAdminSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler notification service. 
     *
     *  @param  authorizationEnablerReceiver the notification callback 
     *  @return an <code> AuthorizationEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerNotificationSession getAuthorizationEnablerNotificationSession(org.osid.authorization.rules.AuthorizationEnablerReceiver authorizationEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler notification service. 
     *
     *  @param  authorizationEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerNotificationSession getAuthorizationEnablerNotificationSession(org.osid.authorization.rules.AuthorizationEnablerReceiver authorizationEnablerReceiver, 
                                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler notification service for the given vault. 
     *
     *  @param  authorizationEnablerReceiver the notification callback 
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no vault found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationEnablerReceiver </code> or <code> vaultId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerNotificationSession getAuthorizationEnablerNotificationSessionForVault(org.osid.authorization.rules.AuthorizationEnablerReceiver authorizationEnablerReceiver, 
                                                                                                                                   org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerNotificationSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler notification service for the given vault. 
     *
     *  @param  authorizationEnablerReceiver the notification callback 
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no vault found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          authorizationEnablerReceiver, vaultId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerNotificationSession getAuthorizationEnablerNotificationSessionForVault(org.osid.authorization.rules.AuthorizationEnablerReceiver authorizationEnablerReceiver, 
                                                                                                                                   org.osid.id.Id vaultId, 
                                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerNotificationSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup authorization 
     *  enabler/vault mappings for authorization enablers. 
     *
     *  @return an <code> AuthorizationEnablerVaultSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerVault() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerVaultSession getAuthorizationEnablerVaultSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerVaultSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup authorization 
     *  enabler/vault mappings for authorization enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerVaultSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerVault() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerVaultSession getAuthorizationEnablerVaultSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerVaultSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  authorization enablers to vaults for authorization. 
     *
     *  @return an <code> AuthorizationEnablerVaultAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerVaultAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerVaultAssignmentSession getAuthorizationEnablerVaultAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerVaultAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  authorization enablers to vaults for authorization. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerVaultAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerVaultAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerVaultAssignmentSession getAuthorizationEnablerVaultAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerVaultAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage authorization enabler 
     *  smart vaults. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationEnablerSmartVaultSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerSmartVault() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerSmartVaultSession getAuthorizationEnablerSmartVaultSession(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerSmartVaultSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage authorization enabler 
     *  smart vaults. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerSmartVaultSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerSmartVault() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerSmartVaultSession getAuthorizationEnablerSmartVaultSession(org.osid.id.Id vaultId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerSmartVaultSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler authorization mapping lookup service. 
     *
     *  @return an <code> AuthorizationEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerRuleLookupSession getAuthorizationEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler mapping lookup service for looking up the rules applied to the 
     *  vault. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerAuthorizationRuleLookup() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerRuleLookupSession getAuthorizationEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler mapping lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerRuleLookupSession getAuthorizationEnablerRuleLookupSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerRuleLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler authorization mapping lookup service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerRuleLookupSession getAuthorizationEnablerRuleLookupSessionForVault(org.osid.id.Id vaultId, 
                                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerRuleLookupSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler assignment service to apply enablers to vaults. 
     *
     *  @return an <code> AuthorizationEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerRuleApplicationSession getAuthorizationEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler authorization assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablernRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerRuleApplicationSession getAuthorizationEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler assignment service for the given vault to apply enablers to 
     *  vaults. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @return an <code> AuthorizationEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerRuleApplicationSession getAuthorizationEnablerRuleApplicationSessionForVault(org.osid.id.Id vaultId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesManager.getAuthorizationEnablerRuleApplicationSessionForVault not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the authorization 
     *  enabler authorization assignment service for the given vault. 
     *
     *  @param  vaultId the <code> Id </code> of the <code> Vault </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AuthorizationEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Vault </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> vaultId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerRuleApplicationSession getAuthorizationEnablerRuleApplicationSessionForVault(org.osid.id.Id vaultId, 
                                                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authorization.rules.AuthorizationRulesProxyManager.getAuthorizationEnablerRuleApplicationSessionForVault not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.authorizationEnablerRecordTypes.clear();
        this.authorizationEnablerRecordTypes.clear();

        this.authorizationEnablerSearchRecordTypes.clear();
        this.authorizationEnablerSearchRecordTypes.clear();

        return;
    }
}
