//
// AbstractFederatingRelationshipLookupSession.java
//
//     An abstract federating adapter for a RelationshipLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RelationshipLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRelationshipLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.relationship.RelationshipLookupSession>
    implements org.osid.relationship.RelationshipLookupSession {

    private boolean parallel = false;
    private org.osid.relationship.Family family = new net.okapia.osid.jamocha.nil.relationship.family.UnknownFamily();


    /**
     *  Constructs a new <code>AbstractFederatingRelationshipLookupSession</code>.
     */

    protected AbstractFederatingRelationshipLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.relationship.RelationshipLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Family/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Family Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFamilyId() {
        return (this.family.getId());
    }


    /**
     *  Gets the <code>Family</code> associated with this 
     *  session.
     *
     *  @return the <code>Family</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.family);
    }


    /**
     *  Sets the <code>Family</code>.
     *
     *  @param  family the family for this session
     *  @throws org.osid.NullArgumentException <code>family</code>
     *          is <code>null</code>
     */

    protected void setFamily(org.osid.relationship.Family family) {
        nullarg(family, "family");
        this.family = family;
        return;
    }


    /**
     *  Tests if this user can perform <code>Relationship</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRelationships() {
        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            if (session.canLookupRelationships()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Relationship</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRelationshipView() {
        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            session.useComparativeRelationshipView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Relationship</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRelationshipView() {
        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            session.usePlenaryRelationshipView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relationships in families which are children
     *  of this family in the family hierarchy.
     */

    @OSID @Override
    public void useFederatedFamilyView() {
        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            session.useFederatedFamilyView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this family only.
     */

    @OSID @Override
    public void useIsolatedFamilyView() {
        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            session.useIsolatedFamilyView();
        }

        return;
    }


    /**
     *  Only relationships whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveRelationshipView() {
        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            session.useEffectiveRelationshipView();
        }

        return;
    }


    /**
     *  All relationships of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRelationshipView() {
        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            session.useAnyEffectiveRelationshipView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Relationship</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Relationship</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Relationship</code> and
     *  retained for compatibility.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @param  relationshipId <code>Id</code> of the
     *          <code>Relationship</code>
     *  @return the relationship
     *  @throws org.osid.NotFoundException <code>relationshipId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>relationshipId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Relationship getRelationship(org.osid.id.Id relationshipId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            try {
                return (session.getRelationship(relationshipId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(relationshipId + " not found");
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Relationships</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  relationshipIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByIds(org.osid.id.IdList relationshipIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.relationship.relationship.MutableRelationshipList ret = new net.okapia.osid.jamocha.relationship.relationship.MutableRelationshipList();

        try (org.osid.id.IdList ids = relationshipIds) {
            while (ids.hasNext()) {
                ret.addRelationship(getRelationship(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the given
     *  relationship genus <code>Type</code> which does not include
     *  relationships of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsByGenusType(relationshipGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the given
     *  relationship genus <code>Type</code> and include any additional
     *  relationships with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByParentGenusType(org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsByParentGenusType(relationshipGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipList</code> containing the given
     *  relationship record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  relationshipRecordType a relationship record type 
     *  @return the returned <code>Relationship</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>relationshipRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByRecordType(org.osid.type.Type relationshipRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsByRecordType(relationshipRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In active mode, relationships are returned that are currently
     *  active. In any status mode, active and inactive relationships
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Relationship</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsOnDate(org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of relationships corresponding to a source
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.relationship.RelationshipList getRelationshipsForSource(org.osid.id.Id sourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsForSource(sourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of relationships corresponding to a source
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForSourceOnDate(org.osid.id.Id sourceId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsForSourceOnDate(sourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given peer <code>Id</code> and relationship genus <code>
     *  Type. Relationships </code> of any genus derived from the
     *  given genus are returned.
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer, including
     *  duplicates, or an error results if a relationship is
     *  inaccessible. Otherwise, inaccessible <code> Relationships
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId a peer <code>Id</code>
     *  @param  relationshipGenusType a relationship genus type
     *  @return the relationships
     *  @throws org.osid.NullArgumentException <code>sourceId</code>
     *          or <code>relationshipGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForSource(org.osid.id.Id sourceId,
                                                                                       org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsByGenusTypeForSource(sourceId, relationshipGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given peer <code>Id</code> and relationship genus
     *  <code>Type</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code>Relationships</code> may be omitted from
     *  the list.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param  sourceId a peer <code>Id</code>
     *  @param  relationshipGenusType a relationship genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return the relationships
     *  @throws org.osid.InvalidArgumentException <code> from is greater than
     *          to </code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>relationshipGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForSourceOnDate(org.osid.id.Id sourceId,
                                                                                             org.osid.type.Type relationshipGenusType,
                                                                                             org.osid.calendaring.DateTime from,
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsByGenusTypeForSourceOnDate(sourceId, relationshipGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of relationships corresponding to a destination
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId the <code>Id</code> of the destination
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>destinationId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.relationship.RelationshipList getRelationshipsForDestination(org.osid.id.Id destinationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsForDestination(destinationId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of relationships corresponding to a destination
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId the <code>Id</code> of the destination
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>destinationId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForDestinationOnDate(org.osid.id.Id destinationId,
                                                                                       org.osid.calendaring.DateTime from,
                                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsForDestinationOnDate(destinationId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given peer <code>Id</code> and relationship genus <code>
     *  Type. Relationships </code> of any genus derived from the
     *  given genus are returned.
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer, including
     *  duplicates, or an error results if a relationship is
     *  inaccessible. Otherwise, inaccessible <code> Relationships
     *  </code> may be omitted from the list and may present the
     *  elements in any order including returning a unique set.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId a peer <code>Id</code>
     *  @param  relationshipGenusType a relationship genus type
     *  @return the relationships
     *  @throws org.osid.NullArgumentException
     *          <code>destinationId</code> or
     *          <code>relationshipGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForDestination(org.osid.id.Id destinationId,
                                                                                            org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsByGenusTypeForDestination(destinationId, relationshipGenusType));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding to the
     *  given peer <code>Id</code> and relationship genus
     *  <code>Type</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code>Relationships</code> may be omitted from
     *  the list.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param  destinationId a peer <code>Id</code>
     *  @param  relationshipGenusType a relationship genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return the relationships
     *  @throws org.osid.InvalidArgumentException <code> from is greater than
     *          to </code>
     *  @throws org.osid.NullArgumentException <code>destinationId</code>,
     *          <code>relationshipGenusType</code>, <code>from</code>
     *          or <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForDestinationOnDate(org.osid.id.Id destinationId,
                                                                                                  org.osid.type.Type relationshipGenusType,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsByGenusTypeForDestinationOnDate(destinationId, relationshipGenusType, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of relationships corresponding to source and destination
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId the <code>Id</code> of the source
     *  @param  destinationId the <code>Id</code> of the destination
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>destinationId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForPeers(org.osid.id.Id sourceId,
                                                                           org.osid.id.Id destinationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsForPeers(sourceId, destinationId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of relationships corresponding to source and destination
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible
     *  through this session.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective.  In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  destinationId the <code>Id</code> of the destination
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RelationshipList</code>
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>destinationId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsForPeersOnDate(org.osid.id.Id sourceId,
                                                                                 org.osid.id.Id destinationId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsForPeersOnDate(sourceId, destinationId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RelationshipList</code> corresponding between the
     *  given peer <code>Ids</code> and relationship genus
     *  <code>Type</code> <code>Relationships</code> of any genus
     *  derived from the given genus are returned.
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code>Relationships</code> may be omitted from
     *  the list.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective. In any effective mode, effective
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceId a peer <code>Id</code>
     *  @param  destinationId a related peer <code>Id</code>
     *  @param  relationshipGenusType a relationship genus type
     *  @return the relationships
     *  @throws org.osid.NullArgumentException <code>sourceId</code>,
     *          <code>destinationId</code>, or
     *  <code>relationshipGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForPeers(org.osid.id.Id sourceId,
                                                                                      org.osid.id.Id destinationId,
                                                                                      org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsByGenusTypeForPeers(sourceId, destinationId, relationshipGenusType));
        }

        ret.noMore();
        return (ret);
    }

        
    /**
     *  Gets a <code>RelationshipList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all of the
     *  relationships corresponding to the given peer set or an error
     *  results if a relationship is inaccessible. Otherwise,
     *  inaccessible <code>Relationships</code> may be omitted from
     *  the list.
     *
     *  In effective mode, relationships are returned that are
     *  currently effective in addition to being effective during the
     *  given dates. In any effective mode, effective relationships
     *  and those currently expired are returned.
     *
     *  @param  sourceId a peer <code>Id</code>
     *  @param  destinationId a related peer <code>Id</code>
     *  @param  relationshipGenusType a relationship genus type
     *  @param  from starting date
     *  @param  to ending date
     *  @return the relationships
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code> sourceId, destinationId,
     *          relationshipGenusType, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationshipsByGenusTypeForPeersOnDate(org.osid.id.Id sourceId,
                                                                                            org.osid.id.Id destinationId,
                                                                                            org.osid.type.Type relationshipGenusType,
                                                                                            org.osid.calendaring.DateTime from,
                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationshipsByGenusTypeForPeersOnDate(sourceId, destinationId, relationshipGenusType, from ,to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Relationships</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned list
     *  may contain only those relationships that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, relationships are returned that are currently
     *  effective.  In any effective mode, effective relationships and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Relationships</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipList getRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList ret = getRelationshipList();

        for (org.osid.relationship.RelationshipLookupSession session : getSessions()) {
            ret.addRelationshipList(session.getRelationships());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.relationship.relationship.FederatingRelationshipList getRelationshipList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.relationship.relationship.ParallelRelationshipList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.relationship.relationship.CompositeRelationshipList());
        }
    }
}
