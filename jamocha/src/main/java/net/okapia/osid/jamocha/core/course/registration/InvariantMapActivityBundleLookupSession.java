//
// InvariantMapActivityBundleLookupSession
//
//    Implements an ActivityBundle lookup service backed by a fixed collection of
//    activityBundles.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration;


/**
 *  Implements an ActivityBundle lookup service backed by a fixed
 *  collection of activity bundles. The activity bundles are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapActivityBundleLookupSession
    extends net.okapia.osid.jamocha.core.course.registration.spi.AbstractMapActivityBundleLookupSession
    implements org.osid.course.registration.ActivityBundleLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityBundleLookupSession</code> with no
     *  activity bundles.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityBundleLookupSession</code> with a single
     *  activity bundle.
     *  
     *  @param courseCatalog the course catalog
     *  @param activityBundle an single activity bundle
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code activityBundle} is <code>null</code>
     */

      public InvariantMapActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.registration.ActivityBundle activityBundle) {
        this(courseCatalog);
        putActivityBundle(activityBundle);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityBundleLookupSession</code> using an array
     *  of activity bundles.
     *  
     *  @param courseCatalog the course catalog
     *  @param activityBundles an array of activity bundles
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code activityBundles} is <code>null</code>
     */

      public InvariantMapActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.registration.ActivityBundle[] activityBundles) {
        this(courseCatalog);
        putActivityBundles(activityBundles);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityBundleLookupSession</code> using a
     *  collection of activity bundles.
     *
     *  @param courseCatalog the course catalog
     *  @param activityBundles a collection of activity bundles
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code activityBundles} is <code>null</code>
     */

      public InvariantMapActivityBundleLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.registration.ActivityBundle> activityBundles) {
        this(courseCatalog);
        putActivityBundles(activityBundles);
        return;
    }
}
