//
// PathElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.path.path.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PathElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the PathElement Id.
     *
     *  @return the path element Id
     */

    public static org.osid.id.Id getPathEntityId() {
        return (makeEntityId("osid.topology.path.Path"));
    }


    /**
     *  Gets the StartingNodeId element Id.
     *
     *  @return the StartingNodeId element Id
     */

    public static org.osid.id.Id getStartingNodeId() {
        return (makeElementId("osid.topology.path.path.StartingNodeId"));
    }


    /**
     *  Gets the StartingNode element Id.
     *
     *  @return the StartingNode element Id
     */

    public static org.osid.id.Id getStartingNode() {
        return (makeElementId("osid.topology.path.path.StartingNode"));
    }


    /**
     *  Gets the EndingNodeId element Id.
     *
     *  @return the EndingNodeId element Id
     */

    public static org.osid.id.Id getEndingNodeId() {
        return (makeElementId("osid.topology.path.path.EndingNodeId"));
    }


    /**
     *  Gets the EndingNode element Id.
     *
     *  @return the EndingNode element Id
     */

    public static org.osid.id.Id getEndingNode() {
        return (makeElementId("osid.topology.path.path.EndingNode"));
    }


    /**
     *  Gets the Hops element Id.
     *
     *  @return the Hops element Id
     */

    public static org.osid.id.Id getHops() {
        return (makeElementId("osid.topology.path.path.Hops"));
    }


    /**
     *  Gets the Distance element Id.
     *
     *  @return the Distance element Id
     */

    public static org.osid.id.Id getDistance() {
        return (makeElementId("osid.topology.path.path.Distance"));
    }


    /**
     *  Gets the Cost element Id.
     *
     *  @return the Cost element Id
     */

    public static org.osid.id.Id getCost() {
        return (makeElementId("osid.topology.path.path.Cost"));
    }


    /**
     *  Gets the EdgeIds element Id.
     *
     *  @return the EdgeIds element Id
     */

    public static org.osid.id.Id getEdgeIds() {
        return (makeElementId("osid.topology.path.path.EdgeIds"));
    }


    /**
     *  Gets the Edges element Id.
     *
     *  @return the Edges element Id
     */

    public static org.osid.id.Id getEdges() {
        return (makeElementId("osid.topology.path.path.Edges"));
    }


    /**
     *  Gets the Complete element Id.
     *
     *  @return the Complete element Id
     */

    public static org.osid.id.Id getComplete() {
        return (makeElementId("osid.topology.path.path.Complete"));
    }


    /**
     *  Gets the Closed element Id.
     *
     *  @return the Closed element Id
     */

    public static org.osid.id.Id getClosed() {
        return (makeElementId("osid.topology.path.path.Closed"));
    }


    /**
     *  Gets the AlongNodeIds element Id.
     *
     *  @return the AlongNodeIds element Id
     */

    public static org.osid.id.Id getAlongNodeIds() {
        return (makeQueryElementId("osid.topology.path.path.AlongNodeIds"));
    }


    /**
     *  Gets the IntersectingPathId element Id.
     *
     *  @return the IntersectingPathId element Id
     */

    public static org.osid.id.Id getIntersectingPathId() {
        return (makeQueryElementId("osid.topology.path.path.IntersectingPathId"));
    }


    /**
     *  Gets the IntersectingPath element Id.
     *
     *  @return the IntersectingPath element Id
     */

    public static org.osid.id.Id getIntersectingPath() {
        return (makeQueryElementId("osid.topology.path.path.IntersectingPath"));
    }


    /**
     *  Gets the NodeId element Id.
     *
     *  @return the NodeId element Id
     */

    public static org.osid.id.Id getNodeId() {
        return (makeQueryElementId("osid.topology.path.path.NodeId"));
    }


    /**
     *  Gets the Node element Id.
     *
     *  @return the Node element Id
     */

    public static org.osid.id.Id getNode() {
        return (makeQueryElementId("osid.topology.path.path.Node"));
    }


    /**
     *  Gets the GraphId element Id.
     *
     *  @return the GraphId element Id
     */

    public static org.osid.id.Id getGraphId() {
        return (makeQueryElementId("osid.topology.path.path.GraphId"));
    }


    /**
     *  Gets the Graph element Id.
     *
     *  @return the Graph element Id
     */

    public static org.osid.id.Id getGraph() {
        return (makeQueryElementId("osid.topology.path.path.Graph"));
    }
}
