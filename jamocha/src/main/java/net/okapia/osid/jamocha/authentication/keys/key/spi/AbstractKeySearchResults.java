//
// AbstractKeySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.keys.key.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractKeySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.authentication.keys.KeySearchResults {

    private org.osid.authentication.keys.KeyList keys;
    private final org.osid.authentication.keys.KeyQueryInspector inspector;
    private final java.util.Collection<org.osid.authentication.keys.records.KeySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractKeySearchResults.
     *
     *  @param keys the result set
     *  @param keyQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>keys</code>
     *          or <code>keyQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractKeySearchResults(org.osid.authentication.keys.KeyList keys,
                                            org.osid.authentication.keys.KeyQueryInspector keyQueryInspector) {
        nullarg(keys, "keys");
        nullarg(keyQueryInspector, "key query inspectpr");

        this.keys = keys;
        this.inspector = keyQueryInspector;

        return;
    }


    /**
     *  Gets the key list resulting from a search.
     *
     *  @return a key list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.authentication.keys.KeyList getKeys() {
        if (this.keys == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.authentication.keys.KeyList keys = this.keys;
        this.keys = null;
	return (keys);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.authentication.keys.KeyQueryInspector getKeyQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  key search record <code> Type. </code> This method must
     *  be used to retrieve a key implementing the requested
     *  record.
     *
     *  @param keySearchRecordType a key search 
     *         record type 
     *  @return the key search
     *  @throws org.osid.NullArgumentException
     *          <code>keySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(keySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.keys.records.KeySearchResultsRecord getKeySearchResultsRecord(org.osid.type.Type keySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.authentication.keys.records.KeySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(keySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(keySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record key search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addKeyRecord(org.osid.authentication.keys.records.KeySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "key record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
