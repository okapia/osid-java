//
// InvariantMapProxyFiscalPeriodLookupSession
//
//    Implements a FiscalPeriod lookup service backed by a fixed
//    collection of fiscalPeriods. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials;


/**
 *  Implements a FiscalPeriod lookup service backed by a fixed
 *  collection of fiscal periods. The fiscal periods are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyFiscalPeriodLookupSession
    extends net.okapia.osid.jamocha.core.financials.spi.AbstractMapFiscalPeriodLookupSession
    implements org.osid.financials.FiscalPeriodLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyFiscalPeriodLookupSession} with no
     *  fiscal periods.
     *
     *  @param business the business
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyFiscalPeriodLookupSession(org.osid.financials.Business business,
                                                  org.osid.proxy.Proxy proxy) {
        setBusiness(business);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyFiscalPeriodLookupSession} with a single
     *  fiscal period.
     *
     *  @param business the business
     *  @param fiscalPeriod a single fiscal period
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code fiscalPeriod} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyFiscalPeriodLookupSession(org.osid.financials.Business business,
                                                  org.osid.financials.FiscalPeriod fiscalPeriod, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putFiscalPeriod(fiscalPeriod);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyFiscalPeriodLookupSession} using
     *  an array of fiscal periods.
     *
     *  @param business the business
     *  @param fiscalPeriods an array of fiscal periods
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code fiscalPeriods} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyFiscalPeriodLookupSession(org.osid.financials.Business business,
                                                  org.osid.financials.FiscalPeriod[] fiscalPeriods, org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putFiscalPeriods(fiscalPeriods);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyFiscalPeriodLookupSession} using a
     *  collection of fiscal periods.
     *
     *  @param business the business
     *  @param fiscalPeriods a collection of fiscal periods
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code fiscalPeriods} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyFiscalPeriodLookupSession(org.osid.financials.Business business,
                                                  java.util.Collection<? extends org.osid.financials.FiscalPeriod> fiscalPeriods,
                                                  org.osid.proxy.Proxy proxy) {

        this(business, proxy);
        putFiscalPeriods(fiscalPeriods);
        return;
    }
}
