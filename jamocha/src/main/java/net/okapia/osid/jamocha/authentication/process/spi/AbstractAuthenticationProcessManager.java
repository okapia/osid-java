//
// AbstractAuthenticationProcessManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.process.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAuthenticationProcessManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.authentication.process.AuthenticationProcessManager,
               org.osid.authentication.process.AuthenticationProcessProxyManager {

    private final Types authenticationRecordTypes          = new TypeRefSet();
    private final Types authenticationInputRecordTypes     = new TypeRefSet();
    private final Types challengeRecordTypes               = new TypeRefSet();
    private final Types credentialTypes                    = new TypeRefSet();
    private final Types trustTypes                         = new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractAuthenticationProcessManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAuthenticationProcessManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if authentication acquisition is supported. Authentication 
     *  acquisition is responsible for acquiring client side authentication 
     *  credentials. 
     *
     *  @return <code> true </code> if authentication acquisiiton is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationAcquisition() {
        return (false);
    }


    /**
     *  Tests if authentication validation is supported. Authentication 
     *  validation verifies given authentication credentials and maps to an 
     *  agent identity. 
     *
     *  @return <code> true </code> if authentication validation is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationValidation() {
        return (false);
    }


    /**
     *  Tests if a trust look up session is supported. 
     *
     *  @return <code> true </code> if trust lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTrustLookup() {
        return (false);
    }


    /**
     *  Tests if a session to examine agent and trust relationships is 
     *  supported. 
     *
     *  @return <code> true </code> if a circle of trust is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCircleOfTrust() {
        return (false);
    }


    /**
     *  Tests if this authentication service supports a challenge-response 
     *  mechanism where credential validation service must implement a means 
     *  to generate challenge data. 
     *
     *  @return <code> true </code> if this is a challenge-response system, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChallenge() {
        return (false);
    }


    /**
     *  Gets the supported authentication record types. 
     *
     *  @return a list containing the supported authentication record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthenticationRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.authenticationRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given authentication record type is supported. 
     *
     *  @param  authenticationRecordType a <code> Type </code> indicating an 
     *          authentication record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> authenticationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuthenticationRecordType(org.osid.type.Type authenticationRecordType) {
        return (this.authenticationRecordTypes.contains(authenticationRecordType));
    }


    /**
     *  Adds support for an authentication record type.
     *
     *  @param authenticationRecordType an authentication record type
     *  @throws org.osid.NullArgumentException
     *  <code>authenticationRecordType</code> is <code>null</code>
     */

    protected void addAuthenticationRecordType(org.osid.type.Type authenticationRecordType) {
        this.authenticationRecordTypes.add(authenticationRecordType);
        return;
    }


    /**
     *  Removes support for an authentication record type.
     *
     *  @param authenticationRecordType an authentication record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>authenticationRecordType</code> is <code>null</code>
     */

    protected void removeAuthenticationRecordType(org.osid.type.Type authenticationRecordType) {
        this.authenticationRecordTypes.remove(authenticationRecordType);
        return;
    }


    /**
     *  Gets the supported authentication input record types. 
     *
     *  @return a list containing the supported authentication input record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAuthenticationInputRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.authenticationInputRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given authentication input record type is supported. 
     *
     *  @param  authenticationInputRecordType a <code> Type </code> indicating 
     *          an authentication input record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          authenticationInputRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAuthenticationInputRecordType(org.osid.type.Type authenticationInputRecordType) {
        return (this.authenticationInputRecordTypes.contains(authenticationInputRecordType));
    }


    /**
     *  Adds support for an authentication input record type.
     *
     *  @param authenticationInputRecordType an authentication input record type
     *  @throws org.osid.NullArgumentException
     *  <code>authenticationInputRecordType</code> is <code>null</code>
     */

    protected void addAuthenticationInputRecordType(org.osid.type.Type authenticationInputRecordType) {
        this.authenticationInputRecordTypes.add(authenticationInputRecordType);
        return;
    }


    /**
     *  Removes support for an authentication input record type.
     *
     *  @param authenticationInputRecordType an authentication input record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>authenticationInputRecordType</code> is <code>null</code>
     */

    protected void removeAuthenticationInputRecordType(org.osid.type.Type authenticationInputRecordType) {
        this.authenticationInputRecordTypes.remove(authenticationInputRecordType);
        return;
    }


    /**
     *  Gets the supported challenge types. 
     *
     *  @return a list containing the supported challenge types 
     */

    @OSID @Override
    public org.osid.type.TypeList getChallengeRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.challengeRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given challenge data type is supported. 
     *
     *  @param  challengeRecordType a <code> Type </code> indicating a 
     *          challenge record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> challengeRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsChallengeRecordType(org.osid.type.Type challengeRecordType) {
        return (this.challengeRecordTypes.contains(challengeRecordType));
    }


    /**
     *  Adds support for a challenge record type.
     *
     *  @param challengeRecordType a challenge record type
     *  @throws org.osid.NullArgumentException
     *  <code>challengeRecordType</code> is <code>null</code>
     */

    protected void addChallengeRecordType(org.osid.type.Type challengeRecordType) {
        this.challengeRecordTypes.add(challengeRecordType);
        return;
    }


    /**
     *  Removes support for a challenge record type.
     *
     *  @param challengeRecordType a challenge record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>challengeRecordType</code> is <code>null</code>
     */

    protected void removeChallengeRecordType(org.osid.type.Type challengeRecordType) {
        this.challengeRecordTypes.remove(challengeRecordType);
        return;
    }


    /**
     *  Tests if <code> Authentication </code> objects can export serialzied 
     *  credentials for transport. 
     *
     *  @return <code> true </code> if the given credentials export is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialExport() {
        return (false);
    }


    /**
     *  Gets the supported credential types. 
     *
     *  @return a list containing the supported credential types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.credentialTypes.toCollection()));
    }


    /**
     *  Tests if the given credential type is supported. 
     *
     *  @param  credentialType a <code> Type </code> indicating a credential 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> credentialType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialType(org.osid.type.Type credentialType) {
        return (this.credentialTypes.contains(credentialType));
    }


    /**
     *  Adds support for a credential type.
     *
     *  @param credentialType a credential type
     *  @throws org.osid.NullArgumentException
     *  <code>credentialType</code> is <code>null</code>
     */

    protected void addCredentialType(org.osid.type.Type credentialType) {
        this.credentialTypes.add(credentialType);
        return;
    }


    /**
     *  Removes support for a credential type.
     *
     *  @param credentialType a credential type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>credentialType</code> is <code>null</code>
     */

    protected void removeCredentialType(org.osid.type.Type credentialType) {
        this.credentialTypes.remove(credentialType);
        return;
    }


    /**
     *  Gets the supported trust types. 
     *
     *  @return a list containing the supported trust types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTrustTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.trustTypes.toCollection()));
    }


    /**
     *  Tests if the given trust type is supported. 
     *
     *  @param  trustType a <code> Type </code> indicating a trust type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> trustType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTrustType(org.osid.type.Type trustType) {
        return (this.trustTypes.contains(trustType));
    }


    /**
     *  Adds support for a trust type.
     *
     *  @param trustType a trust type
     *  @throws org.osid.NullArgumentException
     *  <code>trustType</code> is <code>null</code>
     */

    protected void addTrustType(org.osid.type.Type trustType) {
        this.trustTypes.add(trustType);
        return;
    }


    /**
     *  Removes support for a trust type.
     *
     *  @param trustType a trust type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>trustType</code> is <code>null</code>
     */

    protected void removeTrustType(org.osid.type.Type trustType) {
        this.trustTypes.remove(trustType);
        return;
    }


    /**
     *  Gets an <code> AuthenticationAcquisitionSession </code> which is 
     *  responsible for acquiring authentication credentials on behalf of a 
     *  service client. 
     *
     *  @return an acquisition session for this service 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationAcquisition() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.AuthenticationAcquisitionSession getAuthenticationAcquisitionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessManager.getAuthenticationAcquisitionSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the <code> 
     *  AuthenticationAcquisitionSession </code> using the supplied <code> 
     *  Authentication. </code> 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthenticationAcquisitionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationAcquisition() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.AuthenticationAcquisitionSession getAuthenticationAcquisitionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessProxyManager.getAuthenticationAcquisitionSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the <code> 
     *  AuthenticationValidation </code> service. 
     *
     *  @return an <code> AuthenticationValidationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationValidation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.AuthenticationValidationSession getAuthenticationValidationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessManager.getAuthenticationValidationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the <code> 
     *  AuthenticationValidation </code> service using the supplied <code> 
     *  Authentication. </code> 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AuthenticationValidationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationValidation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.AuthenticationValidationSession getAuthenticationValidationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessProxyManager.getAuthenticationValidationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust lookup 
     *  service. 
     *
     *  @return a <code> TrustLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTrustLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustLookupSession getTrustLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessManager.getTrustLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TrustLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTrustLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustLookupSession getTrustLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessProxyManager.getTrustLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust lookup 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return a <code> TrustLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsTrustLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustLookupSession getTrustLookupSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessManager.getTrustLookupSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust lookup 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return a <code> TrustLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsTrustLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustLookupSession getTrustLookupSessionForAgency(org.osid.id.Id agencyId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessProxyManager.getTrustLookupSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust circle 
     *  service. 
     *
     *  @return a <code> CircleOfTrustSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCircleOfTrust() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.CircleOfTrustSession getCircleOfTrustSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessManager.getCircleOfTrustSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust circle 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CircleOfTrustSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCircleOfTrust() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.CircleOfTrustSession getCircleOfTrustSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessProxyManager.getCircleOfTrustSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust circle 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @return a <code> CircleOfTrustSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsCiirleOfTrust() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.CircleOfTrustSession getCircleOfTrustSessionForAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessManager.getCircleOfTrustSessionForAgency not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trust circle 
     *  service for the given agency. 
     *
     *  @param  agencyId the <code> Id </code> of the agency 
     *  @param  proxy a proxy 
     *  @return a <code> CircleOfTrustSession </code> 
     *  @throws org.osid.NotFoundException <code> agencyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsCiirleOfTrust() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.CircleOfTrustSession getCircleOfTrustSessionForAgency(org.osid.id.Id agencyId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.authentication.process.AuthenticationProcessProxyManager.getCircleOfTrustSessionForAgency not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.authenticationRecordTypes.clear();
        this.authenticationRecordTypes.clear();

        this.authenticationInputRecordTypes.clear();
        this.authenticationInputRecordTypes.clear();

        this.challengeRecordTypes.clear();
        this.challengeRecordTypes.clear();

        this.credentialTypes.clear();
        this.credentialTypes.clear();

        this.trustTypes.clear();
        this.trustTypes.clear();

        return;
    }
}
