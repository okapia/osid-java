//
// AbstractNodeBookHierarchySession.java
//
//     Defines a Book hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a book hierarchy session for delivering a hierarchy
 *  of books using the BookNode interface.
 */

public abstract class AbstractNodeBookHierarchySession
    extends net.okapia.osid.jamocha.commenting.spi.AbstractBookHierarchySession
    implements org.osid.commenting.BookHierarchySession {

    private java.util.Collection<org.osid.commenting.BookNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root book <code> Ids </code> in this hierarchy.
     *
     *  @return the root book <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootBookIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.commenting.booknode.BookNodeToIdList(this.roots));
    }


    /**
     *  Gets the root books in the book hierarchy. A node
     *  with no parents is an orphan. While all book <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root books 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getRootBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.commenting.booknode.BookNodeToBookList(new net.okapia.osid.jamocha.commenting.booknode.ArrayBookNodeList(this.roots)));
    }


    /**
     *  Adds a root book node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootBook(org.osid.commenting.BookNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root book nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootBooks(java.util.Collection<org.osid.commenting.BookNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root book node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootBook(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.commenting.BookNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Book </code> has any parents. 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @return <code> true </code> if the book has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentBooks(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getBookNode(bookId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  book.
     *
     *  @param  id an <code> Id </code> 
     *  @param  bookId the <code> Id </code> of a book 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> bookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> bookId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfBook(org.osid.id.Id id, org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.commenting.BookNodeList parents = getBookNode(bookId).getParentBookNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextBookNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given book. 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @return the parent <code> Ids </code> of the book 
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentBookIds(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.commenting.book.BookToIdList(getParentBooks(bookId)));
    }


    /**
     *  Gets the parents of the given book. 
     *
     *  @param  bookId the <code> Id </code> to query 
     *  @return the parents of the book 
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getParentBooks(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.commenting.booknode.BookNodeToBookList(getBookNode(bookId).getParentBookNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  book.
     *
     *  @param  id an <code> Id </code> 
     *  @param  bookId the Id of a book 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> bookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfBook(org.osid.id.Id id, org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBook(id, bookId)) {
            return (true);
        }

        try (org.osid.commenting.BookList parents = getParentBooks(bookId)) {
            while (parents.hasNext()) {
                if (isAncestorOfBook(id, parents.getNextBook().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a book has any children. 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @return <code> true </code> if the <code> bookId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildBooks(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBookNode(bookId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  book.
     *
     *  @param  id an <code> Id </code> 
     *  @param bookId the <code> Id </code> of a 
     *         book
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> bookId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> bookId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfBook(org.osid.id.Id id, org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfBook(bookId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  book.
     *
     *  @param  bookId the <code> Id </code> to query 
     *  @return the children of the book 
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildBookIds(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.commenting.book.BookToIdList(getChildBooks(bookId)));
    }


    /**
     *  Gets the children of the given book. 
     *
     *  @param  bookId the <code> Id </code> to query 
     *  @return the children of the book 
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getChildBooks(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.commenting.booknode.BookNodeToBookList(getBookNode(bookId).getChildBookNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  book.
     *
     *  @param  id an <code> Id </code> 
     *  @param bookId the <code> Id </code> of a 
     *         book
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> bookId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfBook(org.osid.id.Id id, org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfBook(bookId, id)) {
            return (true);
        }

        try (org.osid.commenting.BookList children = getChildBooks(bookId)) {
            while (children.hasNext()) {
                if (isDescendantOfBook(id, children.getNextBook().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  book.
     *
     *  @param  bookId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified book node 
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getBookNodeIds(org.osid.id.Id bookId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.commenting.booknode.BookNodeToNode(getBookNode(bookId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given book.
     *
     *  @param  bookId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified book node 
     *  @throws org.osid.NotFoundException <code> bookId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> bookId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookNode getBookNodes(org.osid.id.Id bookId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBookNode(bookId));
    }


    /**
     *  Closes this <code>BookHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a book node.
     *
     *  @param bookId the id of the book node
     *  @throws org.osid.NotFoundException <code>bookId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>bookId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.commenting.BookNode getBookNode(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(bookId, "book Id");
        for (org.osid.commenting.BookNode book : this.roots) {
            if (book.getId().equals(bookId)) {
                return (book);
            }

            org.osid.commenting.BookNode r = findBook(book, bookId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(bookId + " is not found");
    }


    protected org.osid.commenting.BookNode findBook(org.osid.commenting.BookNode node, 
                                                            org.osid.id.Id bookId) 
        throws org.osid.OperationFailedException {

        try (org.osid.commenting.BookNodeList children = node.getChildBookNodes()) {
            while (children.hasNext()) {
                org.osid.commenting.BookNode book = children.getNextBookNode();
                if (book.getId().equals(bookId)) {
                    return (book);
                }
                
                book = findBook(book, bookId);
                if (book != null) {
                    return (book);
                }
            }
        }

        return (null);
    }
}
