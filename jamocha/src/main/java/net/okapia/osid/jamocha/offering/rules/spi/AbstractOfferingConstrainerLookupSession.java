//
// AbstractOfferingConstrainerLookupSession.java
//
//    A starter implementation framework for providing an OfferingConstrainer
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an OfferingConstrainer
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getOfferingConstrainers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractOfferingConstrainerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.offering.rules.OfferingConstrainerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.offering.Catalogue catalogue = new net.okapia.osid.jamocha.nil.offering.catalogue.UnknownCatalogue();
    

    /**
     *  Gets the <code>Catalogue/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Catalogue Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCatalogueId() {
        return (this.catalogue.getId());
    }


    /**
     *  Gets the <code>Catalogue</code> associated with this 
     *  session.
     *
     *  @return the <code>Catalogue</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.catalogue);
    }


    /**
     *  Sets the <code>Catalogue</code>.
     *
     *  @param  catalogue the catalogue for this session
     *  @throws org.osid.NullArgumentException <code>catalogue</code>
     *          is <code>null</code>
     */

    protected void setCatalogue(org.osid.offering.Catalogue catalogue) {
        nullarg(catalogue, "catalogue");
        this.catalogue = catalogue;
        return;
    }


    /**
     *  Tests if this user can perform <code>OfferingConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOfferingConstrainers() {
        return (true);
    }


    /**
     *  A complete view of the <code>OfferingConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeOfferingConstrainerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>OfferingConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryOfferingConstrainerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include offering constrainers in catalogues which are children
     *  of this catalogue in the catalogue hierarchy.
     */

    @OSID @Override
    public void useFederatedCatalogueView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this catalogue only.
     */

    @OSID @Override
    public void useIsolatedCatalogueView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active offering constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveOfferingConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive offering constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusOfferingConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>OfferingConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>OfferingConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>OfferingConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  @param  offeringConstrainerId <code>Id</code> of the
     *          <code>OfferingConstrainer</code>
     *  @return the offering constrainer
     *  @throws org.osid.NotFoundException <code>offeringConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainer getOfferingConstrainer(org.osid.id.Id offeringConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.offering.rules.OfferingConstrainerList offeringConstrainers = getOfferingConstrainers()) {
            while (offeringConstrainers.hasNext()) {
                org.osid.offering.rules.OfferingConstrainer offeringConstrainer = offeringConstrainers.getNextOfferingConstrainer();
                if (offeringConstrainer.getId().equals(offeringConstrainerId)) {
                    return (offeringConstrainer);
                }
            }
        } 

        throw new org.osid.NotFoundException(offeringConstrainerId + " not found");
    }


    /**
     *  Gets an <code>OfferingConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  offeringConstrainers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>OfferingConstrainers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getOfferingConstrainers()</code>.
     *
     *  @param  offeringConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>OfferingConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByIds(org.osid.id.IdList offeringConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.offering.rules.OfferingConstrainer> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = offeringConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getOfferingConstrainer(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("offering constrainer " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.offering.rules.offeringconstrainer.LinkedOfferingConstrainerList(ret));
    }


    /**
     *  Gets an <code>OfferingConstrainerList</code> corresponding to the given
     *  offering constrainer genus <code>Type</code> which does not include
     *  offering constrainers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getOfferingConstrainers()</code>.
     *
     *  @param  offeringConstrainerGenusType an offeringConstrainer genus type 
     *  @return the returned <code>OfferingConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByGenusType(org.osid.type.Type offeringConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.rules.offeringconstrainer.OfferingConstrainerGenusFilterList(getOfferingConstrainers(), offeringConstrainerGenusType));
    }


    /**
     *  Gets an <code>OfferingConstrainerList</code> corresponding to the given
     *  offering constrainer genus <code>Type</code> and include any additional
     *  offering constrainers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOfferingConstrainers()</code>.
     *
     *  @param  offeringConstrainerGenusType an offeringConstrainer genus type 
     *  @return the returned <code>OfferingConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByParentGenusType(org.osid.type.Type offeringConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getOfferingConstrainersByGenusType(offeringConstrainerGenusType));
    }


    /**
     *  Gets an <code>OfferingConstrainerList</code> containing the given
     *  offering constrainer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  offering constrainers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getOfferingConstrainers()</code>.
     *
     *  @param  offeringConstrainerRecordType an offeringConstrainer record type 
     *  @return the returned <code>OfferingConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainersByRecordType(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.offering.rules.offeringconstrainer.OfferingConstrainerRecordFilterList(getOfferingConstrainers(), offeringConstrainerRecordType));
    }


    /**
     *  Gets all <code>OfferingConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  offering constrainers or an error results. Otherwise, the returned list
     *  may contain only those offering constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, offering constrainers are returned that are currently
     *  active. In any status mode, active and inactive offering constrainers
     *  are returned.
     *
     *  @return a list of <code>OfferingConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.offering.rules.OfferingConstrainerList getOfferingConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the offering constrainer list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of offering constrainers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.offering.rules.OfferingConstrainerList filterOfferingConstrainersOnViews(org.osid.offering.rules.OfferingConstrainerList list)
        throws org.osid.OperationFailedException {

        org.osid.offering.rules.OfferingConstrainerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.offering.rules.offeringconstrainer.ActiveOfferingConstrainerFilterList(ret);
        }

        return (ret);
    }
}
