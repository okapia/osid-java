//
// AbstractParameter.java
//
//     Defines a Parameter.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.configuration.parameter.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Parameter</code>.
 */

public abstract class AbstractParameter
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.configuration.Parameter {

    private org.osid.Syntax syntax;
    private org.osid.type.Type coordinateType;
    private org.osid.type.Type headingType;
    private org.osid.type.Type spatialUnitRecordType;
    private org.osid.type.Type objectType;
    private org.osid.type.Type versionType;
    private boolean shuffled = false;

    private final java.util.Collection<org.osid.configuration.records.ParameterRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the syntax for the values of this parameter. 
     *
     *  @return the syntax of the values 
     */

    @OSID @Override
    public org.osid.Syntax getValueSyntax() {
        return (this.syntax);
    }


    /**
     *  Sets the value syntax.
     *
     *  @param syntax a value syntax
     *  @throws org.osid.NullArgumentException
     *          <code>syntax</code> is <code>null</code>
     */

    protected void setValueSyntax(org.osid.Syntax syntax) {
        nullarg(syntax, "value syntax");
        this.syntax = syntax;
        return;
    }


    /**
     *  Gets the type of the value if the syntax is a coordinate. 
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax()
     *          != COORDINATE </code>
     */

    @OSID @Override
    public org.osid.type.Type getValueCoordinateType() {
        return (this.coordinateType);
    }


    /**
     *  Sets the coordinate type.
     *
     *  @param coordinateType a coordinate type
     *  @throws org.osid.NullArgumentException
     *          <code>coordinateType</code> is <code>null</code>
     */

    protected void setValueCoordinateType(org.osid.type.Type coordinateType) {
        nullarg(coordinateType, "coordinate type");
        this.coordinateType = coordinateType;
        return;
    }


    /**
     *  Tests if the coordinate supports the given coordinate <code> Type. </code> 
     *
     *  @param  coordinateType a coordinate type 
     *  @return <code> true </code> if the coordinate values associated with 
     *          this parameter implement the given coordinate <code> Type, </code> 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          COORDINATE </code> 
     *  @throws org.osid.NullArgumentException <code> coordinateType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueCoordinateType(org.osid.type.Type coordinateType) {
        return (this.coordinateType.equals(coordinateType));
    }


    /**
     *  Gets the type of the value if the syntax is a heading. 
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax()
     *          != HEADING </code>
     */

    @OSID @Override
    public org.osid.type.Type getValueHeadingType() {
        return (this.headingType);
    }


    /**
     *  Sets the heading type.
     *
     *  @param headingType a heading type
     *  @throws org.osid.NullArgumentException
     *          <code>headingType</code> is <code>null</code>
     */

    protected void setValueHeadingType(org.osid.type.Type headingType) {
        nullarg(headingType, "heading type");
        this.headingType = headingType;
        return;
    }


    /**
     *  Tests if the heading supports the given heading <code> Type. </code> 
     *
     *  @param  headingType a heading type 
     *  @return <code> true </code> if the heading values associated with 
     *          this parameter implement the given heading <code> Type, </code> 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          HEADING </code> 
     *  @throws org.osid.NullArgumentException <code> headingType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueHeadingType(org.osid.type.Type headingType) {
        return (this.headingType.equals(headingType));
    }


    /**
     *  Gets the type of the value if the syntax is a spatial unit.
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.type.Type getValueSpatialUnitRecordType() {
        return (this.spatialUnitRecordType);
    }


    /**
     *  Sets the spatial unit record type.
     *
     *  @param spatialUnitRecordType a spatial unit record type
     *  @throws org.osid.NullArgumentException
     *          <code>spatialUnitRecordType</code> is
     *          <code>null</code>
     */

    protected void setValueSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        nullarg(spatialUnitRecordType, "spaticl unit record type");
        this.spatialUnitRecordType = spatialUnitRecordType;
        return;
    }


    /**
     *  Tests if the spatial unit supports the given record <code>
     *  Type.  </code>
     *
     *  @param  spatialUnitRecordType a spatial unit record type 
     *  @return <code> true </code> if the spatial unit values associated with 
     *          this parameter implement the given record <code> Type, </code> 
     *          <code> false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax()
     *          != SPATIALUNIT </code>
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordTYpe 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        return (this.spatialUnitRecordType.equals(spatialUnitRecordType));
    }


    /**
     *  Gets the type of the value if the syntax is an object. 
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          OBJECT </code> 
     */

    @OSID @Override
    public org.osid.type.Type getValueObjectType() {
        return (this.objectType);
    }


    /**
     *  Sets the object type.
     *
     *  @param objectType an object type
     *  @throws org.osid.NullArgumentException
     *          <code>objectType</code> is <code>null</code>
     */

    protected void setValueObjectType(org.osid.type.Type objectType) {
        nullarg(objectType, "object type");
        this.objectType = objectType;
        return;
    }


    /**
     *  Tests if the object supports the given <code> Type. </code> This 
     *  method should be checked before retrieving the object value. 
     *
     *  @param  objectType a type 
     *  @return <code> true </code> if the obect values associated with this 
     *          parameter implement the given <code> Type, </code> <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          OBJECT </code> 
     *  @throws org.osid.NullArgumentException <code> objectType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueObjectType(org.osid.type.Type objectType) {
        return (this.objectType.equals(objectType));
    }


    /**
     *  Gets the type of the value if the syntax is a version. 
     *
     *  @return the type of the values 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          VERSION </code> 
     */

    @OSID @Override
    public org.osid.type.Type getValueVersionScheme() {
        return (this.versionType);
    }


    /**
     *  Sets the version scheme.
     *
     *  @param versionType an version scheme
     *  @throws org.osid.NullArgumentException
     *          <code>versionType</code> is <code>null</code>
     */

    protected void setValueVersionScheme(org.osid.type.Type versionType) {
        nullarg(versionType, "version type");
        this.versionType = versionType;
        return;
    }


    /**
     *  Tests if the version supports the given <code> Type. </code> This 
     *  method should be checked before retrieving the version value. 
     *
     *  @param  versionType a type 
     *  @return <code> true </code> if the obect values associated with this 
     *          parameter implement the given <code> Type, </code> <code> 
     *          false </code> otherwise 
     *  @throws org.osid.IllegalStateException <code> getValueSyntax() != 
     *          VERSION </code> 
     *  @throws org.osid.NullArgumentException <code> versionType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean implementsValueVersionScheme(org.osid.type.Type versionType) {
        return (this.versionType.equals(versionType));
    }


    /**
     *  Tests if if the values assigned to this parameter will be
     *  shuffled or values are sorted by index.
     *
     *  @return <code> true </code> if the values are shuffled, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean areValuesShuffled() {
        return (this.shuffled);
    }


    /**
     *  Sets the shuffled flag.
     *
     *  @param shuffled <code> true </code> if the values are
     *         shuffled, <code> false </code> otherwise
     */

    protected void setShuffled(boolean shuffled) {
        this.shuffled = shuffled;
        return;
    }


    /**
     *  Tests if this parameter supports the given record
     *  <code>Type</code>.
     *
     *  @param  parameterRecordType a parameter record type 
     *  @return <code>true</code> if the parameterRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type parameterRecordType) {
        for (org.osid.configuration.records.ParameterRecord record : this.records) {
            if (record.implementsRecordType(parameterRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Parameter</code> record <code>Type</code>.
     *
     *  @param  parameterRecordType the parameter record type 
     *  @return the parameter record 
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(parameterRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.configuration.records.ParameterRecord getParameterRecord(org.osid.type.Type parameterRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.configuration.records.ParameterRecord record : this.records) {
            if (record.implementsRecordType(parameterRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(parameterRecordType + " is not supported");
    }


    /**
     *  Adds a record to this parameter. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param parameterRecord the parameter record
     *  @param parameterRecordType parameter record type
     *  @throws org.osid.NullArgumentException
     *          <code>parameterRecord</code> or
     *          <code>parameterRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addParameterRecord(org.osid.configuration.records.ParameterRecord parameterRecord, 
                                      org.osid.type.Type parameterRecordType) {
        
        nullarg(parameterRecord, "parameter record");
        addRecordType(parameterRecordType);
        this.records.add(parameterRecord);
        
        return;
    }
}
