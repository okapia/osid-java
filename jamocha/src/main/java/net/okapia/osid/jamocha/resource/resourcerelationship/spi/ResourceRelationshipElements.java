//
// ResourceRelationshipElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resourcerelationship.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ResourceRelationshipElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ResourceRelationshipElement Id.
     *
     *  @return the resource relationship element Id
     */

    public static org.osid.id.Id getResourceRelationshipEntityId() {
        return (makeEntityId("osid.resource.ResourceRelationship"));
    }


    /**
     *  Gets the SourceResourceId element Id.
     *
     *  @return the SourceResourceId element Id
     */

    public static org.osid.id.Id getSourceResourceId() {
        return (makeElementId("osid.resource.resourcerelationship.SourceResourceId"));
    }


    /**
     *  Gets the SourceResource element Id.
     *
     *  @return the SourceResource element Id
     */

    public static org.osid.id.Id getSourceResource() {
        return (makeElementId("osid.resource.resourcerelationship.SourceResource"));
    }


    /**
     *  Gets the DestinationResourceId element Id.
     *
     *  @return the DestinationResourceId element Id
     */

    public static org.osid.id.Id getDestinationResourceId() {
        return (makeElementId("osid.resource.resourcerelationship.DestinationResourceId"));
    }


    /**
     *  Gets the DestinationResource element Id.
     *
     *  @return the DestinationResource element Id
     */

    public static org.osid.id.Id getDestinationResource() {
        return (makeElementId("osid.resource.resourcerelationship.DestinationResource"));
    }


    /**
     *  Gets the SameResource element Id.
     *
     *  @return the SameResource element Id
     */

    public static org.osid.id.Id getSameResource() {
        return (makeQueryElementId("osid.resource.resourcerelationship.SameResource"));
    }


    /**
     *  Gets the BinId element Id.
     *
     *  @return the BinId element Id
     */

    public static org.osid.id.Id getBinId() {
        return (makeQueryElementId("osid.resource.resourcerelationship.BinId"));
    }


    /**
     *  Gets the Bin element Id.
     *
     *  @return the Bin element Id
     */

    public static org.osid.id.Id getBin() {
        return (makeQueryElementId("osid.resource.resourcerelationship.Bin"));
    }
}
