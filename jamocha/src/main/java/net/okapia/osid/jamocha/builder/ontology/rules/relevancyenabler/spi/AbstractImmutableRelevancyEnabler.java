//
// AbstractImmutableRelevancyEnabler.java
//
//     Wraps a mutable RelevancyEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ontology.rules.relevancyenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>RelevancyEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized RelevancyEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying relevancyEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableRelevancyEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.ontology.rules.RelevancyEnabler {

    private final org.osid.ontology.rules.RelevancyEnabler relevancyEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableRelevancyEnabler</code>.
     *
     *  @param relevancyEnabler the relevancy enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>relevancyEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableRelevancyEnabler(org.osid.ontology.rules.RelevancyEnabler relevancyEnabler) {
        super(relevancyEnabler);
        this.relevancyEnabler = relevancyEnabler;
        return;
    }


    /**
     *  Gets the relevancy enabler record corresponding to the given <code> 
     *  RelevancyEnabler </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> relevancyEnablerRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(relevancyEnablerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  relevancyEnablerRecordType the type of relevancy enabler 
     *          record to retrieve 
     *  @return the relevancy enabler record 
     *  @throws org.osid.NullArgumentException <code> 
     *          relevancyEnablerRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(relevancyEnablerRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.ontology.rules.records.RelevancyEnablerRecord getRelevancyEnablerRecord(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.relevancyEnabler.getRelevancyEnablerRecord(relevancyEnablerRecordType));
    }
}

