//
// AbstractImmutableConferral.java
//
//     Wraps a mutable Conferral to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recognition.conferral.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Conferral</code> to hide modifiers. This
 *  wrapper provides an immutized Conferral from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying conferral whose state changes are visible.
 */

public abstract class AbstractImmutableConferral
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.recognition.Conferral {

    private final org.osid.recognition.Conferral conferral;


    /**
     *  Constructs a new <code>AbstractImmutableConferral</code>.
     *
     *  @param conferral the conferral to immutablize
     *  @throws org.osid.NullArgumentException <code>conferral</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableConferral(org.osid.recognition.Conferral conferral) {
        super(conferral);
        this.conferral = conferral;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the subscriber's award. 
     *
     *  @return the subscriber <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAwardId() {
        return (this.conferral.getAwardId());
    }


    /**
     *  Gets the subscriber's award. 
     *
     *  @return the subscriber's award. 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward()
        throws org.osid.OperationFailedException {

        return (this.conferral.getAward());
    }


    /**
     *  Gets the <code> Ids </code> of the recipient.
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRecipientId() {
        return (this.conferral.getRecipientId());
    }


    /**
     *  Gets the recipient (e.g. Samuel Goldwyn). 
     *
     *  @return the resource
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getRecipient()
        throws org.osid.OperationFailedException {

        return (this.conferral.getRecipient());
    }


    /**
     *  Tests if the was conferred for a reference object. 
     *
     *  @return <code> true </code> if a reference exists, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasReference() {
        return (this.conferral.hasReference());
    }


    /**
     *  Gets the <code> Id </code> of the reference (e.g. "Best Years of Our 
     *  Lives"). 
     *
     *  @return the reference <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasReference() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        return (this.conferral.getReferenceId());
    }


    /**
     *  Tests if the award was conferred as part of a convocation. 
     *
     *  @return <code> true </code> if a convocation exists, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasConvocation() {
        return (this.conferral.hasConvocation());
    }


    /**
     *  Gets the <code> Id </code> of the convocation. 
     *
     *  @return the convocation <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasConvocation() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getConvocationId() {
        return (this.conferral.getConvocationId());
    }


    /**
     *  Gets the convocation (e.g. 19th Academy Awards for the time period 
     *  1946). 
     *
     *  @return the convocation 
     *  @throws org.osid.IllegalStateException <code> hasConvocation() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recognition.Convocation getConvocation()
        throws org.osid.OperationFailedException {

        return (this.conferral.getConvocation());
    }


    /**
     *  Gets the conferral record corresponding to the given <code> Conferral 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  conferralRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(conferralRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  conferralRecordType the type of conferral record to retrieve 
     *  @return the conferral record 
     *  @throws org.osid.NullArgumentException <code> conferralRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(conferralRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.records.ConferralRecord getConferralRecord(org.osid.type.Type conferralRecordType)
        throws org.osid.OperationFailedException {

        return (this.conferral.getConferralRecord(conferralRecordType));
    }
}

