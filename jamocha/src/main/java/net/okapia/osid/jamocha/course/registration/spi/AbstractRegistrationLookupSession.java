//
// AbstractRegistrationLookupSession.java
//
//    A starter implementation framework for providing a Registration
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Registration
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRegistrations(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRegistrationLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.registration.RegistrationLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>Registration</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRegistrations() {
        return (true);
    }


    /**
     *  A complete view of the <code>Registration</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRegistrationView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Registration</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRegistrationView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include registrations in course catalogs which are
     *  children of this course catalog in the course catalog
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only registrations whose effective dates are current are
     *  returned by methods in this session.
     */

    @OSID @Override
    public void useEffectiveRegistrationView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All registrations of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveRegistrationView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Registration</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Registration</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Registration</code> and
     *  retained for compatibility.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @param  registrationId <code>Id</code> of the
     *          <code>Registration</code>
     *  @return the registration
     *  @throws org.osid.NotFoundException <code>registrationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>registrationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.Registration getRegistration(org.osid.id.Id registrationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.registration.RegistrationList registrations = getRegistrations()) {
            while (registrations.hasNext()) {
                org.osid.course.registration.Registration registration = registrations.getNextRegistration();
                if (registration.getId().equals(registrationId)) {
                    return (registration);
                }
            }
        } 

        throw new org.osid.NotFoundException(registrationId + " not found");
    }


    /**
     *  Gets a <code>RegistrationList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  registrations specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>Registrations</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRegistrations()</code>.
     *
     *  @param  registrationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Registration</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>registrationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByIds(org.osid.id.IdList registrationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.registration.Registration> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = registrationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRegistration(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("registration " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.registration.registration.LinkedRegistrationList(ret));
    }


    /**
     *  Gets a <code>RegistrationList</code> corresponding to the
     *  given registration genus <code>Type</code> which does not
     *  include registrations of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRegistrations()</code>.
     *
     *  @param  registrationGenusType a registration genus type 
     *  @return the returned <code>Registration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>registrationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByGenusType(org.osid.type.Type registrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.RegistrationGenusFilterList(getRegistrations(), registrationGenusType));
    }


    /**
     *  Gets a <code>RegistrationList</code> corresponding to the given
     *  registration genus <code>Type</code> and include any additional
     *  registrations with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRegistrations()</code>.
     *
     *  @param  registrationGenusType a registration genus type 
     *  @return the returned <code>Registration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>registrationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByParentGenusType(org.osid.type.Type registrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRegistrationsByGenusType(registrationGenusType));
    }


    /**
     *  Gets a <code>RegistrationList</code> containing the given
     *  registration record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRegistrations()</code>.
     *
     *  @param  registrationRecordType a registration record type 
     *  @return the returned <code>Registration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>registrationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsByRecordType(org.osid.type.Type registrationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.RegistrationRecordFilterList(getRegistrations(), registrationRecordType));
    }


    /**
     *  Gets a <code>RegistrationList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *  
     *  In active mode, registrations are returned that are currently
     *  active. In any status mode, active and inactive registrations
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Registration</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsOnDate(org.osid.calendaring.DateTime from, 
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.TemporalRegistrationFilterList(getRegistrations(), from, to));
    }
        

    /**
     *  Gets a list of registrations corresponding to an activity
     *  bundle <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  activityBundleId the <code>Id</code> of the activity bundle
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundle(org.osid.id.Id activityBundleId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.RegistrationFilterList(new ActivityBundleFilter(activityBundleId), getRegistrations()));
    }


    /**
     *  Gets a list of registrations corresponding to an activity
     *  bundle <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  activityBundleId the <code>Id</code> of the activity bundle
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundleOnDate(org.osid.id.Id activityBundleId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.TemporalRegistrationFilterList(getRegistrationsForActivityBundle(activityBundleId), from, to));
    }


    /**
     *  Gets a list of registrations corresponding to a student
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.RegistrationList getRegistrationsForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.RegistrationFilterList(new StudentFilter(resourceId), getRegistrations()));
    }


    /**
     *  Gets a list of registrations corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForStudentOnDate(org.osid.id.Id resourceId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.TemporalRegistrationFilterList(getRegistrationsForStudent(resourceId), from, to));
    }


    /**
     *  Gets a list of registrations corresponding to an activity
     *  bundle and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  activityBundleId the <code>Id</code> of the activity bundle
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundleAndStudent(org.osid.id.Id activityBundleId,
                                                                                                     org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.RegistrationFilterList(new StudentFilter(resourceId), getRegistrationsForActivityBundle(activityBundleId)));
    }


    /**
     *  Gets a list of registrations corresponding to an activity
     *  bundle and student <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityBundleId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForActivityBundleAndStudentOnDate(org.osid.id.Id activityBundleId,
                                                                                                           org.osid.id.Id resourceId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.TemporalRegistrationFilterList(getRegistrationsForActivityBundleAndStudent(activityBundleId, resourceId), from, to));
    }



    /**
     *  Gets a list of registrations corresponding to a course
     *  offering <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.RegistrationList getRegistrationsForCourseOffering(org.osid.id.Id courseOfferingId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.registration.Registration> ret = new java.util.ArrayList<>();

        try (org.osid.course.registration.RegistrationList registrations = getRegistrations()) {
            while (registrations.hasNext()) {
                org.osid.course.registration.Registration registration = registrations.getNextRegistration();
                if (registration.getActivityBundle().getCourseOfferingId().equals(courseOfferingId)) {
                    ret.add(registration);
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.registration.registration.LinkedRegistrationList(ret));
    }


    /**
     *  Gets a list of registrations corresponding to a course
     *  offering <code>Id</code> and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOfferingOnDate(org.osid.id.Id courseOfferingId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.TemporalRegistrationFilterList(getRegistrationsForCourseOffering(courseOfferingId), from, to));
    }


    /**
     *  Gets a list of registrations corresponding to a course
     *  offering and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOfferingAndStudent(org.osid.id.Id courseOfferingId,
                                                                                                     org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.RegistrationFilterList(new StudentFilter(resourceId), getRegistrationsForCourseOffering(courseOfferingId)));
    }


    /**
     *  Gets a list of registrations corresponding to a course
     *  offering and student <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible
     *  through this session.
     *
     *  In effective mode, registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  registrations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>RegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrationsForCourseOfferingAndStudentOnDate(org.osid.id.Id courseOfferingId,
                                                                                                           org.osid.id.Id resourceId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.registration.registration.TemporalRegistrationFilterList(getRegistrationsForCourseOfferingAndStudent(courseOfferingId, resourceId), from, to));
    }


    /**
     *  Gets all <code>Registrations</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  registrations or an error results. Otherwise, the returned list
     *  may contain only those registrations that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, registrations are returned that are currently
     *  effective.  In any effective mode, effective registrations and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Registrations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.registration.RegistrationList getRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the registration list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of registrations
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.registration.RegistrationList filterRegistrationsOnViews(org.osid.course.registration.RegistrationList list)
        throws org.osid.OperationFailedException {

        org.osid.course.registration.RegistrationList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.registration.registration.EffectiveRegistrationFilterList(ret);
        }

        return (ret);
    }


    public static class ActivityBundleFilter
        implements net.okapia.osid.jamocha.inline.filter.course.registration.registration.RegistrationFilter {
         
        private final org.osid.id.Id activityBundleId;
         
         
        /**
         *  Constructs a new <code>ActivityBundleFilter</code>.
         *
         *  @param activityBundleId the activityBundle to filter
         *  @throws org.osid.NullArgumentException
         *          <code>activityBundleId</code> is <code>null</code>
         */
        
        public ActivityBundleFilter(org.osid.id.Id activityBundleId) {
            nullarg(activityBundleId, "activityBundle Id");
            this.activityBundleId = activityBundleId;
            return;
        }

         
        /**
         *  Used by the RegistrationFilterList to filter the 
         *  registration list based on activityBundle.
         *
         *  @param registration the registration
         *  @return <code>true</code> to pass the registration,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.registration.Registration registration) {
            return (registration.getActivityBundleId().equals(this.activityBundleId));
        }
    }


    public static class StudentFilter
        implements net.okapia.osid.jamocha.inline.filter.course.registration.registration.RegistrationFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>StudentFilter</code>.
         *
         *  @param resourceId the student to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public StudentFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "student Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the RegistrationFilterList to filter the 
         *  registration list based on student.
         *
         *  @param registration the registration
         *  @return <code>true</code> to pass the registration,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.registration.Registration registration) {
            return (registration.getStudentId().equals(this.resourceId));
        }
    }
}
