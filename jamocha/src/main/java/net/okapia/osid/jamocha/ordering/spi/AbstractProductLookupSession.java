//
// AbstractProductLookupSession.java
//
//    A starter implementation framework for providing a Product
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Product
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getProducts(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractProductLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.ordering.ProductLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.ordering.Store store = new net.okapia.osid.jamocha.nil.ordering.store.UnknownStore();
    

    /**
     *  Gets the <code>Store/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Store Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.store.getId());
    }


    /**
     *  Gets the <code>Store</code> associated with this 
     *  session.
     *
     *  @return the <code>Store</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.store);
    }


    /**
     *  Sets the <code>Store</code>.
     *
     *  @param  store the store for this session
     *  @throws org.osid.NullArgumentException <code>store</code>
     *          is <code>null</code>
     */

    protected void setStore(org.osid.ordering.Store store) {
        nullarg(store, "store");
        this.store = store;
        return;
    }


    /**
     *  Tests if this user can perform <code>Product</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProducts() {
        return (true);
    }


    /**
     *  A complete view of the <code>Product</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProductView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Product</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProductView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include products in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this store only.
     */

    @OSID @Override
    public void useIsolatedStoreView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Product</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Product</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Product</code> and
     *  retained for compatibility.
     *
     *  @param  productId <code>Id</code> of the
     *          <code>Product</code>
     *  @return the product
     *  @throws org.osid.NotFoundException <code>productId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>productId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Product getProduct(org.osid.id.Id productId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.ordering.ProductList products = getProducts()) {
            while (products.hasNext()) {
                org.osid.ordering.Product product = products.getNextProduct();
                if (product.getId().equals(productId)) {
                    return (product);
                }
            }
        } 

        throw new org.osid.NotFoundException(productId + " not found");
    }


    /**
     *  Gets a <code>ProductList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  products specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Products</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getProducts()</code>.
     *
     *  @param  productIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Product</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>productIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByIds(org.osid.id.IdList productIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.ordering.Product> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = productIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getProduct(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("product " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.ordering.product.LinkedProductList(ret));
    }


    /**
     *  Gets a <code>ProductList</code> corresponding to the given
     *  product genus <code>Type</code> which does not include
     *  products of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getProducts()</code>.
     *
     *  @param  productGenusType a product genus type 
     *  @return the returned <code>Product</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>productGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByGenusType(org.osid.type.Type productGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.product.ProductGenusFilterList(getProducts(), productGenusType));
    }


    /**
     *  Gets a <code>ProductList</code> corresponding to the given
     *  product genus <code>Type</code> and include any additional
     *  products with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProducts()</code>.
     *
     *  @param  productGenusType a product genus type 
     *  @return the returned <code>Product</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>productGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByParentGenusType(org.osid.type.Type productGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getProductsByGenusType(productGenusType));
    }


    /**
     *  Gets a <code>ProductList</code> containing the given
     *  product record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getProducts()</code>.
     *
     *  @param  productRecordType a product record type 
     *  @return the returned <code>Product</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByRecordType(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.product.ProductRecordFilterList(getProducts(), productRecordType));
    }


    /**
     *  Gets the <code>Products</code> specified by its product code.
     *
     *  @param  code a product code 
     *  @return the product  list
     *  @throws org.osid.NotFoundException <code>code</code> not found 
     *  @throws org.osid.NullArgumentException <code>code</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByCode(String code)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.ordering.product.ProductFilterList(new CodeFilter(code), getProducts()));
    }


    /**
     *  Gets a <code>ProductList</code> containing the given price
     *  schedule.  In plenary mode, the returned list contains all
     *  known products or an error results. Otherwise, the returned
     *  list may contain only those products that are accessible
     *  through this session.
     *
     *  @param  priceScheduleId a price schedule <code>Id</code> 
     *  @return the product list 
     *  @throws org.osid.NullArgumentException
     *          <code>priceScheduleId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ordering.ProductList getProductsByPriceSchedule(org.osid.id.Id priceScheduleId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.ordering.Product> ret = new java.util.ArrayList<>();

        try (org.osid.ordering.ProductList products = getProducts()) {
            while (products.hasNext()) {
                org.osid.ordering.Product product = products.getNextProduct();
                try (org.osid.id.IdList ids = product.getPriceScheduleIds()) {
                    while (ids.hasNext()) {
                        if (ids.getNextId().equals(priceScheduleId)) {
                            ret.add(product);
                        }
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.ordering.product.LinkedProductList(ret));
    }


    /**
     *  Gets all <code>Products</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  products or an error results. Otherwise, the returned list
     *  may contain only those products that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Products</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.ordering.ProductList getProducts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the product list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of products
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.ordering.ProductList filterProductsOnViews(org.osid.ordering.ProductList list)
        throws org.osid.OperationFailedException {

        return (list);
    }


    public static class CodeFilter
        implements net.okapia.osid.jamocha.inline.filter.ordering.product.ProductFilter {

        private final String code;

        
        /**
         *  Constructs a new <code>CodeFilter</code>.
         *
         *  @param code the code to filter
         *  @throws org.osid.NullArgumentException <code>code</code>
         *          is <code>null</code>
         */

        public CodeFilter(String code) {
            nullarg(code, "code");
            this.code = code;
            return;
        }


        /**
         *  Used by the ProductFilterList to filter the product list
         *  based on code.
         *
         *  @param product the product
         *  @return <code>true</code> to pass the product,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.ordering.Product product) {
            return (product.getCode().equals(this.code));
        }
    }
}
