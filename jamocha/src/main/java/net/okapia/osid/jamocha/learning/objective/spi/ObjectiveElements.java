//
// ObjectiveElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.objective.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ObjectiveElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ObjectiveElement Id.
     *
     *  @return the objective element Id
     */

    public static org.osid.id.Id getObjectiveEntityId() {
        return (makeEntityId("osid.learning.Objective"));
    }


    /**
     *  Gets the AssessmentId element Id.
     *
     *  @return the AssessmentId element Id
     */

    public static org.osid.id.Id getAssessmentId() {
        return (makeElementId("osid.learning.objective.AssessmentId"));
    }


    /**
     *  Gets the Assessment element Id.
     *
     *  @return the Assessment element Id
     */

    public static org.osid.id.Id getAssessment() {
        return (makeElementId("osid.learning.objective.Assessment"));
    }


    /**
     *  Gets the KnowledgeCategoryId element Id.
     *
     *  @return the KnowledgeCategoryId element Id
     */

    public static org.osid.id.Id getKnowledgeCategoryId() {
        return (makeElementId("osid.learning.objective.KnowledgeCategoryId"));
    }


    /**
     *  Gets the KnowledgeCategory element Id.
     *
     *  @return the KnowledgeCategory element Id
     */

    public static org.osid.id.Id getKnowledgeCategory() {
        return (makeElementId("osid.learning.objective.KnowledgeCategory"));
    }


    /**
     *  Gets the CognitiveProcessId element Id.
     *
     *  @return the CognitiveProcessId element Id
     */

    public static org.osid.id.Id getCognitiveProcessId() {
        return (makeElementId("osid.learning.objective.CognitiveProcessId"));
    }


    /**
     *  Gets the CognitiveProcess element Id.
     *
     *  @return the CognitiveProcess element Id
     */

    public static org.osid.id.Id getCognitiveProcess() {
        return (makeElementId("osid.learning.objective.CognitiveProcess"));
    }


    /**
     *  Gets the ActivityId element Id.
     *
     *  @return the ActivityId element Id
     */

    public static org.osid.id.Id getActivityId() {
        return (makeQueryElementId("osid.learning.objective.ActivityId"));
    }


    /**
     *  Gets the Activity element Id.
     *
     *  @return the Activity element Id
     */

    public static org.osid.id.Id getActivity() {
        return (makeQueryElementId("osid.learning.objective.Activity"));
    }


    /**
     *  Gets the RequisiteObjectiveId element Id.
     *
     *  @return the RequisiteObjectiveId element Id
     */

    public static org.osid.id.Id getRequisiteObjectiveId() {
        return (makeQueryElementId("osid.learning.objective.RequisiteObjectiveId"));
    }


    /**
     *  Gets the RequisiteObjective element Id.
     *
     *  @return the RequisiteObjective element Id
     */

    public static org.osid.id.Id getRequisiteObjective() {
        return (makeQueryElementId("osid.learning.objective.RequisiteObjective"));
    }


    /**
     *  Gets the DependentObjectiveId element Id.
     *
     *  @return the DependentObjectiveId element Id
     */

    public static org.osid.id.Id getDependentObjectiveId() {
        return (makeQueryElementId("osid.learning.objective.DependentObjectiveId"));
    }


    /**
     *  Gets the DependentObjective element Id.
     *
     *  @return the DependentObjective element Id
     */

    public static org.osid.id.Id getDependentObjective() {
        return (makeQueryElementId("osid.learning.objective.DependentObjective"));
    }


    /**
     *  Gets the EquivalentObjectiveId element Id.
     *
     *  @return the EquivalentObjectiveId element Id
     */

    public static org.osid.id.Id getEquivalentObjectiveId() {
        return (makeQueryElementId("osid.learning.objective.EquivalentObjectiveId"));
    }


    /**
     *  Gets the EquivalentObjective element Id.
     *
     *  @return the EquivalentObjective element Id
     */

    public static org.osid.id.Id getEquivalentObjective() {
        return (makeQueryElementId("osid.learning.objective.EquivalentObjective"));
    }


    /**
     *  Gets the AncestorObjectiveId element Id.
     *
     *  @return the AncestorObjectiveId element Id
     */

    public static org.osid.id.Id getAncestorObjectiveId() {
        return (makeQueryElementId("osid.learning.objective.AncestorObjectiveId"));
    }


    /**
     *  Gets the AncestorObjective element Id.
     *
     *  @return the AncestorObjective element Id
     */

    public static org.osid.id.Id getAncestorObjective() {
        return (makeQueryElementId("osid.learning.objective.AncestorObjective"));
    }


    /**
     *  Gets the DescendantObjectiveId element Id.
     *
     *  @return the DescendantObjectiveId element Id
     */

    public static org.osid.id.Id getDescendantObjectiveId() {
        return (makeQueryElementId("osid.learning.objective.DescendantObjectiveId"));
    }


    /**
     *  Gets the DescendantObjective element Id.
     *
     *  @return the DescendantObjective element Id
     */

    public static org.osid.id.Id getDescendantObjective() {
        return (makeQueryElementId("osid.learning.objective.DescendantObjective"));
    }


    /**
     *  Gets the ObjectiveBankId element Id.
     *
     *  @return the ObjectiveBankId element Id
     */

    public static org.osid.id.Id getObjectiveBankId() {
        return (makeQueryElementId("osid.learning.objective.ObjectiveBankId"));
    }


    /**
     *  Gets the ObjectiveBank element Id.
     *
     *  @return the ObjectiveBank element Id
     */

    public static org.osid.id.Id getObjectiveBank() {
        return (makeQueryElementId("osid.learning.objective.ObjectiveBank"));
    }
}
