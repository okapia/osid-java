//
// AbstractOrganizationSearch.java
//
//     A template for making an Organization Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.organization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing organization searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOrganizationSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.personnel.OrganizationSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.personnel.records.OrganizationSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.personnel.OrganizationSearchOrder organizationSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of organizations. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  organizationIds list of organizations
     *  @throws org.osid.NullArgumentException
     *          <code>organizationIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOrganizations(org.osid.id.IdList organizationIds) {
        while (organizationIds.hasNext()) {
            try {
                this.ids.add(organizationIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOrganizations</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of organization Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOrganizationIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  organizationSearchOrder organization search order 
     *  @throws org.osid.NullArgumentException
     *          <code>organizationSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>organizationSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOrganizationResults(org.osid.personnel.OrganizationSearchOrder organizationSearchOrder) {
	this.organizationSearchOrder = organizationSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.personnel.OrganizationSearchOrder getOrganizationSearchOrder() {
	return (this.organizationSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given organization search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an organization implementing the requested record.
     *
     *  @param organizationSearchRecordType an organization search record
     *         type
     *  @return the organization search record
     *  @throws org.osid.NullArgumentException
     *          <code>organizationSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(organizationSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.OrganizationSearchRecord getOrganizationSearchRecord(org.osid.type.Type organizationSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.personnel.records.OrganizationSearchRecord record : this.records) {
            if (record.implementsRecordType(organizationSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(organizationSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this organization search. 
     *
     *  @param organizationSearchRecord organization search record
     *  @param organizationSearchRecordType organization search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOrganizationSearchRecord(org.osid.personnel.records.OrganizationSearchRecord organizationSearchRecord, 
                                           org.osid.type.Type organizationSearchRecordType) {

        addRecordType(organizationSearchRecordType);
        this.records.add(organizationSearchRecord);        
        return;
    }
}
