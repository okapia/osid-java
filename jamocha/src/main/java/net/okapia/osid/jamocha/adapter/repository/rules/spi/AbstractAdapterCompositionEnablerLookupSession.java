//
// AbstractAdapterCompositionEnablerLookupSession.java
//
//    A CompositionEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.repository.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CompositionEnabler lookup session adapter.
 */

public abstract class AbstractAdapterCompositionEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.repository.rules.CompositionEnablerLookupSession {

    private final org.osid.repository.rules.CompositionEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCompositionEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCompositionEnablerLookupSession(org.osid.repository.rules.CompositionEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Repository/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Repository Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getRepositoryId() {
        return (this.session.getRepositoryId());
    }


    /**
     *  Gets the {@code Repository} associated with this session.
     *
     *  @return the {@code Repository} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getRepository());
    }


    /**
     *  Tests if this user can perform {@code CompositionEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCompositionEnablers() {
        return (this.session.canLookupCompositionEnablers());
    }


    /**
     *  A complete view of the {@code CompositionEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCompositionEnablerView() {
        this.session.useComparativeCompositionEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code CompositionEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCompositionEnablerView() {
        this.session.usePlenaryCompositionEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include composition enablers in repositories which are children
     *  of this repository in the repository hierarchy.
     */

    @OSID @Override
    public void useFederatedRepositoryView() {
        this.session.useFederatedRepositoryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this repository only.
     */

    @OSID @Override
    public void useIsolatedRepositoryView() {
        this.session.useIsolatedRepositoryView();
        return;
    }
    

    /**
     *  Only active composition enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveCompositionEnablerView() {
        this.session.useActiveCompositionEnablerView();
        return;
    }


    /**
     *  Active and inactive composition enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusCompositionEnablerView() {
        this.session.useAnyStatusCompositionEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code CompositionEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CompositionEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CompositionEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, composition enablers are returned that are currently
     *  active. In any status mode, active and inactive composition enablers
     *  are returned.
     *
     *  @param compositionEnablerId {@code Id} of the {@code CompositionEnabler}
     *  @return the composition enabler
     *  @throws org.osid.NotFoundException {@code compositionEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code compositionEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnabler getCompositionEnabler(org.osid.id.Id compositionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionEnabler(compositionEnablerId));
    }


    /**
     *  Gets a {@code CompositionEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  compositionEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CompositionEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, composition enablers are returned that are currently
     *  active. In any status mode, active and inactive composition enablers
     *  are returned.
     *
     *  @param  compositionEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CompositionEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code compositionEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByIds(org.osid.id.IdList compositionEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionEnablersByIds(compositionEnablerIds));
    }


    /**
     *  Gets a {@code CompositionEnablerList} corresponding to the given
     *  composition enabler genus {@code Type} which does not include
     *  composition enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the returned list
     *  may contain only those composition enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, composition enablers are returned that are currently
     *  active. In any status mode, active and inactive composition enablers
     *  are returned.
     *
     *  @param  compositionEnablerGenusType a compositionEnabler genus type 
     *  @return the returned {@code CompositionEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code compositionEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByGenusType(org.osid.type.Type compositionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionEnablersByGenusType(compositionEnablerGenusType));
    }


    /**
     *  Gets a {@code CompositionEnablerList} corresponding to the given
     *  composition enabler genus {@code Type} and include any additional
     *  composition enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the returned list
     *  may contain only those composition enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, composition enablers are returned that are currently
     *  active. In any status mode, active and inactive composition enablers
     *  are returned.
     *
     *  @param  compositionEnablerGenusType a compositionEnabler genus type 
     *  @return the returned {@code CompositionEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code compositionEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByParentGenusType(org.osid.type.Type compositionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionEnablersByParentGenusType(compositionEnablerGenusType));
    }


    /**
     *  Gets a {@code CompositionEnablerList} containing the given
     *  composition enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the returned list
     *  may contain only those composition enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, composition enablers are returned that are currently
     *  active. In any status mode, active and inactive composition enablers
     *  are returned.
     *
     *  @param  compositionEnablerRecordType a compositionEnabler record type 
     *  @return the returned {@code CompositionEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code compositionEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersByRecordType(org.osid.type.Type compositionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionEnablersByRecordType(compositionEnablerRecordType));
    }


    /**
     *  Gets a {@code CompositionEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the returned list
     *  may contain only those composition enablers that are accessible
     *  through this session.
     *  
     *  In active mode, composition enablers are returned that are currently
     *  active. In any status mode, active and inactive composition enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code CompositionEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code CompositionEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the returned list
     *  may contain only those composition enablers that are accessible
     *  through this session.
     *
     *  In active mode, composition enablers are returned that are currently
     *  active. In any status mode, active and inactive composition enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code CompositionEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getCompositionEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code CompositionEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  composition enablers or an error results. Otherwise, the returned list
     *  may contain only those composition enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, composition enablers are returned that are currently
     *  active. In any status mode, active and inactive composition enablers
     *  are returned.
     *
     *  @return a list of {@code CompositionEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.rules.CompositionEnablerList getCompositionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCompositionEnablers());
    }
}
