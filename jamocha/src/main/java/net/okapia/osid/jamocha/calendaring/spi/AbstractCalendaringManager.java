//
// AbstractCalendaringManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCalendaringManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.calendaring.CalendaringManager,
               org.osid.calendaring.CalendaringProxyManager {

    private final Types eventRecordTypes                   = new TypeRefSet();
    private final Types eventSearchRecordTypes             = new TypeRefSet();

    private final Types recurringEventRecordTypes          = new TypeRefSet();
    private final Types recurringEventSearchRecordTypes    = new TypeRefSet();

    private final Types supersedingEventRecordTypes        = new TypeRefSet();
    private final Types supersedingEventSearchRecordTypes  = new TypeRefSet();

    private final Types offsetEventRecordTypes             = new TypeRefSet();
    private final Types offsetEventSearchRecordTypes       = new TypeRefSet();

    private final Types scheduleRecordTypes                = new TypeRefSet();
    private final Types scheduleSearchRecordTypes          = new TypeRefSet();

    private final Types scheduleSlotRecordTypes            = new TypeRefSet();
    private final Types scheduleSlotSearchRecordTypes      = new TypeRefSet();

    private final Types timePeriodRecordTypes              = new TypeRefSet();
    private final Types timePeriodSearchRecordTypes        = new TypeRefSet();

    private final Types commitmentRecordTypes              = new TypeRefSet();
    private final Types commitmentSearchRecordTypes        = new TypeRefSet();

    private final Types calendarRecordTypes                = new TypeRefSet();
    private final Types calendarSearchRecordTypes          = new TypeRefSet();

    private final Types spatialUnitRecordTypes             = new TypeRefSet();
    private final Types coordinateRecordTypes              = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCalendaringManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCalendaringManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if an event lookup service is supported. An event lookup service 
     *  defines methods to access events. 
     *
     *  @return true if event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsEventLookup() {
        return (false);
    }


    /**
     *  Tests if an event query service is supported. 
     *
     *  @return <code> true </code> if event query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Tests if an event search service is supported. 
     *
     *  @return <code> true </code> if event search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventSearch() {
        return (false);
    }


    /**
     *  Tests if an event administrative service is supported. 
     *
     *  @return <code> true </code> if event admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventAdmin() {
        return (false);
    }


    /**
     *  Tests if event notification is supported. Messages may be sent when 
     *  events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if event notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventNotification() {
        return (false);
    }


    /**
     *  Tests if an event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if event calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventCalendar() {
        return (false);
    }


    /**
     *  Tests if an event to calendar assignment session is available. 
     *
     *  @return <code> true </code> if event calendar assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if event smart calendaring is available. 
     *
     *  @return <code> true </code> if event smart calendaring is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if a recurring event lookup service is supported. 
     *
     *  @return true if recurring event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventLookup() {
        return (false);
    }


    /**
     *  Tests if a recurring event unravelling service is supported. 
     *
     *  @return true if recurring event unravelling is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventUnravelling() {
        return (false);
    }


    /**
     *  Tests if a recurring event query service is supported. 
     *
     *  @return <code> true </code> if recurring event query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventQuery() {
        return (false);
    }


    /**
     *  Tests if a recurring event search service is supported. 
     *
     *  @return <code> true </code> if recurring event search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventSearch() {
        return (false);
    }


    /**
     *  Tests if a recurring event administrative service is supported. 
     *
     *  @return <code> true </code> if recurring event admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventAdmin() {
        return (false);
    }


    /**
     *  Tests if recurring event notification is supported. Messages may be 
     *  sent when recurring events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if recurring event notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventNotification() {
        return (false);
    }


    /**
     *  Tests if a recurring event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if recurring event calendar lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventCalendar() {
        return (false);
    }


    /**
     *  Tests if a recurring event to calendar assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if recurring event calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if recurring event smart calendaring is available. 
     *
     *  @return <code> true </code> if recurring event smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecurringEventSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if a superseding event lookup service is supported. A 
     *  superseding event lookup service defines methods to access superseding 
     *  events. 
     *
     *  @return true if superseding event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventLookup() {
        return (false);
    }


    /**
     *  Tests if a superseding event query service is supported. 
     *
     *  @return <code> true </code> if superseding event query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventQuery() {
        return (false);
    }


    /**
     *  Tests if a superseding event search service is supported. 
     *
     *  @return <code> true </code> if superseding event search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventSearch() {
        return (false);
    }


    /**
     *  Tests if a superseding event administrative service is supported. 
     *
     *  @return <code> true </code> if superseding event admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventAdmin() {
        return (false);
    }


    /**
     *  Tests if superseding event notification is supported. Messages may be 
     *  sent when supsreding events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if superseding event notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventNotification() {
        return (false);
    }


    /**
     *  Tests if superseding event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if superseding event calendar lookup 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventCalendar() {
        return (false);
    }


    /**
     *  Tests if a superseding event to calendar assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if superseding event calendar assignment 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if supsreding event smart calendaring is available. 
     *
     *  @return <code> true </code> if superseding smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingEventSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if an offset event lookup service is supported. An offset event 
     *  lookup service defines methods to access events. 
     *
     *  @return true if offset event lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventLookup() {
        return (false);
    }


    /**
     *  Tests if an offset event query service is supported. 
     *
     *  @return <code> true </code> if offset event query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventQuery() {
        return (false);
    }


    /**
     *  Tests if an offset event search service is supported. 
     *
     *  @return <code> true </code> if offset event search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventSearch() {
        return (false);
    }


    /**
     *  Tests if an offset event administrative service is supported. 
     *
     *  @return <code> true </code> if offset event admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventAdmin() {
        return (false);
    }


    /**
     *  Tests if offset event notification is supported. Messages may be sent 
     *  when events are created, modified, or deleted. 
     *
     *  @return <code> true </code> if offset event notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventNotification() {
        return (false);
    }


    /**
     *  Tests if an offset event to calendar lookup session is available. 
     *
     *  @return <code> true </code> if event calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventCalendar() {
        return (false);
    }


    /**
     *  Tests if an offset event to calendar assignment session is available. 
     *
     *  @return <code> true </code> if offset event calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if offset event smart calendaring is available. 
     *
     *  @return <code> true </code> if offset event smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOffsetEventSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if a schedule lookup service is supported. A schedule lookup 
     *  service defines methods to access schedules. 
     *
     *  @return true if schedule lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleLookup() {
        return (false);
    }


    /**
     *  Tests if a schedule query service is supported. 
     *
     *  @return <code> true </code> if schedule query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleQuery() {
        return (false);
    }


    /**
     *  Tests if a schedule search service is supported. 
     *
     *  @return <code> true </code> if schedule search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSearch() {
        return (false);
    }


    /**
     *  Tests if a schedule administrative service is supported. 
     *
     *  @return <code> true </code> if schedule admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleAdmin() {
        return (false);
    }


    /**
     *  Tests if schedule notification is supported. Messages may be sent when 
     *  schedules are created, modified, or deleted. 
     *
     *  @return <code> true </code> if schedule notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleNotification() {
        return (false);
    }


    /**
     *  Tests if a schedule to calendar lookup session is available. 
     *
     *  @return <code> true </code> if schedule calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleCalendar() {
        return (false);
    }


    /**
     *  Tests if a schedule to calendar assignment session is available. 
     *
     *  @return <code> true </code> if schedule calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if schedule smart calendaring is available. 
     *
     *  @return <code> true </code> if schedule smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if a schedule slot lookup service is supported. A schedule sot 
     *  lookup service defines methods to access schedule slots. 
     *
     *  @return true if schedule slot lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotLookup() {
        return (false);
    }


    /**
     *  Tests if a schedule slot query service is supported. 
     *
     *  @return <code> true </code> if schedule slot query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotQuery() {
        return (false);
    }


    /**
     *  Tests if a schedule slot search service is supported. 
     *
     *  @return <code> true </code> if schedule slots search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotSearch() {
        return (false);
    }


    /**
     *  Tests if a schedule slot administrative service is supported. 
     *
     *  @return <code> true </code> if schedule slot admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotAdmin() {
        return (false);
    }


    /**
     *  Tests if schedule slot notification is supported. Messages may be sent 
     *  when schedule slots are created, modified, or deleted. 
     *
     *  @return <code> true </code> if schedule slot notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotNotification() {
        return (false);
    }


    /**
     *  Tests if a schedule slot to calendar lookup session is available. 
     *
     *  @return <code> true </code> if schedule slot calendar lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotCalendar() {
        return (false);
    }


    /**
     *  Tests if a schedule slot to calendar assignment session is available. 
     *
     *  @return <code> true </code> if schedule slot calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if schedule slot smart calendaring is available. 
     *
     *  @return <code> true </code> if schedule slot smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleSlotSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if an event commitment lookup service is supported. 
     *
     *  @return <code> true </code> if commitment lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentLookup() {
        return (false);
    }


    /**
     *  Tests if a commitment query service is supported. 
     *
     *  @return <code> true </code> if commitment query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentQuery() {
        return (false);
    }


    /**
     *  Tests if a commitment search service is supported. 
     *
     *  @return <code> true </code> if commitment search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentSearch() {
        return (false);
    }


    /**
     *  Tests if a commitment administrative service is supported. 
     *
     *  @return <code> true </code> if commitment admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentAdmin() {
        return (false);
    }


    /**
     *  Tests if commitment notification is supported. Messages may be sent 
     *  when commitments are created, modified, or deleted. 
     *
     *  @return <code> true </code> if commitment notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentNotification() {
        return (false);
    }


    /**
     *  Tests if a commitment to calendar lookup session is available. 
     *
     *  @return <code> true </code> if commitment calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentCalendar() {
        return (false);
    }


    /**
     *  Tests if a commitment to calendar assignment session is available. 
     *
     *  @return <code> true </code> if commitment calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if commitment smart calendaring is available. 
     *
     *  @return <code> true </code> if commitment smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if a time period lookup service is supported. 
     *
     *  @return <code> true </code> if time period lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodLookup() {
        return (false);
    }


    /**
     *  Tests if a time period search service is supported. 
     *
     *  @return <code> true </code> if time period search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearch() {
        return (false);
    }


    /**
     *  Tests if a time period administrative service is supported. 
     *
     *  @return <code> true </code> if time period admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodAdmin() {
        return (false);
    }


    /**
     *  Tests if time period notification is supported. Messages may be sent 
     *  when time periods are created, modified, or deleted. 
     *
     *  @return <code> true </code> if time period notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodNotification() {
        return (false);
    }


    /**
     *  Tests if a time period to calendar lookup session is available. 
     *
     *  @return <code> true </code> if time period calendar lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodCalendar() {
        return (false);
    }


    /**
     *  Tests if a time period to calendar assignment session is available. 
     *
     *  @return <code> true </code> if time period calendar assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodCalendarAssignment() {
        return (false);
    }


    /**
     *  Tests if time period smart calendaring is available. 
     *
     *  @return <code> true </code> if time period smart calendaring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodSmartCalendar() {
        return (false);
    }


    /**
     *  Tests if a calendar lookup service is supported. 
     *
     *  @return <code> true </code> if calendar lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarLookup() {
        return (false);
    }


    /**
     *  Tests if a calendar search service is supported. 
     *
     *  @return <code> true </code> if calendar search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarSearch() {
        return (false);
    }


    /**
     *  Tests if a calendar administrative service is supported. 
     *
     *  @return <code> true </code> if calendar admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarAdmin() {
        return (false);
    }


    /**
     *  Tests if calendar notification is supported. Messages may be sent when 
     *  calendars are created, modified, or deleted. 
     *
     *  @return <code> true </code> if calendar notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarNotification() {
        return (false);
    }


    /**
     *  Tests if a calendar hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a calendar hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarHierarchy() {
        return (false);
    }


    /**
     *  Tests if calendar hierarchy design is supported. 
     *
     *  @return <code> true </code> if a calendar hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a calendaring batch subpackage is supported. 
     *
     *  @return <code> true </code> if a calendar batch package is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringBatch() {
        return (false);
    }


    /**
     *  Tests if a calendaring cycle subpackage is supported. 
     *
     *  @return <code> true </code> if a calendar cycle package is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringCycle() {
        return (false);
    }


    /**
     *  Tests if a calendaring rules subpackage is supported. 
     *
     *  @return <code> true </code> if a calendar rules package is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Event </code> record types. 
     *
     *  @return a list containing the supported <code> Event </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEventRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.eventRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Event </code> record type is supported. 
     *
     *  @param  eventRecordType a <code> Type </code> indicating an <code> 
     *          Event </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> eventRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEventRecordType(org.osid.type.Type eventRecordType) {
        return (this.eventRecordTypes.contains(eventRecordType));
    }


    /**
     *  Adds support for an event record type.
     *
     *  @param eventRecordType an event record type
     *  @throws org.osid.NullArgumentException
     *  <code>eventRecordType</code> is <code>null</code>
     */

    protected void addEventRecordType(org.osid.type.Type eventRecordType) {
        this.eventRecordTypes.add(eventRecordType);
        return;
    }


    /**
     *  Removes support for an event record type.
     *
     *  @param eventRecordType an event record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>eventRecordType</code> is <code>null</code>
     */

    protected void removeEventRecordType(org.osid.type.Type eventRecordType) {
        this.eventRecordTypes.remove(eventRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Event </code> search record types. 
     *
     *  @return a list containing the supported <code> Event </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEventSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.eventSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Event </code> search record type is 
     *  supported. 
     *
     *  @param  eventSearchRecordType a <code> Type </code> indicating an 
     *          <code> Event </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> eventSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEventSearchRecordType(org.osid.type.Type eventSearchRecordType) {
        return (this.eventSearchRecordTypes.contains(eventSearchRecordType));
    }


    /**
     *  Adds support for an event search record type.
     *
     *  @param eventSearchRecordType an event search record type
     *  @throws org.osid.NullArgumentException
     *  <code>eventSearchRecordType</code> is <code>null</code>
     */

    protected void addEventSearchRecordType(org.osid.type.Type eventSearchRecordType) {
        this.eventSearchRecordTypes.add(eventSearchRecordType);
        return;
    }


    /**
     *  Removes support for an event search record type.
     *
     *  @param eventSearchRecordType an event search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>eventSearchRecordType</code> is <code>null</code>
     */

    protected void removeEventSearchRecordType(org.osid.type.Type eventSearchRecordType) {
        this.eventSearchRecordTypes.remove(eventSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RecurringEvent </code> record types. 
     *
     *  @return a list containing the supported <code> RecurringEvent </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecurringEventRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.recurringEventRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RecurringEvent </code> record type is 
     *  supported. 
     *
     *  @param  recurringEventRecordType a <code> Type </code> indicating a 
     *          <code> RecurringEvent </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> recurringEventRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecurringEventRecordType(org.osid.type.Type recurringEventRecordType) {
        return (this.recurringEventRecordTypes.contains(recurringEventRecordType));
    }


    /**
     *  Adds support for a recurring event record type.
     *
     *  @param recurringEventRecordType a recurring event record type
     *  @throws org.osid.NullArgumentException
     *  <code>recurringEventRecordType</code> is <code>null</code>
     */

    protected void addRecurringEventRecordType(org.osid.type.Type recurringEventRecordType) {
        this.recurringEventRecordTypes.add(recurringEventRecordType);
        return;
    }


    /**
     *  Removes support for a recurring event record type.
     *
     *  @param recurringEventRecordType a recurring event record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>recurringEventRecordType</code> is <code>null</code>
     */

    protected void removeRecurringEventRecordType(org.osid.type.Type recurringEventRecordType) {
        this.recurringEventRecordTypes.remove(recurringEventRecordType);
        return;
    }


    /**
     *  Gets the supported <code> RecurringEvent </code> search record types. 
     *
     *  @return a list containing the supported <code> RecurringEvent </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecurringEventSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.recurringEventSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> RecurringEvent </code> search record type is 
     *  supported. 
     *
     *  @param  recurringEventSearchRecordType a <code> Type </code> 
     *          indicating a <code> RecurringEvent </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          recurringEventSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecurringEventSearchRecordType(org.osid.type.Type recurringEventSearchRecordType) {
        return (this.recurringEventSearchRecordTypes.contains(recurringEventSearchRecordType));
    }


    /**
     *  Adds support for a recurring event search record type.
     *
     *  @param recurringEventSearchRecordType a recurring event search record type
     *  @throws org.osid.NullArgumentException
     *  <code>recurringEventSearchRecordType</code> is <code>null</code>
     */

    protected void addRecurringEventSearchRecordType(org.osid.type.Type recurringEventSearchRecordType) {
        this.recurringEventSearchRecordTypes.add(recurringEventSearchRecordType);
        return;
    }


    /**
     *  Removes support for a recurring event search record type.
     *
     *  @param recurringEventSearchRecordType a recurring event search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>recurringEventSearchRecordType</code> is <code>null</code>
     */

    protected void removeRecurringEventSearchRecordType(org.osid.type.Type recurringEventSearchRecordType) {
        this.recurringEventSearchRecordTypes.remove(recurringEventSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Superseding </code> record types. 
     *
     *  @return a list containing the supported <code> SupersedingEvent 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSupersedingEventRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.supersedingEventRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SupersedingEvent </code> record type is 
     *  supported. 
     *
     *  @param  supersedingEventRecordType a <code> Type </code> indicating a 
     *          <code> SupersedingEvent </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSupersedingEventRecordType(org.osid.type.Type supersedingEventRecordType) {
        return (this.supersedingEventRecordTypes.contains(supersedingEventRecordType));
    }


    /**
     *  Adds support for a superseding event record type.
     *
     *  @param supersedingEventRecordType a superseding event record type
     *  @throws org.osid.NullArgumentException
     *  <code>supersedingEventRecordType</code> is <code>null</code>
     */

    protected void addSupersedingEventRecordType(org.osid.type.Type supersedingEventRecordType) {
        this.supersedingEventRecordTypes.add(supersedingEventRecordType);
        return;
    }


    /**
     *  Removes support for a superseding event record type.
     *
     *  @param supersedingEventRecordType a superseding event record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>supersedingEventRecordType</code> is <code>null</code>
     */

    protected void removeSupersedingEventRecordType(org.osid.type.Type supersedingEventRecordType) {
        this.supersedingEventRecordTypes.remove(supersedingEventRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SupersedingEvent </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> SupersedingEvent 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSupersedingEventSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.supersedingEventSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SupersedingEvent </code> search record type 
     *  is supported. 
     *
     *  @param  supersedingEventSearchRecordType a <code> Type </code> 
     *          indicating a <code> SupersedingEvent </code> search record 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSupersedingEventSearchRecordType(org.osid.type.Type supersedingEventSearchRecordType) {
        return (this.supersedingEventSearchRecordTypes.contains(supersedingEventSearchRecordType));
    }


    /**
     *  Adds support for a superseding event search record type.
     *
     *  @param supersedingEventSearchRecordType a superseding event search record type
     *  @throws org.osid.NullArgumentException
     *  <code>supersedingEventSearchRecordType</code> is <code>null</code>
     */

    protected void addSupersedingEventSearchRecordType(org.osid.type.Type supersedingEventSearchRecordType) {
        this.supersedingEventSearchRecordTypes.add(supersedingEventSearchRecordType);
        return;
    }


    /**
     *  Removes support for a superseding event search record type.
     *
     *  @param supersedingEventSearchRecordType a superseding event search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>supersedingEventSearchRecordType</code> is <code>null</code>
     */

    protected void removeSupersedingEventSearchRecordType(org.osid.type.Type supersedingEventSearchRecordType) {
        this.supersedingEventSearchRecordTypes.remove(supersedingEventSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> OffsetEvent </code> record types. 
     *
     *  @return a list containing the supported <code> OffsetEvent </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOffsetEventRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offsetEventRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> OffsetEvent </code> record type is 
     *  supported. 
     *
     *  @param  offsetEventRecordType a <code> Type </code> indicating a 
     *          <code> OffsetEvent </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> offsetEventRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOffsetEventRecordType(org.osid.type.Type offsetEventRecordType) {
        return (this.offsetEventRecordTypes.contains(offsetEventRecordType));
    }


    /**
     *  Adds support for an offset event record type.
     *
     *  @param offsetEventRecordType an offset event record type
     *  @throws org.osid.NullArgumentException
     *  <code>offsetEventRecordType</code> is <code>null</code>
     */

    protected void addOffsetEventRecordType(org.osid.type.Type offsetEventRecordType) {
        this.offsetEventRecordTypes.add(offsetEventRecordType);
        return;
    }


    /**
     *  Removes support for an offset event record type.
     *
     *  @param offsetEventRecordType an offset event record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offsetEventRecordType</code> is <code>null</code>
     */

    protected void removeOffsetEventRecordType(org.osid.type.Type offsetEventRecordType) {
        this.offsetEventRecordTypes.remove(offsetEventRecordType);
        return;
    }


    /**
     *  Gets the supported <code> OffsetEvent </code> search record types. 
     *
     *  @return a list containing the supported <code> OffsetEvent </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOffsetEventSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offsetEventSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> OffsetEvent </code> search record type is 
     *  supported. 
     *
     *  @param  offsetEventSearchRecordType a <code> Type </code> indicating a 
     *          <code> OffsetEvent </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offsetEventSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOffsetEventSearchRecordType(org.osid.type.Type offsetEventSearchRecordType) {
        return (this.offsetEventSearchRecordTypes.contains(offsetEventSearchRecordType));
    }


    /**
     *  Adds support for an offset event search record type.
     *
     *  @param offsetEventSearchRecordType an offset event search record type
     *  @throws org.osid.NullArgumentException
     *  <code>offsetEventSearchRecordType</code> is <code>null</code>
     */

    protected void addOffsetEventSearchRecordType(org.osid.type.Type offsetEventSearchRecordType) {
        this.offsetEventSearchRecordTypes.add(offsetEventSearchRecordType);
        return;
    }


    /**
     *  Removes support for an offset event search record type.
     *
     *  @param offsetEventSearchRecordType an offset event search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offsetEventSearchRecordType</code> is <code>null</code>
     */

    protected void removeOffsetEventSearchRecordType(org.osid.type.Type offsetEventSearchRecordType) {
        this.offsetEventSearchRecordTypes.remove(offsetEventSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Schedule </code> record types. 
     *
     *  @return a list containing the supported <code> Schedule </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getScheduleRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.scheduleRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Schedule </code> record type is supported. 
     *
     *  @param  scheduleRecordType a <code> Type </code> indicating a <code> 
     *          Schedule </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> scheduleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsScheduleRecordType(org.osid.type.Type scheduleRecordType) {
        return (this.scheduleRecordTypes.contains(scheduleRecordType));
    }


    /**
     *  Adds support for a schedule record type.
     *
     *  @param scheduleRecordType a schedule record type
     *  @throws org.osid.NullArgumentException
     *  <code>scheduleRecordType</code> is <code>null</code>
     */

    protected void addScheduleRecordType(org.osid.type.Type scheduleRecordType) {
        this.scheduleRecordTypes.add(scheduleRecordType);
        return;
    }


    /**
     *  Removes support for a schedule record type.
     *
     *  @param scheduleRecordType a schedule record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>scheduleRecordType</code> is <code>null</code>
     */

    protected void removeScheduleRecordType(org.osid.type.Type scheduleRecordType) {
        this.scheduleRecordTypes.remove(scheduleRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Schedule </code> search record types. 
     *
     *  @return a list containing the supported <code> Schedule </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getScheduleSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.scheduleSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Schedule </code> search record type is 
     *  supported. 
     *
     *  @param  scheduleSearchRecordType a <code> Type </code> indicating a 
     *          <code> Schedule </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> scheduleSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsScheduleSearchRecordType(org.osid.type.Type scheduleSearchRecordType) {
        return (this.scheduleSearchRecordTypes.contains(scheduleSearchRecordType));
    }


    /**
     *  Adds support for a schedule search record type.
     *
     *  @param scheduleSearchRecordType a schedule search record type
     *  @throws org.osid.NullArgumentException
     *  <code>scheduleSearchRecordType</code> is <code>null</code>
     */

    protected void addScheduleSearchRecordType(org.osid.type.Type scheduleSearchRecordType) {
        this.scheduleSearchRecordTypes.add(scheduleSearchRecordType);
        return;
    }


    /**
     *  Removes support for a schedule search record type.
     *
     *  @param scheduleSearchRecordType a schedule search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>scheduleSearchRecordType</code> is <code>null</code>
     */

    protected void removeScheduleSearchRecordType(org.osid.type.Type scheduleSearchRecordType) {
        this.scheduleSearchRecordTypes.remove(scheduleSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ScheduleSlot </code> record types. 
     *
     *  @return a list containing the supported <code> ScheduleSlot </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getScheduleSlotRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.scheduleSlotRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ScheduleSlot </code> record type is 
     *  supported. 
     *
     *  @param  scheduleSlotRecordType a <code> Type </code> indicating a 
     *          <code> ScheduleSlot </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsScheduleSlotRecordType(org.osid.type.Type scheduleSlotRecordType) {
        return (this.scheduleSlotRecordTypes.contains(scheduleSlotRecordType));
    }


    /**
     *  Adds support for a schedule slot record type.
     *
     *  @param scheduleSlotRecordType a schedule slot record type
     *  @throws org.osid.NullArgumentException
     *  <code>scheduleSlotRecordType</code> is <code>null</code>
     */

    protected void addScheduleSlotRecordType(org.osid.type.Type scheduleSlotRecordType) {
        this.scheduleSlotRecordTypes.add(scheduleSlotRecordType);
        return;
    }


    /**
     *  Removes support for a schedule slot record type.
     *
     *  @param scheduleSlotRecordType a schedule slot record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>scheduleSlotRecordType</code> is <code>null</code>
     */

    protected void removeScheduleSlotRecordType(org.osid.type.Type scheduleSlotRecordType) {
        this.scheduleSlotRecordTypes.remove(scheduleSlotRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ScheduleSlot </code> search record types. 
     *
     *  @return a list containing the supported <code> ScheduleSlot </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getScheduleSlotSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.scheduleSlotSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ScheduleSlot </code> search record type is 
     *  supported. 
     *
     *  @param  scheduleSlotSearchRecordType a <code> Type </code> indicating 
     *          a <code> ScheduleSlot </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          scheduleSlotSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsScheduleSlotSearchRecordType(org.osid.type.Type scheduleSlotSearchRecordType) {
        return (this.scheduleSlotSearchRecordTypes.contains(scheduleSlotSearchRecordType));
    }


    /**
     *  Adds support for a schedule slot search record type.
     *
     *  @param scheduleSlotSearchRecordType a schedule slot search record type
     *  @throws org.osid.NullArgumentException
     *  <code>scheduleSlotSearchRecordType</code> is <code>null</code>
     */

    protected void addScheduleSlotSearchRecordType(org.osid.type.Type scheduleSlotSearchRecordType) {
        this.scheduleSlotSearchRecordTypes.add(scheduleSlotSearchRecordType);
        return;
    }


    /**
     *  Removes support for a schedule slot search record type.
     *
     *  @param scheduleSlotSearchRecordType a schedule slot search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>scheduleSlotSearchRecordType</code> is <code>null</code>
     */

    protected void removeScheduleSlotSearchRecordType(org.osid.type.Type scheduleSlotSearchRecordType) {
        this.scheduleSlotSearchRecordTypes.remove(scheduleSlotSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> TimePeriod </code> record types. 
     *
     *  @return a list containing the supported <code> TimePeriod </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimePeriodRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.timePeriodRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> TimePeriod </code> record type is supported. 
     *
     *  @param  timePeriodRecordType a <code> Type </code> indicating a <code> 
     *          TimePeriod </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> timePeriodRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimePeriodRecordType(org.osid.type.Type timePeriodRecordType) {
        return (this.timePeriodRecordTypes.contains(timePeriodRecordType));
    }


    /**
     *  Adds support for a time period record type.
     *
     *  @param timePeriodRecordType a time period record type
     *  @throws org.osid.NullArgumentException
     *  <code>timePeriodRecordType</code> is <code>null</code>
     */

    protected void addTimePeriodRecordType(org.osid.type.Type timePeriodRecordType) {
        this.timePeriodRecordTypes.add(timePeriodRecordType);
        return;
    }


    /**
     *  Removes support for a time period record type.
     *
     *  @param timePeriodRecordType a time period record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>timePeriodRecordType</code> is <code>null</code>
     */

    protected void removeTimePeriodRecordType(org.osid.type.Type timePeriodRecordType) {
        this.timePeriodRecordTypes.remove(timePeriodRecordType);
        return;
    }


    /**
     *  Gets the supported <code> TimePeriod </code> search record types. 
     *
     *  @return a list containing the supported <code> TimePeriod </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTimePeriodSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.timePeriodSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> TimePeriod </code> search record type is 
     *  supported. 
     *
     *  @param  timePeriodSearchRecordType a <code> Type </code> indicating a 
     *          <code> TimePeriod </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          timePeriodSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimePeriodSearchRecordType(org.osid.type.Type timePeriodSearchRecordType) {
        return (this.timePeriodSearchRecordTypes.contains(timePeriodSearchRecordType));
    }


    /**
     *  Adds support for a time period search record type.
     *
     *  @param timePeriodSearchRecordType a time period search record type
     *  @throws org.osid.NullArgumentException
     *  <code>timePeriodSearchRecordType</code> is <code>null</code>
     */

    protected void addTimePeriodSearchRecordType(org.osid.type.Type timePeriodSearchRecordType) {
        this.timePeriodSearchRecordTypes.add(timePeriodSearchRecordType);
        return;
    }


    /**
     *  Removes support for a time period search record type.
     *
     *  @param timePeriodSearchRecordType a time period search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>timePeriodSearchRecordType</code> is <code>null</code>
     */

    protected void removeTimePeriodSearchRecordType(org.osid.type.Type timePeriodSearchRecordType) {
        this.timePeriodSearchRecordTypes.remove(timePeriodSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Commitment </code> record types. 
     *
     *  @return a list containing the supported <code> Commitment </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommitmentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commitmentRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Commitment </code> record type is supported. 
     *
     *  @param  commitmentRecordType a <code> Type </code> indicating a <code> 
     *          Commitment </code> type 
     *  @return <code> true </code> if the given commitment record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> commitmentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommitmentRecordType(org.osid.type.Type commitmentRecordType) {
        return (this.commitmentRecordTypes.contains(commitmentRecordType));
    }


    /**
     *  Adds support for a commitment record type.
     *
     *  @param commitmentRecordType a commitment record type
     *  @throws org.osid.NullArgumentException
     *  <code>commitmentRecordType</code> is <code>null</code>
     */

    protected void addCommitmentRecordType(org.osid.type.Type commitmentRecordType) {
        this.commitmentRecordTypes.add(commitmentRecordType);
        return;
    }


    /**
     *  Removes support for a commitment record type.
     *
     *  @param commitmentRecordType a commitment record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commitmentRecordType</code> is <code>null</code>
     */

    protected void removeCommitmentRecordType(org.osid.type.Type commitmentRecordType) {
        this.commitmentRecordTypes.remove(commitmentRecordType);
        return;
    }


    /**
     *  Gets the supported commitment search record types. 
     *
     *  @return a list containing the supported <code> Commitment </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommitmentSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commitmentSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given commitment search record type is supported. 
     *
     *  @param  commitmentSearchRecordType a <code> Type </code> indicating a 
     *          <code> Commitment </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          commitmentSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommitmentSearchRecordType(org.osid.type.Type commitmentSearchRecordType) {
        return (this.commitmentSearchRecordTypes.contains(commitmentSearchRecordType));
    }


    /**
     *  Adds support for a commitment search record type.
     *
     *  @param commitmentSearchRecordType a commitment search record type
     *  @throws org.osid.NullArgumentException
     *  <code>commitmentSearchRecordType</code> is <code>null</code>
     */

    protected void addCommitmentSearchRecordType(org.osid.type.Type commitmentSearchRecordType) {
        this.commitmentSearchRecordTypes.add(commitmentSearchRecordType);
        return;
    }


    /**
     *  Removes support for a commitment search record type.
     *
     *  @param commitmentSearchRecordType a commitment search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commitmentSearchRecordType</code> is <code>null</code>
     */

    protected void removeCommitmentSearchRecordType(org.osid.type.Type commitmentSearchRecordType) {
        this.commitmentSearchRecordTypes.remove(commitmentSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Calendar </code> record types. 
     *
     *  @return a list containing the supported <code> Calendar </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.calendarRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Calendar </code> record type is supported. 
     *
     *  @param  calendarRecordType a <code> Type </code> indicating a <code> 
     *          Calendar </code> type 
     *  @return <code> true </code> if the given calendar record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> calendarRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarRecordType(org.osid.type.Type calendarRecordType) {
        return (this.calendarRecordTypes.contains(calendarRecordType));
    }


    /**
     *  Adds support for a calendar record type.
     *
     *  @param calendarRecordType a calendar record type
     *  @throws org.osid.NullArgumentException
     *  <code>calendarRecordType</code> is <code>null</code>
     */

    protected void addCalendarRecordType(org.osid.type.Type calendarRecordType) {
        this.calendarRecordTypes.add(calendarRecordType);
        return;
    }


    /**
     *  Removes support for a calendar record type.
     *
     *  @param calendarRecordType a calendar record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>calendarRecordType</code> is <code>null</code>
     */

    protected void removeCalendarRecordType(org.osid.type.Type calendarRecordType) {
        this.calendarRecordTypes.remove(calendarRecordType);
        return;
    }


    /**
     *  Gets the supported calendar search record types. 
     *
     *  @return a list containing the supported <code> Calendar </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCalendarSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.calendarSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given calendar search record type is supported. 
     *
     *  @param  calendarSearchRecordType a <code> Type </code> indicating a 
     *          <code> Calendar </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> calendarSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCalendarSearchRecordType(org.osid.type.Type calendarSearchRecordType) {
        return (this.calendarSearchRecordTypes.contains(calendarSearchRecordType));
    }


    /**
     *  Adds support for a calendar search record type.
     *
     *  @param calendarSearchRecordType a calendar search record type
     *  @throws org.osid.NullArgumentException
     *  <code>calendarSearchRecordType</code> is <code>null</code>
     */

    protected void addCalendarSearchRecordType(org.osid.type.Type calendarSearchRecordType) {
        this.calendarSearchRecordTypes.add(calendarSearchRecordType);
        return;
    }


    /**
     *  Removes support for a calendar search record type.
     *
     *  @param calendarSearchRecordType a calendar search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>calendarSearchRecordType</code> is <code>null</code>
     */

    protected void removeCalendarSearchRecordType(org.osid.type.Type calendarSearchRecordType) {
        this.calendarSearchRecordTypes.remove(calendarSearchRecordType);
        return;
    }


    /**
     *  Gets all the spatial unit record types supported. 
     *
     *  @return the list of supported spatial unit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpatialUnitRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.spatialUnitRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given spatial unit record type is supported. 
     *
     *  @param  spatialUnitRecordType the spatial unit record type 
     *  @return <code> true </code> if the spatial unit record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        return (this.spatialUnitRecordTypes.contains(spatialUnitRecordType));
    }


    /**
     *  Adds support for a spatial unit record type.
     *
     *  @param spatialUnitRecordType a spatial unit record type
     *  @throws org.osid.NullArgumentException
     *  <code>spatialUnitRecordType</code> is <code>null</code>
     */

    protected void addSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        this.spatialUnitRecordTypes.add(spatialUnitRecordType);
        return;
    }


    /**
     *  Removes support for a spatial unit record type.
     *
     *  @param spatialUnitRecordType a spatial unit record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>spatialUnitRecordType</code> is <code>null</code>
     */

    protected void removeSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        this.spatialUnitRecordTypes.remove(spatialUnitRecordType);
        return;
    }


    /**
     *  Gets all the coordinate record types supported. 
     *
     *  @return the list of supported coordinate record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.coordinateRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given coordinate record type is supported. 
     *
     *  @param  coordinateRecordType the coordinate domain type 
     *  @return <code> true </code> if the coordinate record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> coordinateRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateRecordType(org.osid.type.Type coordinateRecordType) {
        return (this.coordinateRecordTypes.contains(coordinateRecordType));
    }


    /**
     *  Adds support for a coordinate record type.
     *
     *  @param coordinateRecordType a coordinate record type
     *  @throws org.osid.NullArgumentException
     *  <code>coordinateRecordType</code> is <code>null</code>
     */

    protected void addCoordinateRecordType(org.osid.type.Type coordinateRecordType) {
        this.coordinateRecordTypes.add(coordinateRecordType);
        return;
    }


    /**
     *  Removes support for a coordinate record type.
     *
     *  @param coordinateRecordType a coordinate record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>coordinateRecordType</code> is <code>null</code>
     */

    protected void removeCoordinateRecordType(org.osid.type.Type coordinateRecordType) {
        this.coordinateRecordTypes.remove(coordinateRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service. 
     *
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event lookup 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventLookupSession getEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service. 
     *
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event query 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuerySession getEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event search 
     *  service. 
     *
     *  @return an <code> EventSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchSession getEventSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchSession getEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event search 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchSession getEventSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event search 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSearchSession getEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event 
     *  administration service. 
     *
     *  @return an <code> EventAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventAdminSession getEventAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventAdminSession getEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event admin 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventAdminSession getEventAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event admin 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventAdminSession getEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to event 
     *  changes. 
     *
     *  @param  eventReceiver the event receiver 
     *  @return an <code> EventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> eventReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventNotificationSession getEventNotificationSession(org.osid.calendaring.EventReceiver eventReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to event 
     *  changes. 
     *
     *  @param  eventReceiver the event receiver 
     *  @param  proxy a proxy 
     *  @return an <code> EventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> eventReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventNotificationSession getEventNotificationSession(org.osid.calendaring.EventReceiver eventReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event 
     *  notification service for the given calendar. 
     *
     *  @param  eventReceiver the event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> eventReceiver </code> or 
     *          <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventNotificationSession getEventNotificationSessionForCalendar(org.osid.calendaring.EventReceiver eventReceiver, 
                                                                                                org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the event 
     *  notification service for the given calendar. 
     *
     *  @param  eventReceiver the event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> an EventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> eventReceiver, </code> 
     *          <code> calendarId </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventNotificationSession getEventNotificationSessionForCalendar(org.osid.calendaring.EventReceiver eventReceiver, 
                                                                                                org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving event to calendar mappings. 
     *
     *  @return an <code> EventCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventCalendar() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventCalendarSession getEventCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventCalendar() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventCalendarSession getEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning event to calendar mappings. 
     *
     *  @return an <code> EventCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventCalendarAssignmentSession getEventCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventCalendarAssignmentSession getEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the event smart calendar for the 
     *  given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> EventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventSmartCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSmartCalendarSession getEventSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the event smart calendar for the 
     *  given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> EventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEventSmartCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventSmartCalendarSession getEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event lookup service. 
     *
     *  @return a <code> RecurringEventLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventLookupSession getRecurringEventLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventLookupSession getRecurringEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> RecurringEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventLookupSession getRecurringEventLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventLookupSession getRecurringEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event query service. 
     *
     *  @return a <code> RecurringEventQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuerySession getRecurringEventQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuerySession getRecurringEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> RecurringEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuerySession getRecurringEventQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQuerySession getRecurringEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event search service. 
     *
     *  @return a <code> RecurringEventSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSearchSession getRecurringEventSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSearchSession getRecurringEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> RecurringEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSearchSession getRecurringEventSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSearchSession getRecurringEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event administration service. 
     *
     *  @return a <code> RecurringEventAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventAdminSession getRecurringEventAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventAdminSession getRecurringEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> RecurringEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventAdminSession getRecurringEventAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventAdminSession getRecurringEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  recurring event changes. 
     *
     *  @param  recurringEventReceiver the recurring event receiver 
     *  @return a <code> RecurringEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> recurringEventReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventNotificationSession getRecurringEventNotificationSession(org.osid.calendaring.RecurringEventReceiver recurringEventReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  recurring event changes. 
     *
     *  @param  recurringEventReceiver the recurring event receiver 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> recurringEventReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventNotificationSession getRecurringEventNotificationSession(org.osid.calendaring.RecurringEventReceiver recurringEventReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event notification service for the given calendar. 
     *
     *  @param  recurringEventReceiver the recurring event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> RecurringEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> recurringEventReceiver 
     *          </code> or <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventNotificationSession getRecurringEventNotificationSessionForCalendar(org.osid.calendaring.RecurringEventReceiver recurringEventReceiver, 
                                                                                                                  org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recurring 
     *  event notification service for the given calendar. 
     *
     *  @param  recurringEventReceiver the recurring event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> recurringEventReceiver, 
     *          </code> <code> calendarId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventNotificationSession getRecurringEventNotificationSessionForCalendar(org.osid.calendaring.RecurringEventReceiver recurringEventReceiver, 
                                                                                                                  org.osid.id.Id calendarId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving recurring event to calendar mappings. 
     *
     *  @return A <code> RecurringEventCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventCalendarSession getRecurringEventCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving recurring event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventCalendarSession getRecurringEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning recurring event to calendar mappings. 
     *
     *  @return a <code> RecurringEventCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventCalendarAssignmentSession getRecurringEventCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning recurring event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventCalendarAssignmentSession getRecurringEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the recurring event smart calendar 
     *  for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> RecurringEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSmartCalendarSession getRecurringEventSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getRecurringEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the recurring event smart calendar 
     *  for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> RecurringEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecurringEventSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventSmartCalendarSession getRecurringEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getRecurringEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event lookup service. 
     *
     *  @return a <code> SupersedingEventLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventLookupSession getSupersedingEventLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventLookupSession getSupersedingEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> SupersedingEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventLookupSession getSupersedingEventLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventLookupSession getSupersedingEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event query service. 
     *
     *  @return a <code> SupersedingEventQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuerySession getSupersedingEventQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuerySession getSupersedingEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> SupersedingEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuerySession getSupersedingEventQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventQuerySession getSupersedingEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event search service. 
     *
     *  @return a <code> SupersedingEventSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSearchSession getSupersedingEventSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSearchSession getSupersedingEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> SupersedingEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSearchSession getSupersedingEventSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSearchSession getSupersedingEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event administration service. 
     *
     *  @return a <code> SupersedingEventAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventAdminSession getSupersedingEventAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEventAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventAdminSession getSupersedingEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> SupersedingEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventAdminSession getSupersedingEventAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventAdminSession getSupersedingEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  superseding event changes. 
     *
     *  @param  supersedingEventReceiver the superseding event receiver 
     *  @return a <code> SupersedingEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> supersedingEventReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventNotificationSession getSupersedingEventNotificationSession(org.osid.calendaring.SupersedingEventReceiver supersedingEventReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  superseding event changes. 
     *
     *  @param  supersedingEventReceiver the superseding event receiver 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> supersedingEventReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventNotificationSession getSupersedingEventNotificationSession(org.osid.calendaring.SupersedingEventReceiver supersedingEventReceiver, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event notification service for the given calendar. 
     *
     *  @param  supersedingEventReceiver the superseding event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> SupersedingEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> supersedingEventReceiver 
     *          </code> or <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventNotificationSession getSupersedingEventNotificationSessionForCalendar(org.osid.calendaring.SupersedingEventReceiver supersedingEventReceiver, 
                                                                                                                      org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the superseding 
     *  event notification service for the given calendar. 
     *
     *  @param  supersedingEventReceiver the superseding event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventReceiver, calendarId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventNotificationSession getSupersedingEventNotificationSessionForCalendar(org.osid.calendaring.SupersedingEventReceiver supersedingEventReceiver, 
                                                                                                                      org.osid.id.Id calendarId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving superseding event to calendar 
     *  mappings. 
     *
     *  @return a <code> SupersedingEventCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventCalendarSession getSupersedingEventCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving superseding event to calendar 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventCalendarSession getSupersedingEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning superseding event to calendar mappings. 
     *
     *  @return a <code> SupersedingEventCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventCalendarAssignmentSession getSupersedingEventCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning superseding event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventCalendarAssignmentSession getSupersedingEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the superseding event smart calendar 
     *  for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> SupersedingEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSmartCalendarSession getSupersedingEventSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getSupersedingEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the superseding event smart calendar 
     *  for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> SupersedingEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingEventSmartCalendar() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventSmartCalendarSession getSupersedingEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getSupersedingEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  lookup service. 
     *
     *  @return an <code> OffsetEventLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventLookupSession getOffsetEventLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventLookupSession getOffsetEventLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> OffsetEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventLookupSession getOffsetEventLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventLookupSession getOffsetEventLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  query service. 
     *
     *  @return an <code> OffsetEventQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuerySession getOffsetEventQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuerySession getOffsetEventQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> OffsetEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuerySession getOffsetEventQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventQuerySession getOffsetEventQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  search service. 
     *
     *  @return an <code> OffsetEventSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSearchSession getOffsetEventSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSearchSession getOffsetEventSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> OffsetEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSearchSession getOffsetEventSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSearchSession getOffsetEventSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  administration service. 
     *
     *  @return an <code> OffsetEventAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventAdminSession getOffsetEventAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventAdminSession getOffsetEventAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> OffsetEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventAdminSession getOffsetEventAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventAdminSession getOffsetEventAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to offset 
     *  event changes. 
     *
     *  @param  offsetEventReceiver the offset event receiver 
     *  @return an <code> OffsetEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> offsetEventReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventNotificationSession getOffsetEventNotificationSession(org.osid.calendaring.OffsetEventReceiver offsetEventReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to offset 
     *  event changes. 
     *
     *  @param  offsetEventReceiver the offset event receiver 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> offsetEventReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventNotificationSession getOffsetEventNotificationSession(org.osid.calendaring.OffsetEventReceiver offsetEventReceiver, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  notification service for the given calendar. 
     *
     *  @param  offsetEventReceiver the offset event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> OffsetEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> offsetEventReceiver 
     *          </code> or <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventNotificationSession getOffsetEventNotificationSessionForCalendar(org.osid.calendaring.OffsetEventReceiver offsetEventReceiver, 
                                                                                                            org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offset event 
     *  notification service for the given calendar. 
     *
     *  @param  offsetEventReceiver the offset event receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> offsetEventReceiver, 
     *          calendarId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventNotificationSession getOffsetEventNotificationSessionForCalendar(org.osid.calendaring.OffsetEventReceiver offsetEventReceiver, 
                                                                                                            org.osid.id.Id calendarId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving offset event to calendar mappings. 
     *
     *  @return an <code> OffsetEventCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventCalendarSession getOffsetEventCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving offset event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventCalendarSession getOffsetEventCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning offset event to calendar mappings. 
     *
     *  @return an <code> OffsetEventCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventCalendarAssignmentSession getOffsetEventCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning offset event to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventCalendarAssignmentSession getOffsetEventCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the offset event smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return an <code> OffsetEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSmartCalendarSession getOffsetEventSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getOffsetEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the offset event smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return an <code> OffsetEventSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOffsetEventSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.OffsetEventSmartCalendarSession getOffsetEventSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getOffsetEventSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  lookup service. 
     *
     *  @return a <code> ScheduleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleLookupSession getScheduleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleLookupSession getScheduleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleLookupSession getScheduleLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleLookupSession getScheduleLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule query 
     *  service. 
     *
     *  @return a <code> ScheduleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuerySession getScheduleQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuerySession getScheduleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule query 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuerySession getScheduleQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule query 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuerySession getScheduleQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  search service. 
     *
     *  @return a <code> ScheduleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSearchSession getScheduleSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSearchSession getScheduleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSearchSession getScheduleSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSearchSession getScheduleSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  administration service. 
     *
     *  @return a <code> ScheduleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleAdminSession getScheduleAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleAdminSession getScheduleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule admin 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleAdminSession getScheduleAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule admin 
     *  service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleAdminSession getScheduleAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to schedule 
     *  changes. 
     *
     *  @param  scheduleReceiver the schedule receiver 
     *  @return a <code> ScheduleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> scheduleReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleNotificationSession getScheduleNotificationSession(org.osid.calendaring.ScheduleReceiver scheduleReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to schedule 
     *  changes. 
     *
     *  @param  scheduleReceiver the schedule receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> scheduleReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleNotificationSession getScheduleNotificationSession(org.osid.calendaring.ScheduleReceiver scheduleReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  notification service for the given calendar. 
     *
     *  @param  scheduleReceiver the schedule receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> scheduleReceiver </code> 
     *          or <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleNotificationSession getScheduleNotificationSessionForCalendar(org.osid.calendaring.ScheduleReceiver scheduleReceiver, 
                                                                                                      org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule 
     *  notification service for the given calendar. 
     *
     *  @param  scheduleReceiver the schedule receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> scheduleReceiver, 
     *          calendarId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleNotificationSession getScheduleNotificationSessionForCalendar(org.osid.calendaring.ScheduleReceiver scheduleReceiver, 
                                                                                                      org.osid.id.Id calendarId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving schedule to calendar mappings. 
     *
     *  @return a <code> ScheduleCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleCalendarSession getScheduleCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving schedule to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleCalendarSession getScheduleCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning schedule to calendar mappings. 
     *
     *  @return a <code> ScheduleCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleCalendarAssignmentSession getScheduleCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning schedule to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleCalendarAssignmentSession getScheduleCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the schedule smart calendar for the 
     *  given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSmartCalendarSession getScheduleSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the schedule smart calendar for the 
     *  given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSmartCalendarSession getScheduleSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  lookup service. 
     *
     *  @return a <code> ScheduleSlotLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotLookupSession getScheduleSlotLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotLookupSession getScheduleSlotLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleSlotLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotLookupSession getScheduleSlotLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotLookupSession getScheduleSlotLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  query service. 
     *
     *  @return a <code> ScheduleSlotQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuerySession getScheduleSlotQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuerySession getScheduleSlotQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleSlotQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuerySession getScheduleSlotQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotQuerySession getScheduleSlotQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  search service. 
     *
     *  @return a <code> ScheduleSlotSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSearchSession getScheduleSlotSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSearchSession getScheduleSlotSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleSlotSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSearchSession getScheduleSlotSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSearchSession getScheduleSlotSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  administration service. 
     *
     *  @return a <code> ScheduleSlotAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotAdminSession getScheduleSlotAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotAdminSession getScheduleSlotAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleSlotAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotAdminSession getScheduleSlotAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotAdminSession getScheduleSlotAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to schedule 
     *  slot changes. 
     *
     *  @param  scheduleSlotReceiver the schedule slot receiver 
     *  @return a <code> ScheduleSlotNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotNotificationSession getScheduleSlotNotificationSession(org.osid.calendaring.ScheduleSlotReceiver scheduleSlotReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to schedule 
     *  slot changes. 
     *
     *  @param  scheduleSlotReceiver the schedule slot receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotNotificationSession getScheduleSlotNotificationSession(org.osid.calendaring.ScheduleSlotReceiver scheduleSlotReceiver, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  notification service for the given calendar. 
     *
     *  @param  scheduleSlotReceiver the schedule slot receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleSlotNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotReceiver 
     *          </code> or <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotNotificationSession getScheduleSlotNotificationSessionForCalendar(org.osid.calendaring.ScheduleSlotReceiver scheduleSlotReceiver, 
                                                                                                              org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the schedule slot 
     *  notification service for the given calendar. 
     *
     *  @param  scheduleSlotReceiver the schedule slot receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotReceiver, 
     *          calendarId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotNotificationSession getScheduleSlotNotificationSessionForCalendar(org.osid.calendaring.ScheduleSlotReceiver scheduleSlotReceiver, 
                                                                                                              org.osid.id.Id calendarId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving schedule slot to calendar mappings. 
     *
     *  @return a <code> ScheduleSlotCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotCalendarSession getScheduleSlotCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving schedule slot to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotCalendarSession getScheduleSlotCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning schedule slot to calendar mappings. 
     *
     *  @return a <code> ScheduleSlotCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotCalendarAssignmentSession getScheduleSlotCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning schedule slot to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotCalendarAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotCalendarAssignmentSession getScheduleSlotCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the schedule slot smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> ScheduleSlotSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSmartCalendarSession getScheduleSlotSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getScheduleSlotSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the schedule slot smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> ScheduleSlotSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsScheduleSlotSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlotSmartCalendarSession getScheduleSlotSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getScheduleSlotSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  lookup service. 
     *
     *  @return a <code> CommitmentLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentLookupSession getCommitmentLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentLookupSession getCommitmentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CommitmentLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentLookupSession getCommitmentLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> a CommitmentLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentLookupSession getCommitmentLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  query service. 
     *
     *  @return a <code> CommitmentQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuerySession getCommitmentQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuerySession getCommitmentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CommitmentQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuerySession getCommitmentQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuerySession getCommitmentQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  search service. 
     *
     *  @return a <code> CommitmentSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentSearchSession getCommitmentSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentSearchSession getCommitmentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CommitmentSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentSearchSession getCommitmentSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentSearchSession getCommitmentSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  administration service. 
     *
     *  @return a <code> CommitmentAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentAdminSession getCommitmentAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentAdminSession getCommitmentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CommitmenttAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentAdminSession getCommitmentAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmenttAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentAdminSession getCommitmentAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  commitment changes. 
     *
     *  @param  commitmentReceiver the commitment receiver 
     *  @return a <code> CommitmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> commitmentReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentNotificationSession getCommitmentNotificationSession(org.osid.calendaring.CommitmentReceiver commitmentReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  commitment changes. 
     *
     *  @param  commitmentReceiver the commitment receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> commitmentReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentNotificationSession getCommitmentNotificationSession(org.osid.calendaring.CommitmentReceiver commitmentReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  notification service for the given calendar. 
     *
     *  @param  commitmentReceiver the commitment receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CommitmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> commitmentReceiver 
     *          </code> or <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentNotificationSession getCommitmentNotificationSessionForCalendar(org.osid.calendaring.CommitmentReceiver commitmentReceiver, 
                                                                                                          org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the commitment 
     *  notification service for the given calendar. 
     *
     *  @param  commitmentReceiver the commitment receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> commitmentReceiver, 
     *          calendarId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentNotificationSession getCommitmentNotificationSessionForCalendar(org.osid.calendaring.CommitmentReceiver commitmentReceiver, 
                                                                                                          org.osid.id.Id calendarId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving commitment to calendar mappings. 
     *
     *  @return a <code> CommitmentCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentCalendarSession getCommitmentCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving commitment to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentCalendarSession getCommitmentCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning commitment to calendar mappings. 
     *
     *  @return a <code> CommitmentCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentCalendarAssignmentSession getCommitmentCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning commitment to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentCalendarAssignmentSession getCommitmentCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the commitment smart calendar for the 
     *  given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> CommitmentSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentSmartCalendarSession getCommitmentSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCommitmentSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the commitment smart calendar for the 
     *  given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> CommitmentSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentSmartCalendarSession getCommitmentSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCommitmentSmartCalendarSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  lookup service. 
     *
     *  @return a <code> TimePeriodLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodLookupSession getTimePeriodLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodLookupSession getTimePeriodLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> TimePeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodLookupSession getTimePeriodLookupSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  lookup service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodLookupSession getTimePeriodLookupSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodLookupSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  query service. 
     *
     *  @return a <code> TimePeriodQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuerySession getTimePeriodQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuerySession getTimePeriodQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> TimePeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuerySession getTimePeriodQuerySessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  query service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuerySession getTimePeriodQuerySessionForCalendar(org.osid.id.Id calendarId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodQuerySessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  search service. 
     *
     *  @return a <code> TimePeriodSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchSession getTimePeriodSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchSession getTimePeriodSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> TimePeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchSession getTimePeriodSearchSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  search service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSearchSession getTimePeriodSearchSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodSearchSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  administration service. 
     *
     *  @return a <code> TimePeriodAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodAdminSession getTimePeriodAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodAdminSession getTimePeriodAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> TimePeriodAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodAdminSession getTimePeriodAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  admin service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> calendarId </code> not found 
     *  @throws org.osid.NotFoundException a <code> TimePeriodAdminSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodAdminSession getTimePeriodAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to time 
     *  period changes. 
     *
     *  @param  timePeriodReceiver the time period receiver 
     *  @return a <code> TimePeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> timePeriodReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodNotificationSession getTimePeriodNotificationSession(org.osid.calendaring.TimePeriodReceiver timePeriodReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to time 
     *  period changes. 
     *
     *  @param  timePeriodReceiver the time period receiver 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> timePeriodReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodNotificationSession getTimePeriodNotificationSession(org.osid.calendaring.TimePeriodReceiver timePeriodReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  notification service for the given calendar. 
     *
     *  @param  timePeriodReceiver the time period receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return <code> a TimePeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> timePeriodReceiver 
     *          </code> or <code> calendarId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodNotificationSession getTimePeriodNotificationSessionForCalendar(org.osid.calendaring.TimePeriodReceiver timePeriodReceiver, 
                                                                                                          org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the time period 
     *  notification service for the given calendar. 
     *
     *  @param  timePeriodReceiver the time period receiver 
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return <code> a TimePeriodNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> timePeriodReceiver 
     *          </code> or <code> calendarId </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodNotificationSession getTimePeriodNotificationSessionForCalendar(org.osid.calendaring.TimePeriodReceiver timePeriodReceiver, 
                                                                                                          org.osid.id.Id calendarId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodNotificationSessionForCalendar not implemented");
    }


    /**
     *  Gets the session for retrieving time period to calendar mappings. 
     *
     *  @return a <code> TimePeriodCalendarSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodCalendarSession getTimePeriodCalendarSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodCalendarSession not implemented");
    }


    /**
     *  Gets the session for retrieving time period to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCalendarSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCalendar() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodCalendarSession getTimePeriodCalendarSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodCalendarSession not implemented");
    }


    /**
     *  Gets the session for assigning time period to calendar mappings. 
     *
     *  @return a <code> TimePeriodCalendarAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodCalendarAssignmentSession getTimePeriodCalendarAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning time period to calendar mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodCalendarAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodCalendarAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodCalendarAssignmentSession getTimePeriodCalendarAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodCalendarAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the time period smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @return a <code> TimePeriodSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSmartCalendarSession getTimePeriodSmartCalendarSession(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getTimePeriodSmartCalendarSession not implemented");
    }


    /**
     *  Gets the session associated with the time period smart calendar for 
     *  the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the calendar 
     *  @param  proxy a proxy 
     *  @return a <code> TimePeriodSmartCalendarSession </code> 
     *  @throws org.osid.NotFoundException <code> calendarId </code> not found 
     *  @throws org.osid.NullArgumentException <code> calendarId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodSmartCalendar() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodSmartCalendarSession getTimePeriodSmartCalendarSession(org.osid.id.Id calendarId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getTimePeriodSmartCalendarSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the calendar lookup service. 
     *
     *  @return a <code> CalendarLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarLookupSession getCalendarLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCalendarLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the calendar lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarLookup() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarLookupSession getCalendarLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCalendarLookupSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the calendar search service. 
     *
     *  @return a <code> CalendarSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarSearch() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarSearchSession getCalendarSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCalendarSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the calendar search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarSearch() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarSearchSession getCalendarSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCalendarSearchSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the calendar administration 
     *  service. 
     *
     *  @return a <code> CalendarAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarAdmin() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarAdminSession getCalendarAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCalendarAdminSession not implemented");
    }


    /**
     *  Gets the OsidSession associated with the calendar administration 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarAdmin() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarAdminSession getCalendarAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCalendarAdminSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to calendar 
     *  service changes. 
     *
     *  @param  calendarReceiver the calendar receiver 
     *  @return a <code> CalendarNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarNotificationSession getCalendarNotificationSession(org.osid.calendaring.CalendarReceiver calendarReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCalendarNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to calendar 
     *  service changes. 
     *
     *  @param  calendarReceiver the calendar receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CalendarNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> calendarReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarNotificationSession getCalendarNotificationSession(org.osid.calendaring.CalendarReceiver calendarReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCalendarNotificationSession not implemented");
    }


    /**
     *  Gets the session traversing calendar hierarchies. 
     *
     *  @return a <code> CalendarHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarHierarchySession getCalendarHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCalendarHierarchySession not implemented");
    }


    /**
     *  Gets the session traversing calendar hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarHierarchy() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarHierarchySession getCalendarHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCalendarHierarchySession not implemented");
    }


    /**
     *  Gets the session designing calendar hierarchies. 
     *
     *  @return a <code> CalendarHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarHierarchyDesignSession getCalendarHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCalendarHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the session designing calendar hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CalendarHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendarHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarHierarchyDesignSession getCalendarHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCalendarHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the calendaring batch manager. 
     *
     *  @return a <code> CalendaringBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.CalendaringBatchManager getCalandaringBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCalandaringBatchManager not implemented");
    }


    /**
     *  Gets the calendaring batch proxy manager. 
     *
     *  @return a <code> CalendaringBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.batch.CalendaringBatchProxyManager getCalandaringBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCalandaringBatchProxyManager not implemented");
    }


    /**
     *  Gets the calendaring cycle manager. 
     *
     *  @return a <code> CalendaringCycleManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringCycle() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CalendaringCycleManager getCalandaringCycleManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCalandaringCycleManager not implemented");
    }


    /**
     *  Gets the calendaring cycle proxy manager. 
     *
     *  @return a <code> CalendaringCycleProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringCycle() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CalendaringCycleProxyManager getCalandaringCycleProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCalandaringCycleProxyManager not implemented");
    }


    /**
     *  Gets the calendaring rules manager. 
     *
     *  @return a <code> CalendaringRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CalendaringRulesManager getCalandaringRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringManager.getCalandaringRulesManager not implemented");
    }


    /**
     *  Gets the calendaring rules proxy manager. 
     *
     *  @return a <code> CalendaringRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CalendaringRulesProxyManager getCalandaringRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.CalendaringProxyManager.getCalandaringRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.eventRecordTypes.clear();
        this.eventRecordTypes.clear();

        this.eventSearchRecordTypes.clear();
        this.eventSearchRecordTypes.clear();

        this.recurringEventRecordTypes.clear();
        this.recurringEventRecordTypes.clear();

        this.recurringEventSearchRecordTypes.clear();
        this.recurringEventSearchRecordTypes.clear();

        this.supersedingEventRecordTypes.clear();
        this.supersedingEventRecordTypes.clear();

        this.supersedingEventSearchRecordTypes.clear();
        this.supersedingEventSearchRecordTypes.clear();

        this.offsetEventRecordTypes.clear();
        this.offsetEventRecordTypes.clear();

        this.offsetEventSearchRecordTypes.clear();
        this.offsetEventSearchRecordTypes.clear();

        this.scheduleRecordTypes.clear();
        this.scheduleRecordTypes.clear();

        this.scheduleSearchRecordTypes.clear();
        this.scheduleSearchRecordTypes.clear();

        this.scheduleSlotRecordTypes.clear();
        this.scheduleSlotRecordTypes.clear();

        this.scheduleSlotSearchRecordTypes.clear();
        this.scheduleSlotSearchRecordTypes.clear();

        this.timePeriodRecordTypes.clear();
        this.timePeriodRecordTypes.clear();

        this.timePeriodSearchRecordTypes.clear();
        this.timePeriodSearchRecordTypes.clear();

        this.commitmentRecordTypes.clear();
        this.commitmentRecordTypes.clear();

        this.commitmentSearchRecordTypes.clear();
        this.commitmentSearchRecordTypes.clear();

        this.calendarRecordTypes.clear();
        this.calendarRecordTypes.clear();

        this.calendarSearchRecordTypes.clear();
        this.calendarSearchRecordTypes.clear();

        this.spatialUnitRecordTypes.clear();
        this.spatialUnitRecordTypes.clear();

        this.coordinateRecordTypes.clear();
        this.coordinateRecordTypes.clear();

        return;
    }
}
