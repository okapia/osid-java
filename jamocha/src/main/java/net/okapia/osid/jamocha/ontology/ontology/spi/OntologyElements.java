//
// OntologyElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.ontology.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class OntologyElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the OntologyElement Id.
     *
     *  @return the ontology element Id
     */

    public static org.osid.id.Id getOntologyEntityId() {
        return (makeEntityId("osid.ontology.Ontology"));
    }


    /**
     *  Gets the SubjectId element Id.
     *
     *  @return the SubjectId element Id
     */

    public static org.osid.id.Id getSubjectId() {
        return (makeQueryElementId("osid.ontology.ontology.SubjectId"));
    }


    /**
     *  Gets the Subject element Id.
     *
     *  @return the Subject element Id
     */

    public static org.osid.id.Id getSubject() {
        return (makeQueryElementId("osid.ontology.ontology.Subject"));
    }


    /**
     *  Gets the RelevancyId element Id.
     *
     *  @return the RelevancyId element Id
     */

    public static org.osid.id.Id getRelevancyId() {
        return (makeQueryElementId("osid.ontology.ontology.RelevancyId"));
    }


    /**
     *  Gets the Relevancy element Id.
     *
     *  @return the Relevancy element Id
     */

    public static org.osid.id.Id getRelevancy() {
        return (makeQueryElementId("osid.ontology.ontology.Relevancy"));
    }


    /**
     *  Gets the AncestorOntologyId element Id.
     *
     *  @return the AncestorOntologyId element Id
     */

    public static org.osid.id.Id getAncestorOntologyId() {
        return (makeQueryElementId("osid.ontology.ontology.AncestorOntologyId"));
    }


    /**
     *  Gets the AncestorOntology element Id.
     *
     *  @return the AncestorOntology element Id
     */

    public static org.osid.id.Id getAncestorOntology() {
        return (makeQueryElementId("osid.ontology.ontology.AncestorOntology"));
    }


    /**
     *  Gets the DescendantOntologyId element Id.
     *
     *  @return the DescendantOntologyId element Id
     */

    public static org.osid.id.Id getDescendantOntologyId() {
        return (makeQueryElementId("osid.ontology.ontology.DescendantOntologyId"));
    }


    /**
     *  Gets the DescendantOntology element Id.
     *
     *  @return the DescendantOntology element Id
     */

    public static org.osid.id.Id getDescendantOntology() {
        return (makeQueryElementId("osid.ontology.ontology.DescendantOntology"));
    }
}
