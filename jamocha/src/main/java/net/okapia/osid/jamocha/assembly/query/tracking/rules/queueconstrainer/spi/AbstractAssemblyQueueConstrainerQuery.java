//
// AbstractAssemblyQueueConstrainerQuery.java
//
//     A QueueConstrainerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.tracking.rules.queueconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A QueueConstrainerQuery that stores terms.
 */

public abstract class AbstractAssemblyQueueConstrainerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidConstrainerQuery
    implements org.osid.tracking.rules.QueueConstrainerQuery,
               org.osid.tracking.rules.QueueConstrainerQueryInspector,
               org.osid.tracking.rules.QueueConstrainerSearchOrder {

    private final java.util.Collection<org.osid.tracking.rules.records.QueueConstrainerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.rules.records.QueueConstrainerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.rules.records.QueueConstrainerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyQueueConstrainerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyQueueConstrainerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to a queue. 
     *
     *  @param  queueId the queue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledQueueId(org.osid.id.Id queueId, boolean match) {
        getAssembler().addIdTerm(getRuledQueueIdColumn(), queueId, match);
        return;
    }


    /**
     *  Clears the queue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledQueueIdTerms() {
        getAssembler().clearTerms(getRuledQueueIdColumn());
        return;
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledQueueIdTerms() {
        return (getAssembler().getIdTerms(getRuledQueueIdColumn()));
    }


    /**
     *  Gets the RuledQueueId column name.
     *
     * @return the column name
     */

    protected String getRuledQueueIdColumn() {
        return ("ruled_queue_id");
    }


    /**
     *  Tests if a <code> QueueQuery </code> is available. 
     *
     *  @return <code> true </code> if a queue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledQueueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a queue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the queue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledQueueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuery getRuledQueueQuery() {
        throw new org.osid.UnimplementedException("supportsRuledQueueQuery() is false");
    }


    /**
     *  Matches constrainers mapped to any queue. 
     *
     *  @param  match <code> true </code> for constrainers mapped to any 
     *          queue, <code> false </code> to match constrainers mapped to no 
     *          queues 
     */

    @OSID @Override
    public void matchAnyRuledQueue(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledQueueColumn(), match);
        return;
    }


    /**
     *  Clears the queue query terms. 
     */

    @OSID @Override
    public void clearRuledQueueTerms() {
        getAssembler().clearTerms(getRuledQueueColumn());
        return;
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.QueueQueryInspector[] getRuledQueueTerms() {
        return (new org.osid.tracking.QueueQueryInspector[0]);
    }


    /**
     *  Gets the RuledQueue column name.
     *
     * @return the column name
     */

    protected String getRuledQueueColumn() {
        return ("ruled_queue");
    }


    /**
     *  Matches constrainers mapped to the front office. 
     *
     *  @param  frontOfficeId the front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFrontOfficeId(org.osid.id.Id frontOfficeId, boolean match) {
        getAssembler().addIdTerm(getFrontOfficeIdColumn(), frontOfficeId, match);
        return;
    }


    /**
     *  Clears the front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeIdTerms() {
        getAssembler().clearTerms(getFrontOfficeIdColumn());
        return;
    }


    /**
     *  Gets the front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFrontOfficeIdTerms() {
        return (getAssembler().getIdTerms(getFrontOfficeIdColumn()));
    }


    /**
     *  Gets the FrontOfficeId column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeIdColumn() {
        return ("front_office_id");
    }


    /**
     *  Tests if an <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a front office. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsFrontOfficeQuery() is false");
    }


    /**
     *  Clears the front office query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeTerms() {
        getAssembler().clearTerms(getFrontOfficeColumn());
        return;
    }


    /**
     *  Gets the front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }


    /**
     *  Gets the FrontOffice column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeColumn() {
        return ("front_office");
    }


    /**
     *  Tests if this queueConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  queueConstrainerRecordType a queue constrainer record type 
     *  @return <code>true</code> if the queueConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type queueConstrainerRecordType) {
        for (org.osid.tracking.rules.records.QueueConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  queueConstrainerRecordType the queue constrainer record type 
     *  @return the queue constrainer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueConstrainerQueryRecord getQueueConstrainerQueryRecord(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  queueConstrainerRecordType the queue constrainer record type 
     *  @return the queue constrainer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueConstrainerQueryInspectorRecord getQueueConstrainerQueryInspectorRecord(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueConstrainerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param queueConstrainerRecordType the queue constrainer record type
     *  @return the queue constrainer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.rules.records.QueueConstrainerSearchOrderRecord getQueueConstrainerSearchOrderRecord(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.rules.records.QueueConstrainerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(queueConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this queue constrainer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param queueConstrainerQueryRecord the queue constrainer query record
     *  @param queueConstrainerQueryInspectorRecord the queue constrainer query inspector
     *         record
     *  @param queueConstrainerSearchOrderRecord the queue constrainer search order record
     *  @param queueConstrainerRecordType queue constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>queueConstrainerQueryRecord</code>,
     *          <code>queueConstrainerQueryInspectorRecord</code>,
     *          <code>queueConstrainerSearchOrderRecord</code> or
     *          <code>queueConstrainerRecordTypequeueConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addQueueConstrainerRecords(org.osid.tracking.rules.records.QueueConstrainerQueryRecord queueConstrainerQueryRecord, 
                                      org.osid.tracking.rules.records.QueueConstrainerQueryInspectorRecord queueConstrainerQueryInspectorRecord, 
                                      org.osid.tracking.rules.records.QueueConstrainerSearchOrderRecord queueConstrainerSearchOrderRecord, 
                                      org.osid.type.Type queueConstrainerRecordType) {

        addRecordType(queueConstrainerRecordType);

        nullarg(queueConstrainerQueryRecord, "queue constrainer query record");
        nullarg(queueConstrainerQueryInspectorRecord, "queue constrainer query inspector record");
        nullarg(queueConstrainerSearchOrderRecord, "queue constrainer search odrer record");

        this.queryRecords.add(queueConstrainerQueryRecord);
        this.queryInspectorRecords.add(queueConstrainerQueryInspectorRecord);
        this.searchOrderRecords.add(queueConstrainerSearchOrderRecord);
        
        return;
    }
}
