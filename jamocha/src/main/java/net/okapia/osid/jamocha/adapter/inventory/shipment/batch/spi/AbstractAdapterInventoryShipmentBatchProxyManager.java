//
// AbstractInventoryShipmentBatchProxyManager.java
//
//     An adapter for a InventoryShipmentBatchProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.shipment.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a InventoryShipmentBatchProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterInventoryShipmentBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.inventory.shipment.batch.InventoryShipmentBatchProxyManager>
    implements org.osid.inventory.shipment.batch.InventoryShipmentBatchProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterInventoryShipmentBatchProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterInventoryShipmentBatchProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterInventoryShipmentBatchProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterInventoryShipmentBatchProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of shipments is available. 
     *
     *  @return <code> true </code> if a shipment bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsShipmentBatchAdmin() {
        return (getAdapteeManager().supportsShipmentBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk shipment 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ShipmentBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.batch.ShipmentBatchAdminSession getShipmentBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentBatchAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk shipment 
     *  administration service for the given warehouse. 
     *
     *  @param  warehouseId the <code> Id </code> of the <code> Warehouse 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ShipmentBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Warehouse </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsShipmentBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.shipment.batch.ShipmentBatchAdminSession getShipmentBatchAdminSessionForWarehouse(org.osid.id.Id warehouseId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getShipmentBatchAdminSessionForWarehouse(warehouseId, proxy));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
