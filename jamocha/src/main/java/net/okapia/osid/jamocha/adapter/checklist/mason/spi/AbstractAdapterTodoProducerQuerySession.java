//
// AbstractQueryTodoProducerLookupSession.java
//
//    A TodoProducerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.checklist.mason.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A TodoProducerQuerySession adapter.
 */

public abstract class AbstractAdapterTodoProducerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.checklist.mason.TodoProducerQuerySession {

    private final org.osid.checklist.mason.TodoProducerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterTodoProducerQuerySession.
     *
     *  @param session the underlying todo producer query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTodoProducerQuerySession(org.osid.checklist.mason.TodoProducerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeChecklist</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeChecklist Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getChecklistId() {
        return (this.session.getChecklistId());
    }


    /**
     *  Gets the {@codeChecklist</code> associated with this 
     *  session.
     *
     *  @return the {@codeChecklist</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getChecklist());
    }


    /**
     *  Tests if this user can perform {@codeTodoProducer</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchTodoProducers() {
        return (this.session.canSearchTodoProducers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todo producers in checklists which are children
     *  of this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        this.session.useFederatedChecklistView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this checklist only.
     */
    
    @OSID @Override
    public void useIsolatedChecklistView() {
        this.session.useIsolatedChecklistView();
        return;
    }
    
      
    /**
     *  Gets a todo producer query. The returned query will not have an
     *  extension query.
     *
     *  @return the todo producer query 
     */
      
    @OSID @Override
    public org.osid.checklist.mason.TodoProducerQuery getTodoProducerQuery() {
        return (this.session.getTodoProducerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  todoProducerQuery the todo producer query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code todoProducerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code todoProducerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.checklist.mason.TodoProducerList getTodoProducersByQuery(org.osid.checklist.mason.TodoProducerQuery todoProducerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getTodoProducersByQuery(todoProducerQuery));
    }
}
