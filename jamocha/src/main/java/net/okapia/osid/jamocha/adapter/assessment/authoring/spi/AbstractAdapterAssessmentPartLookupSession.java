//
// AbstractAdapterAssessmentPartLookupSession.java
//
//    An AssessmentPart lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AssessmentPart lookup session adapter.
 */

public abstract class AbstractAdapterAssessmentPartLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.assessment.authoring.AssessmentPartLookupSession {

    private final org.osid.assessment.authoring.AssessmentPartLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentPartLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAssessmentPartLookupSession(org.osid.assessment.authoring.AssessmentPartLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the {@code Bank} associated with this session.
     *
     *  @return the {@code Bank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform {@code AssessmentPart} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAssessmentParts() {
        return (this.session.canLookupAssessmentParts());
    }


    /**
     *  A complete view of the {@code AssessmentPart} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentPartView() {
        this.session.useComparativeAssessmentPartView();
        return;
    }


    /**
     *  A complete view of the {@code AssessmentPart} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentPartView() {
        this.session.usePlenaryAssessmentPartView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessment parts in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    

    /**
     *  Only active assessment parts are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAssessmentPartView() {
        this.session.useActiveAssessmentPartView();
        return;
    }


    /**
     *  Active and inactive assessment parts are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAssessmentPartView() {
        this.session.useAnyStatusAssessmentPartView();
        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  assessment parts.
     */

    @OSID @Override
    public void useSequesteredAssessmentPartView() {
        this.session.useSequesteredAssessmentPartView();
        return;
    }


    /**
     *  All assessment parts are returned including sequestered
     *  assessment parts.
     */

    @OSID @Override
    public void useUnsequesteredAssessmentPartView() {
        this.session.useUnsequesteredAssessmentPartView();
        return;
    }

     
    /**
     *  Gets the {@code AssessmentPart} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code AssessmentPart} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code AssessmentPart} and
     *  retained for compatibility.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @param assessmentPartId {@code Id} of the {@code AssessmentPart}
     *  @return the assessment part
     *  @throws org.osid.NotFoundException {@code assessmentPartId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code assessmentPartId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getAssessmentPart(org.osid.id.Id assessmentPartId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentPart(assessmentPartId));
    }


    /**
     *  Gets an {@code AssessmentPartList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentParts specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code AssessmentParts} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @param  assessmentPartIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code AssessmentPart} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentPartIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByIds(org.osid.id.IdList assessmentPartIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentPartsByIds(assessmentPartIds));
    }


    /**
     *  Gets an {@code AssessmentPartList} corresponding to the given
     *  assessment part genus {@code Type} which does not include
     *  assessment parts of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned
     *  list may contain only those assessment parts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, assessment parts are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment parts are returned.
     *
     *  @param  assessmentPartGenusType an assessmentPart genus type 
     *  @return the returned {@code AssessmentPart} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentPartGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByGenusType(org.osid.type.Type assessmentPartGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentPartsByGenusType(assessmentPartGenusType));
    }


    /**
     *  Gets an {@code AssessmentPartList} corresponding to the given
     *  assessment part genus {@code Type} and include any additional
     *  assessment parts with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned
     *  list may contain only those assessment parts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, assessment parts are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment parts are returned.
     *
     *  @param  assessmentPartGenusType an assessmentPart genus type 
     *  @return the returned {@code AssessmentPart} list
     *  @throws org.osid.NullArgumentException {@code
     *          assessmentPartGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByParentGenusType(org.osid.type.Type assessmentPartGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentPartsByParentGenusType(assessmentPartGenusType));
    }


    /**
     *  Gets an {@code AssessmentPartList} containing the given
     *  assessment part record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned list
     *  may contain only those assessment parts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *
     *  @param  assessmentPartRecordType an assessmentPart record type 
     *  @return the returned {@code AssessmentPart} list
     *  @throws org.osid.NullArgumentException
     *          {@code assessmentPartRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByRecordType(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentPartsByRecordType(assessmentPartRecordType));
    }


    /**
     *  Gets an <code> AssessmentPart </code> for the given
     *  assessment.
     *  
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned
     *  list may contain only those assessment parts that are
     *  accessible through this session.
     *  
     *  In active mode, assessment parts are returned that are
     *  currently active. In any effective mode, active and inactive
     *  assessment parts are returned.
     *  
     *  In sequestered mode, no sequestered assessment parts are
     *  returned. In unsequestered mode, all assessment parts are
     *  returned.
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @return the returned <code> AssessmentPart </code> list 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentPartsForAssessment(assessmentId));
    }


    /**
     *  Gets all {@code AssessmentParts}. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned
     *  list may contain only those assessment parts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @return a list of {@code AssessmentParts} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentParts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAssessmentParts());
    }
}
