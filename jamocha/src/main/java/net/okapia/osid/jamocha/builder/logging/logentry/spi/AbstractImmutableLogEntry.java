//
// AbstractImmutableLogEntry.java
//
//     Wraps a mutable LogEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.logging.logentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>LogEntry</code> to hide modifiers. This
 *  wrapper provides an immutized LogEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying logEntry whose state changes are visible.
 */

public abstract class AbstractImmutableLogEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.logging.LogEntry {

    private final org.osid.logging.LogEntry logEntry;


    /**
     *  Constructs a new <code>AbstractImmutableLogEntry</code>.
     *
     *  @param logEntry the log entry to immutablize
     *  @throws org.osid.NullArgumentException <code>logEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableLogEntry(org.osid.logging.LogEntry logEntry) {
        super(logEntry);
        this.logEntry = logEntry;
        return;
    }


    /**
     *  Gets the priority level of this entry. 
     *
     *  @return the priority level 
     */

    @OSID @Override
    public org.osid.type.Type getPriority() {
        return (this.logEntry.getPriority());
    }


    /**
     *  Gets the time this entry was logged. 
     *
     *  @return the time stamp of this entry 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getTimestamp() {
        return (this.logEntry.getTimestamp());
    }


    /**
     *  Gets the resource <code> Id </code> who created this entry. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.logEntry.getResourceId());
    }


    /**
     *  Gets the <code> Resource </code> who created this entry. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.logEntry.getResource());
    }


    /**
     *  Gets the agent <code> Id </code> who created this entry. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.logEntry.getAgentId());
    }


    /**
     *  Gets the <code> Agent </code> who created this entry. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.logEntry.getAgent());
    }


    /**
     *  Gets the log entry record corresponding to the given <code> LogEntry 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  logEntryRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(logEntryRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  logEntryRecordType the type of log entry record to retrieve 
     *  @return the log entry record 
     *  @throws org.osid.NullArgumentException <code> logEntryRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(logEntryRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.logging.records.LogEntryRecord getLogEntryRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.logEntry.getLogEntryRecord(logEntryRecordType));
    }
}

