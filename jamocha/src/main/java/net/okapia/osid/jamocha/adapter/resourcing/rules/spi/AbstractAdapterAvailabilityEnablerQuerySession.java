//
// AbstractQueryAvailabilityEnablerLookupSession.java
//
//    An AvailabilityEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AvailabilityEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterAvailabilityEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.rules.AvailabilityEnablerQuerySession {

    private final org.osid.resourcing.rules.AvailabilityEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAvailabilityEnablerQuerySession.
     *
     *  @param session the underlying availability enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAvailabilityEnablerQuerySession(org.osid.resourcing.rules.AvailabilityEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeFoundry</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeFoundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@codeFoundry</code> associated with this 
     *  session.
     *
     *  @return the {@codeFoundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@codeAvailabilityEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAvailabilityEnablers() {
        return (this.session.canSearchAvailabilityEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include availability enablers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this foundry only.
     */
    
    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    
      
    /**
     *  Gets an availability enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the availability enabler query 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerQuery getAvailabilityEnablerQuery() {
        return (this.session.getAvailabilityEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  availabilityEnablerQuery the availability enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code availabilityEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code availabilityEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByQuery(org.osid.resourcing.rules.AvailabilityEnablerQuery availabilityEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAvailabilityEnablersByQuery(availabilityEnablerQuery));
    }
}
