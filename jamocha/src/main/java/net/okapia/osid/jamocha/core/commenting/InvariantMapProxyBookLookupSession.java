//
// InvariantMapProxyBookLookupSession
//
//    Implements a Book lookup service backed by a fixed
//    collection of books. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.commenting;


/**
 *  Implements a Book lookup service backed by a fixed
 *  collection of books. The books are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyBookLookupSession
    extends net.okapia.osid.jamocha.core.commenting.spi.AbstractMapBookLookupSession
    implements org.osid.commenting.BookLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBookLookupSession} with no
     *  books.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyBookLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyBookLookupSession} with a
     *  single book.
     *
     *  @param book a single book
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code book} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyBookLookupSession(org.osid.commenting.Book book, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBook(book);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyBookLookupSession} using
     *  an array of books.
     *
     *  @param books an array of books
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code books} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyBookLookupSession(org.osid.commenting.Book[] books, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBooks(books);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyBookLookupSession} using a
     *  collection of books.
     *
     *  @param books a collection of books
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code books} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyBookLookupSession(java.util.Collection<? extends org.osid.commenting.Book> books,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBooks(books);
        return;
    }
}
