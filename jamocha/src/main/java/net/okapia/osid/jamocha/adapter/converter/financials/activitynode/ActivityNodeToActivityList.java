//
// ActivityNodeToActivityList.java
//
//     Implements an ActivityNodeToActivityList adapter.
//
//
// Tom Coppeto
// Okapia
// 21 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.financials.activitynode;

import org.osid.binding.java.annotation.OSID;


/**
 *  Implements an ActivityList adapter to convert activity nodes to
 *  activities.
 */

public final class ActivityNodeToActivityList
    extends net.okapia.osid.jamocha.adapter.converter.financials.activitynode.spi.AbstractActivityNodeToActivityList
    implements org.osid.financials.ActivityList {


    /**
     *  Constructs a new {@code ActivityNodeToActivityList}.
     *
     *  @param list the activity node list to convert
     *  @throws org.osid.NullArgumentException {@code list} is {@code
     *          null}
     */

    public ActivityNodeToActivityList(org.osid.financials.ActivityNodeList list) {
        super(list);
        return;
    }
}
