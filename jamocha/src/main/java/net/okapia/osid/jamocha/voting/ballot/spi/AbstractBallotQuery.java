//
// AbstractBallotQuery.java
//
//     A template for making a Ballot Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.ballot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for ballots.
 */

public abstract class AbstractBallotQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.voting.BallotQuery {

    private final java.util.Collection<org.osid.voting.records.BallotQueryRecord> records = new java.util.ArrayList<>();
    private final OsidTemporalQuery query = new OsidTemporalQuery();

    
    /**
     *  Matches ballots that allow vote modification. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRevote(boolean match) {
        return;
    }


    /**
     *  Clears the revote ballot terms. 
     */

    @OSID @Override
    public void clearRevoteTerms() {
        return;
    }


    /**
     *  Sets the race <code> Id </code> for this query. 
     *
     *  @param  raceId the race <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRaceId(org.osid.id.Id raceId, boolean match) {
        return;
    }


    /**
     *  Clears the race <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRaceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RaceQuery </code> is available. 
     *
     *  @return <code> true </code> if a race query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the race query 
     *  @throws org.osid.UnimplementedException <code> supportsRaceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.RaceQuery getRaceQuery() {
        throw new org.osid.UnimplementedException("supportsRaceQuery() is false");
    }


    /**
     *  Matches ballots with any race. 
     *
     *  @param  match <code> true </code> to match any race, <code> false 
     *          </code> to match ballots with no races 
     */

    @OSID @Override
    public void matchAnyRace(boolean match) {
        return;
    }


    /**
     *  Clears the race terms. 
     */

    @OSID @Override
    public void clearRaceTerms() {
        return;
    }


    /**
     *  Sets the polls <code> Id </code> for this query. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        return;
    }


    /**
     *  Clears the polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        return;
    }


    /**
     *  Match effective objects where the current date falls within the start
     *  and end dates inclusive.
     *
     *  @param  match <code> true </code> to match any effective, <code> false
     *          </code> to match ineffective
     */

    @OSID @Override
    public void matchEffective(boolean match) {
        this.query.matchEffective(match);
        return;
    }


    /**
     *  Clears the effective query terms.
     */

    @OSID @Override
    public void clearEffectiveTerms() {
        this.query.clearEffectiveTerms();
        return;
    }


    /**
     *  Matches temporals whose start date falls in between the given dates
     *  inclusive.
     *
     *  @param  start start of date range
     *  @param  end end of date range
     *  @param  match <code> true </code> if a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less
     *          than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> start </code> or <code>
     *          end </code> is <code> null </code>
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start,
                               org.osid.calendaring.DateTime end,
                               boolean match) {
        this.query.matchStartDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any start date set.
     *
     *  @param  match <code> true </code> to match any start date, <code>
     *          false </code> to match no start date
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        this.query.matchAnyStartDate(match);
        return;
    }


    /**
     *  Clears the start date query terms.
     */

    @OSID @Override
    public void clearStartDateTerms() {
        this.query.clearStartDateTerms();
        return;
    }


    /**
     *  Matches temporals whose effective end date falls in between the given
     *  dates inclusive.
     *
     *  @param  start start of date range
     *  @param  end end of date range
     *  @param  match <code> true </code> if a positive match, <code> false
     *          </code> for negative match
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less
     *          than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> start </code> or <code>
     *          end </code> is <code> null </code>
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start,
                             org.osid.calendaring.DateTime end,
                             boolean match) {

        this.query.matchEndDate(start, end, match);
        return;
    }

    
    /**
     *  Matches temporals with any end date set.
     *
     *  @param match <code> true </code> to match any end date, <code>
     *         false </code> to match no start date
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        this.query.matchAnyEndDate(match);
        return;
    }


    /**
     *  Clears the end date query terms.
     */

    @OSID @Override
    public void clearEndDateTerms() {
        this.query.clearEndDateTerms();
        return;
    }


    /**
     *  Matches temporals where the given date is included within the
     *  start and end dates inclusive.
     *
     *  @param  from start of date range
     *  @param  to start of date range
     *  @param  match <code> true </code> if a positive match, <code> false
     *          </code> for a negative match
     *  @throws org.osid.InvalidArgumentException
     *          <code>from</code> is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code>
     *          or <code>to</code> is <code>null</code>
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from,
                          org.osid.calendaring.DateTime to,
                          boolean match) {
        this.query.matchDate(from, to, match);
        return;

    }


    /**
     *  Clears the date query terms.
     */

    @OSID @Override
    public void clearDateTerms() {
        this.query.clearDateTerms();
        return;
    }

    
    /**
     *  Gets the record corresponding to the given ballot query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a ballot implementing the requested record.
     *
     *  @param ballotRecordType a ballot record type
     *  @return the ballot query record
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.BallotQueryRecord getBallotQueryRecord(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.BallotQueryRecord record : this.records) {
            if (record.implementsRecordType(ballotRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotRecordType + " is not supported");
    }


    /**
     *  Adds a record to this ballot query. 
     *
     *  @param ballotQueryRecord ballot query record
     *  @param ballotRecordType ballot record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBallotQueryRecord(org.osid.voting.records.BallotQueryRecord ballotQueryRecord, 
                                        org.osid.type.Type ballotRecordType) {

        addRecordType(ballotRecordType);
        nullarg(ballotQueryRecord, "ballot query record");
        this.records.add(ballotQueryRecord);        
        return;
    }


    protected class OsidTemporalQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidTemporalQuery
        implements org.osid.OsidTemporalQuery {
    }
}
