//
// AbstractAdapterRecurringEventEnablerLookupSession.java
//
//    A RecurringEventEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A RecurringEventEnabler lookup session adapter.
 */

public abstract class AbstractAdapterRecurringEventEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.rules.RecurringEventEnablerLookupSession {

    private final org.osid.calendaring.rules.RecurringEventEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterRecurringEventEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRecurringEventEnablerLookupSession(org.osid.calendaring.rules.RecurringEventEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code RecurringEventEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupRecurringEventEnablers() {
        return (this.session.canLookupRecurringEventEnablers());
    }


    /**
     *  A complete view of the {@code RecurringEventEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRecurringEventEnablerView() {
        this.session.useComparativeRecurringEventEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code RecurringEventEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRecurringEventEnablerView() {
        this.session.usePlenaryRecurringEventEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include recurring event enablers in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    

    /**
     *  Only active recurring event enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRecurringEventEnablerView() {
        this.session.useActiveRecurringEventEnablerView();
        return;
    }


    /**
     *  Active and inactive recurring event enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRecurringEventEnablerView() {
        this.session.useAnyStatusRecurringEventEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code RecurringEventEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code RecurringEventEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code RecurringEventEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param recurringEventEnablerId {@code Id} of the {@code RecurringEventEnabler}
     *  @return the recurring event enabler
     *  @throws org.osid.NotFoundException {@code recurringEventEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code recurringEventEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnabler getRecurringEventEnabler(org.osid.id.Id recurringEventEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventEnabler(recurringEventEnablerId));
    }


    /**
     *  Gets a {@code RecurringEventEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  recurringEventEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code RecurringEventEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code RecurringEventEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code recurringEventEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByIds(org.osid.id.IdList recurringEventEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventEnablersByIds(recurringEventEnablerIds));
    }


    /**
     *  Gets a {@code RecurringEventEnablerList} corresponding to the given
     *  recurring event enabler genus {@code Type} which does not include
     *  recurring event enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerGenusType a recurringEventEnabler genus type 
     *  @return the returned {@code RecurringEventEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code recurringEventEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByGenusType(org.osid.type.Type recurringEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventEnablersByGenusType(recurringEventEnablerGenusType));
    }


    /**
     *  Gets a {@code RecurringEventEnablerList} corresponding to the given
     *  recurring event enabler genus {@code Type} and include any additional
     *  recurring event enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerGenusType a recurringEventEnabler genus type 
     *  @return the returned {@code RecurringEventEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code recurringEventEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByParentGenusType(org.osid.type.Type recurringEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventEnablersByParentGenusType(recurringEventEnablerGenusType));
    }


    /**
     *  Gets a {@code RecurringEventEnablerList} containing the given
     *  recurring event enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  recurringEventEnablerRecordType a recurringEventEnabler record type 
     *  @return the returned {@code RecurringEventEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code recurringEventEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersByRecordType(org.osid.type.Type recurringEventEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventEnablersByRecordType(recurringEventEnablerRecordType));
    }


    /**
     *  Gets a {@code RecurringEventEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible
     *  through this session.
     *  
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code RecurringEventEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code RecurringEventEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible
     *  through this session.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code RecurringEventEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getRecurringEventEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code RecurringEventEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  recurring event enablers or an error results. Otherwise, the returned list
     *  may contain only those recurring event enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, recurring event enablers are returned that are currently
     *  active. In any status mode, active and inactive recurring event enablers
     *  are returned.
     *
     *  @return a list of {@code RecurringEventEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getRecurringEventEnablers());
    }
}
