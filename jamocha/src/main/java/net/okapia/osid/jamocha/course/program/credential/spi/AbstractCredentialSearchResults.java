//
// AbstractCredentialSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.credential.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCredentialSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.course.program.CredentialSearchResults {

    private org.osid.course.program.CredentialList credentials;
    private final org.osid.course.program.CredentialQueryInspector inspector;
    private final java.util.Collection<org.osid.course.program.records.CredentialSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCredentialSearchResults.
     *
     *  @param credentials the result set
     *  @param credentialQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>credentials</code>
     *          or <code>credentialQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCredentialSearchResults(org.osid.course.program.CredentialList credentials,
                                            org.osid.course.program.CredentialQueryInspector credentialQueryInspector) {
        nullarg(credentials, "credentials");
        nullarg(credentialQueryInspector, "credential query inspectpr");

        this.credentials = credentials;
        this.inspector = credentialQueryInspector;

        return;
    }


    /**
     *  Gets the credential list resulting from a search.
     *
     *  @return a credential list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.course.program.CredentialList getCredentials() {
        if (this.credentials == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.course.program.CredentialList credentials = this.credentials;
        this.credentials = null;
	return (credentials);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.course.program.CredentialQueryInspector getCredentialQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  credential search record <code> Type. </code> This method must
     *  be used to retrieve a credential implementing the requested
     *  record.
     *
     *  @param credentialSearchRecordType a credential search 
     *         record type 
     *  @return the credential search
     *  @throws org.osid.NullArgumentException
     *          <code>credentialSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(credentialSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.CredentialSearchResultsRecord getCredentialSearchResultsRecord(org.osid.type.Type credentialSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.course.program.records.CredentialSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(credentialSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(credentialSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record credential search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCredentialRecord(org.osid.course.program.records.CredentialSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "credential record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
