//
// AbstractAdapterGradebookColumnLookupSession.java
//
//    A GradebookColumn lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A GradebookColumn lookup session adapter.
 */

public abstract class AbstractAdapterGradebookColumnLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.grading.GradebookColumnLookupSession {

    private final org.osid.grading.GradebookColumnLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterGradebookColumnLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterGradebookColumnLookupSession(org.osid.grading.GradebookColumnLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Gradebook/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Gradebook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.session.getGradebookId());
    }


    /**
     *  Gets the {@code Gradebook} associated with this session.
     *
     *  @return the {@code Gradebook} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGradebook());
    }


    /**
     *  Tests if this user can perform {@code GradebookColumn} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupGradebookColumns() {
        return (this.session.canLookupGradebookColumns());
    }


    /**
     *  A complete view of the {@code GradebookColumn} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeGradebookColumnView() {
        this.session.useComparativeGradebookColumnView();
        return;
    }


    /**
     *  A complete view of the {@code GradebookColumn} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryGradebookColumnView() {
        this.session.usePlenaryGradebookColumnView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include gradebook columns in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.session.useFederatedGradebookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this gradebook only.
     */

    @OSID @Override
    public void useIsolatedGradebookView() {
        this.session.useIsolatedGradebookView();
        return;
    }
    
     
    /**
     *  Gets the {@code GradebookColumn} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code GradebookColumn} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code GradebookColumn} and
     *  retained for compatibility.
     *
     *  @param gradebookColumnId {@code Id} of the {@code GradebookColumn}
     *  @return the gradebook column
     *  @throws org.osid.NotFoundException {@code gradebookColumnId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code gradebookColumnId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumn getGradebookColumn(org.osid.id.Id gradebookColumnId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumn(gradebookColumnId));
    }


    /**
     *  Gets a {@code GradebookColumnList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  gradebookColumns specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code GradebookColumns} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  gradebookColumnIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code GradebookColumn} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByIds(org.osid.id.IdList gradebookColumnIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnsByIds(gradebookColumnIds));
    }


    /**
     *  Gets a {@code GradebookColumnList} corresponding to the given
     *  gradebook column genus {@code Type} which does not include
     *  gradebook columns of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook columns or an error results. Otherwise, the returned list
     *  may contain only those gradebook columns that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradebookColumnGenusType a gradebookColumn genus type 
     *  @return the returned {@code GradebookColumn} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByGenusType(org.osid.type.Type gradebookColumnGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnsByGenusType(gradebookColumnGenusType));
    }


    /**
     *  Gets a {@code GradebookColumnList} corresponding to the given
     *  gradebook column genus {@code Type} and include any additional
     *  gradebook columns with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook columns or an error results. Otherwise, the returned list
     *  may contain only those gradebook columns that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradebookColumnGenusType a gradebookColumn genus type 
     *  @return the returned {@code GradebookColumn} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByParentGenusType(org.osid.type.Type gradebookColumnGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnsByParentGenusType(gradebookColumnGenusType));
    }


    /**
     *  Gets a {@code GradebookColumnList} containing the given
     *  gradebook column record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  gradebook columns or an error results. Otherwise, the returned list
     *  may contain only those gradebook columns that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  gradebookColumnRecordType a gradebookColumn record type 
     *  @return the returned {@code GradebookColumn} list
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookColumnRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumnsByRecordType(org.osid.type.Type gradebookColumnRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumnsByRecordType(gradebookColumnRecordType));
    }


    /**
     *  Gets all {@code GradebookColumns}. 
     *
     *  In plenary mode, the returned list contains all known
     *  gradebook columns or an error results. Otherwise, the returned list
     *  may contain only those gradebook columns that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code GradebookColumns} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getGradebookColumns()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getGradebookColumns());
    }

    
    /**
     *  Tests if a summary entry is available. 
     *
     *  @return {@code true} if a summary entry is available, {@code 
     *          false} otherwise 
     */

    @OSID @Override
    public boolean supportsSummary() {
        return (this.session.supportsSummary());
    }


    /**
     *  Gets the {@code GradebookColumnSummary} for summary results. 
     *
     *  @param  gradebookColumnId {@code Id} of the {@code 
     *          GradebookColumn} 
     *  @return the gradebook column summary 
     *  @throws org.osid.NotFoundException {@code gradebookColumnId} is 
     *          not found 
     *  @throws org.osid.NullArgumentException {@code
     *         gradebookColumnId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnimplementedException {@code hasSummary()} is 
     *          {@code false} 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnSummary getGradebookColumnSummary(org.osid.id.Id gradebookColumnId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getGradebookColumnSummary(gradebookColumnId));
    }

}
