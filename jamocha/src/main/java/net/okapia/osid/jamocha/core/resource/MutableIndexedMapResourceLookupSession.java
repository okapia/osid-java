//
// MutableIndexedMapResourceLookupSession
//
//    Implements a Resource lookup service backed by a collection of
//    resources indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource;


/**
 *  Implements a Resource lookup service backed by a collection of
 *  resources. The resources are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some resources may be compatible
 *  with more types than are indicated through these resource
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of resources can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapResourceLookupSession
    extends net.okapia.osid.jamocha.core.resource.spi.AbstractIndexedMapResourceLookupSession
    implements org.osid.resource.ResourceLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapResourceLookupSession} with no resources.
     *
     *  @param bin the bin
     *  @throws org.osid.NullArgumentException {@code bin}
     *          is {@code null}
     */

      public MutableIndexedMapResourceLookupSession(org.osid.resource.Bin bin) {
        setBin(bin);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapResourceLookupSession} with a
     *  single resource.
     *  
     *  @param bin the bin
     *  @param  resource a single resource
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resource} is {@code null}
     */

    public MutableIndexedMapResourceLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.Resource resource) {
        this(bin);
        putResource(resource);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapResourceLookupSession} using an
     *  array of resources.
     *
     *  @param bin the bin
     *  @param  resources an array of resources
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resources} is {@code null}
     */

    public MutableIndexedMapResourceLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.resource.Resource[] resources) {
        this(bin);
        putResources(resources);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapResourceLookupSession} using a
     *  collection of resources.
     *
     *  @param bin the bin
     *  @param  resources a collection of resources
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code resources} is {@code null}
     */

    public MutableIndexedMapResourceLookupSession(org.osid.resource.Bin bin,
                                                  java.util.Collection<? extends org.osid.resource.Resource> resources) {

        this(bin);
        putResources(resources);
        return;
    }
    

    /**
     *  Makes a {@code Resource} available in this session.
     *
     *  @param  resource a resource
     *  @throws org.osid.NullArgumentException {@code resource{@code  is
     *          {@code null}
     */

    @Override
    public void putResource(org.osid.resource.Resource resource) {
        super.putResource(resource);
        return;
    }


    /**
     *  Makes an array of resources available in this session.
     *
     *  @param  resources an array of resources
     *  @throws org.osid.NullArgumentException {@code resources{@code 
     *          is {@code null}
     */

    @Override
    public void putResources(org.osid.resource.Resource[] resources) {
        super.putResources(resources);
        return;
    }


    /**
     *  Makes collection of resources available in this session.
     *
     *  @param  resources a collection of resources
     *  @throws org.osid.NullArgumentException {@code resource{@code  is
     *          {@code null}
     */

    @Override
    public void putResources(java.util.Collection<? extends org.osid.resource.Resource> resources) {
        super.putResources(resources);
        return;
    }


    /**
     *  Removes a Resource from this session.
     *
     *  @param resourceId the {@code Id} of the resource
     *  @throws org.osid.NullArgumentException {@code resourceId{@code  is
     *          {@code null}
     */

    @Override
    public void removeResource(org.osid.id.Id resourceId) {
        super.removeResource(resourceId);
        return;
    }    
}
