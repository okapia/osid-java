//
// UnknownOrder.java
//
//     Defines an unknown Order.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.ordering.order;


/**
 *  Defines an unknown <code>Order</code>.
 */

public final class UnknownOrder
    extends net.okapia.osid.jamocha.nil.ordering.order.spi.AbstractUnknownOrder
    implements org.osid.ordering.Order {


    /**
     *  Constructs a new <code>UnknownOrder</code>.
     */

    public UnknownOrder() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownOrder</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownOrder(boolean optional) {
        super(optional);
        addOrderRecord(new OrderRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Order.
     *
     *  @return an unknown Order
     */

    public static org.osid.ordering.Order create() {
        return (net.okapia.osid.jamocha.builder.validator.ordering.order.OrderValidator.validateOrder(new UnknownOrder()));
    }


    public class OrderRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.ordering.records.OrderRecord {

        
        protected OrderRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
