//
// AbstractGradingBatchManager.java
//
//     An adapter for a GradingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a GradingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterGradingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.grading.batch.GradingBatchManager>
    implements org.osid.grading.batch.GradingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterGradingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterGradingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterGradingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterGradingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of grade systems is available. 
     *
     *  @return <code> true </code> if a grade system bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeSystemBatchAdmin() {
        return (getAdapteeManager().supportsGradeSystemBatchAdmin());
    }


    /**
     *  Tests if bulk administration of grade entries is available. 
     *
     *  @return <code> true </code> if a grade entry bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradeEntryBatchAdmin() {
        return (getAdapteeManager().supportsGradeEntryBatchAdmin());
    }


    /**
     *  Tests if bulk administration of gradebook columns is available. 
     *
     *  @return <code> true </code> if a gradebook columnbulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookColumnBatchAdmin() {
        return (getAdapteeManager().supportsGradebookColumnBatchAdmin());
    }


    /**
     *  Tests if bulk administration of gradebooks is available. 
     *
     *  @return <code> true </code> if a gradebook bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradebookBatchAdmin() {
        return (getAdapteeManager().supportsGradebookBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  system administration service. 
     *
     *  @return a <code> GradeSystemBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeSystemBatchAdminSession getGradeSystemBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  system administration service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the <code> Gradebook 
     *          </code> 
     *  @return a <code> GradeSystemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Gradebook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeSystemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeSystemBatchAdminSession getGradeSystemBatchAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeSystemBatchAdminSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  entry administration service. 
     *
     *  @return a <code> GradeEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeEntryBatchAdminSession getGradeEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk grade 
     *  entry administration service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the <code> Gradebook 
     *          </code> 
     *  @return a <code> GradeEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Gradebook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradeEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradeEntryBatchAdminSession getGradeEntryBatchAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradeEntryBatchAdminSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk gradebook 
     *  columnadministration service. 
     *
     *  @return a <code> GradebookColumnBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradebookColumnBatchAdminSession getGradebookColumnBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk gradebook 
     *  columnadministration service for the given gradebook. 
     *
     *  @param  gradebookId the <code> Id </code> of the <code> Gradebook 
     *          </code> 
     *  @return a <code> GradebookColumnBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Gradebook </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> gradebookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookColumnBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradebookColumnBatchAdminSession getGradebookColumnBatchAdminSessionForGradebook(org.osid.id.Id gradebookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookColumnBatchAdminSessionForGradebook(gradebookId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk gradebook 
     *  administration service. 
     *
     *  @return a <code> GradebookBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradebookBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.batch.GradebookBatchAdminSession getGradebookBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getGradebookBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
