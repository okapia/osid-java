//
// AbstractIndexedMapStoreLookupSession.java
//
//    A simple framework for providing a Store lookup service
//    backed by a fixed collection of stores with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Store lookup service backed by a
 *  fixed collection of stores. The stores are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some stores may be compatible
 *  with more types than are indicated through these store
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Stores</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapStoreLookupSession
    extends AbstractMapStoreLookupSession
    implements org.osid.ordering.StoreLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.ordering.Store> storesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.Store>());
    private final MultiMap<org.osid.type.Type, org.osid.ordering.Store> storesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.ordering.Store>());


    /**
     *  Makes a <code>Store</code> available in this session.
     *
     *  @param  store a store
     *  @throws org.osid.NullArgumentException <code>store<code> is
     *          <code>null</code>
     */

    @Override
    protected void putStore(org.osid.ordering.Store store) {
        super.putStore(store);

        this.storesByGenus.put(store.getGenusType(), store);
        
        try (org.osid.type.TypeList types = store.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.storesByRecord.put(types.getNextType(), store);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a store from this session.
     *
     *  @param storeId the <code>Id</code> of the store
     *  @throws org.osid.NullArgumentException <code>storeId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeStore(org.osid.id.Id storeId) {
        org.osid.ordering.Store store;
        try {
            store = getStore(storeId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.storesByGenus.remove(store.getGenusType());

        try (org.osid.type.TypeList types = store.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.storesByRecord.remove(types.getNextType(), store);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeStore(storeId);
        return;
    }


    /**
     *  Gets a <code>StoreList</code> corresponding to the given
     *  store genus <code>Type</code> which does not include
     *  stores of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known stores or an error results. Otherwise,
     *  the returned list may contain only those stores that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  storeGenusType a store genus type 
     *  @return the returned <code>Store</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>storeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByGenusType(org.osid.type.Type storeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.store.ArrayStoreList(this.storesByGenus.get(storeGenusType)));
    }


    /**
     *  Gets a <code>StoreList</code> containing the given
     *  store record <code>Type</code>. In plenary mode, the
     *  returned list contains all known stores or an error
     *  results. Otherwise, the returned list may contain only those
     *  stores that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  storeRecordType a store record type 
     *  @return the returned <code>store</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>storeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.StoreList getStoresByRecordType(org.osid.type.Type storeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.store.ArrayStoreList(this.storesByRecord.get(storeRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.storesByGenus.clear();
        this.storesByRecord.clear();

        super.close();

        return;
    }
}
