//
// AbstractBloggingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractBloggingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.blogging.BloggingManager,
               org.osid.blogging.BloggingProxyManager {

    private final Types entryRecordTypes                   = new TypeRefSet();
    private final Types entrySearchRecordTypes             = new TypeRefSet();

    private final Types blogRecordTypes                    = new TypeRefSet();
    private final Types blogSearchRecordTypes              = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractBloggingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractBloggingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if sending entries is supported. 
     *
     *  @return <code> true </code> if entry sending is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogging() {
        return (false);
    }


    /**
     *  Tests if entry lookup is supported. 
     *
     *  @return <code> true </code> if entry lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryLookup() {
        return (false);
    }


    /**
     *  Tests if entry query is supported. 
     *
     *  @return <code> true </code> if entry query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Tests if entry search is supported. 
     *
     *  @return <code> true </code> if entry search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySearch() {
        return (false);
    }


    /**
     *  Tests if creating, updating and deleting entries is supported. 
     *
     *  @return <code> true </code> if entry administration is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if entry notification is supported. Entries may be sent when 
     *  entries are created, modified, or deleted. 
     *
     *  @return <code> true </code> if entry notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of entry and blogs is supported. 
     *
     *  @return <code> true </code> if entry blog mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBlog() {
        return (false);
    }


    /**
     *  Tests if managing mappings of entries and blogs is supported. 
     *
     *  @return <code> true </code> if entry blog assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryBlogAssignment() {
        return (false);
    }


    /**
     *  Tests if entry smart blogging is available. 
     *
     *  @return <code> true </code> if entry smart blog is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntrySmartBlog() {
        return (false);
    }


    /**
     *  Tests if blog lookup is supported. 
     *
     *  @return <code> true </code> if blog lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogLookup() {
        return (false);
    }


    /**
     *  Tests if blog query is supported. 
     *
     *  @return <code> true </code> if blog query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogQuery() {
        return (false);
    }


    /**
     *  Tests if blog search is supported. 
     *
     *  @return <code> true </code> if blog search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogSearch() {
        return (false);
    }


    /**
     *  Tests if blog administration is supported. 
     *
     *  @return <code> true </code> if blog administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogAdmin() {
        return (false);
    }


    /**
     *  Tests if blog notification is supported. Entries may be sent when 
     *  <code> Blog </code> objects are created, deleted or updated. 
     *  Notifications for entries within blogs are sent via the entry 
     *  notification session. 
     *
     *  @return <code> true </code> if blog notification is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogNotification() {
        return (false);
    }


    /**
     *  Tests if a blog hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a blog hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogHierarchy() {
        return (false);
    }


    /**
     *  Tests if a blog hierarchy design is supported. 
     *
     *  @return <code> true </code> if a blog hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlogHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a blogging batch service is supported. 
     *
     *  @return <code> true </code> if a blogging batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBloggingBatch() {
        return (false);
    }


    /**
     *  Gets all the entry record types supported. 
     *
     *  @return the list of supported entry record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entryRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given entry record type is supported. 
     *
     *  @param  entryRecordType the entry type 
     *  @return <code> true </code> if the entry record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entryRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntryRecordType(org.osid.type.Type entryRecordType) {
        return (this.entryRecordTypes.contains(entryRecordType));
    }


    /**
     *  Adds support for an entry record type.
     *
     *  @param entryRecordType an entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>entryRecordType</code> is <code>null</code>
     */

    protected void addEntryRecordType(org.osid.type.Type entryRecordType) {
        this.entryRecordTypes.add(entryRecordType);
        return;
    }


    /**
     *  Removes support for an entry record type.
     *
     *  @param entryRecordType an entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>entryRecordType</code> is <code>null</code>
     */

    protected void removeEntryRecordType(org.osid.type.Type entryRecordType) {
        this.entryRecordTypes.remove(entryRecordType);
        return;
    }


    /**
     *  Gets all the entry search record types supported. 
     *
     *  @return the list of supported entry search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.entrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given entry search type is supported. 
     *
     *  @param  entrySearchRecordType the entry search type 
     *  @return <code> true </code> if the entry search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> entrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        return (this.entrySearchRecordTypes.contains(entrySearchRecordType));
    }


    /**
     *  Adds support for an entry search record type.
     *
     *  @param entrySearchRecordType an entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>entrySearchRecordType</code> is <code>null</code>
     */

    protected void addEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        this.entrySearchRecordTypes.add(entrySearchRecordType);
        return;
    }


    /**
     *  Removes support for an entry search record type.
     *
     *  @param entrySearchRecordType an entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>entrySearchRecordType</code> is <code>null</code>
     */

    protected void removeEntrySearchRecordType(org.osid.type.Type entrySearchRecordType) {
        this.entrySearchRecordTypes.remove(entrySearchRecordType);
        return;
    }


    /**
     *  Gets all the blog record types supported. 
     *
     *  @return the list of supported blog record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBlogRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.blogRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given blog record type is supported. 
     *
     *  @param  blogRecordType the blog record type 
     *  @return <code> true </code> if the blog record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> blogRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBlogRecordType(org.osid.type.Type blogRecordType) {
        return (this.blogRecordTypes.contains(blogRecordType));
    }


    /**
     *  Adds support for a blog record type.
     *
     *  @param blogRecordType a blog record type
     *  @throws org.osid.NullArgumentException
     *  <code>blogRecordType</code> is <code>null</code>
     */

    protected void addBlogRecordType(org.osid.type.Type blogRecordType) {
        this.blogRecordTypes.add(blogRecordType);
        return;
    }


    /**
     *  Removes support for a blog record type.
     *
     *  @param blogRecordType a blog record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>blogRecordType</code> is <code>null</code>
     */

    protected void removeBlogRecordType(org.osid.type.Type blogRecordType) {
        this.blogRecordTypes.remove(blogRecordType);
        return;
    }


    /**
     *  Gets all the blog search record types supported. 
     *
     *  @return the list of supported blog search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBlogSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.blogSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given blog search record type is supported. 
     *
     *  @param  blogSearchRecordType the blog search record type 
     *  @return <code> true </code> if the blog search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> blogSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBlogSearchRecordType(org.osid.type.Type blogSearchRecordType) {
        return (this.blogSearchRecordTypes.contains(blogSearchRecordType));
    }


    /**
     *  Adds support for a blog search record type.
     *
     *  @param blogSearchRecordType a blog search record type
     *  @throws org.osid.NullArgumentException
     *  <code>blogSearchRecordType</code> is <code>null</code>
     */

    protected void addBlogSearchRecordType(org.osid.type.Type blogSearchRecordType) {
        this.blogSearchRecordTypes.add(blogSearchRecordType);
        return;
    }


    /**
     *  Removes support for a blog search record type.
     *
     *  @param blogSearchRecordType a blog search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>blogSearchRecordType</code> is <code>null</code>
     */

    protected void removeBlogSearchRecordType(org.osid.type.Type blogSearchRecordType) {
        this.blogSearchRecordTypes.remove(blogSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service. 
     *
     *  @return <code> an EntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryLookupSession getEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> an EntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryLookupSession getEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @return <code> an EntryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryLookupSession getEntryLookupSessionForBlog(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryLookupSessionForBlog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the entry lookup 
     *  service for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEntryLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryLookupSession getEntryLookupSessionForBlog(org.osid.id.Id blogId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryLookupSessionForBlog not implemented");
    }


    /**
     *  Gets an entry query session. 
     *
     *  @return <code> an EntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryQuerySession getEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryQuerySession not implemented");
    }


    /**
     *  Gets an entry query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> an EntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryQuerySession getEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryQuerySession not implemented");
    }


    /**
     *  Gets an entry query session for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @return <code> an EntryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryQuerySession getEntryQuerySessionForBlog(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryQuerySessionForBlog not implemented");
    }


    /**
     *  Gets an entry query session for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryQuerySession getEntryQuerySessionForBlog(org.osid.id.Id blogId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryQuerySessionForBlog not implemented");
    }


    /**
     *  Gets an entry search session. 
     *
     *  @return <code> an EntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntrySearchSession getEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntrySearchSession not implemented");
    }


    /**
     *  Gets an entry search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> an EntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntrySearchSession getEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntrySearchSession not implemented");
    }


    /**
     *  Gets an entry search session for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @return <code> an EntrySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntrySearchSession getEntrySearchSessionForBlog(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntrySearchSessionForBlog not implemented");
    }


    /**
     *  Gets an entry search session for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntrySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsEntrySearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntrySearchSession getEntrySearchSessionForBlog(org.osid.id.Id blogId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntrySearchSessionForBlog not implemented");
    }


    /**
     *  Gets an entry administration session for creating, updating and 
     *  deleting entries. 
     *
     *  @return <code> an EntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryAdminSession getEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryAdminSession not implemented");
    }


    /**
     *  Gets an entry administration session for creating, updating and 
     *  deleting entries. 
     *
     *  @param  proxy a proxy 
     *  @return <code> an EntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryAdminSession getEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryAdminSession not implemented");
    }


    /**
     *  Gets an entry administration session for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @return <code> an EntryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryAdminSession getEntryAdminSessionForBlog(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryAdminSessionForBlog not implemented");
    }


    /**
     *  Gets an entry administration session for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryAdminSession getEntryAdminSessionForBlog(org.osid.id.Id blogId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryAdminSessionForBlog not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to entry 
     *  changes. 
     *
     *  @param  entryReceiver the notification callback 
     *  @return <code> an EntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryNotificationSession getEntryNotificationSession(org.osid.blogging.EntryReceiver entryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryNotificationSession not implemented");
    }


    /**
     *  Gets the entry notification session for the given blog. 
     *
     *  @param  entryReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> an EntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryNotificationSession getEntryNotificationSession(org.osid.blogging.EntryReceiver entryReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryNotificationSession not implemented");
    }


    /**
     *  Gets the entry notification session for the given blog. 
     *
     *  @param  entryReceiver the notification callback 
     *  @param  blogId the <code> Id </code> of the blog 
     *  @return <code> an EntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> entryReceiver </code> or 
     *          <code> blogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryNotificationSession getEntryNotificationSessionForBlog(org.osid.blogging.EntryReceiver entryReceiver, 
                                                                                         org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryNotificationSessionForBlog not implemented");
    }


    /**
     *  Gets the entry notification session for the given blog. 
     *
     *  @param  entryReceiver notification callback 
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return <code> an EntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> not found 
     *  @throws org.osid.NullArgumentException <code> entryReceiver, blogId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryNotificationSession getEntryNotificationSessionForBlog(org.osid.blogging.EntryReceiver entryReceiver, 
                                                                                         org.osid.id.Id blogId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryNotificationSessionForBlog not implemented");
    }


    /**
     *  Gets the session for retrieving entry to blog mappings. 
     *
     *  @return an <code> EntryBlogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryBlog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryBlogSession getEntryBlogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryBlogSession not implemented");
    }


    /**
     *  Gets the session for retrieving entry to blog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryBlogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEntryBlog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryBlogSession getEntryBlogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryBlogSession not implemented");
    }


    /**
     *  Gets the session for assigning entry to blog mappings. 
     *
     *  @return a <code> EntryBlogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBlogAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryBlogAssignmentSession getEntryBlogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntryBlogAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning entry to blog mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EntryBlogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntryBlogAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryBlogAssignmentSession getEntryBlogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntryBlogAssignmentSession not implemented");
    }


    /**
     *  Gets the session for managing smart blogs for the given blog. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @return an <code> EntrySmartBlogSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> is not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntrySmartBlog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntrySmartBlogSession getEntrySmartBlogSession(org.osid.id.Id blogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getEntrySmartBlogSession not implemented");
    }


    /**
     *  Gets the session managing smart blogs. 
     *
     *  @param  blogId the <code> Id </code> of the blog 
     *  @param  proxy a proxy 
     *  @return a <code> EntrySmartBlogSession </code> 
     *  @throws org.osid.NotFoundException <code> blogId </code> is not found 
     *  @throws org.osid.NullArgumentException <code> blogId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEntrySmartBlog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntrySmartBlogSession getEntrySmartBlogSession(org.osid.id.Id blogId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getEntrySmartBlogSession not implemented");
    }


    /**
     *  Gets the blog lookup session. 
     *
     *  @return a <code> BlogLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogLookupSession getBlogLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getBlogLookupSession not implemented");
    }


    /**
     *  Gets the blog lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogLookupSession getBlogLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getBlogLookupSession not implemented");
    }


    /**
     *  Gets the blog query session. 
     *
     *  @return a <code> BlogQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogQuerySession getBlogQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getBlogQuerySession not implemented");
    }


    /**
     *  Gets the blog query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogQuerySession getBlogQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getBlogQuerySession not implemented");
    }


    /**
     *  Gets the blog search session. 
     *
     *  @return a <code> BlogSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogSearchSession getBlogSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getBlogSearchSession not implemented");
    }


    /**
     *  Gets the blog search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogSearchSession getBlogSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getBlogSearchSession not implemented");
    }


    /**
     *  Gets the blog administrative session for creating, updating and 
     *  deleteing blogs. 
     *
     *  @return a <code> BlogAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogAdminSession getBlogAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getBlogAdminSession not implemented");
    }


    /**
     *  Gets the blog administrative session for creating, updating and 
     *  deleteing blogs. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogAdminSession getBlogAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getBlogAdminSession not implemented");
    }


    /**
     *  Gets the notification session for subscriblogg to changes to a blog. 
     *
     *  @param  blogReceiver the notification callback 
     *  @return a <code> BlogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> blogReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlogNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogNotificationSession getBlogNotificationSession(org.osid.blogging.BlogReceiver blogReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getBlogNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for subscriblogg to changes to a blog. 
     *
     *  @param  blogReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BlogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> blogReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlogNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogNotificationSession getBlogNotificationSession(org.osid.blogging.BlogReceiver blogReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getBlogNotificationSession not implemented");
    }


    /**
     *  Gets the blog hierarchy traversal session. 
     *
     *  @return <code> a BlogHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogHierarchySession getBlogHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getBlogHierarchySession not implemented");
    }


    /**
     *  Gets the blog hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BlogHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlogHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogHierarchySession getBlogHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getBlogHierarchySession not implemented");
    }


    /**
     *  Gets the blog hierarchy design session. 
     *
     *  @return a <code> BlogHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlogHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogHierarchyDesignSession getBlogHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getBlogHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the blog hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlogHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlogHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogHierarchyDesignSession getBlogHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getBlogHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> BloggingBatchManager. </code> 
     *
     *  @return a <code> BloggingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBloggingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.BloggingBatchManager getBloggingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingManager.getBloggingBatchManager not implemented");
    }


    /**
     *  Gets a <code> BloggingBatchProxyManager. </code> 
     *
     *  @return a <code> BloggingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBloggingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.batch.BloggingBatchProxyManager getBloggingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.blogging.BloggingProxyManager.getBloggingBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.entryRecordTypes.clear();
        this.entryRecordTypes.clear();

        this.entrySearchRecordTypes.clear();
        this.entrySearchRecordTypes.clear();

        this.blogRecordTypes.clear();
        this.blogRecordTypes.clear();

        this.blogSearchRecordTypes.clear();
        this.blogSearchRecordTypes.clear();

        return;
    }
}
