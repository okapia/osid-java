//
// AbstractQueryItemLookupSession.java
//
//    An inline adapter that maps an ItemLookupSession to
//    an ItemQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ItemLookupSession to
 *  an ItemQuerySession.
 */

public abstract class AbstractQueryItemLookupSession
    extends net.okapia.osid.jamocha.inventory.spi.AbstractItemLookupSession
    implements org.osid.inventory.ItemLookupSession {

    private final org.osid.inventory.ItemQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryItemLookupSession.
     *
     *  @param querySession the underlying item query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryItemLookupSession(org.osid.inventory.ItemQuerySession querySession) {
        nullarg(querySession, "item query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Warehouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform <code>Item</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupItems() {
        return (this.session.canSearchItems());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include items in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the <code>Item</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Item</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Item</code> and
     *  retained for compatibility.
     *
     *  @param  itemId <code>Id</code> of the
     *          <code>Item</code>
     *  @return the item
     *  @throws org.osid.NotFoundException <code>itemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>itemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ItemQuery query = getQuery();
        query.matchId(itemId, true);
        org.osid.inventory.ItemList items = this.session.getItemsByQuery(query);
        if (items.hasNext()) {
            return (items.getNextItem());
        } 
        
        throw new org.osid.NotFoundException(itemId + " not found");
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  items specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Items</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  itemIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>itemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByIds(org.osid.id.IdList itemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ItemQuery query = getQuery();

        try (org.osid.id.IdList ids = itemIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getItemsByQuery(query));
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> which does not include
     *  items of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ItemQuery query = getQuery();
        query.matchGenusType(itemGenusType, true);
        return (this.session.getItemsByQuery(query));
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> and include any additional
     *  items with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByParentGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ItemQuery query = getQuery();
        query.matchParentGenusType(itemGenusType, true);
        return (this.session.getItemsByQuery(query));
    }


    /**
     *  Gets an <code>ItemList</code> containing the given
     *  item record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ItemQuery query = getQuery();
        query.matchRecordType(itemRecordType, true);
        return (this.session.getItemsByQuery(query));
    }

    
    /**
     *  Gets an <code> ItemList </code> for the given stock. In
     *  plenary mode, the returned list contains all known items or an
     *  error results.  Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @return the returned <code> Item </code> list 
     *  @throws org.osid.NullArgumentException <code> stockId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ItemQuery query = getQuery();
        query.matchStockId(stockId, true);
        return (this.session.getItemsByQuery(query));
    }


    /**
     *  Gets an <code> ItemList </code> for the given property tag. In
     *  plenary mode, the returned list contains all known items or an
     *  error results.  Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  property a property number 
     *  @return the returned <code> Item </code> list 
     *  @throws org.osid.NullArgumentException <code> property </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByPropertyTag(String property)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ItemQuery query = getQuery();
        query.matchPropertyTag(property, getStringMatchType(), true);
        return (this.session.getItemsByQuery(query));
    }


    /**
     *  Gets an <code> ItemList </code> for the given serial
     *  number. In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list may
     *  contain only those items that are accessible through this
     *  session.
     *
     *  @param  serialNumber a serial number 
     *  @return the returned <code> Item </code> list 
     *  @throws org.osid.NullArgumentException <code> serialNumber
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsBySerialNumber(String serialNumber)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ItemQuery query = getQuery();
        query.matchSerialNumber(serialNumber, getStringMatchType(), true);
        return (this.session.getItemsByQuery(query));
    }


    /**
     *  Gets an <code> ItemList </code> immediately contained within
     *  the given item. In plenary mode, the returned list contains
     *  all known items or an error results. Otherwise, the returned
     *  list may contain only those items that are accessible through
     *  this session.
     *
     *  @param  itemId an item <code> Id </code> 
     *  @return the returned <code> Item </code> list 
     *  @throws org.osid.NullArgumentException <code> itemId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemParts(org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.inventory.ItemQuery query = getQuery();
        query.matchItemId(itemId, true);
        return (this.session.getItemsByQuery(query));
    }


    /**
     *  Gets all <code>Items</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Items</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.inventory.ItemQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getItemsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.inventory.ItemQuery getQuery() {
        org.osid.inventory.ItemQuery query = this.session.getItemQuery();
        return (query);
    }


    /**
     *  Gets the string match type for use in string queries.
     *
     *  @return a type
     */

    protected org.osid.type.Type getStringMatchType() {
        return (net.okapia.osid.primordium.types.search.StringMatchTypes.EXACT.getType());
    }
}
