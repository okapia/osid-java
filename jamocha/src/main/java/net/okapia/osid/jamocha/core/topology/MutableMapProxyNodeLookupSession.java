//
// MutableMapProxyNodeLookupSession
//
//    Implements a Node lookup service backed by a collection of
//    nodes that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology;


/**
 *  Implements a Node lookup service backed by a collection of
 *  nodes. The nodes are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of nodes can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyNodeLookupSession
    extends net.okapia.osid.jamocha.core.topology.spi.AbstractMapNodeLookupSession
    implements org.osid.topology.NodeLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyNodeLookupSession}
     *  with no nodes.
     *
     *  @param graph the graph
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyNodeLookupSession(org.osid.topology.Graph graph,
                                                  org.osid.proxy.Proxy proxy) {
        setGraph(graph);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyNodeLookupSession} with a
     *  single node.
     *
     *  @param graph the graph
     *  @param node a node
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code node}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyNodeLookupSession(org.osid.topology.Graph graph,
                                                org.osid.topology.Node node, org.osid.proxy.Proxy proxy) {
        this(graph, proxy);
        putNode(node);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyNodeLookupSession} using an
     *  array of nodes.
     *
     *  @param graph the graph
     *  @param nodes an array of nodes
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code nodes}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyNodeLookupSession(org.osid.topology.Graph graph,
                                                org.osid.topology.Node[] nodes, org.osid.proxy.Proxy proxy) {
        this(graph, proxy);
        putNodes(nodes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyNodeLookupSession} using a
     *  collection of nodes.
     *
     *  @param graph the graph
     *  @param nodes a collection of nodes
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph},
     *          {@code nodes}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyNodeLookupSession(org.osid.topology.Graph graph,
                                                java.util.Collection<? extends org.osid.topology.Node> nodes,
                                                org.osid.proxy.Proxy proxy) {
   
        this(graph, proxy);
        setSessionProxy(proxy);
        putNodes(nodes);
        return;
    }

    
    /**
     *  Makes a {@code Node} available in this session.
     *
     *  @param node an node
     *  @throws org.osid.NullArgumentException {@code node{@code 
     *          is {@code null}
     */

    @Override
    public void putNode(org.osid.topology.Node node) {
        super.putNode(node);
        return;
    }


    /**
     *  Makes an array of nodes available in this session.
     *
     *  @param nodes an array of nodes
     *  @throws org.osid.NullArgumentException {@code nodes{@code 
     *          is {@code null}
     */

    @Override
    public void putNodes(org.osid.topology.Node[] nodes) {
        super.putNodes(nodes);
        return;
    }


    /**
     *  Makes collection of nodes available in this session.
     *
     *  @param nodes
     *  @throws org.osid.NullArgumentException {@code node{@code 
     *          is {@code null}
     */

    @Override
    public void putNodes(java.util.Collection<? extends org.osid.topology.Node> nodes) {
        super.putNodes(nodes);
        return;
    }


    /**
     *  Removes a Node from this session.
     *
     *  @param nodeId the {@code Id} of the node
     *  @throws org.osid.NullArgumentException {@code nodeId{@code  is
     *          {@code null}
     */

    @Override
    public void removeNode(org.osid.id.Id nodeId) {
        super.removeNode(nodeId);
        return;
    }    
}
