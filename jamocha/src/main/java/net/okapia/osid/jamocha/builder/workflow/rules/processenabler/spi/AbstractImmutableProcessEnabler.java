//
// AbstractImmutableProcessEnabler.java
//
//     Wraps a mutable ProcessEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.workflow.rules.processenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ProcessEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized ProcessEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying processEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableProcessEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.workflow.rules.ProcessEnabler {

    private final org.osid.workflow.rules.ProcessEnabler processEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableProcessEnabler</code>.
     *
     *  @param processEnabler the process enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>processEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProcessEnabler(org.osid.workflow.rules.ProcessEnabler processEnabler) {
        super(processEnabler);
        this.processEnabler = processEnabler;
        return;
    }


    /**
     *  Gets the process enabler record corresponding to the given <code> 
     *  ProcessEnabler </code> record <code> Type. </code> This method is used 
     *  to retrieve an object implementing the requested record. The <code> 
     *  processEnablerRecordType </code> may be the <code> Type </code> 
     *  returned in <code> getRecordTypes() </code> or any of its parents in a 
     *  <code> Type </code> hierarchy where <code> 
     *  hasRecordType(processEnablerRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  processEnablerRecordType the type of process enabler record to 
     *          retrieve 
     *  @return the process enabler record 
     *  @throws org.osid.NullArgumentException <code> processEnablerRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(processEnablerRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.records.ProcessEnablerRecord getProcessEnablerRecord(org.osid.type.Type processEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.processEnabler.getProcessEnablerRecord(processEnablerRecordType));
    }
}

