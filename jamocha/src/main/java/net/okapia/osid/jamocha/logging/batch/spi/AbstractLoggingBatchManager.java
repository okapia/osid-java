//
// AbstractLoggingBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.logging.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractLoggingBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.logging.batch.LoggingBatchManager,
               org.osid.logging.batch.LoggingBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractLoggingBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractLoggingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of log entries is available. 
     *
     *  @return <code> true </code> if a log entry bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk purging of log entries is available. 
     *
     *  @return <code> true </code> if a log entry purge service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryPurgeSession() {
        return (false);
    }


    /**
     *  Tests if bulk administration of logs is available. 
     *
     *  @return <code> true </code> if a log bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk log entry 
     *  administration service. 
     *
     *  @return a <code> LogEntryBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryBatchAdminSession getLogEntryBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchManager.getLogEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk log entry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryBatchAdminSession getLogEntryBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchProxyManager.getLogEntryBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk log entry 
     *  administration service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryBatchAdminSession getLogEntryBatchAdminSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchManager.getLogEntryBatchAdminSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk log entry 
     *  administration service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryBatchAdminSession getLogEntryBatchAdminSessionForLog(org.osid.id.Id logId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchProxyManager.getLogEntryBatchAdminSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  purge service. 
     *
     *  @return a <code> LogEntryPurgeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryPurge() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryPurgeSession getLogEntryPurgeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchManager.getLogEntryPurgeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  purge service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryPurgeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryPurge() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryPurgeSession getLogEntryPurgeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchProxyManager.getLogEntryPurgeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  purge service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryPurgeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryPurge() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryPurgeSession getLogEntryPurgeSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchManager.getLogEntryPurgeSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  purge service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryPurgeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryPurge() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogEntryPurgeSession getLogEntryPurgeSessionForLog(org.osid.id.Id logId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchProxyManager.getLogEntryPurgeSessionForLog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk log 
     *  administration service. 
     *
     *  @return a <code> LogBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogBatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogBatchAdminSession getLogBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchManager.getLogBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk log 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogBatchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LogBatchAdminSession getLogBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.logging.batch.LoggingBatchProxyManager.getLogBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
