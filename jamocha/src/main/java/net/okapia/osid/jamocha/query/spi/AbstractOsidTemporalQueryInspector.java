//
// AbstractOsidTemporalQueryInspector.java
//
//     Defines an OsidTemporalQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidQueryInspector to extend. 
 */

public abstract class AbstractOsidTemporalQueryInspector
    extends AbstractOsidQueryInspector
    implements org.osid.OsidTemporalQueryInspector {
    
    private final java.util.Collection<org.osid.search.terms.BooleanTerm> effectiveTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> startDateTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> endDateTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> dateTerms = new java.util.LinkedHashSet<>();


    /**
     *  Gets the effective query terms.
     *
     *  @return the effective terms
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getEffectiveTerms() {
        return (this.effectiveTerms.toArray(new org.osid.search.terms.BooleanTerm[this.effectiveTerms.size()]));
    }


    /**
     *  Adds an effective term.
     *
     *  @param term an effective term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEffectiveTerm(org.osid.search.terms.BooleanTerm term) {
        nullarg(term, "effective term");
        this.effectiveTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of effective terms.
     *
     *  @param terms a collection of effective terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is
     *          <code>null</code>
     */

    protected void addEffectiveTerms(java.util.Collection<org.osid.search.terms.BooleanTerm> terms) {
        nullarg(terms, "effective terms");
        this.effectiveTerms.addAll(terms);
        return;
    }


    /**
     *  Adds an effective term.
     *
     *  @param effective flag
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     */

    protected void addEffectiveTerm(boolean effective, boolean match) {
        this.effectiveTerms.add(new net.okapia.osid.primordium.terms.BooleanTerm(effective, match));
        return;
    }


    /**
     *  Gets the start date query terms.
     *
     *  @return the start date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getStartDateTerms() {
        return (this.startDateTerms.toArray(new org.osid.search.terms.DateTimeRangeTerm[this.startDateTerms.size()]));
    }

    
    /**
     *  Adds a start date term.
     *
     *  @param term a start date
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addStartDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
        nullarg(term, "start date term");
        this.startDateTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of start date terms.
     *
     *  @param terms a collection of start date terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addStartDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
        nullarg(terms, "start date terms");
        this.startDateTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a start date term.
     *
     *  @param start start of date range
     *  @param end end of date range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> or is <code>null</code>
     */

    protected void addStartDateTerm(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                    boolean match) {
        this.startDateTerms.add(new net.okapia.osid.primordium.terms.DateTimeRangeTerm(start, end, match));
        return;
    }


    /**
     *  Gets the end date query terms.
     *
     *  @return the end date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getEndDateTerms() {
        return (this.endDateTerms.toArray(new org.osid.search.terms.DateTimeRangeTerm[this.endDateTerms.size()]));
    }

    
    /**
     *  Adds an end date term.
     *
     *  @param term an end date
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addEndDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
        nullarg(term, "end date term");
        this.endDateTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of end date terms.
     *
     *  @param terms a collection of end date terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addEndDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
        nullarg(terms, "end date terms");
        this.endDateTerms.addAll(terms);
        return;
    }


    /**
     *  Adds an end date term.
     *
     *  @param start start of date range
     *  @param end end of date range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>start</code> or
     *          <code>end</code> or is <code>null</code>
     */

    protected void addEndDateTerm(org.osid.calendaring.DateTime start, org.osid.calendaring.DateTime end, 
                                    boolean match) {
        this.endDateTerms.add(new net.okapia.osid.primordium.terms.DateTimeRangeTerm(start, end, match));
        return;
    }


    /**
     *  Gets the date query terms.
     *
     *  @return the date terms
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (this.dateTerms.toArray(new org.osid.search.terms.DateTimeRangeTerm[this.dateTerms.size()]));
    }

    
    /**
     *  Adds a date term.
     *
     *  @param term a date term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addDateTerm(org.osid.search.terms.DateTimeRangeTerm term) {
        nullarg(term, "date term");
        this.dateTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of date terms.
     *
     *  @param terms a collection of date terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addDateTerms(java.util.Collection<org.osid.search.terms.DateTimeRangeTerm> terms) {
        nullarg(terms, "date terms");
        this.dateTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a date term.
     *
     *  @param from start of date range
     *  @param to end of date range
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     */

    protected void addDateTerm(org.osid.calendaring.DateTime from, 
                               org.osid.calendaring.DateTime to, boolean match) {
        this.dateTerms.add(new net.okapia.osid.primordium.terms.DateTimeRangeTerm(from, to, match));
        return;
    }
}
