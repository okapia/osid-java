//
// AbstractStepProcessorEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractStepProcessorEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.workflow.rules.StepProcessorEnablerSearchResults {

    private org.osid.workflow.rules.StepProcessorEnablerList stepProcessorEnablers;
    private final org.osid.workflow.rules.StepProcessorEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.workflow.rules.records.StepProcessorEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractStepProcessorEnablerSearchResults.
     *
     *  @param stepProcessorEnablers the result set
     *  @param stepProcessorEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>stepProcessorEnablers</code>
     *          or <code>stepProcessorEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractStepProcessorEnablerSearchResults(org.osid.workflow.rules.StepProcessorEnablerList stepProcessorEnablers,
                                            org.osid.workflow.rules.StepProcessorEnablerQueryInspector stepProcessorEnablerQueryInspector) {
        nullarg(stepProcessorEnablers, "step processor enablers");
        nullarg(stepProcessorEnablerQueryInspector, "step processor enabler query inspectpr");

        this.stepProcessorEnablers = stepProcessorEnablers;
        this.inspector = stepProcessorEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the step processor enabler list resulting from a search.
     *
     *  @return a step processor enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorEnablerList getStepProcessorEnablers() {
        if (this.stepProcessorEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.workflow.rules.StepProcessorEnablerList stepProcessorEnablers = this.stepProcessorEnablers;
        this.stepProcessorEnablers = null;
	return (stepProcessorEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.workflow.rules.StepProcessorEnablerQueryInspector getStepProcessorEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  step processor enabler search record <code> Type. </code> This method must
     *  be used to retrieve a stepProcessorEnabler implementing the requested
     *  record.
     *
     *  @param stepProcessorEnablerSearchRecordType a stepProcessorEnabler search 
     *         record type 
     *  @return the step processor enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(stepProcessorEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepProcessorEnablerSearchResultsRecord getStepProcessorEnablerSearchResultsRecord(org.osid.type.Type stepProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.workflow.rules.records.StepProcessorEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(stepProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(stepProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record step processor enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addStepProcessorEnablerRecord(org.osid.workflow.rules.records.StepProcessorEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "step processor enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
