//
// AbstractRoomManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRoomManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.room.RoomManager,
               org.osid.room.RoomProxyManager {

    private final Types roomRecordTypes                    = new TypeRefSet();
    private final Types roomSearchRecordTypes              = new TypeRefSet();

    private final Types floorRecordTypes                   = new TypeRefSet();
    private final Types floorSearchRecordTypes             = new TypeRefSet();

    private final Types buildingRecordTypes                = new TypeRefSet();
    private final Types buildingSearchRecordTypes          = new TypeRefSet();

    private final Types campusRecordTypes                  = new TypeRefSet();
    private final Types campusSearchRecordTypes            = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRoomManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRoomManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any building federation is exposed. Federation is exposed 
     *  when a specific building may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of buildinges appears as a single building. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a room lookup service. 
     *
     *  @return <code> true </code> if room lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomLookup() {
        return (false);
    }


    /**
     *  Tests if querying rooms is available. 
     *
     *  @return <code> true </code> if room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Tests if searching for rooms is available. 
     *
     *  @return <code> true </code> if room search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSearch() {
        return (false);
    }


    /**
     *  Tests if searching for rooms is available. 
     *
     *  @return <code> true </code> if room search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomAdmin() {
        return (false);
    }


    /**
     *  Tests if room notification is available. 
     *
     *  @return <code> true </code> if room notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomNotification() {
        return (false);
    }


    /**
     *  Tests if a room to campus lookup session is available. 
     *
     *  @return <code> true </code> if room campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomCampus() {
        return (false);
    }


    /**
     *  Tests if a room to campus assignment session is available. 
     *
     *  @return <code> true </code> if room campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomCampusAssignment() {
        return (false);
    }


    /**
     *  Tests if a room smart campus session is available. 
     *
     *  @return <code> true </code> if room smart campus is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSmartCampus() {
        return (false);
    }


    /**
     *  Tests for the availability of an floor lookup service. 
     *
     *  @return <code> true </code> if floor lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorLookup() {
        return (false);
    }


    /**
     *  Tests if querying floores is available. 
     *
     *  @return <code> true </code> if floor query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorQuery() {
        return (false);
    }


    /**
     *  Tests if searching for floores is available. 
     *
     *  @return <code> true </code> if floor search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a floor administrative service for 
     *  creating and deleting floores. 
     *
     *  @return <code> true </code> if floor administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a floor notification service. 
     *
     *  @return <code> true </code> if floor notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorNotification() {
        return (false);
    }


    /**
     *  Tests if a floor to campus lookup session is available. 
     *
     *  @return <code> true </code> if floor campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorCampus() {
        return (false);
    }


    /**
     *  Tests if a floor to campus assignment session is available. 
     *
     *  @return <code> true </code> if floor campus assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorCampusAssignment() {
        return (false);
    }


    /**
     *  Tests if a floor smart campus session is available. 
     *
     *  @return <code> true </code> if floor smart campus is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFloorSmartCampus() {
        return (false);
    }


    /**
     *  Tests for the availability of an building lookup service. 
     *
     *  @return <code> true </code> if building lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingLookup() {
        return (false);
    }


    /**
     *  Tests if querying buildinges is available. 
     *
     *  @return <code> true </code> if building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingQuery() {
        return (false);
    }


    /**
     *  Tests if searching for buildinges is available. 
     *
     *  @return <code> true </code> if building search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a building administrative service for 
     *  creating and deleting buildinges. 
     *
     *  @return <code> true </code> if building administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a building notification service. 
     *
     *  @return <code> true </code> if building notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingNotification() {
        return (false);
    }


    /**
     *  Tests if a building to campus lookup session is available. 
     *
     *  @return <code> true </code> if building campus lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingCampus() {
        return (false);
    }


    /**
     *  Tests if a building to campus assignment session is available. 
     *
     *  @return <code> true </code> if building campus assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingCampusAssignment() {
        return (false);
    }


    /**
     *  Tests if a building smart campus session is available. 
     *
     *  @return <code> true </code> if building smart campus is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBuildingSmartCampus() {
        return (false);
    }


    /**
     *  Tests for the availability of an campus lookup service. 
     *
     *  @return <code> true </code> if campus lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusLookup() {
        return (false);
    }


    /**
     *  Tests if querying campuses is available. 
     *
     *  @return <code> true </code> if campus query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Tests if searching for campuses is available. 
     *
     *  @return <code> true </code> if campus search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a campus administrative service for 
     *  creating and deleting campuses. 
     *
     *  @return <code> true </code> if campus administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a campus notification service. 
     *
     *  @return <code> true </code> if campus notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a campus hierarchy traversal service. 
     *
     *  @return <code> true </code> if campus hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a campus hierarchy design service. 
     *
     *  @return <code> true </code> if campus hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a room batch service. 
     *
     *  @return <code> true </code> if a room batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a room construction service. 
     *
     *  @return <code> true </code> if a room construction service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomConstruction() {
        return (false);
    }


    /**
     *  Tests for the availability of a room squatting service. 
     *
     *  @return <code> true </code> if a room squatting service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomSquatting() {
        return (false);
    }


    /**
     *  Gets the supported <code> Room </code> record types. 
     *
     *  @return a list containing the supported room record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRoomRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.roomRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Room </code> record type is supported. 
     *
     *  @param  roomRecordType a <code> Type </code> indicating a <code> Room 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> roomRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRoomRecordType(org.osid.type.Type roomRecordType) {
        return (this.roomRecordTypes.contains(roomRecordType));
    }


    /**
     *  Adds support for a room record type.
     *
     *  @param roomRecordType a room record type
     *  @throws org.osid.NullArgumentException
     *  <code>roomRecordType</code> is <code>null</code>
     */

    protected void addRoomRecordType(org.osid.type.Type roomRecordType) {
        this.roomRecordTypes.add(roomRecordType);
        return;
    }


    /**
     *  Removes support for a room record type.
     *
     *  @param roomRecordType a room record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>roomRecordType</code> is <code>null</code>
     */

    protected void removeRoomRecordType(org.osid.type.Type roomRecordType) {
        this.roomRecordTypes.remove(roomRecordType);
        return;
    }


    /**
     *  Gets the supported room search record types. 
     *
     *  @return a list containing the supported room search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRoomSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.roomSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given room search record type is supported. 
     *
     *  @param  roomSearchRecordType a <code> Type </code> indicating a room 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> roomSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRoomSearchRecordType(org.osid.type.Type roomSearchRecordType) {
        return (this.roomSearchRecordTypes.contains(roomSearchRecordType));
    }


    /**
     *  Adds support for a room search record type.
     *
     *  @param roomSearchRecordType a room search record type
     *  @throws org.osid.NullArgumentException
     *  <code>roomSearchRecordType</code> is <code>null</code>
     */

    protected void addRoomSearchRecordType(org.osid.type.Type roomSearchRecordType) {
        this.roomSearchRecordTypes.add(roomSearchRecordType);
        return;
    }


    /**
     *  Removes support for a room search record type.
     *
     *  @param roomSearchRecordType a room search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>roomSearchRecordType</code> is <code>null</code>
     */

    protected void removeRoomSearchRecordType(org.osid.type.Type roomSearchRecordType) {
        this.roomSearchRecordTypes.remove(roomSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Floor </code> record types. 
     *
     *  @return a list containing the supported floor record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFloorRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.floorRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Floor </code> record type is supported. 
     *
     *  @param  floorRecordType a <code> Type </code> indicating a <code> 
     *          Floor </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> floorRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFloorRecordType(org.osid.type.Type floorRecordType) {
        return (this.floorRecordTypes.contains(floorRecordType));
    }


    /**
     *  Adds support for a floor record type.
     *
     *  @param floorRecordType a floor record type
     *  @throws org.osid.NullArgumentException
     *  <code>floorRecordType</code> is <code>null</code>
     */

    protected void addFloorRecordType(org.osid.type.Type floorRecordType) {
        this.floorRecordTypes.add(floorRecordType);
        return;
    }


    /**
     *  Removes support for a floor record type.
     *
     *  @param floorRecordType a floor record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>floorRecordType</code> is <code>null</code>
     */

    protected void removeFloorRecordType(org.osid.type.Type floorRecordType) {
        this.floorRecordTypes.remove(floorRecordType);
        return;
    }


    /**
     *  Gets the supported floor search record types. 
     *
     *  @return a list containing the supported floor search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFloorSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.floorSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given floor search record type is supported. 
     *
     *  @param  floorSearchRecordType a <code> Type </code> indicating a floor 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> floorSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFloorSearchRecordType(org.osid.type.Type floorSearchRecordType) {
        return (this.floorSearchRecordTypes.contains(floorSearchRecordType));
    }


    /**
     *  Adds support for a floor search record type.
     *
     *  @param floorSearchRecordType a floor search record type
     *  @throws org.osid.NullArgumentException
     *  <code>floorSearchRecordType</code> is <code>null</code>
     */

    protected void addFloorSearchRecordType(org.osid.type.Type floorSearchRecordType) {
        this.floorSearchRecordTypes.add(floorSearchRecordType);
        return;
    }


    /**
     *  Removes support for a floor search record type.
     *
     *  @param floorSearchRecordType a floor search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>floorSearchRecordType</code> is <code>null</code>
     */

    protected void removeFloorSearchRecordType(org.osid.type.Type floorSearchRecordType) {
        this.floorSearchRecordTypes.remove(floorSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Building </code> record types. 
     *
     *  @return a list containing the supported building record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBuildingRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.buildingRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Building </code> record type is supported. 
     *
     *  @param  buildingRecordType a <code> Type </code> indicating a <code> 
     *          Building </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> buildingRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBuildingRecordType(org.osid.type.Type buildingRecordType) {
        return (this.buildingRecordTypes.contains(buildingRecordType));
    }


    /**
     *  Adds support for a building record type.
     *
     *  @param buildingRecordType a building record type
     *  @throws org.osid.NullArgumentException
     *  <code>buildingRecordType</code> is <code>null</code>
     */

    protected void addBuildingRecordType(org.osid.type.Type buildingRecordType) {
        this.buildingRecordTypes.add(buildingRecordType);
        return;
    }


    /**
     *  Removes support for a building record type.
     *
     *  @param buildingRecordType a building record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>buildingRecordType</code> is <code>null</code>
     */

    protected void removeBuildingRecordType(org.osid.type.Type buildingRecordType) {
        this.buildingRecordTypes.remove(buildingRecordType);
        return;
    }


    /**
     *  Gets the supported building search record types. 
     *
     *  @return a list containing the supported building search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBuildingSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.buildingSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given building search record type is supported. 
     *
     *  @param  buildingSearchRecordType a <code> Type </code> indicating a 
     *          building record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> buildingSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBuildingSearchRecordType(org.osid.type.Type buildingSearchRecordType) {
        return (this.buildingSearchRecordTypes.contains(buildingSearchRecordType));
    }


    /**
     *  Adds support for a building search record type.
     *
     *  @param buildingSearchRecordType a building search record type
     *  @throws org.osid.NullArgumentException
     *  <code>buildingSearchRecordType</code> is <code>null</code>
     */

    protected void addBuildingSearchRecordType(org.osid.type.Type buildingSearchRecordType) {
        this.buildingSearchRecordTypes.add(buildingSearchRecordType);
        return;
    }


    /**
     *  Removes support for a building search record type.
     *
     *  @param buildingSearchRecordType a building search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>buildingSearchRecordType</code> is <code>null</code>
     */

    protected void removeBuildingSearchRecordType(org.osid.type.Type buildingSearchRecordType) {
        this.buildingSearchRecordTypes.remove(buildingSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Campus </code> record types. 
     *
     *  @return a list containing the supported campus record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCampusRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.campusRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Campus </code> record type is supported. 
     *
     *  @param  campusRecordType a <code> Type </code> indicating a <code> 
     *          Campus </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> campusRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCampusRecordType(org.osid.type.Type campusRecordType) {
        return (this.campusRecordTypes.contains(campusRecordType));
    }


    /**
     *  Adds support for a campus record type.
     *
     *  @param campusRecordType a campus record type
     *  @throws org.osid.NullArgumentException
     *  <code>campusRecordType</code> is <code>null</code>
     */

    protected void addCampusRecordType(org.osid.type.Type campusRecordType) {
        this.campusRecordTypes.add(campusRecordType);
        return;
    }


    /**
     *  Removes support for a campus record type.
     *
     *  @param campusRecordType a campus record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>campusRecordType</code> is <code>null</code>
     */

    protected void removeCampusRecordType(org.osid.type.Type campusRecordType) {
        this.campusRecordTypes.remove(campusRecordType);
        return;
    }


    /**
     *  Gets the supported campus search record types. 
     *
     *  @return a list containing the supported campus search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCampusSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.campusSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given campus search record type is supported. 
     *
     *  @param  campusSearchRecordType a <code> Type </code> indicating a 
     *          campus record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> campusSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCampusSearchRecordType(org.osid.type.Type campusSearchRecordType) {
        return (this.campusSearchRecordTypes.contains(campusSearchRecordType));
    }


    /**
     *  Adds support for a campus search record type.
     *
     *  @param campusSearchRecordType a campus search record type
     *  @throws org.osid.NullArgumentException
     *  <code>campusSearchRecordType</code> is <code>null</code>
     */

    protected void addCampusSearchRecordType(org.osid.type.Type campusSearchRecordType) {
        this.campusSearchRecordTypes.add(campusSearchRecordType);
        return;
    }


    /**
     *  Removes support for a campus search record type.
     *
     *  @param campusSearchRecordType a campus search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>campusSearchRecordType</code> is <code>null</code>
     */

    protected void removeCampusSearchRecordType(org.osid.type.Type campusSearchRecordType) {
        this.campusSearchRecordTypes.remove(campusSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room lookup 
     *  service. 
     *
     *  @return a <code> RoomLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomLookupSession getRoomLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomLookupSession getRoomLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> RoomLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomLookupSession getRoomLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomLookupSession getRoomLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room query 
     *  service. 
     *
     *  @return a <code> RoomQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuerySession getRoomQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuerySession getRoomQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> RoomQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuerySession getRoomQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Room </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuerySession getRoomQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room search 
     *  service. 
     *
     *  @return a <code> RoomSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchSession getRoomSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchSession getRoomSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> RoomSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchSession getRoomSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Room </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSearchSession getRoomSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  administration service. 
     *
     *  @return a <code> RoomAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomAdminSession getRoomAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomAdminSession getRoomAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> RoomAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomAdminSession getRoomAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Room </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomAdminSession getRoomAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  notification service. 
     *
     *  @param  roomReceiver the receiver 
     *  @return a <code> RoomNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> roomReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomNotificationSession getRoomNotificationSession(org.osid.room.RoomReceiver roomReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  notification service. 
     *
     *  @param  roomReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> RoomNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> roomReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomNotificationSession getRoomNotificationSession(org.osid.room.RoomReceiver roomReceiver, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  notification service for the given campus. 
     *
     *  @param  roomReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> RoomNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> roomReceiver </code> or 
     *          <code> campusId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomNotificationSession getRoomNotificationSessionForCampus(org.osid.room.RoomReceiver roomReceiver, 
                                                                                     org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the room 
     *  notification service for the given campus. 
     *
     *  @param  roomReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RoomNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Room </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> roomReceiver, campusId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomNotificationSession getRoomNotificationSessionForCampus(org.osid.room.RoomReceiver roomReceiver, 
                                                                                     org.osid.id.Id campusId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the session for retrieving room to campus mappings. 
     *
     *  @return a <code> RoomCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomCampusSession getRoomCampusSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomCampusSession not implemented");
    }


    /**
     *  Gets the session for retrieving room to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomCampusSession getRoomCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomCampusSession not implemented");
    }


    /**
     *  Gets the session for assigning room to campus mappings. 
     *
     *  @return a <code> RoomCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomCampusAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomCampusAssignmentSession getRoomCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning room to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RoomCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomCampusAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomCampusAssignmentSession getRoomCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the room smart campus for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> RoomSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSmartCampusSession getRoomSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomSmartCampusSession not implemented");
    }


    /**
     *  Gets the session associated with the room smart campus for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @param  proxy a proxy 
     *  @return a <code> RoomSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomSmartCampusSession getRoomSmartCampusSession(org.osid.id.Id campusId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomSmartCampusSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor lookup 
     *  service. 
     *
     *  @return a <code> FloorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorLookupSession getFloorLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorLookupSession getFloorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Floor </code> 
     *  @return a <code> FloorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorLookupSession getFloorLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor lookup 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorLookupSession getFloorLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor query 
     *  service. 
     *
     *  @return a <code> FloorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuerySession getFloorQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuerySession getFloorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Floor </code> 
     *  @return a <code> FloorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuerySession getFloorQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Floor </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorQuerySession getFloorQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor search 
     *  service. 
     *
     *  @return a <code> FloorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSearchSession getFloorSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSearchSession getFloorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Floor </code> 
     *  @return a <code> FloorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSearchSession getFloorSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor search 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Floor </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSearchSession getFloorSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  administrative service. 
     *
     *  @return a <code> FloorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorAdminSession getFloorAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorAdminSession getFloorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Floor </code> 
     *  @return a <code> FloorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorAdminSession getFloorAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Floor </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorAdminSession getFloorAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  notification service. 
     *
     *  @param  floorReceiver the receiver 
     *  @return a <code> FloorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> floorReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorNotificationSession getFloorNotificationSession(org.osid.room.FloorReceiver floorReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  notification service. 
     *
     *  @param  floorReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> FloorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> floorReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorNotificationSession getFloorNotificationSession(org.osid.room.FloorReceiver floorReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  notification service for the given campus. 
     *
     *  @param  floorReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> FloorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> floorReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorNotificationSession getFloorNotificationSessionForCampus(org.osid.room.FloorReceiver floorReceiver, 
                                                                                       org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the floor 
     *  notification service for the given campus. 
     *
     *  @param  floorReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> FloorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Floor </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> floorReceiver, campusId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorNotificationSession getFloorNotificationSessionForCampus(org.osid.room.FloorReceiver floorReceiver, 
                                                                                       org.osid.id.Id campusId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the session for retrieving floor to campus mappings. 
     *
     *  @return a <code> FloorCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorCampusSession getFloorCampusSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorCampusSession not implemented");
    }


    /**
     *  Gets the session for retrieving floor to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFloorCampus() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorCampusSession getFloorCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorCampusSession not implemented");
    }


    /**
     *  Gets the session for assigning floor to campus mappings. 
     *
     *  @return a <code> FloorCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.FloorCampusAssignmentSession getFloorCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning floor to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FloorCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.FloorCampusAssignmentSession getFloorCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the floor smart campus for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> FloorSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSmartCampusSession getFloorSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getFloorSmartCampusSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic floor campuses for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of a campus 
     *  @param  proxy a proxy 
     *  @return a <code> FloorSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFloorSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.FloorSmartCampusSession getFloorSmartCampusSession(org.osid.id.Id campusId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getFloorSmartCampusSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  lookup service. 
     *
     *  @return a <code> BuildingLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingLookupSession getBuildingLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingLookupSession getBuildingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  lookup service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Building </code> 
     *  @return a <code> BuildingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingLookupSession getBuildingLookupSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  lookup service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingLookupSession getBuildingLookupSessionForCampus(org.osid.id.Id campusId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingLookupSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building query 
     *  service. 
     *
     *  @return a <code> BuildingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuerySession getBuildingQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuerySession getBuildingQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Building </code> 
     *  @return a <code> BuildingQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuerySession getBuildingQuerySessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building query 
     *  service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Building </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuerySession getBuildingQuerySessionForCampus(org.osid.id.Id campusId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingQuerySessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  search service. 
     *
     *  @return a <code> BuildingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchSession getBuildingSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchSession getBuildingSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  search service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Building </code> 
     *  @return a <code> BuildingSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchSession getBuildingSearchSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  search service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Building </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSearchSession getBuildingSearchSessionForCampus(org.osid.id.Id campusId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingSearchSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  administrative service. 
     *
     *  @return a <code> BuildingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingAdminSession getBuildingAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingAdminSession getBuildingAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  administrative service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Building </code> 
     *  @return a <code> BuildingAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingAdminSession getBuildingAdminSessionForCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  administration service for the given campus. 
     *
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Building </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBuildingAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingAdminSession getBuildingAdminSessionForCampus(org.osid.id.Id campusId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingAdminSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  notification service. 
     *
     *  @param  buildingReceiver the receiver 
     *  @return a <code> BuildingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> buildingReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingNotificationSession getBuildingNotificationSession(org.osid.room.BuildingReceiver buildingReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  notification service. 
     *
     *  @param  buildingReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> buildingReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingNotificationSession getBuildingNotificationSession(org.osid.room.BuildingReceiver buildingReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  notification service for the given campus. 
     *
     *  @param  buildingReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @return a <code> BuildingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Campus </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> buildingReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingNotificationSession getBuildingNotificationSessionForCampus(org.osid.room.BuildingReceiver buildingReceiver, 
                                                                                             org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the building 
     *  notification service for the given campus. 
     *
     *  @param  buildingReceiver the receiver 
     *  @param  campusId the <code> Id </code> of the <code> Campus </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Building </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> buildingReceiver, 
     *          campusId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingNotificationSession getBuildingNotificationSessionForCampus(org.osid.room.BuildingReceiver buildingReceiver, 
                                                                                             org.osid.id.Id campusId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingNotificationSessionForCampus not implemented");
    }


    /**
     *  Gets the session for retrieving building to campus mappings. 
     *
     *  @return a <code> BuildingCampusSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingCampusSession getBuildingCampusSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingCampusSession not implemented");
    }


    /**
     *  Gets the session for retrieving building to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingCampusSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingCampusSession getBuildingCampusSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingCampusSession not implemented");
    }


    /**
     *  Gets the session for assigning building to campus mappings. 
     *
     *  @return a <code> BuildingCampusAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingCampusAssignmentSession getBuildingCampusAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning building to campus mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BuildingCampusAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingCampusAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingCampusAssignmentSession getBuildingCampusAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingCampusAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the building smart campus for the 
     *  given campus. 
     *
     *  @param  campusId the <code> Id </code> of the campus 
     *  @return a <code> BuildingSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSmartCampusSession getBuildingSmartCampusSession(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getBuildingSmartCampusSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic building campuses for the given 
     *  campus. 
     *
     *  @param  campusId the <code> Id </code> of a campus 
     *  @param  proxy a proxy 
     *  @return a <code> BuildingSmartCampusSession </code> 
     *  @throws org.osid.NotFoundException <code> campusId </code> not found 
     *  @throws org.osid.NullArgumentException <code> campusId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBuildingSmartCampus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingSmartCampusSession getBuildingSmartCampusSession(org.osid.id.Id campusId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getBuildingSmartCampusSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus lookup 
     *  service. 
     *
     *  @return a <code> CampusLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusLookupSession getCampusLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getCampusLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusLookupSession getCampusLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getCampusLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus query 
     *  service. 
     *
     *  @return a <code> CampusQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuerySession getCampusQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getCampusQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuerySession getCampusQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getCampusQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus search 
     *  service. 
     *
     *  @return a <code> CampusSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusSearchSession getCampusSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getCampusSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusSearchSession getCampusSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getCampusSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  administrative service. 
     *
     *  @return a <code> CampusAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusAdminSession getCampusAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getCampusAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCampusAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusAdminSession getCampusAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getCampusAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  notification service. 
     *
     *  @param  campusReceiver the receiver 
     *  @return a <code> CampusNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> campusReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusNotificationSession getCampusNotificationSession(org.osid.room.CampusReceiver campusReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getCampusNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  notification service. 
     *
     *  @param  campusReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CampusNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> campusReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusNotificationSession getCampusNotificationSession(org.osid.room.CampusReceiver campusReceiver, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getCampusNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  hierarchy service. 
     *
     *  @return a <code> CampusHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusHierarchySession getCampusHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getCampusHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusHierarchySession getCampusHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getCampusHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  hierarchy design service. 
     *
     *  @return a <code> CampusHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.CampusHierarchyDesignSession getCampusHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getCampusHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the campus 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CampusHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCampusHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.CampusHierarchyDesignSession getCampusHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getCampusHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> RoomBatchManager. </code> 
     *
     *  @return a <code> RoomBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoombatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.RoomBatchManager getRoomBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomBatchManager not implemented");
    }


    /**
     *  Gets the <code> RoomBatchProxyManager. </code> 
     *
     *  @return a <code> RoomBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.batch.RoomBatchProxyManager getRoomBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomBatchProxyManager not implemented");
    }


    /**
     *  Gets the <code> RoomConstructionManager. </code> 
     *
     *  @return a <code> RoomConstructionManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomConstruction() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RoomConstructionManager getRoomConstructionManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomConstructionManager not implemented");
    }


    /**
     *  Gets the <code> RoomConstructionProxyManager. </code> 
     *
     *  @return a <code> RoomConstructionProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRoomConstruction() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.construction.RoomConstructionProxyManager getRoomConstructionProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomConstructionProxyManager not implemented");
    }


    /**
     *  Gets the <code> RoomSquattingManager. </code> 
     *
     *  @return a <code> RoomSquattingManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomSquatting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.RoomSquattingManager getRoomSquattingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomManager.getRoomSquattingManager not implemented");
    }


    /**
     *  Gets the <code> RoomSquattingProxyManager. </code> 
     *
     *  @return a <code> RoomSquattingProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRoomSquatting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.RoomSquattingProxyManager getRoomSquattingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.room.RoomProxyManager.getRoomSquattingProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.roomRecordTypes.clear();
        this.roomRecordTypes.clear();

        this.roomSearchRecordTypes.clear();
        this.roomSearchRecordTypes.clear();

        this.floorRecordTypes.clear();
        this.floorRecordTypes.clear();

        this.floorSearchRecordTypes.clear();
        this.floorSearchRecordTypes.clear();

        this.buildingRecordTypes.clear();
        this.buildingRecordTypes.clear();

        this.buildingSearchRecordTypes.clear();
        this.buildingSearchRecordTypes.clear();

        this.campusRecordTypes.clear();
        this.campusRecordTypes.clear();

        this.campusSearchRecordTypes.clear();
        this.campusSearchRecordTypes.clear();

        return;
    }
}
