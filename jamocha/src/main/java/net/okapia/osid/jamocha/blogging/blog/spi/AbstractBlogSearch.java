//
// AbstractBlogSearch.java
//
//     A template for making a Blog Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.blogging.blog.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing blog searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBlogSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.blogging.BlogSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.blogging.records.BlogSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.blogging.BlogSearchOrder blogSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of blogs. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  blogIds list of blogs
     *  @throws org.osid.NullArgumentException
     *          <code>blogIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBlogs(org.osid.id.IdList blogIds) {
        while (blogIds.hasNext()) {
            try {
                this.ids.add(blogIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBlogs</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of blog Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBlogIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  blogSearchOrder blog search order 
     *  @throws org.osid.NullArgumentException
     *          <code>blogSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>blogSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBlogResults(org.osid.blogging.BlogSearchOrder blogSearchOrder) {
	this.blogSearchOrder = blogSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.blogging.BlogSearchOrder getBlogSearchOrder() {
	return (this.blogSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given blog search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a blog implementing the requested record.
     *
     *  @param blogSearchRecordType a blog search record
     *         type
     *  @return the blog search record
     *  @throws org.osid.NullArgumentException
     *          <code>blogSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blogSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.BlogSearchRecord getBlogSearchRecord(org.osid.type.Type blogSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.blogging.records.BlogSearchRecord record : this.records) {
            if (record.implementsRecordType(blogSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blogSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this blog search. 
     *
     *  @param blogSearchRecord blog search record
     *  @param blogSearchRecordType blog search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBlogSearchRecord(org.osid.blogging.records.BlogSearchRecord blogSearchRecord, 
                                           org.osid.type.Type blogSearchRecordType) {

        addRecordType(blogSearchRecordType);
        this.records.add(blogSearchRecord);        
        return;
    }
}
