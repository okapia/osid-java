//
// AbstractBusinessQueryInspector.java
//
//     A template for making a BusinessQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.business.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for businesses.
 */

public abstract class AbstractBusinessQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.billing.BusinessQueryInspector {

    private final java.util.Collection<org.osid.billing.records.BusinessQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the customer <code> Id </code> query terms. 
     *
     *  @return the customer <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the customer query terms. 
     *
     *  @return the customer query terms 
     */

    @OSID @Override
    public org.osid.billing.CustomerQueryInspector[] getCustomerTerms() {
        return (new org.osid.billing.CustomerQueryInspector[0]);
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.billing.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.billing.ItemQueryInspector[0]);
    }


    /**
     *  Gets the category <code> Id </code> query terms. 
     *
     *  @return the category <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCategoryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the category query terms. 
     *
     *  @return the category query terms 
     */

    @OSID @Override
    public org.osid.billing.CategoryQueryInspector[] getCategoryTerms() {
        return (new org.osid.billing.CategoryQueryInspector[0]);
    }


    /**
     *  Gets the entry <code> Id </code> query terms. 
     *
     *  @return the entry <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the entry query terms. 
     *
     *  @return the entry query terms 
     */

    @OSID @Override
    public org.osid.billing.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.billing.EntryQueryInspector[0]);
    }


    /**
     *  Gets the period <code> Id </code> query terms. 
     *
     *  @return the period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the period query terms. 
     *
     *  @return the period query terms 
     */

    @OSID @Override
    public org.osid.billing.PeriodQueryInspector[] getPeriodTerms() {
        return (new org.osid.billing.PeriodQueryInspector[0]);
    }


    /**
     *  Gets the ancestor business <code> Id </code> query terms. 
     *
     *  @return the ancestor business <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor business query terms. 
     *
     *  @return the ancestor business terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getAncestorBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the descendant business <code> Id </code> query terms. 
     *
     *  @return the descendant business <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant business query terms. 
     *
     *  @return the descendant business terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getDescendantBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given business query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a business implementing the requested record.
     *
     *  @param businessRecordType a business record type
     *  @return the business query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.records.BusinessQueryInspectorRecord getBusinessQueryInspectorRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.records.BusinessQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Adds a record to this business query. 
     *
     *  @param businessQueryInspectorRecord business query inspector
     *         record
     *  @param businessRecordType business record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBusinessQueryInspectorRecord(org.osid.billing.records.BusinessQueryInspectorRecord businessQueryInspectorRecord, 
                                                   org.osid.type.Type businessRecordType) {

        addRecordType(businessRecordType);
        nullarg(businessRecordType, "business record type");
        this.records.add(businessQueryInspectorRecord);        
        return;
    }
}
