//
// AbstractObstacleEnablerQueryInspector.java
//
//     A template for making an ObstacleEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.rules.obstacleenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for obstacle enablers.
 */

public abstract class AbstractObstacleEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.mapping.path.rules.ObstacleEnablerQueryInspector {

    private final java.util.Collection<org.osid.mapping.path.rules.records.ObstacleEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the obstacle <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledObstacleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the obstacle query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.path.ObstacleQueryInspector[] getRuledObstacleTerms() {
        return (new org.osid.mapping.path.ObstacleQueryInspector[0]);
    }


    /**
     *  Gets the map <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getMapIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the map query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.mapping.MapQueryInspector[] getMapTerms() {
        return (new org.osid.mapping.MapQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given obstacle enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an obstacle enabler implementing the requested record.
     *
     *  @param obstacleEnablerRecordType an obstacle enabler record type
     *  @return the obstacle enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>obstacleEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(obstacleEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.rules.records.ObstacleEnablerQueryInspectorRecord getObstacleEnablerQueryInspectorRecord(org.osid.type.Type obstacleEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.rules.records.ObstacleEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(obstacleEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(obstacleEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this obstacle enabler query. 
     *
     *  @param obstacleEnablerQueryInspectorRecord obstacle enabler query inspector
     *         record
     *  @param obstacleEnablerRecordType obstacleEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addObstacleEnablerQueryInspectorRecord(org.osid.mapping.path.rules.records.ObstacleEnablerQueryInspectorRecord obstacleEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type obstacleEnablerRecordType) {

        addRecordType(obstacleEnablerRecordType);
        nullarg(obstacleEnablerRecordType, "obstacle enabler record type");
        this.records.add(obstacleEnablerQueryInspectorRecord);        
        return;
    }
}
