//
// MutableMapProxyStepLookupSession
//
//    Implements a Step lookup service backed by a collection of
//    steps that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow;


/**
 *  Implements a Step lookup service backed by a collection of
 *  steps. The steps are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of steps can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyStepLookupSession
    extends net.okapia.osid.jamocha.core.workflow.spi.AbstractMapStepLookupSession
    implements org.osid.workflow.StepLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyStepLookupSession}
     *  with no steps.
     *
     *  @param office the office
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyStepLookupSession(org.osid.workflow.Office office,
                                                  org.osid.proxy.Proxy proxy) {
        setOffice(office);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyStepLookupSession} with a
     *  single step.
     *
     *  @param office the office
     *  @param step a step
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code step}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyStepLookupSession(org.osid.workflow.Office office,
                                                org.osid.workflow.Step step, org.osid.proxy.Proxy proxy) {
        this(office, proxy);
        putStep(step);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyStepLookupSession} using an
     *  array of steps.
     *
     *  @param office the office
     *  @param steps an array of steps
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code steps}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyStepLookupSession(org.osid.workflow.Office office,
                                                org.osid.workflow.Step[] steps, org.osid.proxy.Proxy proxy) {
        this(office, proxy);
        putSteps(steps);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyStepLookupSession} using a
     *  collection of steps.
     *
     *  @param office the office
     *  @param steps a collection of steps
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code office},
     *          {@code steps}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyStepLookupSession(org.osid.workflow.Office office,
                                                java.util.Collection<? extends org.osid.workflow.Step> steps,
                                                org.osid.proxy.Proxy proxy) {
   
        this(office, proxy);
        setSessionProxy(proxy);
        putSteps(steps);
        return;
    }

    
    /**
     *  Makes a {@code Step} available in this session.
     *
     *  @param step an step
     *  @throws org.osid.NullArgumentException {@code step{@code 
     *          is {@code null}
     */

    @Override
    public void putStep(org.osid.workflow.Step step) {
        super.putStep(step);
        return;
    }


    /**
     *  Makes an array of steps available in this session.
     *
     *  @param steps an array of steps
     *  @throws org.osid.NullArgumentException {@code steps{@code 
     *          is {@code null}
     */

    @Override
    public void putSteps(org.osid.workflow.Step[] steps) {
        super.putSteps(steps);
        return;
    }


    /**
     *  Makes collection of steps available in this session.
     *
     *  @param steps
     *  @throws org.osid.NullArgumentException {@code step{@code 
     *          is {@code null}
     */

    @Override
    public void putSteps(java.util.Collection<? extends org.osid.workflow.Step> steps) {
        super.putSteps(steps);
        return;
    }


    /**
     *  Removes a Step from this session.
     *
     *  @param stepId the {@code Id} of the step
     *  @throws org.osid.NullArgumentException {@code stepId{@code  is
     *          {@code null}
     */

    @Override
    public void removeStep(org.osid.id.Id stepId) {
        super.removeStep(stepId);
        return;
    }    
}
