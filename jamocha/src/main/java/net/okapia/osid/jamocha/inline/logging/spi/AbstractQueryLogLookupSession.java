//
// AbstractQueryLogLookupSession.java
//
//    An inline adapter that maps a LogLookupSession to
//    a LogQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.logging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a LogLookupSession to
 *  a LogQuerySession.
 */

public abstract class AbstractQueryLogLookupSession
    extends net.okapia.osid.jamocha.logging.spi.AbstractLogLookupSession
    implements org.osid.logging.LogLookupSession {

    private final org.osid.logging.LogQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryLogLookupSession.
     *
     *  @param querySession the underlying log query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryLogLookupSession(org.osid.logging.LogQuerySession querySession) {
        nullarg(querySession, "log query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Log</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupLogs() {
        return (this.session.canSearchLogs());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Log</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Log</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Log</code> and
     *  retained for compatibility.
     *
     *  @param  logId <code>Id</code> of the
     *          <code>Log</code>
     *  @return the log
     *  @throws org.osid.NotFoundException <code>logId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>logId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.Log getLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogQuery query = getQuery();
        query.matchId(logId, true);
        org.osid.logging.LogList logs = this.session.getLogsByQuery(query);
        if (logs.hasNext()) {
            return (logs.getNextLog());
        } 
        
        throw new org.osid.NotFoundException(logId + " not found");
    }


    /**
     *  Gets a <code>LogList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  logs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Logs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  logIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Log</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>logIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByIds(org.osid.id.IdList logIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogQuery query = getQuery();

        try (org.osid.id.IdList ids = logIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getLogsByQuery(query));
    }


    /**
     *  Gets a <code>LogList</code> corresponding to the given
     *  log genus <code>Type</code> which does not include
     *  logs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  logs or an error results. Otherwise, the returned list
     *  may contain only those logs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logGenusType a log genus type 
     *  @return the returned <code>Log</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByGenusType(org.osid.type.Type logGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogQuery query = getQuery();
        query.matchGenusType(logGenusType, true);
        return (this.session.getLogsByQuery(query));
    }


    /**
     *  Gets a <code>LogList</code> corresponding to the given
     *  log genus <code>Type</code> and include any additional
     *  logs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  logs or an error results. Otherwise, the returned list
     *  may contain only those logs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logGenusType a log genus type 
     *  @return the returned <code>Log</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByParentGenusType(org.osid.type.Type logGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogQuery query = getQuery();
        query.matchParentGenusType(logGenusType, true);
        return (this.session.getLogsByQuery(query));
    }


    /**
     *  Gets a <code>LogList</code> containing the given
     *  log record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  logs or an error results. Otherwise, the returned list
     *  may contain only those logs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logRecordType a log record type 
     *  @return the returned <code>Log</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByRecordType(org.osid.type.Type logRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogQuery query = getQuery();
        query.matchRecordType(logRecordType, true);
        return (this.session.getLogsByQuery(query));
    }


    /**
     *  Gets a <code>LogList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known logs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  logs that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Log</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getLogsByQuery(query));        
    }

    
    /**
     *  Gets all <code>Logs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  logs or an error results. Otherwise, the returned list
     *  may contain only those logs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Logs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getLogsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.logging.LogQuery getQuery() {
        org.osid.logging.LogQuery query = this.session.getLogQuery();
        return (query);
    }
}
