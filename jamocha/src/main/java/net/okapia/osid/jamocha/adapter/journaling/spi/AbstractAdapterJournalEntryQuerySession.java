//
// AbstractQueryJournalEntryLookupSession.java
//
//    A JournalEntryQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A JournalEntryQuerySession adapter.
 */

public abstract class AbstractAdapterJournalEntryQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.journaling.JournalEntryQuerySession {

    private final org.osid.journaling.JournalEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterJournalEntryQuerySession.
     *
     *  @param session the underlying journal entry query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterJournalEntryQuerySession(org.osid.journaling.JournalEntryQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeJournal</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeJournal Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getJournalId() {
        return (this.session.getJournalId());
    }


    /**
     *  Gets the {@codeJournal</code> associated with this 
     *  session.
     *
     *  @return the {@codeJournal</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.Journal getJournal()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getJournal());
    }


    /**
     *  Tests if this user can perform {@codeJournalEntry</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchJournalEntries() {
        return (this.session.canSearchJournalEntries());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include journal entries in journals which are children
     *  of this journal in the journal hierarchy.
     */

    @OSID @Override
    public void useFederatedJournalView() {
        this.session.useFederatedJournalView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this journal only.
     */
    
    @OSID @Override
    public void useIsolatedJournalView() {
        this.session.useIsolatedJournalView();
        return;
    }
    
      
    /**
     *  Gets a journal entry query. The returned query will not have an
     *  extension query.
     *
     *  @return the journal entry query 
     */
      
    @OSID @Override
    public org.osid.journaling.JournalEntryQuery getJournalEntryQuery() {
        return (this.session.getJournalEntryQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  journalEntryQuery the journal entry query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code journalEntryQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code journalEntryQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryList getJournalEntriesByQuery(org.osid.journaling.JournalEntryQuery journalEntryQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getJournalEntriesByQuery(journalEntryQuery));
    }
}
