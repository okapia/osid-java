//
// JDBCFamilyNodeList
//
//     Implements a FamilyNodeList. This list creates familynodes from
//     the return of a JDBC query.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.jdbc.relationship.familynode;

import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  <p>Implements a FamilyNodeList. This list creates familynodes from the
 *  return of a JDBC query. The query is processed in a separate
 *  thread.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more familynodes to be added.</p>
 */

public final class JDBCFamilyNodeList
    extends net.okapia.osid.jamocha.relationship.familynode.spi.AbstractMutableFamilyNodeList
    implements org.osid.relationship.FamilyNodeList,
               Runnable {

    private boolean running = false;
    private FamilyNodeFetcher fetcher;


    /**
     *  Creates a new <code>JDBCFamilyNodeList</code>.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator a familyNode to parse a result row and
     *         generate a FamilyNode
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of familynodes in this list
     *         buffer such that when the number of familynodes exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until familynodes are retrieved. The value
     *         of this may depend on the amount of memory a FamilyNode
     *         consumes.
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>familyNode</code>
     *          is <code>null</code>
     */

    public JDBCFamilyNodeList(String query, java.sql.Connection connection, JDBCFamilyNodeGenerator generator,
                        boolean closeWhenDone, int bufferSize)
        throws org.osid.OperationFailedException {

        nullarg(query, "query");
        nullarg(connection, "connection");
        nullarg(generator, "generator");

        if (bufferSize <= 0) {
            throw new org.osid.InvalidArgumentException("buffer size too small");
        }

        this.fetcher = new FamilyNodeFetcher(query, connection, generator, closeWhenDone, bufferSize);
        return;
    }


    /**
     *  Creates a new <code>JDBCFamilyNodeList</code> and runs it upon
     *  instantiation.
     *
     *  @param query an SQL query
     *  @param connection a JDBC connection
     *  @param generator a FamilyNode to parse a result row and
     *         generate a FamilyNode
     *  @param closeWhenDone <code>true</code> if the connection
     *         should be closed following the database transaction,
     *         <code>false</code> to leave it open
     *  @param bufferSize limits the number of familynodes in this list
     *         buffer such that when the number of familynodes exceeds
     *         <code>bufferSize</code>, reading from the JDBC result
     *         set will stop until familynodes are retrieved. The value
     *         of this may depend on the amount of memory a FamilyNode
     *         consumes.
     *  @param run <code>true</code> to start the fetching thread
     *         immediately
     *  @throws org.osid.InvalidArgumentException
     *          <code>bufferSize</code> not greater than zero
     *  @throws org.osid.NullArgumentException <code>familyNode</code>
     *          is <code>null</code>
     */

    public JDBCFamilyNodeList(String query, java.sql.Connection connection, JDBCFamilyNodeGenerator generator,
                        boolean closeWhenDone, int bufferSize, boolean run)
        throws org.osid.OperationFailedException {

        this(query, connection, generator, closeWhenDone, bufferSize);
        if (run) {
            run();
        }
        return;
    }


    /**
     *  Starts the JDBC process.
     *
     *  @throws org.osid.IllegalStateException already started
     */

    public void run() {
        if (this.running || hasError()) {
            throw new org.osid.IllegalStateException("already started");
        }

        this.fetcher.start();
        return;
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already
     *          closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.running = false;
        return;
    }


    /**
     *  Tests if the list thread is running to populate elements from
     *  the underlying list.
     *
     *  @return <code>true</code> if the list is running,
     *          <code>false</code> otherwise.
     */

    public boolean isRunning() {
        return (this.running);
    }


    class FamilyNodeFetcher
        extends Thread {
        
        private String query;
        private java.sql.Connection connection;
        private JDBCFamilyNodeGenerator generator;
        private boolean closeWhenDone;
        private int bufferSize;

        
        FamilyNodeFetcher(String query, java.sql.Connection connection, JDBCFamilyNodeGenerator generator,
                        boolean closeWhenDone, int bufferSize) {

            this.query         = query;
            this.connection    = connection;
            this.generator     = generator;
            this.closeWhenDone = closeWhenDone;
            this.bufferSize    = bufferSize;

            return;
        }
            
        
        public void run() {
            java.sql.Statement statement = null;
            java.sql.ResultSet resultset = null;
            JDBCFamilyNodeList.this.running = true;

            try {
                statement = this.connection.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,
                                                            java.sql.ResultSet.CONCUR_READ_ONLY);
                resultset = statement.executeQuery(this.query);
                long length = 0;

                while (resultset.next() && JDBCFamilyNodeList.this.running && !JDBCFamilyNodeList.this.hasError()) {
                    JDBCFamilyNodeList.this.addFamilyNode(generator.makeFamilyNode(resultset));
                    synchronized (JDBCFamilyNodeList.this) {
                        JDBCFamilyNodeList.this.notifyAll();
                    }

                    if (++length > this.bufferSize) {
                        length = JDBCFamilyNodeList.this.available();
                        if (length > this.bufferSize) {
                            synchronized (JDBCFamilyNodeList.this) {
                                try {
                                    JDBCFamilyNodeList.this.wait();
                                } catch (InterruptedException ie) {}
                            }
                        }
                    } 
                }
            } catch (Exception e) {
                JDBCFamilyNodeList.this.error(new org.osid.OperationFailedException("cannot generate familynode", e));
                return;
            } finally {
                try {
                    if (this.closeWhenDone) {
                        this.connection.close();
                    }
                 
                    JDBCFamilyNodeList.this.running = false;

                    if (statement != null) {
                        statement.close();
                    }

                    if (resultset != null) {
                        resultset.close();
                    }
                } catch (Exception e) {}
            }

            JDBCFamilyNodeList.this.eol();
            return;
        }
    }
}
