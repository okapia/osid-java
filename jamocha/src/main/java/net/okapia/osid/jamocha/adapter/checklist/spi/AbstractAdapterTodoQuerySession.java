//
// AbstractQueryTodoLookupSession.java
//
//    A TodoQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A TodoQuerySession adapter.
 */

public abstract class AbstractAdapterTodoQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.checklist.TodoQuerySession {

    private final org.osid.checklist.TodoQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterTodoQuerySession.
     *
     *  @param session the underlying todo query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTodoQuerySession(org.osid.checklist.TodoQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeChecklist</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeChecklist Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getChecklistId() {
        return (this.session.getChecklistId());
    }


    /**
     *  Gets the {@codeChecklist</code> associated with this 
     *  session.
     *
     *  @return the {@codeChecklist</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Checklist getChecklist()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getChecklist());
    }


    /**
     *  Tests if this user can perform {@codeTodo</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchTodos() {
        return (this.session.canSearchTodos());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include todos in checklists which are children
     *  of this checklist in the checklist hierarchy.
     */

    @OSID @Override
    public void useFederatedChecklistView() {
        this.session.useFederatedChecklistView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this checklist only.
     */
    
    @OSID @Override
    public void useIsolatedChecklistView() {
        this.session.useIsolatedChecklistView();
        return;
    }
    
      
    /**
     *  The returns from the lookup methods omit sequestered
     *  todos.
     */

    @OSID @Override
    public void useSequesteredTodoView() {
        this.session.useSequesteredTodoView();
        return;
    }


    /**
     *  All todos are returned including sequestered todos.
     */

    @OSID @Override
    public void useUnsequesteredTodoView() {
        this.session.useUnsequesteredTodoView();
        return;
    }


    /**
     *  Gets a todo query. The returned query will not have an
     *  extension query.
     *
     *  @return the todo query 
     */
      
    @OSID @Override
    public org.osid.checklist.TodoQuery getTodoQuery() {
        return (this.session.getTodoQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  todoQuery the todo query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code todoQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code todoQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodosByQuery(org.osid.checklist.TodoQuery todoQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getTodosByQuery(todoQuery));
    }
}
