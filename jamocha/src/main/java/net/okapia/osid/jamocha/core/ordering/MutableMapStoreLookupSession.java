//
// MutableMapStoreLookupSession
//
//    Implements a Store lookup service backed by a collection of
//    stores that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering;


/**
 *  Implements a Store lookup service backed by a collection of
 *  stores. The stores are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of stores can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapStoreLookupSession
    extends net.okapia.osid.jamocha.core.ordering.spi.AbstractMapStoreLookupSession
    implements org.osid.ordering.StoreLookupSession {


    /**
     *  Constructs a new {@code MutableMapStoreLookupSession}
     *  with no stores.
     */

    public MutableMapStoreLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapStoreLookupSession} with a
     *  single store.
     *  
     *  @param store a store
     *  @throws org.osid.NullArgumentException {@code store}
     *          is {@code null}
     */

    public MutableMapStoreLookupSession(org.osid.ordering.Store store) {
        putStore(store);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapStoreLookupSession}
     *  using an array of stores.
     *
     *  @param stores an array of stores
     *  @throws org.osid.NullArgumentException {@code stores}
     *          is {@code null}
     */

    public MutableMapStoreLookupSession(org.osid.ordering.Store[] stores) {
        putStores(stores);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapStoreLookupSession}
     *  using a collection of stores.
     *
     *  @param stores a collection of stores
     *  @throws org.osid.NullArgumentException {@code stores}
     *          is {@code null}
     */

    public MutableMapStoreLookupSession(java.util.Collection<? extends org.osid.ordering.Store> stores) {
        putStores(stores);
        return;
    }

    
    /**
     *  Makes a {@code Store} available in this session.
     *
     *  @param store a store
     *  @throws org.osid.NullArgumentException {@code store{@code  is
     *          {@code null}
     */

    @Override
    public void putStore(org.osid.ordering.Store store) {
        super.putStore(store);
        return;
    }


    /**
     *  Makes an array of stores available in this session.
     *
     *  @param stores an array of stores
     *  @throws org.osid.NullArgumentException {@code stores{@code 
     *          is {@code null}
     */

    @Override
    public void putStores(org.osid.ordering.Store[] stores) {
        super.putStores(stores);
        return;
    }


    /**
     *  Makes collection of stores available in this session.
     *
     *  @param stores a collection of stores
     *  @throws org.osid.NullArgumentException {@code stores{@code  is
     *          {@code null}
     */

    @Override
    public void putStores(java.util.Collection<? extends org.osid.ordering.Store> stores) {
        super.putStores(stores);
        return;
    }


    /**
     *  Removes a Store from this session.
     *
     *  @param storeId the {@code Id} of the store
     *  @throws org.osid.NullArgumentException {@code storeId{@code 
     *          is {@code null}
     */

    @Override
    public void removeStore(org.osid.id.Id storeId) {
        super.removeStore(storeId);
        return;
    }    
}
