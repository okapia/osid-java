//
// AbstractJobProcessorEnablerList
//
//     Implements a filter for a JobProcessorEnablerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.resourcing.rules.jobprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a JobProcessorEnablerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedJobProcessorEnablerList
 *  to improve performance.
 */

public abstract class AbstractJobProcessorEnablerFilterList
    extends net.okapia.osid.jamocha.resourcing.rules.jobprocessorenabler.spi.AbstractJobProcessorEnablerList
    implements org.osid.resourcing.rules.JobProcessorEnablerList,
               net.okapia.osid.jamocha.inline.filter.resourcing.rules.jobprocessorenabler.JobProcessorEnablerFilter {

    private org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler;
    private final org.osid.resourcing.rules.JobProcessorEnablerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractJobProcessorEnablerFilterList</code>.
     *
     *  @param jobProcessorEnablerList a <code>JobProcessorEnablerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerList</code> is <code>null</code>
     */

    protected AbstractJobProcessorEnablerFilterList(org.osid.resourcing.rules.JobProcessorEnablerList jobProcessorEnablerList) {
        nullarg(jobProcessorEnablerList, "job processor enabler list");
        this.list = jobProcessorEnablerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.jobProcessorEnabler == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> JobProcessorEnabler </code> in this list. 
     *
     *  @return the next <code> JobProcessorEnabler </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> JobProcessorEnabler </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnabler getNextJobProcessorEnabler()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler = this.jobProcessorEnabler;
            this.jobProcessorEnabler = null;
            return (jobProcessorEnabler);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in job processor enabler list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.jobProcessorEnabler = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters JobProcessorEnablers.
     *
     *  @param jobProcessorEnabler the job processor enabler to filter
     *  @return <code>true</code> if the job processor enabler passes the filter,
     *          <code>false</code> if the job processor enabler should be filtered
     */

    public abstract boolean pass(org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler);


    protected void prime() {
        if (this.jobProcessorEnabler != null) {
            return;
        }

        org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler = null;

        while (this.list.hasNext()) {
            try {
                jobProcessorEnabler = this.list.getNextJobProcessorEnabler();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(jobProcessorEnabler)) {
                this.jobProcessorEnabler = jobProcessorEnabler;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
