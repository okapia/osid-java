//
// MutableMapRegistrationLookupSession
//
//    Implements a Registration lookup service backed by a collection of
//    registrations that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.registration;


/**
 *  Implements a Registration lookup service backed by a collection of
 *  registrations. The registrations are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of registrations can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapRegistrationLookupSession
    extends net.okapia.osid.jamocha.core.course.registration.spi.AbstractMapRegistrationLookupSession
    implements org.osid.course.registration.RegistrationLookupSession {


    /**
     *  Constructs a new {@code MutableMapRegistrationLookupSession}
     *  with no registrations.
     *
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumentException {@code courseCatalog} is
     *          {@code null}
     */

      public MutableMapRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRegistrationLookupSession} with a
     *  single registration.
     *
     *  @param courseCatalog the course catalog  
     *  @param registration a registration
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code registration} is {@code null}
     */

    public MutableMapRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.registration.Registration registration) {
        this(courseCatalog);
        putRegistration(registration);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRegistrationLookupSession}
     *  using an array of registrations.
     *
     *  @param courseCatalog the course catalog
     *  @param registrations an array of registrations
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code registrations} is {@code null}
     */

    public MutableMapRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           org.osid.course.registration.Registration[] registrations) {
        this(courseCatalog);
        putRegistrations(registrations);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapRegistrationLookupSession}
     *  using a collection of registrations.
     *
     *  @param courseCatalog the course catalog
     *  @param registrations a collection of registrations
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code registrations} is {@code null}
     */

    public MutableMapRegistrationLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                           java.util.Collection<? extends org.osid.course.registration.Registration> registrations) {

        this(courseCatalog);
        putRegistrations(registrations);
        return;
    }

    
    /**
     *  Makes a {@code Registration} available in this session.
     *
     *  @param registration a registration
     *  @throws org.osid.NullArgumentException {@code registration{@code  is
     *          {@code null}
     */

    @Override
    public void putRegistration(org.osid.course.registration.Registration registration) {
        super.putRegistration(registration);
        return;
    }


    /**
     *  Makes an array of registrations available in this session.
     *
     *  @param registrations an array of registrations
     *  @throws org.osid.NullArgumentException {@code registrations{@code 
     *          is {@code null}
     */

    @Override
    public void putRegistrations(org.osid.course.registration.Registration[] registrations) {
        super.putRegistrations(registrations);
        return;
    }


    /**
     *  Makes collection of registrations available in this session.
     *
     *  @param registrations a collection of registrations
     *  @throws org.osid.NullArgumentException {@code registrations{@code  is
     *          {@code null}
     */

    @Override
    public void putRegistrations(java.util.Collection<? extends org.osid.course.registration.Registration> registrations) {
        super.putRegistrations(registrations);
        return;
    }


    /**
     *  Removes a Registration from this session.
     *
     *  @param registrationId the {@code Id} of the registration
     *  @throws org.osid.NullArgumentException {@code registrationId{@code 
     *          is {@code null}
     */

    @Override
    public void removeRegistration(org.osid.id.Id registrationId) {
        super.removeRegistration(registrationId);
        return;
    }    
}
