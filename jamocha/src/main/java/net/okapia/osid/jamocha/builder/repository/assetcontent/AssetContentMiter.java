//
// AssetContentMiter.java
//
//     Defines an AssetContent miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.repository.assetcontent;


/**
 *  Defines an <code>AssetContent</code> miter for use with the builders.
 */

public interface AssetContentMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.repository.AssetContent {


    /**
     *  Sets the asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    public void setAsset(org.osid.repository.Asset asset);


    /**
     *  Adds an accessibility type.
     *
     *  @param accessibilityType an accessibility type
     *  @throws org.osid.NullArgumentException
     *          <code>accessibilityType</code> is <code>null</code>
     */

    public void addAccessibilityType(org.osid.type.Type accessibilityType);


    /**
     *  Sets all the accessibility types.
     *
     *  @param accessibilityTypes a collection of accessibility types
     *  @throws org.osid.NullArgumentException
     *          <code>accessibilityTypes</code> is <code>null</code>
     */

    public void setAccessibilityTypes(java.util.Collection<org.osid.type.Type> accessibilityTypes);


    /**
     *  Sets the data length.
     *
     *  @param length a data length
     *  @throws org.osid.InvalidArgumentException <code>length</code>
     *          is negative
     */

    public void setDataLength(long length);


    /**
     *  Sets the asset content data. 
     *
     *  @param data a flipped data buffer
     *  @throws org.osid.NullArgumentException <code>data</code> is
     *          <code>null</code>
     */

    public void setData(java.nio.ByteBuffer data);


    /**
     *  Sets the url.
     *
     *  @param url a URL
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public void setURL(String url);


    /**
     *  Adds an AssetContent record.
     *
     *  @param record an assetContent record
     *  @param recordType the type of assetContent record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAssetContentRecord(org.osid.repository.records.AssetContentRecord record, org.osid.type.Type recordType);
}       


