//
// AbstractProvision.java
//
//     Defines a Provision builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.provision.spi;


/**
 *  Defines a <code>Provision</code> builder.
 */

public abstract class AbstractProvisionBuilder<T extends AbstractProvisionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.provision.ProvisionMiter provision;


    /**
     *  Constructs a new <code>AbstractProvisionBuilder</code>.
     *
     *  @param provision the provision to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProvisionBuilder(net.okapia.osid.jamocha.builder.provisioning.provision.ProvisionMiter provision) {
        super(provision);
        this.provision = provision;
        return;
    }


    /**
     *  Builds the provision.
     *
     *  @return the new provision
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.Provision build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.provision.ProvisionValidator(getValidations())).validate(this.provision);
        return (new net.okapia.osid.jamocha.builder.provisioning.provision.ImmutableProvision(this.provision));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the provision miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.provision.ProvisionMiter getMiter() {
        return (this.provision);
    }


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>broker</code> is <code>null</code>
     */

    public T broker(org.osid.provisioning.Broker broker) {
        getMiter().setBroker(broker);
        return (self());
    }


    /**
     *  Sets the provisionable.
     *
     *  @param provisionable a provisionable
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>provisionable</code> is <code>null</code>
     */

    public T provisionable(org.osid.provisioning.Provisionable provisionable) {
        getMiter().setProvisionable(provisionable);
        return (self());
    }


    /**
     *  Sets the recipient.
     *
     *  @param recipient a recipient
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>recipient</code>
     *          is <code>null</code>
     */

    public T recipient(org.osid.resource.Resource recipient) {
        getMiter().setRecipient(recipient);
        return (self());
    }


    /**
     *  Sets the request.
     *
     *  @param request a request
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>request</code> is
     *          <code>null</code>
     */

    public T request(org.osid.provisioning.Request request) {
        getMiter().setRequest(request);
        return (self());
    }


    /**
     *  Sets the provision date.
     *
     *  @param date a provision date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T provisionDate(org.osid.calendaring.DateTime date) {
        getMiter().setProvisionDate(date);
        return (self());
    }

    
    /**
     *  Sets the leased flag.
     *
     *  @return the builder
     */

    public T leased() {
        getMiter().setLeased(true);
        return (self());
    }


    /**
     *  Unsets the leased flag.
     *
     *  @return the builder
     */

    public T owned() {
        getMiter().setLeased(false);
        return (self());
    }


    /**
     *  Sets the must return flag.
     *
     *  @return the builder
     */

    public T mustReturn() {
        getMiter().setMustReturn(true);
        return (self());
    }


    /**
     *  Unsets the must return flag.
     *
     *  @return the builder
     */

    public T canKeep() {
        getMiter().setMustReturn(false);
        return (self());
    }


    /**
     *  Sets the due date.
     *
     *  @param date a due date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T dueDate(org.osid.calendaring.DateTime date) {
        getMiter().setDueDate(date);
        return (self());
    }


    /**
     *  Sets the cost.
     *
     *  @param cost a cost
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>cost</code> is
     *          <code>null</code>
     */

    public T cost(org.osid.financials.Currency cost) {
        getMiter().setCost(cost);
        return (self());
    }


    /**
     *  Sets the rate amount.
     *
     *  @param amount a rate amount
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public T rateAmount(org.osid.financials.Currency amount) {
        getMiter().setRateAmount(amount);
        return (self());
    }


    /**
     *  Sets the rate period.
     *
     *  @param period a rate period
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    public T ratePeriod(org.osid.calendaring.Duration period) {
        getMiter().setRatePeriod(period);
        return (self());
    }


    /**
     *  Sets the provision return.
     *
     *  @param provisionReturn a provision return
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>provisionReturn</code> is <code>null</code>
     */

    public T provisionReturn(org.osid.provisioning.ProvisionReturn provisionReturn) {
        getMiter().setProvisionReturn(provisionReturn);
        return (self());
    }


    /**
     *  Adds a Provision record.
     *
     *  @param record a provision record
     *  @param recordType the type of provision record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.records.ProvisionRecord record, org.osid.type.Type recordType) {
        getMiter().addProvisionRecord(record, recordType);
        return (self());
    }
}       


