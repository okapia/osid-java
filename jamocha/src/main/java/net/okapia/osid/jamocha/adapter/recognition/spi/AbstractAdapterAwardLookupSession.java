//
// AbstractAdapterAwardLookupSession.java
//
//    An Award lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recognition.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Award lookup session adapter.
 */

public abstract class AbstractAdapterAwardLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recognition.AwardLookupSession {

    private final org.osid.recognition.AwardLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAwardLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAwardLookupSession(org.osid.recognition.AwardLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Academy/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Academy Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAcademyId() {
        return (this.session.getAcademyId());
    }


    /**
     *  Gets the {@code Academy} associated with this session.
     *
     *  @return the {@code Academy} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAcademy());
    }


    /**
     *  Tests if this user can perform {@code Award} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAwards() {
        return (this.session.canLookupAwards());
    }


    /**
     *  A complete view of the {@code Award} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAwardView() {
        this.session.useComparativeAwardView();
        return;
    }


    /**
     *  A complete view of the {@code Award} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAwardView() {
        this.session.usePlenaryAwardView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include awards in academies which are children
     *  of this academy in the academy hierarchy.
     */

    @OSID @Override
    public void useFederatedAcademyView() {
        this.session.useFederatedAcademyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this academy only.
     */

    @OSID @Override
    public void useIsolatedAcademyView() {
        this.session.useIsolatedAcademyView();
        return;
    }
    
     
    /**
     *  Gets the {@code Award} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Award} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Award} and
     *  retained for compatibility.
     *
     *  @param awardId {@code Id} of the {@code Award}
     *  @return the award
     *  @throws org.osid.NotFoundException {@code awardId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code awardId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Award getAward(org.osid.id.Id awardId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAward(awardId));
    }


    /**
     *  Gets an {@code AwardList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  awards specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Awards} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  awardIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Award} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code awardIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByIds(org.osid.id.IdList awardIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwardsByIds(awardIds));
    }


    /**
     *  Gets an {@code AwardList} corresponding to the given
     *  award genus {@code Type} which does not include
     *  awards of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  awardGenusType an award genus type 
     *  @return the returned {@code Award} list
     *  @throws org.osid.NullArgumentException
     *          {@code awardGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByGenusType(org.osid.type.Type awardGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwardsByGenusType(awardGenusType));
    }


    /**
     *  Gets an {@code AwardList} corresponding to the given
     *  award genus {@code Type} and include any additional
     *  awards with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  awardGenusType an award genus type 
     *  @return the returned {@code Award} list
     *  @throws org.osid.NullArgumentException
     *          {@code awardGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByParentGenusType(org.osid.type.Type awardGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwardsByParentGenusType(awardGenusType));
    }


    /**
     *  Gets an {@code AwardList} containing the given
     *  award record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  awardRecordType an award record type 
     *  @return the returned {@code Award} list
     *  @throws org.osid.NullArgumentException
     *          {@code awardRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwardsByRecordType(org.osid.type.Type awardRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwardsByRecordType(awardRecordType));
    }


    /**
     *  Gets all {@code Awards}. 
     *
     *  In plenary mode, the returned list contains all known
     *  awards or an error results. Otherwise, the returned list
     *  may contain only those awards that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Awards} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AwardList getAwards()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAwards());
    }
}
