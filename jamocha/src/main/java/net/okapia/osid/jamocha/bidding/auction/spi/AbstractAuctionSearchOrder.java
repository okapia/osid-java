//
// AbstractAuctionSearchOdrer.java
//
//     Defines an AuctionSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.auction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AuctionSearchOrder}.
 */

public abstract class AbstractAuctionSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorSearchOrder
    implements org.osid.bidding.AuctionSearchOrder {

    private final java.util.Collection<org.osid.bidding.records.AuctionSearchOrderRecord> records = new java.util.LinkedHashSet<>();

    private final OsidTemporalSearchOrder order = new OsidTemporalSearchOrder();


    /**
     *  Specifies a preference for ordering the result set by the
     *  effective status.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByEffective(org.osid.SearchOrderStyle style) {
        this.order.orderByEffective(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the
     *  start date.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code 
     *          null} 
     */

    @OSID @Override
    public void orderByStartDate(org.osid.SearchOrderStyle style) {
        this.order.orderByStartDate(style);
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the end
     *  date.
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException {@code style} is {@code
     *          null}
     */

    @OSID @Override
    public void orderByEndDate(org.osid.SearchOrderStyle style) {
        order.orderByEndDate(style);
        return;
    }

    
    /**
     *  Orders the results by the currency type. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCurrencyType(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the minimum bidders. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMinimumBidders(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the sealed flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySealed(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by seller. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySeller(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSellerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsSellerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getSellerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSellerSearchOrder() is false");
    }


    /**
     *  Orders the results by item. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItem(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource search order is available. 
     *
     *  @return <code> true </code> if a resource search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsItemSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getItemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsItemSearchOrder() is false");
    }


    /**
     *  Orders the results by lot size. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLotSize(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by remaining items. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRemainingItems(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the auction item limit. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItemLimit(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the starting price. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStartingPrice(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the price increment 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPriceIncrement(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the reserve price. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReservePrice(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by the buyout price. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBuyoutPrice(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  auctionRecordType an auction record type 
     *  @return {@code true} if the auctionRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code auctionRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type auctionRecordType) {
        for (org.osid.bidding.records.AuctionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(auctionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  auctionRecordType the auction record type 
     *  @return the auction search order record
     *  @throws org.osid.NullArgumentException
     *          {@code auctionRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(auctionRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionSearchOrderRecord getAuctionSearchOrderRecord(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.AuctionSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(auctionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this auction. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param auctionRecord the auction search odrer record
     *  @param auctionRecordType auction record type
     *  @throws org.osid.NullArgumentException
     *          {@code auctionRecord} or
     *          {@code auctionRecordTypeauction} is
     *          {@code null}
     */
            
    protected void addAuctionRecord(org.osid.bidding.records.AuctionSearchOrderRecord auctionSearchOrderRecord, 
                                    org.osid.type.Type auctionRecordType) {

        addRecordType(auctionRecordType);
        this.records.add(auctionSearchOrderRecord);
        
        return;
    }


    protected class OsidTemporalSearchOrder
        extends net.okapia.osid.jamocha.spi.AbstractOsidTemporalSearchOrder
        implements org.osid.OsidTemporalSearchOrder {
    }
}
