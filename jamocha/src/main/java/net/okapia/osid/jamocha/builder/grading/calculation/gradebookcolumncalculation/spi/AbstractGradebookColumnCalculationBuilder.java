//
// AbstractGradebookColumnCalculation.java
//
//     Defines a GradebookColumnCalculation builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation.spi;


/**
 *  Defines a <code>GradebookColumnCalculation</code> builder.
 */

public abstract class AbstractGradebookColumnCalculationBuilder<T extends AbstractGradebookColumnCalculationBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation.GradebookColumnCalculationMiter gradebookColumnCalculation;


    /**
     *  Constructs a new <code>AbstractGradebookColumnCalculationBuilder</code>.
     *
     *  @param gradebookColumnCalculation the gradebook column calculation to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractGradebookColumnCalculationBuilder(net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation.GradebookColumnCalculationMiter gradebookColumnCalculation) {
        super(gradebookColumnCalculation);
        this.gradebookColumnCalculation = gradebookColumnCalculation;
        return;
    }


    /**
     *  Builds the gradebook column calculation.
     *
     *  @return the new gradebook column calculation
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.grading.calculation.GradebookColumnCalculation build() {
        (new net.okapia.osid.jamocha.builder.validator.grading.calculation.gradebookcolumncalculation.GradebookColumnCalculationValidator(getValidations())).validate(this.gradebookColumnCalculation);
        return (new net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation.ImmutableGradebookColumnCalculation(this.gradebookColumnCalculation));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the gradebook column calculation miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation.GradebookColumnCalculationMiter getMiter() {
        return (this.gradebookColumnCalculation);
    }


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    public T gradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        getMiter().setGradebookColumn(gradebookColumn);
        return (self());
    }


    /**
     *  Adds an input gradebook column.
     *
     *  @param gradebookColumn an input gradebook column
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    public T inputGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn) {
        getMiter().addInputGradebookColumn(gradebookColumn);
        return (self());
    }


    /**
     *  Sets all the input gradebook columns.
     *
     *  @param gradebookColumns a collection of input gradebook columns
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumns</code> is <code>null</code>
     */

    public T inputGradebookColumns(java.util.Collection<org.osid.grading.GradebookColumn> gradebookColumns) {
        getMiter().setInputGradebookColumns(gradebookColumns);
        return (self());
    }


    /**
     *  Sets the operation.
     *
     *  @param operation an operation
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>operation</code>
     *          is <code>null</code>
     */

    public T operation(org.osid.grading.calculation.CalculationOperation operation) {
        getMiter().setOperation(operation);
        return (self());
    }


    /**
     *  Sets the tweaked center.
     *
     *  @param center a tweaked center
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>center</code> is
     *          <code>null</code>
     */

    public T tweakedCenter(java.math.BigDecimal center) {
        getMiter().setTweakedCenter(center);
        return (self());
    }


    /**
     *  Sets the tweaked standard deviation.
     *
     *  @param standardDeviation a tweaked standard deviation
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    public T tweakedStandardDeviation(java.math.BigDecimal standardDeviation) {
        getMiter().setTweakedStandardDeviation(standardDeviation);
        return (self());
    }


    /**
     *  Adds a GradebookColumnCalculation record.
     *
     *  @param record a gradebook column calculation record
     *  @param recordType the type of gradebook column calculation record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.grading.calculation.records.GradebookColumnCalculationRecord record, org.osid.type.Type recordType) {
        getMiter().addGradebookColumnCalculationRecord(record, recordType);
        return (self());
    }
}       


