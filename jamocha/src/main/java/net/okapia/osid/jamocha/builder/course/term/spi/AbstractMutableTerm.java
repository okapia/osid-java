//
// AbstractMutableTerm.java
//
//     Defines a mutable Term.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.term.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Term</code>.
 */

public abstract class AbstractMutableTerm
    extends net.okapia.osid.jamocha.course.term.spi.AbstractTerm
    implements org.osid.course.Term,
               net.okapia.osid.jamocha.builder.course.term.TermMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this term. 
     *
     *  @param record term record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addTermRecord(org.osid.course.records.TermRecord record, org.osid.type.Type recordType) {
        super.addTermRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this term.
     *
     *  @param displayName the name for this term
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this term.
     *
     *  @param description the description of this term
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the display label.
     *
     *  @param displayLabel a display label
     *  @throws org.osid.NullArgumentException
     *          <code>displayLabel</code> is <code>null</code>
     */

    @Override
    public void setDisplayLabel(org.osid.locale.DisplayText displayLabel) {
        super.setDisplayLabel(displayLabel);
        return;
    }


    /**
     *  Sets the open date.
     *
     *  @param date an open date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setOpenDate(org.osid.calendaring.DateTime date) {
        super.setOpenDate(date);
        return;
    }


    /**
     *  Sets the registration start.
     *
     *  @param date a registration start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setRegistrationStart(org.osid.calendaring.DateTime date) {
        super.setRegistrationStart(date);
        return;
    }


    /**
     *  Sets the registration end.
     *
     *  @param date a registration end
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setRegistrationEnd(org.osid.calendaring.DateTime date) {
        super.setRegistrationEnd(date);
        return;
    }


    /**
     *  Sets the classes start.
     *
     *  @param date a classes start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setClassesStart(org.osid.calendaring.DateTime date) {
        super.setClassesStart(date);
        return;
    }


    /**
     *  Sets the classes end.
     *
     *  @param date a classes end
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setClassesEnd(org.osid.calendaring.DateTime date) {
        super.setClassesEnd(date);
        return;
    }


    /**
     *  Sets the add date.
     *
     *  @param date the add date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setAddDate(org.osid.calendaring.DateTime date) {
        super.setAddDate(date);
        return;
    }



    /**
     *  Sets the drop date.
     *
     *  @param date the drop date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setDropDate(org.osid.calendaring.DateTime date) {
        super.setDropDate(date);
        return;
    }


    /**
     *  Sets the final exam start.
     *
     *  @param date a final exam start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setFinalExamStart(org.osid.calendaring.DateTime date) {
        super.setFinalExamStart(date);
        return;
    }


    /**
     *  Sets the final exam end.
     *
     *  @param date a final exam end
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setFinalExamEnd(org.osid.calendaring.DateTime date) {
        super.setFinalExamEnd(date);
        return;
    }


    /**
     *  Sets the close date.
     *
     *  @param date a close date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    @Override
    public void setCloseDate(org.osid.calendaring.DateTime date) {
        super.setCloseDate(date);
        return;
    }
}

