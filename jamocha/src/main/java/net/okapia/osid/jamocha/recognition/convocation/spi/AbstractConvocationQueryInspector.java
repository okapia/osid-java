//
// AbstractConvocationQueryInspector.java
//
//     A template for making a ConvocationQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recognition.convocation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for convocations.
 */

public abstract class AbstractConvocationQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQueryInspector
    implements org.osid.recognition.ConvocationQueryInspector {

    private final java.util.Collection<org.osid.recognition.records.ConvocationQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the award <code> Id </code> terms. 
     *
     *  @return the award <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAwardIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the award terms. 
     *
     *  @return the award terms 
     */

    @OSID @Override
    public org.osid.recognition.AwardQueryInspector[] getAwardTerms() {
        return (new org.osid.recognition.AwardQueryInspector[0]);
    }


    /**
     *  Gets the date terms. 
     *
     *  @return the date terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the time period <code> Id </code> terms. 
     *
     *  @return the time period <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTimePeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the time period terms. 
     *
     *  @return the time period terms 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQueryInspector[] getTimePeriodTerms() {
        return (new org.osid.calendaring.TimePeriodQueryInspector[0]);
    }


    /**
     *  Gets the conferral <code> Id </code> terms. 
     *
     *  @return the conferral <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getConferralIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the conferral terms. 
     *
     *  @return the conferral terms 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQueryInspector[] getConferralTerms() {
        return (new org.osid.recognition.ConferralQueryInspector[0]);
    }


    /**
     *  Gets the academy <code> Id </code> terms. 
     *
     *  @return the academy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAcademyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the academy terms. 
     *
     *  @return the academy terms 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQueryInspector[] getAcademyTerms() {
        return (new org.osid.recognition.AcademyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given convocation query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a convocation implementing the requested record.
     *
     *  @param convocationRecordType a convocation record type
     *  @return the convocation query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>convocationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(convocationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.recognition.records.ConvocationQueryInspectorRecord getConvocationQueryInspectorRecord(org.osid.type.Type convocationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recognition.records.ConvocationQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(convocationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(convocationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this convocation query. 
     *
     *  @param convocationQueryInspectorRecord convocation query inspector
     *         record
     *  @param convocationRecordType convocation record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addConvocationQueryInspectorRecord(org.osid.recognition.records.ConvocationQueryInspectorRecord convocationQueryInspectorRecord, 
                                                   org.osid.type.Type convocationRecordType) {

        addRecordType(convocationRecordType);
        nullarg(convocationRecordType, "convocation record type");
        this.records.add(convocationQueryInspectorRecord);        
        return;
    }
}
