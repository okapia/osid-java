//
// AbstractTriggerSearchOdrer.java
//
//     Defines a TriggerSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.trigger.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code TriggerSearchOrder}.
 */

public abstract class AbstractTriggerSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleSearchOrder
    implements org.osid.control.TriggerSearchOrder {

    private final java.util.Collection<org.osid.control.records.TriggerSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by controller. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByController(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a controller search order is available. 
     *
     *  @return <code> true </code> if a controller search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSearchOrder() {
        return (false);
    }


    /**
     *  Gets the controller search order. 
     *
     *  @return the controller search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsControllerSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchOrder getControllerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsControllerSearchOrder() is false");
    }


    /**
     *  Orders the results by ON event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTurnedOn(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by OFF event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTurnedOff(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by changed variable amount event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByChangedVariableAmount(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by exceeds variable amount event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByExceedsVariableAmount(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by deceeds variable amount event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDeceedsVariableAmount(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by state change event listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByChangedDiscreetState(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by state listeners. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDiscreetState(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a state search order is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDiscreetStateSearchOrder() {
        return (false);
    }


    /**
     *  Gets the discreet state search order. 
     *
     *  @return the state search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsDiscreetStateSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getDiscreetStateSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDiscreetStateSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  triggerRecordType a trigger record type 
     *  @return {@code true} if the triggerRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code triggerRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type triggerRecordType) {
        for (org.osid.control.records.TriggerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  triggerRecordType the trigger record type 
     *  @return the trigger search order record
     *  @throws org.osid.NullArgumentException
     *          {@code triggerRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(triggerRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.control.records.TriggerSearchOrderRecord getTriggerSearchOrderRecord(org.osid.type.Type triggerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.TriggerSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(triggerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this trigger. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param triggerRecord the trigger search odrer record
     *  @param triggerRecordType trigger record type
     *  @throws org.osid.NullArgumentException
     *          {@code triggerRecord} or
     *          {@code triggerRecordTypetrigger} is
     *          {@code null}
     */
            
    protected void addTriggerRecord(org.osid.control.records.TriggerSearchOrderRecord triggerSearchOrderRecord, 
                                     org.osid.type.Type triggerRecordType) {

        addRecordType(triggerRecordType);
        this.records.add(triggerSearchOrderRecord);
        
        return;
    }
}
