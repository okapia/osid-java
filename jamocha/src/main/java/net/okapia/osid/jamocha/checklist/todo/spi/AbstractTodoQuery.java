//
// AbstractTodoQuery.java
//
//     A template for making a Todo Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.checklist.todo.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for todos.
 */

public abstract class AbstractTodoQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.checklist.TodoQuery {

    private final java.util.Collection<org.osid.checklist.records.TodoQueryRecord> records = new java.util.ArrayList<>();
    private final OsidContainableQuery containableQuery = new OsidContainableQuery();

    
    /**
     *  Matches completed todos. 
     *
     *  @param  match <code> true </code> for to match completed todos, <code> 
     *          false </code> to match incomplete todos 
     */

    @OSID @Override
    public void matchComplete(boolean match) {
        return;
    }


    /**
     *  Clears the complete terms. 
     */

    @OSID @Override
    public void clearCompleteTerms() {
        return;
    }


    /**
     *  Matches todos of the given priority. 
     *
     *  @param  priorityType a priority type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPriority(org.osid.type.Type priorityType, boolean match) {
        return;
    }


    /**
     *  Matches todos with any priority set. 
     *
     *  @param  match <code> true </code> for to match todos with any 
     *          priority, <code> false </code> to match todos with no priority 
     */

    @OSID @Override
    public void matchAnyPriority(boolean match) {
        return;
    }


    /**
     *  Clears the priority terms. 
     */

    @OSID @Override
    public void clearPriorityTerms() {
        return;
    }


    /**
     *  Matches todos of the given priority or greater. 
     *
     *  @param  priorityType a priority type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMinimumPriority(org.osid.type.Type priorityType, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the minimum priority terms. 
     */

    @OSID @Override
    public void clearMinimumPriorityTerms() {
        return;
    }


    /**
     *  Matches todos with dues dates within the given date range inclusive. 
     *
     *  @param  from starting date 
     *  @param  to ending date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDueDate(org.osid.calendaring.DateTime from, 
                             org.osid.calendaring.DateTime to, boolean match) {
        return;
    }


    /**
     *  Matches todos with any due date. 
     *
     *  @param  match <code> true </code> for to match todos with any due 
     *          date, <code> false </code> to match todos with no due date 
     */

    @OSID @Override
    public void matchAnyDueDate(boolean match) {
        return;
    }


    /**
     *  Clears the due date terms. 
     */

    @OSID @Override
    public void clearDueDateTerms() {
        return;
    }


    /**
     *  Matches a depednency todo. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDependencyId(org.osid.id.Id todoId, boolean match) {
        return;
    }


    /**
     *  Clears the todo <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDependencyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TodoQuery </code> is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDependencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDependencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getDependencyQuery() {
        throw new org.osid.UnimplementedException("supportsDependencyQuery() is false");
    }


    /**
     *  Matches tdos with any dependencies. 
     *
     *  @param  match <code> true </code> for to matchc todos with any 
     *          dependency, <code> false </code> to matchtodos with no 
     *          dependencies 
     */

    @OSID @Override
    public void matchAnyDependency(boolean match) {
        return;
    }


    /**
     *  Clears the dependency terms. 
     */

    @OSID @Override
    public void clearDependencyTerms() {
        return;
    }


    /**
     *  Sets the todo <code> Id </code> for this query to match todos that 
     *  have the specified todo as an ancestor. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorTodoId(org.osid.id.Id todoId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor todo <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorTodoIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TodoQuery </code> is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorTodoQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the todo query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorTodoQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getAncestorTodoQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorTodoQuery() is false");
    }


    /**
     *  Matches todos that have any ancestor. 
     *
     *  @param  match <code> true </code> to match todos with any ancestor, 
     *          <code> false </code> to match root todos 
     */

    @OSID @Override
    public void matchAnyAncestorTodo(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor todo query terms. 
     */

    @OSID @Override
    public void clearAncestorTodoTerms() {
        return;
    }


    /**
     *  Sets the todo <code> Id </code> for this query to match todos that 
     *  have the specified todo as a descendant. 
     *
     *  @param  todoId a todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantTodoId(org.osid.id.Id todoId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant todo <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantTodoIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TodoQuery </code> is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantTodoQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the todo query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantTodoQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getDescendantTodoQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantTodoQuery() is false");
    }


    /**
     *  Matches todos that have any descendant. 
     *
     *  @param  match <code> true </code> to match todos with any descendant, 
     *          <code> false </code> to match leaf todos 
     */

    @OSID @Override
    public void matchAnyDescendantTodo(boolean match) {
        return;
    }


    /**
     *  Clears the descendant todo query terms. 
     */

    @OSID @Override
    public void clearDescendantTodoTerms() {
        return;
    }


    /**
     *  Sets the checklist <code> Id </code> for this query to match todos 
     *  assigned to checklists. 
     *
     *  @param  checklistId a checklist <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchChecklistId(org.osid.id.Id checklistId, boolean match) {
        return;
    }


    /**
     *  Clears the checklist <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearChecklistIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ChecklistQuery </code> is available. 
     *
     *  @return <code> true </code> if a checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistQuery() {
        return (false);
    }


    /**
     *  Gets the query for a checklist query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuery getChecklistQuery() {
        throw new org.osid.UnimplementedException("supportsChecklistQuery() is false");
    }


    /**
     *  Clears the checklist terms. 
     */

    @OSID @Override
    public void clearChecklistTerms() {
        return;
    }


    /**
     *  Match containables that are sequestered.
     *
     *  @param  match <code> true </code> to match any sequestered
     *          containables, <code> false </code> to match non-sequestered
     *          containables
     */

    @OSID @Override
    public void matchSequestered(boolean match) {
        this.containableQuery.matchSequestered(match);
        return;
    }


    /**
     *  Clears the sequestered query terms.
     */

    @OSID @Override
    public void clearSequesteredTerms() {
        this.containableQuery.clearSequesteredTerms();
        return;
    }


    /**
     *  Gets the record corresponding to the given todo query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a todo implementing the requested record.
     *
     *  @param todoRecordType a todo record type
     *  @return the todo query record
     *  @throws org.osid.NullArgumentException
     *          <code>todoRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.records.TodoQueryRecord getTodoQueryRecord(org.osid.type.Type todoRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.records.TodoQueryRecord record : this.records) {
            if (record.implementsRecordType(todoRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoRecordType + " is not supported");
    }


    /**
     *  Adds a record to this todo query. 
     *
     *  @param todoQueryRecord todo query record
     *  @param todoRecordType todo record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTodoQueryRecord(org.osid.checklist.records.TodoQueryRecord todoQueryRecord, 
                                      org.osid.type.Type todoRecordType) {

        addRecordType(todoRecordType);
        nullarg(todoQueryRecord, "todo query record");
        this.records.add(todoQueryRecord);        
        return;
    }


    protected class OsidContainableQuery
        extends net.okapia.osid.jamocha.spi.AbstractOsidContainableQuery
        implements org.osid.OsidContainableQuery {

    }    
}
