//
// AbstractAdapterStatisticLookupSession.java
//
//    A Statistic lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.metering.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Statistic lookup session adapter.
 */

public abstract class AbstractAdapterStatisticLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.metering.StatisticLookupSession {

    private final org.osid.metering.StatisticLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterStatisticLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStatisticLookupSession(org.osid.metering.StatisticLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Utility/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Utility Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getUtilityId() {
        return (this.session.getUtilityId());
    }


    /**
     *  Gets the {@code Utility} associated with this session.
     *
     *  @return the {@code Utility} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Utility getUtility()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getUtility());
    }


    /**
     *  Tests if this user can perform {@code Statistic} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canGetMeterStatistics() {
        return (this.session.canGetMeterStatistics());
    }


    /**
     *  A complete view of the {@code Statistic} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeMeterView() {
        this.session.useComparativeMeterView();
        return;
    }


    /**
     *  A complete view of the {@code Statistic} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryMeterView() {
        this.session.usePlenaryMeterView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include statistics in utilities which are children
     *  of this utility in the utility hierarchy.
     */

    @OSID @Override
    public void useFederatedUtilityView() {
        this.session.useFederatedUtilityView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this utility only.
     */

    @OSID @Override
    public void useIsolatedUtilityView() {
        this.session.useIsolatedUtilityView();
        return;
    }
    

    /**
     *  If the supplied dates are beyond the lifetime of the metered
     *  object, bound the statistics to the dates of the lifetime.
     */

    @OSID @Override
    public void useBoundedMeteredView() {
        this.session.useBoundedMeteredView();
        return;
    }


    /**
     *  If the supplied dates are beyond the lifetime of the metered
     *  object, the readings that do not exist are zero.
     */

    @OSID @Override
    public void useUnboundedMeteredView() {
        this.session.useUnboundedMeteredView();
        return;
    }


    /**
     *  Gets the date range of the available statistics for a metered
     *  object.
     *
     *  @param  meterId the {@code Id} of the {@code Meter} 
     *  @param  meteredObjectId the {@code Id} of the metered object 
     *  @return the date range of the available statistics 
     *  @throws org.osid.NotFoundException no {@code Meter} or metered 
     *          object found with the given {@code Id} or no data 
     *          available 
     *  @throws org.osid.NullArgumentException {@code meterId} or 
     *          {@code meteredObjectId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeInterval getAvailableDateRange(org.osid.id.Id meterId, 
                                                                       org.osid.id.Id meteredObjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAvailableDateRange(meterId, meteredObjectId));
    }

     
    /**
     *  Gets a statistic for the {@code Meter} and metered
     *  object for all available data.
     *
     *  @param  meterId the {@code Id} of the {@code Meter} 
     *  @param  meteredObjectId the {@code Id} of the metered object 
     *  @return the meter stat 
     *  @throws org.osid.NotFoundException no {@code Meter} or metered 
     *          object found with the given {@code Id} 
     *  @throws org.osid.NullArgumentException {@code meterId} or 
     *          {@code meteredObjectId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    public org.osid.metering.Statistic getStatistic(org.osid.id.Id meterId, 
                                                    org.osid.id.Id meteredObjectId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStatistic(meterId, meteredObjectId));
    }


    /**
     *  Gets a statistic within a period of time of the {@code Meter}
     *  and metered object specified by its {@code Id}.
     *
     *  @param  meterId the {@code Id} of the {@code Meter} 
     *  @param  meteredObjectId the {@code Id} of the metered object 
     *  @param  from the start time 
     *  @param  to the end time 
     *  @return the meter stat 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NotFoundException no {@code Meter} or metered 
     *          object found with the given {@code Id} or no data 
     *          available within the given date range 
     *  @throws org.osid.NullArgumentException {@code meterId,
     *          meteredObjectId, from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.metering.Statistic getStatisticByDate(org.osid.id.Id meterId, 
                                                          org.osid.id.Id meteredObjectId, 
                                                          org.osid.calendaring.DateTime from, 
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getStatisticByDate(meterId, meteredObjectId, from, to));
    }


    /**
     *  Gets the statistics for the given metered object {@code
     *  IdList} for all available data. In plenary mode, the returned
     *  list contains all of the readings specified in the {@code Id}
     *  list, in the order of the list, including duplicates, or an
     *  error results if an {@code Id} in the supplied list is not
     *  found or inaccessible.  Otherwise, inaccessible statistics may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  meterId the {@code Id} of the {@code Meter} 
     *  @param  meteredObjectIds the {@code Id} of the metered object 
     *  @return the returned {@code Statistic} list 
     *  @throws org.osid.NotFoundException {@code meterId} or an {@code 
     *          Id was} not found 
     *  @throws org.osid.NullArgumentException {@code meterId} or 
     *          {@code meteredObjects} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.metering.StatisticList getStatistics(org.osid.id.Id meterId, 
                                                         org.osid.id.IdList meteredObjectIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getStatistics(meterId, meteredObjectIds));
    }


    /**
     *  Gets the statistics within a period of time corresponding to
     *  the given metered object {@code IdList.} In plenary mode, the
     *  returned list contains all of the statistics specified in the
     *  {@code Id} list, in the order of the list, including
     *  duplicates, or an error results if an {@code Id} in the
     *  supplied list is not found or inaccessible. Otherwise,
     *  inaccessible statistics or statistics not contained within the
     *  given date range may be omitted from the list and may present
     *  the elements in any order including returning a unique set.
     *
     *  @param  meterId the {@code Id} of the {@code Meter} 
     *  @param  meteredObjectIds the {@code Id} of the metered object 
     *  @param  from the start time 
     *  @param  to the end time 
     *  @return the returned {@code Statistic} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NotFoundException {@code meterId} or an
     *          {@code Id was} not found or no data available within
     *          the given date range
     *  @throws org.osid.NullArgumentException {@code meterId,
     *          meteredObjects, from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.metering.StatisticList getStatisticsByDate(org.osid.id.Id meterId, 
                                                               org.osid.id.IdList meteredObjectIds, 
                                                               org.osid.calendaring.DateTime from, 
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getStatisticsByDate(meterId, meteredObjectIds, from, to));
    }


    /**
     *  Gets the statistics for a metered object within a period of
     *  time at consecutive specified intervals. In plenary mode, the
     *  returned list contains all of the statistics requested or an
     *  error results data in the supplied date range is not found or
     *  inaccessible. Otherwise, inaccessible statistics or statistics
     *  not contained within the given date range may be omitted from
     *  the list and may present the elements in any order including
     *  returning a unique set.
     *
     *  @param  meterId the {@code Id} of the {@code Meter} 
     *  @param meteredObjectId the {@code Id} of the metered object
     *  @param  from the start time 
     *  @param  to the end time 
     *  @param  interval the interval 
     *  @return the returned {@code Statistic} list 
     *  @throws org.osid.InvalidArgumentException {@code to} is less 
     *          than {@code from} 
     *  @throws org.osid.NotFoundException {@code meterId} or an {@code 
     *          Id was} not found or no data available within the given 
     *          date range 
     *  @throws org.osid.NullArgumentException {@code meterId, 
     *          meteredObjectId, from, to} or {@code interval} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.metering.StatisticList getStatisticsAtInterval(org.osid.id.Id meterId, 
                                                                   org.osid.id.Id meteredObjectId, 
                                                                   org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to, 
                                                                   org.osid.calendaring.DateTimeResolution interval)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStatisticsAtInterval(meterId, meteredObjectId, from, to, interval));
    }
}
