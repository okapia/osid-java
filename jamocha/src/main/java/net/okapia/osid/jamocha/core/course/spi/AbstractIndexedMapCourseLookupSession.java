//
// AbstractIndexedMapCourseLookupSession.java
//
//    A simple framework for providing a Course lookup service
//    backed by a fixed collection of courses with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Course lookup service backed by a
 *  fixed collection of courses. The courses are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some courses may be compatible
 *  with more types than are indicated through these course
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Courses</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCourseLookupSession
    extends AbstractMapCourseLookupSession
    implements org.osid.course.CourseLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.Course> coursesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.Course>());
    private final MultiMap<org.osid.type.Type, org.osid.course.Course> coursesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.Course>());


    /**
     *  Makes a <code>Course</code> available in this session.
     *
     *  @param  course a course
     *  @throws org.osid.NullArgumentException <code>course<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCourse(org.osid.course.Course course) {
        super.putCourse(course);

        this.coursesByGenus.put(course.getGenusType(), course);
        
        try (org.osid.type.TypeList types = course.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.coursesByRecord.put(types.getNextType(), course);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a course from this session.
     *
     *  @param courseId the <code>Id</code> of the course
     *  @throws org.osid.NullArgumentException <code>courseId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCourse(org.osid.id.Id courseId) {
        org.osid.course.Course course;
        try {
            course = getCourse(courseId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.coursesByGenus.remove(course.getGenusType());

        try (org.osid.type.TypeList types = course.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.coursesByRecord.remove(types.getNextType(), course);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCourse(courseId);
        return;
    }


    /**
     *  Gets a <code>CourseList</code> corresponding to the given
     *  course genus <code>Type</code> which does not include
     *  courses of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known courses or an error results. Otherwise,
     *  the returned list may contain only those courses that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  courseGenusType a course genus type 
     *  @return the returned <code>Course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByGenusType(org.osid.type.Type courseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.course.ArrayCourseList(this.coursesByGenus.get(courseGenusType)));
    }


    /**
     *  Gets a <code>CourseList</code> containing the given
     *  course record <code>Type</code>. In plenary mode, the
     *  returned list contains all known courses or an error
     *  results. Otherwise, the returned list may contain only those
     *  courses that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  courseRecordType a course record type 
     *  @return the returned <code>course</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseList getCoursesByRecordType(org.osid.type.Type courseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.course.ArrayCourseList(this.coursesByRecord.get(courseRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.coursesByGenus.clear();
        this.coursesByRecord.clear();

        super.close();

        return;
    }
}
