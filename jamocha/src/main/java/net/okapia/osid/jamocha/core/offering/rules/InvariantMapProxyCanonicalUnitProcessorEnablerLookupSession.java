//
// InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession
//
//    Implements a CanonicalUnitProcessorEnabler lookup service backed by a fixed
//    collection of canonicalUnitProcessorEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules;


/**
 *  Implements a CanonicalUnitProcessorEnabler lookup service backed by a fixed
 *  collection of canonical unit processor enablers. The canonical unit processor enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.offering.rules.spi.AbstractMapCanonicalUnitProcessorEnablerLookupSession
    implements org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession} with no
     *  canonical unit processor enablers.
     *
     *  @param catalogue the catalogue
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.proxy.Proxy proxy) {
        setCatalogue(catalogue);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession} with a single
     *  canonical unit processor enabler.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessorEnabler a single canonical unit processor enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnitProcessorEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.rules.CanonicalUnitProcessorEnabler canonicalUnitProcessorEnabler, org.osid.proxy.Proxy proxy) {

        this(catalogue, proxy);
        putCanonicalUnitProcessorEnabler(canonicalUnitProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession} using
     *  an array of canonical unit processor enablers.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessorEnablers an array of canonical unit processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnitProcessorEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.rules.CanonicalUnitProcessorEnabler[] canonicalUnitProcessorEnablers, org.osid.proxy.Proxy proxy) {

        this(catalogue, proxy);
        putCanonicalUnitProcessorEnablers(canonicalUnitProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession} using a
     *  collection of canonical unit processor enablers.
     *
     *  @param catalogue the catalogue
     *  @param canonicalUnitProcessorEnablers a collection of canonical unit processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code catalogue},
     *          {@code canonicalUnitProcessorEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCanonicalUnitProcessorEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                                  java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitProcessorEnabler> canonicalUnitProcessorEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(catalogue, proxy);
        putCanonicalUnitProcessorEnablers(canonicalUnitProcessorEnablers);
        return;
    }
}
