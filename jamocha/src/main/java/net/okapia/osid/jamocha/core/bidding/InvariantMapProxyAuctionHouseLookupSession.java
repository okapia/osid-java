//
// InvariantMapProxyAuctionHouseLookupSession
//
//    Implements an AuctionHouse lookup service backed by a fixed
//    collection of auctionHouses. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding;


/**
 *  Implements an AuctionHouse lookup service backed by a fixed
 *  collection of auction houses. The auction houses are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyAuctionHouseLookupSession
    extends net.okapia.osid.jamocha.core.bidding.spi.AbstractMapAuctionHouseLookupSession
    implements org.osid.bidding.AuctionHouseLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuctionHouseLookupSession} with no
     *  auction houses.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public InvariantMapProxyAuctionHouseLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAuctionHouseLookupSession} with a
     *  single auction house.
     *
     *  @param auctionHouse an single auction house
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouse} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionHouseLookupSession(org.osid.bidding.AuctionHouse auctionHouse, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAuctionHouse(auctionHouse);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyAuctionHouseLookupSession} using
     *  an array of auction houses.
     *
     *  @param auctionHouses an array of auction houses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouses} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionHouseLookupSession(org.osid.bidding.AuctionHouse[] auctionHouses, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAuctionHouses(auctionHouses);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyAuctionHouseLookupSession} using a
     *  collection of auction houses.
     *
     *  @param auctionHouses a collection of auction houses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code auctionHouses} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyAuctionHouseLookupSession(java.util.Collection<? extends org.osid.bidding.AuctionHouse> auctionHouses,
                                                  org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putAuctionHouses(auctionHouses);
        return;
    }
}
