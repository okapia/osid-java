//
// InvariantMapCredentialLookupSession
//
//    Implements a Credential lookup service backed by a fixed collection of
//    credentials.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program;


/**
 *  Implements a Credential lookup service backed by a fixed
 *  collection of credentials. The credentials are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCredentialLookupSession
    extends net.okapia.osid.jamocha.core.course.program.spi.AbstractMapCredentialLookupSession
    implements org.osid.course.program.CredentialLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCredentialLookupSession</code> with no
     *  credentials.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCredentialLookupSession</code> with a single
     *  credential.
     *  
     *  @param courseCatalog the course catalog
     *  @param credential a single credential
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credential} is <code>null</code>
     */

      public InvariantMapCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.program.Credential credential) {
        this(courseCatalog);
        putCredential(credential);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCredentialLookupSession</code> using an array
     *  of credentials.
     *  
     *  @param courseCatalog the course catalog
     *  @param credentials an array of credentials
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentials} is <code>null</code>
     */

      public InvariantMapCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.program.Credential[] credentials) {
        this(courseCatalog);
        putCredentials(credentials);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCredentialLookupSession</code> using a
     *  collection of credentials.
     *
     *  @param courseCatalog the course catalog
     *  @param credentials a collection of credentials
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code credentials} is <code>null</code>
     */

      public InvariantMapCredentialLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.program.Credential> credentials) {
        this(courseCatalog);
        putCredentials(credentials);
        return;
    }
}
