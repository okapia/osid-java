//
// AbstractIndexedMapQueueProcessorLookupSession.java
//
//    A simple framework for providing a QueueProcessor lookup service
//    backed by a fixed collection of queue processors with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a QueueProcessor lookup service backed by a
 *  fixed collection of queue processors. The queue processors are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some queue processors may be compatible
 *  with more types than are indicated through these queue processor
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>QueueProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapQueueProcessorLookupSession
    extends AbstractMapQueueProcessorLookupSession
    implements org.osid.provisioning.rules.QueueProcessorLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.QueueProcessor> queueProcessorsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.QueueProcessor>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.QueueProcessor> queueProcessorsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.QueueProcessor>());


    /**
     *  Makes a <code>QueueProcessor</code> available in this session.
     *
     *  @param  queueProcessor a queue processor
     *  @throws org.osid.NullArgumentException <code>queueProcessor<code> is
     *          <code>null</code>
     */

    @Override
    protected void putQueueProcessor(org.osid.provisioning.rules.QueueProcessor queueProcessor) {
        super.putQueueProcessor(queueProcessor);

        this.queueProcessorsByGenus.put(queueProcessor.getGenusType(), queueProcessor);
        
        try (org.osid.type.TypeList types = queueProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queueProcessorsByRecord.put(types.getNextType(), queueProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a queue processor from this session.
     *
     *  @param queueProcessorId the <code>Id</code> of the queue processor
     *  @throws org.osid.NullArgumentException <code>queueProcessorId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeQueueProcessor(org.osid.id.Id queueProcessorId) {
        org.osid.provisioning.rules.QueueProcessor queueProcessor;
        try {
            queueProcessor = getQueueProcessor(queueProcessorId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.queueProcessorsByGenus.remove(queueProcessor.getGenusType());

        try (org.osid.type.TypeList types = queueProcessor.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queueProcessorsByRecord.remove(types.getNextType(), queueProcessor);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeQueueProcessor(queueProcessorId);
        return;
    }


    /**
     *  Gets a <code>QueueProcessorList</code> corresponding to the given
     *  queue processor genus <code>Type</code> which does not include
     *  queue processors of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known queue processors or an error results. Otherwise,
     *  the returned list may contain only those queue processors that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  queueProcessorGenusType a queue processor genus type 
     *  @return the returned <code>QueueProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByGenusType(org.osid.type.Type queueProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueprocessor.ArrayQueueProcessorList(this.queueProcessorsByGenus.get(queueProcessorGenusType)));
    }


    /**
     *  Gets a <code>QueueProcessorList</code> containing the given
     *  queue processor record <code>Type</code>. In plenary mode, the
     *  returned list contains all known queue processors or an error
     *  results. Otherwise, the returned list may contain only those
     *  queue processors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  queueProcessorRecordType a queue processor record type 
     *  @return the returned <code>queueProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByRecordType(org.osid.type.Type queueProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueprocessor.ArrayQueueProcessorList(this.queueProcessorsByRecord.get(queueProcessorRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.queueProcessorsByGenus.clear();
        this.queueProcessorsByRecord.clear();

        super.close();

        return;
    }
}
