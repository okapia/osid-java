//
// AbstractDispatchQuery.java
//
//     A template for making a Dispatch Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.dispatch.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for dispatches.
 */

public abstract class AbstractDispatchQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.subscription.DispatchQuery {

    private final java.util.Collection<org.osid.subscription.records.DispatchQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches an address genus <code> Type </code> accepted by a dispatch. 
     *
     *  @param  addressGenusType an address genus <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressGenusType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAddressGenusType(org.osid.type.Type addressGenusType, 
                                      boolean match) {
        return;
    }


    /**
     *  Matches dispatches with any address genus type. 
     *
     *  @param  match <code> true </code> to match dispatches with any addres 
     *          sgenus type, <code> false </code> to match dispatches with no 
     *          address genus types 
     */

    @OSID @Override
    public void matchAnyAddressGenusType(boolean match) {
        return;
    }


    /**
     *  Clears the address genus <code> Type </code> terms. 
     */

    @OSID @Override
    public void clearAddressGenusTypeTerms() {
        return;
    }


    /**
     *  Sets the subscription <code> Id </code> for this query to match 
     *  subscriptions assigned to dispatches. 
     *
     *  @param  subscriptionId a subscription <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subscriptionId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchSubscriptionId(org.osid.id.Id subscriptionId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the subscription <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubscriptionIdTerms() {
        return;
    }


    /**
     *  Tests if a subscription query is available. 
     *
     *  @return <code> true </code> if a subscription query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dispatch. 
     *
     *  @return the subscription query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuery getSubscriptionQuery() {
        throw new org.osid.UnimplementedException("supportsSubscriptionQuery() is false");
    }


    /**
     *  Matches dispatches with any subscription. 
     *
     *  @param  match <code> true </code> to match dispatches with any 
     *          subscription, <code> false </code> to match dispatches with no 
     *          subscriptions 
     */

    @OSID @Override
    public void matchAnySubscription(boolean match) {
        return;
    }


    /**
     *  Clears the subscription terms. 
     */

    @OSID @Override
    public void clearSubscriptionTerms() {
        return;
    }


    /**
     *  Sets the dispatch <code> Id </code> for this query to match 
     *  subscriptions assigned to publishers. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPublisherId(org.osid.id.Id publisherId, boolean match) {
        return;
    }


    /**
     *  Clears the publisher <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPublisherIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsPublisherQuery() is false");
    }


    /**
     *  Clears the publisher terms. 
     */

    @OSID @Override
    public void clearPublisherTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given dispatch query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a dispatch implementing the requested record.
     *
     *  @param dispatchRecordType a dispatch record type
     *  @return the dispatch query record
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dispatchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.DispatchQueryRecord getDispatchQueryRecord(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.DispatchQueryRecord record : this.records) {
            if (record.implementsRecordType(dispatchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dispatchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this dispatch query. 
     *
     *  @param dispatchQueryRecord dispatch query record
     *  @param dispatchRecordType dispatch record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addDispatchQueryRecord(org.osid.subscription.records.DispatchQueryRecord dispatchQueryRecord, 
                                          org.osid.type.Type dispatchRecordType) {

        addRecordType(dispatchRecordType);
        nullarg(dispatchQueryRecord, "dispatch query record");
        this.records.add(dispatchQueryRecord);        
        return;
    }
}
