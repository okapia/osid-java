//
// CommentMiter.java
//
//     Defines a Comment miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.commenting.comment;


/**
 *  Defines a <code>Comment</code> miter for use with the builders.
 */

public interface CommentMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.commenting.Comment {


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    public void setReferenceId(org.osid.id.Id referenceId);


    /**
     *  Sets the commentor.
     *
     *  @param commentor a commentor
     *  @throws org.osid.NullArgumentException <code>commentor</code>
     *          is <code>null</code>
     */

    public void setCommentor(org.osid.resource.Resource commentor);


    /**
     *  Sets the commenting agent.
     *
     *  @param agent a commenting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    public void setCommentingAgent(org.osid.authentication.Agent agent);


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @throws org.osid.NullArgumentException <code>text</code> is
     *          <code>null</code>
     */

    public void setText(org.osid.locale.DisplayText text);


    /**
     *  Sets the rating.
     *
     *  @param rating a rating
     *  @throws org.osid.NullArgumentException <code>rating</code> is
     *          <code>null</code>
     */

    public void setRating(org.osid.grading.Grade rating);


    /**
     *  Adds a Comment record.
     *
     *  @param record a comment record
     *  @param recordType the type of comment record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCommentRecord(org.osid.commenting.records.CommentRecord record, org.osid.type.Type recordType);
}       


