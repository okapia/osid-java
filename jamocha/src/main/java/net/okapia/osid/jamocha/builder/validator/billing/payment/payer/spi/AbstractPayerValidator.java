//
// AbstractPayerValidator.java
//
//     Validates a Payer.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.billing.payment.payer.spi;


/**
 *  Validates a Payer.
 */

public abstract class AbstractPayerValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractTemporalOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractPayerValidator</code>.
     */

    protected AbstractPayerValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractPayerValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractPayerValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Payer.
     *
     *  @param payer a payer to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>payer</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.billing.payment.Payer payer) {
        super.validate(payer);

        testNestedObject(payer, "getResource");

        testConditionalMethod(payer, "getCustomerId", payer.hasCustomer(), "hasCustomer()");
        testConditionalMethod(payer, "getCustomer", payer.hasCustomer(), "hasCustomer()");

        if (payer.hasCustomer()) {
            testNestedObject(payer, "getCustomer");
        }

        testConditionalMethod(payer, "getCreditCardNumber", payer.hasCreditCard(), "hasCreditCard()");
        testConditionalMethod(payer, "getCreditCardExpiration", payer.hasCreditCard(), "hasCreditCard()");
        testConditionalMethod(payer, "getCreditCardCode", payer.hasCreditCard(), "hasCreditCard()");
        testConditionalMethod(payer, "getBankRoutingNumber", payer.hasBankAccount(), "hasBankAccount()");
        testConditionalMethod(payer, "getBankAccountNumber", payer.hasBankAccount(), "hasBankAccount()");
        
        if (payer.usesActivity() && (payer.usesCash() || payer.hasCreditCard() || payer.hasBankAccount())) {
            throw new org.osid.BadLogicException("payer has multiple accounts");
        }

        if (payer.usesCash() && (payer.usesActivity() || payer.hasCreditCard() || payer.hasBankAccount())) {
            throw new org.osid.BadLogicException("payer has multiple accounts");
        }

        if (payer.hasCreditCard() && (payer.usesActivity() || payer.usesCash() || payer.hasBankAccount())) {
            throw new org.osid.BadLogicException("payer has multiple accounts");
        }

        if (payer.hasBankAccount() && (payer.usesActivity() || payer.usesCash() || payer.hasCreditCard())) {
            throw new org.osid.BadLogicException("payer has multiple accounts");
        }

        return;
    }
}
