//
// AbstractCourseChronicleProxyManager.java
//
//     An adapter for a CourseChronicleProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseChronicleProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseChronicleProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.course.chronicle.CourseChronicleProxyManager>
    implements org.osid.course.chronicle.CourseChronicleProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseChronicleProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseChronicleProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseChronicleProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseChronicleProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if retrieving an academic record is supported. 
     *
     *  @return <code> true </code> if academic record is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademicRecord() {
        return (getAdapteeManager().supportsAcademicRecord());
    }


    /**
     *  Tests if looking up program entries is supported. 
     *
     *  @return <code> true </code> if program entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryLookup() {
        return (getAdapteeManager().supportsProgramEntryLookup());
    }


    /**
     *  Tests if querying program entries is supported. 
     *
     *  @return <code> true </code> if program entry query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryQuery() {
        return (getAdapteeManager().supportsProgramEntryQuery());
    }


    /**
     *  Tests if searching program entries is supported. 
     *
     *  @return <code> true </code> if program entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntrySearch() {
        return (getAdapteeManager().supportsProgramEntrySearch());
    }


    /**
     *  Tests if program entry administrative service is supported. 
     *
     *  @return <code> true </code> if program entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryAdmin() {
        return (getAdapteeManager().supportsProgramEntryAdmin());
    }


    /**
     *  Tests if a program entry notification service is supported. 
     *
     *  @return <code> true </code> if program entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryNotification() {
        return (getAdapteeManager().supportsProgramEntryNotification());
    }


    /**
     *  Tests if a program entry cataloging service is supported. 
     *
     *  @return <code> true </code> if program entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryCourseCatalog() {
        return (getAdapteeManager().supportsProgramEntryCourseCatalog());
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps program entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntryCourseCatalogAssignment() {
        return (getAdapteeManager().supportsProgramEntryCourseCatalogAssignment());
    }


    /**
     *  Tests if a program entry smart course catalog session is available. 
     *
     *  @return <code> true </code> if a program entry smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramEntrySmartCourseCatalog() {
        return (getAdapteeManager().supportsProgramEntrySmartCourseCatalog());
    }


    /**
     *  Tests if looking up course entries is supported. 
     *
     *  @return <code> true </code> if course entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryLookup() {
        return (getAdapteeManager().supportsCourseEntryLookup());
    }


    /**
     *  Tests if querying course entries is supported. 
     *
     *  @return <code> true </code> if course entry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryQuery() {
        return (getAdapteeManager().supportsCourseEntryQuery());
    }


    /**
     *  Tests if searching course entries is supported. 
     *
     *  @return <code> true </code> if course entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntrySearch() {
        return (getAdapteeManager().supportsCourseEntrySearch());
    }


    /**
     *  Tests if course entry administrative service is supported. 
     *
     *  @return <code> true </code> if course entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryAdmin() {
        return (getAdapteeManager().supportsCourseEntryAdmin());
    }


    /**
     *  Tests if a course entry notification service is supported. 
     *
     *  @return <code> true </code> if course entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryNotification() {
        return (getAdapteeManager().supportsCourseEntryNotification());
    }


    /**
     *  Tests if a course entry cataloging service is supported. 
     *
     *  @return <code> true </code> if course entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryCourseCatalog() {
        return (getAdapteeManager().supportsCourseEntryCourseCatalog());
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps course entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntryCourseCatalogAssignment() {
        return (getAdapteeManager().supportsCourseEntryCourseCatalogAssignment());
    }


    /**
     *  Tests if a course entry smart course catalog session is available. 
     *
     *  @return <code> true </code> if a course entry smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseEntrySmartCourseCatalog() {
        return (getAdapteeManager().supportsCourseEntrySmartCourseCatalog());
    }


    /**
     *  Tests if looking up credential entries is supported. 
     *
     *  @return <code> true </code> if credential entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryLookup() {
        return (getAdapteeManager().supportsCredentialEntryLookup());
    }


    /**
     *  Tests if querying credential entries is supported. 
     *
     *  @return <code> true </code> if credential entry query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryQuery() {
        return (getAdapteeManager().supportsCredentialEntryQuery());
    }


    /**
     *  Tests if searching credential entries is supported. 
     *
     *  @return <code> true </code> if credential entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntrySearch() {
        return (getAdapteeManager().supportsCredentialEntrySearch());
    }


    /**
     *  Tests if credential entry administrative service is supported. 
     *
     *  @return <code> true </code> if credential entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryAdmin() {
        return (getAdapteeManager().supportsCredentialEntryAdmin());
    }


    /**
     *  Tests if a credential entry notification service is supported. 
     *
     *  @return <code> true </code> if credential entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryNotification() {
        return (getAdapteeManager().supportsCredentialEntryNotification());
    }


    /**
     *  Tests if a credential entry cataloging service is supported. 
     *
     *  @return <code> true </code> if credential entry cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryCourseCatalog() {
        return (getAdapteeManager().supportsCredentialEntryCourseCatalog());
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps credential entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntryCourseCatalogAssignment() {
        return (getAdapteeManager().supportsCredentialEntryCourseCatalogAssignment());
    }


    /**
     *  Tests if a credential entry smart course catalog session is available. 
     *
     *  @return <code> true </code> if a credential entry smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialEntrySmartCourseCatalog() {
        return (getAdapteeManager().supportsCredentialEntrySmartCourseCatalog());
    }


    /**
     *  Tests if looking up assessment entries is supported. 
     *
     *  @return <code> true </code> if assessment entry lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryLookup() {
        return (getAdapteeManager().supportsAssessmentEntryLookup());
    }


    /**
     *  Tests if querying assessment entries is supported. 
     *
     *  @return <code> true </code> if assessment entry query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryQuery() {
        return (getAdapteeManager().supportsAssessmentEntryQuery());
    }


    /**
     *  Tests if searching assessment entries is supported. 
     *
     *  @return <code> true </code> if assessment entry search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntrySearch() {
        return (getAdapteeManager().supportsAssessmentEntrySearch());
    }


    /**
     *  Tests if assessment entry administrative service is supported. 
     *
     *  @return <code> true </code> if assessment entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryAdmin() {
        return (getAdapteeManager().supportsAssessmentEntryAdmin());
    }


    /**
     *  Tests if an assessment entry notification service is supported. 
     *
     *  @return <code> true </code> if assessment entry notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryNotification() {
        return (getAdapteeManager().supportsAssessmentEntryNotification());
    }


    /**
     *  Tests if an assessment entry cataloging service is supported. 
     *
     *  @return <code> true </code> if assessment entry cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryCourseCatalog() {
        return (getAdapteeManager().supportsAssessmentEntryCourseCatalog());
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps assessment entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryCourseCatalogAssignment() {
        return (getAdapteeManager().supportsAssessmentEntryCourseCatalogAssignment());
    }


    /**
     *  Tests if an assessment entry smart course catalog session is 
     *  available. 
     *
     *  @return <code> true </code> if an assessment entry smart course 
     *          catalog session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentEntrySmartCourseCatalog() {
        return (getAdapteeManager().supportsAssessmentEntrySmartCourseCatalog());
    }


    /**
     *  Tests if looking up award entries is supported. 
     *
     *  @return <code> true </code> if award entry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryLookup() {
        return (getAdapteeManager().supportsAwardEntryLookup());
    }


    /**
     *  Tests if querying award entries is supported. 
     *
     *  @return <code> true </code> if award entry query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryQuery() {
        return (getAdapteeManager().supportsAwardEntryQuery());
    }


    /**
     *  Tests if searching award entries is supported. 
     *
     *  @return <code> true </code> if award entry search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntrySearch() {
        return (getAdapteeManager().supportsAwardEntrySearch());
    }


    /**
     *  Tests if award entry administrative service is supported. 
     *
     *  @return <code> true </code> if award entry administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryAdmin() {
        return (getAdapteeManager().supportsAwardEntryAdmin());
    }


    /**
     *  Tests if an award entry notification service is supported. 
     *
     *  @return <code> true </code> if award entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryNotification() {
        return (getAdapteeManager().supportsAwardEntryNotification());
    }


    /**
     *  Tests if an award entry cataloging service is supported. 
     *
     *  @return <code> true </code> if award entry cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryCourseCatalog() {
        return (getAdapteeManager().supportsAwardEntryCourseCatalog());
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps award entries to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntryCourseCatalogAssignment() {
        return (getAdapteeManager().supportsAwardEntryCourseCatalogAssignment());
    }


    /**
     *  Tests if an award entry smart course catalog session is available. 
     *
     *  @return <code> true </code> if an award entry smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardEntrySmartCourseCatalog() {
        return (getAdapteeManager().supportsAwardEntrySmartCourseCatalog());
    }


    /**
     *  Tests if a course chronicle batch service is available. 
     *
     *  @return <code> true </code> if a course chronicle batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseChronicalBatch() {
        return (getAdapteeManager().supportsCourseChronicalBatch());
    }


    /**
     *  Gets the supported <code> ProgramEntry </code> record types. 
     *
     *  @return a list containing the supported <code> ProgramEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramEntryRecordTypes() {
        return (getAdapteeManager().getProgramEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> ProgramEntry </code> record type is 
     *  supported. 
     *
     *  @param  programEntryRecordType a <code> Type </code> indicating a 
     *          <code> ProgramEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> programEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramEntryRecordType(org.osid.type.Type programEntryRecordType) {
        return (getAdapteeManager().supportsProgramEntryRecordType(programEntryRecordType));
    }


    /**
     *  Gets the supported <code> ProgramEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> ProgramEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramEntrySearchRecordTypes() {
        return (getAdapteeManager().getProgramEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ProgramEntry </code> search record type is 
     *  supported. 
     *
     *  @param  programEntrySearchRecordType a <code> Type </code> indicating 
     *          a <code> ProgramEntry </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          programEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramEntrySearchRecordType(org.osid.type.Type programEntrySearchRecordType) {
        return (getAdapteeManager().supportsProgramEntrySearchRecordType(programEntrySearchRecordType));
    }


    /**
     *  Gets the supported <code> CourseEntry </code> record types. 
     *
     *  @return a list containing the supported <code> CourseEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseEntryRecordTypes() {
        return (getAdapteeManager().getCourseEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> CourseEntry </code> record type is 
     *  supported. 
     *
     *  @param  courseEntryRecordType a <code> Type </code> indicating a 
     *          <code> CourseEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseEntryRecordType(org.osid.type.Type courseEntryRecordType) {
        return (getAdapteeManager().supportsCourseEntryRecordType(courseEntryRecordType));
    }


    /**
     *  Gets the supported <code> CourseEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> CourseEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseEntrySearchRecordTypes() {
        return (getAdapteeManager().getCourseEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CourseEntry </code> search record type is 
     *  supported. 
     *
     *  @param  courseEntrySearchRecordType a <code> Type </code> indicating a 
     *          <code> CourseEntry </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          courseEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseEntrySearchRecordType(org.osid.type.Type courseEntrySearchRecordType) {
        return (getAdapteeManager().supportsCourseEntrySearchRecordType(courseEntrySearchRecordType));
    }


    /**
     *  Gets the supported <code> CredentialEntry </code> record types. 
     *
     *  @return a list containing the supported <code> CredentialEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialEntryRecordTypes() {
        return (getAdapteeManager().getCredentialEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> CredentialEntry </code> record type is 
     *  supported. 
     *
     *  @param  credentialEntryRecordType a <code> Type </code> indicating a 
     *          <code> CredentialEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialEntryRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialEntryRecordType(org.osid.type.Type credentialEntryRecordType) {
        return (getAdapteeManager().supportsCredentialEntryRecordType(credentialEntryRecordType));
    }


    /**
     *  Gets the supported <code> CredentialEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> CredentialEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialEntrySearchRecordTypes() {
        return (getAdapteeManager().getCredentialEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CredentialEntry </code> search record type 
     *  is supported. 
     *
     *  @param  credentialEntrySearchRecordType a <code> Type </code> 
     *          indicating a <code> CredentialEntry </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialEntrySearchRecordType(org.osid.type.Type credentialEntrySearchRecordType) {
        return (getAdapteeManager().supportsCredentialEntrySearchRecordType(credentialEntrySearchRecordType));
    }


    /**
     *  Gets the supported <code> AssessmentEntry </code> record types. 
     *
     *  @return a list containing the supported <code> AssessmentEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentEntryRecordTypes() {
        return (getAdapteeManager().getAssessmentEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> AssessmentEntry </code> record type is 
     *  supported. 
     *
     *  @param  assessmentEntryRecordType an <code> Type </code> indicating an 
     *          <code> AssessmentEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentEntryRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentEntryRecordType(org.osid.type.Type assessmentEntryRecordType) {
        return (getAdapteeManager().supportsAssessmentEntryRecordType(assessmentEntryRecordType));
    }


    /**
     *  Gets the supported <code> AssessmentEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> AssessmentEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentEntrySearchRecordTypes() {
        return (getAdapteeManager().getAssessmentEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AssessmentEntry </code> search record type 
     *  is supported. 
     *
     *  @param  assessmentEntrySearchRecordType an <code> Type </code> 
     *          indicating an <code> AssessmentEntry </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentEntrySearchRecordType(org.osid.type.Type assessmentEntrySearchRecordType) {
        return (getAdapteeManager().supportsAssessmentEntrySearchRecordType(assessmentEntrySearchRecordType));
    }


    /**
     *  Gets the supported <code> AwardEntry </code> record types. 
     *
     *  @return a list containing the supported <code> AwardEntry </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardEntryRecordTypes() {
        return (getAdapteeManager().getAwardEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> AwardEntry </code> record type is supported. 
     *
     *  @param  awardEntryRecordType an <code> Type </code> indicating an 
     *          <code> AwardEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> awardEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardEntryRecordType(org.osid.type.Type awardEntryRecordType) {
        return (getAdapteeManager().supportsAwardEntryRecordType(awardEntryRecordType));
    }


    /**
     *  Gets the supported <code> AwardEntry </code> search record types. 
     *
     *  @return a list containing the supported <code> AwardEntry </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardEntrySearchRecordTypes() {
        return (getAdapteeManager().getAwardEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AwardEntry </code> search record type is 
     *  supported. 
     *
     *  @param  awardEntrySearchRecordType an <code> Type </code> indicating 
     *          an <code> AwardEntry </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          awardEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardEntrySearchRecordType(org.osid.type.Type awardEntrySearchRecordType) {
        return (getAdapteeManager().supportsAwardEntrySearchRecordType(awardEntrySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academic 
     *  record service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AcademicRecordSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademicRecord() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AcademicRecordSession getAcademicRecordSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcademicRecordSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academic 
     *  record service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> AcademicRecordSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademicRecord() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AcademicRecordSession getAcademicRecordSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAcademicRecordSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryLookupSession getProgramEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryLookupSession getProgramEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryLookupSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryQuerySession getProgramEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryQuerySession getProgramEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryQuerySessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntrySearchSession getProgramEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntrySearchSession getProgramEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntrySearchSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryAdminSession getProgramEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryAdminSession getProgramEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  notification service. 
     *
     *  @param  programEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> programEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryNotificationSession getProgramEntryNotificationSession(org.osid.course.chronicle.ProgramEntryReceiver programEntryReceiver, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryNotificationSession(programEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  notification service for the given course catalog. 
     *
     *  @param  programEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> programEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryNotificationSession getProgramEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.ProgramEntryReceiver programEntryReceiver, 
                                                                                                                        org.osid.id.Id courseCatalogId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryNotificationSessionForCourseCatalog(programEntryReceiver, courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup program entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryCourseCatalogSession getProgramEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryCourseCatalogSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning program 
     *  entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntryCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryCourseCatalogAssignmentSession getProgramEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntryCourseCatalogAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program entry 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntrySmartCourseCatalogSession getProgramEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProgramEntrySmartCourseCatalogSession(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryLookupSession getCourseEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryLookupSession getCourseEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryLookupSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryQuerySession getCourseEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryQuerySession getCourseEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryQuerySessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntrySearchSession getCourseEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntrySearchSession getCourseEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntrySearchSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryAdminSession getCourseEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryAdminSession getCourseEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  notification service. 
     *
     *  @param  courseEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryNotificationSession getCourseEntryNotificationSession(org.osid.course.chronicle.CourseEntryReceiver courseEntryReceiver, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryNotificationSession(courseEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  notification service for the given course catalog. 
     *
     *  @param  courseEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryNotificationSession getCourseEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.CourseEntryReceiver courseEntryReceiver, 
                                                                                                                      org.osid.id.Id courseCatalogId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryNotificationSessionForCourseCatalog(courseEntryReceiver, courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup course entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryCourseCatalogSession getCourseEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryCourseCatalogSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning course 
     *  entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntryCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryCourseCatalogAssignmentSession getCourseEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntryCourseCatalogAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course entry 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CourseEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntrySmartCourseCatalogSession getCourseEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseEntrySmartCourseCatalogSession(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryLookupSession getCredentialEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryLookupSession getCredentialEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryLookupSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryQuerySession getCredentialEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryQuerySession getCredentialEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryQuerySessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntrySearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntrySearchSession getCredentialEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntrySearchSession getCredentialEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntrySearchSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryAdminSession getCredentialEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryAdminSession getCredentialEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry notification service. 
     *
     *  @param  credentialEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> credentialEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryNotificationSession getCredentialEntryNotificationSession(org.osid.course.chronicle.CredentialEntryReceiver credentialEntryReceiver, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryNotificationSession(credentialEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry notification service for the given course catalog. 
     *
     *  @param  credentialEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> credentialEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryNotificationSession getCredentialEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.CredentialEntryReceiver credentialEntryReceiver, 
                                                                                                                              org.osid.id.Id courseCatalogId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryNotificationSessionForCourseCatalog(credentialEntryReceiver, courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup credential entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryCourseCatalogSession getCredentialEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryCourseCatalogSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  credential entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntryCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryCourseCatalogAssignmentSession getCredentialEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntryCourseCatalogAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  entry smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntrySmartCourseCatalogSession getCredentialEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCredentialEntrySmartCourseCatalogSession(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryLookupSession getAssessmentEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryLookupSession getAssessmentEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryLookupSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryQuerySession getAssessmentEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryQuerySession getAssessmentEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryQuerySessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntrySearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntrySearchSession getAssessmentEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntrySearchSession getAssessmentEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntrySearchSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryAdminSession getAssessmentEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryAdminSession getAssessmentEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry notification service. 
     *
     *  @param  assessmentEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryNotificationSession getAssessmentEntryNotificationSession(org.osid.course.chronicle.AssessmentEntryReceiver assessmentEntryReceiver, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryNotificationSession(assessmentEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry notification service for the given course catalog. 
     *
     *  @param  assessmentEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryNotificationSession getAssessmentEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.AssessmentEntryReceiver assessmentEntryReceiver, 
                                                                                                                              org.osid.id.Id courseCatalogId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryNotificationSessionForCourseCatalog(assessmentEntryReceiver, courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup assessment entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryCourseCatalogSession getAssessmentEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryCourseCatalogSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  assessment entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntryCourseCatalogAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntryCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntryCourseCatalogAssignmentSession getAssessmentEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntryCourseCatalogAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  entry smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AssessmentEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AssessmentEntrySmartCourseCatalogSession getAssessmentEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentEntrySmartCourseCatalogSession(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryLookupSession getAwardEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryLookupSession getAwardEntryLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryLookupSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryQuerySession getAwardEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryQuerySession getAwardEntryQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryQuerySessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntrySearchSession getAwardEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntrySearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntrySearchSession getAwardEntrySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntrySearchSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryAdminSession getAwardEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryAdminSession getAwardEntryAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryAdminSessionForCourseCatalog(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  notification service. 
     *
     *  @param  awardEntryReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> awardEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryNotificationSession getAwardEntryNotificationSession(org.osid.course.chronicle.AwardEntryReceiver awardEntryReceiver, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryNotificationSession(awardEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  notification service for the given course catalog. 
     *
     *  @param  awardEntryReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> awardEntryReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryNotificationSession getAwardEntryNotificationSessionForCourseCatalog(org.osid.course.chronicle.AwardEntryReceiver awardEntryReceiver, 
                                                                                                                    org.osid.id.Id courseCatalogId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryNotificationSessionForCourseCatalog(awardEntryReceiver, courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup award entry/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryCourseCatalogSession getAwardEntryCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryCourseCatalogSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning award 
     *  entries to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return an <code> AwardEntryCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntryCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntryCourseCatalogAssignmentSession getAwardEntryCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntryCourseCatalogAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award entry 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> AwardEntrySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardEntrySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.AwardEntrySmartCourseCatalogSession getAwardEntrySmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardEntrySmartCourseCatalogSession(courseCatalogId, proxy));
    }


    /**
     *  Gets the <code> CourseChronicleBatchProxyManager. </code> 
     *
     *  @return a <code> CourseChronicleBatcProxyhManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseChronicleBatchProxyManager() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.batch.CourseChronicleBatchProxyManager getCourseChronicleBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseChronicleBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
