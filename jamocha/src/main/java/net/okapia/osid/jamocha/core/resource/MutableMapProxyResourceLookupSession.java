//
// MutableMapProxyResourceLookupSession
//
//    Implements a Resource lookup service backed by a collection of
//    resources that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource;


/**
 *  Implements a Resource lookup service backed by a collection of
 *  resources. The resources are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of resources can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyResourceLookupSession
    extends net.okapia.osid.jamocha.core.resource.spi.AbstractMapResourceLookupSession
    implements org.osid.resource.ResourceLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyResourceLookupSession}
     *  with no resources.
     *
     *  @param bin the bin
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyResourceLookupSession(org.osid.resource.Bin bin,
                                                  org.osid.proxy.Proxy proxy) {
        setBin(bin);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyResourceLookupSession} with a
     *  single resource.
     *
     *  @param bin the bin
     *  @param resource a resource
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code resource}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyResourceLookupSession(org.osid.resource.Bin bin,
                                                org.osid.resource.Resource resource, org.osid.proxy.Proxy proxy) {
        this(bin, proxy);
        putResource(resource);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyResourceLookupSession} using an
     *  array of resources.
     *
     *  @param bin the bin
     *  @param resources an array of resources
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code resources}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyResourceLookupSession(org.osid.resource.Bin bin,
                                                org.osid.resource.Resource[] resources, org.osid.proxy.Proxy proxy) {
        this(bin, proxy);
        putResources(resources);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyResourceLookupSession} using a
     *  collection of resources.
     *
     *  @param bin the bin
     *  @param resources a collection of resources
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bin},
     *          {@code resources}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyResourceLookupSession(org.osid.resource.Bin bin,
                                                java.util.Collection<? extends org.osid.resource.Resource> resources,
                                                org.osid.proxy.Proxy proxy) {
   
        this(bin, proxy);
        setSessionProxy(proxy);
        putResources(resources);
        return;
    }

    
    /**
     *  Makes a {@code Resource} available in this session.
     *
     *  @param resource an resource
     *  @throws org.osid.NullArgumentException {@code resource{@code 
     *          is {@code null}
     */

    @Override
    public void putResource(org.osid.resource.Resource resource) {
        super.putResource(resource);
        return;
    }


    /**
     *  Makes an array of resources available in this session.
     *
     *  @param resources an array of resources
     *  @throws org.osid.NullArgumentException {@code resources{@code 
     *          is {@code null}
     */

    @Override
    public void putResources(org.osid.resource.Resource[] resources) {
        super.putResources(resources);
        return;
    }


    /**
     *  Makes collection of resources available in this session.
     *
     *  @param resources
     *  @throws org.osid.NullArgumentException {@code resource{@code 
     *          is {@code null}
     */

    @Override
    public void putResources(java.util.Collection<? extends org.osid.resource.Resource> resources) {
        super.putResources(resources);
        return;
    }


    /**
     *  Removes a Resource from this session.
     *
     *  @param resourceId the {@code Id} of the resource
     *  @throws org.osid.NullArgumentException {@code resourceId{@code  is
     *          {@code null}
     */

    @Override
    public void removeResource(org.osid.id.Id resourceId) {
        super.removeResource(resourceId);
        return;
    }    
}
