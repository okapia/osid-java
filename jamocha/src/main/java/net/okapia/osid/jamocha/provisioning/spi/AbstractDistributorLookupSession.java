//
// AbstractDistributorLookupSession.java
//
//    A starter implementation framework for providing a Distributor
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Distributor
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getDistributors(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractDistributorLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.provisioning.DistributorLookupSession {

    private boolean pedantic = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();
    


    /**
     *  Tests if this user can perform <code>Distributor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDistributors() {
        return (true);
    }


    /**
     *  A complete view of the <code>Distributor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDistributorView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Distributor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDistributorView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Distributor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Distributor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Distributor</code> and
     *  retained for compatibility.
     *
     *  @param  distributorId <code>Id</code> of the
     *          <code>Distributor</code>
     *  @return the distributor
     *  @throws org.osid.NotFoundException <code>distributorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>distributorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.provisioning.DistributorList distributors = getDistributors()) {
            while (distributors.hasNext()) {
                org.osid.provisioning.Distributor distributor = distributors.getNextDistributor();
                if (distributor.getId().equals(distributorId)) {
                    return (distributor);
                }
            }
        } 

        throw new org.osid.NotFoundException(distributorId + " not found");
    }


    /**
     *  Gets a <code>DistributorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  distributors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Distributors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getDistributors()</code>.
     *
     *  @param  distributorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Distributor</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>distributorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByIds(org.osid.id.IdList distributorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.provisioning.Distributor> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = distributorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getDistributor(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("distributor " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.provisioning.distributor.LinkedDistributorList(ret));
    }


    /**
     *  Gets a <code>DistributorList</code> corresponding to the given
     *  distributor genus <code>Type</code> which does not include
     *  distributors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getDistributors()</code>.
     *
     *  @param  distributorGenusType a distributor genus type 
     *  @return the returned <code>Distributor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>distributorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByGenusType(org.osid.type.Type distributorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.distributor.DistributorGenusFilterList(getDistributors(), distributorGenusType));
    }


    /**
     *  Gets a <code>DistributorList</code> corresponding to the given
     *  distributor genus <code>Type</code> and include any additional
     *  distributors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDistributors()</code>.
     *
     *  @param  distributorGenusType a distributor genus type 
     *  @return the returned <code>Distributor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>distributorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByParentGenusType(org.osid.type.Type distributorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDistributorsByGenusType(distributorGenusType));
    }


    /**
     *  Gets a <code>DistributorList</code> containing the given
     *  distributor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getDistributors()</code>.
     *
     *  @param  distributorRecordType a distributor record type 
     *  @return the returned <code>Distributor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>distributorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByRecordType(org.osid.type.Type distributorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.provisioning.distributor.DistributorRecordFilterList(getDistributors(), distributorRecordType));
    }


    /**
     *  Gets a <code>DistributorList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known distributors or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  distributors that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Distributor</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getDistributorsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.provisioning.distributor.DistributorProviderFilterList(getDistributors(), resourceId));
    }


    /**
     *  Gets all <code>Distributors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  distributors or an error results. Otherwise, the returned list
     *  may contain only those distributors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Distributors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.provisioning.DistributorList getDistributors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the distributor list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of distributors
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.provisioning.DistributorList filterDistributorsOnViews(org.osid.provisioning.DistributorList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
