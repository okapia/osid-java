//
// AbstractRepositoryManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRepositoryManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.repository.RepositoryManager,
               org.osid.repository.RepositoryProxyManager {

    private final Types assetRecordTypes                   = new TypeRefSet();
    private final Types assetSearchRecordTypes             = new TypeRefSet();

    private final Types assetContentRecordTypes            = new TypeRefSet();
    private final Types compositionRecordTypes             = new TypeRefSet();
    private final Types compositionSearchRecordTypes       = new TypeRefSet();

    private final Types repositoryRecordTypes              = new TypeRefSet();
    private final Types repositorySearchRecordTypes        = new TypeRefSet();

    private final Types spatialUnitRecordTypes             = new TypeRefSet();
    private final Types coordinateTypes                    = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRepositoryManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRepositoryManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if asset lookup is supported. 
     *
     *  @return <code> true </code> if asset lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetLookup() {
        return (false);
    }


    /**
     *  Tests if asset query is supported. 
     *
     *  @return <code> true </code> if asset query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Tests if asset search is supported. 
     *
     *  @return <code> true </code> if asset search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetSearch() {
        return (false);
    }


    /**
     *  Tests if asset administration is supported. 
     *
     *  @return <code> true </code> if asset administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetAdmin() {
        return (false);
    }


    /**
     *  Tests if asset notification is supported. A repository may send 
     *  messages when assets are created, modified, or deleted. 
     *
     *  @return <code> true </code> if asset notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetNotification() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of assets and repositories is supported. 
     *
     *  @return <code> true </code> if asset repository mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetRepository() {
        return (false);
    }


    /**
     *  Tests if managing mappings of assets and repositories is supported. 
     *
     *  @return <code> true </code> if asset repository assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetRepositoryAssignment() {
        return (false);
    }


    /**
     *  Tests if asset smart repository is supported. 
     *
     *  @return <code> true </code> if asset smart repository is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetSmartRepository() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of assets and time coverage is supported. 
     *
     *  @return <code> true </code> if asset temporal mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetTemporal() {
        return (false);
    }


    /**
     *  Tests if managing mappings of assets and time ocverage is supported. 
     *
     *  @return <code> true </code> if asset temporal assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetTemporalAssignment() {
        return (false);
    }


    /**
     *  Tests if rerieving mappings of assets and spatial coverage is 
     *  supported. 
     *
     *  @return <code> true </code> if asset spatial mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetSpatial() {
        return (false);
    }


    /**
     *  Tests if managing mappings of assets and spatial ocverage is 
     *  supported. 
     *
     *  @return <code> true </code> if asset spatial assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetSpatialAssignment() {
        return (false);
    }


    /**
     *  Tests if assets are included in compositions. 
     *
     *  @return <code> true </code> if asset composition supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetComposition() {
        return (false);
    }


    /**
     *  Tests if mapping assets to compositions is supported. 
     *
     *  @return <code> true </code> if designing asset compositions is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetCompositionDesign() {
        return (false);
    }


    /**
     *  Tests if composition lookup is supported. 
     *
     *  @return <code> true </code> if composition lookup is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionLookup() {
        return (false);
    }


    /**
     *  Tests if composition query is supported. 
     *
     *  @return <code> true </code> if composition query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionQuery() {
        return (false);
    }


    /**
     *  Tests if composition search is supported. 
     *
     *  @return <code> true </code> if composition search is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionSearch() {
        return (false);
    }


    /**
     *  Tests if composition administration is supported. 
     *
     *  @return <code> true </code> if composition administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionAdmin() {
        return (false);
    }


    /**
     *  Tests if composition notification is supported. 
     *
     *  @return <code> true </code> if composition notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionNotification() {
        return (false);
    }


    /**
     *  Tests if retrieval of composition to repository mappings is supported. 
     *
     *  @return <code> true </code> if composition to repository mapping is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionRepository() {
        return (false);
    }


    /**
     *  Tests if assigning composition to repository mappings is supported. 
     *
     *  @return <code> true </code> if composition to repository assignment is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionRepositoryAssignment() {
        return (false);
    }


    /**
     *  Tests if composition smart repository is supported. 
     *
     *  @return <code> true </code> if composition smart repository is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCompositionSmartRepository() {
        return (false);
    }


    /**
     *  Tests if repository lookup is supported. 
     *
     *  @return <code> true </code> if repository lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryLookup() {
        return (false);
    }


    /**
     *  Tests if repository query is supported. 
     *
     *  @return <code> true </code> if repository query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryQuery() {
        return (false);
    }


    /**
     *  Tests if repository search is supported. 
     *
     *  @return <code> true </code> if repository search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositorySearch() {
        return (false);
    }


    /**
     *  Tests if repository administration is supported. 
     *
     *  @return <code> true </code> if repository administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryAdmin() {
        return (false);
    }


    /**
     *  Tests if repository notification is supported. Messages may be sent 
     *  when <code> Repository </code> objects are created, deleted or 
     *  updated. Notifications for assets within repositories are sent via the 
     *  asset notification session. 
     *
     *  @return <code> true </code> if repository notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryNotification() {
        return (false);
    }


    /**
     *  Tests if a repository hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a repository hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryHierarchy() {
        return (false);
    }


    /**
     *  Tests if a repository hierarchy design is supported. 
     *
     *  @return <code> true </code> if a repository hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests if a repository batch service is supported. 
     *
     *  @return <code> true </code> if a repository batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryBatch() {
        return (false);
    }


    /**
     *  Tests if a repository rules service is supported. 
     *
     *  @return <code> true </code> if a repository rules service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryRules() {
        return (false);
    }


    /**
     *  Gets all the asset record types supported. 
     *
     *  @return the list of supported asset record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssetRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.assetRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given asset type is supported. 
     *
     *  @param  assetRecordType the asset record type 
     *  @return <code> true </code> if the asset record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> assetRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssetRecordType(org.osid.type.Type assetRecordType) {
        return (this.assetRecordTypes.contains(assetRecordType));
    }


    /**
     *  Adds support for an asset record type.
     *
     *  @param assetRecordType an asset record type
     *  @throws org.osid.NullArgumentException
     *  <code>assetRecordType</code> is <code>null</code>
     */

    protected void addAssetRecordType(org.osid.type.Type assetRecordType) {
        this.assetRecordTypes.add(assetRecordType);
        return;
    }


    /**
     *  Removes support for an asset record type.
     *
     *  @param assetRecordType an asset record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>assetRecordType</code> is <code>null</code>
     */

    protected void removeAssetRecordType(org.osid.type.Type assetRecordType) {
        this.assetRecordTypes.remove(assetRecordType);
        return;
    }


    /**
     *  Gets all the asset search record types supported. 
     *
     *  @return the list of supported asset search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssetSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.assetSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given asset search record type is supported. 
     *
     *  @param  assetSearchRecordType the asset search record type 
     *  @return <code> true </code> if the asset search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> assetSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssetSearchRecordType(org.osid.type.Type assetSearchRecordType) {
        return (this.assetSearchRecordTypes.contains(assetSearchRecordType));
    }


    /**
     *  Adds support for an asset search record type.
     *
     *  @param assetSearchRecordType an asset search record type
     *  @throws org.osid.NullArgumentException
     *  <code>assetSearchRecordType</code> is <code>null</code>
     */

    protected void addAssetSearchRecordType(org.osid.type.Type assetSearchRecordType) {
        this.assetSearchRecordTypes.add(assetSearchRecordType);
        return;
    }


    /**
     *  Removes support for an asset search record type.
     *
     *  @param assetSearchRecordType an asset search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>assetSearchRecordType</code> is <code>null</code>
     */

    protected void removeAssetSearchRecordType(org.osid.type.Type assetSearchRecordType) {
        this.assetSearchRecordTypes.remove(assetSearchRecordType);
        return;
    }


    /**
     *  Gets all the asset content record types supported. 
     *
     *  @return the list of supported asset content record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssetContentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.assetContentRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given asset content record type is supported. 
     *
     *  @param  assetContentRecordType the asset content record type 
     *  @return <code> true </code> if the asset content record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> assetContentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssetContentRecordType(org.osid.type.Type assetContentRecordType) {
        return (this.assetContentRecordTypes.contains(assetContentRecordType));
    }


    /**
     *  Adds support for an asset content record type.
     *
     *  @param assetContentRecordType an asset content record type
     *  @throws org.osid.NullArgumentException
     *  <code>assetContentRecordType</code> is <code>null</code>
     */

    protected void addAssetContentRecordType(org.osid.type.Type assetContentRecordType) {
        this.assetContentRecordTypes.add(assetContentRecordType);
        return;
    }


    /**
     *  Removes support for an asset content record type.
     *
     *  @param assetContentRecordType an asset content record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>assetContentRecordType</code> is <code>null</code>
     */

    protected void removeAssetContentRecordType(org.osid.type.Type assetContentRecordType) {
        this.assetContentRecordTypes.remove(assetContentRecordType);
        return;
    }


    /**
     *  Gets all the composition record types supported. 
     *
     *  @return the list of supported composition record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompositionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.compositionRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given composition record type is supported. 
     *
     *  @param  compositionRecordType the composition record type 
     *  @return <code> true </code> if the composition record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> compositionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompositionRecordType(org.osid.type.Type compositionRecordType) {
        return (this.compositionRecordTypes.contains(compositionRecordType));
    }


    /**
     *  Adds support for a composition record type.
     *
     *  @param compositionRecordType a composition record type
     *  @throws org.osid.NullArgumentException
     *  <code>compositionRecordType</code> is <code>null</code>
     */

    protected void addCompositionRecordType(org.osid.type.Type compositionRecordType) {
        this.compositionRecordTypes.add(compositionRecordType);
        return;
    }


    /**
     *  Removes support for a composition record type.
     *
     *  @param compositionRecordType a composition record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>compositionRecordType</code> is <code>null</code>
     */

    protected void removeCompositionRecordType(org.osid.type.Type compositionRecordType) {
        this.compositionRecordTypes.remove(compositionRecordType);
        return;
    }


    /**
     *  Gets all the composition search record types supported. 
     *
     *  @return the list of supported composition search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCompositionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.compositionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given composition search record type is supported. 
     *
     *  @param  compositionSearchRecordType the composition serach type 
     *  @return <code> true </code> if the composition search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          compositionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCompositionSearchRecordType(org.osid.type.Type compositionSearchRecordType) {
        return (this.compositionSearchRecordTypes.contains(compositionSearchRecordType));
    }


    /**
     *  Adds support for a composition search record type.
     *
     *  @param compositionSearchRecordType a composition search record type
     *  @throws org.osid.NullArgumentException
     *  <code>compositionSearchRecordType</code> is <code>null</code>
     */

    protected void addCompositionSearchRecordType(org.osid.type.Type compositionSearchRecordType) {
        this.compositionSearchRecordTypes.add(compositionSearchRecordType);
        return;
    }


    /**
     *  Removes support for a composition search record type.
     *
     *  @param compositionSearchRecordType a composition search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>compositionSearchRecordType</code> is <code>null</code>
     */

    protected void removeCompositionSearchRecordType(org.osid.type.Type compositionSearchRecordType) {
        this.compositionSearchRecordTypes.remove(compositionSearchRecordType);
        return;
    }


    /**
     *  Gets all the repository record types supported. 
     *
     *  @return the list of supported repository record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRepositoryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.repositoryRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given repository record type is supported. 
     *
     *  @param  repositoryRecordType the repository record type 
     *  @return <code> true </code> if the repository record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> repositoryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRepositoryRecordType(org.osid.type.Type repositoryRecordType) {
        return (this.repositoryRecordTypes.contains(repositoryRecordType));
    }


    /**
     *  Adds support for a repository record type.
     *
     *  @param repositoryRecordType a repository record type
     *  @throws org.osid.NullArgumentException
     *  <code>repositoryRecordType</code> is <code>null</code>
     */

    protected void addRepositoryRecordType(org.osid.type.Type repositoryRecordType) {
        this.repositoryRecordTypes.add(repositoryRecordType);
        return;
    }


    /**
     *  Removes support for a repository record type.
     *
     *  @param repositoryRecordType a repository record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>repositoryRecordType</code> is <code>null</code>
     */

    protected void removeRepositoryRecordType(org.osid.type.Type repositoryRecordType) {
        this.repositoryRecordTypes.remove(repositoryRecordType);
        return;
    }


    /**
     *  Gets all the repository search record types supported. 
     *
     *  @return the list of supported repository search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRepositorySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.repositorySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given repository search record type is supported. 
     *
     *  @param  repositorySearchRecordType the repository search type 
     *  @return <code> true </code> if the repository search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          repositorySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRepositorySearchRecordType(org.osid.type.Type repositorySearchRecordType) {
        return (this.repositorySearchRecordTypes.contains(repositorySearchRecordType));
    }


    /**
     *  Adds support for a repository search record type.
     *
     *  @param repositorySearchRecordType a repository search record type
     *  @throws org.osid.NullArgumentException
     *  <code>repositorySearchRecordType</code> is <code>null</code>
     */

    protected void addRepositorySearchRecordType(org.osid.type.Type repositorySearchRecordType) {
        this.repositorySearchRecordTypes.add(repositorySearchRecordType);
        return;
    }


    /**
     *  Removes support for a repository search record type.
     *
     *  @param repositorySearchRecordType a repository search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>repositorySearchRecordType</code> is <code>null</code>
     */

    protected void removeRepositorySearchRecordType(org.osid.type.Type repositorySearchRecordType) {
        this.repositorySearchRecordTypes.remove(repositorySearchRecordType);
        return;
    }


    /**
     *  Gets all the spatial unit record types supported. 
     *
     *  @return the list of supported spatial unit record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSpatialUnitRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.spatialUnitRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given spatial unit record type is supported. 
     *
     *  @param  spatialUnitRecordType the spatial unit record type 
     *  @return <code> true </code> if the spatial unit record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> spatialUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        return (this.spatialUnitRecordTypes.contains(spatialUnitRecordType));
    }


    /**
     *  Adds support for a spatial unit record type.
     *
     *  @param spatialUnitRecordType a spatial unit record type
     *  @throws org.osid.NullArgumentException
     *  <code>spatialUnitRecordType</code> is <code>null</code>
     */

    protected void addSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        this.spatialUnitRecordTypes.add(spatialUnitRecordType);
        return;
    }


    /**
     *  Removes support for a spatial unit record type.
     *
     *  @param spatialUnitRecordType a spatial unit record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>spatialUnitRecordType</code> is <code>null</code>
     */

    protected void removeSpatialUnitRecordType(org.osid.type.Type spatialUnitRecordType) {
        this.spatialUnitRecordTypes.remove(spatialUnitRecordType);
        return;
    }


    /**
     *  Gets all the coordinate types supported. 
     *
     *  @return the list of supported coordinate types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCoordinateTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.coordinateTypes.toCollection()));
    }


    /**
     *  Tests if a given coordinate type is supported. 
     *
     *  @param  coordinateType the coordinate type 
     *  @return <code> true </code> if the coordinate type is supported <code> 
     *          , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> coordinateType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCoordinateType(org.osid.type.Type coordinateType) {
        return (this.coordinateTypes.contains(coordinateType));
    }


    /**
     *  Adds support for a coordinate type.
     *
     *  @param coordinateType a coordinate type
     *  @throws org.osid.NullArgumentException
     *  <code>coordinateType</code> is <code>null</code>
     */

    protected void addCoordinateType(org.osid.type.Type coordinateType) {
        this.coordinateTypes.add(coordinateType);
        return;
    }


    /**
     *  Removes support for a coordinate type.
     *
     *  @param coordinateType a coordinate type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>coordinateType</code> is <code>null</code>
     */

    protected void removeCoordinateType(org.osid.type.Type coordinateType) {
        this.coordinateTypes.remove(coordinateType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset lookup 
     *  service. 
     *
     *  @return the new <code> AssetLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetLookupSession getAssetLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetLookupSession getAssetLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset lookup 
     *  service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return the new <code> AssetLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAssetLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetLookupSession getAssetLookupSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetLookupSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset lookup 
     *  service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAssetLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetLookupSession getAssetLookupSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetLookupSessionForRepository not implemented");
    }


    /**
     *  Gets an asset query session. 
     *
     *  @return an <code> AssetQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuerySession getAssetQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuerySession getAssetQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetQuerySession not implemented");
    }


    /**
     *  Gets an asset query session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return an <code> AssetQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuerySession getAssetQuerySessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetQuerySessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the asset query 
     *  service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuerySession getAssetQuerySessionForRepository(org.osid.id.Id repositoryId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetQuerySessionForRepository not implemented");
    }


    /**
     *  Gets an asset search session. 
     *
     *  @return an <code> AssetSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSearchSession getAssetSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetSearchSession not implemented");
    }


    /**
     *  Gets an asset search session. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSearchSession getAssetSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetSearchSession not implemented");
    }


    /**
     *  Gets an asset search session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return an <code> AssetSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAssetSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSearchSession getAssetSearchSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetSearchSessionForRepository not implemented");
    }


    /**
     *  Gets an asset search session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsAssetSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSearchSession getAssetSearchSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetSearchSessionForRepository not implemented");
    }


    /**
     *  Gets an asset administration session for creating, updating and 
     *  deleting assets. 
     *
     *  @return an <code> AssetAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetAdminSession getAssetAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetAdminSession not implemented");
    }


    /**
     *  Gets an asset administration session for creating, updating and 
     *  deleting assets. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetAdminSession getAssetAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetAdminSession not implemented");
    }


    /**
     *  Gets an asset administration session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return an <code> AssetAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetAdminSession getAssetAdminSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetAdminSessionForRepository not implemented");
    }


    /**
     *  Gets an asset administration session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetAdminSession getAssetAdminSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetAdminSessionForRepository not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to asset 
     *  changes. 
     *
     *  @param  assetReceiver the notification callback 
     *  @return an <code> AssetNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assetReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetNotificationSession getAssetNotificationSession(org.osid.repository.AssetReceiver assetReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to asset 
     *  changes. 
     *
     *  @param  assetReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AssetNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assetReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetNotificationSession getAssetNotificationSession(org.osid.repository.AssetReceiver assetReceiver, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetNotificationSession not implemented");
    }


    /**
     *  Gets the asset notification session for the given repository. 
     *
     *  @param  assetReceiver the notification callback 
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return an <code> AssetNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> assetReceiver </code> or 
     *          <code> repositoryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetNotificationSession getAssetNotificationSessionForRepository(org.osid.repository.AssetReceiver assetReceiver, 
                                                                                                 org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetNotificationSessionForRepository not implemented");
    }


    /**
     *  Gets the asset notification session for the given repository. 
     *
     *  @param  assetReceiver the notification callback 
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> assetReceiver, 
     *          repositoryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetNotificationSession getAssetNotificationSessionForRepository(org.osid.repository.AssetReceiver assetReceiver, 
                                                                                                 org.osid.id.Id repositoryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetNotificationSessionForRepository not implemented");
    }


    /**
     *  Gets the session for retrieving asset to repository mappings. 
     *
     *  @return an <code> AssetRepositorySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetRepository() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetRepositorySession getAssetRepositorySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetRepositorySession not implemented");
    }


    /**
     *  Gets the session for retrieving asset to repository mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetRepositorySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetRepository() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetRepositorySession getAssetRepositorySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetRepositorySession not implemented");
    }


    /**
     *  Gets the session for assigning asset to repository mappings. 
     *
     *  @return an <code> AssetRepositoryAsignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetRepositoryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetRepositoryAssignmentSession getAssetRepositoryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetRepositoryAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning asset to repository mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetRepositoryAsignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetRepositoryAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetRepositoryAssignmentSession getAssetRepositoryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetRepositoryAssignmentSession not implemented");
    }


    /**
     *  Gets an asset smart repository session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return an <code> AssetSmartRepositorySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetSmartRepository() </code> <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSmartRepositorySession getAssetSmartRepositorySession(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetSmartRepositorySession not implemented");
    }


    /**
     *  Gets an asset smart repository session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetSmartRepositorySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetSmartRepository() </code> <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSmartRepositorySession getAssetSmartRepositorySession(org.osid.id.Id repositoryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetSmartRepositorySession not implemented");
    }


    /**
     *  Gets the session for retrieving temporal coverage of an asset. 
     *
     *  @return an <code> AssetTemporalSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetTemporal() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalSession getAssetTemporalSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetTemporalSession not implemented");
    }


    /**
     *  Gets the session for retrieving temporal coverage of an asset. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetTemporalSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetTemporal() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalSession getAssetTemporalSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetTemporalSession not implemented");
    }


    /**
     *  Gets the session for retrieving temporal coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return an <code> AssetTemporalSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetTemporal() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalSession getAssetTemporalSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetTemporalSessionForRepository not implemented");
    }


    /**
     *  Gets the session for retrieving temporal coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetTemporalSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetTemporal() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalSession getAssetTemporalSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetTemporalSessionForRepository not implemented");
    }


    /**
     *  Gets the session for assigning temporal coverage to an asset. 
     *
     *  @return an <code> AssetTemporalAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetTemporalAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalAssignmentSession getAssetTemporalAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetTemporalAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning temporal coverage to an asset. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetTemporalAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetTemporalAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalAssignmentSession getAssetTemporalAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetTemporalAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning temporal coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return an <code> AssetTemporalAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetTemporalAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalAssignmentSession getAssetTemporalAssignmentSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetTemporalAssignmentSessionForRepository not implemented");
    }


    /**
     *  Gets the session for assigning temporal coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetTemporalAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetTemporalAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetTemporalAssignmentSession getAssetTemporalAssignmentSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetTemporalAssignmentSessionForRepository not implemented");
    }


    /**
     *  Gets the session for retrieving spatial coverage of an asset. 
     *
     *  @return an <code> AssetSpatialSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSpatialAssets() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialSession getAssetSpatialSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetSpatialSession not implemented");
    }


    /**
     *  Gets the session for retrieving spatial coverage of an asset. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetSpatialSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSpatialAssets() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialSession getAssetSpatialSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetSpatialSession not implemented");
    }


    /**
     *  Gets the session for retrieving spatial coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return an <code> AssetSpatialSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetSpatial() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialSession getAssetSpatialSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetSpatialSessionForRepository not implemented");
    }


    /**
     *  Gets the session for retrieving spatial coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetSpatialSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssetSpatial() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialSession getAssetSpatialSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetSpatialSessionForRepository not implemented");
    }


    /**
     *  Gets the session for assigning spatial coverage to an asset. 
     *
     *  @return an <code> AssetSpatialAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetSpatialAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialAssignmentSession getAssetSpatialAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetSpatialAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning spatial coverage to an asset. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetSpatialAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetSpatialAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialAssignmentSession getAssetSpatialAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetSpatialAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning spatial coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return an <code> AssetSpatialAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetSpatialAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialAssignmentSession getAssetSpatialAssignmentSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetSpatialAssignmentSessionForRepository not implemented");
    }


    /**
     *  Gets the session for assigning spatial coverage of an asset for the 
     *  given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return an <code> AssetSpatialAssignmentSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetSpatialAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetSpatialAssignmentSession getAssetSpatialAssignmentSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetSpatialAssignmentSessionForRepository not implemented");
    }


    /**
     *  Gets the session for retrieving asset compositions. 
     *
     *  @return an <code> AssetCompositionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetComposition() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetCompositionSession getAssetCompositionSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetCompositionSession not implemented");
    }


    /**
     *  Gets the session for retrieving asset compositions. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetCompositionSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetComposition() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetCompositionSession getAssetCompositionSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetCompositionSession not implemented");
    }


    /**
     *  Gets the session for creating asset compositions. 
     *
     *  @return an <code> AssetCompositionDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetCompositionDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetCompositionDesignSession getAssetCompositionDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getAssetCompositionDesignSession not implemented");
    }


    /**
     *  Gets the session for creating asset compositions. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssetCompositionDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssetCompositionDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetCompositionDesignSession getAssetCompositionDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getAssetCompositionDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  lookup service. 
     *
     *  @return the new <code> CompositionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionLookupSession getCompositionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return the new <code> CompositionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionLookupSession getCompositionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  lookup service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return the new <code> CompositionLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionLookupSession getCompositionLookupSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionLookupSessionForRepository not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the composition 
     *  lookup service for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return the new <code> CompositionLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionLookupSession getCompositionLookupSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionLookupSessionForRepository not implemented");
    }


    /**
     *  Gets a composition query session. 
     *
     *  @return a <code> CompositionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuerySession getCompositionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionQuerySession not implemented");
    }


    /**
     *  Gets a composition query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSearchSession getCompositionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionQuerySession not implemented");
    }


    /**
     *  Gets a composition query session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return a <code> CompositionQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuerySession getCompositionQuerySessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionQuerySessionForRepository not implemented");
    }


    /**
     *  Gets a composition query session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuerySession getCompositionQuerySessionForRepository(org.osid.id.Id repositoryId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionQuerySessionForRepository not implemented");
    }


    /**
     *  Gets a composition search session. 
     *
     *  @return a <code> CompositionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSearchSession getCompositionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionSearchSession not implemented");
    }


    /**
     *  Gets a composition search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSearchSession getCompositionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionSearchSession not implemented");
    }


    /**
     *  Gets a composition search session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return a <code> CompositionSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSearchSession getCompositionSearchSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionSearchSessionForRepository not implemented");
    }


    /**
     *  Gets a composition search session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSearchSession getCompositionSearchSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionSearchSessionForRepository not implemented");
    }


    /**
     *  Gets a composition administration session for creating, updating and 
     *  deleting compositions. 
     *
     *  @return a <code> CompositionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionAdminSession getCompositionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionAdminSession not implemented");
    }


    /**
     *  Gets a composition administration session for creating, updating and 
     *  deleting compositions. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionAdminSession getCompositionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionAdminSession not implemented");
    }


    /**
     *  Gets a composiiton administrative session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return a <code> CompositionAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionAdminSession getCompositionAdminSessionForRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionAdminSessionForRepository not implemented");
    }


    /**
     *  Gets a composiiton administrative session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionAdminSession getCompositionAdminSessionForRepository(org.osid.id.Id repositoryId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionAdminSessionForRepository not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  composition changes. 
     *
     *  @param  compositionReceiver the notification callback 
     *  @return a <code> CompositionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> compositionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionNotificationSession getCompositionNotificationSession(org.osid.repository.CompositionReceiver compositionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  composition changes. 
     *
     *  @param  compositionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> compositionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionNotificationSession getCompositionNotificationSession(org.osid.repository.CompositionReceiver compositionReceiver, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionNotificationSession not implemented");
    }


    /**
     *  Gets the composition notification session for the given repository. 
     *
     *  @param  compositionReceiver the notification callback 
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return a <code> CompositionNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> compositionReceiver 
     *          </code> or <code> repositoryId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionNotificationSession getCompositionNotificationSessionForRepository(org.osid.repository.CompositionReceiver compositionReceiver, 
                                                                                                             org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionNotificationSessionForRepository not implemented");
    }


    /**
     *  Gets the composition notification session for the given repository. 
     *
     *  @param  compositionReceiver the notification callback 
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> compositionReceiver, 
     *          repositoryId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionNotificationSession getCompositionNotificationSessionForRepository(org.osid.repository.CompositionReceiver compositionReceiver, 
                                                                                                             org.osid.id.Id repositoryId, 
                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionNotificationSessionForRepository not implemented");
    }


    /**
     *  Gets the session for retrieving composition to repository mappings. 
     *
     *  @return a <code> CompositionRepositorySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionRepository() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionRepositorySession getCompositionRepositorySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionRepositorySession not implemented");
    }


    /**
     *  Gets the session for retrieving composition to repository mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionRepositorySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionRepository() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionRepositorySession getCompositionRepositorySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionRepositorySession not implemented");
    }


    /**
     *  Gets the session for assigning composition to repository mappings. 
     *
     *  @return a <code> CompositionRepositoryAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionRepositoryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionRepositoryAssignmentSession getCompositionRepositoryAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionRepositoryAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning composition to repository mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CompositionRepositoryAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionRepositoryAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionRepositoryAssignmentSession getCompositionRepositoryAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionRepositoryAssignmentSession not implemented");
    }


    /**
     *  Gets a composition smart repository session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @return a <code> CompositionSmartRepositorySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionSmartRepository() </code> <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSmartRepositorySession getCompositionSmartRepositorySession(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getCompositionSmartRepositorySession not implemented");
    }


    /**
     *  Gets a composition smart repository session for the given repository. 
     *
     *  @param  repositoryId the <code> Id </code> of the repository 
     *  @param  proxy a proxy 
     *  @return a <code> CompositionSmartRepositorySession </code> 
     *  @throws org.osid.NotFoundException <code> repositoryId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCompositionSmartRepository() </code> <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionSmartRepositorySession getCompositionSmartRepositorySession(org.osid.id.Id repositoryId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getCompositionSmartRepositorySession not implemented");
    }


    /**
     *  Gets the repository lookup session. 
     *
     *  @return a <code> RepositoryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryLookupSession getRepositoryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getRepositoryLookupSession not implemented");
    }


    /**
     *  Gets the repository lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepositoryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryLookupSession getRepositoryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getRepositoryLookupSession not implemented");
    }


    /**
     *  Gets the repository query session. 
     *
     *  @return a <code> RepositoryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuerySession getRepositoryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getRepositoryQuerySession not implemented");
    }


    /**
     *  Gets the repository query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepositoryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuerySession getRepositoryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getRepositoryQuerySession not implemented");
    }


    /**
     *  Gets the repository search session. 
     *
     *  @return a <code> RepositorySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositorySearchSession getRepositorySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getRepositorySearchSession not implemented");
    }


    /**
     *  Gets the repository search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepositorySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositorySearchSession getRepositorySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getRepositorySearchSession not implemented");
    }


    /**
     *  Gets the repository administrative session for creating, updating and 
     *  deleteing repositories. 
     *
     *  @return a <code> RepositoryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryAdminSession getRepositoryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getRepositoryAdminSession not implemented");
    }


    /**
     *  Gets the repository administrative session for creating, updating and 
     *  deleteing repositories. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepositoryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryAdminSession getRepositoryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getRepositoryAdminSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to a 
     *  repository. 
     *
     *  @param  repositoryReceiver the notification callback 
     *  @return a <code> RepositoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryNotificationSession getRepositoryNotificationSession(org.osid.repository.RepositoryReceiver repositoryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getRepositoryNotificationSession not implemented");
    }


    /**
     *  Gets the notification session for subscribing to changes to a 
     *  repository. 
     *
     *  @param  repositoryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RepositoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> repositoryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryNotificationSession getRepositoryNotificationSession(org.osid.repository.RepositoryReceiver repositoryReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getRepositoryNotificationSession not implemented");
    }


    /**
     *  Gets the repository hierarchy traversal session. 
     *
     *  @return <code> a RepositoryHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryHierarchySession getRepositoryHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getRepositoryHierarchySession not implemented");
    }


    /**
     *  Gets the repository hierarchy traversal session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a RepositoryHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryHierarchySession getRepositoryHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getRepositoryHierarchySession not implemented");
    }


    /**
     *  Gets the repository hierarchy design session. 
     *
     *  @return a <code> RepostoryHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryHierarchyDesignSession getRepositoryHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getRepositoryHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the repository hierarchy design session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RepostoryHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryHierarchyDesignSession getRepositoryHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getRepositoryHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> RepositoryBatchManager. </code> 
     *
     *  @return a <code> RepostoryBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.RepositoryBatchManager getRepositoryBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getRepositoryBatchManager not implemented");
    }


    /**
     *  Gets a <code> RepositoryBatchProxyManager. </code> 
     *
     *  @return a <code> RepostoryBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.batch.RepositoryBatchProxyManager getRepositoryBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getRepositoryBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> RepositoryRulesManager. </code> 
     *
     *  @return a <code> RepostoryRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.RepositoryRulesManager getRepositoryRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryManager.getRepositoryRulesManager not implemented");
    }


    /**
     *  Gets a <code> RepositoryRulesProxyManager. </code> 
     *
     *  @return a <code> RepostoryRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.rules.RepositoryRulesProxyManager getRepositoryRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.repository.RepositoryProxyManager.getRepositoryRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.assetRecordTypes.clear();
        this.assetRecordTypes.clear();

        this.assetSearchRecordTypes.clear();
        this.assetSearchRecordTypes.clear();

        this.assetContentRecordTypes.clear();
        this.assetContentRecordTypes.clear();

        this.compositionRecordTypes.clear();
        this.compositionRecordTypes.clear();

        this.compositionSearchRecordTypes.clear();
        this.compositionSearchRecordTypes.clear();

        this.repositoryRecordTypes.clear();
        this.repositoryRecordTypes.clear();

        this.repositorySearchRecordTypes.clear();
        this.repositorySearchRecordTypes.clear();

        this.spatialUnitRecordTypes.clear();
        this.spatialUnitRecordTypes.clear();

        this.coordinateTypes.clear();
        this.coordinateTypes.clear();

        return;
    }
}
