//
// AbstractPostEntry.java
//
//     Defines a PostEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.posting.postentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>PostEntry</code>.
 */

public abstract class AbstractPostEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.financials.posting.PostEntry {

    private org.osid.financials.posting.Post post;
    private org.osid.financials.Account account;
    private org.osid.financials.Activity activity;
    private org.osid.financials.Currency amount;
    private boolean debit = true;

    private final java.util.Collection<org.osid.financials.posting.records.PostEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the post <code> Id </code> to which this entry belongs. 
     *
     *  @return the payer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPostId() {
        return (this.post.getId());
    }


    /**
     *  Gets the post to which this entry belongs. 
     *
     *  @return the post 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.posting.Post getPost()
        throws org.osid.OperationFailedException {

        return (this.post);
    }


    /**
     *  Sets the post.
     *
     *  @param post a post
     *  @throws org.osid.NullArgumentException <code>post</code> is
     *          <code>null</code>
     */

    protected void setPost(org.osid.financials.posting.Post post) {
        nullarg(post, "post");
        this.post = post;
        return;
    }


    /**
     *  Gets the G/L account <code> Id </code> to which this entry applies. 
     *
     *  @return the account <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAccountId() {
        return (this.account.getId());
    }


    /**
     *  Gets the G/L account to which this entry applies. 
     *
     *  @return the account 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Account getAccount()
        throws org.osid.OperationFailedException {

        return (this.account);
    }


    /**
     *  Sets the account.
     *
     *  @param account an account
     *  @throws org.osid.NullArgumentException
     *          <code>account</code> is <code>null</code>
     */

    protected void setAccount(org.osid.financials.Account account) {
        nullarg(account, "account");
        this.account = account;
        return;
    }


    /**
     *  Gets the financial activity <code> Id </code> to which this entry 
     *  applies. 
     *
     *  @return the activity <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        return (this.activity.getId());
    }


    /**
     *  Gets the financial activity to which this entry applies. 
     *
     *  @return the activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Activity getActivity()
        throws org.osid.OperationFailedException {

        return (this.activity);
    }


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException
     *          <code>activity</code> is <code>null</code>
     */

    protected void setActivity(org.osid.financials.Activity activity) {
        nullarg(activity, "activity");
        this.activity = activity;
        return;
    }


    /**
     *  Gets the amount. 
     *
     *  @return the amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getAmount() {
        return (this.amount);
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "amount");
        this.amount = amount;
        return;
    }


    /**
     *  Tests if the amount is a debit or a credit. 
     *
     *  @return <code> true </code> if this entry amount is a debit,
     *          <code> false </code> if it is a credit
     */

    @OSID @Override
    public boolean isDebit() {
        return (this.debit);
    }


    /**
     *  Sets the debit flag.
     *
     *  @param debit <code> true </code> if this entry amount is a
     *         debit, <code> false </code> if it is a credit
     */

    protected void setDebit(boolean debit) {
        this.debit = debit;
        return;
    }


    /**
     *  Tests if this postEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  postEntryRecordType a post entry record type 
     *  @return <code>true</code> if the postEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type postEntryRecordType) {
        for (org.osid.financials.posting.records.PostEntryRecord record : this.records) {
            if (record.implementsRecordType(postEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>PostEntry</code> record <code>Type</code>.
     *
     *  @param  postEntryRecordType the post entry record type 
     *  @return the post entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(postEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostEntryRecord getPostEntryRecord(org.osid.type.Type postEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.posting.records.PostEntryRecord record : this.records) {
            if (record.implementsRecordType(postEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(postEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this post entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param postEntryRecord the post entry record
     *  @param postEntryRecordType post entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>postEntryRecord</code> or
     *          <code>postEntryRecordTypepostEntry</code> is
     *          <code>null</code>
     */
            
    protected void addPostEntryRecord(org.osid.financials.posting.records.PostEntryRecord postEntryRecord, 
                                      org.osid.type.Type postEntryRecordType) {

        nullarg(postEntryRecord, "post entry record");
        addRecordType(postEntryRecordType);
        this.records.add(postEntryRecord);
        
        return;
    }
}
