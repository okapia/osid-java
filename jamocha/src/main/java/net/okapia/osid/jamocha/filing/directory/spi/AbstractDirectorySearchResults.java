//
// AbstractDirectorySearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.directory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractDirectorySearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.filing.DirectorySearchResults {

    private org.osid.filing.DirectoryList directories;
    private final org.osid.filing.DirectoryQueryInspector inspector;
    private final java.util.Collection<org.osid.filing.records.DirectorySearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractDirectorySearchResults.
     *
     *  @param directories the result set
     *  @param directoryQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>directories</code>
     *          or <code>directoryQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractDirectorySearchResults(org.osid.filing.DirectoryList directories,
                                            org.osid.filing.DirectoryQueryInspector directoryQueryInspector) {
        nullarg(directories, "directories");
        nullarg(directoryQueryInspector, "directory query inspectpr");

        this.directories = directories;
        this.inspector = directoryQueryInspector;

        return;
    }


    /**
     *  Gets the directory list resulting from a search.
     *
     *  @return a directory list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.filing.DirectoryList getDirectories() {
        if (this.directories == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.filing.DirectoryList directories = this.directories;
        this.directories = null;
	return (directories);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.filing.DirectoryQueryInspector getDirectoryQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  directory search record <code> Type. </code> This method must
     *  be used to retrieve a directory implementing the requested
     *  record.
     *
     *  @param directorySearchRecordType a directory search 
     *         record type 
     *  @return the directory search
     *  @throws org.osid.NullArgumentException
     *          <code>directorySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(directorySearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.DirectorySearchResultsRecord getDirectorySearchResultsRecord(org.osid.type.Type directorySearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.filing.records.DirectorySearchResultsRecord record : this.records) {
            if (record.implementsRecordType(directorySearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(directorySearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record directory search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addDirectoryRecord(org.osid.filing.records.DirectorySearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "directory record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
