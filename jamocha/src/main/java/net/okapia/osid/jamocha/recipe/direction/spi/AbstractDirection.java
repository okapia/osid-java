//
// AbstractDirection.java
//
//     Defines a Direction.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.direction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Direction</code>.
 */

public abstract class AbstractDirection
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.recipe.Direction {

    private org.osid.recipe.Recipe recipe;
    private final java.util.Collection<org.osid.recipe.Procedure> procedures = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recipe.Ingredient> ingredients = new java.util.LinkedHashSet<>();
    private org.osid.calendaring.Duration estimatedDuration;
    private final java.util.Collection<org.osid.repository.Asset> assets = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.recipe.records.DirectionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the recipe. 
     *
     *  @return the recipe <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRecipeId() {
        return (this.recipe.getId());
    }


    /**
     *  Gets the recipe. 
     *
     *  @return the recipe 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recipe.Recipe getRecipe()
        throws org.osid.OperationFailedException {

        return (this.recipe);
    }


    /**
     *  Sets the recipe.
     *
     *  @param recipe a recipe
     *  @throws org.osid.NullArgumentException
     *          <code>recipe</code> is <code>null</code>
     */

    protected void setRecipe(org.osid.recipe.Recipe recipe) {
        nullarg(recipe, "recipe");
        this.recipe = recipe;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the procedure to execute in this 
     *  direction. 
     *
     *  @return the procedure <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getProcedureIds() {
        try {
            org.osid.recipe.ProcedureList procedures = getProcedures();
            return (new net.okapia.osid.jamocha.adapter.converter.recipe.procedure.ProcedureToIdList(procedures));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the procedures to execute in this direction. 
     *
     *  @return the procedures 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureList getProcedures()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.recipe.procedure.ArrayProcedureList(this.procedures));
    }


    /**
     *  Adds a procedure.
     *
     *  @param procedure a procedure
     *  @throws org.osid.NullArgumentException
     *          <code>procedure</code> is <code>null</code>
     */

    protected void addProcedure(org.osid.recipe.Procedure procedure) {
        nullarg(procedure, "procedure");
        this.procedures.add(procedure);
        return;
    }


    /**
     *  Sets all the procedures.
     *
     *  @param procedures a collection of procedures
     *  @throws org.osid.NullArgumentException
     *          <code>procedures</code> is <code>null</code>
     */

    protected void setProcedures(java.util.Collection<org.osid.recipe.Procedure> procedures) {
        nullarg(procedures, "procedures");
        this.procedures.clear();
        this.procedures.addAll(procedures);
        return;
    }


    /**
     *  Gets the required ingredient Ids. 
     *
     *  @return the ingredients 
     */

    @OSID @Override
    public org.osid.id.IdList getIngredientIds() {
        try {
            org.osid.recipe.IngredientList ingredients = getIngredients();
            return (new net.okapia.osid.jamocha.adapter.converter.recipe.ingredient.IngredientToIdList(ingredients));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the required ingredients. 
     *
     *  @return the ingredients 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.recipe.IngredientList getIngredients()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.recipe.ingredient.ArrayIngredientList(this.ingredients));
    }


    /**
     *  Adds an ingredient.
     *
     *  @param ingredient an ingredient
     *  @throws org.osid.NullArgumentException
     *          <code>ingredient</code> is <code>null</code>
     */

    protected void addIngredient(org.osid.recipe.Ingredient ingredient) {
        nullarg(ingredient, "ingredient");
        this.ingredients.add(ingredient);
        return;
    }


    /**
     *  Sets all the ingredients.
     *
     *  @param ingredients a collection of ingredients
     *  @throws org.osid.NullArgumentException
     *          <code>ingredients</code> is <code>null</code>
     */

    protected void setIngredients(java.util.Collection<org.osid.recipe.Ingredient> ingredients) {
        nullarg(ingredients, "ingredients");
        this.ingredients.clear();
        this.ingredients.addAll(ingredients);
        return;
    }


    /**
     *  Gets the estimated time required for this direction. 
     *
     *  @return the estimated duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getEstimatedDuration() {
        return (this.estimatedDuration);
    }


    /**
     *  Sets the estimated duration.
     *
     *  @param duration an estimated duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setEstimatedDuration(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.estimatedDuration = duration;
        return;
    }


    /**
     *  Gets any asset <code> Ids </code> to assist in carrying out this 
     *  direction. 
     *
     *  @return the asset <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getAssetIds() {
        try {
            org.osid.repository.AssetList assets = getAssets();
            return (new net.okapia.osid.jamocha.adapter.converter.repository.asset.AssetToIdList(assets));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets any assets to assist in carrying out this direction. 
     *
     *  @return the assets 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.repository.AssetList getAssets()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.repository.asset.ArrayAssetList(this.assets));
    }


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException
     *          <code>asset</code> is <code>null</code>
     */

    protected void addAsset(org.osid.repository.Asset asset) {
        nullarg(asset, "asset");
        this.assets.add(asset);
        return;
    }


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @throws org.osid.NullArgumentException
     *          <code>assets</code> is <code>null</code>
     */

    protected void setAssets(java.util.Collection<org.osid.repository.Asset> assets) {
        nullarg(assets, "assets");
        this.assets.clear();
        this.assets.addAll(assets);
        return;
    }


    /**
     *  Tests if this direction supports the given record
     *  <code>Type</code>.
     *
     *  @param  directionRecordType a direction record type 
     *  @return <code>true</code> if the directionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type directionRecordType) {
        for (org.osid.recipe.records.DirectionRecord record : this.records) {
            if (record.implementsRecordType(directionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Direction</code> record <code>Type</code>.
     *
     *  @param  directionRecordType the direction record type 
     *  @return the direction record 
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionRecord getDirectionRecord(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.DirectionRecord record : this.records) {
            if (record.implementsRecordType(directionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this direction. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param directionRecord the direction record
     *  @param directionRecordType direction record type
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecord</code> or
     *          <code>directionRecordTypedirection</code> is
     *          <code>null</code>
     */
            
    protected void addDirectionRecord(org.osid.recipe.records.DirectionRecord directionRecord, 
                                      org.osid.type.Type directionRecordType) {
        
        nullarg(directionRecord, "direction record");
        addRecordType(directionRecordType);
        this.records.add(directionRecord);
        
        return;
    }
}
