//
// AbstractRelevancyEnablerQuery.java
//
//     A template for making a RelevancyEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.rules.relevancyenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for relevancy enablers.
 */

public abstract class AbstractRelevancyEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.ontology.rules.RelevancyEnablerQuery {

    private final java.util.Collection<org.osid.ontology.rules.records.RelevancyEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the relevancy. 
     *
     *  @param  relevancyId the relevancy <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> relevancyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRelevancyId(org.osid.id.Id relevancyId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the relevancy <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRelevancyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RelevancyQuery </code> is available. 
     *
     *  @return <code> true </code> if a relevancy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRelevancyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a relevancy. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the relevancy query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRelevancyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQuery getRuledRelevancyQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRelevancyQuery() is false");
    }


    /**
     *  Matches enablers mapped to any relevancy. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          relevancy, <code> false </code> to match enablers mapped to no 
     *          relevancy 
     */

    @OSID @Override
    public void matchAnyRuledRelevancy(boolean match) {
        return;
    }


    /**
     *  Clears the relevancy query terms. 
     */

    @OSID @Override
    public void clearRuledRelevancyTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the ontology. 
     *
     *  @param  ontologyId the ontology <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ontologyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOntologyId(org.osid.id.Id ontologyId, boolean match) {
        return;
    }


    /**
     *  Clears the ontology <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOntologyIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> OntologyQuery </code> is available. 
     *
     *  @return <code> true </code> if a ontology query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ontology. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ontology query 
     *  @throws org.osid.UnimplementedException <code> supportsOntologyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQuery getOntologyQuery() {
        throw new org.osid.UnimplementedException("supportsOntologyQuery() is false");
    }


    /**
     *  Clears the ontology query terms. 
     */

    @OSID @Override
    public void clearOntologyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given relevancy enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a relevancy enabler implementing the requested record.
     *
     *  @param relevancyEnablerRecordType a relevancy enabler record type
     *  @return the relevancy enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>relevancyEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(relevancyEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.rules.records.RelevancyEnablerQueryRecord getRelevancyEnablerQueryRecord(org.osid.type.Type relevancyEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.rules.records.RelevancyEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(relevancyEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(relevancyEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this relevancy enabler query. 
     *
     *  @param relevancyEnablerQueryRecord relevancy enabler query record
     *  @param relevancyEnablerRecordType relevancyEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRelevancyEnablerQueryRecord(org.osid.ontology.rules.records.RelevancyEnablerQueryRecord relevancyEnablerQueryRecord, 
                                          org.osid.type.Type relevancyEnablerRecordType) {

        addRecordType(relevancyEnablerRecordType);
        nullarg(relevancyEnablerQueryRecord, "relevancy enabler query record");
        this.records.add(relevancyEnablerQueryRecord);        
        return;
    }
}
