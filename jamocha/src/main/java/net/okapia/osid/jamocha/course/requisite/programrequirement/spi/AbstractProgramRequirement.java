//
// AbstractProgramRequirement.java
//
//     Defines a ProgramRequirement.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.programrequirement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>ProgramRequirement</code>.
 */

public abstract class AbstractProgramRequirement
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.course.requisite.ProgramRequirement {

    private boolean requiresCompletion = true;
    private org.osid.course.program.Program program;
    private org.osid.calendaring.Duration timeframe;
    private org.osid.grading.GradeSystem minimumGPASystem;
    private java.math.BigDecimal minimumGPA;
    private java.math.BigDecimal minimumEarnedCredits;

    private final java.util.Collection<org.osid.course.requisite.Requisite> altRequisites = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.records.ProgramRequirementRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets any <code> Requisites </code> that may be substituted in
     *  place of this <code> ProgramRequirement. </code> All <code>
     *  Requisites </code> must be satisifed to be a substitute for
     *  this program requirement.  Inactive <code> Requisites </code>
     *  are not evaluated but if no applicable requisite exists, then
     *  the alternate requisite is not satisifed.
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.altRequisites.toArray(new org.osid.course.requisite.Requisite[this.altRequisites.size()]));
    }


    /**
     *  Adds an alt requisite.
     *
     *  @param altRequisite an alternate requisite
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisite</code> is <code>null</code>
     */

    protected void addAltRequisite(org.osid.course.requisite.Requisite altRequisite) {
        nullarg(altRequisite, "alt requisite");
        this.altRequisites.add(altRequisite);
        return;
    }


    /**
     *  Sets all the alt requisites.
     *
     *  @param altRequisites a collection of alternate requisites
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisites</code> is <code>null</code>
     */

    protected void setAltRequisites(java.util.Collection<org.osid.course.requisite.Requisite> altRequisites) {
        nullarg(altRequisites, "alt requisites");

        this.altRequisites.clear();
        this.altRequisites.addAll(altRequisites);

        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        return (this.program.getId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        return (this.program);
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @throws org.osid.NullArgumentException
     *          <code>program</code> is <code>null</code>
     */

    protected void setProgram(org.osid.course.program.Program program) {
        nullarg(program, "program");
        this.program = program;
        return;
    }


    /**
     *  Tests if this requirement requires completion of the program.
     *
     *  @return <code> true </code> if a completion of the program is
     *          required, <code> false </code> if enrollment in the
     *          program is required
     */

    @OSID @Override
    public boolean requiresCompletion() {
        return (this.requiresCompletion);
    }


    /**
     *  Sets the requires completion flag.
     *
     *  @param requires <code> true </code> if a completion of the
     *         program is required, <code> false </code> if enrollment
     *         in the program is required
     */

    protected void setRequiresCompletion(boolean requires) {
        this.requiresCompletion = requires;
        return;
    }


    /**
     *  Tests if the program must be completed within the required
     *  duration.
     *
     *  @return <code> true </code> if the program has to be completed
     *          within a required time, <code> false </code> if it
     *          could have been completed at any time in the past
     */

    @OSID @Override
    public boolean hasTimeframe() {
        return (this.timeframe != null);
    }


    /**
     *  Gets the timeframe in which the program has to be completed. A
     *  negative duration indicates the program had to be completed
     *  within the specified amount of time in the past. A posiitive
     *  duration indicates the program must be completed within the
     *  specified amount of time in the future. A zero duration
     *  indicates the program must be completed in the current term.
     *
     *  @return the time frame 
     *  @throws org.osid.IllegalStateException <code> hasTimeframe()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTimeframe() {
        if (!hasTimeframe()) {
            throw new org.osid.IllegalStateException("hasTimeframe() is false");
        }

        return (this.timeframe);
    }


    /**
     *  Sets the timeframe.
     *
     *  @param timeframe a timeframe
     *  @throws org.osid.NullArgumentException <code>timeframe</code>
     *          is <code>null</code>
     */

    protected void setTimeframe(org.osid.calendaring.Duration timeframe) {
        nullarg(timeframe, "timeframe");
        this.timeframe = timeframe;
        return;
    }


    /**
     *  Tests if a minimum GPA above passing is required in the
     *  completion of the program or maintained at this level during
     *  enrollment.
     *
     *  @return <code> true </code> if a minimum gpa is required,
     *          <code> false </code> if the course just has to be
     *          passed
     */

    @OSID @Override
    public boolean hasMinimumGPA() {
        return ((this.minimumGPA != null) && (this.minimumGPA != null));
    }


    /**
     *  Gets the scoring system <code> Id </code> for the minimum GPA.
     *
     *  @return the scoring system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGPA() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumGPASystemId() {
        if (!hasMinimumGPA()) {
            throw new org.osid.IllegalStateException("hasMinimumGPA() is false");
        }

        return (this.minimumGPASystem.getId());
    }


    /**
     *  Gets the scoring system for the minimum GPA. 
     *
     *  @return the scoring system 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGPA() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getMinimumGPASystem()
        throws org.osid.OperationFailedException {

        if (!hasMinimumGPA()) {
            throw new org.osid.IllegalStateException("hasMinimumGPA() is false");
        }

        return (this.minimumGPASystem);
    }


    /**
     *  Sets the minimum gpa system.
     *
     *  @param system the minimum gpa system
     *  @throws org.osid.NullArgumentException <code>system</code> is
     *          <code>null</code>
     */

    protected void setMinimumGPASystem(org.osid.grading.GradeSystem system) {
        nullarg(system, "minimum gpa system");
        this.minimumGPASystem = system;
        return;
    }


    /**
     *  Gets the minimum GPA. 
     *
     *  @return the minimum gpa 
     *  @throws org.osid.IllegalStateException <code> hasMinimumGPA()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumGPA() {
        if (!hasMinimumGPA()) {
            throw new org.osid.IllegalStateException("hasMinimumGPA() is false");
        }

        return (this.minimumGPA);
    }


    /**
     *  Sets the minimum gpa.
     *
     *  @param gpa a minimum gpa
     *  @throws org.osid.NullArgumentException <code>gpa</code> is
     *          <code>null</code>
     */

    protected void setMinimumGPA(java.math.BigDecimal gpa) {
        nullarg(gpa, "gpa");
        this.minimumGPA = gpa;
        return;
    }


    /**
     *  Tests if a minimum credits earned in the program is required.
     *
     *  @return <code> true </code> if a minimum credits is required,
     *          <code> false </code> otehrwise
     */

    @OSID @Override
    public boolean hasMinimumEarnedCredits() {
        return (this.minimumEarnedCredits != null);
    }


    /**
     *  Gets the minimum earned credits. 
     *
     *  @return the minimum credits 
     *  @throws org.osid.IllegalStateException <code> 
     *          hasMinimumEarnedCredits() </code> is <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getMinimumEarnedCredits() {
        if (!hasMinimumEarnedCredits()) {
            throw new org.osid.IllegalStateException("hasMinimumEarnedCredits() is false");
        }

        return (this.minimumEarnedCredits);
    }


    /**
     *  Sets the minimum earned credits.
     *
     *  @param credits a minimum earned credits
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    protected void setMinimumEarnedCredits(java.math.BigDecimal credits) {
        nullarg(credits, "credits");
        this.minimumEarnedCredits = credits;
        return;
    }


    /**
     *  Tests if this program requirement supports the given record
     *  <code>Type</code>.
     *
     *  @param  programRequirementRecordType a program requirement record type 
     *  @return <code>true</code> if the programRequirementRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type programRequirementRecordType) {
        for (org.osid.course.requisite.records.ProgramRequirementRecord record : this.records) {
            if (record.implementsRecordType(programRequirementRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ProgramRequirement</code> record <code>Type</code>.
     *
     *  @param  programRequirementRecordType the program requirement record type 
     *  @return the program requirement record 
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programRequirementRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.ProgramRequirementRecord getProgramRequirementRecord(org.osid.type.Type programRequirementRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.ProgramRequirementRecord record : this.records) {
            if (record.implementsRecordType(programRequirementRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programRequirementRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program requirement. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param programRequirementRecord the program requirement record
     *  @param programRequirementRecordType program requirement record type
     *  @throws org.osid.NullArgumentException
     *          <code>programRequirementRecord</code> or
     *          <code>programRequirementRecordTypeprogramRequirement</code> is
     *          <code>null</code>
     */
            
    protected void addProgramRequirementRecord(org.osid.course.requisite.records.ProgramRequirementRecord programRequirementRecord, 
                                               org.osid.type.Type programRequirementRecordType) {

        nullarg(programRequirementRecord, "program requirement record");
        addRecordType(programRequirementRecordType);
        this.records.add(programRequirementRecord);
        
        return;
    }
}
