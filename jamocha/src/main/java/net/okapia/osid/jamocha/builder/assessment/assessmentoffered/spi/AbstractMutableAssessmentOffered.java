//
// AbstractMutableAssessmentOffered.java
//
//     Defines a mutable AssessmentOffered.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmentoffered.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>AssessmentOffered</code>.
 */

public abstract class AbstractMutableAssessmentOffered
    extends net.okapia.osid.jamocha.assessment.assessmentoffered.spi.AbstractAssessmentOffered
    implements org.osid.assessment.AssessmentOffered,
               net.okapia.osid.jamocha.builder.assessment.assessmentoffered.AssessmentOfferedMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this assessment offered. 
     *
     *  @param record assessment offered record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addAssessmentOfferedRecord(org.osid.assessment.records.AssessmentOfferedRecord record, org.osid.type.Type recordType) {
        super.addAssessmentOfferedRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this assessment offered.
     *
     *  @param displayName the name for this assessment offered
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this assessment offered.
     *
     *  @param description the description of this assessment offered
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    @Override
    public void setAssessment(org.osid.assessment.Assessment assessment) {
        super.setAssessment(assessment);
        return;
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException
     *          <code>level</code> is <code>null</code>
     */

    @Override
    public void setLevel(org.osid.grading.Grade level) {
        super.setLevel(level);
        return;
    }


    /**
     *  Sets the sequential flag.
     *
     *  @param sequential <code> true </code> if the items are taken
     *         sequentially, <code> false </code> if the items can be
     *         skipped and revisited
     */

    @Override
    public void setItemsSequential(boolean sequential) {
        super.setItemsSequential(sequential);
        return;
    }


    /**
     *  Sets the shuffled flag.
     *
     *  @param shuffled <code> true </code> if the items appear in a
     *         random order, <code> false </code> otherwise
     */

    @Override
    public void setItemsShuffled(boolean shuffled) {
        super.setItemsShuffled(shuffled);
        return;
    }

    
    /**
     *  Sets the start time.
     *
     *  @param startTime a start time
     *  @throws org.osid.NullArgumentException
     *          <code>startTime</code> is <code>null</code>
     */

    @Override
    public void setStartTime(org.osid.calendaring.DateTime startTime) {
        super.setStartTime(startTime);
        return;
    }


    /**
     *  Sets the deadline.
     *
     *  @param deadline a deadline
     *  @throws org.osid.NullArgumentException
     *          <code>deadline</code> is <code>null</code>
     */

    @Override
    public void setDeadline(org.osid.calendaring.DateTime deadline) {
        super.setDeadline(deadline);
        return;
    }


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException
     *          <code>duration</code> is <code>null</code>
     */

    @Override
    public void setDuration(org.osid.calendaring.Duration duration) {
        super.setDuration(duration);
        return;
    }


    /**
     *  Sets the score system.
     *
     *  @param gradeSystem the scoring system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    @Override
    public void setScoreSystem(org.osid.grading.GradeSystem gradeSystem) {
        super.setScoreSystem(gradeSystem);
        return;
    }


    /**
     *  Sets the grade system.
     *
     *  @param gradeSystem the scoring system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    @Override
    public void setGradeSystem(org.osid.grading.GradeSystem gradeSystem) {
        super.setGradeSystem(gradeSystem);
        return;
    }


    /**
     *  Sets the rubric.
     *
     *  @param assessmentOffered the rubric assessment offered
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentOffered</code> is <code>null</code>
     */

    @Override
    public void setRubric(org.osid.assessment.AssessmentOffered assessmentOffered) {
        super.setRubric(assessmentOffered);
        return;
    }
}

