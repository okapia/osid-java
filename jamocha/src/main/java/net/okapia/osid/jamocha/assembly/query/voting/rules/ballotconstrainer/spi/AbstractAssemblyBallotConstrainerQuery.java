//
// AbstractAssemblyBallotConstrainerQuery.java
//
//     A BallotConstrainerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.rules.ballotconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BallotConstrainerQuery that stores terms.
 */

public abstract class AbstractAssemblyBallotConstrainerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidConstrainerQuery
    implements org.osid.voting.rules.BallotConstrainerQuery,
               org.osid.voting.rules.BallotConstrainerQueryInspector,
               org.osid.voting.rules.BallotConstrainerSearchOrder {

    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBallotConstrainerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBallotConstrainerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches rules mapped to a ballot. 
     *
     *  @param  ballotId the ballot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ballotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBallotId(org.osid.id.Id ballotId, boolean match) {
        getAssembler().addIdTerm(getRuledBallotIdColumn(), ballotId, match);
        return;
    }


    /**
     *  Clears the ballot <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBallotIdTerms() {
        getAssembler().clearTerms(getRuledBallotIdColumn());
        return;
    }


    /**
     *  Gets the ballot <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledBallotIdTerms() {
        return (getAssembler().getIdTerms(getRuledBallotIdColumn()));
    }


    /**
     *  Gets the RuledBallotId column name.
     *
     * @return the column name
     */

    protected String getRuledBallotIdColumn() {
        return ("ruled_ballot_id");
    }


    /**
     *  Tests if a <code> BallotQuery </code> is available. 
     *
     *  @return <code> true </code> if a ballot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBallotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ballot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ballot query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBallotQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuery getRuledBallotQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBallotQuery() is false");
    }


    /**
     *  Matches rules mapped to any ballot. 
     *
     *  @param  match <code> true </code> for rules mapped to any ballot, 
     *          <code> false </code> to match rules mapped to no ballots 
     */

    @OSID @Override
    public void matchAnyRuledBallot(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledBallotColumn(), match);
        return;
    }


    /**
     *  Clears the ballot query terms. 
     */

    @OSID @Override
    public void clearRuledBallotTerms() {
        getAssembler().clearTerms(getRuledBallotColumn());
        return;
    }


    /**
     *  Gets the ballot query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.BallotQueryInspector[] getRuledBallotTerms() {
        return (new org.osid.voting.BallotQueryInspector[0]);
    }


    /**
     *  Gets the RuledBallot column name.
     *
     * @return the column name
     */

    protected String getRuledBallotColumn() {
        return ("ruled_ballot");
    }


    /**
     *  Matches rules mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        getAssembler().addIdTerm(getPollsIdColumn(), pollsId, match);
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        getAssembler().clearTerms(getPollsIdColumn());
        return;
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (getAssembler().getIdTerms(getPollsIdColumn()));
    }


    /**
     *  Gets the PollsId column name.
     *
     * @return the column name
     */

    protected String getPollsIdColumn() {
        return ("polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        getAssembler().clearTerms(getPollsColumn());
        return;
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the Polls column name.
     *
     * @return the column name
     */

    protected String getPollsColumn() {
        return ("polls");
    }


    /**
     *  Tests if this ballotConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  ballotConstrainerRecordType a ballot constrainer record type 
     *  @return <code>true</code> if the ballotConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type ballotConstrainerRecordType) {
        for (org.osid.voting.rules.records.BallotConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ballotConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  ballotConstrainerRecordType the ballot constrainer record type 
     *  @return the ballot constrainer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerQueryRecord getBallotConstrainerQueryRecord(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.BallotConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ballotConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  ballotConstrainerRecordType the ballot constrainer record type 
     *  @return the ballot constrainer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerQueryInspectorRecord getBallotConstrainerQueryInspectorRecord(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.BallotConstrainerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(ballotConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param ballotConstrainerRecordType the ballot constrainer record type
     *  @return the ballot constrainer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerSearchOrderRecord getBallotConstrainerSearchOrderRecord(org.osid.type.Type ballotConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.BallotConstrainerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(ballotConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this ballot constrainer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param ballotConstrainerQueryRecord the ballot constrainer query record
     *  @param ballotConstrainerQueryInspectorRecord the ballot constrainer query inspector
     *         record
     *  @param ballotConstrainerSearchOrderRecord the ballot constrainer search order record
     *  @param ballotConstrainerRecordType ballot constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerQueryRecord</code>,
     *          <code>ballotConstrainerQueryInspectorRecord</code>,
     *          <code>ballotConstrainerSearchOrderRecord</code> or
     *          <code>ballotConstrainerRecordTypeballotConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addBallotConstrainerRecords(org.osid.voting.rules.records.BallotConstrainerQueryRecord ballotConstrainerQueryRecord, 
                                      org.osid.voting.rules.records.BallotConstrainerQueryInspectorRecord ballotConstrainerQueryInspectorRecord, 
                                      org.osid.voting.rules.records.BallotConstrainerSearchOrderRecord ballotConstrainerSearchOrderRecord, 
                                      org.osid.type.Type ballotConstrainerRecordType) {

        addRecordType(ballotConstrainerRecordType);

        nullarg(ballotConstrainerQueryRecord, "ballot constrainer query record");
        nullarg(ballotConstrainerQueryInspectorRecord, "ballot constrainer query inspector record");
        nullarg(ballotConstrainerSearchOrderRecord, "ballot constrainer search odrer record");

        this.queryRecords.add(ballotConstrainerQueryRecord);
        this.queryInspectorRecords.add(ballotConstrainerQueryInspectorRecord);
        this.searchOrderRecords.add(ballotConstrainerSearchOrderRecord);
        
        return;
    }
}
