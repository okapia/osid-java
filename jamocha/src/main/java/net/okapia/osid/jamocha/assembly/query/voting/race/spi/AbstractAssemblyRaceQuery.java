//
// AbstractAssemblyRaceQuery.java
//
//     A RaceQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.race.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RaceQuery that stores terms.
 */

public abstract class AbstractAssemblyRaceQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.voting.RaceQuery,
               org.osid.voting.RaceQueryInspector,
               org.osid.voting.RaceSearchOrder {

    private final java.util.Collection<org.osid.voting.records.RaceQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.records.RaceQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.records.RaceSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRaceQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRaceQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the ballot <code> Id </code> for this query. 
     *
     *  @param  ballotId a ballot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ballotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBallotId(org.osid.id.Id ballotId, boolean match) {
        getAssembler().addIdTerm(getBallotIdColumn(), ballotId, match);
        return;
    }


    /**
     *  Clears the ballot <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBallotIdTerms() {
        getAssembler().clearTerms(getBallotIdColumn());
        return;
    }


    /**
     *  Gets the ballot <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBallotIdTerms() {
        return (getAssembler().getIdTerms(getBallotIdColumn()));
    }


    /**
     *  Orders the results by the ballot. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBallot(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getBallotColumn(), style);
        return;
    }


    /**
     *  Gets the BallotId column name.
     *
     * @return the column name
     */

    protected String getBallotIdColumn() {
        return ("ballot_id");
    }


    /**
     *  Tests if a <code> BallotQuery </code> is available. 
     *
     *  @return <code> true </code> if a ballot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ballot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ballot query 
     *  @throws org.osid.UnimplementedException <code> supportsBallotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuery getBallotQuery() {
        throw new org.osid.UnimplementedException("supportsBallotQuery() is false");
    }


    /**
     *  Clears the ballot terms. 
     */

    @OSID @Override
    public void clearBallotTerms() {
        getAssembler().clearTerms(getBallotColumn());
        return;
    }


    /**
     *  Gets the ballot query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.BallotQueryInspector[] getBallotTerms() {
        return (new org.osid.voting.BallotQueryInspector[0]);
    }


    /**
     *  Tests if a ballot search order is available. 
     *
     *  @return <code> true </code> if a ballot search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotSearchOrder() {
        return (false);
    }


    /**
     *  Gets the ballot search order. 
     *
     *  @return the ballot search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsBallotSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotSearchOrder getBallotSearchOrder() {
        throw new org.osid.UnimplementedException("supportsBallotSearchOrder() is false");
    }


    /**
     *  Gets the Ballot column name.
     *
     * @return the column name
     */

    protected String getBallotColumn() {
        return ("ballot");
    }


    /**
     *  Sets the candidate <code> Id </code> for this query. 
     *
     *  @param  candidateId a candidate <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> candidateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCandidateId(org.osid.id.Id candidateId, boolean match) {
        getAssembler().addIdTerm(getCandidateIdColumn(), candidateId, match);
        return;
    }


    /**
     *  Clears the candidate <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCandidateIdTerms() {
        getAssembler().clearTerms(getCandidateIdColumn());
        return;
    }


    /**
     *  Gets the candidate <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCandidateIdTerms() {
        return (getAssembler().getIdTerms(getCandidateIdColumn()));
    }


    /**
     *  Gets the CandidateId column name.
     *
     * @return the column name
     */

    protected String getCandidateIdColumn() {
        return ("candidate_id");
    }


    /**
     *  Tests if a <code> CandidateQuery </code> is available. 
     *
     *  @return <code> true </code> if a candidate query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a candidate. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the candidate query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuery getCandidateQuery() {
        throw new org.osid.UnimplementedException("supportsCandidateQuery() is false");
    }


    /**
     *  Matches polls with any candidate. 
     *
     *  @param  match <code> true </code> to match polls with any candidate, 
     *          <code> false </code> to match polls with no candidates 
     */

    @OSID @Override
    public void matchAnyCandidate(boolean match) {
        getAssembler().addIdWildcardTerm(getCandidateColumn(), match);
        return;
    }


    /**
     *  Clears the candidate terms. 
     */

    @OSID @Override
    public void clearCandidateTerms() {
        getAssembler().clearTerms(getCandidateColumn());
        return;
    }


    /**
     *  Gets the candidate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.CandidateQueryInspector[] getCandidateTerms() {
        return (new org.osid.voting.CandidateQueryInspector[0]);
    }


    /**
     *  Gets the Candidate column name.
     *
     * @return the column name
     */

    protected String getCandidateColumn() {
        return ("candidate");
    }


    /**
     *  Sets the polls <code> Id </code> for this query. 
     *
     *  @param  pollsid a polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsid, boolean match) {
        getAssembler().addIdTerm(getPollsIdColumn(), pollsid, match);
        return;
    }


    /**
     *  Clears the polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        getAssembler().clearTerms(getPollsIdColumn());
        return;
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (getAssembler().getIdTerms(getPollsIdColumn()));
    }


    /**
     *  Gets the PollsId column name.
     *
     * @return the column name
     */

    protected String getPollsIdColumn() {
        return ("polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        getAssembler().clearTerms(getPollsColumn());
        return;
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the Polls column name.
     *
     * @return the column name
     */

    protected String getPollsColumn() {
        return ("polls");
    }


    /**
     *  Tests if this race supports the given record
     *  <code>Type</code>.
     *
     *  @param  raceRecordType a race record type 
     *  @return <code>true</code> if the raceRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type raceRecordType) {
        for (org.osid.voting.records.RaceQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  raceRecordType the race record type 
     *  @return the race query record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.RaceQueryRecord getRaceQueryRecord(org.osid.type.Type raceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.RaceQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(raceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  raceRecordType the race record type 
     *  @return the race query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.RaceQueryInspectorRecord getRaceQueryInspectorRecord(org.osid.type.Type raceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.RaceQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(raceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param raceRecordType the race record type
     *  @return the race search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.RaceSearchOrderRecord getRaceSearchOrderRecord(org.osid.type.Type raceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.RaceSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(raceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this race. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param raceQueryRecord the race query record
     *  @param raceQueryInspectorRecord the race query inspector
     *         record
     *  @param raceSearchOrderRecord the race search order record
     *  @param raceRecordType race record type
     *  @throws org.osid.NullArgumentException
     *          <code>raceQueryRecord</code>,
     *          <code>raceQueryInspectorRecord</code>,
     *          <code>raceSearchOrderRecord</code> or
     *          <code>raceRecordTyperace</code> is
     *          <code>null</code>
     */
            
    protected void addRaceRecords(org.osid.voting.records.RaceQueryRecord raceQueryRecord, 
                                      org.osid.voting.records.RaceQueryInspectorRecord raceQueryInspectorRecord, 
                                      org.osid.voting.records.RaceSearchOrderRecord raceSearchOrderRecord, 
                                      org.osid.type.Type raceRecordType) {

        addRecordType(raceRecordType);

        nullarg(raceQueryRecord, "race query record");
        nullarg(raceQueryInspectorRecord, "race query inspector record");
        nullarg(raceSearchOrderRecord, "race search odrer record");

        this.queryRecords.add(raceQueryRecord);
        this.queryInspectorRecords.add(raceQueryInspectorRecord);
        this.searchOrderRecords.add(raceSearchOrderRecord);
        
        return;
    }
}
