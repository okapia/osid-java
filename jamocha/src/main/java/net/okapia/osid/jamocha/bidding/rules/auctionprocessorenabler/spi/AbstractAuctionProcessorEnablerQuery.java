//
// AbstractAuctionProcessorEnablerQuery.java
//
//     A template for making an AuctionProcessorEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for auction processor enablers.
 */

public abstract class AbstractAuctionProcessorEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.bidding.rules.AuctionProcessorEnablerQuery {

    private final java.util.Collection<org.osid.bidding.rules.records.AuctionProcessorEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the auction processor. 
     *
     *  @param  auctionProcessorId the auction <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionProcessorId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuctionProcessorId(org.osid.id.Id auctionProcessorId, 
                                             boolean match) {
        return;
    }


    /**
     *  Clears the auction processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionProcessorIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuctionProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction processor query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuctionProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuctionProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorQuery getRuledAuctionProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuctionProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any auction processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any auction 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          auction processors 
     */

    @OSID @Override
    public void matchAnyRuledAuctionProcessor(boolean match) {
        return;
    }


    /**
     *  Clears the auction processor query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionProcessorTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the auction house. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given auction processor enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction processor enabler implementing the requested record.
     *
     *  @param auctionProcessorEnablerRecordType an auction processor enabler record type
     *  @return the auction processor enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionProcessorEnablerQueryRecord getAuctionProcessorEnablerQueryRecord(org.osid.type.Type auctionProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionProcessorEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(auctionProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction processor enabler query. 
     *
     *  @param auctionProcessorEnablerQueryRecord auction processor enabler query record
     *  @param auctionProcessorEnablerRecordType auctionProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionProcessorEnablerQueryRecord(org.osid.bidding.rules.records.AuctionProcessorEnablerQueryRecord auctionProcessorEnablerQueryRecord, 
                                          org.osid.type.Type auctionProcessorEnablerRecordType) {

        addRecordType(auctionProcessorEnablerRecordType);
        nullarg(auctionProcessorEnablerQueryRecord, "auction processor enabler query record");
        this.records.add(auctionProcessorEnablerQueryRecord);        
        return;
    }
}
