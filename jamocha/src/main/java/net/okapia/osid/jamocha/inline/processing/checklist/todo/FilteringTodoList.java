//
// FilteringTodoList.java
//
//     Filters todos according to a rules evaluation. 
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.processing.checklist.todo;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Filters todos according to a rules evaluation. The rules
 *  evaluation is performed for each todo in the TodoList. The
 *  boolean result from <code>RulesSession.evaluateRule(ruleId)</code>
 *  is used to pass or filter the Todo. The mapping among
 *  Todos and Rules is specified in the constructor.
 *
 *  The rules evaluation is performed synchronously with each access
 *  to this list. If asynchronous evaluation is desired, wrap this
 *  list inside a <code>BufferedTodoList</code>.
 */

public final class FilteringTodoList
    extends net.okapia.osid.jamocha.inline.filter.checklist.todo.spi.AbstractTodoFilterList
    implements org.osid.checklist.TodoList {

    private final boolean passIfNone;
    private final org.osid.rules.RulesSession session;
    private final java.util.Map<org.osid.id.Id, java.util.Collection<org.osid.id.Id>> rules;


    /**
     *  Creates a new <code>FilteringTodoList</code>.
     *
     *  @param todoList a <code>TodoList</code> to filter
     *  @param rulesSession a <code>RulesSession</code> to use for
     *         evaluating rules
     *  @param rules a map (todoId, ruleId) 
     *  @param passIfNone <code>true</code> to pass todos that do
     *         not exist in the map, <code>false</code> to filter
     *         todos not defined in the map
     *  @throws org.osid.NullArgumentException
     *          <code>todoList</code>, <code>rulesSession</code>,
     *          or <code>rules</code> is <code>null</code>
     */

    public FilteringTodoList(org.osid.checklist.TodoList todoList, 
                                 org.osid.rules.RulesSession rulesSession,
                                 java.util.Map<org.osid.id.Id, java.util.Collection<org.osid.id.Id>> rules, 
                                 boolean passIfNone) {

        super(todoList);

        nullarg(rulesSession, "rules session");
        nullarg(rules, "rules map");

        this.session    = rulesSession;
        this.rules      = rules;
        this.passIfNone = passIfNone;

        return;
    }


    /**
     *  Filters Todos based on rules evaluation.
     *
     *  @param todo the todo to filter
     *  @return <code>true</code> if the todo passes the filter,
     *          <code>false</code> if the todo should be filtered
     */

    @Override
    public boolean pass(org.osid.checklist.Todo todo) {

        java.util.Collection<org.osid.id.Id> set = this.rules.get(todo.getId());
        if ((set == null) || (set.size() == 0)) {
            return (this.passIfNone);
        }
        
        for (org.osid.id.Id id : set) {
            try {
                if (!this.session.evaluateRule(id, this.session.getConditionForRule(id))) {
                    return (false);
                }

            } catch (org.osid.OsidException oe) {
                error(oe);
                return (true);
            }
        }
        
        return (true);
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.session.close();
        this.rules.clear();
        super.close();

        return;
    }
}
