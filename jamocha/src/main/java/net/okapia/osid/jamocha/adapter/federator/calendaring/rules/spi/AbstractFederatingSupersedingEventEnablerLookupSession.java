//
// AbstractFederatingSupersedingEventEnablerLookupSession.java
//
//     An abstract federating adapter for a SupersedingEventEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SupersedingEventEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSupersedingEventEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.calendaring.rules.SupersedingEventEnablerLookupSession>
    implements org.osid.calendaring.rules.SupersedingEventEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.calendaring.Calendar calendar = new net.okapia.osid.jamocha.nil.calendaring.calendar.UnknownCalendar();


    /**
     *  Constructs a new <code>AbstractFederatingSupersedingEventEnablerLookupSession</code>.
     */

    protected AbstractFederatingSupersedingEventEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Calendar/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.calendar.getId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.calendar);
    }


    /**
     *  Sets the <code>Calendar</code>.
     *
     *  @param  calendar the calendar for this session
     *  @throws org.osid.NullArgumentException <code>calendar</code>
     *          is <code>null</code>
     */

    protected void setCalendar(org.osid.calendaring.Calendar calendar) {
        nullarg(calendar, "calendar");
        this.calendar = calendar;
        return;
    }


    /**
     *  Tests if this user can perform <code>SupersedingEventEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSupersedingEventEnablers() {
        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            if (session.canLookupSupersedingEventEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>SupersedingEventEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSupersedingEventEnablerView() {
        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            session.useComparativeSupersedingEventEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>SupersedingEventEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySupersedingEventEnablerView() {
        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            session.usePlenarySupersedingEventEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include superseding event enablers in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            session.useFederatedCalendarView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            session.useIsolatedCalendarView();
        }

        return;
    }


    /**
     *  Only active superseding event enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSupersedingEventEnablerView() {
        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            session.useActiveSupersedingEventEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive superseding event enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSupersedingEventEnablerView() {
        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            session.useAnyStatusSupersedingEventEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>SupersedingEventEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SupersedingEventEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>SupersedingEventEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, superseding event enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  superseding event enablers are returned.
     *
     *  @param  supersedingEventEnablerId <code>Id</code> of the
     *          <code>SupersedingEventEnabler</code>
     *  @return the superseding event enabler
     *  @throws org.osid.NotFoundException <code>supersedingEventEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>supersedingEventEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnabler getSupersedingEventEnabler(org.osid.id.Id supersedingEventEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            try {
                return (session.getSupersedingEventEnabler(supersedingEventEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(supersedingEventEnablerId + " not found");
    }


    /**
     *  Gets a <code>SupersedingEventEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  supersedingEventEnablers specified in the <code>Id</code>
     *  list, in the order of the list, including duplicates, or an
     *  error results if an <code>Id</code> in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible
     *  <code>SupersedingEventEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, superseding event enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  superseding event enablers are returned.
     *
     *  @param  supersedingEventEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SupersedingEventEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByIds(org.osid.id.IdList supersedingEventEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.MutableSupersedingEventEnablerList ret = new net.okapia.osid.jamocha.calendaring.rules.supersedingeventenabler.MutableSupersedingEventEnablerList();

        try (org.osid.id.IdList ids = supersedingEventEnablerIds) {
            while (ids.hasNext()) {
                ret.addSupersedingEventEnabler(getSupersedingEventEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SupersedingEventEnablerList</code> corresponding to the given
     *  superseding event enabler genus <code>Type</code> which does not include
     *  superseding event enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the
     *  returned list may contain only those superseding event
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, superseding event enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  superseding event enablers are returned.
     *
     *  @param  supersedingEventEnablerGenusType a supersedingEventEnabler genus type 
     *  @return the returned <code>SupersedingEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByGenusType(org.osid.type.Type supersedingEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.rules.supersedingeventenabler.FederatingSupersedingEventEnablerList ret = getSupersedingEventEnablerList();

        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            ret.addSupersedingEventEnablerList(session.getSupersedingEventEnablersByGenusType(supersedingEventEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SupersedingEventEnablerList</code> corresponding to the given
     *  superseding event enabler genus <code>Type</code> and include any additional
     *  superseding event enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the
     *  returned list may contain only those superseding event
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, superseding event enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  superseding event enablers are returned.
     *
     *  @param  supersedingEventEnablerGenusType a supersedingEventEnabler genus type 
     *  @return the returned <code>SupersedingEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByParentGenusType(org.osid.type.Type supersedingEventEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.rules.supersedingeventenabler.FederatingSupersedingEventEnablerList ret = getSupersedingEventEnablerList();

        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            ret.addSupersedingEventEnablerList(session.getSupersedingEventEnablersByParentGenusType(supersedingEventEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SupersedingEventEnablerList</code> containing the given
     *  superseding event enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the
     *  returned list may contain only those superseding event
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, superseding event enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  superseding event enablers are returned.
     *
     *  @param  supersedingEventEnablerRecordType a supersedingEventEnabler record type 
     *  @return the returned <code>SupersedingEventEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersByRecordType(org.osid.type.Type supersedingEventEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.rules.supersedingeventenabler.FederatingSupersedingEventEnablerList ret = getSupersedingEventEnablerList();

        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            ret.addSupersedingEventEnablerList(session.getSupersedingEventEnablersByRecordType(supersedingEventEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SupersedingEventEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the
     *  returned list may contain only those superseding event
     *  enablers that are accessible through this session.
     *  
     *  In active mode, superseding event enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  superseding event enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>SupersedingEventEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.rules.supersedingeventenabler.FederatingSupersedingEventEnablerList ret = getSupersedingEventEnablerList();

        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            ret.addSupersedingEventEnablerList(session.getSupersedingEventEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>SupersedingEventEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the
     *  returned list may contain only those superseding event
     *  enablers that are accessible through this session.
     *
     *  In active mode, superseding event enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  superseding event enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>SupersedingEventEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.rules.supersedingeventenabler.FederatingSupersedingEventEnablerList ret = getSupersedingEventEnablerList();

        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            ret.addSupersedingEventEnablerList(session.getSupersedingEventEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>SupersedingEventEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  superseding event enablers or an error results. Otherwise, the
     *  returned list may contain only those superseding event
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, superseding event enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  superseding event enablers are returned.
     *
     *  @return a list of <code>SupersedingEventEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.SupersedingEventEnablerList getSupersedingEventEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.calendaring.rules.supersedingeventenabler.FederatingSupersedingEventEnablerList ret = getSupersedingEventEnablerList();

        for (org.osid.calendaring.rules.SupersedingEventEnablerLookupSession session : getSessions()) {
            ret.addSupersedingEventEnablerList(session.getSupersedingEventEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.calendaring.rules.supersedingeventenabler.FederatingSupersedingEventEnablerList getSupersedingEventEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.rules.supersedingeventenabler.ParallelSupersedingEventEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.calendaring.rules.supersedingeventenabler.CompositeSupersedingEventEnablerList());
        }
    }
}
