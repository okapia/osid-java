//
// AbstractAssemblyDispatchQuery.java
//
//     A DispatchQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.subscription.dispatch.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DispatchQuery that stores terms.
 */

public abstract class AbstractAssemblyDispatchQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidGovernatorQuery
    implements org.osid.subscription.DispatchQuery,
               org.osid.subscription.DispatchQueryInspector,
               org.osid.subscription.DispatchSearchOrder {

    private final java.util.Collection<org.osid.subscription.records.DispatchQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.subscription.records.DispatchQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.subscription.records.DispatchSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDispatchQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDispatchQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches an address genus <code> Type </code> accepted by a dispatch. 
     *
     *  @param  addressGenusType an address genus <code> Type </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressGenusType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAddressGenusType(org.osid.type.Type addressGenusType, 
                                      boolean match) {
        getAssembler().addTypeTerm(getAddressGenusTypeColumn(), addressGenusType, match);
        return;
    }


    /**
     *  Matches dispatches with any address genus type. 
     *
     *  @param  match <code> true </code> to match dispatches with any addres 
     *          sgenus type, <code> false </code> to match dispatches with no 
     *          address genus types 
     */

    @OSID @Override
    public void matchAnyAddressGenusType(boolean match) {
        getAssembler().addTypeWildcardTerm(getAddressGenusTypeColumn(), match);
        return;
    }


    /**
     *  Clears the address genus <code> Type </code> terms. 
     */

    @OSID @Override
    public void clearAddressGenusTypeTerms() {
        getAssembler().clearTerms(getAddressGenusTypeColumn());
        return;
    }


    /**
     *  Gets the address genus <code> Type </code> terms. 
     *
     *  @return the address genus <code> Type </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getAddressGenusTypeTerms() {
        return (getAssembler().getTypeTerms(getAddressGenusTypeColumn()));
    }


    /**
     *  Gets the AddressGenusType column name.
     *
     * @return the column name
     */

    protected String getAddressGenusTypeColumn() {
        return ("address_genus_type");
    }


    /**
     *  Sets the subscription <code> Id </code> for this query to match 
     *  subscriptions assigned to dispatches. 
     *
     *  @param  subscriptionId a subscription <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subscriptionId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchSubscriptionId(org.osid.id.Id subscriptionId, 
                                    boolean match) {
        getAssembler().addIdTerm(getSubscriptionIdColumn(), subscriptionId, match);
        return;
    }


    /**
     *  Clears the subscription <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSubscriptionIdTerms() {
        getAssembler().clearTerms(getSubscriptionIdColumn());
        return;
    }


    /**
     *  Gets the subscription <code> Id </code> terms. 
     *
     *  @return the subscription <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubscriptionIdTerms() {
        return (getAssembler().getIdTerms(getSubscriptionIdColumn()));
    }


    /**
     *  Gets the SubscriptionId column name.
     *
     * @return the column name
     */

    protected String getSubscriptionIdColumn() {
        return ("subscription_id");
    }


    /**
     *  Tests if a subscription query is available. 
     *
     *  @return <code> true </code> if a subscription query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a dispatch. 
     *
     *  @return the subscription query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuery getSubscriptionQuery() {
        throw new org.osid.UnimplementedException("supportsSubscriptionQuery() is false");
    }


    /**
     *  Matches dispatches with any subscription. 
     *
     *  @param  match <code> true </code> to match dispatches with any 
     *          subscription, <code> false </code> to match dispatches with no 
     *          subscriptions 
     */

    @OSID @Override
    public void matchAnySubscription(boolean match) {
        getAssembler().addIdWildcardTerm(getSubscriptionColumn(), match);
        return;
    }


    /**
     *  Clears the subscription terms. 
     */

    @OSID @Override
    public void clearSubscriptionTerms() {
        getAssembler().clearTerms(getSubscriptionColumn());
        return;
    }


    /**
     *  Gets the subscription terms. 
     *
     *  @return the subscription terms 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQueryInspector[] getSubscriptionTerms() {
        return (new org.osid.subscription.SubscriptionQueryInspector[0]);
    }


    /**
     *  Gets the Subscription column name.
     *
     * @return the column name
     */

    protected String getSubscriptionColumn() {
        return ("subscription");
    }


    /**
     *  Sets the dispatch <code> Id </code> for this query to match 
     *  subscriptions assigned to publishers. 
     *
     *  @param  publisherId a publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPublisherId(org.osid.id.Id publisherId, boolean match) {
        getAssembler().addIdTerm(getPublisherIdColumn(), publisherId, match);
        return;
    }


    /**
     *  Clears the publisher <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPublisherIdTerms() {
        getAssembler().clearTerms(getPublisherIdColumn());
        return;
    }


    /**
     *  Gets the publisher <code> Id </code> terms. 
     *
     *  @return the publisher <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPublisherIdTerms() {
        return (getAssembler().getIdTerms(getPublisherIdColumn()));
    }


    /**
     *  Gets the PublisherId column name.
     *
     * @return the column name
     */

    protected String getPublisherIdColumn() {
        return ("publisher_id");
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsPublisherQuery() is false");
    }


    /**
     *  Clears the publisher terms. 
     */

    @OSID @Override
    public void clearPublisherTerms() {
        getAssembler().clearTerms(getPublisherColumn());
        return;
    }


    /**
     *  Gets the publisher terms. 
     *
     *  @return the publisher terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }


    /**
     *  Gets the Publisher column name.
     *
     * @return the column name
     */

    protected String getPublisherColumn() {
        return ("publisher");
    }


    /**
     *  Tests if this dispatch supports the given record
     *  <code>Type</code>.
     *
     *  @param  dispatchRecordType a dispatch record type 
     *  @return <code>true</code> if the dispatchRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type dispatchRecordType) {
        for (org.osid.subscription.records.DispatchQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(dispatchRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  dispatchRecordType the dispatch record type 
     *  @return the dispatch query record 
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dispatchRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.DispatchQueryRecord getDispatchQueryRecord(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.DispatchQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(dispatchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dispatchRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  dispatchRecordType the dispatch record type 
     *  @return the dispatch query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dispatchRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.DispatchQueryInspectorRecord getDispatchQueryInspectorRecord(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.DispatchQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(dispatchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dispatchRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param dispatchRecordType the dispatch record type
     *  @return the dispatch search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(dispatchRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.records.DispatchSearchOrderRecord getDispatchSearchOrderRecord(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.records.DispatchSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(dispatchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(dispatchRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this dispatch. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param dispatchQueryRecord the dispatch query record
     *  @param dispatchQueryInspectorRecord the dispatch query inspector
     *         record
     *  @param dispatchSearchOrderRecord the dispatch search order record
     *  @param dispatchRecordType dispatch record type
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchQueryRecord</code>,
     *          <code>dispatchQueryInspectorRecord</code>,
     *          <code>dispatchSearchOrderRecord</code> or
     *          <code>dispatchRecordTypedispatch</code> is
     *          <code>null</code>
     */
            
    protected void addDispatchRecords(org.osid.subscription.records.DispatchQueryRecord dispatchQueryRecord, 
                                      org.osid.subscription.records.DispatchQueryInspectorRecord dispatchQueryInspectorRecord, 
                                      org.osid.subscription.records.DispatchSearchOrderRecord dispatchSearchOrderRecord, 
                                      org.osid.type.Type dispatchRecordType) {

        addRecordType(dispatchRecordType);

        nullarg(dispatchQueryRecord, "dispatch query record");
        nullarg(dispatchQueryInspectorRecord, "dispatch query inspector record");
        nullarg(dispatchSearchOrderRecord, "dispatch search odrer record");

        this.queryRecords.add(dispatchQueryRecord);
        this.queryInspectorRecords.add(dispatchQueryInspectorRecord);
        this.searchOrderRecords.add(dispatchSearchOrderRecord);
        
        return;
    }
}
