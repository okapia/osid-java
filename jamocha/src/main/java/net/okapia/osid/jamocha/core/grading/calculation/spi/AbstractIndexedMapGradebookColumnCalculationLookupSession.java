//
// AbstractIndexedMapGradebookColumnCalculationLookupSession.java
//
//    A simple framework for providing a GradebookColumnCalculation lookup service
//    backed by a fixed collection of gradebook column calculations with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.grading.calculation.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a GradebookColumnCalculation lookup service backed by a
 *  fixed collection of gradebook column calculations. The gradebook column calculations are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some gradebook column calculations may be compatible
 *  with more types than are indicated through these gradebook column calculation
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>GradebookColumnCalculations</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapGradebookColumnCalculationLookupSession
    extends AbstractMapGradebookColumnCalculationLookupSession
    implements org.osid.grading.calculation.GradebookColumnCalculationLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.grading.calculation.GradebookColumnCalculation> gradebookColumnCalculationsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.calculation.GradebookColumnCalculation>());
    private final MultiMap<org.osid.type.Type, org.osid.grading.calculation.GradebookColumnCalculation> gradebookColumnCalculationsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.grading.calculation.GradebookColumnCalculation>());


    /**
     *  Makes a <code>GradebookColumnCalculation</code> available in this session.
     *
     *  @param  gradebookColumnCalculation a gradebook column calculation
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculation<code> is
     *          <code>null</code>
     */

    @Override
    protected void putGradebookColumnCalculation(org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation) {
        super.putGradebookColumnCalculation(gradebookColumnCalculation);

        this.gradebookColumnCalculationsByGenus.put(gradebookColumnCalculation.getGenusType(), gradebookColumnCalculation);
        
        try (org.osid.type.TypeList types = gradebookColumnCalculation.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradebookColumnCalculationsByRecord.put(types.getNextType(), gradebookColumnCalculation);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a gradebook column calculation from this session.
     *
     *  @param gradebookColumnCalculationId the <code>Id</code> of the gradebook column calculation
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculationId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeGradebookColumnCalculation(org.osid.id.Id gradebookColumnCalculationId) {
        org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation;
        try {
            gradebookColumnCalculation = getGradebookColumnCalculation(gradebookColumnCalculationId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.gradebookColumnCalculationsByGenus.remove(gradebookColumnCalculation.getGenusType());

        try (org.osid.type.TypeList types = gradebookColumnCalculation.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.gradebookColumnCalculationsByRecord.remove(types.getNextType(), gradebookColumnCalculation);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeGradebookColumnCalculation(gradebookColumnCalculationId);
        return;
    }


    /**
     *  Gets a <code>GradebookColumnCalculationList</code> corresponding to the given
     *  gradebook column calculation genus <code>Type</code> which does not include
     *  gradebook column calculations of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known gradebook column calculations or an error results. Otherwise,
     *  the returned list may contain only those gradebook column calculations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  gradebookColumnCalculationGenusType a gradebook column calculation genus type 
     *  @return the returned <code>GradebookColumnCalculation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnCalculationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByGenusType(org.osid.type.Type gradebookColumnCalculationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.calculation.gradebookcolumncalculation.ArrayGradebookColumnCalculationList(this.gradebookColumnCalculationsByGenus.get(gradebookColumnCalculationGenusType)));
    }


    /**
     *  Gets a <code>GradebookColumnCalculationList</code> containing the given
     *  gradebook column calculation record <code>Type</code>. In plenary mode, the
     *  returned list contains all known gradebook column calculations or an error
     *  results. Otherwise, the returned list may contain only those
     *  gradebook column calculations that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  gradebookColumnCalculationRecordType a gradebook column calculation record type 
     *  @return the returned <code>gradebookColumnCalculation</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnCalculationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.calculation.GradebookColumnCalculationList getGradebookColumnCalculationsByRecordType(org.osid.type.Type gradebookColumnCalculationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.grading.calculation.gradebookcolumncalculation.ArrayGradebookColumnCalculationList(this.gradebookColumnCalculationsByRecord.get(gradebookColumnCalculationRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.gradebookColumnCalculationsByGenus.clear();
        this.gradebookColumnCalculationsByRecord.clear();

        super.close();

        return;
    }
}
