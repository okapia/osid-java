//
// AppointmentElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.appointment.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AppointmentElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the AppointmentElement Id.
     *
     *  @return the appointment element Id
     */

    public static org.osid.id.Id getAppointmentEntityId() {
        return (makeEntityId("osid.personnel.Appointment"));
    }


    /**
     *  Gets the PersonId element Id.
     *
     *  @return the PersonId element Id
     */

    public static org.osid.id.Id getPersonId() {
        return (makeElementId("osid.personnel.appointment.PersonId"));
    }


    /**
     *  Gets the Person element Id.
     *
     *  @return the Person element Id
     */

    public static org.osid.id.Id getPerson() {
        return (makeElementId("osid.personnel.appointment.Person"));
    }


    /**
     *  Gets the PositionId element Id.
     *
     *  @return the PositionId element Id
     */

    public static org.osid.id.Id getPositionId() {
        return (makeElementId("osid.personnel.appointment.PositionId"));
    }


    /**
     *  Gets the Position element Id.
     *
     *  @return the Position element Id
     */

    public static org.osid.id.Id getPosition() {
        return (makeElementId("osid.personnel.appointment.Position"));
    }


    /**
     *  Gets the Commitment element Id.
     *
     *  @return the Commitment element Id
     */

    public static org.osid.id.Id getCommitment() {
        return (makeElementId("osid.personnel.appointment.Commitment"));
    }


    /**
     *  Gets the Title element Id.
     *
     *  @return the Title element Id
     */

    public static org.osid.id.Id getTitle() {
        return (makeElementId("osid.personnel.appointment.Title"));
    }


    /**
     *  Gets the Salary element Id.
     *
     *  @return the Salary element Id
     */

    public static org.osid.id.Id getSalary() {
        return (makeElementId("osid.personnel.appointment.Salary"));
    }


    /**
     *  Gets the SalaryBasis element Id.
     *
     *  @return the SalaryBasis element Id
     */

    public static org.osid.id.Id getSalaryBasis() {
        return (makeElementId("osid.personnel.appointment.SalaryBasis"));
    }


    /**
     *  Gets the RealmId element Id.
     *
     *  @return the RealmId element Id
     */

    public static org.osid.id.Id getRealmId() {
        return (makeQueryElementId("osid.personnel.appointment.RealmId"));
    }


    /**
     *  Gets the Realm element Id.
     *
     *  @return the Realm element Id
     */

    public static org.osid.id.Id getRealm() {
        return (makeQueryElementId("osid.personnel.appointment.Realm"));
    }
}
