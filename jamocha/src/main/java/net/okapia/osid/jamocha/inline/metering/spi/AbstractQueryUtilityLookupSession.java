//
// AbstractQueryUtilityLookupSession.java
//
//    An inline adapter that maps an UtilityLookupSession to
//    an UtilityQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.metering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an UtilityLookupSession to
 *  an UtilityQuerySession.
 */

public abstract class AbstractQueryUtilityLookupSession
    extends net.okapia.osid.jamocha.metering.spi.AbstractUtilityLookupSession
    implements org.osid.metering.UtilityLookupSession {

    private final org.osid.metering.UtilityQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryUtilityLookupSession.
     *
     *  @param querySession the underlying utility query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryUtilityLookupSession(org.osid.metering.UtilityQuerySession querySession) {
        nullarg(querySession, "utility query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Utility</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupUtilities() {
        return (this.session.canSearchUtilities());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Utility</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Utility</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Utility</code> and
     *  retained for compatibility.
     *
     *  @param  utilityId <code>Id</code> of the
     *          <code>Utility</code>
     *  @return the utility
     *  @throws org.osid.NotFoundException <code>utilityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>utilityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Utility getUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.UtilityQuery query = getQuery();
        query.matchId(utilityId, true);
        org.osid.metering.UtilityList utilities = this.session.getUtilitiesByQuery(query);
        if (utilities.hasNext()) {
            return (utilities.getNextUtility());
        } 
        
        throw new org.osid.NotFoundException(utilityId + " not found");
    }


    /**
     *  Gets an <code>UtilityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  utilities specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Utilities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  utilityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Utility</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>utilityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByIds(org.osid.id.IdList utilityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.UtilityQuery query = getQuery();

        try (org.osid.id.IdList ids = utilityIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getUtilitiesByQuery(query));
    }


    /**
     *  Gets an <code>UtilityList</code> corresponding to the given
     *  utility genus <code>Type</code> which does not include
     *  utilities of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  utilities or an error results. Otherwise, the returned list
     *  may contain only those utilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  utilityGenusType an utility genus type 
     *  @return the returned <code>Utility</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>utilityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByGenusType(org.osid.type.Type utilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.UtilityQuery query = getQuery();
        query.matchGenusType(utilityGenusType, true);
        return (this.session.getUtilitiesByQuery(query));
    }


    /**
     *  Gets an <code>UtilityList</code> corresponding to the given
     *  utility genus <code>Type</code> and include any additional
     *  utilities with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  utilities or an error results. Otherwise, the returned list
     *  may contain only those utilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  utilityGenusType an utility genus type 
     *  @return the returned <code>Utility</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>utilityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByParentGenusType(org.osid.type.Type utilityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.UtilityQuery query = getQuery();
        query.matchParentGenusType(utilityGenusType, true);
        return (this.session.getUtilitiesByQuery(query));
    }


    /**
     *  Gets an <code>UtilityList</code> containing the given
     *  utility record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  utilities or an error results. Otherwise, the returned list
     *  may contain only those utilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  utilityRecordType an utility record type 
     *  @return the returned <code>Utility</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByRecordType(org.osid.type.Type utilityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.UtilityQuery query = getQuery();
        query.matchRecordType(utilityRecordType, true);
        return (this.session.getUtilitiesByQuery(query));
    }


    /**
     *  Gets an <code>UtilityList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known utilities or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  utilities that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Utility</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilitiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.UtilityQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getUtilitiesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Utilities</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  utilities or an error results. Otherwise, the returned list
     *  may contain only those utilities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Utilities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.UtilityList getUtilities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.metering.UtilityQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getUtilitiesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.metering.UtilityQuery getQuery() {
        org.osid.metering.UtilityQuery query = this.session.getUtilityQuery();
        return (query);
    }
}
