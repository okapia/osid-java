//
// AbstractMailboxSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.mailbox.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractMailboxSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.messaging.MailboxSearchResults {

    private org.osid.messaging.MailboxList mailboxes;
    private final org.osid.messaging.MailboxQueryInspector inspector;
    private final java.util.Collection<org.osid.messaging.records.MailboxSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractMailboxSearchResults.
     *
     *  @param mailboxes the result set
     *  @param mailboxQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>mailboxes</code>
     *          or <code>mailboxQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractMailboxSearchResults(org.osid.messaging.MailboxList mailboxes,
                                            org.osid.messaging.MailboxQueryInspector mailboxQueryInspector) {
        nullarg(mailboxes, "mailboxes");
        nullarg(mailboxQueryInspector, "mailbox query inspectpr");

        this.mailboxes = mailboxes;
        this.inspector = mailboxQueryInspector;

        return;
    }


    /**
     *  Gets the mailbox list resulting from a search.
     *
     *  @return a mailbox list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.messaging.MailboxList getMailboxes() {
        if (this.mailboxes == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.messaging.MailboxList mailboxes = this.mailboxes;
        this.mailboxes = null;
	return (mailboxes);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.messaging.MailboxQueryInspector getMailboxQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  mailbox search record <code> Type. </code> This method must
     *  be used to retrieve a mailbox implementing the requested
     *  record.
     *
     *  @param mailboxSearchRecordType a mailbox search 
     *         record type 
     *  @return the mailbox search
     *  @throws org.osid.NullArgumentException
     *          <code>mailboxSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(mailboxSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.messaging.records.MailboxSearchResultsRecord getMailboxSearchResultsRecord(org.osid.type.Type mailboxSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.messaging.records.MailboxSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(mailboxSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(mailboxSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record mailbox search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addMailboxRecord(org.osid.messaging.records.MailboxSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "mailbox record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
