//
// AbstractFederatingPayerLookupSession.java
//
//     An abstract federating adapter for a PayerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PayerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPayerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.billing.payment.PayerLookupSession>
    implements org.osid.billing.payment.PayerLookupSession {

    private boolean parallel = false;
    private org.osid.billing.Business business = new net.okapia.osid.jamocha.nil.billing.business.UnknownBusiness();


    /**
     *  Constructs a new <code>AbstractFederatingPayerLookupSession</code>.
     */

    protected AbstractFederatingPayerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.billing.payment.PayerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.billing.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }


    /**
     *  Tests if this user can perform <code>Payer</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPayers() {
        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            if (session.canLookupPayers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Payer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePayerView() {
        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            session.useComparativePayerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Payer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPayerView() {
        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            session.usePlenaryPayerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include payers in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            session.useFederatedBusinessView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            session.useIsolatedBusinessView();
        }

        return;
    }


    /**
     *  Only payers whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectivePayerView() {
        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            session.useEffectivePayerView();
        }

        return;
    }


    /**
     *  All payers of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectivePayerView() {
        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            session.useAnyEffectivePayerView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Payer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Payer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Payer</code> and
     *  retained for compatibility.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  payerId <code>Id</code> of the
     *          <code>Payer</code>
     *  @return the payer
     *  @throws org.osid.NotFoundException <code>payerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>payerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.Payer getPayer(org.osid.id.Id payerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            try {
                return (session.getPayer(payerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(payerId + " not found");
    }


    /**
     *  Gets a <code>PayerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  payers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Payers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, payers are returned that are currently effective.
     *  In any effective mode, effective payers and those currently expired
     *  are returned.
     *
     *  @param  payerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Payer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>payerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByIds(org.osid.id.IdList payerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.billing.payment.payer.MutablePayerList ret = new net.okapia.osid.jamocha.billing.payment.payer.MutablePayerList();

        try (org.osid.id.IdList ids = payerIds) {
            while (ids.hasNext()) {
                ret.addPayer(getPayer(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PayerList</code> corresponding to the given
     *  payer genus <code>Type</code> which does not include
     *  payers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently effective.
     *  In any effective mode, effective payers and those currently expired
     *  are returned.
     *
     *  @param  payerGenusType a payer genus type 
     *  @return the returned <code>Payer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>payerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByGenusType(org.osid.type.Type payerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList ret = getPayerList();

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            ret.addPayerList(session.getPayersByGenusType(payerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PayerList</code> corresponding to the given
     *  payer genus <code>Type</code> and include any additional
     *  payers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  payerGenusType a payer genus type 
     *  @return the returned <code>Payer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>payerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByParentGenusType(org.osid.type.Type payerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList ret = getPayerList();

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            ret.addPayerList(session.getPayersByParentGenusType(payerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PayerList</code> containing the given
     *  payer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @param  payerRecordType a payer record type 
     *  @return the returned <code>Payer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>payerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByRecordType(org.osid.type.Type payerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList ret = getPayerList();

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            ret.addPayerList(session.getPayersByRecordType(payerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PayerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible
     *  through this session.
     *  
     *  In active mode, payers are returned that are currently
     *  active. In any status mode, active and inactive payers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Payer</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList ret = getPayerList();

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            ret.addPayerList(session.getPayersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>PayerList</code> related to the given resource.
     *
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @return the returned <code> PayerList </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
              org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList ret = getPayerList();

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            ret.addPayerList(session.getPayersByResource(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> PayerList </code> of the given resource and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *
     *  In effective mode, payers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code>
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code> PayerList </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByResourceOnDate(org.osid.id.Id resourceId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList ret = getPayerList();

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            ret.addPayerList(session.getPayersByResourceOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> PayerList </code> related to the given payer
     *  customer.
     *
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer <code> Id </code>
     *  @return the returned <code> PayerList </code> list
     *  @throws org.osid.NullArgumentException <code> customerId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByCustomer(org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList ret = getPayerList();

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            ret.addPayerList(session.getPayersByCustomer(customerId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code> PayerList </code> of the given customer and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known payers
     *  or an error results. Otherwise, the returned list may contain
     *  only those payers that are accessible through this session.
     *
     *  In effective mode, payers are returned that are currently
     *  effective in addition to being effective during the given date
     *  range. In any effective mode, effective payers and those
     *  currently expired are returned.
     *
     *  @param  customerId a customer <code> Id </code>
     *  @param  from starting date
     *  @param  to ending date
     *  @return the returned <code> PayerList </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> customerId, from,
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayersByCustomerOnDate(org.osid.id.Id customerId,
                                                                        org.osid.calendaring.DateTime from,
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList ret = getPayerList();

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            ret.addPayerList(session.getPayersByCustomerOnDate(customerId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Payers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  payers or an error results. Otherwise, the returned list
     *  may contain only those payers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, payers are returned that are currently
     *  effective.  In any effective mode, effective payers and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Payers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerList getPayers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList ret = getPayerList();

        for (org.osid.billing.payment.PayerLookupSession session : getSessions()) {
            ret.addPayerList(session.getPayers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.FederatingPayerList getPayerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.ParallelPayerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.billing.payment.payer.CompositePayerList());
        }
    }
}
