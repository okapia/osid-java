//
// AbstractImmutableSupersedingEvent.java
//
//     Wraps a mutable SupersedingEvent to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.supersedingevent.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>SupersedingEvent</code> to hide modifiers. This
 *  wrapper provides an immutized SupersedingEvent from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying supersedingEvent whose state changes are visible.
 */

public abstract class AbstractImmutableSupersedingEvent
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.calendaring.SupersedingEvent {

    private final org.osid.calendaring.SupersedingEvent supersedingEvent;


    /**
     *  Constructs a new <code>AbstractImmutableSupersedingEvent</code>.
     *
     *  @param supersedingEvent the superseding event to immutablize
     *  @throws org.osid.NullArgumentException <code>supersedingEvent</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSupersedingEvent(org.osid.calendaring.SupersedingEvent supersedingEvent) {
        super(supersedingEvent);
        this.supersedingEvent = supersedingEvent;
        return;
    }


    /**
     *  Gets the event <code> Id </code> that is to be superseded. 
     *
     *  @return the superseding event <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSupersededEventId() {
        return (this.supersedingEvent.getSupersededEventId());
    }


    /**
     *  Gets the event that is to be superseded. 
     *
     *  @return the superseding event 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getSupersededEvent()
        throws org.osid.OperationFailedException {

        return (this.supersedingEvent.getSupersededEvent());
    }


    /**
     *  Gets the event <code> Id </code> that is superseding another. 
     *
     *  @return the superseding event <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSupersedingEventId() {
        return (this.supersedingEvent.getSupersedingEventId());
    }


    /**
     *  Gets the event that is superseding another. 
     *
     *  @return the superseding event 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.Event getSupersedingEvent()
        throws org.osid.OperationFailedException {

        return (this.supersedingEvent.getSupersedingEvent());
    }


    /**
     *  Tests if the superseding event replaces an event within a recurring 
     *  series offered at a specific date/time. If <code> supersedesByDate() 
     *  </code> is true, then <code> supersedesByPosition() </code> must be 
     *  false. 
     *
     *  @return <code> true </code> if an event is superseded by date, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supersedesByDate() {
        return (this.supersedingEvent.supersedesByDate());
    }


    /**
     *  Gets the date of an event to replace if a recurring event is offered 
     *  on that date. 
     *
     *  @return the date of the event to replace 
     *  @throws org.osid.IllegalStateException <code> supersedesByDate() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getSupersededDate() {
        return (this.supersedingEvent.getSupersededDate());
    }


    /**
     *  Tests if the superseding event replaces an event within a recurring 
     *  series identified by its denormalized position in the series. A 
     *  negative number counts from the end of the series. If <code> 
     *  supersedesByPosition() </code> is <code> true, </code> then <code> 
     *  supersedesByDate() </code> must be <code> false. </code> 
     *
     *  @return <code> true </code> if an event is superseded by position, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supersedesByPosition() {
        return (this.supersedingEvent.supersedesByPosition());
    }


    /**
     *  Gets the position in the denormalized recurring series of the event to 
     *  replace. Positive numbers count from the start and negative numbers 
     *  count from the end. Zero is invalid. 
     *
     *  @return the position of the event to replace 
     *  @throws org.osid.IllegalStateException <code> supersedesByPosition() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public long getSupersededEventPosition() {
        return (this.supersedingEvent.getSupersededEventPosition());
    }


    /**
     *  Gets the superseding event record corresponding to the given <code> 
     *  SupersedingEvent </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> supersedingEventRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(supersedingEventRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  supersedingEventRecordType the type of the record to retrieve 
     *  @return the superseding event record 
     *  @throws org.osid.NullArgumentException <code> 
     *          supersedingEventRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(supersedingEventRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.records.SupersedingEventRecord getSupersedingEventRecord(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException {

        return (this.supersedingEvent.getSupersedingEventRecord(supersedingEventRecordType));
    }
}

