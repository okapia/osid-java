//
// AbstractAssessmentAuthoringManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractAssessmentAuthoringManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.assessment.authoring.AssessmentAuthoringManager,
               org.osid.assessment.authoring.AssessmentAuthoringProxyManager {

    private final Types assessmentPartRecordTypes          = new TypeRefSet();
    private final Types assessmentPartSearchRecordTypes    = new TypeRefSet();

    private final Types sequenceRuleRecordTypes            = new TypeRefSet();
    private final Types sequenceRuleSearchRecordTypes      = new TypeRefSet();

    private final Types sequenceRuleEnablerRecordTypes     = new TypeRefSet();
    private final Types sequenceRuleEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractAssessmentAuthoringManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractAssessmentAuthoringManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up assessment part is supported. 
     *
     *  @return <code> true </code> if assessment part lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartLookup() {
        return (false);
    }


    /**
     *  Tests if querying assessment part is supported. 
     *
     *  @return <code> true </code> if assessment part query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartQuery() {
        return (false);
    }


    /**
     *  Tests if searching assessment part is supported. 
     *
     *  @return <code> true </code> if assessment part search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartSearch() {
        return (false);
    }


    /**
     *  Tests if an assessment part administrative service is supported. 
     *
     *  @return <code> true </code> if assessment part administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartAdmin() {
        return (false);
    }


    /**
     *  Tests if an assessment part notification service is supported. 
     *
     *  @return <code> true </code> if assessment part notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartNotification() {
        return (false);
    }


    /**
     *  Tests if an assessment part bank lookup service is supported. 
     *
     *  @return <code> true </code> if an assessment part bank lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartBank() {
        return (false);
    }


    /**
     *  Tests if an assessment part bank service is supported. 
     *
     *  @return <code> true </code> if assessment part bank assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartBankAssignment() {
        return (false);
    }


    /**
     *  Tests if an assessment part bank lookup service is supported. 
     *
     *  @return <code> true </code> if an assessment part bank service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartSmartBank() {
        return (false);
    }


    /**
     *  Tests if an assessment part item service is supported for looking up 
     *  assessment part and item mappings. 
     *
     *  @return <code> true </code> if assessment part item service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartItem() {
        return (false);
    }


    /**
     *  Tests if an assessment part item design session is supported. 
     *
     *  @return <code> true </code> if an assessment part item design service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentPartItemDesign() {
        return (false);
    }


    /**
     *  Tests if looking up sequence rule is supported. 
     *
     *  @return <code> true </code> if sequence rule lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleLookup() {
        return (false);
    }


    /**
     *  Tests if querying sequence rule is supported. 
     *
     *  @return <code> true </code> if sequence rule query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleQuery() {
        return (false);
    }


    /**
     *  Tests if searching sequence rule is supported. 
     *
     *  @return <code> true </code> if sequence rule search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleSearch() {
        return (false);
    }


    /**
     *  Tests if a sequence rule administrative service is supported. 
     *
     *  @return <code> true </code> if sequence rule administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleAdmin() {
        return (false);
    }


    /**
     *  Tests if a sequence rule notification service is supported. 
     *
     *  @return <code> true </code> if sequence rule notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleNotification() {
        return (false);
    }


    /**
     *  Tests if a sequence rule bank lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule bank lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleBank() {
        return (false);
    }


    /**
     *  Tests if a sequence rule bank service is supported. 
     *
     *  @return <code> true </code> if sequence rule bank assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleBankAssignment() {
        return (false);
    }


    /**
     *  Tests if a sequence rule bank lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule bank service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleSmartBank() {
        return (false);
    }


    /**
     *  Tests if looking up sequence rule enablers is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying sequence rule enablers is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching sequence rule enablers is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a sequence rule enabler administrative service is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a sequence rule enabler notification service is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a sequence rule enabler bank lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule enabler bank lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerBank() {
        return (false);
    }


    /**
     *  Tests if a sequence rule enabler bank service is supported. 
     *
     *  @return <code> true </code> if sequence rule enabler bank assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerBankAssignment() {
        return (false);
    }


    /**
     *  Tests if a sequence rule enabler bank lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule enabler bank service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerSmartBank() {
        return (false);
    }


    /**
     *  Tests if a sequence rule enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a sequence rule enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a sequence rule enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if sequence rule enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> AssessmentPart </code> record types. 
     *
     *  @return a list containing the supported <code> AssessmentPart </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentPartRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.assessmentPartRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AssessmentPart </code> record type is 
     *  supported. 
     *
     *  @param  assessmentPartRecordType a <code> Type </code> indicating an 
     *          <code> AssessmentPart </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> assessmentPartRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentPartRecordType(org.osid.type.Type assessmentPartRecordType) {
        return (this.assessmentPartRecordTypes.contains(assessmentPartRecordType));
    }


    /**
     *  Adds support for an assessment part record type.
     *
     *  @param assessmentPartRecordType an assessment part record type
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentPartRecordType</code> is <code>null</code>
     */

    protected void addAssessmentPartRecordType(org.osid.type.Type assessmentPartRecordType) {
        this.assessmentPartRecordTypes.add(assessmentPartRecordType);
        return;
    }


    /**
     *  Removes support for an assessment part record type.
     *
     *  @param assessmentPartRecordType an assessment part record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentPartRecordType</code> is <code>null</code>
     */

    protected void removeAssessmentPartRecordType(org.osid.type.Type assessmentPartRecordType) {
        this.assessmentPartRecordTypes.remove(assessmentPartRecordType);
        return;
    }


    /**
     *  Gets the supported <code> AssessmentPart </code> search record types. 
     *
     *  @return a list containing the supported <code> AssessmentPart </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentPartSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.assessmentPartSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> AssessmentPart </code> search record type is 
     *  supported. 
     *
     *  @param  assessmentPartSearchRecordType a <code> Type </code> 
     *          indicating an <code> AssessmentPart </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentPartSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentPartSearchRecordType(org.osid.type.Type assessmentPartSearchRecordType) {
        return (this.assessmentPartSearchRecordTypes.contains(assessmentPartSearchRecordType));
    }


    /**
     *  Adds support for an assessment part search record type.
     *
     *  @param assessmentPartSearchRecordType an assessment part search record type
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentPartSearchRecordType</code> is <code>null</code>
     */

    protected void addAssessmentPartSearchRecordType(org.osid.type.Type assessmentPartSearchRecordType) {
        this.assessmentPartSearchRecordTypes.add(assessmentPartSearchRecordType);
        return;
    }


    /**
     *  Removes support for an assessment part search record type.
     *
     *  @param assessmentPartSearchRecordType an assessment part search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>assessmentPartSearchRecordType</code> is <code>null</code>
     */

    protected void removeAssessmentPartSearchRecordType(org.osid.type.Type assessmentPartSearchRecordType) {
        this.assessmentPartSearchRecordTypes.remove(assessmentPartSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SequenceRule </code> record types. 
     *
     *  @return a list containing the supported <code> SequenceRule </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSequenceRuleRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.sequenceRuleRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SequenceRule </code> record type is 
     *  supported. 
     *
     *  @param  sequenceRuleRecordType a <code> Type </code> indicating a 
     *          <code> SequenceRule </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSequenceRuleRecordType(org.osid.type.Type sequenceRuleRecordType) {
        return (this.sequenceRuleRecordTypes.contains(sequenceRuleRecordType));
    }


    /**
     *  Adds support for a sequence rule record type.
     *
     *  @param sequenceRuleRecordType a sequence rule record type
     *  @throws org.osid.NullArgumentException
     *  <code>sequenceRuleRecordType</code> is <code>null</code>
     */

    protected void addSequenceRuleRecordType(org.osid.type.Type sequenceRuleRecordType) {
        this.sequenceRuleRecordTypes.add(sequenceRuleRecordType);
        return;
    }


    /**
     *  Removes support for a sequence rule record type.
     *
     *  @param sequenceRuleRecordType a sequence rule record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>sequenceRuleRecordType</code> is <code>null</code>
     */

    protected void removeSequenceRuleRecordType(org.osid.type.Type sequenceRuleRecordType) {
        this.sequenceRuleRecordTypes.remove(sequenceRuleRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SequenceRule </code> search record types. 
     *
     *  @return a list containing the supported <code> SequenceRule </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSequenceRuleSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.sequenceRuleSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SequenceRule </code> search record type is 
     *  supported. 
     *
     *  @param  sequenceRuleSearchRecordType a <code> Type </code> indicating 
     *          a <code> SequenceRule </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSequenceRuleSearchRecordType(org.osid.type.Type sequenceRuleSearchRecordType) {
        return (this.sequenceRuleSearchRecordTypes.contains(sequenceRuleSearchRecordType));
    }


    /**
     *  Adds support for a sequence rule search record type.
     *
     *  @param sequenceRuleSearchRecordType a sequence rule search record type
     *  @throws org.osid.NullArgumentException
     *  <code>sequenceRuleSearchRecordType</code> is <code>null</code>
     */

    protected void addSequenceRuleSearchRecordType(org.osid.type.Type sequenceRuleSearchRecordType) {
        this.sequenceRuleSearchRecordTypes.add(sequenceRuleSearchRecordType);
        return;
    }


    /**
     *  Removes support for a sequence rule search record type.
     *
     *  @param sequenceRuleSearchRecordType a sequence rule search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>sequenceRuleSearchRecordType</code> is <code>null</code>
     */

    protected void removeSequenceRuleSearchRecordType(org.osid.type.Type sequenceRuleSearchRecordType) {
        this.sequenceRuleSearchRecordTypes.remove(sequenceRuleSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SequenceRuleEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> SequenceRuleEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSequenceRuleEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.sequenceRuleEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SequenceRuleEnabler </code> record type is 
     *  supported. 
     *
     *  @param  sequenceRuleEnablerRecordType a <code> Type </code> indicating 
     *          a <code> SequenceRuleEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerRecordType(org.osid.type.Type sequenceRuleEnablerRecordType) {
        return (this.sequenceRuleEnablerRecordTypes.contains(sequenceRuleEnablerRecordType));
    }


    /**
     *  Adds support for a sequence rule enabler record type.
     *
     *  @param sequenceRuleEnablerRecordType a sequence rule enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>sequenceRuleEnablerRecordType</code> is <code>null</code>
     */

    protected void addSequenceRuleEnablerRecordType(org.osid.type.Type sequenceRuleEnablerRecordType) {
        this.sequenceRuleEnablerRecordTypes.add(sequenceRuleEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a sequence rule enabler record type.
     *
     *  @param sequenceRuleEnablerRecordType a sequence rule enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>sequenceRuleEnablerRecordType</code> is <code>null</code>
     */

    protected void removeSequenceRuleEnablerRecordType(org.osid.type.Type sequenceRuleEnablerRecordType) {
        this.sequenceRuleEnablerRecordTypes.remove(sequenceRuleEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> SequenceRuleEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> SequenceRuleEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSequenceRuleEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.sequenceRuleEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> SequenceRuleEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  sequenceRuleEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> SequenceRuleEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsSequenceRuleEnablerSearchRecordType(org.osid.type.Type sequenceRuleEnablerSearchRecordType) {
        return (this.sequenceRuleEnablerSearchRecordTypes.contains(sequenceRuleEnablerSearchRecordType));
    }


    /**
     *  Adds support for a sequence rule enabler search record type.
     *
     *  @param sequenceRuleEnablerSearchRecordType a sequence rule enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>sequenceRuleEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addSequenceRuleEnablerSearchRecordType(org.osid.type.Type sequenceRuleEnablerSearchRecordType) {
        this.sequenceRuleEnablerSearchRecordTypes.add(sequenceRuleEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a sequence rule enabler search record type.
     *
     *  @param sequenceRuleEnablerSearchRecordType a sequence rule enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>sequenceRuleEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeSequenceRuleEnablerSearchRecordType(org.osid.type.Type sequenceRuleEnablerSearchRecordType) {
        this.sequenceRuleEnablerSearchRecordTypes.remove(sequenceRuleEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part lookup service. 
     *
     *  @return an <code> AssessmentPartLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartLookupSession getAssessmentPartLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartLookupSession getAssessmentPartLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartLookupSession getAssessmentPartLookupSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartLookupSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartLookupSession getAssessmentPartLookupSessionForBank(org.osid.id.Id bankId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartLookupSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part query service. 
     *
     *  @return an <code> AssessmentPartQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuerySession getAssessmentPartQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuerySession getAssessmentPartQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuerySession getAssessmentPartQuerySessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartQuerySessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartQuerySession getAssessmentPartQuerySessionForBank(org.osid.id.Id bankId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartQuerySessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part search service. 
     *
     *  @return an <code> AssessmentPartSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchSession getAssessmentPartSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchSession getAssessmentPartSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part earch service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchSession getAssessmentPartSearchSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartSearchSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part earch service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSearchSession getAssessmentPartSearchSessionForBank(org.osid.id.Id bankId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartSearchSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part administration service. 
     *
     *  @return an <code> AssessmentPartAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartAdminSession getAssessmentPartAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartAdminSession getAssessmentPartAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartAdminSession getAssessmentPartAdminSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartAdminSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartAdminSession getAssessmentPartAdminSessionForBank(org.osid.id.Id bankId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartAdminSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part notification service. 
     *
     *  @param  assessmentPartReceiver the notification callback 
     *  @return an <code> AssessmentPartNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentPartReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartNotificationSession getAssessmentPartNotificationSession(org.osid.assessment.authoring.AssessmentPartReceiver assessmentPartReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part notification service. 
     *
     *  @param  assessmentPartReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentPartReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartNotificationSession getAssessmentPartNotificationSession(org.osid.assessment.authoring.AssessmentPartReceiver assessmentPartReceiver, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part notification service for the given bank. 
     *
     *  @param  assessmentPartReceiver the notification callback 
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bank found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentPartReceiver 
     *          </code> or <code> bankId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartNotificationSession getAssessmentPartNotificationSessionForBank(org.osid.assessment.authoring.AssessmentPartReceiver assessmentPartReceiver, 
                                                                                                                       org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartNotificationSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  part notification service for the given bank. 
     *
     *  @param  assessmentPartReceiver the notification callback 
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bank found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentPartReceiver, 
     *          bankId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartNotificationSession getAssessmentPartNotificationSessionForBank(org.osid.assessment.authoring.AssessmentPartReceiver assessmentPartReceiver, 
                                                                                                                       org.osid.id.Id bankId, 
                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartNotificationSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup assessment part/bank 
     *  mappings for assessment parts. 
     *
     *  @return an <code> AssessmentPartBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartBank() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartBankSession getAssessmentPartBankSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup assessment part/bank 
     *  mappings for assessment parts. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartBankSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartBank() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartBankSession getAssessmentPartBankSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  assessment part to bank. 
     *
     *  @return an <code> AssessmentPartBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartBankAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartBankAssignmentSession getAssessmentPartBankAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  assessment part to bank. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartBankAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartBankAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartBankAssignmentSession getAssessmentPartBankAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage assessment part smart 
     *  bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return an <code> AssessmentPartSmartBankSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSmartBankSession getAssessmentPartSmartBankSession(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getAssessmentPartSmartBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage assessment part smart 
     *  bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentPartSmartBankSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentPartSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartSmartBankSession getAssessmentPartSmartBankSession(org.osid.id.Id bankId, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getAssessmentPartSmartBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  lookup service. 
     *
     *  @return a <code> SequenceRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleLookupSession getSequenceRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleLookupSession getSequenceRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleLookupSession getSequenceRuleLookupSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleLookupSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleLookupSession getSequenceRuleLookupSessionForBank(org.osid.id.Id bankId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleLookupSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  query service. 
     *
     *  @return a <code> SequenceRuleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQuerySession getSequenceRuleQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQuerySession getSequenceRuleQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQuerySession getSequenceRuleQuerySessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleQuerySessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleQuerySession getSequenceRuleQuerySessionForBank(org.osid.id.Id bankId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleQuerySessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  search service. 
     *
     *  @return a <code> SequenceRuleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleSearchSession getSequenceRuleSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleSearchSession getSequenceRuleSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  earch service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleSearchSession getSequenceRuleSearchSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleSearchSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  earch service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleSearchSession getSequenceRuleSearchSessionForBank(org.osid.id.Id bankId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleSearchSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  administration service. 
     *
     *  @return a <code> SequenceRuleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleAdminSession getSequenceRuleAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleAdminSession getSequenceRuleAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleAdminSession getSequenceRuleAdminSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleAdminSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleAdminSession getSequenceRuleAdminSessionForBank(org.osid.id.Id bankId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleAdminSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  notification service. 
     *
     *  @param  sequenceRuleReceiver the notification callback 
     *  @return a <code> SequenceRuleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleNotificationSession getSequenceRuleNotificationSession(org.osid.assessment.authoring.SequenceRuleReceiver sequenceRuleReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  notification service. 
     *
     *  @param  sequenceRuleReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleNotificationSession getSequenceRuleNotificationSession(org.osid.assessment.authoring.SequenceRuleReceiver sequenceRuleReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  notification service for the given bank. 
     *
     *  @param  sequenceRuleReceiver the notification callback 
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bank found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleReceiver 
     *          </code> or <code> bankId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleNotificationSession getSequenceRuleNotificationSessionForBank(org.osid.assessment.authoring.SequenceRuleReceiver sequenceRuleReceiver, 
                                                                                                                   org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleNotificationSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  notification service for the given bank. 
     *
     *  @param  sequenceRuleReceiver the notification callback 
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bank found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> sequenceRuleReceiver, 
     *          bankId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleNotificationSession getSequenceRuleNotificationSessionForBank(org.osid.assessment.authoring.SequenceRuleReceiver sequenceRuleReceiver, 
                                                                                                                   org.osid.id.Id bankId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleNotificationSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup sequence rule/bank 
     *  mappings for sequence rules. 
     *
     *  @return a <code> SequenceRuleBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleBank() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleBankSession getSequenceRuleBankSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup sequence rule/bank 
     *  mappings for sequence rules. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleBankSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleBank() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleBankSession getSequenceRuleBankSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning sequence 
     *  rule to bank. 
     *
     *  @return a <code> SequenceRuleBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleBankAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleBankAssignmentSession getSequenceRuleBankAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning sequence 
     *  rule to bank. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleBankAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleBankAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleBankAssignmentSession getSequenceRuleBankAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage sequence rule smart 
     *  bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleSmartBankSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleSmartBankSession getSequenceRuleSmartBankSession(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleSmartBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage sequence rule smart 
     *  bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleSmartBankSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleSmartBankSession getSequenceRuleSmartBankSession(org.osid.id.Id bankId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleSmartBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler lookup service. 
     *
     *  @return a <code> SequenceRuleEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerLookupSession getSequenceRuleEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerLookupSession getSequenceRuleEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerLookupSession getSequenceRuleEnablerLookupSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerLookupSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerLookupSession getSequenceRuleEnablerLookupSessionForBank(org.osid.id.Id bankId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerLookupSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler query service. 
     *
     *  @return a <code> SequenceRuleEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerQuerySession getSequenceRuleEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerQuerySession getSequenceRuleEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerQuerySession getSequenceRuleEnablerQuerySessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerQuerySessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerQuerySession getSequenceRuleEnablerQuerySessionForBank(org.osid.id.Id bankId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerQuerySessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler search service. 
     *
     *  @return a <code> SequenceRuleEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerSearchSession getSequenceRuleEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerSearchSession getSequenceRuleEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enablers earch service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerSearchSession getSequenceRuleEnablerSearchSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerSearchSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enablers earch service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerSearchSession getSequenceRuleEnablerSearchSessionForBank(org.osid.id.Id bankId, 
                                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerSearchSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler administration service. 
     *
     *  @return a <code> SequenceRuleEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerAdminSession getSequenceRuleEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerAdminSession getSequenceRuleEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerAdminSession getSequenceRuleEnablerAdminSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerAdminSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler administration service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerAdminSession getSequenceRuleEnablerAdminSessionForBank(org.osid.id.Id bankId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerAdminSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler notification service. 
     *
     *  @param  sequenceRuleEnablerReceiver the notification callback 
     *  @return a <code> SequenceRuleEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerNotificationSession getSequenceRuleEnablerNotificationSession(org.osid.assessment.authoring.SequenceRuleEnablerReceiver sequenceRuleEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler notification service. 
     *
     *  @param  sequenceRuleEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerNotificationSession getSequenceRuleEnablerNotificationSession(org.osid.assessment.authoring.SequenceRuleEnablerReceiver sequenceRuleEnablerReceiver, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler notification service for the given bank. 
     *
     *  @param  sequenceRuleEnablerReceiver the notification callback 
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bank found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerReceiver </code> or <code> bankId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerNotificationSession getSequenceRuleEnablerNotificationSessionForBank(org.osid.assessment.authoring.SequenceRuleEnablerReceiver sequenceRuleEnablerReceiver, 
                                                                                                                                 org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerNotificationSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler notification service for the given bank. 
     *
     *  @param  sequenceRuleEnablerReceiver the notification callback 
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bank found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          sequenceRuleEnablerReceiver, bankId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerNotificationSession getSequenceRuleEnablerNotificationSessionForBank(org.osid.assessment.authoring.SequenceRuleEnablerReceiver sequenceRuleEnablerReceiver, 
                                                                                                                                 org.osid.id.Id bankId, 
                                                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerNotificationSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup sequence rule 
     *  enabler/bank mappings for sequence rule enablers. 
     *
     *  @return a <code> SequenceRuleEnablerBankSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerBankSession getSequenceRuleEnablerBankSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup sequence rule 
     *  enabler/bank mappings for sequence rule enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerBankSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerBankSession getSequenceRuleEnablerBankSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning sequence 
     *  rule enablers to bank. 
     *
     *  @return a <code> SequenceRuleEnablerBankAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerBankAssignmentSession getSequenceRuleEnablerBankAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning sequence 
     *  rule enablers to bank. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerBankAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerBankAssignmentSession getSequenceRuleEnablerBankAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerBankAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage sequence rule enabler 
     *  smart bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerSmartBankSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerSmartBankSession getSequenceRuleEnablerSmartBankSession(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerSmartBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage sequence rule enabler 
     *  smart bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerSmartBankSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerSmartBankSession getSequenceRuleEnablerSmartBankSession(org.osid.id.Id bankId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerSmartBankSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> SequenceRuleEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleLookupSession getSequenceRuleEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleLookupSession getSequenceRuleEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler mapping lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleLookupSession getSequenceRuleEnablerRuleLookupSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerRuleLookupSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler mapping lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleLookupSession getSequenceRuleEnablerRuleLookupSessionForBank(org.osid.id.Id bankId, 
                                                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerRuleLookupSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler assignment service. 
     *
     *  @return a <code> SequenceRuleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleApplicationSession getSequenceRuleEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleApplicationSession getSequenceRuleEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler assignment service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @return a <code> SequenceRuleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleApplicationSession getSequenceRuleEnablerRuleApplicationSessionForBank(org.osid.id.Id bankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringManager.getSequenceRuleEnablerRuleApplicationSessionForBank not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the sequence rule 
     *  enabler assignment service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the <code> Bank </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SequenceRuleEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bank </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequenceRuleEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleEnablerRuleApplicationSession getSequenceRuleEnablerRuleApplicationSessionForBank(org.osid.id.Id bankId, 
                                                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.assessment.authoring.AssessmentAuthoringProxyManager.getSequenceRuleEnablerRuleApplicationSessionForBank not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.assessmentPartRecordTypes.clear();
        this.assessmentPartRecordTypes.clear();

        this.assessmentPartSearchRecordTypes.clear();
        this.assessmentPartSearchRecordTypes.clear();

        this.sequenceRuleRecordTypes.clear();
        this.sequenceRuleRecordTypes.clear();

        this.sequenceRuleSearchRecordTypes.clear();
        this.sequenceRuleSearchRecordTypes.clear();

        this.sequenceRuleEnablerRecordTypes.clear();
        this.sequenceRuleEnablerRecordTypes.clear();

        this.sequenceRuleEnablerSearchRecordTypes.clear();
        this.sequenceRuleEnablerSearchRecordTypes.clear();

        return;
    }
}
