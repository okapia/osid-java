//
// AbstractEntry.java
//
//     Defines an Entry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.blogging.entry.spi;


/**
 *  Defines an <code>Entry</code> builder.
 */

public abstract class AbstractEntryBuilder<T extends AbstractEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractSourceableOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.blogging.entry.EntryMiter entry;


    /**
     *  Constructs a new <code>AbstractEntryBuilder</code>.
     *
     *  @param entry the entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractEntryBuilder(net.okapia.osid.jamocha.builder.blogging.entry.EntryMiter entry) {
        super(entry);
        this.entry = entry;
        return;
    }


    /**
     *  Builds the entry.
     *
     *  @return the new entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.blogging.Entry build() {
        (new net.okapia.osid.jamocha.builder.validator.blogging.entry.EntryValidator(getValidations())).validate(this.entry);
        return (new net.okapia.osid.jamocha.builder.blogging.entry.ImmutableEntry(this.entry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.blogging.entry.EntryMiter getMiter() {
        return (this.entry);
    }


    /**
     *  Sets the timestamp.
     *
     *  @param timestamp a timestamp
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>timestamp</code> is <code>null</code>
     */

    public T timestamp(org.osid.calendaring.DateTime timestamp) {
        getMiter().setTimestamp(timestamp);
        return (self());
    }


    /**
     *  Sets the poster.
     *
     *  @param poster a poster
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>poster</code> is <code>null</code>
     */

    public T poster(org.osid.resource.Resource poster) {
        getMiter().setPoster(poster);
        return (self());
    }


    /**
     *  Sets the posting agent.
     *
     *  @param agent a posting agent
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    public T agent(org.osid.authentication.Agent agent) {
        getMiter().setPostingAgent(agent);
        return (self());
    }


    /**
     *  Sets the subject line.
     *
     *  @param subjectLine a subject line
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>subjectLine</code> is <code>null</code>
     */

    public T subjectLine(org.osid.locale.DisplayText subjectLine) {
        getMiter().setSubjectLine(subjectLine);
        return (self());
    }


    /**
     *  Sets the summary.
     *
     *  @param summary a summary
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>summary</code> is <code>null</code>
     */

    public T summary(org.osid.locale.DisplayText summary) {
        getMiter().setSummary(summary);
        return (self());
    }


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>text</code> is <code>null</code>
     */

    public T text(org.osid.locale.DisplayText text) {
        getMiter().setText(text);
        return (self());
    }


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>copyright</code> is <code>null</code>
     */

    public T copyright(org.osid.locale.DisplayText copyright) {
        getMiter().setCopyright(copyright);
        return (self());
    }


    /**
     *  Adds an Entry record.
     *
     *  @param record an entry record
     *  @param recordType the type of entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.blogging.records.EntryRecord record, org.osid.type.Type recordType) {
        getMiter().addEntryRecord(record, recordType);
        return (self());
    }
}       


