//
// AbstractIndexedMapBinLookupSession.java
//
//    A simple framework for providing a Bin lookup service
//    backed by a fixed collection of bins with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resource.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Bin lookup service backed by a
 *  fixed collection of bins. The bins are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some bins may be compatible
 *  with more types than are indicated through these bin
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Bins</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBinLookupSession
    extends AbstractMapBinLookupSession
    implements org.osid.resource.BinLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resource.Bin> binsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.Bin>());
    private final MultiMap<org.osid.type.Type, org.osid.resource.Bin> binsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resource.Bin>());


    /**
     *  Makes a <code>Bin</code> available in this session.
     *
     *  @param  bin a bin
     *  @throws org.osid.NullArgumentException <code>bin<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBin(org.osid.resource.Bin bin) {
        super.putBin(bin);

        this.binsByGenus.put(bin.getGenusType(), bin);
        
        try (org.osid.type.TypeList types = bin.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.binsByRecord.put(types.getNextType(), bin);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a bin from this session.
     *
     *  @param binId the <code>Id</code> of the bin
     *  @throws org.osid.NullArgumentException <code>binId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBin(org.osid.id.Id binId) {
        org.osid.resource.Bin bin;
        try {
            bin = getBin(binId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.binsByGenus.remove(bin.getGenusType());

        try (org.osid.type.TypeList types = bin.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.binsByRecord.remove(types.getNextType(), bin);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBin(binId);
        return;
    }


    /**
     *  Gets a <code>BinList</code> corresponding to the given
     *  bin genus <code>Type</code> which does not include
     *  bins of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known bins or an error results. Otherwise,
     *  the returned list may contain only those bins that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  binGenusType a bin genus type 
     *  @return the returned <code>Bin</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>binGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByGenusType(org.osid.type.Type binGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.bin.ArrayBinList(this.binsByGenus.get(binGenusType)));
    }


    /**
     *  Gets a <code>BinList</code> containing the given
     *  bin record <code>Type</code>. In plenary mode, the
     *  returned list contains all known bins or an error
     *  results. Otherwise, the returned list may contain only those
     *  bins that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  binRecordType a bin record type 
     *  @return the returned <code>bin</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>binRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.BinList getBinsByRecordType(org.osid.type.Type binRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resource.bin.ArrayBinList(this.binsByRecord.get(binRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.binsByGenus.clear();
        this.binsByRecord.clear();

        super.close();

        return;
    }
}
