//
// AbstractQueryPriceEnablerLookupSession.java
//
//    A PriceEnablerQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ordering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PriceEnablerQuerySession adapter.
 */

public abstract class AbstractAdapterPriceEnablerQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ordering.rules.PriceEnablerQuerySession {

    private final org.osid.ordering.rules.PriceEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterPriceEnablerQuerySession.
     *
     *  @param session the underlying price enabler query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPriceEnablerQuerySession(org.osid.ordering.rules.PriceEnablerQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeStore</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeStore Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getStoreId() {
        return (this.session.getStoreId());
    }


    /**
     *  Gets the {@codeStore</code> associated with this 
     *  session.
     *
     *  @return the {@codeStore</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.Store getStore()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getStore());
    }


    /**
     *  Tests if this user can perform {@codePriceEnabler</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchPriceEnablers() {
        return (this.session.canSearchPriceEnablers());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include price enablers in stores which are children
     *  of this store in the store hierarchy.
     */

    @OSID @Override
    public void useFederatedStoreView() {
        this.session.useFederatedStoreView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this store only.
     */
    
    @OSID @Override
    public void useIsolatedStoreView() {
        this.session.useIsolatedStoreView();
        return;
    }
    
      
    /**
     *  Gets a price enabler query. The returned query will not have an
     *  extension query.
     *
     *  @return the price enabler query 
     */
      
    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerQuery getPriceEnablerQuery() {
        return (this.session.getPriceEnablerQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  priceEnablerQuery the price enabler query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code priceEnablerQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code priceEnablerQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.ordering.rules.PriceEnablerList getPriceEnablersByQuery(org.osid.ordering.rules.PriceEnablerQuery priceEnablerQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getPriceEnablersByQuery(priceEnablerQuery));
    }
}
