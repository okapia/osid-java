//
// AbstractFederatingDemographicEnablerLookupSession.java
//
//     An abstract federating adapter for a DemographicEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  DemographicEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingDemographicEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.resource.demographic.DemographicEnablerLookupSession>
    implements org.osid.resource.demographic.DemographicEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.resource.Bin bin = new net.okapia.osid.jamocha.nil.resource.bin.UnknownBin();


    /**
     *  Constructs a new <code>AbstractFederatingDemographicEnablerLookupSession</code>.
     */

    protected AbstractFederatingDemographicEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.resource.demographic.DemographicEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Bin/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bin Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.bin.getId());
    }


    /**
     *  Gets the <code>Bin</code> associated with this 
     *  session.
     *
     *  @return the <code>Bin</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bin);
    }


    /**
     *  Sets the <code>Bin</code>.
     *
     *  @param  bin the bin for this session
     *  @throws org.osid.NullArgumentException <code>bin</code>
     *          is <code>null</code>
     */

    protected void setBin(org.osid.resource.Bin bin) {
        nullarg(bin, "bin");
        this.bin = bin;
        return;
    }


    /**
     *  Tests if this user can perform <code>DemographicEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupDemographicEnablers() {
        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            if (session.canLookupDemographicEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>DemographicEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeDemographicEnablerView() {
        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            session.useComparativeDemographicEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>DemographicEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryDemographicEnablerView() {
        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            session.usePlenaryDemographicEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include demographic enablers in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            session.useFederatedBinView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            session.useIsolatedBinView();
        }

        return;
    }


    /**
     *  Only active demographic enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveDemographicEnablerView() {
        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            session.useActiveDemographicEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive demographic enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusDemographicEnablerView() {
        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            session.useAnyStatusDemographicEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>DemographicEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>DemographicEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>DemographicEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerId <code>Id</code> of the
     *          <code>DemographicEnabler</code>
     *  @return the demographic enabler
     *  @throws org.osid.NotFoundException <code>demographicEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>demographicEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnabler getDemographicEnabler(org.osid.id.Id demographicEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            try {
                return (session.getDemographicEnabler(demographicEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(demographicEnablerId + " not found");
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  demographicEnablers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>DemographicEnablers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByIds(org.osid.id.IdList demographicEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.resource.demographic.demographicenabler.MutableDemographicEnablerList ret = new net.okapia.osid.jamocha.resource.demographic.demographicenabler.MutableDemographicEnablerList();

        try (org.osid.id.IdList ids = demographicEnablerIds) {
            while (ids.hasNext()) {
                ret.addDemographicEnabler(getDemographicEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> corresponding to
     *  the given demographic enabler genus <code>Type</code> which
     *  does not include demographic enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerGenusType a demographicEnabler genus type 
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByGenusType(org.osid.type.Type demographicEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.demographic.demographicenabler.FederatingDemographicEnablerList ret = getDemographicEnablerList();

        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            ret.addDemographicEnablerList(session.getDemographicEnablersByGenusType(demographicEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> corresponding to
     *  the given demographic enabler genus <code>Type</code> and
     *  include any additional demographic enablers with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerGenusType a demographicEnabler genus type 
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByParentGenusType(org.osid.type.Type demographicEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.demographic.demographicenabler.FederatingDemographicEnablerList ret = getDemographicEnablerList();

        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            ret.addDemographicEnablerList(session.getDemographicEnablersByParentGenusType(demographicEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> containing the given
     *  demographic enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  demographicEnablerRecordType a demographicEnabler record type 
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>demographicEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersByRecordType(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.demographic.demographicenabler.FederatingDemographicEnablerList ret = getDemographicEnablerList();

        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            ret.addDemographicEnablerList(session.getDemographicEnablersByRecordType(demographicEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>DemographicEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session.
     *  
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>DemographicEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.demographic.demographicenabler.FederatingDemographicEnablerList ret = getDemographicEnablerList();

        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            ret.addDemographicEnablerList(session.getDemographicEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>DemographicEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>DemographicEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.demographic.demographicenabler.FederatingDemographicEnablerList ret = getDemographicEnablerList();

        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            ret.addDemographicEnablerList(session.getDemographicEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>DemographicEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  demographic enablers or an error results. Otherwise, the
     *  returned list may contain only those demographic enablers that
     *  are accessible through this session. In both cases, the order
     *  of the set is not specified.
     *
     *  In active mode, demographic enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  demographic enablers are returned.
     *
     *  @return a list of <code>DemographicEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerList getDemographicEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resource.demographic.demographicenabler.FederatingDemographicEnablerList ret = getDemographicEnablerList();

        for (org.osid.resource.demographic.DemographicEnablerLookupSession session : getSessions()) {
            ret.addDemographicEnablerList(session.getDemographicEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.resource.demographic.demographicenabler.FederatingDemographicEnablerList getDemographicEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.resource.demographic.demographicenabler.ParallelDemographicEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.resource.demographic.demographicenabler.CompositeDemographicEnablerList());
        }
    }
}
