//
// AbstractIndexedMapBuildingLookupSession.java
//
//    A simple framework for providing a Building lookup service
//    backed by a fixed collection of buildings with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Building lookup service backed by a
 *  fixed collection of buildings. The buildings are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some buildings may be compatible
 *  with more types than are indicated through these building
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Buildings</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBuildingLookupSession
    extends AbstractMapBuildingLookupSession
    implements org.osid.room.BuildingLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.room.Building> buildingsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.Building>());
    private final MultiMap<org.osid.type.Type, org.osid.room.Building> buildingsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.Building>());


    /**
     *  Makes a <code>Building</code> available in this session.
     *
     *  @param  building a building
     *  @throws org.osid.NullArgumentException <code>building<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBuilding(org.osid.room.Building building) {
        super.putBuilding(building);

        this.buildingsByGenus.put(building.getGenusType(), building);
        
        try (org.osid.type.TypeList types = building.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.buildingsByRecord.put(types.getNextType(), building);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a building from this session.
     *
     *  @param buildingId the <code>Id</code> of the building
     *  @throws org.osid.NullArgumentException <code>buildingId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBuilding(org.osid.id.Id buildingId) {
        org.osid.room.Building building;
        try {
            building = getBuilding(buildingId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.buildingsByGenus.remove(building.getGenusType());

        try (org.osid.type.TypeList types = building.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.buildingsByRecord.remove(types.getNextType(), building);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBuilding(buildingId);
        return;
    }


    /**
     *  Gets a <code>BuildingList</code> corresponding to the given
     *  building genus <code>Type</code> which does not include
     *  buildings of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known buildings or an error results. Otherwise,
     *  the returned list may contain only those buildings that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  buildingGenusType a building genus type 
     *  @return the returned <code>Building</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>buildingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByGenusType(org.osid.type.Type buildingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.building.ArrayBuildingList(this.buildingsByGenus.get(buildingGenusType)));
    }


    /**
     *  Gets a <code>BuildingList</code> containing the given
     *  building record <code>Type</code>. In plenary mode, the
     *  returned list contains all known buildings or an error
     *  results. Otherwise, the returned list may contain only those
     *  buildings that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  buildingRecordType a building record type 
     *  @return the returned <code>building</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByRecordType(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.building.ArrayBuildingList(this.buildingsByRecord.get(buildingRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.buildingsByGenus.clear();
        this.buildingsByRecord.clear();

        super.close();

        return;
    }
}
