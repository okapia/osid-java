//
// AbstractAvailabilityEnablerSearch.java
//
//     A template for making an AvailabilityEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.availabilityenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing availability enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAvailabilityEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resourcing.rules.AvailabilityEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.AvailabilityEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resourcing.rules.AvailabilityEnablerSearchOrder availabilityEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of availability enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  availabilityEnablerIds list of availability enablers
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAvailabilityEnablers(org.osid.id.IdList availabilityEnablerIds) {
        while (availabilityEnablerIds.hasNext()) {
            try {
                this.ids.add(availabilityEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAvailabilityEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of availability enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAvailabilityEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  availabilityEnablerSearchOrder availability enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>availabilityEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAvailabilityEnablerResults(org.osid.resourcing.rules.AvailabilityEnablerSearchOrder availabilityEnablerSearchOrder) {
	this.availabilityEnablerSearchOrder = availabilityEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resourcing.rules.AvailabilityEnablerSearchOrder getAvailabilityEnablerSearchOrder() {
	return (this.availabilityEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given availability enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an availability enabler implementing the requested record.
     *
     *  @param availabilityEnablerSearchRecordType an availability enabler search record
     *         type
     *  @return the availability enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.AvailabilityEnablerSearchRecord getAvailabilityEnablerSearchRecord(org.osid.type.Type availabilityEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resourcing.rules.records.AvailabilityEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(availabilityEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this availability enabler search. 
     *
     *  @param availabilityEnablerSearchRecord availability enabler search record
     *  @param availabilityEnablerSearchRecordType availabilityEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAvailabilityEnablerSearchRecord(org.osid.resourcing.rules.records.AvailabilityEnablerSearchRecord availabilityEnablerSearchRecord, 
                                           org.osid.type.Type availabilityEnablerSearchRecordType) {

        addRecordType(availabilityEnablerSearchRecordType);
        this.records.add(availabilityEnablerSearchRecord);        
        return;
    }
}
