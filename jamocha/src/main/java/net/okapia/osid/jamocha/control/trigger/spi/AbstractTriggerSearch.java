//
// AbstractTriggerSearch.java
//
//     A template for making a Trigger Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.trigger.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing trigger searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractTriggerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.control.TriggerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.control.records.TriggerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.control.TriggerSearchOrder triggerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of triggers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  triggerIds list of triggers
     *  @throws org.osid.NullArgumentException
     *          <code>triggerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongTriggers(org.osid.id.IdList triggerIds) {
        while (triggerIds.hasNext()) {
            try {
                this.ids.add(triggerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongTriggers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of trigger Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getTriggerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  triggerSearchOrder trigger search order 
     *  @throws org.osid.NullArgumentException
     *          <code>triggerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>triggerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderTriggerResults(org.osid.control.TriggerSearchOrder triggerSearchOrder) {
	this.triggerSearchOrder = triggerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.control.TriggerSearchOrder getTriggerSearchOrder() {
	return (this.triggerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given trigger search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a trigger implementing the requested record.
     *
     *  @param triggerSearchRecordType a trigger search record
     *         type
     *  @return the trigger search record
     *  @throws org.osid.NullArgumentException
     *          <code>triggerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(triggerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.TriggerSearchRecord getTriggerSearchRecord(org.osid.type.Type triggerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.control.records.TriggerSearchRecord record : this.records) {
            if (record.implementsRecordType(triggerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(triggerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this trigger search. 
     *
     *  @param triggerSearchRecord trigger search record
     *  @param triggerSearchRecordType trigger search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTriggerSearchRecord(org.osid.control.records.TriggerSearchRecord triggerSearchRecord, 
                                           org.osid.type.Type triggerSearchRecordType) {

        addRecordType(triggerSearchRecordType);
        this.records.add(triggerSearchRecord);        
        return;
    }
}
