//
// AbstractUnknownProvision.java
//
//     Defines an unknown Provision.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.provisioning.provision.spi;


/**
 *  Defines an unknown <code>Provision</code>.
 */

public abstract class AbstractUnknownProvision
    extends net.okapia.osid.jamocha.provisioning.provision.spi.AbstractProvision
    implements org.osid.provisioning.Provision {

    protected static final String OBJECT = "osid.provisioning.Provision";


    /**
     *  Constructs a new <code>AbstractUnknownProvision</code>.
     */

    public AbstractUnknownProvision() {
        setId(net.okapia.osid.jamocha.nil.privateutil.UnknownId.valueOf(OBJECT));
        setDisplayName(net.okapia.osid.jamocha.nil.privateutil.DisplayName.valueOf(OBJECT));
        setDescription(net.okapia.osid.jamocha.nil.privateutil.Description.valueOf(OBJECT));

        setStartDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setEndDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        setBroker(new net.okapia.osid.jamocha.nil.provisioning.broker.UnknownBroker());
        setProvisionable(new net.okapia.osid.jamocha.nil.provisioning.provisionable.UnknownProvisionable());
        setRecipient(new net.okapia.osid.jamocha.nil.resource.resource.UnknownResource());
        setProvisionDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());

        return;
    }


    /**
     *  Constructs a new <code>AbstractUnknownProvision</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public AbstractUnknownProvision(boolean optional) {
        this();

        setRequest(new net.okapia.osid.jamocha.nil.provisioning.request.UnknownRequest());
        setLeased(true);
        setMustReturn(true);
        setDueDate(net.okapia.osid.primordium.calendaring.GregorianUTCDateTime.unknown());
        setRateAmount(net.okapia.osid.primordium.financials.USDCurrency.valueOf("$0"));
        setRatePeriod(net.okapia.osid.primordium.calendaring.GregorianUTCDuration.unknown());
        setProvisionReturn(new net.okapia.osid.jamocha.nil.provisioning.provisionreturn.UnknownProvisionReturn());

        return;
    }
}
