//
// AbstractEdgeEnablerQueryInspector.java
//
//     A template for making an EdgeEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.rules.edgeenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for edge enablers.
 */

public abstract class AbstractEdgeEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.topology.rules.EdgeEnablerQueryInspector {

    private final java.util.Collection<org.osid.topology.rules.records.EdgeEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the ruled edge <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledEdgeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ruled edge query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.EdgeQueryInspector[] getRuledEdgeTerms() {
        return (new org.osid.topology.EdgeQueryInspector[0]);
    }


    /**
     *  Gets the graph <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGraphIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the graph query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.topology.GraphQueryInspector[] getGraphTerms() {
        return (new org.osid.topology.GraphQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given edge enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an edge enabler implementing the requested record.
     *
     *  @param edgeEnablerRecordType an edge enabler record type
     *  @return the edge enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>edgeEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(edgeEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.rules.records.EdgeEnablerQueryInspectorRecord getEdgeEnablerQueryInspectorRecord(org.osid.type.Type edgeEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.rules.records.EdgeEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(edgeEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this edge enabler query. 
     *
     *  @param edgeEnablerQueryInspectorRecord edge enabler query inspector
     *         record
     *  @param edgeEnablerRecordType edgeEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addEdgeEnablerQueryInspectorRecord(org.osid.topology.rules.records.EdgeEnablerQueryInspectorRecord edgeEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type edgeEnablerRecordType) {

        addRecordType(edgeEnablerRecordType);
        nullarg(edgeEnablerRecordType, "edge enabler record type");
        this.records.add(edgeEnablerQueryInspectorRecord);        
        return;
    }
}
