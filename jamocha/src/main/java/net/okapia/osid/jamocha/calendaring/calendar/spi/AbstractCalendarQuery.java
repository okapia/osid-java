//
// AbstractCalendarQuery.java
//
//     A template for making a Calendar Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.calendar.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for calendars.
 */

public abstract class AbstractCalendarQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.calendaring.CalendarQuery {

    private final java.util.Collection<org.osid.calendaring.records.CalendarQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the event <code> Id </code> for this query. 
     *
     *  @param  eventId an event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EventQuery </code> is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Matches a calendar that has any event assigned. 
     *
     *  @param  match <code> true </code> to match calendars with any event, 
     *          <code> false </code> to match calendars with no events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        return;
    }


    /**
     *  Sets the time period <code> Id </code> for this query. 
     *
     *  @param  timePeriodId a time period <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> timePeriodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchTimePeriodId(org.osid.id.Id timePeriodId, boolean match) {
        return;
    }


    /**
     *  Clears the time period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTimePeriodIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TimePeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a time period query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTimePeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a time period. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the tiem period query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTimePeriodQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodQuery getTimePeriodQuery() {
        throw new org.osid.UnimplementedException("supportsTimePeriodQuery() is false");
    }


    /**
     *  Matches a calendar that has any time period assigned. 
     *
     *  @param  match <code> true </code> to match calendars with any time period, 
     *          <code> false </code> to match calendars with no time periods 
     */

    @OSID @Override
    public void matchAnyTimePeriod(boolean match) {
        return;
    }


    /**
     *  Clears the time periods terms.
     */

    @OSID @Override
    public void clearTimePeriodTerms() {
        return;
    }


    /**
     *  Sets the commitment <code> Id </code> for this query. 
     *
     *  @param  commitmentId a commitment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> commitmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommitmentId(org.osid.id.Id commitmentId, boolean match) {
        return;
    }


    /**
     *  Clears the commitment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommitmentIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CommitmentQuery </code> is available. 
     *
     *  @return <code> true </code> if a commitment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommitmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a commitment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the commitment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommitmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CommitmentQuery getCommitmentQuery() {
        throw new org.osid.UnimplementedException("supportsCommitmentQuery() is false");
    }


    /**
     *  Matches a calendar that has any event commitment. 
     *
     *  @param  match <code> true </code> to match calendars with any 
     *          commitment, <code> false </code> to match calendars with no 
     *          commitments 
     */

    @OSID @Override
    public void matchAnyCommitment(boolean match) {
        return;
    }


    /**
     *  Clears the commitment terms. 
     */

    @OSID @Override
    public void clearCommitmentTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query to match calendars 
     *  that have the specified calendar as an ancestor. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCalendarId(org.osid.id.Id calendarId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the ancestor calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCalendarQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getAncestorCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCalendarQuery() is false");
    }


    /**
     *  Matches a calendar that has any ancestor. 
     *
     *  @param  match <code> true </code> to match calendars with any 
     *          ancestor, <code> false </code> to match root calendars 
     */

    @OSID @Override
    public void matchAnyAncestorCalendar(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor calendar terms. 
     */

    @OSID @Override
    public void clearAncestorCalendarTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query to match calendars 
     *  that have the specified calendar as a descendant. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCalendarId(org.osid.id.Id calendarId, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the descendant calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery. </code> 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCalendarQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getDescendantCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCalendarQuery() is false");
    }


    /**
     *  Matches a calendar that has any descendant. 
     *
     *  @param  match <code> true </code> to match calendars with any 
     *          descendant, <code> false </code> to match leaf calendars 
     */

    @OSID @Override
    public void matchAnyDescendantCalendar(boolean match) {
        return;
    }


    /**
     *  Clears the descendant calendar terms. 
     */

    @OSID @Override
    public void clearDescendantCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given calendar query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a calendar implementing the requested record.
     *
     *  @param calendarRecordType a calendar record type
     *  @return the calendar query record
     *  @throws org.osid.NullArgumentException
     *          <code>calendarRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(calendarRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.CalendarQueryRecord getCalendarQueryRecord(org.osid.type.Type calendarRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.CalendarQueryRecord record : this.records) {
            if (record.implementsRecordType(calendarRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(calendarRecordType + " is not supported");
    }


    /**
     *  Adds a record to this calendar query. 
     *
     *  @param calendarQueryRecord calendar query record
     *  @param calendarRecordType calendar record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCalendarQueryRecord(org.osid.calendaring.records.CalendarQueryRecord calendarQueryRecord, 
                                          org.osid.type.Type calendarRecordType) {

        addRecordType(calendarRecordType);
        nullarg(calendarQueryRecord, "calendar query record");
        this.records.add(calendarQueryRecord);        
        return;
    }
}
