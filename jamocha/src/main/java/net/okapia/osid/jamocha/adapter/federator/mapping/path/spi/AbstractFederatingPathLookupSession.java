//
// AbstractFederatingPathLookupSession.java
//
//     An abstract federating adapter for an PathLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.mapping.path.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a PathLookupSession. Sessions
 *  are added to this session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPathLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.mapping.path.PathLookupSession>
    implements org.osid.mapping.path.PathLookupSession {

    private boolean parallel = false;
    private org.osid.mapping.Map map = new net.okapia.osid.jamocha.nil.mapping.map.UnknownMap();


    /**
     *  Constructs a new <code>AbstractFederatingPathLookupSession</code>.
     */

    protected AbstractFederatingPathLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.mapping.path.PathLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Map/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Map Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMapId() {
        return (this.map.getId());
    }


    /**
     *  Gets the <code>Map</code> associated with this 
     *  session.
     *
     *  @return the <code>Map</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.Map getMap()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.map);
    }


    /**
     *  Sets the <code>Map</code>.
     *
     *  @param  map the map for this session
     *  @throws org.osid.NullArgumentException <code>map</code>
     *          is <code>null</code>
     */

    protected void setMap(org.osid.mapping.Map map) {
        nullarg(map, "map");
        this.map = map;
        return;
    }


    /**
     *  Tests if this user can perform <code>Path</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPaths() {
        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            if (session.canLookupPaths()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Path</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePathView() {
        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            session.useComparativePathView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Path</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPathView() {
        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            session.usePlenaryPathView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include paths in maps which are children
     *  of this map in the map hierarchy.
     */

    @OSID @Override
    public void useFederatedMapView() {
        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            session.useFederatedMapView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this map only.
     */

    @OSID @Override
    public void useIsolatedMapView() {
        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            session.useIsolatedMapView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Path</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Path</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Path</code> and
     *  retained for compatibility.
     *
     *  @param  pathId <code>Id</code> of the
     *          <code>Path</code>
     *  @return the path
     *  @throws org.osid.NotFoundException <code>pathId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pathId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath(org.osid.id.Id pathId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            try {
                return (session.getPath(pathId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(pathId + " not found");
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  paths specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Paths</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  pathIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>pathIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByIds(org.osid.id.IdList pathIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.mapping.path.path.MutablePathList ret = new net.okapia.osid.jamocha.mapping.path.path.MutablePathList();

        try (org.osid.id.IdList ids = pathIds) {
            while (ids.hasNext()) {
                ret.addPath(getPath(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> which does not include
     *  paths of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.path.FederatingPathList ret = getPathList();

        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByGenusType(pathGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> corresponding to the given
     *  path genus <code>Type</code> and include any additional
     *  paths with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pathGenusType a path genus type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByParentGenusType(org.osid.type.Type pathGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.path.FederatingPathList ret = getPathList();

        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByParentGenusType(pathGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> containing the given
     *  path record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pathRecordType a path record type 
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pathRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsByRecordType(org.osid.type.Type pathRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.path.FederatingPathList ret = getPathList();

        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsByRecordType(pathRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PathList</code> connected to all the given
     *  <code>Locations</code.> In plenary mode, the returned list
     *  contains all of the paths along the locations, or an error
     *  results if a path connected to the location is not found or
     *  inaccessible. Otherwise, inaccessible <code>Paths</code> may
     *  be omitted from the list.
     *
     *  @param  locationIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Path</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>locationIds</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPathsAlongLocations(org.osid.id.IdList locationIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.path.FederatingPathList ret = getPathList();

        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPathsAlongLocations(locationIds));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Paths</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  paths or an error results. Otherwise, the returned list
     *  may contain only those paths that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Paths</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.mapping.path.PathList getPaths()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.mapping.path.path.FederatingPathList ret = getPathList();

        for (org.osid.mapping.path.PathLookupSession session : getSessions()) {
            ret.addPathList(session.getPaths());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.mapping.path.path.FederatingPathList getPathList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.path.ParallelPathList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.mapping.path.path.CompositePathList());
        }
    }
}
