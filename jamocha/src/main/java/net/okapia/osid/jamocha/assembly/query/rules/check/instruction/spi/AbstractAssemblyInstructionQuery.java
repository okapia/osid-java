//
// AbstractAssemblyInstructionQuery.java
//
//     An InstructionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.rules.check.instruction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An InstructionQuery that stores terms.
 */

public abstract class AbstractAssemblyInstructionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.rules.check.InstructionQuery,
               org.osid.rules.check.InstructionQueryInspector,
               org.osid.rules.check.InstructionSearchOrder {

    private final java.util.Collection<org.osid.rules.check.records.InstructionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.rules.check.records.InstructionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.rules.check.records.InstructionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();

    private final AssemblyOsidRelationshipQuery query;


    /** 
     *  Constructs a new <code>AbstractAssemblyInstructionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyInstructionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidRelationshipQuery(assembler);
        return;
    }
    

    /**
     *  Adds a state Id to match. Multiple state Id matches can be 
     *  added to perform a boolean <code> OR </code> among them. 
     *
     *  @param  stateId state Id to match 
     *  @param match <code> true </code> for a positive match, <code>
     *          false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> stateId </code>
     *          is <code> null </code>
     */

    @OSID @Override
    public void matchEndReasonId(org.osid.id.Id stateId, boolean match) {
        this.query.matchEndReasonId(stateId, match);
        return;
    }


    /**
     *  Clears all state Id terms. 
     */

    @OSID @Override
    public void clearEndReasonIdTerms() {
        this.query.clearEndReasonIdTerms();
        return;
    }


    /**
     *  Gets the state Id query terms.
     *
     *  @return the state Id terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEndReasonIdTerms() {
        return (getAssembler().getIdTerms(this.query.getEndReasonIdColumn()));
    }


    /**
     *  Gets the column name for the state Id field.
     *
     *  @return the column name
     */

    protected String getEndReasonIdColumn() {
        return (this.query.getEndReasonIdColumn());
    }


    /**
     *  Tests if a <code> EndReasonQuery </code> is available. 
     *
     *  @return <code> true </code> if a state query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndReasonQuery() {
        return (this.query.supportsEndReasonQuery());
    }


    /**
     *  Gets the query for a state. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the state query 
     *  @throws org.osid.UnimplementedException <code> supportsEndReasonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.StateQuery getEndReasonQuery(boolean match) {
        return (this.query.getEndReasonQuery(match));
    }


    /**
     *  Matches any relationship with a state Id.
     *
     *  @param match <code> true </code> to match any state Id,
     *          <code> false </code> to match relationships with no state
     *          Id
     */

    @OSID @Override
    public void matchAnyEndReason(boolean match) {
        this.query.matchAnyEndReason(match);
        return;
    }


    /**
     *  Clears all state terms. 
     */

    @OSID @Override
    public void clearEndReasonTerms() {
        this.query.clearEndReasonTerms();
        return;
    }


    /**
     *  Gets the state query terms. 
     *
     *  @return the state query terms 
     */

    @OSID @Override
    public org.osid.process.StateQueryInspector[] getEndReasonTerms() {
        return (this.query.getEndReasonTerms());
    }


    /**
     *  Specifies a preference for ordering the results by the end
     *  reason state.
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEndReason(org.osid.SearchOrderStyle style) {
        this.query.orderByEndReason(style);
        return;
    }

    
    /**
     *  Tests if a <code> StateSearchOrder </code> is available. 
     *
     *  @return <code> true </code> if a state search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndReasonSearchOrder() {
        return (this.query.supportsEndReasonSearchOrder());
    }


    /**
     *  Gets the search order for a state. 
     *
     *  @return the state search order 
     *  @throws org.osid.UnimplementedException <code>
     *          supportsEndReasonSearchOrder() </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.process.StateSearchOrder getEndReasonSearchOrder() {
        return (this.query.getEndReasonSearchOrder());
    }


    /**
     *  Gets the column name for the state field.
     *
     *  @return the column name
     */

    protected String getEndReasonColumn() {
        return (this.query.getEndReasonColumn());
    }
    

    /**
     *  Sets the agenda <code> Id </code> for this query. 
     *
     *  @param  agendaId the agenda <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agendaId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgendaId(org.osid.id.Id agendaId, boolean match) {
        getAssembler().addIdTerm(getAgendaIdColumn(), agendaId, match);
        return;
    }


    /**
     *  Clears the agenda <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAgendaIdTerms() {
        getAssembler().clearTerms(getAgendaIdColumn());
        return;
    }


    /**
     *  Gets the agenda <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgendaIdTerms() {
        return (getAssembler().getIdTerms(getAgendaIdColumn()));
    }


    /**
     *  Orders the results by agenda. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgenda(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAgendaColumn(), style);
        return;
    }


    /**
     *  Gets the AgendaId column name.
     *
     * @return the column name
     */

    protected String getAgendaIdColumn() {
        return ("agenda_id");
    }


    /**
     *  Tests if an <code> AgendaQuery </code> is available. 
     *
     *  @return <code> true </code> if an agenda query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agenda. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agenda query 
     *  @throws org.osid.UnimplementedException <code> supportsAgendaQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQuery getAgendaQuery() {
        throw new org.osid.UnimplementedException("supportsAgendaQuery() is false");
    }


    /**
     *  Clears the agenda query terms. 
     */

    @OSID @Override
    public void clearAgendaTerms() {
        getAssembler().clearTerms(getAgendaColumn());
        return;
    }


    /**
     *  Gets the agenda query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaQueryInspector[] getAgendaTerms() {
        return (new org.osid.rules.check.AgendaQueryInspector[0]);
    }


    /**
     *  Tests if an agenda search order is available. 
     *
     *  @return <code> true </code> if an agenda search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgendaSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agenda search order. 
     *
     *  @return the agenda search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsAgendaSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaSearchOrder getAgendaSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgendaSearchOrder() is false");
    }


    /**
     *  Gets the Agenda column name.
     *
     * @return the column name
     */

    protected String getAgendaColumn() {
        return ("agenda");
    }


    /**
     *  Sets the check <code> Id </code> for this query. 
     *
     *  @param  checkId the check <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checkId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCheckId(org.osid.id.Id checkId, boolean match) {
        getAssembler().addIdTerm(getCheckIdColumn(), checkId, match);
        return;
    }


    /**
     *  Clears the check <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCheckIdTerms() {
        getAssembler().clearTerms(getCheckIdColumn());
        return;
    }


    /**
     *  Gets the check query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCheckIdTerms() {
        return (getAssembler().getIdTerms(getCheckIdColumn()));
    }


    /**
     *  Orders the results by check. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCheck(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCheckColumn(), style);
        return;
    }


    /**
     *  Gets the CheckId column name.
     *
     * @return the column name
     */

    protected String getCheckIdColumn() {
        return ("check_id");
    }


    /**
     *  Tests if a <code> CheckQuery </code> is available. 
     *
     *  @return <code> true </code> if a check query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckQuery() {
        return (false);
    }


    /**
     *  Gets the query for a check. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the check query 
     *  @throws org.osid.UnimplementedException <code> supportsCheckQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQuery getCheckQuery() {
        throw new org.osid.UnimplementedException("supportsCheckQuery() is false");
    }


    /**
     *  Clears the check query terms. 
     */

    @OSID @Override
    public void clearCheckTerms() {
        getAssembler().clearTerms(getCheckColumn());
        return;
    }


    /**
     *  Gets the check query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.check.CheckQueryInspector[] getCheckTerms() {
        return (new org.osid.rules.check.CheckQueryInspector[0]);
    }


    /**
     *  Tests if a check search order is available. 
     *
     *  @return <code> true </code> if a check search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCheckSearchOrder() {
        return (false);
    }


    /**
     *  Gets the check search order. 
     *
     *  @return the check search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCheckSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.CheckSearchOrder getCheckSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCheckSearchOrder() is false");
    }


    /**
     *  Gets the Check column name.
     *
     * @return the column name
     */

    protected String getCheckColumn() {
        return ("check");
    }


    /**
     *  Matches instructions within the given position range inclusive. 
     *
     *  @param  message text to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> message or 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchMessage(String message, 
                             org.osid.type.Type stringMatchType, boolean match) {
        getAssembler().addStringTerm(getMessageColumn(), message, stringMatchType, match);
        return;
    }


    /**
     *  Matches instructions that have any message. 
     *
     *  @param  match <code> true </code> to match instructions with any 
     *          message, <code> false </code> to match instructions with no 
     *          message 
     */

    @OSID @Override
    public void matchAnyMessage(boolean match) {
        getAssembler().addStringWildcardTerm(getMessageColumn(), match);
        return;
    }


    /**
     *  Clears the message query terms. 
     */

    @OSID @Override
    public void clearMessageTerms() {
        getAssembler().clearTerms(getMessageColumn());
        return;
    }


    /**
     *  Gets the message query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getMessageTerms() {
        return (getAssembler().getStringTerms(getMessageColumn()));
    }


    /**
     *  Orders the results by message. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMessage(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMessageColumn(), style);
        return;
    }


    /**
     *  Gets the Message column name.
     *
     * @return the column name
     */

    protected String getMessageColumn() {
        return ("message");
    }


    /**
     *  Matches warning instructions. 
     *
     *  @param  match <code> true </code> to match warning instructions, 
     *          <code> false </code> to match hard error instructions 
     */

    @OSID @Override
    public void matchWarning(boolean match) {
        getAssembler().addBooleanTerm(getWarningColumn(), match);
        return;
    }


    /**
     *  Clears the warning query terms. 
     */

    @OSID @Override
    public void clearWarningTerms() {
        getAssembler().clearTerms(getWarningColumn());
        return;
    }


    /**
     *  Gets the warning query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getWarningTerms() {
        return (getAssembler().getBooleanTerms(getWarningColumn()));
    }


    /**
     *  Orders the results by the warning flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByWarning(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getWarningColumn(), style);
        return;
    }


    /**
     *  Gets the Warning column name.
     *
     * @return the column name
     */

    protected String getWarningColumn() {
        return ("warning");
    }


    /**
     *  Matches continue-on-fail instructions. 
     *
     *  @param  match <code> true </code> to match continue-on-fail 
     *          instructions, <code> false </code> otherwise 
     */

    @OSID @Override
    public void matchContinueOnFail(boolean match) {
        getAssembler().addBooleanTerm(getContinueOnFailColumn(), match);
        return;
    }


    /**
     *  Clears the continue-on-fail query terms. 
     */

    @OSID @Override
    public void clearContinueOnFailTerms() {
        getAssembler().clearTerms(getContinueOnFailColumn());
        return;
    }


    /**
     *  Gets the continue-on-fail query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getContinueOnFailTerms() {
        return (getAssembler().getBooleanTerms(getContinueOnFailColumn()));
    }


    /**
     *  Orders the results by the continue-on-fail flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByContinueOnFail(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getContinueOnFailColumn(), style);
        return;
    }


    /**
     *  Gets the ContinueOnFail column name.
     *
     * @return the column name
     */

    protected String getContinueOnFailColumn() {
        return ("continue_on_fail");
    }


    /**
     *  Sets the engine <code> Id </code> for this query to match instructions 
     *  assigned to engines. 
     *
     *  @param  engineId the engine <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEngineId(org.osid.id.Id engineId, boolean match) {
        getAssembler().addIdTerm(getEngineIdColumn(), engineId, match);
        return;
    }


    /**
     *  Clears the engine <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEngineIdTerms() {
        getAssembler().clearTerms(getEngineIdColumn());
        return;
    }


    /**
     *  Gets the engine <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEngineIdTerms() {
        return (getAssembler().getIdTerms(getEngineIdColumn()));
    }


    /**
     *  Gets the EngineId column name.
     *
     * @return the column name
     */

    protected String getEngineIdColumn() {
        return ("engine_id");
    }


    /**
     *  Tests if a <code> EngineQuery </code> is available. 
     *
     *  @return <code> true </code> if an engine query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (false);
    }


    /**
     *  Gets the query for an engine. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the engine query 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuery getEngineQuery() {
        throw new org.osid.UnimplementedException("supportsEngineQuery() is false");
    }


    /**
     *  Clears the engine query terms. 
     */

    @OSID @Override
    public void clearEngineTerms() {
        getAssembler().clearTerms(getEngineColumn());
        return;
    }


    /**
     *  Gets the engine query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.rules.EngineQueryInspector[] getEngineTerms() {
        return (new org.osid.rules.EngineQueryInspector[0]);
    }


    /**
     *  Gets the Engine column name.
     *
     * @return the column name
     */

    protected String getEngineColumn() {
        return ("engine");
    }


    /**
     *  Tests if this instruction supports the given record
     *  <code>Type</code>.
     *
     *  @param  instructionRecordType an instruction record type 
     *  @return <code>true</code> if the instructionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type instructionRecordType) {
        for (org.osid.rules.check.records.InstructionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  instructionRecordType the instruction record type 
     *  @return the instruction query record 
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(instructionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.InstructionQueryRecord getInstructionQueryRecord(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.InstructionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(instructionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  instructionRecordType the instruction record type 
     *  @return the instruction query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(instructionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.InstructionQueryInspectorRecord getInstructionQueryInspectorRecord(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.InstructionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(instructionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param instructionRecordType the instruction record type
     *  @return the instruction search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(instructionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.InstructionSearchOrderRecord getInstructionSearchOrderRecord(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.rules.check.records.InstructionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(instructionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(instructionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this instruction. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param instructionQueryRecord the instruction query record
     *  @param instructionQueryInspectorRecord the instruction query inspector
     *         record
     *  @param instructionSearchOrderRecord the instruction search order record
     *  @param instructionRecordType instruction record type
     *  @throws org.osid.NullArgumentException
     *          <code>instructionQueryRecord</code>,
     *          <code>instructionQueryInspectorRecord</code>,
     *          <code>instructionSearchOrderRecord</code> or
     *          <code>instructionRecordTypeinstruction</code> is
     *          <code>null</code>
     */
            
    protected void addInstructionRecords(org.osid.rules.check.records.InstructionQueryRecord instructionQueryRecord, 
                                      org.osid.rules.check.records.InstructionQueryInspectorRecord instructionQueryInspectorRecord, 
                                      org.osid.rules.check.records.InstructionSearchOrderRecord instructionSearchOrderRecord, 
                                      org.osid.type.Type instructionRecordType) {

        addRecordType(instructionRecordType);

        nullarg(instructionQueryRecord, "instruction query record");
        nullarg(instructionQueryInspectorRecord, "instruction query inspector record");
        nullarg(instructionSearchOrderRecord, "instruction search odrer record");

        this.queryRecords.add(instructionQueryRecord);
        this.queryInspectorRecords.add(instructionQueryInspectorRecord);
        this.searchOrderRecords.add(instructionSearchOrderRecord);
        
        return;
    }


    protected class AssemblyOsidRelationshipQuery
        extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
        implements org.osid.OsidRelationshipQuery,
                   org.osid.OsidRelationshipQueryInspector,
                   org.osid.OsidRelationshipSearchOrder {

        
        /** 
         *  Constructs a new
         *  <code>AssemblyOsidRelationshipQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidRelationshipQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }
    

        /**
         *  Gets the column name for the state Id field.
         *
         *  @return the column name
         */
        
        protected String getEndReasonIdColumn() {
            return (super.getEndReasonIdColumn());
        }
        
        
        /**
         *  Gets the column name for the state field.
         *
         *  @return the column name
         */
        
        protected String getEndReasonColumn() {
            return (super.getEndReasonColumn());
        }
    }
}
