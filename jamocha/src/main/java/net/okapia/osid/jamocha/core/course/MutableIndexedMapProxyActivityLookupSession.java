//
// MutableIndexedMapProxyActivityLookupSession
//
//    Implements an Activity lookup service backed by a collection of
//    activities indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course;


/**
 *  Implements an Activity lookup service backed by a collection of
 *  activities. The activities are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some activities may be compatible
 *  with more types than are indicated through these activity
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of activities can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyActivityLookupSession
    extends net.okapia.osid.jamocha.core.course.spi.AbstractIndexedMapActivityLookupSession
    implements org.osid.course.ActivityLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityLookupSession} with
     *  no activity.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityLookupSession} with
     *  a single activity.
     *
     *  @param courseCatalog the course catalog
     *  @param  activity an activity
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activity}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.Activity activity, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putActivity(activity);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityLookupSession} using
     *  an array of activities.
     *
     *  @param courseCatalog the course catalog
     *  @param  activities an array of activities
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activities}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.Activity[] activities, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putActivities(activities);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyActivityLookupSession} using
     *  a collection of activities.
     *
     *  @param courseCatalog the course catalog
     *  @param  activities a collection of activities
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code activities}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyActivityLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       java.util.Collection<? extends org.osid.course.Activity> activities,
                                                       org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putActivities(activities);
        return;
    }

    
    /**
     *  Makes an {@code Activity} available in this session.
     *
     *  @param  activity an activity
     *  @throws org.osid.NullArgumentException {@code activity{@code 
     *          is {@code null}
     */

    @Override
    public void putActivity(org.osid.course.Activity activity) {
        super.putActivity(activity);
        return;
    }


    /**
     *  Makes an array of activities available in this session.
     *
     *  @param  activities an array of activities
     *  @throws org.osid.NullArgumentException {@code activities{@code 
     *          is {@code null}
     */

    @Override
    public void putActivities(org.osid.course.Activity[] activities) {
        super.putActivities(activities);
        return;
    }


    /**
     *  Makes collection of activities available in this session.
     *
     *  @param  activities a collection of activities
     *  @throws org.osid.NullArgumentException {@code activity{@code 
     *          is {@code null}
     */

    @Override
    public void putActivities(java.util.Collection<? extends org.osid.course.Activity> activities) {
        super.putActivities(activities);
        return;
    }


    /**
     *  Removes an Activity from this session.
     *
     *  @param activityId the {@code Id} of the activity
     *  @throws org.osid.NullArgumentException {@code activityId{@code  is
     *          {@code null}
     */

    @Override
    public void removeActivity(org.osid.id.Id activityId) {
        super.removeActivity(activityId);
        return;
    }    
}
