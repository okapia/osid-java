//
// AbstractPlanQueryInspector.java
//
//     A template for making a PlanQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.plan.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for plans.
 */

public abstract class AbstractPlanQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.course.plan.PlanQueryInspector {

    private final java.util.Collection<org.osid.course.plan.records.PlanQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the syllabus <code> Id </code> terms. 
     *
     *  @return the syllabus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSyllabusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the syllabus terms. 
     *
     *  @return the syllabus terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusQueryInspector[] getSyllabusTerms() {
        return (new org.osid.course.syllabus.SyllabusQueryInspector[0]);
    }


    /**
     *  Gets the course offering <code> Id </code> terms. 
     *
     *  @return the course offering <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course offering terms. 
     *
     *  @return the course offering terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Gets the module <code> Id </code> terms. 
     *
     *  @return the module <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModuleIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the module terms. 
     *
     *  @return the module terms 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleQueryInspector[] getModuleTerms() {
        return (new org.osid.course.syllabus.ModuleQueryInspector[0]);
    }


    /**
     *  Gets the lesson <code> Id </code> terms. 
     *
     *  @return the lesson <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLessonIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the lesson terms. 
     *
     *  @return the lesson terms 
     */

    @OSID @Override
    public org.osid.course.plan.LessonQueryInspector[] getLessonTerms() {
        return (new org.osid.course.plan.LessonQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> terms. 
     *
     *  @return the course catalog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog terms. 
     *
     *  @return the course catalog terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given plan query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a plan implementing the requested record.
     *
     *  @param planRecordType a plan record type
     *  @return the plan query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>planRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(planRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.plan.records.PlanQueryInspectorRecord getPlanQueryInspectorRecord(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.plan.records.PlanQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(planRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(planRecordType + " is not supported");
    }


    /**
     *  Adds a record to this plan query. 
     *
     *  @param planQueryInspectorRecord plan query inspector
     *         record
     *  @param planRecordType plan record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPlanQueryInspectorRecord(org.osid.course.plan.records.PlanQueryInspectorRecord planQueryInspectorRecord, 
                                                   org.osid.type.Type planRecordType) {

        addRecordType(planRecordType);
        nullarg(planRecordType, "plan record type");
        this.records.add(planQueryInspectorRecord);        
        return;
    }
}
