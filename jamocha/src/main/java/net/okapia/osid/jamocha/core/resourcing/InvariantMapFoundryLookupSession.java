//
// InvariantMapFoundryLookupSession
//
//    Implements a Foundry lookup service backed by a fixed collection of
//    foundries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing;


/**
 *  Implements a Foundry lookup service backed by a fixed
 *  collection of foundries. The foundries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapFoundryLookupSession
    extends net.okapia.osid.jamocha.core.resourcing.spi.AbstractMapFoundryLookupSession
    implements org.osid.resourcing.FoundryLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapFoundryLookupSession</code> with no
     *  foundries.
     */

    public InvariantMapFoundryLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapFoundryLookupSession</code> with a single
     *  foundry.
     *  
     *  @throws org.osid.NullArgumentException {@code foundry}
     *          is <code>null</code>
     */

    public InvariantMapFoundryLookupSession(org.osid.resourcing.Foundry foundry) {
        putFoundry(foundry);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapFoundryLookupSession</code> using an array
     *  of foundries.
     *  
     *  @throws org.osid.NullArgumentException {@code foundries}
     *          is <code>null</code>
     */

    public InvariantMapFoundryLookupSession(org.osid.resourcing.Foundry[] foundries) {
        putFoundries(foundries);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapFoundryLookupSession</code> using a
     *  collection of foundries.
     *
     *  @throws org.osid.NullArgumentException {@code foundries}
     *          is <code>null</code>
     */

    public InvariantMapFoundryLookupSession(java.util.Collection<? extends org.osid.resourcing.Foundry> foundries) {
        putFoundries(foundries);
        return;
    }
}
