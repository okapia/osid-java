//
// AbstractFederatingPackageLookupSession.java
//
//     An abstract federating adapter for a PackageLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PackageLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPackageLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.installation.PackageLookupSession>
    implements org.osid.installation.PackageLookupSession {

    private boolean parallel = false;
    private org.osid.installation.Depot depot = new net.okapia.osid.jamocha.nil.installation.depot.UnknownDepot();


    /**
     *  Constructs a new <code>AbstractFederatingPackageLookupSession</code>.
     */

    protected AbstractFederatingPackageLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.installation.PackageLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Depot/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Depot Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDepotId() {
        return (this.depot.getId());
    }


    /**
     *  Gets the <code>Depot</code> associated with this 
     *  session.
     *
     *  @return the <code>Depot</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.depot);
    }


    /**
     *  Sets the <code>Depot</code>.
     *
     *  @param  depot the depot for this session
     *  @throws org.osid.NullArgumentException <code>depot</code>
     *          is <code>null</code>
     */

    protected void setDepot(org.osid.installation.Depot depot) {
        nullarg(depot, "depot");
        this.depot = depot;
        return;
    }


    /**
     *  Tests if this user can perform <code>Package</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPackages() {
        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            if (session.canLookupPackages()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Package</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePackageView() {
        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            session.useComparativePackageView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Package</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPackageView() {
        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            session.usePlenaryPackageView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include packages in depots which are children
     *  of this depot in the depot hierarchy.
     */

    @OSID @Override
    public void useFederatedDepotView() {
        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            session.useFederatedDepotView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this depot only.
     */

    @OSID @Override
    public void useIsolatedDepotView() {
        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            session.useIsolatedDepotView();
        }

        return;
    }

    
    /**
     *  The returns from the lookup methods may omit multiple versions
     *  of the same package.
     */

    @OSID @Override
    public void useNormalizedVersionView() {
        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            session.useNormalizedVersionView();
        }

        return;
    }


    /**
     *  All versions of the same package are returned.
     */

    @OSID @Override
    public void useDenormalizedVersionView() {
        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            session.useDenormalizedVersionView();
        }

        return;
    }


    /**
     *  Gets the <code>Package</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Package</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Package</code> and
     *  retained for compatibility.
     *
     *  @param  pkgId <code>Id</code> of the
     *          <code>Package</code>
     *  @return the package
     *  @throws org.osid.NotFoundException <code>pkgId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>pkgId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.Package getPackage(org.osid.id.Id pkgId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            try {
                return (session.getPackage(pkgId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(pkgId + " not found");
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  packages specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Packages</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  pkgIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>pkgIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByIds(org.osid.id.IdList pkgIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.installation.pkg.MutablePackageList ret = new net.okapia.osid.jamocha.installation.pkg.MutablePackageList();

        try (org.osid.id.IdList ids = pkgIds) {
            while (ids.hasNext()) {
                ret.addPackage(getPackage(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  package genus <code>Type</code> which does not include
     *  packages of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pkgGenusType a pkg genus type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByGenusType(org.osid.type.Type pkgGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.pkg.FederatingPackageList ret = getPackageList();

        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            ret.addPackageList(session.getPackagesByGenusType(pkgGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PackageList</code> corresponding to the given
     *  package genus <code>Type</code> and include any additional
     *  packages with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pkgGenusType a pkg genus type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByParentGenusType(org.osid.type.Type pkgGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.pkg.FederatingPackageList ret = getPackageList();

        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            ret.addPackageList(session.getPackagesByParentGenusType(pkgGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    

    /**
     *  Gets a <code>PackageList</code> containing the given
     *  package record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  pkgRecordType a pkg record type 
     *  @return the returned <code>Package</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>pkgRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByRecordType(org.osid.type.Type pkgRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.pkg.FederatingPackageList ret = getPackageList();

        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            ret.addPackageList(session.getPackagesByRecordType(pkgRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PackageList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known packages or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  packages that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Package</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackagesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.installation.pkg.FederatingPackageList ret = getPackageList();

        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            ret.addPackageList(session.getPackagesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of packages depending on the given package.
     *
     *  @param  packageId an <code>Id</code> of a <code>Package</code>
     *  @return list of package dependents
     *  @throws org.osid.NotFoundException <code>packageId</code> is
     *          not found
     *  @throws org.osid.NullArgumentException <code>packageId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.installation.PackageList getDependentPackages(org.osid.id.Id packageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.pkg.FederatingPackageList ret = getPackageList();

        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            ret.addPackageList(session.getDependentPackages(packageId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of packages in the specified package version chain.
     *
     *  @param  packageId an <code>Id</code> of a <code>Package</code>
     *  @return list of dependencies
     *  @throws org.osid.NotFoundException <code>packageId</code> is
     *          not found
     *  @throws org.osid.NullArgumentException <code>packageId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackageVersions(org.osid.id.Id packageId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.pkg.FederatingPackageList ret = getPackageList();

        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            ret.addPackageList(session.getPackageVersions(packageId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Packages</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  packages or an error results. Otherwise, the returned list
     *  may contain only those packages that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Packages</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.installation.PackageList getPackages()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.installation.pkg.FederatingPackageList ret = getPackageList();

        for (org.osid.installation.PackageLookupSession session : getSessions()) {
            ret.addPackageList(session.getPackages());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.installation.pkg.FederatingPackageList getPackageList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.installation.pkg.ParallelPackageList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.installation.pkg.CompositePackageList());
        }
    }
}
