//
// AbstractIndexedMapInstructionLookupSession.java
//
//    A simple framework for providing an Instruction lookup service
//    backed by a fixed collection of instructions with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Instruction lookup service backed by a
 *  fixed collection of instructions. The instructions are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some instructions may be compatible
 *  with more types than are indicated through these instruction
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Instructions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapInstructionLookupSession
    extends AbstractMapInstructionLookupSession
    implements org.osid.rules.check.InstructionLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.rules.check.Instruction> instructionsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.rules.check.Instruction>());
    private final MultiMap<org.osid.type.Type, org.osid.rules.check.Instruction> instructionsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.rules.check.Instruction>());


    /**
     *  Makes an <code>Instruction</code> available in this session.
     *
     *  @param  instruction an instruction
     *  @throws org.osid.NullArgumentException <code>instruction<code> is
     *          <code>null</code>
     */

    @Override
    protected void putInstruction(org.osid.rules.check.Instruction instruction) {
        super.putInstruction(instruction);

        this.instructionsByGenus.put(instruction.getGenusType(), instruction);
        
        try (org.osid.type.TypeList types = instruction.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.instructionsByRecord.put(types.getNextType(), instruction);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an instruction from this session.
     *
     *  @param instructionId the <code>Id</code> of the instruction
     *  @throws org.osid.NullArgumentException <code>instructionId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeInstruction(org.osid.id.Id instructionId) {
        org.osid.rules.check.Instruction instruction;
        try {
            instruction = getInstruction(instructionId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.instructionsByGenus.remove(instruction.getGenusType());

        try (org.osid.type.TypeList types = instruction.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.instructionsByRecord.remove(types.getNextType(), instruction);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeInstruction(instructionId);
        return;
    }


    /**
     *  Gets an <code>InstructionList</code> corresponding to the given
     *  instruction genus <code>Type</code> which does not include
     *  instructions of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known instructions or an error results. Otherwise,
     *  the returned list may contain only those instructions that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  instructionGenusType an instruction genus type 
     *  @return the returned <code>Instruction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>instructionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByGenusType(org.osid.type.Type instructionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.check.instruction.ArrayInstructionList(this.instructionsByGenus.get(instructionGenusType)));
    }


    /**
     *  Gets an <code>InstructionList</code> containing the given
     *  instruction record <code>Type</code>. In plenary mode, the
     *  returned list contains all known instructions or an error
     *  results. Otherwise, the returned list may contain only those
     *  instructions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  instructionRecordType an instruction record type 
     *  @return the returned <code>instruction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>instructionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.InstructionList getInstructionsByRecordType(org.osid.type.Type instructionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.check.instruction.ArrayInstructionList(this.instructionsByRecord.get(instructionRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.instructionsByGenus.clear();
        this.instructionsByRecord.clear();

        super.close();

        return;
    }
}
