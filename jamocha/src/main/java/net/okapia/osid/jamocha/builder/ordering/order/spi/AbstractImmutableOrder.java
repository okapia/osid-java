//
// AbstractImmutableOrder.java
//
//     Wraps a mutable Order to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.ordering.order.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Order</code> to hide modifiers. This
 *  wrapper provides an immutized Order from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying order whose state changes are visible.
 */

public abstract class AbstractImmutableOrder
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.ordering.Order {

    private final org.osid.ordering.Order order;


    /**
     *  Constructs a new <code>AbstractImmutableOrder</code>.
     *
     *  @param order the order to immutablize
     *  @throws org.osid.NullArgumentException <code>order</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOrder(org.osid.ordering.Order order) {
        super(order);
        this.order = order;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the customer. 
     *
     *  @return the customer <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCustomerId() {
        return (this.order.getCustomerId());
    }


    /**
     *  Gets the customer. 
     *
     *  @return the customer 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCustomer()
        throws org.osid.OperationFailedException {

        return (this.order.getCustomer());
    }


    /**
     *  Gets the <code> Ids </code> of the items. 
     *
     *  @return the item <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getItemIds() {
        return (this.order.getItemIds());
    }


    /**
     *  Gets the items. 
     *
     *  @return the items 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.ItemList getItems()
        throws org.osid.OperationFailedException {

        return (this.order.getItems());
    }


    /**
     *  Gets the total cost for this order. 
     *
     *  @return the total cost 
     */

    @OSID @Override
    public org.osid.financials.Currency getTotalCost() {
        return (this.order.getTotalCost());
    }


    /**
     *  Tests if all the items are processed atomically. 
     *
     *  @return <code> true </code> if the order is atomic, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isAtomic() {
        return (this.order.isAtomic());
    }


    /**
     *  Tests if all this order has been submitted. 
     *
     *  @return <code> true </code> if the order has been submitted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isSubmitted() {
        return (this.order.isSubmitted());
    }


    /**
     *  Gets the date submitted. 
     *
     *  @return the date submitted 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getSubmitDate() {
        return (this.order.getSubmitDate());
    }


    /**
     *  Gets the resource <code> Id </code> who submitted the order. 
     *
     *  @return the submitting resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubmitterId() {
        return (this.order.getSubmitterId());
    }


    /**
     *  Gets the resource who submitted the order. 
     *
     *  @return the submitting resource 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSubmitter()
        throws org.osid.OperationFailedException {

        return (this.order.getSubmitter());
    }


    /**
     *  Gets the agent <code> Id </code> who submitted the order. 
     *
     *  @return the submitting agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubmittingAgentId() {
        return (this.order.getSubmitterId());
    }


    /**
     *  Gets the agent who submitted the order. 
     *
     *  @return the submitting agent 
     *  @throws org.osid.IllegalStateException <code> isSubmitted() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getSubmittingAgent()
        throws org.osid.OperationFailedException {

        return (this.order.getSubmittingAgent());
    }


    /**
     *  Tests if all this order is closed or canceled. 
     *
     *  @return <code> true </code> if the order is closed, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isClosed() {
        return (this.order.isClosed());
    }


    /**
     *  Gets the date this order has been closed. 
     *
     *  @return the date closed 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClosedDate() {
        return (this.order.getClosedDate());
    }


    /**
     *  Gets the resource <code> Id </code> who closed the order. 
     *
     *  @return the closing resource <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCloserId() {
        return (this.order.getCloserId());
    }


    /**
     *  Gets the resource who closed the order. 
     *
     *  @return the closing resource
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCloser()
        throws org.osid.OperationFailedException {

        return (this.order.getCloser());
    }


    /**
     *  Gets the agent <code> Id </code> who closed the order. 
     *
     *  @return the closing agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getClosingAgentId() {
        return (this.order.getCloserId());
    }


    /**
     *  Gets the agent who closed the order. 
     *
     *  @return the closing agent 
     *  @throws org.osid.IllegalStateException <code> isClosed() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getClosingAgent()
        throws org.osid.OperationFailedException {

        return (this.order.getClosingAgent());
    }


    /**
     *  Gets the order record corresponding to the given <code> Order </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> orderRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(orderRecordType) </code> is <code> true </code> . 
     *
     *  @param  orderRecordType the type of order record to retrieve 
     *  @return the order record 
     *  @throws org.osid.NullArgumentException <code> orderRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(orderRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.records.OrderRecord getOrderRecord(org.osid.type.Type orderRecordType)
        throws org.osid.OperationFailedException {

        return (this.order.getOrderRecord(orderRecordType));
    }
}

