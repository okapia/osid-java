//
// AbstractBankQuery.java
//
//     A template for making a Bank Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.bank.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for banks.
 */

public abstract class AbstractBankQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.assessment.BankQuery {

    private final java.util.Collection<org.osid.assessment.records.BankQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears all item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches assessment banks that have any item assigned. 
     *
     *  @param  match <code> true </code> to match banks with any item, <code> 
     *          false </code> to match assessments with no item 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears all item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        return;
    }


    /**
     *  Clears all assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches assessment banks that have any assessment assigned. 
     *
     *  @param  match <code> true </code> to match banks with any assessment, 
     *          <code> false </code> to match banks with no assessment 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        return;
    }


    /**
     *  Clears all assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        return;
    }


    /**
     *  Sets the assessment offered <code> Id </code> for this query. 
     *
     *  @param  assessmentOfferedId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentOfferedId(org.osid.id.Id assessmentOfferedId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears all assessment offered <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AssessmentOfferedQuery </code> is available. 
     *
     *  @return <code> true </code> if an assessment offered query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedQuery() {
        return (false);
    }


    /**
     *  Gets the query for an assessment offered. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the assessment offered query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuery getAssessmentOfferedQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentOfferedQuery() is false");
    }


    /**
     *  Matches assessment banks that have any assessment offering assigned. 
     *
     *  @param  match <code> true </code> to match banks with any assessment 
     *          offering, <code> false </code> to match banks with no offering 
     */

    @OSID @Override
    public void matchAnyAssessmentOffered(boolean match) {
        return;
    }


    /**
     *  Clears all assessment offered terms. 
     */

    @OSID @Override
    public void clearAssessmentOfferedTerms() {
        return;
    }


    /**
     *  Sets the bank <code> Id </code> for to match banks in which the 
     *  specified bank is an acestor. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorBankId(org.osid.id.Id bankId, boolean match) {
        return;
    }


    /**
     *  Clears all ancestor bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ancestor bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBankQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getAncestorBankQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBankQuery() is false");
    }


    /**
     *  Matches a bank that has any ancestor. 
     *
     *  @param  match <code> true </code> to match banks with any ancestor 
     *          banks, <code> false </code> to match root banks 
     */

    @OSID @Override
    public void matchAnyAncestorBank(boolean match) {
        return;
    }


    /**
     *  Clears all ancestor bank terms. 
     */

    @OSID @Override
    public void clearAncestorBankTerms() {
        return;
    }


    /**
     *  Sets the bank <code> Id </code> for to match banks in which the 
     *  specified bank is a descendant. 
     *
     *  @param  bankId a bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bankId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantBankId(org.osid.id.Id bankId, boolean match) {
        return;
    }


    /**
     *  Clears all descendant bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBankIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BankQuery </code> is available. 
     *
     *  @return <code> true </code> if a bank query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for a descendant bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBankQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuery getDescendantBankQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBankQuery() is false");
    }


    /**
     *  Matches a bank that has any descendant. 
     *
     *  @param  match <code> true </code> to match banks with any descendant 
     *          banks, <code> false </code> to match leaf banks 
     */

    @OSID @Override
    public void matchAnyDescendantBank(boolean match) {
        return;
    }


    /**
     *  Clears all descendant bank terms. 
     */

    @OSID @Override
    public void clearDescendantBankTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given bank query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a bank implementing the requested record.
     *
     *  @param bankRecordType a bank record type
     *  @return the bank query record
     *  @throws org.osid.NullArgumentException
     *          <code>bankRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bankRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.BankQueryRecord getBankQueryRecord(org.osid.type.Type bankRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.BankQueryRecord record : this.records) {
            if (record.implementsRecordType(bankRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bankRecordType + " is not supported");
    }


    /**
     *  Adds a record to this bank query. 
     *
     *  @param bankQueryRecord bank query record
     *  @param bankRecordType bank record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBankQueryRecord(org.osid.assessment.records.BankQueryRecord bankQueryRecord, 
                                          org.osid.type.Type bankRecordType) {

        addRecordType(bankRecordType);
        nullarg(bankQueryRecord, "bank query record");
        this.records.add(bankQueryRecord);        
        return;
    }
}
