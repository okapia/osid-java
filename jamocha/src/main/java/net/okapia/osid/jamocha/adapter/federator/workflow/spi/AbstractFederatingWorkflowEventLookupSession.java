//
// AbstractFederatingWorkflowEventLookupSession.java
//
//     An abstract federating adapter for a WorkflowEventLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  WorkflowEventLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingWorkflowEventLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.workflow.WorkflowEventLookupSession>
    implements org.osid.workflow.WorkflowEventLookupSession {

    private boolean parallel = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();


    /**
     *  Constructs a new <code>AbstractFederatingWorkflowEventLookupSession</code>.
     */

    protected AbstractFederatingWorkflowEventLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.workflow.WorkflowEventLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Office/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>WorkflowEvent</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupWorkflowEvents() {
        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            if (session.canLookupWorkflowEvents()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>WorkflowEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeWorkflowEventView() {
        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            session.useComparativeWorkflowEventView();
        }

        return;
    }


    /**
     *  A complete view of the <code>WorkflowEvent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryWorkflowEventView() {
        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            session.usePlenaryWorkflowEventView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include workflow events in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            session.useFederatedOfficeView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            session.useIsolatedOfficeView();
        }

        return;
    }

     
    /**
     *  Gets the <code>WorkflowEvent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>WorkflowEvent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>WorkflowEvent</code> and
     *  retained for compatibility.
     *
     *  @param  workflowEventId <code>Id</code> of the
     *          <code>WorkflowEvent</code>
     *  @return the workflow event
     *  @throws org.osid.NotFoundException <code>workflowEventId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>workflowEventId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEvent getWorkflowEvent(org.osid.id.Id workflowEventId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            try {
                return (session.getWorkflowEvent(workflowEventId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(workflowEventId + " not found");
    }


    /**
     *  Gets a <code>WorkflowEventList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  workflowEvents specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>WorkflowEvents</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  @param  workflowEventIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>WorkflowEvent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByIds(org.osid.id.IdList workflowEventIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.workflow.workflowevent.MutableWorkflowEventList ret = new net.okapia.osid.jamocha.workflow.workflowevent.MutableWorkflowEventList();

        try (org.osid.id.IdList ids = workflowEventIds) {
            while (ids.hasNext()) {
                ret.addWorkflowEvent(getWorkflowEvent(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>WorkflowEventList</code> corresponding to the
     *  given workflow event genus <code>Type</code> which does not
     *  include workflow events of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known workflow
     *  events or an error results. Otherwise, the returned list may
     *  contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workflowEventGenusType a workflowEvent genus type 
     *  @return the returned <code>WorkflowEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByGenusType(org.osid.type.Type workflowEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByGenusType(workflowEventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>WorkflowEventList</code> corresponding to the
     *  given workflow event genus <code>Type</code> and include any
     *  additional workflow events with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known workflow
     *  events or an error results. Otherwise, the returned list may
     *  contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workflowEventGenusType a workflowEvent genus type 
     *  @return the returned <code>WorkflowEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByParentGenusType(org.osid.type.Type workflowEventGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByParentGenusType(workflowEventGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>WorkflowEventList</code> containing the given
     *  workflow event record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known workflow
     *  events or an error results. Otherwise, the returned list may
     *  contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  workflowEventRecordType a workflowEvent record type 
     *  @return the returned <code>WorkflowEvent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>workflowEventRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByRecordType(org.osid.type.Type workflowEventRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByRecordType(workflowEventRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the entire workflow log within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  from start range
     *  @param  to end range
     *  @return the workflow events
     *  @throws org.osid.InvalidArgumentException <code> from</code> is
     *          greater than <code> to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDate(org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByDate(from, to));
        }

        ret.noMore();
        return (ret);
    }

    
    /**
     *  Gets the entire workflow log for a process. In plenary mode,
     *  the returned list contains all known workflow events or an
     *  error results.  Otherwise, the returned list may contain only
     *  those workflow events that are accessible through this
     *  session.
     *
     *  @param  processId a process <code>Id</code>
     *  @return the workflow events
     *  @throws org.osid.NullArgumentException <code>processId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForProcess(org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsForProcess(processId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the entire workflow log for this process within the given
     *  date range inclusive. In plenary mode, the returned list
     *  contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  processId a process <code>Id</code>
     *  @param  from start range
     *  @param  to end range
     *  @return the workflow events
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>processId</code>,
     *         <code>from</code>, or <code>to</code> is
     *         <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForProcess(org.osid.id.Id processId,
                                                                                 org.osid.calendaring.DateTime from,
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByDateForProcess(processId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a step. In plenary mode, the
     *  returned list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  stepId a step <code>Id</code>
     *  @return the workflow events
     *  @throws org.osid.NullArgumentException <code>stepId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForStep(org.osid.id.Id stepId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsForStep(stepId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a step within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  stepId a step <code>Id</code>
     *  @param  from start range
     *  @param  to end range
     *  @return the workflow events
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>stepId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForStep(org.osid.id.Id stepId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByDateForStep(stepId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a work. In plenary mode, the
     *  returned list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  workId a work <code>Id</code>
     *  @return the workflow events
     *  @throws org.osid.NullArgumentException <code>workId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWork(org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsForWork(workId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a work within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  workId a work <code>Id</code>
     *  @param  from start range
     *  @param  to end range
     *  @return the workflow events
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>workId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWork(org.osid.id.Id workId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByDateForWork(workId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a work in a process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results.  Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  processId a process <code>Id</code>
     *  @param  workId a work <code>Id</code>
     *  @return the workflow events
     *  @throws org.osid.NullArgumentException <code>workId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWorkAndProcess(org.osid.id.Id processId,
                                                                                  org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsForWorkAndProcess(processId, workId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a work in a process within the given
     *  date range inclusive. In plenary mode, the returned list
     *  contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  processId a process <code>Id</code>
     *  @param  workId a work <code>Id</code>
     *  @param  from start range
     *  @param  to end range
     *  @return the workflow events
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>workId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWorkAndProcess(org.osid.id.Id processId,
                                                                                        org.osid.id.Id workId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByDateForWorkAndProcess(processId, workId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a work in this process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results.  Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  stepId a step <code>Id</code>
     *  @param  workId a work <code>Id</code>
     *  @return the workflow events
     *  @throws org.osid.NullArgumentException <code>stepId</code> or
     *          <code>workId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForStepAndWork(org.osid.id.Id stepId,
                                                                               org.osid.id.Id workId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsForStepAndWork(stepId, workId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a work in this process within the
     *  given date range inclusive. In plenary mode, the returned list
     *  contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  stepId a step <code>Id</code>
     *  @param  workId a work <code>Id</code>
     *  @param  from start range
     *  @param  to end range
     *  @return the workflow events
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>stepId</code>,
     *          <code>workId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForStepAndWork(org.osid.id.Id stepId,
                                                                                     org.osid.id.Id workId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByDateForStepAndWork(stepId, workId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a worker. In plenary mode, the
     *  returned list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  resourceId a worker <code>Id</code>
     *  @return the workflow events
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWorker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsForWorker(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log for a worker within the given date range
     *  inclusive. In plenary mode, the returned list contains all
     *  known workflow events or an error results. Otherwise, the
     *  returned list may contain only those workflow events that are
     *  accessible through this session.
     *
     *  @param  resourceId a worker <code>Id</code>
     *  @param  from start range
     *  @param  to end range
     *  @return the workflow events
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWorker(org.osid.id.Id resourceId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByDateForWorker(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }
    

    /**
     *  Gets the workflow log by an agent in this process. In plenary
     *  mode, the returned list contains all known workflow events or
     *  an error results. Otherwise, the returned list may contain
     *  only those workflow events that are accessible through this
     *  session.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  processId a process <code>Id</code>
     *  @return the workflow events
     *  @throws org.osid.NullArgumentException <code>resourceId</code> or
     *          <code>processId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsForWorkerAndProcess(org.osid.id.Id resourceId,
                                                                                    org.osid.id.Id processId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsForWorkerAndProcess(resourceId, processId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets the workflow log by the resource in this process within
     *  the given date range inclusive. In plenary mode, the returned
     *  list contains all known workflow events or an error
     *  results. Otherwise, the returned list may contain only those
     *  workflow events that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code>
     *  @param  processId a process <code>Id</code>
     *  @param  from start range
     *  @param  to end range
     *  @return the workflow events
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>processId</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEventsByDateForWorkerAndProcess(org.osid.id.Id resourceId,
                                                                                          org.osid.id.Id processId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEventsByDateForWorkerAndProcess(resourceId, processId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>WorkflowEvents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  workflow events or an error results. Otherwise, the returned list
     *  may contain only those workflow events that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>WorkflowEvents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowEventList getWorkflowEvents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList ret = getWorkflowEventList();

        for (org.osid.workflow.WorkflowEventLookupSession session : getSessions()) {
            ret.addWorkflowEventList(session.getWorkflowEvents());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.FederatingWorkflowEventList getWorkflowEventList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.ParallelWorkflowEventList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.workflowevent.CompositeWorkflowEventList());
        }
    }
}
