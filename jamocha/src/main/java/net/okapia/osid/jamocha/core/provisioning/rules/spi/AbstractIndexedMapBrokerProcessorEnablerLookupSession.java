//
// AbstractIndexedMapBrokerProcessorEnablerLookupSession.java
//
//    A simple framework for providing a BrokerProcessorEnabler lookup service
//    backed by a fixed collection of broker processor enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a BrokerProcessorEnabler lookup service backed by a
 *  fixed collection of broker processor enablers. The broker processor enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some broker processor enablers may be compatible
 *  with more types than are indicated through these broker processor enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BrokerProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBrokerProcessorEnablerLookupSession
    extends AbstractMapBrokerProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.BrokerProcessorEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.BrokerProcessorEnabler> brokerProcessorEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.BrokerProcessorEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.BrokerProcessorEnabler> brokerProcessorEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.BrokerProcessorEnabler>());


    /**
     *  Makes a <code>BrokerProcessorEnabler</code> available in this session.
     *
     *  @param  brokerProcessorEnabler a broker processor enabler
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBrokerProcessorEnabler(org.osid.provisioning.rules.BrokerProcessorEnabler brokerProcessorEnabler) {
        super.putBrokerProcessorEnabler(brokerProcessorEnabler);

        this.brokerProcessorEnablersByGenus.put(brokerProcessorEnabler.getGenusType(), brokerProcessorEnabler);
        
        try (org.osid.type.TypeList types = brokerProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokerProcessorEnablersByRecord.put(types.getNextType(), brokerProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a broker processor enabler from this session.
     *
     *  @param brokerProcessorEnablerId the <code>Id</code> of the broker processor enabler
     *  @throws org.osid.NullArgumentException <code>brokerProcessorEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBrokerProcessorEnabler(org.osid.id.Id brokerProcessorEnablerId) {
        org.osid.provisioning.rules.BrokerProcessorEnabler brokerProcessorEnabler;
        try {
            brokerProcessorEnabler = getBrokerProcessorEnabler(brokerProcessorEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.brokerProcessorEnablersByGenus.remove(brokerProcessorEnabler.getGenusType());

        try (org.osid.type.TypeList types = brokerProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokerProcessorEnablersByRecord.remove(types.getNextType(), brokerProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBrokerProcessorEnabler(brokerProcessorEnablerId);
        return;
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> corresponding to the given
     *  broker processor enabler genus <code>Type</code> which does not include
     *  broker processor enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known broker processor enablers or an error results. Otherwise,
     *  the returned list may contain only those broker processor enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  brokerProcessorEnablerGenusType a broker processor enabler genus type 
     *  @return the returned <code>BrokerProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByGenusType(org.osid.type.Type brokerProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerprocessorenabler.ArrayBrokerProcessorEnablerList(this.brokerProcessorEnablersByGenus.get(brokerProcessorEnablerGenusType)));
    }


    /**
     *  Gets a <code>BrokerProcessorEnablerList</code> containing the given
     *  broker processor enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known broker processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  broker processor enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  brokerProcessorEnablerRecordType a broker processor enabler record type 
     *  @return the returned <code>brokerProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorEnablerList getBrokerProcessorEnablersByRecordType(org.osid.type.Type brokerProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerprocessorenabler.ArrayBrokerProcessorEnablerList(this.brokerProcessorEnablersByRecord.get(brokerProcessorEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.brokerProcessorEnablersByGenus.clear();
        this.brokerProcessorEnablersByRecord.clear();

        super.close();

        return;
    }
}
