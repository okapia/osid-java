//
// AbstractAdapterProficiencyLookupSession.java
//
//    A Proficiency lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.learning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Proficiency lookup session adapter.
 */

public abstract class AbstractAdapterProficiencyLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.learning.ProficiencyLookupSession {

    private final org.osid.learning.ProficiencyLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProficiencyLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProficiencyLookupSession(org.osid.learning.ProficiencyLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code ObjectiveBank/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code ObjectiveBank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.session.getObjectiveBankId());
    }


    /**
     *  Gets the {@code ObjectiveBank} associated with this session.
     *
     *  @return the {@code ObjectiveBank} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getObjectiveBank());
    }


    /**
     *  Tests if this user can perform {@code Proficiency} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProficiencies() {
        return (this.session.canLookupProficiencies());
    }


    /**
     *  A complete view of the {@code Proficiency} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProficiencyView() {
        this.session.useComparativeProficiencyView();
        return;
    }


    /**
     *  A complete view of the {@code Proficiency} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProficiencyView() {
        this.session.usePlenaryProficiencyView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include proficiencies in objective banks which are children
     *  of this objective bank in the objective bank hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.session.useFederatedObjectiveBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.session.useIsolatedObjectiveBankView();
        return;
    }
    

    /**
     *  Only proficiencies whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProficiencyView() {
        this.session.useEffectiveProficiencyView();
        return;
    }
    

    /**
     *  All proficiencies of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProficiencyView() {
        this.session.useAnyEffectiveProficiencyView();
        return;
    }

     
    /**
     *  Gets the {@code Proficiency} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Proficiency} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Proficiency} and
     *  retained for compatibility.
     *
     *  In effective mode, proficiencies are returned that are currently
     *  effective.  In any effective mode, effective proficiencies and
     *  those currently expired are returned.
     *
     *  @param proficiencyId {@code Id} of the {@code Proficiency}
     *  @return the proficiency
     *  @throws org.osid.NotFoundException {@code proficiencyId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code proficiencyId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.Proficiency getProficiency(org.osid.id.Id proficiencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficiency(proficiencyId));
    }


    /**
     *  Gets a {@code ProficiencyList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  proficiencies specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Proficiencies} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, proficiencies are returned that are currently
     *  effective.  In any effective mode, effective proficiencies and
     *  those currently expired are returned.
     *
     *  @param  proficiencyIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Proficiency} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code proficiencyIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByIds(org.osid.id.IdList proficiencyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesByIds(proficiencyIds));
    }


    /**
     *  Gets a {@code ProficiencyList} corresponding to the given
     *  proficiency genus {@code Type} which does not include
     *  proficiencies of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, proficiencies are returned that are currently
     *  effective.  In any effective mode, effective proficiencies and
     *  those currently expired are returned.
     *
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned {@code Proficiency} list
     *  @throws org.osid.NullArgumentException
     *          {@code proficiencyGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusType(org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesByGenusType(proficiencyGenusType));
    }


    /**
     *  Gets a {@code ProficiencyList} corresponding to the given
     *  proficiency genus {@code Type} and include any additional
     *  proficiencies with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, proficiencies are returned that are currently
     *  effective.  In any effective mode, effective proficiencies and
     *  those currently expired are returned.
     *
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned {@code Proficiency} list
     *  @throws org.osid.NullArgumentException
     *          {@code proficiencyGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByParentGenusType(org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesByParentGenusType(proficiencyGenusType));
    }


    /**
     *  Gets a {@code ProficiencyList} containing the given
     *  proficiency record {@code Type}.
     * 
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  proficiencyRecordType a proficiency record type 
     *  @return the returned {@code Proficiency} list
     *  @throws org.osid.NullArgumentException
     *          {@code proficiencyRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByRecordType(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesByRecordType(proficiencyRecordType));
    }


    /**
     *  Gets a {@code ProficiencyList} effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In active mode, proficiencies are returned that are currently
     *  active. In any status mode, active and inactive proficiencies
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesOnDate(org.osid.calendaring.DateTime from, 
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesOnDate(from, to));
    }


    /**
     *  Gets a {@code ProficiencyList} by genus type and effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In active mode, proficiencies are returned that are currently
     *  active. In any status mode, active and inactive proficiencies
     *  are returned.
     *
     *  @Param proficienyGenusType a proficiency genus type
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code
     *          proficienyGenusType}, {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeOnDate(org.osid.type.Type proficienyGenusType,
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesByGenusTypeOnDate(proficienyGenusType, from, to));
    }
        

    /**
     *  Gets a list of proficiencies corresponding to an objective
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId the {@code Id} of the objective
     *  @return the returned {@code ProficiencyList}
     *  @throws org.osid.NullArgumentException {@code objectiveId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForObjective(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesForObjective(objectiveId));
    }


    /**
     *  Gets a list of proficiencies corresponding to an objective
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId the {@code Id} of the objective
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProficiencyList}
     *  @throws org.osid.NullArgumentException {@code objectiveId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForObjectiveOnDate(org.osid.id.Id objectiveId,
                                                                                org.osid.calendaring.DateTime from,
                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesForObjectiveOnDate(objectiveId, from, to));
    }


    /**
     *  Gets a {@code ProficiencyList} relating to the given objective
     *  and proficiency genus {@code Type.}
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId an objective {@code Id} 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.NullArgumentException {@code objectiveId} or 
     *          {@code proficiencyGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForObjective(org.osid.id.Id objectiveId, 
                                                                                     org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getProficienciesByGenusTypeForObjective(objectiveId, proficiencyGenusType));
    }


    /**
     *  Gets a {@code ProficiencyList} of the given proficiency genus
     *  type relating to the given objective effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId an objective {@code Id} 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code objectiveId, 
     *          proficiencyGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForObjectiveOnDate(org.osid.id.Id objectiveId, 
                                                                                           org.osid.type.Type proficiencyGenusType, 
                                                                                           org.osid.calendaring.DateTime from, 
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getProficienciesByGenusTypeForObjectiveOnDate(objectiveId, proficiencyGenusType, from, to));
    }


    /**
     *  Gets a {@code ProficiencyList} relating to the given 
     *  objectives. 
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveIds the objective {@code Ids} 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.NullArgumentException {@code objectiveIds} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForObjectives(org.osid.id.IdList objectiveIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesForObjectives(objectiveIds));
    }


    /**
     *  Gets a list of proficiencies corresponding to a resource
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code ProficiencyList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesForResource(resourceId));
    }


    /**
     *  Gets a list of proficiencies corresponding to a resource
     *  {@code Id} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProficiencyList}
     *  @throws org.osid.NullArgumentException {@code resourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForResourceOnDate(org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesForResourceOnDate(resourceId, from, to));
    }


    /**
     *  Gets a {@code ProficiencyList} relating to the given resource
     *  and proficiency genus {@code Type}.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.NullArgumentException {@code resourceId} or 
     *          {@code proficiencyGenusType} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForResource(org.osid.id.Id resourceId, 
                                                                                    org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesByGenusTypeForResource(resourceId, proficiencyGenusType));
    }


    /**
     *  Gets a {@code ProficiencyList} of the given proficiency genus
     *  type relating to the given resource effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code resourceId, 
     *          proficiencyGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForResourceOnDate(org.osid.id.Id resourceId, 
                                                                                          org.osid.type.Type proficiencyGenusType, 
                                                                                          org.osid.calendaring.DateTime from, 
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getProficienciesByGenusTypeForResourceOnDate(resourceId, proficiencyGenusType, from, to));
    }


    /**
     *  Gets a {@code ProficiencyList} relating to the given resources. 
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceIds the resource {@code Ids} 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.NullArgumentException {@code resourceIds} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForResources(org.osid.id.IdList resourceIds)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getProficienciesForResources(resourceIds));
    }

    
    /**
     *  Gets a list of proficiencies corresponding to objective and
     *  resource {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective.  In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId the {@code Id} of the objective
     *  @param  resourceId the {@code Id} of the resource
     *  @return the returned {@code ProficiencyList}
     *  @throws org.osid.NullArgumentException {@code objectiveId},
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForObjectiveAndResource(org.osid.id.Id objectiveId,
                                                                                     org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesForObjectiveAndResource(objectiveId, resourceId));
    }


    /**
     *  Gets a list of proficiencies corresponding to objective and
     *  resource {@code Ids} and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProficiencyList}
     *  @throws org.osid.NullArgumentException {@code objectiveId},
     *          {@code resourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesForObjectiveAndResourceOnDate(org.osid.id.Id objectiveId,
                                                                                           org.osid.id.Id resourceId,
                                                                                           org.osid.calendaring.DateTime from,
                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficienciesForObjectiveAndResourceOnDate(objectiveId, resourceId, from, to));
    }


    /**
     *  Gets a {@code ProficiencyList} of the given genus type
     *  relating to the given objective and resource {@code}.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId an objective {@code Id} 
     *  @param  resourceId a resource {@code Id} 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.NullArgumentException {@code objectiveId,
     *          resourceId} or {@code proficiencyGenusType} is {@code
     *          null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForObjectiveAndResource(org.osid.id.Id objectiveId, 
                                                                                                org.osid.id.Id resourceId, 
                                                                                                org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getProficienciesByGenusTypeForObjectiveAndResource(objectiveId, resourceId, proficiencyGenusType));
    }


    /**
     *  Gets a {@code ProficiencyList} of the given genus type
     *  relating to the given resource and objective effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned
     *  list may contain only those proficiencies that are accessible
     *  through this session.
     *  
     *  In effective mode, proficiencies are returned that are
     *  currently effective. In any effective mode, effective
     *  proficiencies and those currently expired are returned.
     *
     *  @param  objectiveId an objective {@code Id} 
     *  @param  resourceId a resource {@code Id} 
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @param  from starting date 
     *  @param  to ending date 
     *  @return the returned {@code Proficiency} list 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code objectiveId, resourceId, 
     *          proficiencyGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusTypeForObjectiveAndResourceOnDate(org.osid.id.Id objectiveId, 
                                                                                                      org.osid.id.Id resourceId, 
                                                                                                      org.osid.type.Type proficiencyGenusType, 
                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.getProficienciesByGenusTypeForObjectiveAndResourceOnDate(objectiveId, resourceId, proficiencyGenusType, from, to));
    }


    /**
     *  Gets all {@code Proficiencies}. 
     *
     *  In plenary mode, the returned list contains all known
     *  proficiencies or an error results. Otherwise, the returned list
     *  may contain only those proficiencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, proficiencies are returned that are currently
     *  effective.  In any effective mode, effective proficiencies and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Proficiencies} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficiencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProficiencies());
    }
}
