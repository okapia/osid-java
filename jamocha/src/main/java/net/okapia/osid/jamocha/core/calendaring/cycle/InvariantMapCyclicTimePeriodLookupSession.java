//
// InvariantMapCyclicTimePeriodLookupSession
//
//    Implements a CyclicTimePeriod lookup service backed by a fixed collection of
//    cyclicTimePeriods.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.cycle;


/**
 *  Implements a CyclicTimePeriod lookup service backed by a fixed
 *  collection of cyclic time periods. The cyclic time periods are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapCyclicTimePeriodLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.cycle.spi.AbstractMapCyclicTimePeriodLookupSession
    implements org.osid.calendaring.cycle.CyclicTimePeriodLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapCyclicTimePeriodLookupSession</code> with no
     *  cyclic time periods.
     *  
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumnetException {@code calendar} is
     *          {@code null}
     */

    public InvariantMapCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCyclicTimePeriodLookupSession</code> with a single
     *  cyclic time period.
     *  
     *  @param calendar the calendar
     *  @param cyclicTimePeriod a single cyclic time period
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code cyclicTimePeriod} is <code>null</code>
     */

      public InvariantMapCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod) {
        this(calendar);
        putCyclicTimePeriod(cyclicTimePeriod);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCyclicTimePeriodLookupSession</code> using an array
     *  of cyclic time periods.
     *  
     *  @param calendar the calendar
     *  @param cyclicTimePeriods an array of cyclic time periods
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code cyclicTimePeriods} is <code>null</code>
     */

      public InvariantMapCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.cycle.CyclicTimePeriod[] cyclicTimePeriods) {
        this(calendar);
        putCyclicTimePeriods(cyclicTimePeriods);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapCyclicTimePeriodLookupSession</code> using a
     *  collection of cyclic time periods.
     *
     *  @param calendar the calendar
     *  @param cyclicTimePeriods a collection of cyclic time periods
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code cyclicTimePeriods} is <code>null</code>
     */

      public InvariantMapCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                               java.util.Collection<? extends org.osid.calendaring.cycle.CyclicTimePeriod> cyclicTimePeriods) {
        this(calendar);
        putCyclicTimePeriods(cyclicTimePeriods);
        return;
    }
}
