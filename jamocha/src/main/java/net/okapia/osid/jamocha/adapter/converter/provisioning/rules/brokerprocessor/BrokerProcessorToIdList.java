//
// BrokerProcessorToIdList.java
//
//     Implements a BrokerProcessor IdList. 
//
//
// Tom Coppeto
// Okapia
// 15 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.converter.provisioning.rules.brokerprocessor;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  Implements an IdList converting the underlying BrokerProcessorList to a
 *  list of Ids.
 */

public final class BrokerProcessorToIdList
    extends net.okapia.osid.jamocha.adapter.id.id.spi.AbstractAdapterIdList
    implements org.osid.id.IdList {

    private final org.osid.provisioning.rules.BrokerProcessorList brokerProcessorList;


    /**
     *  Creates a new {@code BrokerProcessorToIdList}.
     *
     *  @param brokerProcessorList a {@code BrokerProcessorList}
     *  @throws org.osid.NullArgumentException {@code brokerProcessorList}
     *          is {@code null}
     */

    public BrokerProcessorToIdList(org.osid.provisioning.rules.BrokerProcessorList brokerProcessorList) {
        super(brokerProcessorList);
        this.brokerProcessorList = brokerProcessorList;
        return;
    }


    /**
     *  Creates a new {@code BrokerProcessorToIdList}.
     *
     *  @param brokerProcessors a collection of BrokerProcessors
     *  @throws org.osid.NullArgumentException {@code brokerProcessors}
     *          is {@code null}
     */

    public BrokerProcessorToIdList(java.util.Collection<org.osid.provisioning.rules.BrokerProcessor> brokerProcessors) {
        this(new net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.ArrayBrokerProcessorList(brokerProcessors)); 
        return;
    }


    /**
     *  Gets the next {@code Id} in this list. 
     *
     *  @return the next {@code Id} in this list. The {@code
     *          hasNext()} method should be used to test that a next
     *          {@code Id} is available before calling this method.
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.Id getNextId()
        throws org.osid.OperationFailedException {

        return (this.brokerProcessorList.getNextBrokerProcessor().getId());
    }


    /**
     *  Closes this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.brokerProcessorList.close();
        return;
    }
}
