//
// AbstractQueueQuery.java
//
//     A template for making a Queue Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.queue.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for queues.
 */

public abstract class AbstractQueueQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.provisioning.QueueQuery {

    private final java.util.Collection<org.osid.provisioning.records.QueueQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the broker <code> Id </code> for this query. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrokerId(org.osid.id.Id brokerId, boolean match) {
        return;
    }


    /**
     *  Clears the broker <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearBrokerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BrokerQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the broker query 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuery getBrokerQuery() {
        throw new org.osid.UnimplementedException("supportsBrokerQuery() is false");
    }


    /**
     *  Clears the broker query terms. 
     */

    @OSID @Override
    public void clearBrokerTerms() {
        return;
    }


    /**
     *  Matches queues of the given size inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     */

    @OSID @Override
    public void matchSize(long start, long end, boolean match) {
        return;
    }


    /**
     *  Matches queues with any known size. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnySize(boolean match) {
        return;
    }


    /**
     *  Clears the size query terms. 
     */

    @OSID @Override
    public void clearSizeTerms() {
        return;
    }


    /**
     *  Matches queues whose estimated waiting time is in the given range 
     *  inclusive,. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEWA(org.osid.calendaring.Duration start, 
                         org.osid.calendaring.Duration end, boolean match) {
        return;
    }


    /**
     *  Matches queues with any estimated wiating time. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAnyEWA(boolean match) {
        return;
    }


    /**
     *  Clears the estimated waiting time query terms. 
     */

    @OSID @Override
    public void clearEWATerms() {
        return;
    }


    /**
     *  Matches queues that permit requests for specific provisionables. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchCanSpecifyProvisionable(boolean match) {
        return;
    }


    /**
     *  Clears the can request provisionables terms. 
     */

    @OSID @Override
    public void clearCanSpecifyProvisionableTerms() {
        return;
    }


    /**
     *  Sets the request <code> Id </code> for this query. 
     *
     *  @param  requestId the request <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> requestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRequestId(org.osid.id.Id requestId, boolean match) {
        return;
    }


    /**
     *  Clears the request <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRequestIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RequestQuery </code> is available. 
     *
     *  @return <code> true </code> if a request query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestQuery() {
        return (false);
    }


    /**
     *  Gets the query for a <code> Request. </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the request query 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuery getRequestQuery() {
        throw new org.osid.UnimplementedException("supportsRequestQuery() is false");
    }


    /**
     *  Matches queues that have any request. 
     *
     *  @param  match <code> true </code> to match queues with any request, 
     *          <code> false </code> to match queues with no request 
     */

    @OSID @Override
    public void matchAnyRequest(boolean match) {
        return;
    }


    /**
     *  Clears the request query terms. 
     */

    @OSID @Override
    public void clearRequestTerms() {
        return;
    }


    /**
     *  Sets the broker <code> Id </code> for this query to match queues 
     *  assigned to brokers. 
     *
     *  @param  brokerId the broker <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id brokerId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given queue query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a queue implementing the requested record.
     *
     *  @param queueRecordType a queue record type
     *  @return the queue query record
     *  @throws org.osid.NullArgumentException
     *          <code>queueRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.QueueQueryRecord getQueueQueryRecord(org.osid.type.Type queueRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.QueueQueryRecord record : this.records) {
            if (record.implementsRecordType(queueRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue query. 
     *
     *  @param queueQueryRecord queue query record
     *  @param queueRecordType queue record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueQueryRecord(org.osid.provisioning.records.QueueQueryRecord queueQueryRecord, 
                                          org.osid.type.Type queueRecordType) {

        addRecordType(queueRecordType);
        nullarg(queueQueryRecord, "queue query record");
        this.records.add(queueQueryRecord);        
        return;
    }
}
