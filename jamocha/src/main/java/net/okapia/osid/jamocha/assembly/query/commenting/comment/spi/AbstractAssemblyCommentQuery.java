//
// AbstractAssemblyCommentQuery.java
//
//     A CommentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.commenting.comment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CommentQuery that stores terms.
 */

public abstract class AbstractAssemblyCommentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRelationshipQuery
    implements org.osid.commenting.CommentQuery,
               org.osid.commenting.CommentQueryInspector,
               org.osid.commenting.CommentSearchOrder {

    private final java.util.Collection<org.osid.commenting.records.CommentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.commenting.records.CommentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.commenting.records.CommentSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCommentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCommentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets reference <code> Id. </code> 
     *
     *  @param  sourceId a source <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchReferenceId(org.osid.id.Id sourceId, boolean match) {
        getAssembler().addIdTerm(getReferenceIdColumn(), sourceId, match);
        return;
    }


    /**
     *  Clears the reference <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearReferenceIdTerms() {
        getAssembler().clearTerms(getReferenceIdColumn());
        return;
    }


    /**
     *  Gets the reference <code> Id </code> terms. 
     *
     *  @return the reference <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getReferenceIdTerms() {
        return (getAssembler().getIdTerms(getReferenceIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the reference. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByReference(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getReferenceColumn(), style);
        return;
    }


    /**
     *  Gets the Reference Id column name.
     *
     * @return the column name
     */

    protected String getReferenceIdColumn() {
        return ("reference_id");
    }


    /**
     *  Gets the Reference column name.
     *
     * @return the column name
     */

    protected String getReferenceColumn() {
        return ("reference");
    }


    /**
     *  Sets a resource <code> Id </code> to match a commentor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommentorId(org.osid.id.Id resourceId, boolean match) {
        getAssembler().addIdTerm(getCommentorIdColumn(), resourceId, match);
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommentorIdTerms() {
        getAssembler().clearTerms(getCommentorIdColumn());
        return;
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommentorIdTerms() {
        return (getAssembler().getIdTerms(getCommentorIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCommentor(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCommentorColumn(), style);
        return;
    }


    /**
     *  Gets the CommentorId column name.
     *
     * @return the column name
     */

    protected String getCommentorIdColumn() {
        return ("commentor_id");
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCommentorQuery() {
        throw new org.osid.UnimplementedException("supportsCommentorQuery() is false");
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearCommentorTerms() {
        getAssembler().clearTerms(getCommentorColumn());
        return;
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getCommentorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Tests if a resource order interface is available. 
     *
     *  @return <code> true </code> if a resource order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentorSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order interface. 
     *
     *  @return the resource search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentorSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getCommentorSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCommentorSearchOrder() is false");
    }


    /**
     *  Gets the Commentor column name.
     *
     * @return the column name
     */

    protected String getCommentorColumn() {
        return ("commentor");
    }


    /**
     *  Sets an agent <code> Id. </code> 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCommentingAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getCommentingAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCommentingAgentIdTerms() {
        getAssembler().clearTerms(getCommentingAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCommentingAgentIdTerms() {
        return (getAssembler().getIdTerms(getCommentingAgentIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCommentingAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCommentingAgentColumn(), style);
        return;
    }


    /**
     *  Gets the CommentingAgentId column name.
     *
     * @return the column name
     */

    protected String getCommentingAgentIdColumn() {
        return ("commenting_agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getCommentingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsCommentingAgentQuery() is false");
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearCommentingAgentTerms() {
        getAssembler().clearTerms(getCommentingAgentColumn());
        return;
    }


    /**
     *  Gets the agent terms. 
     *
     *  @return the agent terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getCommentingAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent order interface is available. 
     *
     *  @return <code> true </code> if an agent order interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent order interface. 
     *
     *  @return the agent search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingAgentSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getCommentingAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCommentingAgentSearchOrder() is false");
    }


    /**
     *  Gets the CommentingAgent column name.
     *
     * @return the column name
     */

    protected String getCommentingAgentColumn() {
        return ("commenting_agent");
    }


    /**
     *  Matches text. 
     *
     *  @param  text the text 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> text </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> text </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchText(String text, org.osid.type.Type stringMatchType, 
                          boolean match) {
        getAssembler().addStringTerm(getTextColumn(), text, stringMatchType, match);
        return;
    }


    /**
     *  Matches a comment that has any text assigned. 
     *
     *  @param  match <code> true </code> to match comments with any text, 
     *          <code> false </code> to match comments with no text 
     */

    @OSID @Override
    public void matchAnyText(boolean match) {
        getAssembler().addStringWildcardTerm(getTextColumn(), match);
        return;
    }


    /**
     *  Clears the text terms. 
     */

    @OSID @Override
    public void clearTextTerms() {
        getAssembler().clearTerms(getTextColumn());
        return;
    }


    /**
     *  Gets the text query terms. 
     *
     *  @return the text query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTextTerms() {
        return (getAssembler().getStringTerms(getTextColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the text. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByText(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getTextColumn(), style);
        return;
    }


    /**
     *  Gets the Text column name.
     *
     * @return the column name
     */

    protected String getTextColumn() {
        return ("text");
    }


    /**
     *  Sets a grade <code> Id. </code> 
     *
     *  @param  gradeId a grade <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRatingId(org.osid.id.Id gradeId, boolean match) {
        getAssembler().addIdTerm(getRatingIdColumn(), gradeId, match);
        return;
    }


    /**
     *  Clears the rating <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRatingIdTerms() {
        getAssembler().clearTerms(getRatingIdColumn());
        return;
    }


    /**
     *  Gets the rating <code> Id </code> terms. 
     *
     *  @return the rating <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRatingIdTerms() {
        return (getAssembler().getIdTerms(getRatingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the rating. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRating(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRatingColumn(), style);
        return;
    }


    /**
     *  Gets the RatingId column name.
     *
     * @return the column name
     */

    protected String getRatingIdColumn() {
        return ("rating_id");
    }


    /**
     *  Tests if a <code> GradeQuery </code> is available. 
     *
     *  @return <code> true </code> if a rating query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRatingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a rating query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the rating query 
     *  @throws org.osid.UnimplementedException <code> supportsRatingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeQuery getRatingQuery() {
        throw new org.osid.UnimplementedException("supportsRatingQuery() is false");
    }


    /**
     *  Matches books with any rating. 
     *
     *  @param  match <code> true </code> to match comments with any rating, 
     *          <code> false </code> to match comments with no ratings 
     */

    @OSID @Override
    public void matchAnyRating(boolean match) {
        getAssembler().addIdWildcardTerm(getRatingColumn(), match);
        return;
    }


    /**
     *  Clears the rating terms. 
     */

    @OSID @Override
    public void clearRatingTerms() {
        getAssembler().clearTerms(getRatingColumn());
        return;
    }


    /**
     *  Gets the rating terms. 
     *
     *  @return the rating terms 
     */

    @OSID @Override
    public org.osid.grading.GradeQueryInspector[] getRatingTerms() {
        return (new org.osid.grading.GradeQueryInspector[0]);
    }


    /**
     *  Tests if a rating order interface is available. 
     *
     *  @return <code> true </code> if a rating order interface is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRatingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the rating order interface. 
     *
     *  @return the rating search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRatingSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSearchOrder getRatingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRatingSearchOrder() is false");
    }


    /**
     *  Gets the Rating column name.
     *
     * @return the column name
     */

    protected String getRatingColumn() {
        return ("rating");
    }


    /**
     *  Sets the book <code> Id </code> for this query to match comments 
     *  assigned to books. 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBookId(org.osid.id.Id bookId, boolean match) {
        getAssembler().addIdTerm(getBookIdColumn(), bookId, match);
        return;
    }


    /**
     *  Clears the book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBookIdTerms() {
        getAssembler().clearTerms(getBookIdColumn());
        return;
    }


    /**
     *  Gets the book <code> Id </code> terms. 
     *
     *  @return the book <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBookIdTerms() {
        return (getAssembler().getIdTerms(getBookIdColumn()));
    }


    /**
     *  Gets the BookId column name.
     *
     * @return the column name
     */

    protected String getBookIdColumn() {
        return ("book_id");
    }


    /**
     *  Tests if a <code> BookQuery </code> is available. 
     *
     *  @return <code> true </code> if a book query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a book query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the book query 
     *  @throws org.osid.UnimplementedException <code> supportsBookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookQuery getBookQuery() {
        throw new org.osid.UnimplementedException("supportsBookQuery() is false");
    }


    /**
     *  Clears the book terms. 
     */

    @OSID @Override
    public void clearBookTerms() {
        getAssembler().clearTerms(getBookColumn());
        return;
    }


    /**
     *  Gets the book terms. 
     *
     *  @return the book terms 
     */

    @OSID @Override
    public org.osid.commenting.BookQueryInspector[] getBookTerms() {
        return (new org.osid.commenting.BookQueryInspector[0]);
    }


    /**
     *  Gets the Book column name.
     *
     * @return the column name
     */

    protected String getBookColumn() {
        return ("book");
    }


    /**
     *  Tests if this comment supports the given record
     *  <code>Type</code>.
     *
     *  @param  commentRecordType a comment record type 
     *  @return <code>true</code> if the commentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type commentRecordType) {
        for (org.osid.commenting.records.CommentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  commentRecordType the comment record type 
     *  @return the comment query record 
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.CommentQueryRecord getCommentQueryRecord(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.CommentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(commentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  commentRecordType the comment record type 
     *  @return the comment query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.CommentQueryInspectorRecord getCommentQueryInspectorRecord(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.CommentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(commentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commentRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param commentRecordType the comment record type
     *  @return the comment search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.CommentSearchOrderRecord getCommentSearchOrderRecord(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.CommentSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(commentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this comment. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param commentQueryRecord the comment query record
     *  @param commentQueryInspectorRecord the comment query inspector
     *         record
     *  @param commentSearchOrderRecord the comment search order record
     *  @param commentRecordType comment record type
     *  @throws org.osid.NullArgumentException
     *          <code>commentQueryRecord</code>,
     *          <code>commentQueryInspectorRecord</code>,
     *          <code>commentSearchOrderRecord</code> or
     *          <code>commentRecordTypecomment</code> is
     *          <code>null</code>
     */
            
    protected void addCommentRecords(org.osid.commenting.records.CommentQueryRecord commentQueryRecord, 
                                      org.osid.commenting.records.CommentQueryInspectorRecord commentQueryInspectorRecord, 
                                      org.osid.commenting.records.CommentSearchOrderRecord commentSearchOrderRecord, 
                                      org.osid.type.Type commentRecordType) {

        addRecordType(commentRecordType);

        nullarg(commentQueryRecord, "comment query record");
        nullarg(commentQueryInspectorRecord, "comment query inspector record");
        nullarg(commentSearchOrderRecord, "comment search odrer record");

        this.queryRecords.add(commentQueryRecord);
        this.queryInspectorRecords.add(commentQueryInspectorRecord);
        this.searchOrderRecords.add(commentSearchOrderRecord);
        
        return;
    }
}
