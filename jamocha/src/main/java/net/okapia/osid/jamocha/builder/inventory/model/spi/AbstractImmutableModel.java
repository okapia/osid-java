//
// AbstractImmutableModel.java
//
//     Wraps a mutable Model to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inventory.model.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Model</code> to hide modifiers. This
 *  wrapper provides an immutized Model from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying model whose state changes are visible.
 */

public abstract class AbstractImmutableModel
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.inventory.Model {

    private final org.osid.inventory.Model model;


    /**
     *  Constructs a new <code>AbstractImmutableModel</code>.
     *
     *  @param model the model to immutablize
     *  @throws org.osid.NullArgumentException <code>model</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableModel(org.osid.inventory.Model model) {
        super(model);
        this.model = model;
        return;
    }


    /**
     *  Gets the manufacturer <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getManufacturerId() {
        return (this.model.getManufacturerId());
    }


    /**
     *  Gets the manufacturer (e.g. Cisco). 
     *
     *  @return a resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getManufacturer()
        throws org.osid.OperationFailedException {

        return (this.model.getManufacturer());
    }


    /**
     *  Gets the model name component (e.g. ASR 9010) 
     *
     *  @return the model 
     */

    @OSID @Override
    public String getArchetype() {
        return (this.model.getArchetype());
    }


    /**
     *  Gets the model number (e.g. ASR-9010-AC-V2). 
     *
     *  @return the model number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.model.getNumber());
    }


    /**
     *  Gets the model record corresponding to the given <code> Model </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> modelRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(modelRecordType) </code> is <code> true </code> . 
     *
     *  @param  modelRecordType the type of model record to retrieve 
     *  @return the model record 
     *  @throws org.osid.NullArgumentException <code> modelRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(modelRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.records.ModelRecord getModelRecord(org.osid.type.Type modelRecordType)
        throws org.osid.OperationFailedException {

        return (this.model.getModelRecord(modelRecordType));
    }
}

