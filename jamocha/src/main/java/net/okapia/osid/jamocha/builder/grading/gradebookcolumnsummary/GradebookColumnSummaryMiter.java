//
// GradebookColumnSummaryMiter.java
//
//     Defines a GradebookColumnSummary miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary;


/**
 *  Defines a <code>GradebookColumnSummary</code> miter for use with the builders.
 */

public interface GradebookColumnSummaryMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.grading.GradebookColumnSummary {


    /**
     *  Sets the gradebook column.
     *
     *  @param gradebookColumn a gradebook column
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumn</code> is <code>null</code>
     */

    public void setGradebookColumn(org.osid.grading.GradebookColumn gradebookColumn);


    /**
     *  Sets the mean.
     *
     *  @param mean a mean
     *  @throws org.osid.NullArgumentException
     *          <code>mean</code> is <code>null</code>
     */

    public void setMean(java.math.BigDecimal mean);


    /**
     *  Sets the median.
     *
     *  @param median a median
     *  @throws org.osid.NullArgumentException
     *          <code>median</code> is <code>null</code>
     */

    public void setMedian(java.math.BigDecimal median);


    /**
     *  Sets the mode.
     *
     *  @param mode a mode
     *  @throws org.osid.NullArgumentException
     *          <code>mode</code> is <code>null</code>
     */

    public void setMode(java.math.BigDecimal mode);


    /**
     *  Sets the r ms.
     *
     *  @param rM a r ms
     *  @throws org.osid.NullArgumentException
     *          <code>rM</code> is <code>null</code>
     */

    public void setRMS(java.math.BigDecimal rM);


    /**
     *  Sets the standard deviation.
     *
     *  @param standardDeviation a standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    public void setStandardDeviation(java.math.BigDecimal standardDeviation);


    /**
     *  Sets the sum.
     *
     *  @param sum a sum
     *  @throws org.osid.NullArgumentException
     *          <code>sum</code> is <code>null</code>
     */

    public void setSum(java.math.BigDecimal sum);


    /**
     *  Adds a GradebookColumnSummary record.
     *
     *  @param record a gradebookColumnSummary record
     *  @param recordType the type of gradebookColumnSummary record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addGradebookColumnSummaryRecord(org.osid.grading.records.GradebookColumnSummaryRecord record, org.osid.type.Type recordType);
}       


