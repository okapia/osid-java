//
// AbstractMapAssessmentOfferedLookupSession
//
//    A simple framework for providing an AssessmentOffered lookup service
//    backed by a fixed collection of assessments offered.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AssessmentOffered lookup service backed by a
 *  fixed collection of assessments offered. The assessments offered are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AssessmentsOffered</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAssessmentOfferedLookupSession
    extends net.okapia.osid.jamocha.assessment.spi.AbstractAssessmentOfferedLookupSession
    implements org.osid.assessment.AssessmentOfferedLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.assessment.AssessmentOffered> assessmentsOffered = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.assessment.AssessmentOffered>());


    /**
     *  Makes an <code>AssessmentOffered</code> available in this session.
     *
     *  @param  assessmentOffered an assessment offered
     *  @throws org.osid.NullArgumentException <code>assessmentOffered<code>
     *          is <code>null</code>
     */

    protected void putAssessmentOffered(org.osid.assessment.AssessmentOffered assessmentOffered) {
        this.assessmentsOffered.put(assessmentOffered.getId(), assessmentOffered);
        return;
    }


    /**
     *  Makes an array of assessments offered available in this session.
     *
     *  @param  assessmentsOffered an array of assessments offered
     *  @throws org.osid.NullArgumentException <code>assessmentsOffered<code>
     *          is <code>null</code>
     */

    protected void putAssessmentsOffered(org.osid.assessment.AssessmentOffered[] assessmentsOffered) {
        putAssessmentsOffered(java.util.Arrays.asList(assessmentsOffered));
        return;
    }


    /**
     *  Makes a collection of assessments offered available in this session.
     *
     *  @param  assessmentsOffered a collection of assessments offered
     *  @throws org.osid.NullArgumentException <code>assessmentsOffered<code>
     *          is <code>null</code>
     */

    protected void putAssessmentsOffered(java.util.Collection<? extends org.osid.assessment.AssessmentOffered> assessmentsOffered) {
        for (org.osid.assessment.AssessmentOffered assessmentOffered : assessmentsOffered) {
            this.assessmentsOffered.put(assessmentOffered.getId(), assessmentOffered);
        }

        return;
    }


    /**
     *  Removes an AssessmentOffered from this session.
     *
     *  @param  assessmentOfferedId the <code>Id</code> of the assessment offered
     *  @throws org.osid.NullArgumentException <code>assessmentOfferedId<code> is
     *          <code>null</code>
     */

    protected void removeAssessmentOffered(org.osid.id.Id assessmentOfferedId) {
        this.assessmentsOffered.remove(assessmentOfferedId);
        return;
    }


    /**
     *  Gets the <code>AssessmentOffered</code> specified by its <code>Id</code>.
     *
     *  @param  assessmentOfferedId <code>Id</code> of the <code>AssessmentOffered</code>
     *  @return the assessmentOffered
     *  @throws org.osid.NotFoundException <code>assessmentOfferedId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>assessmentOfferedId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOffered getAssessmentOffered(org.osid.id.Id assessmentOfferedId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.assessment.AssessmentOffered assessmentOffered = this.assessmentsOffered.get(assessmentOfferedId);
        if (assessmentOffered == null) {
            throw new org.osid.NotFoundException("assessmentOffered not found: " + assessmentOfferedId);
        }

        return (assessmentOffered);
    }


    /**
     *  Gets all <code>AssessmentsOffered</code>. In plenary mode, the returned
     *  list contains all known assessmentsOffered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessmentsOffered that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AssessmentsOffered</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedList getAssessmentsOffered()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.assessmentoffered.ArrayAssessmentOfferedList(this.assessmentsOffered.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessmentsOffered.clear();
        super.close();
        return;
    }
}
