//
// MutableMapProxyGraphLookupSession
//
//    Implements a Graph lookup service backed by a collection of
//    graphs that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology;


/**
 *  Implements a Graph lookup service backed by a collection of
 *  graphs. The graphs are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of graphs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyGraphLookupSession
    extends net.okapia.osid.jamocha.core.topology.spi.AbstractMapGraphLookupSession
    implements org.osid.topology.GraphLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyGraphLookupSession} with no
     *  graphs.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyGraphLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyGraphLookupSession} with a
     *  single graph.
     *
     *  @param graph a graph
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyGraphLookupSession(org.osid.topology.Graph graph, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putGraph(graph);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyGraphLookupSession} using an
     *  array of graphs.
     *
     *  @param graphs an array of graphs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graphs} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyGraphLookupSession(org.osid.topology.Graph[] graphs, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putGraphs(graphs);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyGraphLookupSession} using
     *  a collection of graphs.
     *
     *  @param graphs a collection of graphs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code graphs} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyGraphLookupSession(java.util.Collection<? extends org.osid.topology.Graph> graphs,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putGraphs(graphs);
        return;
    }

    
    /**
     *  Makes a {@code Graph} available in this session.
     *
     *  @param graph an graph
     *  @throws org.osid.NullArgumentException {@code graph{@code 
     *          is {@code null}
     */

    @Override
    public void putGraph(org.osid.topology.Graph graph) {
        super.putGraph(graph);
        return;
    }


    /**
     *  Makes an array of graphs available in this session.
     *
     *  @param graphs an array of graphs
     *  @throws org.osid.NullArgumentException {@code graphs{@code 
     *          is {@code null}
     */

    @Override
    public void putGraphs(org.osid.topology.Graph[] graphs) {
        super.putGraphs(graphs);
        return;
    }


    /**
     *  Makes collection of graphs available in this session.
     *
     *  @param graphs
     *  @throws org.osid.NullArgumentException {@code graph{@code 
     *          is {@code null}
     */

    @Override
    public void putGraphs(java.util.Collection<? extends org.osid.topology.Graph> graphs) {
        super.putGraphs(graphs);
        return;
    }


    /**
     *  Removes a Graph from this session.
     *
     *  @param graphId the {@code Id} of the graph
     *  @throws org.osid.NullArgumentException {@code graphId{@code  is
     *          {@code null}
     */

    @Override
    public void removeGraph(org.osid.id.Id graphId) {
        super.removeGraph(graphId);
        return;
    }    
}
