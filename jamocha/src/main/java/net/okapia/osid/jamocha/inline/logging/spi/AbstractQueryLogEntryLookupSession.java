//
// AbstractQueryLogEntryLookupSession.java
//
//    An inline adapter that maps a LogEntryLookupSession to
//    a LogEntryQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.logging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a LogEntryLookupSession to
 *  a LogEntryQuerySession.
 */

public abstract class AbstractQueryLogEntryLookupSession
    extends net.okapia.osid.jamocha.logging.spi.AbstractLogEntryLookupSession
    implements org.osid.logging.LogEntryLookupSession {

    private final org.osid.logging.LogEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryLogEntryLookupSession.
     *
     *  @param querySession the underlying log entry query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryLogEntryLookupSession(org.osid.logging.LogEntryQuerySession querySession) {
        nullarg(querySession, "log entry query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Log</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Log Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getLogId() {
        return (this.session.getLogId());
    }


    /**
     *  Gets the <code>Log</code> associated with this 
     *  session.
     *
     *  @return the <code>Log</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.Log getLog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getLog());
    }


    /**
     *  Tests if this user can perform <code>LogEntry</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canReadLog() {
        return (this.session.canSearchLogEntries());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include log entries in logs which are children
     *  of this log in the log hierarchy.
     */

    @OSID @Override
    public void useFederatedLogView() {
        this.session.useFederatedLogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this log only.
     */

    @OSID @Override
    public void useIsolatedLogView() {
        this.session.useIsolatedLogView();
        return;
    }
    
     
    /**
     *  Gets the <code>LogEntry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>LogEntry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>LogEntry</code> and
     *  retained for compatibility.
     *
     *  @param  logEntryId <code>Id</code> of the
     *          <code>LogEntry</code>
     *  @return the log entry
     *  @throws org.osid.NotFoundException <code>logEntryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>logEntryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntry getLogEntry(org.osid.id.Id logEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchId(logEntryId, true);
        org.osid.logging.LogEntryList logEntries = this.session.getLogEntriesByQuery(query);
        if (logEntries.hasNext()) {
            return (logEntries.getNextLogEntry());
        } 
        
        throw new org.osid.NotFoundException(logEntryId + " not found");
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  logEntries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>LogEntries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  logEntryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByIds(org.osid.id.IdList logEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();

        try (org.osid.id.IdList ids = logEntryIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getLogEntriesByQuery(query));
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  log entry genus <code>Type</code> which does not include
     *  log entries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryGenusType a logEntry genus type 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchGenusType(logEntryGenusType, true);
        return (this.session.getLogEntriesByQuery(query));
    }


    /**
     *  Gets a <code>LogEntryList</code> corresponding to the given
     *  log entry genus <code>Type</code> and include any additional
     *  log entries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryGenusType a logEntry genus type 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByParentGenusType(org.osid.type.Type logEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchParentGenusType(logEntryGenusType, true);
        return (this.session.getLogEntriesByQuery(query));
    }


    /**
     *  Gets a <code>LogEntryList</code> containing the given
     *  log entry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  logEntryRecordType a logEntry record type 
     *  @return the returned <code>LogEntry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByRecordType(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchRecordType(logEntryRecordType, true);
        return (this.session.getLogEntriesByQuery(query));
    }

    
    /**
     *  Gets a <code> LogEntryList </code> filtering the list to log
     *  entries including and above the given priority <code>
     *  Type. </code> In plenary mode, the returned list contains all
     *  known entries or an error results. Otherwise, the returned
     *  list may contain only those entries that are accessible
     *  through this session.
     *
     *  @param  priorityType a log entry priority type 
     *  @return the returned <code> LogEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> priorityType
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByPriorityType(org.osid.type.Type priorityType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
     
        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchMinimumPriority(priorityType, true);
        return (this.session.getLogEntriesByQuery(query));
    }


    /**
     *  Gets a <code> LogEntryList </code> corresponding to the given
     *  time interval inclusive. <code> </code> In plenary mode, the
     *  returned list contains all known entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  start a starting time 
     *  @param  end a starting time 
     *  @return the returned <code> LogEntry </code> list 
     *  @throws org.osid.InvalidArgumentException <code> start </code>
     *          is greater than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> start </code> or
     *          <code> end </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByDate(org.osid.calendaring.DateTime start, 
                                                             org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchTimestamp(start, end, true);
        return (this.session.getLogEntriesByQuery(query));
    }

    
    /**
     *  Gets a <code> LogEntryList </code> corresponding to the given
     *  time interval inclusive filtering the list to log entries
     *  including and above the given priority <code> Type. </code> In
     *  plenary mode, the returned list contains all known entries or
     *  an error results.  Otherwise, the returned list may contain
     *  only those entries that are accessible through this session.
     *
     *  @param  priorityType a log entry priority type 
     *  @param  start a starting time 
     *  @param  end a starting time 
     *  @return the returned <code> LogEntry </code> list 
     *  @throws org.osid.InvalidArgumentException <code> start </code>
     *          is greater than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> priorityType,
     *          start </code> or <code> end </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByPriorityTypeAndDate(org.osid.type.Type priorityType, 
                                                                            org.osid.calendaring.DateTime start, 
                                                                            org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchMinimumPriority(priorityType, true);
        query.matchTimestamp(start, end, true);
        return (this.session.getLogEntriesByQuery(query));
    }


    /**
     *  Gets a <code> LogEntryList </code> for an agent associated
     *  with the given resource. In plenary mode, the returned list
     *  contains all known entries or an error results. Otherwise, the
     *  returned list may contain only those entries that are
     *  accessible through this session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> LogEntry </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getLogEntriesByQuery(query));
    }


    /**
     *  Gets a <code> LogEntryList </code> corresponding to the given
     *  time interval inclusive for an agent associated with the given
     *  resource.  <code> </code> In plenary mode, the returned list
     *  contains all known entries or an error results. Otherwise, the
     *  returned list may contain only those entries that are
     *  accessible through this session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  start a starting time 
     *  @param  end a starting time 
     *  @return the returned <code> LogEntry </code> list 
     *  @throws org.osid.InvalidArgumentException <code> start </code>
     *          is greater than <code> end </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          start </code> or <code> end </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByDateForResource(org.osid.id.Id resourceId, 
                                                                        org.osid.calendaring.DateTime start, 
                                                                        org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchTimestamp(start, end, true);
        return (this.session.getLogEntriesByQuery(query));
    }


    /**
     *  Gets a <code> LogEntryList </code> corresponding to the given
     *  time interval inclusive for an agent associated with the given
     *  resource filtering the list to log entries including and above
     *  the given priority <code> Type. </code> In plenary mode, the
     *  returned list contains all known entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  priorityType a log entry priority type 
     *  @param  start a starting time 
     *  @param  end a starting time 
     *  @return the returned <code> LogEntry </code> list 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId, 
     *          priorityType, start </code> or <code> end </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntriesByPriorityTypeAndDateForResource(org.osid.id.Id resourceId, 
                                                                                       org.osid.type.Type priorityType, 
                                                                                       org.osid.calendaring.DateTime start, 
                                                                                       org.osid.calendaring.DateTime end)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchMinimumPriority(priorityType, true);
        query.matchResourceId(resourceId, true);
        query.matchTimestamp(start, end, true);
        return (this.session.getLogEntriesByQuery(query));
    }



    /**
     *  Gets all <code>LogEntries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  log entries or an error results. Otherwise, the returned list
     *  may contain only those log entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>LogEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogEntryList getLogEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.logging.LogEntryQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getLogEntriesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.logging.LogEntryQuery getQuery() {
        org.osid.logging.LogEntryQuery query = this.session.getLogEntryQuery();
        return (query);
    }
}
