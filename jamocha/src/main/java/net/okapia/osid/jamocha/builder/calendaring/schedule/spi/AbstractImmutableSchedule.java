//
// AbstractImmutableSchedule.java
//
//     Wraps a mutable Schedule to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.schedule.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Schedule</code> to hide modifiers. This
 *  wrapper provides an immutized Schedule from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying schedule whose state changes are visible.
 */

public abstract class AbstractImmutableSchedule
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.calendaring.Schedule {

    private final org.osid.calendaring.Schedule schedule;


    /**
     *  Constructs a new <code>AbstractImmutableSchedule</code>.
     *
     *  @param schedule the schedule to immutablize
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSchedule(org.osid.calendaring.Schedule schedule) {
        super(schedule);
        this.schedule = schedule;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the schedule slot. 
     *
     *  @return the schedule slot <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScheduleSlotId() {
        return (this.schedule.getScheduleSlotId());
    }


    /**
     *  Gets the schedule slot included inside this one. 
     *
     *  @return the schedule slot 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleSlot getScheduleSlot()
        throws org.osid.OperationFailedException {

        return (this.schedule.getScheduleSlot());
    }


    /**
     *  Tests if a <code> TimePeriod </code> is associated with this schedule. 
     *  The time period determines the start and end time of the recurring 
     *  series. 
     *
     *  @return <code> true </code> if there is an associated <code> 
     *          TimePeriod, </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasTimePeriod() {
        return (this.schedule.hasTimePeriod());
    }


    /**
     *  Gets the <code> TimePeriod Id </code> for this recurring event. A 
     *  <code> Schedule </code> with an associated <code> TimePeriod </code> 
     *  overrides any start or end date set. 
     *
     *  @return the time period <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimePeriodId() {
        return (this.schedule.getTimePeriodId());
    }


    /**
     *  Gets the <code> TimePeriod </code> for this recurring event. A <code> 
     *  Schedule </code> with an associated <code> TimePeriod </code> 
     *  overrides any start or end date set. 
     *
     *  @return the time period 
     *  @throws org.osid.IllegalStateException <code> hasTimePeriod() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod()
        throws org.osid.OperationFailedException {

        return (this.schedule.getTimePeriod());
    }


    /**
     *  Gets the start date of this schedule. If <code> hasTimePeriod() 
     *  </code> is <code> true, </code> the start date is the start date of 
     *  the associated <code> TimePeriod. </code> 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getScheduleStart() {
        return (this.schedule.getScheduleStart());
    }


    /**
     *  Gets the end date of this schedule. If <code> hasTimePeriod() </code> 
     *  is <code> true, </code> the end date is the end date of the associated 
     *  <code> TimePeriod. </code> 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getScheduleEnd() {
        return (this.schedule.getScheduleEnd());
    }


    /**
     *  Tests if this schedule has a limit on the number of occurrences. 
     *
     *  @return <code> true </code> if there is a limit <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean hasLimit() {
        return (this.schedule.hasLimit());
    }


    /**
     *  Gets the limit of the number of occurences of this schedule. 
     *
     *  @return the limit 
     *  @throws org.osid.IllegalStateException <code> hasLimitl() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getLimit() {
        return (this.schedule.getLimit());
    }


    /**
     *  Gets the total duration of the schedule. 
     *
     *  @return the total duration 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getTotalDuration() {
        return (this.schedule.getTotalDuration());
    }


    /**
     *  Gets a descriptive location. 
     *
     *  @return the location 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.schedule.getLocationDescription());
    }


    /**
     *  Tests if a location is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated location, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.schedule.hasLocation());
    }


    /**
     *  Gets the location <code> Id </code> . 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        return (this.schedule.getLocationId());
    }


    /**
     *  Gets the <code> Location. </code> 
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        return (this.schedule.getLocation());
    }


    /**
     *  Gets the schedule record corresponding to the given <code> Schedule 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> 
     *  scheduleRecordType </code> may be the <code> Type </code> returned in 
     *  <code> getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(scheduleRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  scheduleRecordType the type of the record to retrieve 
     *  @return the schedule record 
     *  @throws org.osid.NullArgumentException <code> scheduleRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(scheduleRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.records.ScheduleRecord getScheduleRecord(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException {

        return (this.schedule.getScheduleRecord(scheduleRecordType));
    }
}

