//
// AbstractIndexedMapNodeLookupSession.java
//
//    A simple framework for providing a Node lookup service
//    backed by a fixed collection of nodes with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Node lookup service backed by a
 *  fixed collection of nodes. The nodes are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some nodes may be compatible
 *  with more types than are indicated through these node
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Nodes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapNodeLookupSession
    extends AbstractMapNodeLookupSession
    implements org.osid.topology.NodeLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.topology.Node> nodesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.Node>());
    private final MultiMap<org.osid.type.Type, org.osid.topology.Node> nodesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.topology.Node>());


    /**
     *  Makes a <code>Node</code> available in this session.
     *
     *  @param  node a node
     *  @throws org.osid.NullArgumentException <code>node<code> is
     *          <code>null</code>
     */

    @Override
    protected void putNode(org.osid.topology.Node node) {
        super.putNode(node);

        this.nodesByGenus.put(node.getGenusType(), node);
        
        try (org.osid.type.TypeList types = node.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.nodesByRecord.put(types.getNextType(), node);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a node from this session.
     *
     *  @param nodeId the <code>Id</code> of the node
     *  @throws org.osid.NullArgumentException <code>nodeId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeNode(org.osid.id.Id nodeId) {
        org.osid.topology.Node node;
        try {
            node = getNode(nodeId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.nodesByGenus.remove(node.getGenusType());

        try (org.osid.type.TypeList types = node.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.nodesByRecord.remove(types.getNextType(), node);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeNode(nodeId);
        return;
    }


    /**
     *  Gets a <code>NodeList</code> corresponding to the given
     *  node genus <code>Type</code> which does not include
     *  nodes of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known nodes or an error results. Otherwise,
     *  the returned list may contain only those nodes that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  nodeGenusType a node genus type 
     *  @return the returned <code>Node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByGenusType(org.osid.type.Type nodeGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.node.ArrayNodeList(this.nodesByGenus.get(nodeGenusType)));
    }


    /**
     *  Gets a <code>NodeList</code> containing the given
     *  node record <code>Type</code>. In plenary mode, the
     *  returned list contains all known nodes or an error
     *  results. Otherwise, the returned list may contain only those
     *  nodes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  nodeRecordType a node record type 
     *  @return the returned <code>node</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>nodeRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.topology.NodeList getNodesByRecordType(org.osid.type.Type nodeRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.topology.node.ArrayNodeList(this.nodesByRecord.get(nodeRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.nodesByGenus.clear();
        this.nodesByRecord.clear();

        super.close();

        return;
    }
}
