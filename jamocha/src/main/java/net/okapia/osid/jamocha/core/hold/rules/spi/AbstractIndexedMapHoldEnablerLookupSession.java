//
// AbstractIndexedMapHoldEnablerLookupSession.java
//
//    A simple framework for providing a HoldEnabler lookup service
//    backed by a fixed collection of hold enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hold.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a HoldEnabler lookup service backed by a
 *  fixed collection of hold enablers. The hold enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some hold enablers may be compatible
 *  with more types than are indicated through these hold enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>HoldEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapHoldEnablerLookupSession
    extends AbstractMapHoldEnablerLookupSession
    implements org.osid.hold.rules.HoldEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.hold.rules.HoldEnabler> holdEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hold.rules.HoldEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.hold.rules.HoldEnabler> holdEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.hold.rules.HoldEnabler>());


    /**
     *  Makes a <code>HoldEnabler</code> available in this session.
     *
     *  @param  holdEnabler a hold enabler
     *  @throws org.osid.NullArgumentException <code>holdEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putHoldEnabler(org.osid.hold.rules.HoldEnabler holdEnabler) {
        super.putHoldEnabler(holdEnabler);

        this.holdEnablersByGenus.put(holdEnabler.getGenusType(), holdEnabler);
        
        try (org.osid.type.TypeList types = holdEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.holdEnablersByRecord.put(types.getNextType(), holdEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a hold enabler from this session.
     *
     *  @param holdEnablerId the <code>Id</code> of the hold enabler
     *  @throws org.osid.NullArgumentException <code>holdEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeHoldEnabler(org.osid.id.Id holdEnablerId) {
        org.osid.hold.rules.HoldEnabler holdEnabler;
        try {
            holdEnabler = getHoldEnabler(holdEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.holdEnablersByGenus.remove(holdEnabler.getGenusType());

        try (org.osid.type.TypeList types = holdEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.holdEnablersByRecord.remove(types.getNextType(), holdEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeHoldEnabler(holdEnablerId);
        return;
    }


    /**
     *  Gets a <code>HoldEnablerList</code> corresponding to the given
     *  hold enabler genus <code>Type</code> which does not include
     *  hold enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known hold enablers or an error results. Otherwise,
     *  the returned list may contain only those hold enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  holdEnablerGenusType a hold enabler genus type 
     *  @return the returned <code>HoldEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByGenusType(org.osid.type.Type holdEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.rules.holdenabler.ArrayHoldEnablerList(this.holdEnablersByGenus.get(holdEnablerGenusType)));
    }


    /**
     *  Gets a <code>HoldEnablerList</code> containing the given
     *  hold enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known hold enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  hold enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  holdEnablerRecordType a hold enabler record type 
     *  @return the returned <code>holdEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablersByRecordType(org.osid.type.Type holdEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.hold.rules.holdenabler.ArrayHoldEnablerList(this.holdEnablersByRecord.get(holdEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.holdEnablersByGenus.clear();
        this.holdEnablersByRecord.clear();

        super.close();

        return;
    }
}
