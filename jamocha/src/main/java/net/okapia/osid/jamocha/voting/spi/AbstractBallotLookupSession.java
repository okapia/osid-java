//
// AbstractBallotLookupSession.java
//
//    A starter implementation framework for providing a Ballot
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Ballot
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBallots(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBallotLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.voting.BallotLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();
    

    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated with
     *  this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this session.
     *
     *  @return the <code>Polls</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>Ballot</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBallots() {
        return (true);
    }


    /**
     *  A complete view of the <code>Ballot</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBallotView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Ballot</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBallotView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballots in pollses which are children of
     *  this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active ballots are returned by methods in this session.
     */
     
    @OSID @Override
    public void useActiveBallotView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive ballots are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusBallotView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Ballot</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Ballot</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Ballot</code> and retained for
     *  compatibility.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots
     *  are returned.
     *
     *  @param  ballotId <code>Id</code> of the
     *          <code>Ballot</code>
     *  @return the ballot
     *  @throws org.osid.NotFoundException <code>ballotId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ballotId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Ballot getBallot(org.osid.id.Id ballotId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.voting.BallotList ballots = getBallots()) {
            while (ballots.hasNext()) {
                org.osid.voting.Ballot ballot = ballots.getNextBallot();
                if (ballot.getId().equals(ballotId)) {
                    return (ballot);
                }
            }
        } 

        throw new org.osid.NotFoundException(ballotId + " not found");
    }


    /**
     *  Gets a <code>BallotList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the ballots
     *  specified in the <code>Id</code> list, in the order of the
     *  list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Ballots</code> may
     *  be omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBallots()</code>.
     *
     *  @param  ballotIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Ballot</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByIds(org.osid.id.IdList ballotIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.voting.Ballot> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = ballotIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBallot(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("ballot " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.voting.ballot.LinkedBallotList(ret));
    }


    /**
     *  Gets a <code>BallotList</code> corresponding to the given
     *  ballot genus <code>Type</code> which does not include ballots
     *  of types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known ballots
     *  or an error results. Otherwise, the returned list may contain
     *  only those ballots that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBallots()</code>.
     *
     *  @param  ballotGenusType a ballot genus type 
     *  @return the returned <code>Ballot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByGenusType(org.osid.type.Type ballotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.ballot.BallotGenusFilterList(getBallots(), ballotGenusType));
    }


    /**
     *  Gets a <code>BallotList</code> corresponding to the given
     *  ballot genus <code>Type</code> and include any additional
     *  ballots with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known ballots
     *  or an error results. Otherwise, the returned list may contain
     *  only those ballots that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBallots()</code>.
     *
     *  @param  ballotGenusType a ballot genus type 
     *  @return the returned <code>Ballot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByParentGenusType(org.osid.type.Type ballotGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBallotsByGenusType(ballotGenusType));
    }


    /**
     *  Gets a <code>BallotList</code> containing the given ballot
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known ballots
     *  or an error results. Otherwise, the returned list may contain
     *  only those ballots that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots are
     *  returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBallots()</code>.
     *
     *  @param  ballotRecordType a ballot record type 
     *  @return the returned <code>Ballot</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByRecordType(org.osid.type.Type ballotRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.ballot.BallotRecordFilterList(getBallots(), ballotRecordType));
    }


    /**
     *  Gets a <code>BallotList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known ballots
     *  or an error results. Otherwise, the returned list may contain
     *  only those ballots that are accessible through this session.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots are
     *  returned.
     *
     *  @param resourceId a resource <code>Id</code>
     *  @return the returned <code>Ballot</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallotsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.voting.ballot.BallotProviderFilterList(getBallots(), resourceId));
    }


    /**
     *  Gets a <code>BallotList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known ballots
     *  or an error results. Otherwise, the returned list may contain
     *  only those ballots that are accessible through this session.
     *  
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Ballot</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.BallotList getBallotsOnDate(org.osid.calendaring.DateTime from, 
                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.ballot.TemporalBallotFilterList(getBallots(), from, to));
    }
        

    /**
     *  Gets all <code>Ballots</code>.
     *
     *  In plenary mode, the returned list contains all known ballots
     *  or an error results. Otherwise, the returned list may contain
     *  only those ballots that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  In active mode, ballots are returned that are currently
     *  active. In any status mode, active and inactive ballots are
     *  returned.
     *
     *  @return a list of <code>Ballots</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.voting.BallotList getBallots()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the ballot list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of ballots
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.voting.BallotList filterBallotsOnViews(org.osid.voting.BallotList list)
        throws org.osid.OperationFailedException {

        org.osid.voting.BallotList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.voting.ballot.ActiveBallotFilterList(ret);
        }

        return (ret);
    }
}
