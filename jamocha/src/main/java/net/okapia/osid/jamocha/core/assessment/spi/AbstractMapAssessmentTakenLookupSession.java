//
// AbstractMapAssessmentTakenLookupSession
//
//    A simple framework for providing an AssessmentTaken lookup service
//    backed by a fixed collection of assessments taken.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an AssessmentTaken lookup service backed by a
 *  fixed collection of assessments taken. The assessments taken are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AssessmentsTaken</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAssessmentTakenLookupSession
    extends net.okapia.osid.jamocha.assessment.spi.AbstractAssessmentTakenLookupSession
    implements org.osid.assessment.AssessmentTakenLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.assessment.AssessmentTaken> assessmentsTaken = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.assessment.AssessmentTaken>());


    /**
     *  Makes an <code>AssessmentTaken</code> available in this session.
     *
     *  @param  assessmentTaken an assessment taken
     *  @throws org.osid.NullArgumentException <code>assessmentTaken<code>
     *          is <code>null</code>
     */

    protected void putAssessmentTaken(org.osid.assessment.AssessmentTaken assessmentTaken) {
        this.assessmentsTaken.put(assessmentTaken.getId(), assessmentTaken);
        return;
    }


    /**
     *  Makes an array of assessments taken available in this session.
     *
     *  @param  assessmentsTaken an array of assessments taken
     *  @throws org.osid.NullArgumentException <code>assessmentsTaken<code>
     *          is <code>null</code>
     */

    protected void putAssessmentsTaken(org.osid.assessment.AssessmentTaken[] assessmentsTaken) {
        putAssessmentsTaken(java.util.Arrays.asList(assessmentsTaken));
        return;
    }


    /**
     *  Makes a collection of assessments taken available in this session.
     *
     *  @param  assessmentsTaken a collection of assessments taken
     *  @throws org.osid.NullArgumentException <code>assessmentsTaken<code>
     *          is <code>null</code>
     */

    protected void putAssessmentsTaken(java.util.Collection<? extends org.osid.assessment.AssessmentTaken> assessmentsTaken) {
        for (org.osid.assessment.AssessmentTaken assessmentTaken : assessmentsTaken) {
            this.assessmentsTaken.put(assessmentTaken.getId(), assessmentTaken);
        }

        return;
    }


    /**
     *  Removes an AssessmentTaken from this session.
     *
     *  @param  assessmentTakenId the <code>Id</code> of the assessment taken
     *  @throws org.osid.NullArgumentException <code>assessmentTakenId<code> is
     *          <code>null</code>
     */

    protected void removeAssessmentTaken(org.osid.id.Id assessmentTakenId) {
        this.assessmentsTaken.remove(assessmentTakenId);
        return;
    }


    /**
     *  Gets the <code>AssessmentTaken</code> specified by its <code>Id</code>.
     *
     *  @param  assessmentTakenId <code>Id</code> of the <code>AssessmentTaken</code>
     *  @return the assessmentTaken
     *  @throws org.osid.NotFoundException <code>assessmentTakenId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>assessmentTakenId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTaken getAssessmentTaken(org.osid.id.Id assessmentTakenId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.assessment.AssessmentTaken assessmentTaken = this.assessmentsTaken.get(assessmentTakenId);
        if (assessmentTaken == null) {
            throw new org.osid.NotFoundException("assessmentTaken not found: " + assessmentTakenId);
        }

        return (assessmentTaken);
    }


    /**
     *  Gets all <code>AssessmentsTaken</code>. In plenary mode, the returned
     *  list contains all known assessmentsTaken or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessmentsTaken that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>AssessmentsTaken</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTaken()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.assessmenttaken.ArrayAssessmentTakenList(this.assessmentsTaken.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessmentsTaken.clear();
        super.close();
        return;
    }
}
