//
// InvariantMapAssessmentTakenLookupSession
//
//    Implements an AssessmentTaken lookup service backed by a fixed collection of
//    assessmentsTaken.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements an AssessmentTaken lookup service backed by a fixed
 *  collection of assessments taken. The assessments taken are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapAssessmentTakenLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractMapAssessmentTakenLookupSession
    implements org.osid.assessment.AssessmentTakenLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentTakenLookupSession</code> with no
     *  assessments taken.
     *  
     *  @param bank the bank
     *  @throws org.osid.NullArgumnetException {@code bank} is
     *          {@code null}
     */

    public InvariantMapAssessmentTakenLookupSession(org.osid.assessment.Bank bank) {
        setBank(bank);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentTakenLookupSession</code> with a single
     *  assessment taken.
     *  
     *  @param bank the bank
     *  @param assessmentTaken an single assessment taken
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentTaken} is <code>null</code>
     */

      public InvariantMapAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.AssessmentTaken assessmentTaken) {
        this(bank);
        putAssessmentTaken(assessmentTaken);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentTakenLookupSession</code> using an array
     *  of assessments taken.
     *  
     *  @param bank the bank
     *  @param assessmentsTaken an array of assessments taken
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentsTaken} is <code>null</code>
     */

      public InvariantMapAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                               org.osid.assessment.AssessmentTaken[] assessmentsTaken) {
        this(bank);
        putAssessmentsTaken(assessmentsTaken);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapAssessmentTakenLookupSession</code> using a
     *  collection of assessments taken.
     *
     *  @param bank the bank
     *  @param assessmentsTaken a collection of assessments taken
     *  @throws org.osid.NullArgumentException {@code bank} or
     *          {@code assessmentsTaken} is <code>null</code>
     */

      public InvariantMapAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                               java.util.Collection<? extends org.osid.assessment.AssessmentTaken> assessmentsTaken) {
        this(bank);
        putAssessmentsTaken(assessmentsTaken);
        return;
    }
}
