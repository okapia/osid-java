//
// MutableIndexedMapQueueConstrainerLookupSession
//
//    Implements a QueueConstrainer lookup service backed by a collection of
//    queueConstrainers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.rules;


/**
 *  Implements a QueueConstrainer lookup service backed by a collection of
 *  queue constrainers. The queue constrainers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some queue constrainers may be compatible
 *  with more types than are indicated through these queue constrainer
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of queue constrainers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapQueueConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.tracking.rules.spi.AbstractIndexedMapQueueConstrainerLookupSession
    implements org.osid.tracking.rules.QueueConstrainerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapQueueConstrainerLookupSession} with no queue constrainers.
     *
     *  @param frontOffice the front office
     *  @throws org.osid.NullArgumentException {@code frontOffice}
     *          is {@code null}
     */

      public MutableIndexedMapQueueConstrainerLookupSession(org.osid.tracking.FrontOffice frontOffice) {
        setFrontOffice(frontOffice);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapQueueConstrainerLookupSession} with a
     *  single queue constrainer.
     *  
     *  @param frontOffice the front office
     *  @param  queueConstrainer a single queueConstrainer
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueConstrainer} is {@code null}
     */

    public MutableIndexedMapQueueConstrainerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                  org.osid.tracking.rules.QueueConstrainer queueConstrainer) {
        this(frontOffice);
        putQueueConstrainer(queueConstrainer);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapQueueConstrainerLookupSession} using an
     *  array of queue constrainers.
     *
     *  @param frontOffice the front office
     *  @param  queueConstrainers an array of queue constrainers
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueConstrainers} is {@code null}
     */

    public MutableIndexedMapQueueConstrainerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                  org.osid.tracking.rules.QueueConstrainer[] queueConstrainers) {
        this(frontOffice);
        putQueueConstrainers(queueConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapQueueConstrainerLookupSession} using a
     *  collection of queue constrainers.
     *
     *  @param frontOffice the front office
     *  @param  queueConstrainers a collection of queue constrainers
     *  @throws org.osid.NullArgumentException {@code frontOffice} or
     *          {@code queueConstrainers} is {@code null}
     */

    public MutableIndexedMapQueueConstrainerLookupSession(org.osid.tracking.FrontOffice frontOffice,
                                                  java.util.Collection<? extends org.osid.tracking.rules.QueueConstrainer> queueConstrainers) {

        this(frontOffice);
        putQueueConstrainers(queueConstrainers);
        return;
    }
    

    /**
     *  Makes a {@code QueueConstrainer} available in this session.
     *
     *  @param  queueConstrainer a queue constrainer
     *  @throws org.osid.NullArgumentException {@code queueConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putQueueConstrainer(org.osid.tracking.rules.QueueConstrainer queueConstrainer) {
        super.putQueueConstrainer(queueConstrainer);
        return;
    }


    /**
     *  Makes an array of queue constrainers available in this session.
     *
     *  @param  queueConstrainers an array of queue constrainers
     *  @throws org.osid.NullArgumentException {@code queueConstrainers{@code 
     *          is {@code null}
     */

    @Override
    public void putQueueConstrainers(org.osid.tracking.rules.QueueConstrainer[] queueConstrainers) {
        super.putQueueConstrainers(queueConstrainers);
        return;
    }


    /**
     *  Makes collection of queue constrainers available in this session.
     *
     *  @param  queueConstrainers a collection of queue constrainers
     *  @throws org.osid.NullArgumentException {@code queueConstrainer{@code  is
     *          {@code null}
     */

    @Override
    public void putQueueConstrainers(java.util.Collection<? extends org.osid.tracking.rules.QueueConstrainer> queueConstrainers) {
        super.putQueueConstrainers(queueConstrainers);
        return;
    }


    /**
     *  Removes a QueueConstrainer from this session.
     *
     *  @param queueConstrainerId the {@code Id} of the queue constrainer
     *  @throws org.osid.NullArgumentException {@code queueConstrainerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeQueueConstrainer(org.osid.id.Id queueConstrainerId) {
        super.removeQueueConstrainer(queueConstrainerId);
        return;
    }    
}
