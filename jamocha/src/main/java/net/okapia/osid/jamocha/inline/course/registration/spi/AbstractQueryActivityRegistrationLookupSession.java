//
// AbstractQueryActivityRegistrationLookupSession.java
//
//    An inline adapter that maps an ActivityRegistrationLookupSession to
//    an ActivityRegistrationQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.registration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ActivityRegistrationLookupSession to
 *  an ActivityRegistrationQuerySession.
 */

public abstract class AbstractQueryActivityRegistrationLookupSession
    extends net.okapia.osid.jamocha.course.registration.spi.AbstractActivityRegistrationLookupSession
    implements org.osid.course.registration.ActivityRegistrationLookupSession {

    private boolean effectiveonly = false;
    private final org.osid.course.registration.ActivityRegistrationQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryActivityRegistrationLookupSession.
     *
     *  @param querySession the underlying activity registration query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryActivityRegistrationLookupSession(org.osid.course.registration.ActivityRegistrationQuerySession querySession) {
        nullarg(querySession, "activity registration query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>ActivityRegistration</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivityRegistrations() {
        return (this.session.canSearchActivityRegistrations());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity registrations in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only activity registrations whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveActivityRegistrationView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All activity registrations of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveActivityRegistrationView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>ActivityRegistration</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActivityRegistration</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>ActivityRegistration</code> and retained for
     *  compatibility.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityRegistrationId <code>Id</code> of the
     *          <code>ActivityRegistration</code>
     *  @return the activity registration
     *  @throws org.osid.NotFoundException <code>activityRegistrationId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityRegistrationId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistration getActivityRegistration(org.osid.id.Id activityRegistrationId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchId(activityRegistrationId, true);
        org.osid.course.registration.ActivityRegistrationList activityRegistrations = this.session.getActivityRegistrationsByQuery(query);
        if (activityRegistrations.hasNext()) {
            return (activityRegistrations.getNextActivityRegistration());
        } 
        
        throw new org.osid.NotFoundException(activityRegistrationId + " not found");
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  activityRegistrations specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>ActivityRegistrations</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In effective mode, activity registrations are returned that are currently effective.
     *  In any effective mode, effective activity registrations and those currently expired
     *  are returned.
     *
     *  @param  activityRegistrationIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActivityRegistration</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByIds(org.osid.id.IdList activityRegistrationIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();

        try (org.osid.id.IdList ids = activityRegistrationIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> corresponding to
     *  the given activity registration genus <code>Type</code> which
     *  does not include activity registrations of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityRegistrationGenusType an activityRegistration genus type 
     *  @return the returned <code>ActivityRegistration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByGenusType(org.osid.type.Type activityRegistrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchGenusType(activityRegistrationGenusType, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> corresponding to
     *  the given activity registration genus <code>Type</code> and
     *  include any additional activity registrations with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, activity registrations are returned that are currently
     *  effective.  In any effective mode, effective activity registrations and
     *  those currently expired are returned.
     *
     *  @param  activityRegistrationGenusType an activityRegistration genus type 
     *  @return the returned <code>ActivityRegistration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByParentGenusType(org.osid.type.Type activityRegistrationGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchParentGenusType(activityRegistrationGenusType, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> containing the
     *  given activity registration record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityRegistrationRecordType an activityRegistration record type 
     *  @return the returned <code>ActivityRegistration</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsByRecordType(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchRecordType(activityRegistrationRecordType, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets an <code>ActivityRegistrationList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *  
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ActivityRegistration</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsOnDate(org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }
        

    /**
     *  Gets a list of activity registrations corresponding to a
     *  registration <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible
     *  through this session.
     *
     *  In effective mode, activity registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are returned.
     *
     *  @param  registrationId the <code>Id</code> of the registration
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>registrationId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForRegistration(org.osid.id.Id registrationId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchRegistrationId(registrationId, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of activity registrations corresponding to a
     *  registration <code>Id</code> and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  registrationId the <code>Id</code> of the registration
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>registrationId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForRegistrationOnDate(org.osid.id.Id registrationId,
                                                                                                               org.osid.calendaring.DateTime from,
                                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchRegistrationId(registrationId, true);
        query.matchDate(from, to, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets all <code> ActivityRegistrations </code> associated with
     *  a given <code> Activity. </code>
     *  
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *  
     *  In effective mode, activity registrations are returned that
     *  are currently effective. In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @return a list of <code> ActivityRegistrations </code> 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivity(org.osid.id.Id activityId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchActivityId(activityId, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets all <code> ActivityRegistrations </code> for the given
     *  activity and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *  
     *  In effective mode, activity registrations are returned that
     *  are currently effective. In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of <code> ActivityRegistrations </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> activityId, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivityOnDate(org.osid.id.Id activityId, 
                                                                                                           org.osid.calendaring.DateTime from, 
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchActivityId(activityId, true);
        query.matchDate(from, to, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of activity registrations corresponding to a
     *  student <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForStudent(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of activity registrations corresponding to a student
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  activity registrations or an error results. Otherwise, the returned list
     *  may contain only those activity registrations that are accessible
     *  through this session.
     *
     *  In effective mode, activity registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForStudentOnDate(org.osid.id.Id resourceId,
                                                                                                          org.osid.calendaring.DateTime from,
                                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchStudentId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of activity registrations corresponding to
     *  activity and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  activityId the <code>Id</code> of the activity
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivityAndStudent(org.osid.id.Id activityId,
                                                                                                                   org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchActivityId(activityId, true);
        query.matchStudentId(resourceId, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets a list of activity registrations corresponding to
     *  activity and student <code>Ids</code> and effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that are
     *  currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are returned.
     *
     *  @param  activityId the <code>Id</code> of the activity
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>activityId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForActivityAndStudentOnDate(org.osid.id.Id activityId,
                                                                                                                         org.osid.id.Id resourceId,
                                                                                                                         org.osid.calendaring.DateTime from,
                                                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchActivityId(activityId, true);
        query.matchStudentId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }

    




    /**
     *  Gets all <code> ActivityRegistrations </code> associated with
     *  a given <code> CourseOffering. </code>
     *  
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *  
     *  In effective mode, activity registrations are returned that
     *  are currently effective. In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @return a list of <code> ActivityRegistrations </code> 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        if (query.supportsActivityQuery()) {
            query.getActivityQuery().matchCourseOfferingId(courseOfferingId, true);
            return (this.session.getActivityRegistrationsByQuery(query));
        }

        return (super.getActivityRegistrationsForCourseOffering(courseOfferingId));
    }


    /**
     *  Gets all <code> ActivityRegistrations </code> for the given
     *  course offering and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those registrations that are accessible
     *  through this session.
     *  
     *  In effective mode, activity registrations are returned that
     *  are currently effective. In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return a list of <code> ActivityRegistrations </code> 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>
     *          courseOfferingId, from, </code> or <code> to </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOfferingOnDate(org.osid.id.Id courseOfferingId, 
                                                                                                           org.osid.calendaring.DateTime from, 
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        if (query.supportsActivityQuery()) {
            query.getActivityQuery().matchCourseOfferingId(courseOfferingId, true);
            query.matchDate(from, to, true);
            return (this.session.getActivityRegistrationsByQuery(query));
        }

        return (super.getActivityRegistrationsForCourseOfferingOnDate(courseOfferingId, from, to));
    }


    /**
     *  Gets a list of activity registrations corresponding to
     *  course offering and student <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param courseOfferingId the <code>Id</code> of the course
     *         offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>courseOfferingId</code>, <code>resourceId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOfferingAndStudent(org.osid.id.Id courseOfferingId,
                                                                                                                   org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        if (query.supportsActivityQuery()) {
            query.getActivityQuery().matchCourseOfferingId(courseOfferingId, true);
            query.matchStudentId(resourceId, true);
            return (this.session.getActivityRegistrationsByQuery(query));
        }

        return (super.getActivityRegistrationsForCourseOfferingAndStudent(courseOfferingId, resourceId));
    }


    /**
     *  Gets a list of activity registrations corresponding to course
     *  offering and student <code>Ids</code> and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @param  courseOfferingId the <code>Id</code> of the course offering
     *  @param  resourceId the <code>Id</code> of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ActivityRegistrationList</code>
     *  @throws org.osid.NullArgumentException <code>courseOfferingId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrationsForCourseOfferingAndStudentOnDate(org.osid.id.Id courseOfferingId,
                                                                                                                         org.osid.id.Id resourceId,
                                                                                                                         org.osid.calendaring.DateTime from,
                                                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        if (query.supportsActivityQuery()) {
            query.getActivityQuery().matchCourseOfferingId(courseOfferingId, true);
            query.matchStudentId(resourceId, true);
            query.matchDate(from, to, true);
            return (this.session.getActivityRegistrationsByQuery(query));
        }

        return (super.getActivityRegistrationsForCourseOfferingAndStudentOnDate(courseOfferingId, resourceId, from, to));
    }


    /**
     *  Gets all <code>ActivityRegistrations</code>. 
     *
     *  In plenary mode, the returned list contains all known activity
     *  registrations or an error results. Otherwise, the returned
     *  list may contain only those activity registrations that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, activity registrations are returned that
     *  are currently effective.  In any effective mode, effective
     *  activity registrations and those currently expired are
     *  returned.
     *
     *  @return a list of <code>ActivityRegistrations</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.registration.ActivityRegistrationList getActivityRegistrations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.registration.ActivityRegistrationQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getActivityRegistrationsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.registration.ActivityRegistrationQuery getQuery() {
        org.osid.course.registration.ActivityRegistrationQuery query = this.session.getActivityRegistrationQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
