//
// AbstractAssemblyBlogQuery.java
//
//     A BlogQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.blogging.blog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BlogQuery that stores terms.
 */

public abstract class AbstractAssemblyBlogQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.blogging.BlogQuery,
               org.osid.blogging.BlogQueryInspector,
               org.osid.blogging.BlogSearchOrder {

    private final java.util.Collection<org.osid.blogging.records.BlogQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.blogging.records.BlogQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.blogging.records.BlogSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBlogQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBlogQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the entry <code> Id </code> for this query. 
     *
     *  @param  entryId an entry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> entryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEntryId(org.osid.id.Id entryId, boolean match) {
        getAssembler().addIdTerm(getEntryIdColumn(), entryId, match);
        return;
    }


    /**
     *  Clears the entry <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEntryIdTerms() {
        getAssembler().clearTerms(getEntryIdColumn());
        return;
    }


    /**
     *  Gets the entry <code> Id </code> terms. 
     *
     *  @return the entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEntryIdTerms() {
        return (getAssembler().getIdTerms(getEntryIdColumn()));
    }


    /**
     *  Gets the EntryId column name.
     *
     * @return the column name
     */

    protected String getEntryIdColumn() {
        return ("entry_id");
    }


    /**
     *  Tests if a <code> EntryQuery </code> is available. 
     *
     *  @return <code> true </code> if an entry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEntryQuery() {
        return (false);
    }


    /**
     *  Gets the query for an entry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the entry query 
     *  @throws org.osid.UnimplementedException <code> supportsEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.EntryQuery getEntryQuery() {
        throw new org.osid.UnimplementedException("supportsEntryQuery() is false");
    }


    /**
     *  Matches blogs with any entries. 
     *
     *  @param  match <code> true </code> to match blogs with any entries, 
     *          <code> false </code> to match blogs with no entries 
     */

    @OSID @Override
    public void matchAnyEntry(boolean match) {
        getAssembler().addIdWildcardTerm(getEntryColumn(), match);
        return;
    }


    /**
     *  Clears the entry terms. 
     */

    @OSID @Override
    public void clearEntryTerms() {
        getAssembler().clearTerms(getEntryColumn());
        return;
    }


    /**
     *  Gets the entry terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.blogging.EntryQueryInspector[] getEntryTerms() {
        return (new org.osid.blogging.EntryQueryInspector[0]);
    }


    /**
     *  Gets the Entry column name.
     *
     * @return the column name
     */

    protected String getEntryColumn() {
        return ("entry");
    }


    /**
     *  Sets the blog <code> Id </code> for this query to match blogs that 
     *  have the specified blog as an ancestor. 
     *
     *  @param  blogId a blog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorBlogId(org.osid.id.Id blogId, boolean match) {
        getAssembler().addIdTerm(getAncestorBlogIdColumn(), blogId, match);
        return;
    }


    /**
     *  Clears the ancestor blog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorBlogIdTerms() {
        getAssembler().clearTerms(getAncestorBlogIdColumn());
        return;
    }


    /**
     *  Gets the ancestor blog <code> Id </code> terms. 
     *
     *  @return the ancestor blog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBlogIdTerms() {
        return (getAssembler().getIdTerms(getAncestorBlogIdColumn()));
    }


    /**
     *  Gets the AncestorBlogId column name.
     *
     * @return the column name
     */

    protected String getAncestorBlogIdColumn() {
        return ("ancestor_blog_id");
    }


    /**
     *  Tests if a <code> BlogQuery </code> is available. 
     *
     *  @return <code> true </code> if a blog query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorBlogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a blog. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the blog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorBlogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogQuery getAncestorBlogQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorBlogQuery() is false");
    }


    /**
     *  Matches blogs with any ancestor. 
     *
     *  @param  match <code> true </code> to match blogs with any ancestor, 
     *          <code> false </code> to match root blogs 
     */

    @OSID @Override
    public void matchAnyAncestorBlog(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorBlogColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor blog terms. 
     */

    @OSID @Override
    public void clearAncestorBlogTerms() {
        getAssembler().clearTerms(getAncestorBlogColumn());
        return;
    }


    /**
     *  Gets the ancestor blog terms. 
     *
     *  @return the ancestor blog terms 
     */

    @OSID @Override
    public org.osid.blogging.BlogQueryInspector[] getAncestorBlogTerms() {
        return (new org.osid.blogging.BlogQueryInspector[0]);
    }


    /**
     *  Gets the AncestorBlog column name.
     *
     * @return the column name
     */

    protected String getAncestorBlogColumn() {
        return ("ancestor_blog");
    }


    /**
     *  Sets the blog <code> Id </code> for this query to match blogs that 
     *  have the specified blog as a descendant. 
     *
     *  @param  blogId a blog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> blogId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantBlogId(org.osid.id.Id blogId, boolean match) {
        getAssembler().addIdTerm(getDescendantBlogIdColumn(), blogId, match);
        return;
    }


    /**
     *  Clears the descendant blog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantBlogIdTerms() {
        getAssembler().clearTerms(getDescendantBlogIdColumn());
        return;
    }


    /**
     *  Gets the descendant blog <code> Id </code> terms. 
     *
     *  @return the descendant blog <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBlogIdTerms() {
        return (getAssembler().getIdTerms(getDescendantBlogIdColumn()));
    }


    /**
     *  Gets the DescendantBlogId column name.
     *
     * @return the column name
     */

    protected String getDescendantBlogIdColumn() {
        return ("descendant_blog_id");
    }


    /**
     *  Tests if a <code> BlogQuery </code> is available. 
     *
     *  @return <code> true </code> if a blog query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantBlogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a blog. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the blog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantBlogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BlogQuery getDescendantBlogQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantBlogQuery() is false");
    }


    /**
     *  Matches blogs with any descendant. 
     *
     *  @param  match <code> true </code> to match blogs with any descendant, 
     *          <code> false </code> to match leaf blogs 
     */

    @OSID @Override
    public void matchAnyDescendantBlog(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantBlogColumn(), match);
        return;
    }


    /**
     *  Clears the descendant blog terms. 
     */

    @OSID @Override
    public void clearDescendantBlogTerms() {
        getAssembler().clearTerms(getDescendantBlogColumn());
        return;
    }


    /**
     *  Gets the descendant blog terms. 
     *
     *  @return the descendant blog terms 
     */

    @OSID @Override
    public org.osid.blogging.BlogQueryInspector[] getDescendantBlogTerms() {
        return (new org.osid.blogging.BlogQueryInspector[0]);
    }


    /**
     *  Gets the DescendantBlog column name.
     *
     * @return the column name
     */

    protected String getDescendantBlogColumn() {
        return ("descendant_blog");
    }


    /**
     *  Tests if this blog supports the given record
     *  <code>Type</code>.
     *
     *  @param  blogRecordType a blog record type 
     *  @return <code>true</code> if the blogRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>blogRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type blogRecordType) {
        for (org.osid.blogging.records.BlogQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(blogRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  blogRecordType the blog record type 
     *  @return the blog query record 
     *  @throws org.osid.NullArgumentException
     *          <code>blogRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blogRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.BlogQueryRecord getBlogQueryRecord(org.osid.type.Type blogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.BlogQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(blogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blogRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  blogRecordType the blog record type 
     *  @return the blog query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>blogRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blogRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.BlogQueryInspectorRecord getBlogQueryInspectorRecord(org.osid.type.Type blogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.BlogQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(blogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blogRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param blogRecordType the blog record type
     *  @return the blog search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>blogRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(blogRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.blogging.records.BlogSearchOrderRecord getBlogSearchOrderRecord(org.osid.type.Type blogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.blogging.records.BlogSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(blogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(blogRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this blog. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param blogQueryRecord the blog query record
     *  @param blogQueryInspectorRecord the blog query inspector
     *         record
     *  @param blogSearchOrderRecord the blog search order record
     *  @param blogRecordType blog record type
     *  @throws org.osid.NullArgumentException
     *          <code>blogQueryRecord</code>,
     *          <code>blogQueryInspectorRecord</code>,
     *          <code>blogSearchOrderRecord</code> or
     *          <code>blogRecordTypeblog</code> is
     *          <code>null</code>
     */
            
    protected void addBlogRecords(org.osid.blogging.records.BlogQueryRecord blogQueryRecord, 
                                      org.osid.blogging.records.BlogQueryInspectorRecord blogQueryInspectorRecord, 
                                      org.osid.blogging.records.BlogSearchOrderRecord blogSearchOrderRecord, 
                                      org.osid.type.Type blogRecordType) {

        addRecordType(blogRecordType);

        nullarg(blogQueryRecord, "blog query record");
        nullarg(blogQueryInspectorRecord, "blog query inspector record");
        nullarg(blogSearchOrderRecord, "blog search odrer record");

        this.queryRecords.add(blogQueryRecord);
        this.queryInspectorRecords.add(blogQueryInspectorRecord);
        this.searchOrderRecords.add(blogSearchOrderRecord);
        
        return;
    }
}
