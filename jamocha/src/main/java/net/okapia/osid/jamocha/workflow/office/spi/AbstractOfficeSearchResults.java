//
// AbstractOfficeSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.office.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractOfficeSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.workflow.OfficeSearchResults {

    private org.osid.workflow.OfficeList offices;
    private final org.osid.workflow.OfficeQueryInspector inspector;
    private final java.util.Collection<org.osid.workflow.records.OfficeSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractOfficeSearchResults.
     *
     *  @param offices the result set
     *  @param officeQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>offices</code>
     *          or <code>officeQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractOfficeSearchResults(org.osid.workflow.OfficeList offices,
                                            org.osid.workflow.OfficeQueryInspector officeQueryInspector) {
        nullarg(offices, "offices");
        nullarg(officeQueryInspector, "office query inspectpr");

        this.offices = offices;
        this.inspector = officeQueryInspector;

        return;
    }


    /**
     *  Gets the office list resulting from a search.
     *
     *  @return an office list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.workflow.OfficeList getOffices() {
        if (this.offices == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.workflow.OfficeList offices = this.offices;
        this.offices = null;
	return (offices);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.workflow.OfficeQueryInspector getOfficeQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  office search record <code> Type. </code> This method must
     *  be used to retrieve an office implementing the requested
     *  record.
     *
     *  @param officeSearchRecordType an office search 
     *         record type 
     *  @return the office search
     *  @throws org.osid.NullArgumentException
     *          <code>officeSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(officeSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.records.OfficeSearchResultsRecord getOfficeSearchResultsRecord(org.osid.type.Type officeSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.workflow.records.OfficeSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(officeSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(officeSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record office search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addOfficeRecord(org.osid.workflow.records.OfficeSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "office record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
