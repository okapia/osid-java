//
// AbstractOfferingConstrainerEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.offeringconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractOfferingConstrainerEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.rules.OfferingConstrainerEnablerSearchResults {

    private org.osid.offering.rules.OfferingConstrainerEnablerList offeringConstrainerEnablers;
    private final org.osid.offering.rules.OfferingConstrainerEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractOfferingConstrainerEnablerSearchResults.
     *
     *  @param offeringConstrainerEnablers the result set
     *  @param offeringConstrainerEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>offeringConstrainerEnablers</code>
     *          or <code>offeringConstrainerEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractOfferingConstrainerEnablerSearchResults(org.osid.offering.rules.OfferingConstrainerEnablerList offeringConstrainerEnablers,
                                            org.osid.offering.rules.OfferingConstrainerEnablerQueryInspector offeringConstrainerEnablerQueryInspector) {
        nullarg(offeringConstrainerEnablers, "offering constrainer enablers");
        nullarg(offeringConstrainerEnablerQueryInspector, "offering constrainer enabler query inspectpr");

        this.offeringConstrainerEnablers = offeringConstrainerEnablers;
        this.inspector = offeringConstrainerEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the offering constrainer enabler list resulting from a search.
     *
     *  @return an offering constrainer enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerList getOfferingConstrainerEnablers() {
        if (this.offeringConstrainerEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.rules.OfferingConstrainerEnablerList offeringConstrainerEnablers = this.offeringConstrainerEnablers;
        this.offeringConstrainerEnablers = null;
	return (offeringConstrainerEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.rules.OfferingConstrainerEnablerQueryInspector getOfferingConstrainerEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  offering constrainer enabler search record <code> Type. </code> This method must
     *  be used to retrieve an offeringConstrainerEnabler implementing the requested
     *  record.
     *
     *  @param offeringConstrainerEnablerSearchRecordType an offeringConstrainerEnabler search 
     *         record type 
     *  @return the offering constrainer enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(offeringConstrainerEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerEnablerSearchResultsRecord getOfferingConstrainerEnablerSearchResultsRecord(org.osid.type.Type offeringConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.rules.records.OfferingConstrainerEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(offeringConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(offeringConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record offering constrainer enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addOfferingConstrainerEnablerRecord(org.osid.offering.rules.records.OfferingConstrainerEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "offering constrainer enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
