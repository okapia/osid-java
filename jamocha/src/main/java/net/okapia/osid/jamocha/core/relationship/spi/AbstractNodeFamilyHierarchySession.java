//
// AbstractNodeFamilyHierarchySession.java
//
//     Defines a Family hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.relationship.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a family hierarchy session for delivering a hierarchy
 *  of families using the FamilyNode interface.
 */

public abstract class AbstractNodeFamilyHierarchySession
    extends net.okapia.osid.jamocha.relationship.spi.AbstractFamilyHierarchySession
    implements org.osid.relationship.FamilyHierarchySession {

    private java.util.Collection<org.osid.relationship.FamilyNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root family <code> Ids </code> in this hierarchy.
     *
     *  @return the root family <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootFamilyIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.relationship.familynode.FamilyNodeToIdList(this.roots));
    }


    /**
     *  Gets the root families in the family hierarchy. A node
     *  with no parents is an orphan. While all family <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root families 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getRootFamilies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.relationship.familynode.FamilyNodeToFamilyList(new net.okapia.osid.jamocha.relationship.familynode.ArrayFamilyNodeList(this.roots)));
    }


    /**
     *  Adds a root family node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootFamily(org.osid.relationship.FamilyNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root family nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootFamilies(java.util.Collection<org.osid.relationship.FamilyNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root family node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootFamily(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.relationship.FamilyNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Family </code> has any parents. 
     *
     *  @param  familyId a family <code> Id </code> 
     *  @return <code> true </code> if the family has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentFamilies(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getFamilyNode(familyId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  family.
     *
     *  @param  id an <code> Id </code> 
     *  @param  familyId the <code> Id </code> of a family 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> familyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> familyId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfFamily(org.osid.id.Id id, org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.relationship.FamilyNodeList parents = getFamilyNode(familyId).getParentFamilyNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextFamilyNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given family. 
     *
     *  @param  familyId a family <code> Id </code> 
     *  @return the parent <code> Ids </code> of the family 
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentFamilyIds(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.relationship.family.FamilyToIdList(getParentFamilies(familyId)));
    }


    /**
     *  Gets the parents of the given family. 
     *
     *  @param  familyId the <code> Id </code> to query 
     *  @return the parents of the family 
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getParentFamilies(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.relationship.familynode.FamilyNodeToFamilyList(getFamilyNode(familyId).getParentFamilyNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  family.
     *
     *  @param  id an <code> Id </code> 
     *  @param  familyId the Id of a family 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> familyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfFamily(org.osid.id.Id id, org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfFamily(id, familyId)) {
            return (true);
        }

        try (org.osid.relationship.FamilyList parents = getParentFamilies(familyId)) {
            while (parents.hasNext()) {
                if (isAncestorOfFamily(id, parents.getNextFamily().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a family has any children. 
     *
     *  @param  familyId a family <code> Id </code> 
     *  @return <code> true </code> if the <code> familyId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildFamilies(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFamilyNode(familyId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  family.
     *
     *  @param  id an <code> Id </code> 
     *  @param familyId the <code> Id </code> of a 
     *         family
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> familyId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> familyId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfFamily(org.osid.id.Id id, org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfFamily(familyId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  family.
     *
     *  @param  familyId the <code> Id </code> to query 
     *  @return the children of the family 
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildFamilyIds(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.relationship.family.FamilyToIdList(getChildFamilies(familyId)));
    }


    /**
     *  Gets the children of the given family. 
     *
     *  @param  familyId the <code> Id </code> to query 
     *  @return the children of the family 
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getChildFamilies(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.relationship.familynode.FamilyNodeToFamilyList(getFamilyNode(familyId).getChildFamilyNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  family.
     *
     *  @param  id an <code> Id </code> 
     *  @param familyId the <code> Id </code> of a 
     *         family
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> familyId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfFamily(org.osid.id.Id id, org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfFamily(familyId, id)) {
            return (true);
        }

        try (org.osid.relationship.FamilyList children = getChildFamilies(familyId)) {
            while (children.hasNext()) {
                if (isDescendantOfFamily(id, children.getNextFamily().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  family.
     *
     *  @param  familyId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified family node 
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getFamilyNodeIds(org.osid.id.Id familyId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.relationship.familynode.FamilyNodeToNode(getFamilyNode(familyId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given family.
     *
     *  @param  familyId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified family node 
     *  @throws org.osid.NotFoundException <code> familyId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> familyId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyNode getFamilyNodes(org.osid.id.Id familyId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFamilyNode(familyId));
    }


    /**
     *  Closes this <code>FamilyHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a family node.
     *
     *  @param familyId the id of the family node
     *  @throws org.osid.NotFoundException <code>familyId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>familyId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.relationship.FamilyNode getFamilyNode(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(familyId, "family Id");
        for (org.osid.relationship.FamilyNode family : this.roots) {
            if (family.getId().equals(familyId)) {
                return (family);
            }

            org.osid.relationship.FamilyNode r = findFamily(family, familyId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(familyId + " is not found");
    }


    protected org.osid.relationship.FamilyNode findFamily(org.osid.relationship.FamilyNode node, 
                                                          org.osid.id.Id familyId) 
	throws org.osid.OperationFailedException {

        try (org.osid.relationship.FamilyNodeList children = node.getChildFamilyNodes()) {
            while (children.hasNext()) {
                org.osid.relationship.FamilyNode family = children.getNextFamilyNode();
                if (family.getId().equals(familyId)) {
                    return (family);
                }
                
                family = findFamily(family, familyId);
                if (family != null) {
                    return (family);
                }
            }
        }

        return (null);
    }
}
