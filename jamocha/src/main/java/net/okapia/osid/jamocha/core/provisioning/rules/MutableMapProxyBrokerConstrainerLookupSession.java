//
// MutableMapProxyBrokerConstrainerLookupSession
//
//    Implements a BrokerConstrainer lookup service backed by a collection of
//    brokerConstrainers that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules;


/**
 *  Implements a BrokerConstrainer lookup service backed by a collection of
 *  brokerConstrainers. The brokerConstrainers are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of broker constrainers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyBrokerConstrainerLookupSession
    extends net.okapia.osid.jamocha.core.provisioning.rules.spi.AbstractMapBrokerConstrainerLookupSession
    implements org.osid.provisioning.rules.BrokerConstrainerLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyBrokerConstrainerLookupSession}
     *  with no broker constrainers.
     *
     *  @param distributor the distributor
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyBrokerConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                  org.osid.proxy.Proxy proxy) {
        setDistributor(distributor);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBrokerConstrainerLookupSession} with a
     *  single broker constrainer.
     *
     *  @param distributor the distributor
     *  @param brokerConstrainer a broker constrainer
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerConstrainer}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBrokerConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                org.osid.provisioning.rules.BrokerConstrainer brokerConstrainer, org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putBrokerConstrainer(brokerConstrainer);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBrokerConstrainerLookupSession} using an
     *  array of broker constrainers.
     *
     *  @param distributor the distributor
     *  @param brokerConstrainers an array of broker constrainers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerConstrainers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBrokerConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                org.osid.provisioning.rules.BrokerConstrainer[] brokerConstrainers, org.osid.proxy.Proxy proxy) {
        this(distributor, proxy);
        putBrokerConstrainers(brokerConstrainers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBrokerConstrainerLookupSession} using a
     *  collection of broker constrainers.
     *
     *  @param distributor the distributor
     *  @param brokerConstrainers a collection of broker constrainers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code distributor},
     *          {@code brokerConstrainers}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBrokerConstrainerLookupSession(org.osid.provisioning.Distributor distributor,
                                                java.util.Collection<? extends org.osid.provisioning.rules.BrokerConstrainer> brokerConstrainers,
                                                org.osid.proxy.Proxy proxy) {
   
        this(distributor, proxy);
        setSessionProxy(proxy);
        putBrokerConstrainers(brokerConstrainers);
        return;
    }

    
    /**
     *  Makes a {@code BrokerConstrainer} available in this session.
     *
     *  @param brokerConstrainer an broker constrainer
     *  @throws org.osid.NullArgumentException {@code brokerConstrainer{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerConstrainer(org.osid.provisioning.rules.BrokerConstrainer brokerConstrainer) {
        super.putBrokerConstrainer(brokerConstrainer);
        return;
    }


    /**
     *  Makes an array of brokerConstrainers available in this session.
     *
     *  @param brokerConstrainers an array of broker constrainers
     *  @throws org.osid.NullArgumentException {@code brokerConstrainers{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerConstrainers(org.osid.provisioning.rules.BrokerConstrainer[] brokerConstrainers) {
        super.putBrokerConstrainers(brokerConstrainers);
        return;
    }


    /**
     *  Makes collection of broker constrainers available in this session.
     *
     *  @param brokerConstrainers
     *  @throws org.osid.NullArgumentException {@code brokerConstrainer{@code 
     *          is {@code null}
     */

    @Override
    public void putBrokerConstrainers(java.util.Collection<? extends org.osid.provisioning.rules.BrokerConstrainer> brokerConstrainers) {
        super.putBrokerConstrainers(brokerConstrainers);
        return;
    }


    /**
     *  Removes a BrokerConstrainer from this session.
     *
     *  @param brokerConstrainerId the {@code Id} of the broker constrainer
     *  @throws org.osid.NullArgumentException {@code brokerConstrainerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBrokerConstrainer(org.osid.id.Id brokerConstrainerId) {
        super.removeBrokerConstrainer(brokerConstrainerId);
        return;
    }    
}
