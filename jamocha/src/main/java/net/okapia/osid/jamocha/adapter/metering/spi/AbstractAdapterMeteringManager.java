//
// AbstractMeteringManager.java
//
//     An adapter for a MeteringManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.metering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MeteringManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMeteringManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.metering.MeteringManager>
    implements org.osid.metering.MeteringManager {


    /**
     *  Constructs a new {@code AbstractAdapterMeteringManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMeteringManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMeteringManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMeteringManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if reading meters is supported. 
     *
     *  @return <code> true </code> if reading meters is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterReading() {
        return (getAdapteeManager().supportsMeterReading());
    }


    /**
     *  Tests if statistical lookup is supported. 
     *
     *  @return <code> true </code> if statistical lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticLookup() {
        return (getAdapteeManager().supportsStatisticLookup());
    }


    /**
     *  Tests if statistical query is supported. 
     *
     *  @return <code> true </code> if statistical query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticQuery() {
        return (getAdapteeManager().supportsStatisticQuery());
    }


    /**
     *  Tests if statistical search is supported. 
     *
     *  @return <code> true </code> if statistical search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticSearch() {
        return (getAdapteeManager().supportsStatisticSearch());
    }


    /**
     *  Tests if statistical notification is supported. 
     *
     *  @return <code> true </code> if statistical notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticNotification() {
        return (getAdapteeManager().supportsStatisticNotification());
    }


    /**
     *  Tests if statistical smart utilitiy is supported. 
     *
     *  @return <code> true </code> if statistical smart utility is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStatisticSmartUtility() {
        return (getAdapteeManager().supportsStatisticSmartUtility());
    }


    /**
     *  Tests if looking up meters is supported. 
     *
     *  @return <code> true </code> if looking up meters is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterLookup() {
        return (getAdapteeManager().supportsMeterLookup());
    }


    /**
     *  Tests if querying meters is supported. 
     *
     *  @return <code> true </code> if querying meters is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterQuery() {
        return (getAdapteeManager().supportsMeterQuery());
    }


    /**
     *  Tests if searching meters is supported. 
     *
     *  @return <code> true </code> if searching meters is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterSearch() {
        return (getAdapteeManager().supportsMeterSearch());
    }


    /**
     *  Tests if meter notification is supported,. 
     *
     *  @return <code> true </code> if meter notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterNotification() {
        return (getAdapteeManager().supportsMeterNotification());
    }


    /**
     *  Tests if looking up meter utlity mappings is supported,. 
     *
     *  @return <code> true </code> if utility meter mapping is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterUtility() {
        return (getAdapteeManager().supportsMeterUtility());
    }


    /**
     *  Tests if managing meter utlity mappings is supported,. 
     *
     *  @return <code> true </code> if assigning utility meter mappings is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterUtilityAssignment() {
        return (getAdapteeManager().supportsMeterUtilityAssignment());
    }


    /**
     *  Tests if managing meter smart utlity service is supported,. 
     *
     *  @return <code> true </code> if a meter smart utility service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSmartMeterUtility() {
        return (getAdapteeManager().supportsSmartMeterUtility());
    }


    /**
     *  Tests for the availability of a utility lookup service. 
     *
     *  @return <code> true </code> if utility lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityLookup() {
        return (getAdapteeManager().supportsUtilityLookup());
    }


    /**
     *  Tests for the availability of a utility query service. 
     *
     *  @return <code> true </code> if utility query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityQuery() {
        return (getAdapteeManager().supportsUtilityQuery());
    }


    /**
     *  Tests if searching for utilities is available. 
     *
     *  @return <code> true </code> if utility search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilitySearch() {
        return (getAdapteeManager().supportsUtilitySearch());
    }


    /**
     *  Tests for the availability of a utility administrative service for 
     *  creating and deleting utilities. 
     *
     *  @return <code> true </code> if utility administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityAdmin() {
        return (getAdapteeManager().supportsUtilityAdmin());
    }


    /**
     *  Tests for the availability of a utility notification service. 
     *
     *  @return <code> true </code> if utility notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityNotification() {
        return (getAdapteeManager().supportsUtilityNotification());
    }


    /**
     *  Tests for the availability of a utility hierarchy traversal service. 
     *
     *  @return <code> true </code> if utility hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityHierarchy() {
        return (getAdapteeManager().supportsUtilityHierarchy());
    }


    /**
     *  Tests for the availability of a utility hierarchy design service. 
     *
     *  @return <code> true </code> if utility hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsUtilityHierarchyDesign() {
        return (getAdapteeManager().supportsUtilityHierarchyDesign());
    }


    /**
     *  Tests for the availability of a metering batch service. 
     *
     *  @return <code> true </code> if metering batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeteringBatch() {
        return (getAdapteeManager().supportsMeteringBatch());
    }


    /**
     *  Tests for the availability of a metering threshold service. 
     *
     *  @return <code> true </code> if metering threshold service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeteringThreshold() {
        return (getAdapteeManager().supportsMeteringThreshold());
    }


    /**
     *  Gets the supported <code> Statistic </code> record types. 
     *
     *  @return a list containing the supported statistic record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStatisticRecordTypes() {
        return (getAdapteeManager().getStatisticRecordTypes());
    }


    /**
     *  Tests if the given <code> Statistic </code> record type is supported. 
     *
     *  @param  statisticRecordType a <code> Type </code> indicating a <code> 
     *          Statistic </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> statisticRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStatisticRecordType(org.osid.type.Type statisticRecordType) {
        return (getAdapteeManager().supportsStatisticRecordType(statisticRecordType));
    }


    /**
     *  Gets the supported statistic search record types. 
     *
     *  @return a list containing the supported statistic search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getStatisticSearchRecordTypes() {
        return (getAdapteeManager().getStatisticSearchRecordTypes());
    }


    /**
     *  Tests if the given statistic search record type is supported. 
     *
     *  @param  statisticSearchRecordType a <code> Type </code> indicating a 
     *          statistic record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          statisticSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsStatisticSearchRecordType(org.osid.type.Type statisticSearchRecordType) {
        return (getAdapteeManager().supportsStatisticSearchRecordType(statisticSearchRecordType));
    }


    /**
     *  Gets the supported <code> Meter </code> record types. 
     *
     *  @return a list containing the supported meter record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMeterRecordTypes() {
        return (getAdapteeManager().getMeterRecordTypes());
    }


    /**
     *  Tests if the given <code> Meter </code> record type is supported. 
     *
     *  @param  meterRecordType a <code> Type </code> indicating a <code> 
     *          Meter </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> meterRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMeterRecordType(org.osid.type.Type meterRecordType) {
        return (getAdapteeManager().supportsMeterRecordType(meterRecordType));
    }


    /**
     *  Gets the supported meter search record types. 
     *
     *  @return a list containing the supported meter search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMeterSearchRecordTypes() {
        return (getAdapteeManager().getMeterSearchRecordTypes());
    }


    /**
     *  Tests if the given meter search record type is supported. 
     *
     *  @param  meterSearchRecordType a <code> Type </code> indicating a meter 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> meterSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMeterSearchRecordType(org.osid.type.Type meterSearchRecordType) {
        return (getAdapteeManager().supportsMeterSearchRecordType(meterSearchRecordType));
    }


    /**
     *  Gets the supported <code> Utility </code> record types. 
     *
     *  @return a list containing the supported utility record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getUtilityRecordTypes() {
        return (getAdapteeManager().getUtilityRecordTypes());
    }


    /**
     *  Tests if the given <code> Utility </code> record type is supported. 
     *
     *  @param  utilityRecordType a <code> Type </code> indicating a <code> 
     *          Utility </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> utilityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsUtilityRecordType(org.osid.type.Type utilityRecordType) {
        return (getAdapteeManager().supportsUtilityRecordType(utilityRecordType));
    }


    /**
     *  Gets the supported utility search record types. 
     *
     *  @return a list containing the supported utility search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getUtilitySearchRecordTypes() {
        return (getAdapteeManager().getUtilitySearchRecordTypes());
    }


    /**
     *  Tests if the given utility search record type is supported. 
     *
     *  @param  utilitySearchRecordType a <code> Type </code> indicating a 
     *          utility record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> utilitySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsUtilitySearchRecordType(org.osid.type.Type utilitySearchRecordType) {
        return (getAdapteeManager().supportsUtilitySearchRecordType(utilitySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter reading 
     *  service. 
     *
     *  @return a <code> MeterReadingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterReading() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterReadingSession getMeterReadingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterReadingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter reading 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterReadingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterReading() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterReadingSession getMeterReadingSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterReadingSessionForUtility(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  lookup service. 
     *
     *  @return a <code> StatisticLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticLookupSession getStatisticLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStatisticLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  lookup service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticLookupSession getStatisticLookupSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStatisticLookupSessionForUtility(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  query service. 
     *
     *  @return a <code> StatisticQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticQuerySession getStatisticQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStatisticQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  query service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticQuerySession getStatisticQuerySessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStatisticQuerySessionForUtility(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  search service. 
     *
     *  @return a <code> StatisticSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticSearchSession getStatisticSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStatisticSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  search service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticSearchSession getStatisticSearchSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStatisticSearchSessionForUtility(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  notification service. 
     *
     *  @param  statisticReceiver the receiver 
     *  @return a <code> StatisticNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> statisticReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getStatisticNotificationSession(org.osid.metering.StatisticReceiver statisticReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStatisticNotificationSession(statisticReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  notification service for the given utility. 
     *
     *  @param  statisticReceiver the receiver 
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> statisticReceiver 
     *          </code> or <code> utilityId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getStatisticNotificationSessionForUtility(org.osid.metering.StatisticReceiver statisticReceiver, 
                                                                                                org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStatisticNotificationSessionForUtility(statisticReceiver, utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the statistic 
     *  smart utility session. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> StatisticSmartUtilitySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStatisticSmartUtility() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.StatisticSmartUtilitySession getStatisticSmartUtilitySession(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getStatisticSmartUtilitySession(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter lookup 
     *  service. 
     *
     *  @return a <code> MeterLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterLookupSession getMeterLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter lookup 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterLookupSession getMeterLookupSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterLookupSessionForUtility(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter query 
     *  service. 
     *
     *  @return a <code> MeterQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuerySession getMeterQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter query 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterQuerySession </code> 
     *  @throws org.osid.NotFoundException no Utility found by the given Id 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuerySession getMeterQuerySessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterQuerySessionForUtility(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the metering 
     *  search service. 
     *
     *  @return a <code> MeterSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSearchSession getMeterSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter search 
     *  service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSearchSession getMeterSearchSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterSearchSessionForUtility(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  administrative service. 
     *
     *  @return a <code> MeterAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterAdminSession getMeterAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  administrative service for the given utility. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterAdminSession getMeterAdminSessionForUtility(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterAdminSessionForUtility(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  notification service. 
     *
     *  @param  meterReceiver the receiver 
     *  @return a <code> MeterNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> meterReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getMeterNotificationSession(org.osid.metering.MeterReceiver meterReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterNotificationSession(meterReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter 
     *  notification service for the given utility. 
     *
     *  @param  meterReceiver the receiver 
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> meterReceiver </code> or 
     *          <code> utilityId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterNotificationSession getMeterNotificationSessionForUtility(org.osid.metering.MeterReceiver meterReceiver, 
                                                                                            org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterNotificationSessionForUtility(meterReceiver, utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with looking up mapping 
     *  of meters and utilities. 
     *
     *  @return a <code> MeterUtilitySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeterUtility() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterUtilitySession getMeterUtilitySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterUtilitySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with managing the 
     *  mapping of meters and utilities. 
     *
     *  @return a <code> MeterUtilityAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterUtilityAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterUtilityAssignmentSession getMeterUtilityAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterUtilityAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the meter smart 
     *  utility session. 
     *
     *  @param  utilityId the <code> Id </code> of the <code> Utility </code> 
     *  @return a <code> MeterSmartUtilitySession </code> 
     *  @throws org.osid.NotFoundException no <code> Utility </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> utilityId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeterSmartUtility() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterSmartUtilitySession getMeterSmartUtilitySession(org.osid.id.Id utilityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMeterSmartUtilitySession(utilityId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility lookup 
     *  service. 
     *
     *  @return a <code> UtilityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityLookupSession getUtilityLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getUtilityLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility query 
     *  service. 
     *
     *  @return a <code> UtilityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityQuerySession getUtilityQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getUtilityQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility search 
     *  service. 
     *
     *  @return a <code> UtilitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilitySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilitySearchSession getUtilitySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getUtilitySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  administrative service. 
     *
     *  @return a <code> UtilityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsUtilityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityAdminSession getUtilityAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getUtilityAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  notification service. 
     *
     *  @param  utilityReceiver the receiver 
     *  @return a <code> UtilityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> utilityReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityNotificationSession getUtilityNotificationSession(org.osid.metering.UtilityReceiver utilityReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getUtilityNotificationSession(utilityReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  hierarchy service. 
     *
     *  @return a <code> UtilityHierarchySession </code> for utilities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityHierarchySession getUtilityHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getUtilityHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the utility 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for utilities 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsUtilityHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.UtilityHierarchyDesignSession getUtilityHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getUtilityHierarchyDesignSession());
    }


    /**
     *  Gets a <code> MeteringBatchManager. </code> 
     *
     *  @return a <code> MeteringBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMeteringBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.batch.MeteringBatchManager getMeteringBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeteringBatchManager());
    }


    /**
     *  Gets a <code> MeteringThresholdManager. </code> 
     *
     *  @return a <code> MeteringThresholdManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeteringThreshold() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.threshold.MeteringThresholdManager getMeteringThresholdManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMeteringThresholdManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
