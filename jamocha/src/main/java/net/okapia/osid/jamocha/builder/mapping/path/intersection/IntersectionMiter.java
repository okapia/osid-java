//
// IntersectionMiter.java
//
//     Defines an Intersection miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.intersection;


/**
 *  Defines an <code>Intersection</code> miter for use with the builders.
 */

public interface IntersectionMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.mapping.path.Intersection {


    /**
     *  Sets the coordinate.
     *
     *  @param coordinate a coordinate
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    public void setCoordinate(org.osid.mapping.Coordinate coordinate);


    /**
     *  Adds a path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException <code>path</code> is
     *          <code>null</code>
     */

    public void addPath(org.osid.mapping.path.Path path);


    /**
     *  Sets all the paths.
     *
     *  @param paths a collection of paths
     *  @throws org.osid.NullArgumentException <code>paths</code> is
     *          <code>null</code>
     */

    public void setPaths(java.util.Collection<org.osid.mapping.path.Path> paths);


    /**
     *  Sets the rotary flag.
     *
     *  @param rotary <code> true </code> if this intersection is a
     *         rotary, <code> false </code> otherwise
     */

    public void setRotary(boolean rotary);


    /**
     *  Sets the fork flag.
     *
     *  @param fork <code> true </code> if this intersection is a
     *         fork, <code> false </code> otherwise
     */

    public void setFork(boolean fork);

    
    /**
     *  Adds an Intersection record.
     *
     *  @param record an intersection record
     *  @param recordType the type of intersection record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addIntersectionRecord(org.osid.mapping.path.records.IntersectionRecord record, org.osid.type.Type recordType);
}       


