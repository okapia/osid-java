//
// AbstractImmutableActivityRegistration.java
//
//     Wraps a mutable ActivityRegistration to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.registration.activityregistration.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>ActivityRegistration</code> to hide modifiers. This
 *  wrapper provides an immutized ActivityRegistration from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying activityRegistration whose state changes are visible.
 */

public abstract class AbstractImmutableActivityRegistration
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.registration.ActivityRegistration {

    private final org.osid.course.registration.ActivityRegistration activityRegistration;


    /**
     *  Constructs a new <code>AbstractImmutableActivityRegistration</code>.
     *
     *  @param activityRegistration the activity registration to immutablize
     *  @throws org.osid.NullArgumentException <code>activityRegistration</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableActivityRegistration(org.osid.course.registration.ActivityRegistration activityRegistration) {
        super(activityRegistration);
        this.activityRegistration = activityRegistration;
        return;
    }


    /**
     *  Gets the registration <code> Id </code> associated with this
     *  activity registration.
     *
     *  @return the registration <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRegistrationId() {
        return (this.activityRegistration.getRegistrationId());
    }


    /**
     *  Gets the registration associated with this activity
     *  registration.
     *
     *  @return the registration 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.registration.Registration getRegistration()
        throws org.osid.OperationFailedException {

        return (this.activityRegistration.getRegistration());
    }


    /**
     *  Gets the activity <code> Id </code> associated with this
     *  registration.
     *
     *  @return the activity <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        return (this.activityRegistration.getActivityId());
    }


    /**
     *  Gets the activity associated with this registration. 
     *
     *  @return the activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Activity getActivity()
        throws org.osid.OperationFailedException {

        return (this.activityRegistration.getActivity());
    }


    /**
     *  Gets the <code> Id </code> of the student <code> Resource. </code> 
     *
     *  @return the <code> Student </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.activityRegistration.getStudentId());
    }


    /**
     *  Gets the student <code> Resource. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.activityRegistration.getStudent());
    }


    /**
     *  Gets the activity registration record corresponding to the given 
     *  <code> ActivityRegistration </code> record <code> Type. </code> This 
     *  method is used to retrieve an object implementing the requested 
     *  record. The <code> activityRegistrationRecordType </code> may be the 
     *  <code> Type </code> returned in <code> getRecordTypes() </code> or any 
     *  of its parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(activityRegistrationRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  activityRegistrationRecordType the type of activity 
     *          registration record to retrieve 
     *  @return the activity registration record 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityRegistrationRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(activityRegistrationRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationRecord getActivityRegistrationRecord(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException {

        return (this.activityRegistration.getActivityRegistrationRecord(activityRegistrationRecordType));
    }
}

