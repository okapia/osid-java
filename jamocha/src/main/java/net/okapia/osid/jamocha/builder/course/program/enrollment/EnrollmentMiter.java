//
// EnrollmentMiter.java
//
//     Defines an Enrollment miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.program.enrollment;


/**
 *  Defines an <code>Enrollment</code> miter for use with the builders.
 */

public interface EnrollmentMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.program.Enrollment {


    /**
     *  Sets the program offering.
     *
     *  @param programOffering a program offering
     *  @throws org.osid.NullArgumentException
     *          <code>programOffering</code> is <code>null</code>
     */

    public void setProgramOffering(org.osid.course.program.ProgramOffering programOffering);


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public void setStudent(org.osid.resource.Resource student);


    /**
     *  Adds an Enrollment record.
     *
     *  @param record an enrollment record
     *  @param recordType the type of enrollment record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addEnrollmentRecord(org.osid.course.program.records.EnrollmentRecord record, org.osid.type.Type recordType);
}       


