//
// AbstractIndexedMapModuleLookupSession.java
//
//    A simple framework for providing a Module lookup service
//    backed by a fixed collection of modules with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Module lookup service backed by a
 *  fixed collection of modules. The modules are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some modules may be compatible
 *  with more types than are indicated through these module
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Modules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapModuleLookupSession
    extends AbstractMapModuleLookupSession
    implements org.osid.course.syllabus.ModuleLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.syllabus.Module> modulesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.syllabus.Module>());
    private final MultiMap<org.osid.type.Type, org.osid.course.syllabus.Module> modulesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.syllabus.Module>());


    /**
     *  Makes a <code>Module</code> available in this session.
     *
     *  @param  module a module
     *  @throws org.osid.NullArgumentException <code>module<code> is
     *          <code>null</code>
     */

    @Override
    protected void putModule(org.osid.course.syllabus.Module module) {
        super.putModule(module);

        this.modulesByGenus.put(module.getGenusType(), module);
        
        try (org.osid.type.TypeList types = module.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.modulesByRecord.put(types.getNextType(), module);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a module from this session.
     *
     *  @param moduleId the <code>Id</code> of the module
     *  @throws org.osid.NullArgumentException <code>moduleId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeModule(org.osid.id.Id moduleId) {
        org.osid.course.syllabus.Module module;
        try {
            module = getModule(moduleId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.modulesByGenus.remove(module.getGenusType());

        try (org.osid.type.TypeList types = module.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.modulesByRecord.remove(types.getNextType(), module);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeModule(moduleId);
        return;
    }


    /**
     *  Gets a <code>ModuleList</code> corresponding to the given
     *  module genus <code>Type</code> which does not include
     *  modules of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known modules or an error results. Otherwise,
     *  the returned list may contain only those modules that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  moduleGenusType a module genus type 
     *  @return the returned <code>Module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByGenusType(org.osid.type.Type moduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.syllabus.module.ArrayModuleList(this.modulesByGenus.get(moduleGenusType)));
    }


    /**
     *  Gets a <code>ModuleList</code> containing the given
     *  module record <code>Type</code>. In plenary mode, the
     *  returned list contains all known modules or an error
     *  results. Otherwise, the returned list may contain only those
     *  modules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  moduleRecordType a module record type 
     *  @return the returned <code>module</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>moduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.ModuleList getModulesByRecordType(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.syllabus.module.ArrayModuleList(this.modulesByRecord.get(moduleRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.modulesByGenus.clear();
        this.modulesByRecord.clear();

        super.close();

        return;
    }
}
