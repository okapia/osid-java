//
// AbstractMapCommissionEnablerLookupSession
//
//    A simple framework for providing a CommissionEnabler lookup service
//    backed by a fixed collection of commission enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CommissionEnabler lookup service backed by a
 *  fixed collection of commission enablers. The commission enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CommissionEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCommissionEnablerLookupSession
    extends net.okapia.osid.jamocha.resourcing.rules.spi.AbstractCommissionEnablerLookupSession
    implements org.osid.resourcing.rules.CommissionEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.resourcing.rules.CommissionEnabler> commissionEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.resourcing.rules.CommissionEnabler>());


    /**
     *  Makes a <code>CommissionEnabler</code> available in this session.
     *
     *  @param  commissionEnabler a commission enabler
     *  @throws org.osid.NullArgumentException <code>commissionEnabler<code>
     *          is <code>null</code>
     */

    protected void putCommissionEnabler(org.osid.resourcing.rules.CommissionEnabler commissionEnabler) {
        this.commissionEnablers.put(commissionEnabler.getId(), commissionEnabler);
        return;
    }


    /**
     *  Makes an array of commission enablers available in this session.
     *
     *  @param  commissionEnablers an array of commission enablers
     *  @throws org.osid.NullArgumentException <code>commissionEnablers<code>
     *          is <code>null</code>
     */

    protected void putCommissionEnablers(org.osid.resourcing.rules.CommissionEnabler[] commissionEnablers) {
        putCommissionEnablers(java.util.Arrays.asList(commissionEnablers));
        return;
    }


    /**
     *  Makes a collection of commission enablers available in this session.
     *
     *  @param  commissionEnablers a collection of commission enablers
     *  @throws org.osid.NullArgumentException <code>commissionEnablers<code>
     *          is <code>null</code>
     */

    protected void putCommissionEnablers(java.util.Collection<? extends org.osid.resourcing.rules.CommissionEnabler> commissionEnablers) {
        for (org.osid.resourcing.rules.CommissionEnabler commissionEnabler : commissionEnablers) {
            this.commissionEnablers.put(commissionEnabler.getId(), commissionEnabler);
        }

        return;
    }


    /**
     *  Removes a CommissionEnabler from this session.
     *
     *  @param  commissionEnablerId the <code>Id</code> of the commission enabler
     *  @throws org.osid.NullArgumentException <code>commissionEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeCommissionEnabler(org.osid.id.Id commissionEnablerId) {
        this.commissionEnablers.remove(commissionEnablerId);
        return;
    }


    /**
     *  Gets the <code>CommissionEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  commissionEnablerId <code>Id</code> of the <code>CommissionEnabler</code>
     *  @return the commissionEnabler
     *  @throws org.osid.NotFoundException <code>commissionEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>commissionEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnabler getCommissionEnabler(org.osid.id.Id commissionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.resourcing.rules.CommissionEnabler commissionEnabler = this.commissionEnablers.get(commissionEnablerId);
        if (commissionEnabler == null) {
            throw new org.osid.NotFoundException("commissionEnabler not found: " + commissionEnablerId);
        }

        return (commissionEnabler);
    }


    /**
     *  Gets all <code>CommissionEnablers</code>. In plenary mode, the returned
     *  list contains all known commissionEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  commissionEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CommissionEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.CommissionEnablerList getCommissionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.commissionenabler.ArrayCommissionEnablerList(this.commissionEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.commissionEnablers.clear();
        super.close();
        return;
    }
}
