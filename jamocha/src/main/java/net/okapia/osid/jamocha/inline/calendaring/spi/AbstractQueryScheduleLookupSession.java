//
// AbstractQueryScheduleLookupSession.java
//
//    An inline adapter that maps a ScheduleLookupSession to
//    a ScheduleQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ScheduleLookupSession to
 *  a ScheduleQuerySession.
 */

public abstract class AbstractQueryScheduleLookupSession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractScheduleLookupSession
    implements org.osid.calendaring.ScheduleLookupSession {

    private final org.osid.calendaring.ScheduleQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryScheduleLookupSession.
     *
     *  @param querySession the underlying schedule query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryScheduleLookupSession(org.osid.calendaring.ScheduleQuerySession querySession) {
        nullarg(querySession, "schedule query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Calendar</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Calendar Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the <code>Calendar</code> associated with this 
     *  session.
     *
     *  @return the <code>Calendar</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform <code>Schedule</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSchedules() {
        return (this.session.canSearchSchedules());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include schedules in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
     
    /**
     *  Gets the <code>Schedule</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Schedule</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Schedule</code> and
     *  retained for compatibility.
     *
     *  @param  scheduleId <code>Id</code> of the
     *          <code>Schedule</code>
     *  @return the schedule
     *  @throws org.osid.NotFoundException <code>scheduleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>scheduleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Schedule getSchedule(org.osid.id.Id scheduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleQuery query = getQuery();
        query.matchId(scheduleId, true);
        org.osid.calendaring.ScheduleList schedules = this.session.getSchedulesByQuery(query);
        if (schedules.hasNext()) {
            return (schedules.getNextSchedule());
        } 
        
        throw new org.osid.NotFoundException(scheduleId + " not found");
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  schedules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Schedules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  scheduleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByIds(org.osid.id.IdList scheduleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleQuery query = getQuery();

        try (org.osid.id.IdList ids = scheduleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getSchedulesByQuery(query));
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  schedule genus <code>Type</code> which does not include
     *  schedules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleGenusType a schedule genus type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByGenusType(org.osid.type.Type scheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleQuery query = getQuery();
        query.matchGenusType(scheduleGenusType, true);
        return (this.session.getSchedulesByQuery(query));
    }


    /**
     *  Gets a <code>ScheduleList</code> corresponding to the given
     *  schedule genus <code>Type</code> and include any additional
     *  schedules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleGenusType a schedule genus type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByParentGenusType(org.osid.type.Type scheduleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleQuery query = getQuery();
        query.matchParentGenusType(scheduleGenusType, true);
        return (this.session.getSchedulesByQuery(query));
    }


    /**
     *  Gets a <code>ScheduleList</code> containing the given
     *  schedule record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  scheduleRecordType a schedule record type 
     *  @return the returned <code>Schedule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>scheduleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByRecordType(org.osid.type.Type scheduleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleQuery query = getQuery();
        query.matchRecordType(scheduleRecordType, true);
        return (this.session.getSchedulesByQuery(query));
    }


    /**
     *  Gets a <code> ScheduleList </code> directly containing the
     *  given shedule slot. <code> </code> In plenary mode, the
     *  returned list contains all known schedule or an error
     *  results. Otherwise, the returned list may contain only those
     *  schedule that are accessible through this session.
     *
     *  @param  scheduleSlotId a schedule slot <code> Id </code> 
     *  @return the returned <code> Schedule </code> list 
     *  @throws org.osid.NullArgumentException <code> scheduleSlotId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByScheduleSlot(org.osid.id.Id scheduleSlotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleQuery query = getQuery();
        query.matchLocationId(scheduleSlotId, true);
        return (this.session.getSchedulesByQuery(query));
    }


    /**
     *  Gets a <code> ScheduleList </code> containing the given
     *  location.  <code> </code> In plenary mode, the returned list
     *  contains all known schedule or an error results. Otherwise,
     *  the returned list may contain only those schedule that are
     *  accessible through this session.
     *
     *  @param  locationId a location <code> Id </code> 
     *  @return the returned <code> Schedule </code> list 
     *  @throws org.osid.NullArgumentException <code> locationId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
    
    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedulesByLocation(org.osid.id.Id locationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleQuery query = getQuery();
        query.matchLocationId(locationId, true);
        return (this.session.getSchedulesByQuery(query));
    }


    /**
     *  Gets all <code>Schedules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  schedules or an error results. Otherwise, the returned list
     *  may contain only those schedules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Schedules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.calendaring.ScheduleQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getSchedulesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.calendaring.ScheduleQuery getQuery() {
        org.osid.calendaring.ScheduleQuery query = this.session.getScheduleQuery();
        return (query);
    }
}
