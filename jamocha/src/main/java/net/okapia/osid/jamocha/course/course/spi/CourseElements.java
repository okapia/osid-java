//
// CourseElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.course.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class CourseElements
    extends net.okapia.osid.jamocha.spi.OperableOsidObjectElements {


    /**
     *  Gets the CourseElement Id.
     *
     *  @return the course element Id
     */

    public static org.osid.id.Id getCourseEntityId() {
        return (makeEntityId("osid.course.Course"));
    }


    /**
     *  Gets the Title element Id.
     *
     *  @return the Title element Id
     */

    public static org.osid.id.Id getTitle() {
        return (makeElementId("osid.course.course.Title"));
    }


    /**
     *  Gets the Number element Id.
     *
     *  @return the Number element Id
     */

    public static org.osid.id.Id getNumber() {
        return (makeElementId("osid.course.course.Number"));
    }


    /**
     *  Gets the SponsorIds element Id.
     *
     *  @return the SponsorIds element Id
     */

    public static org.osid.id.Id getSponsorIds() {
        return (makeElementId("osid.course.course.SponsorIds"));
    }


    /**
     *  Gets the Sponsors element Id.
     *
     *  @return the Sponsors element Id
     */

    public static org.osid.id.Id getSponsors() {
        return (makeElementId("osid.course.course.Sponsors"));
    }


    /**
     *  Gets the CreditScaleId element Id.
     *
     *  @return the CreditScaleId element Id
     */

    public static org.osid.id.Id getCreditScaleId() {
        return (makeElementId("osid.course.course.CreditScaleId"));
    }


    /**
     *  Gets the CreditScale element Id.
     *
     *  @return the CreditScale element Id
     */

    public static org.osid.id.Id getCreditScale() {
        return (makeElementId("osid.course.course.CreditScale"));
    }


    /**
     *  Gets the Credits element Id.
     *
     *  @return the Credits element Id
     */

    public static org.osid.id.Id getCredits() {
        return (makeElementId("osid.course.course.Credits"));
    }


    /**
     *  Gets the PrerequisitesInfo element Id.
     *
     *  @return the PrerequisitesInfo element Id
     */

    public static org.osid.id.Id getPrerequisitesInfo() {
        return (makeElementId("osid.course.course.PrerequisitesInfo"));
    }


    /**
     *  Gets the PrerequisiteIds element Id.
     *
     *  @return the PrerequisiteIds element Id
     */

    public static org.osid.id.Id getPrerequisiteIds() {
        return (makeElementId("osid.course.course.PrerequisiteIds"));
    }


    /**
     *  Gets the Prerequisites element Id.
     *
     *  @return the Prerequisites element Id
     */

    public static org.osid.id.Id getPrerequisites() {
        return (makeElementId("osid.course.course.Prerequisites"));
    }


    /**
     *  Gets the LevelIds element Id.
     *
     *  @return the LevelIds element Id
     */

    public static org.osid.id.Id getLevelIds() {
        return (makeElementId("osid.course.course.LevelIds"));
    }


    /**
     *  Gets the Levels element Id.
     *
     *  @return the Levels element Id
     */

    public static org.osid.id.Id getLevels() {
        return (makeElementId("osid.course.course.Levels"));
    }


    /**
     *  Gets the GradingOptionIds element Id.
     *
     *  @return the GradingOptionIds element Id
     */

    public static org.osid.id.Id getGradingOptionIds() {
        return (makeElementId("osid.course.course.GradingOptionIds"));
    }


    /**
     *  Gets the GradingOptions element Id.
     *
     *  @return the GradingOptions element Id
     */

    public static org.osid.id.Id getGradingOptions() {
        return (makeElementId("osid.course.course.GradingOptions"));
    }


    /**
     *  Gets the LearningObjectiveIds element Id.
     *
     *  @return the LearningObjectiveIds element Id
     */

    public static org.osid.id.Id getLearningObjectiveIds() {
        return (makeElementId("osid.course.course.LearningObjectiveIds"));
    }


    /**
     *  Gets the LearningObjectives element Id.
     *
     *  @return the LearningObjectives element Id
     */

    public static org.osid.id.Id getLearningObjectives() {
        return (makeElementId("osid.course.course.LearningObjectives"));
    }


    /**
     *  Gets the ActivityUnitId element Id.
     *
     *  @return the ActivityUnitId element Id
     */

    public static org.osid.id.Id getActivityUnitId() {
        return (makeQueryElementId("osid.course.course.ActivityUnitId"));
    }


    /**
     *  Gets the ActivityUnit element Id.
     *
     *  @return the ActivityUnit element Id
     */

    public static org.osid.id.Id getActivityUnit() {
        return (makeQueryElementId("osid.course.course.ActivityUnit"));
    }


    /**
     *  Gets the CourseOfferingId element Id.
     *
     *  @return the CourseOfferingId element Id
     */

    public static org.osid.id.Id getCourseOfferingId() {
        return (makeQueryElementId("osid.course.course.CourseOfferingId"));
    }


    /**
     *  Gets the CourseOffering element Id.
     *
     *  @return the CourseOffering element Id
     */

    public static org.osid.id.Id getCourseOffering() {
        return (makeQueryElementId("osid.course.course.CourseOffering"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.course.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.course.CourseCatalog"));
    }
}
