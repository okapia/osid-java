//
// AbstractIndexedMapReplyLookupSession.java
//
//    A simple framework for providing a Reply lookup service
//    backed by a fixed collection of replies with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.forum.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Reply lookup service backed by a
 *  fixed collection of replies. The replies are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some replies may be compatible
 *  with more types than are indicated through these reply
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Replies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapReplyLookupSession
    extends AbstractMapReplyLookupSession
    implements org.osid.forum.ReplyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.forum.Reply> repliesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.forum.Reply>());
    private final MultiMap<org.osid.type.Type, org.osid.forum.Reply> repliesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.forum.Reply>());


    /**
     *  Makes a <code>Reply</code> available in this session.
     *
     *  @param  reply a reply
     *  @throws org.osid.NullArgumentException <code>reply<code> is
     *          <code>null</code>
     */

    @Override
    protected void putReply(org.osid.forum.Reply reply) {
        super.putReply(reply);

        this.repliesByGenus.put(reply.getGenusType(), reply);
        
        try (org.osid.type.TypeList types = reply.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.repliesByRecord.put(types.getNextType(), reply);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a reply from this session.
     *
     *  @param replyId the <code>Id</code> of the reply
     *  @throws org.osid.NullArgumentException <code>replyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeReply(org.osid.id.Id replyId) {
        org.osid.forum.Reply reply;
        try {
            reply = getReply(replyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.repliesByGenus.remove(reply.getGenusType());

        try (org.osid.type.TypeList types = reply.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.repliesByRecord.remove(types.getNextType(), reply);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeReply(replyId);
        return;
    }


    /**
     *  Gets a <code>ReplyList</code> corresponding to the given
     *  reply genus <code>Type</code> which does not include
     *  replies of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known replies or an error results. Otherwise,
     *  the returned list may contain only those replies that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  replyGenusType a reply genus type 
     *  @return the returned <code>Reply</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>replyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByGenusType(org.osid.type.Type replyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.forum.reply.ArrayReplyList(this.repliesByGenus.get(replyGenusType)));
    }


    /**
     *  Gets a <code>ReplyList</code> containing the given
     *  reply record <code>Type</code>. In plenary mode, the
     *  returned list contains all known replies or an error
     *  results. Otherwise, the returned list may contain only those
     *  replies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  replyRecordType a reply record type 
     *  @return the returned <code>reply</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>replyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.forum.ReplyList getRepliesByRecordType(org.osid.type.Type replyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.forum.reply.ArrayReplyList(this.repliesByRecord.get(replyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.repliesByGenus.clear();
        this.repliesByRecord.clear();

        super.close();

        return;
    }
}
