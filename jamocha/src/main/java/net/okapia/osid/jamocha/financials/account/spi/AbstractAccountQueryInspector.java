//
// AbstractAccountQueryInspector.java
//
//     A template for making an AccountQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.account.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for accounts.
 */

public abstract class AbstractAccountQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.financials.AccountQueryInspector {

    private final java.util.Collection<org.osid.financials.records.AccountQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the credit balance query terms. 
     *
     *  @return the credit balance query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCreditBalanceTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the code query terms. 
     *
     *  @return the code query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the summary query terms. 
     *
     *  @return the summary query terms 
     */

    @OSID @Override
    public org.osid.financials.SummaryQueryInspector[] getSummaryTerms() {
        return (new org.osid.financials.SummaryQueryInspector[0]);
    }


    /**
     *  Gets the ancestor account <code> Id </code> query terms. 
     *
     *  @return the ancestor account <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAccountIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor account query terms. 
     *
     *  @return the ancestor account terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAncestorAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Gets the descendant account <code> Id </code> query terms. 
     *
     *  @return the descendant account <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAccountIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant account query terms. 
     *
     *  @return the descendant account terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getDescendantAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given account query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an account implementing the requested record.
     *
     *  @param accountRecordType an account record type
     *  @return the account query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>accountRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(accountRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.AccountQueryInspectorRecord getAccountQueryInspectorRecord(org.osid.type.Type accountRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.AccountQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(accountRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(accountRecordType + " is not supported");
    }


    /**
     *  Adds a record to this account query. 
     *
     *  @param accountQueryInspectorRecord account query inspector
     *         record
     *  @param accountRecordType account record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAccountQueryInspectorRecord(org.osid.financials.records.AccountQueryInspectorRecord accountQueryInspectorRecord, 
                                                   org.osid.type.Type accountRecordType) {

        addRecordType(accountRecordType);
        nullarg(accountRecordType, "account record type");
        this.records.add(accountQueryInspectorRecord);        
        return;
    }
}
