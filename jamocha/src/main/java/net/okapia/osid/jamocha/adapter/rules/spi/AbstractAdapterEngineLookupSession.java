//
// AbstractAdapterEngineLookupSession.java
//
//    An Engine lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Engine lookup session adapter.
 */

public abstract class AbstractAdapterEngineLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.rules.EngineLookupSession {

    private final org.osid.rules.EngineLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterEngineLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEngineLookupSession(org.osid.rules.EngineLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Engine} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupEngines() {
        return (this.session.canLookupEngines());
    }


    /**
     *  A complete view of the {@code Engine} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEngineView() {
        this.session.useComparativeEngineView();
        return;
    }


    /**
     *  A complete view of the {@code Engine} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEngineView() {
        this.session.usePlenaryEngineView();
        return;
    }

     
    /**
     *  Gets the {@code Engine} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Engine} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Engine} and
     *  retained for compatibility.
     *
     *  @param engineId {@code Id} of the {@code Engine}
     *  @return the engine
     *  @throws org.osid.NotFoundException {@code engineId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code engineId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEngine(engineId));
    }


    /**
     *  Gets an {@code EngineList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  engines specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Engines} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  engineIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Engine} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code engineIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.EngineList getEnginesByIds(org.osid.id.IdList engineIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnginesByIds(engineIds));
    }


    /**
     *  Gets an {@code EngineList} corresponding to the given
     *  engine genus {@code Type} which does not include
     *  engines of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  engines or an error results. Otherwise, the returned list
     *  may contain only those engines that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  engineGenusType an engine genus type 
     *  @return the returned {@code Engine} list
     *  @throws org.osid.NullArgumentException
     *          {@code engineGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.EngineList getEnginesByGenusType(org.osid.type.Type engineGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnginesByGenusType(engineGenusType));
    }


    /**
     *  Gets an {@code EngineList} corresponding to the given
     *  engine genus {@code Type} and include any additional
     *  engines with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  engines or an error results. Otherwise, the returned list
     *  may contain only those engines that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  engineGenusType an engine genus type 
     *  @return the returned {@code Engine} list
     *  @throws org.osid.NullArgumentException
     *          {@code engineGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.EngineList getEnginesByParentGenusType(org.osid.type.Type engineGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnginesByParentGenusType(engineGenusType));
    }


    /**
     *  Gets an {@code EngineList} containing the given
     *  engine record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  engines or an error results. Otherwise, the returned list
     *  may contain only those engines that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  engineRecordType an engine record type 
     *  @return the returned {@code Engine} list
     *  @throws org.osid.NullArgumentException
     *          {@code engineRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.EngineList getEnginesByRecordType(org.osid.type.Type engineRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnginesByRecordType(engineRecordType));
    }


    /**
     *  Gets an {@code EngineList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  engines or an error results. Otherwise, the returned list
     *  may contain only those engines that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Engine} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.rules.EngineList getEnginesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEnginesByProvider(resourceId));
    }


    /**
     *  Gets all {@code Engines}. 
     *
     *  In plenary mode, the returned list contains all known
     *  engines or an error results. Otherwise, the returned list
     *  may contain only those engines that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Engines} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.EngineList getEngines()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEngines());
    }
}
