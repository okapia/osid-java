//
// AbstractIndexedMapAuthorizationEnablerLookupSession.java
//
//    A simple framework for providing an AuthorizationEnabler lookup service
//    backed by a fixed collection of authorization enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AuthorizationEnabler lookup service backed by a
 *  fixed collection of authorization enablers. The authorization enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some authorization enablers may be compatible
 *  with more types than are indicated through these authorization enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuthorizationEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuthorizationEnablerLookupSession
    extends AbstractMapAuthorizationEnablerLookupSession
    implements org.osid.authorization.rules.AuthorizationEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.authorization.rules.AuthorizationEnabler> authorizationEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.rules.AuthorizationEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.authorization.rules.AuthorizationEnabler> authorizationEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.authorization.rules.AuthorizationEnabler>());


    /**
     *  Makes an <code>AuthorizationEnabler</code> available in this session.
     *
     *  @param  authorizationEnabler an authorization enabler
     *  @throws org.osid.NullArgumentException <code>authorizationEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAuthorizationEnabler(org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler) {
        super.putAuthorizationEnabler(authorizationEnabler);

        this.authorizationEnablersByGenus.put(authorizationEnabler.getGenusType(), authorizationEnabler);
        
        try (org.osid.type.TypeList types = authorizationEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.authorizationEnablersByRecord.put(types.getNextType(), authorizationEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an authorization enabler from this session.
     *
     *  @param authorizationEnablerId the <code>Id</code> of the authorization enabler
     *  @throws org.osid.NullArgumentException <code>authorizationEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAuthorizationEnabler(org.osid.id.Id authorizationEnablerId) {
        org.osid.authorization.rules.AuthorizationEnabler authorizationEnabler;
        try {
            authorizationEnabler = getAuthorizationEnabler(authorizationEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.authorizationEnablersByGenus.remove(authorizationEnabler.getGenusType());

        try (org.osid.type.TypeList types = authorizationEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.authorizationEnablersByRecord.remove(types.getNextType(), authorizationEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAuthorizationEnabler(authorizationEnablerId);
        return;
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> corresponding to the given
     *  authorization enabler genus <code>Type</code> which does not include
     *  authorization enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known authorization enablers or an error results. Otherwise,
     *  the returned list may contain only those authorization enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  authorizationEnablerGenusType an authorization enabler genus type 
     *  @return the returned <code>AuthorizationEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByGenusType(org.osid.type.Type authorizationEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.rules.authorizationenabler.ArrayAuthorizationEnablerList(this.authorizationEnablersByGenus.get(authorizationEnablerGenusType)));
    }


    /**
     *  Gets an <code>AuthorizationEnablerList</code> containing the given
     *  authorization enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known authorization enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  authorization enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  authorizationEnablerRecordType an authorization enabler record type 
     *  @return the returned <code>authorizationEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authorization.rules.AuthorizationEnablerList getAuthorizationEnablersByRecordType(org.osid.type.Type authorizationEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.authorization.rules.authorizationenabler.ArrayAuthorizationEnablerList(this.authorizationEnablersByRecord.get(authorizationEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.authorizationEnablersByGenus.clear();
        this.authorizationEnablersByRecord.clear();

        super.close();

        return;
    }
}
