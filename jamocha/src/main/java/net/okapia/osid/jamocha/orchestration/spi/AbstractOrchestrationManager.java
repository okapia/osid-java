//
// AbstractOrchestrationManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.orchestration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractOrchestrationManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.orchestration.OrchestrationManager {


    /**
     *  Constructs a new <code>AbstractOrchestrationManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractOrchestrationManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if an acknowledgement provider is supported. 
     *
     *  @return <code> true </code> if an acknowledgement provider is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgementProvider() {
        return (false);
    }


    /**
     *  Tests if an assessment provider is supported. 
     *
     *  @return <code> true </code> if an assessment provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentProvider() {
        return (false);
    }


    /**
     *  Tests if an authentication provider is supported. 
     *
     *  @return <code> true </code> if an authentication provider is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationProvider() {
        return (false);
    }


    /**
     *  Tests if an authorization provider is supported. 
     *
     *  @return <code> true </code> if an authorization provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationProvider() {
        return (false);
    }


    /**
     *  Tests if a bidding provider is supported. 
     *
     *  @return <code> true </code> if a bidding provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingProvider() {
        return (false);
    }


    /**
     *  Tests if a billing provider is supported. 
     *
     *  @return <code> true </code> if a billing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingProvider() {
        return (false);
    }


    /**
     *  Tests if a blogging provider is supported. 
     *
     *  @return <code> true </code> if a blogging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBloggingProvider() {
        return (false);
    }


    /**
     *  Tests if a calendaring provider is supported. 
     *
     *  @return <code> true </code> if a calendaring provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringProvider() {
        return (false);
    }


    /**
     *  Tests if a cataloging provider is supported. 
     *
     *  @return <code> true </code> if a cataloging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogingProvider() {
        return (false);
    }


    /**
     *  Tests if a checklist provider is supported. 
     *
     *  @return <code> true </code> if a checklist provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistProvider() {
        return (false);
    }


    /**
     *  Tests if a commenting provider is supported. 
     *
     *  @return <code> true </code> if a commenting provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingProvider() {
        return (false);
    }


    /**
     *  Tests if a communication provider is supported. 
     *
     *  @return <code> true </code> if a communication provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommunicationProvider() {
        return (false);
    }


    /**
     *  Tests if a configuration provider is supported. 
     *
     *  @return <code> true </code> if a configuration provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationProvider() {
        return (false);
    }


    /**
     *  Tests if a contact provider is supported. 
     *
     *  @return <code> true </code> if a contact provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactProvider() {
        return (false);
    }


    /**
     *  Tests if a control provider is supported. 
     *
     *  @return <code> true </code> if a control provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControlProvider() {
        return (false);
    }


    /**
     *  Tests if a course provider is supported. 
     *
     *  @return <code> true </code> if a course provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProvider() {
        return (false);
    }


    /**
     *  Tests if a dictionary provider is supported. 
     *
     *  @return <code> true </code> if a dictionary provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryProvider() {
        return (false);
    }


    /**
     *  Tests if a filing provider is supported. 
     *
     *  @return <code> true </code> if a filing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingProvider() {
        return (false);
    }


    /**
     *  Tests if a financials provider is supported. 
     *
     *  @return <code> true </code> if a financials provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsProvider() {
        return (false);
    }


    /**
     *  Tests if a forum provider is supported. 
     *
     *  @return <code> true </code> if a forum provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumProvider() {
        return (false);
    }


    /**
     *  Tests if a grading provider is supported. 
     *
     *  @return <code> true </code> if a grading provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingProvider() {
        return (false);
    }


    /**
     *  Tests if a hierarchy provider is supported. 
     *
     *  @return <code> true </code> if a hierarchy provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyProvider() {
        return (false);
    }


    /**
     *  Tests if a hold provider is supported. 
     *
     *  @return <code> true </code> if hold provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldProvider() {
        return (false);
    }


    /**
     *  Tests if an id provider is supported. 
     *
     *  @return <code> true </code> if an id provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdProvider() {
        return (false);
    }


    /**
     *  Tests if an inquiry provider is supported. 
     *
     *  @return <code> true </code> if an inquiry provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryProvider() {
        return (false);
    }


    /**
     *  Tests if an installation provider is supported. 
     *
     *  @return <code> true </code> if an installation provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationProvider() {
        return (false);
    }


    /**
     *  Tests if an inventory provider is supported. 
     *
     *  @return <code> true </code> if an inventory provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryProvider() {
        return (false);
    }


    /**
     *  Tests if a journaling provider is supported. 
     *
     *  @return <code> true </code> if a journaling provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalingProvider() {
        return (false);
    }


    /**
     *  Tests if a learning provider is supported. 
     *
     *  @return <code> true </code> if a learning provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningProvider() {
        return (false);
    }


    /**
     *  Tests if a locale provider is supported. 
     *
     *  @return <code> true </code> if a locale provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocaleProvider() {
        return (false);
    }


    /**
     *  Tests if a logging provider is supported. 
     *
     *  @return <code> true </code> if a logging provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLoggingProvider() {
        return (false);
    }


    /**
     *  Tests if a mapping provider is supported. 
     *
     *  @return <code> true </code> if a mapping provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingProvider() {
        return (false);
    }


    /**
     *  Tests if a messaging provider is supported. 
     *
     *  @return <code> true </code> if a messaging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessagingProvider() {
        return (false);
    }


    /**
     *  Tests if a metering provider is supported. 
     *
     *  @return <code> true </code> if a metering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeteringProvider() {
        return (false);
    }


    /**
     *  Tests if an offering provider is supported. 
     *
     *  @return <code> true </code> if an offering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingProvider() {
        return (false);
    }


    /**
     *  Tests if an ontology provider is supported. 
     *
     *  @return <code> true </code> if an ontology provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyProvider() {
        return (false);
    }


    /**
     *  Tests if an odrering provider is supported. 
     *
     *  @return <code> true </code> if an ordering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderingProvider() {
        return (false);
    }


    /**
     *  Tests if a personnel provider is supported. 
     *
     *  @return <code> true </code> if a personnel provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonnelProvider() {
        return (false);
    }


    /**
     *  Tests if a process provider is supported. 
     *
     *  @return <code> true </code> if a process provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessProvider() {
        return (false);
    }


    /**
     *  Tests if a profile provider is supported. 
     *
     *  @return <code> true </code> if a profile provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileProvider() {
        return (false);
    }


    /**
     *  Tests if a provisioning provider is supported. 
     *
     *  @return <code> true </code> if a provisioning provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisioningProvider() {
        return (false);
    }


    /**
     *  Tests if a proxy provider is supported. 
     *
     *  @return <code> true </code> if a proxy provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a recipe provider is supported. 
     *
     *  @return <code> true </code> if a recipe provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeProvider() {
        return (false);
    }


    /**
     *  Tests if a recognition provider is supported. 
     *
     *  @return <code> true </code> if a recognition provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecognitionProvider() {
        return (false);
    }


    /**
     *  Tests if a relationship provider is supported. 
     *
     *  @return <code> true </code> if a relationship provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipProvider() {
        return (false);
    }


    /**
     *  Tests if a repository provider is supported. 
     *
     *  @return <code> true </code> if a repository provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryProvider() {
        return (false);
    }


    /**
     *  Tests if a resource provider is supported. 
     *
     *  @return <code> true </code> if a resource provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceProvider() {
        return (false);
    }


    /**
     *  Tests if a resourcing provider is supported. 
     *
     *  @return <code> true </code> if a resourcing provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcingProvider() {
        return (false);
    }


    /**
     *  Tests if a rules provider is supported. 
     *
     *  @return <code> true </code> if a rules provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRulesProvider() {
        return (false);
    }


    /**
     *  Tests if a search provider is supported. 
     *
     *  @return <code> true </code> if a search provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSearchProvider() {
        return (false);
    }


    /**
     *  Tests if a sequencing provider is supported. 
     *
     *  @return <code> true </code> if a sequencing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequencingProvider() {
        return (false);
    }


    /**
     *  Tests if a subscription provider is supported. 
     *
     *  @return <code> true </code> if a subscription provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionProvider() {
        return (false);
    }


    /**
     *  Tests if a topology provider is supported. 
     *
     *  @return <code> true </code> if a topology provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyProvider() {
        return (false);
    }


    /**
     *  Tests if a tracking provider is supported. 
     *
     *  @return <code> true </code> if a tracking provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTrackingProvider() {
        return (false);
    }


    /**
     *  Tests if a transaction provider is supported. 
     *
     *  @return <code> true </code> if a transaction provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransactionProvider() {
        return (false);
    }


    /**
     *  Tests if a transport provider is supported. 
     *
     *  @return <code> true </code> if a transport provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransportProvider() {
        return (false);
    }


    /**
     *  Tests if a type provider is supported. 
     *
     *  @return <code> true </code> if a type provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTypeProvider() {
        return (false);
    }


    /**
     *  Tests if a voting provider is supported. 
     *
     *  @return <code> true </code> if a voting provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingProvider() {
        return (false);
    }


    /**
     *  Tests if a workflow provider is supported. 
     *
     *  @return <code> true </code> if a workflow provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowProvider() {
        return (false);
    }


    /**
     *  Tests if an acknowledgement provider is supported. 
     *
     *  @return <code> true </code> if an acknowledgement provider is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcknowledgementProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an assessment provider is supported. 
     *
     *  @return <code> true </code> if an assessment provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an authentication provider is supported. 
     *
     *  @return <code> true </code> if an authentication provider is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthenticationProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an authorization provider is supported. 
     *
     *  @return <code> true </code> if an authorization provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuthorizationProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a bidding provider is supported. 
     *
     *  @return <code> true </code> if a bidding provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBiddingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a billing provider is supported. 
     *
     *  @return <code> true </code> if a billing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBillingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a blogging provider is supported. 
     *
     *  @return <code> true </code> if a blogging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBloggingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a calendaring provider is supported. 
     *
     *  @return <code> true </code> if a calendaring provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendaringProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a cataloging provider is supported. 
     *
     *  @return <code> true </code> if a cataloging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a checklist provider is supported. 
     *
     *  @return <code> true </code> if a checklist provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a commenting provider is supported. 
     *
     *  @return <code> true </code> if a commenting provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a communication provider is supported. 
     *
     *  @return <code> true </code> if a communication provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommunicationProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a configuration provider is supported. 
     *
     *  @return <code> true </code> if a configuration provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a contact provider is supported. 
     *
     *  @return <code> true </code> if a contact provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsContactProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a control provider is supported. 
     *
     *  @return <code> true </code> if a control provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControlProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a course provider is supported. 
     *
     *  @return <code> true </code> if a course provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a dictionary provider is supported. 
     *
     *  @return <code> true </code> if a dictionary provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDictionaryProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a filing provider is supported. 
     *
     *  @return <code> true </code> if a filing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a financials provider is supported. 
     *
     *  @return <code> true </code> if a financials provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFinancialsProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a forum provider is supported. 
     *
     *  @return <code> true </code> if a forum provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsForumProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a grading provider is supported. 
     *
     *  @return <code> true </code> if a grading provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a hierarchy provider is supported. 
     *
     *  @return <code> true </code> if a hierarchy provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHierarchyProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a hold provider is supported. 
     *
     *  @return <code> true </code> if a hold provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an id provider is supported. 
     *
     *  @return <code> true </code> if an id provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIdProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an inquiry provider is supported. 
     *
     *  @return <code> true </code> if an inquiry provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquiryProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an installation provider is supported. 
     *
     *  @return <code> true </code> if an installation provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an inventory provider is supported. 
     *
     *  @return <code> true </code> if an inventory provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInventoryProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a journaling provider is supported. 
     *
     *  @return <code> true </code> if a journaling provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a learning provider is supported. 
     *
     *  @return <code> true </code> if a learning provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLearningProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a locale provider is supported. 
     *
     *  @return <code> true </code> if a locale provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocaleProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a logging provider is supported. 
     *
     *  @return <code> true </code> if a logging provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLoggingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a mapping provider is supported. 
     *
     *  @return <code> true </code> if a mapping provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMappingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a messaging provider is supported. 
     *
     *  @return <code> true </code> if a messaging provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessagingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a metering provider is supported. 
     *
     *  @return <code> true </code> if a metering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeteringProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an offering provider is supported. 
     *
     *  @return <code> true </code> if an offering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an ontology provider is supported. 
     *
     *  @return <code> true </code> if an ontology provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOntologyProxyProvider() {
        return (false);
    }


    /**
     *  Tests if an ordering provider is supported. 
     *
     *  @return <code> true </code> if an ordering provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a personnel provider is supported. 
     *
     *  @return <code> true </code> if a personnel provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonnelProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a process provider is supported. 
     *
     *  @return <code> true </code> if a process provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcessProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a profile provider is supported. 
     *
     *  @return <code> true </code> if a profile provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProfileProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a provisioning provider is supported. 
     *
     *  @return <code> true </code> if a provisioning provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisioningProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a proxy provider is supported. 
     *
     *  @return <code> true </code> if a proxy provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProxyProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a recipe provider is supported. 
     *
     *  @return <code> true </code> if a recipe provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a recognition provider is supported. 
     *
     *  @return <code> true </code> if a recognition provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecognitionProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a relationship provider is supported. 
     *
     *  @return <code> true </code> if a relationship provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRelationshipProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a repository provider is supported. 
     *
     *  @return <code> true </code> if a repository provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a resource provider is supported. 
     *
     *  @return <code> true </code> if a resource provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a resourcing provider is supported. 
     *
     *  @return <code> true </code> if a resourcing provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourcingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a rules provider is supported. 
     *
     *  @return <code> true </code> if a rules provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRulesProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a sequencing provider is supported. 
     *
     *  @return <code> true </code> if a sequencing provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSequencingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a search provider is supported. 
     *
     *  @return <code> true </code> if a search provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSearchProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a subscription provider is supported. 
     *
     *  @return <code> true </code> if a subscription provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubscriptionProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a topology provider is supported. 
     *
     *  @return <code> true </code> if a topology provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTopologyProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a tracking provider is supported. 
     *
     *  @return <code> true </code> if a tracking provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTrackingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a transaction provider is supported. 
     *
     *  @return <code> true </code> if a transaction provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransactionProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a transport provider is supported. 
     *
     *  @return <code> true </code> if a transport provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTransportProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a type provider is supported. 
     *
     *  @return <code> true </code> if a type provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTypeProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a voting provider is supported. 
     *
     *  @return <code> true </code> if a voting provider is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingProxyProvider() {
        return (false);
    }


    /**
     *  Tests if a workflow provider is supported. 
     *
     *  @return <code> true </code> if a workflow provider is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWorkflowProxyProvider() {
        return (false);
    }


    /**
     *  Gets the manager associated with the Acknowledgement service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.AcknowledgementManager getAcknowledgementManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getAcknowledgementManager not implemented");
    }


    /**
     *  Gets the manager associated with the Assessment service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentManager getAssessmentManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getAssessmentManager not implemented");
    }


    /**
     *  Gets the manager associated with the Authentication service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AuthenticationManager getAuthenticationManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getAuthenticationManager not implemented");
    }


    /**
     *  Gets the manager associated with the Authorization service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationManager getAuthorizationManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getAuthorizationManager not implemented");
    }


    /**
     *  Gets the manager associated with the Bidding service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBiddingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BiddingManager getBiddingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getBiddingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Billing service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BillingManager getBillingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getBillingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Blogging service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBloggingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.blogging.BloggingManager getBloggingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getBloggingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Calendaring service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendaringManager getCalendaringManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCalendaringManager not implemented");
    }


    /**
     *  Gets the manager associated with the Cataloging service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogingManager getCatalogingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCatalogingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Checklist service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistManager getChecklistManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getChecklistManager not implemented");
    }


    /**
     *  Gets the manager associated with the Commenting service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentingManager getCommentingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCommentingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Communication service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommunicationProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.communication.CommunicationManager getCommunicationManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCommunicationManager not implemented");
    }


    /**
     *  Gets the manager associated with the Configuration service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationManager getConfigurationManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getConfigurationManager not implemented");
    }


    /**
     *  Gets the manager associated with the Contact service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactManager getContactManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getContactManager not implemented");
    }


    /**
     *  Gets the manager associated with the Control service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControlProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControlManager getControlManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getControlManager not implemented");
    }


    /**
     *  Gets the manager associated with the Course service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseManager getCourseManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCourseManager not implemented");
    }


    /**
     *  Gets the manager associated with the Dictionary service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryManager getDictionaryManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getDictionaryManager not implemented");
    }


    /**
     *  Gets the manager associated with the Filing service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFilingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FilingManager getFilingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getFilingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Financials service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.FinancialsManager getFinancialsManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getFinancialsManager not implemented");
    }


    /**
     *  Gets the manager associated with the Forum service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsForumProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumManager getForumManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getForumManager not implemented");
    }


    /**
     *  Gets the manager associated with the Grading service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradingManager getGradingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getGradingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Hierarchy service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyManager getHierarchyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getHierarchyManager not implemented");
    }


    /**
     *  Gets the manager associated with the Hold service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldManager getHoldManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getHoldManager not implemented");
    }


    /**
     *  Gets the manager associated with the Id service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIdProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdManager getIdManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getIdManager not implemented");
    }


    /**
     *  Gets the manager associated with the Inquiry service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryManager getInquiryManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getInquiryManager not implemented");
    }


    /**
     *  Gets the manager associated with the Installation service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationManager getInstallationManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getInstallationManager not implemented");
    }


    /**
     *  Gets the manager associated with the Inventory service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryManager getInventoryManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getInventoryManager not implemented");
    }


    /**
     *  Gets the manager associated with the Journaling service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdManager getJournalingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getJournalingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Learning service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningManager getLearningManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getLearningManager not implemented");
    }


    /**
     *  Gets the manager associated with the Locale service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocaleProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.LocaleManager getLocaleManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getLocaleManager not implemented");
    }


    /**
     *  Gets the manager associated with the Logging service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLoggingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingManager getLoggingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getLoggingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Mapping service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MappingManager getMappingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getMappingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Messaging service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessagingManager getMessagingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getMessagingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Metering service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeteringManager getMeteringManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getMeteringManager not implemented");
    }


    /**
     *  Gets the manager associated with the Offering service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingManager getOfferingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getOfferingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Ontology service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyManager getOntologyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getOntologyManager not implemented");
    }


    /**
     *  Gets the manager associated with the Ordering service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderingManager getOrderingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getOrderingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Personnel service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonnelProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonnelManager getPersonnelManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getPersonnelManager not implemented");
    }


    /**
     *  Gets the manager associated with the Process service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessManager getProcessManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getProcessManager not implemented");
    }


    /**
     *  Gets the manager associated with the Profile service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileManager getProfileManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getProfileManager not implemented");
    }


    /**
     *  Gets the manager associated with the Provisioning service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisioningManager getProvisioningManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getProvisioningManager not implemented");
    }


    /**
     *  Gets the manager associated with the Proxy service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProxyProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.proxy.ProxyManager getProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getProxyManager not implemented");
    }


    /**
     *  Gets the manager associated with the Recipe service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeManager getRecipieManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRecipieManager not implemented");
    }


    /**
     *  Gets the manager associated with the Recognition service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecognitionProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.RecognitionManager getRecognitionManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRecognitionManager not implemented");
    }


    /**
     *  Gets the manager associated with the Relationship service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipManager getRelationshipManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRelationshipManager not implemented");
    }


    /**
     *  Gets the manager associated with the Repository service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryManager getRepositoryManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRepositoryManager not implemented");
    }


    /**
     *  Gets the manager associated with the Resource service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceManager getResourceManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getResourceManager not implemented");
    }


    /**
     *  Gets the manager associated with the Resourcing service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.ResourcingManager getResourcingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getResourcingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Rules service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRulesProvider() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesManager getRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRulesManager not implemented");
    }


    /**
     *  Gets the manager associated with the Sequencing service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequencingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.SequencingManager getSequencingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getSequencingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Search service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSearchProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchManager getSearchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getSearchManager not implemented");
    }


    /**
     *  Gets the manager associated with the Subscription service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionManager getSubscriptionManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getSubscriptionManager not implemented");
    }


    /**
     *  Gets the manager associated with the Topology service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransportProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyManager getTopologyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTopologyManager not implemented");
    }


    /**
     *  Gets the manager associated with the Tracking service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTrackingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.TrackingManager getTrackingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTrackingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Transaction service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransactionProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transaction.TransactionManager getTransactionManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTransactionManager not implemented");
    }


    /**
     *  Gets the manager associated with the Transport service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransportProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.TransportManager getTransportManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTransportManager not implemented");
    }


    /**
     *  Gets the manager associated with the Type service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.type.TypeManager getTypeManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTypeManager not implemented");
    }


    /**
     *  Gets the manager associated with the Voting service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingManager getVotingManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getVotingManager not implemented");
    }


    /**
     *  Gets the manager associated with the Workflow service. 
     *
     *  @return the manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowManager getWorkflowManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getWorkflowManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Acknowledgement service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcknowledgementProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.AcknowledgementProxyManager getAcknowledgementProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getAcknowledgementProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Assessment service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentProxyManager getAssessmentProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getAssessmentProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Authentication service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthenticationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AuthenticationProxyManager getAuthenticationProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getAuthenticationProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Authorization service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuthorizationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authorization.AuthorizationProxyManager getAuthorizationProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getAuthorizationProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Bidding service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBiddingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.BiddingProxyManager getBiddingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getBiddingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Billing service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBillingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BillingProxyManager getBillingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getBillingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Blogging service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBloggingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.blogging.BloggingProxyManager getBloggingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getBloggingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Calendaring service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCalendaringProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendaringProxyManager getCalendaringProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCalendaringProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Cataloging service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogingProxyManager getCatalogingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCatalogingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Checklist service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistProxyManager getChecklistProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getChecklistProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Commenting service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentingProxyManager getCommentingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCommentingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Communication service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommunicationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.communication.CommunicationProxyManager getCommunicationProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCommunicationProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Configuration service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationProxyManager getConfigurationProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getConfigurationProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Contact service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsContactProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.ContactProxyManager getContactProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getContactProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Control service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControlProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControlProxyManager getControlProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getControlProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Course service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseProxyManager getCourseProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getCourseProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Dictionary service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDictionaryProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryProxyManager getDictionaryProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getDictionaryProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Filing service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFilingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FilingProxyManager getFilingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getFilingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Financials service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFinancialsProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.financials.FinancialsManager getFinancialsProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getFinancialsProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Forum service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsForumProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.forum.ForumProxyManager getForumProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getForumProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Grading service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradingProxyManager getGradingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getGradingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Hierarchy service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHierarchyProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hierarchy.HierarchyProxyManager getHierarchyProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getHierarchyProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Hold service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldManager getHoldProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getHoldProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Id service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIdProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdProxyManager getIdProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getIdProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Inquiry service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInquiryProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquiryProxyManager getInquiryProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getInquiryProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Installation service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.installation.InstallationProxyManager getInstallationProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getInstallationProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Inventory service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInventoryProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.inventory.InventoryProxyManager getInventoryProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getInventoryProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Journaling service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.id.IdProxyManager getJournalingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getJournalingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Learning service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLearningProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.LearningProxyManager getLearningProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getLearningProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Locale service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLocaleProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.locale.LocaleProxyManager getLocaleProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getLocaleProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Logging service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLoggingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingProxyManager getLoggingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getLoggingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Mapping service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMappingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MappingManager getMappingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getMappingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Messaging service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessagingProxyManager getMessagingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getMessagingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Metering service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeteringProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.metering.MeteringProxyManager getMeteringProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getMeteringProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Offering service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.OfferingProxyManager getOfferingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getOfferingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Ontology service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOntologyProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ontology.OntologyProxyManager getOntologyProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getOntologyProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Ordering service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.OrderingProxyManager getOrderingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getOrderingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Personnel service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPersonnelProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonnelProxyManager getPersonnelProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getPersonnelProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Process service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcessProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.process.ProcessProxyManager getProcessProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getProcessProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Profile service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProfileProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.profile.ProfileProxyManager getProfileProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getProfileProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Provisioning service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisioningProxyManager getProvisioningProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getProvisioningProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Proxy service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProxyProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.proxy.ProxyProxyManager getProxyProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getProxyProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Recipe service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeProxyManager getRecipeProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRecipeProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Recognition service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecognitionProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.RecognitionProxyManager getRecognitionProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRecognitionProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Relationship service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRelationshipProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.relationship.RelationshipProxyManager getRelationshipProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRelationshipProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Repository service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryProxyManager getRepositoryProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRepositoryProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Resource service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceProxyManager getResourceProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getResourceProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Resourcing service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourcingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.ResourcingProxyManager getResourcingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getResourcingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Rules service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRulesProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesProxyManager getRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getRulesProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Sequencing service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSequencingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.SequencingProxyManager getSequencingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getSequencingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Search service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSearchProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.search.SearchProxyManager getSearchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getSearchProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Subscription service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubscriptionProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionProxyManager getSubscriptionProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getSubscriptionProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Topology service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransportProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.TopologyProxyManager getTopologyProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTopologyProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Tracking service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTrackingProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.TrackingManager getTrackingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTrackingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Transaction service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransactionProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.transaction.TransactionProxyManager getTransactionProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTransactionProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Transport service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTransportProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.transport.TransportProxyManager getTransportProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTransportProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Type service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTopologyProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.type.TypeProxyManager getTypeProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getTypeProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Voting service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingProxyProvider() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.VotingProxyManager getVotingProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getVotingProxyManager not implemented");
    }


    /**
     *  Gets the proxy manager associated with the Workflow service. 
     *
     *  @return the proxy manager 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWorkflowProxyProvider() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.WorkflowProxyManager getWorkflowProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.orchestration.OrchestrationManager.getWorkflowProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
