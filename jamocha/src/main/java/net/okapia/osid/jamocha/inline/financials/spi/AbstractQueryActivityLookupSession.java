//
// AbstractQueryActivityLookupSession.java
//
//    An inline adapter that maps an ActivityLookupSession to
//    an ActivityQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.financials.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ActivityLookupSession to
 *  an ActivityQuerySession.
 */

public abstract class AbstractQueryActivityLookupSession
    extends net.okapia.osid.jamocha.financials.spi.AbstractActivityLookupSession
    implements org.osid.financials.ActivityLookupSession {

      private boolean effectiveonly = false;

    private final org.osid.financials.ActivityQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryActivityLookupSession.
     *
     *  @param querySession the underlying activity query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryActivityLookupSession(org.osid.financials.ActivityQuerySession querySession) {
        nullarg(querySession, "activity query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Business</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform <code>Activity</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivities() {
        return (this.session.canSearchActivities());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activities in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    

    /**
     *  Only activities whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveActivityView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All activities of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveActivityView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Activity</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Activity</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Activity</code> and
     *  retained for compatibility.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityId <code>Id</code> of the
     *          <code>Activity</code>
     *  @return the activity
     *  @throws org.osid.NotFoundException <code>activityId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.Activity getActivity(org.osid.id.Id activityId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();
        query.matchId(activityId, true);
        org.osid.financials.ActivityList activities = this.session.getActivitiesByQuery(query);
        if (activities.hasNext()) {
            return (activities.getNextActivity());
        } 
        
        throw new org.osid.NotFoundException(activityId + " not found");
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activities specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Activities</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, activities are returned that are currently effective.
     *  In any effective mode, effective activities and those currently expired
     *  are returned.
     *
     *  @param  activityIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>activityIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByIds(org.osid.id.IdList activityIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();

        try (org.osid.id.IdList ids = activityIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> which does not include
     *  activities of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently effective.
     *  In any effective mode, effective activities and those currently expired
     *  are returned.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();
        query.matchGenusType(activityGenusType, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> corresponding to the given
     *  activity genus <code>Type</code> and include any additional
     *  activities with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityGenusType an activity genus type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByParentGenusType(org.osid.type.Type activityGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();
        query.matchParentGenusType(activityGenusType, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> containing the given
     *  activity record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  activityRecordType an activity record type 
     *  @return the returned <code>Activity</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByRecordType(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();
        query.matchRecordType(activityRecordType, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code>ActivityList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible
     *  through this session.
     *  
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Activity</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesOnDate(org.osid.calendaring.DateTime from, 
                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getActivitiesByQuery(query));
    }
        
    
    /**
     *  Gets an <code> ActivityList </code> for the given
     *  organization.
     *  
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *  
     *  In effective mode, activities are returned that are currently
     *  effective. In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @return the returned <code> Activity </code> list 
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByOrganization(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();
        query.matchOrganizationId(resourceId, true);
        return (this.session.getActivitiesByQuery(query));
    }
        

    /**
     *  Gets an <code> ActivityList </code> for the giveb organization
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *  
     *  In effective mode, activities are returned that are currently
     *  effective. In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Activity </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> or 
     *          <code> activityRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByOrganizationOnDate(org.osid.id.Id resourceId, 
                                                                              org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();
        query.matchOrganizationId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets an <code> ActivityList </code> containing the given
     *  activity code.
     *  
     *   In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session.
     *  
     *  In effective mode, activities are returned that are currently
     *  effective. In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @param  code an activity code 
     *  @return the returned <code> Activity </code> list 
     *  @throws org.osid.NullArgumentException <code> code </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivitiesByCode(String code)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();
        query.matchCode(code, getStringMatchType(), true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets all <code>Activities</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  activities or an error results. Otherwise, the returned list
     *  may contain only those activities that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, activities are returned that are currently
     *  effective.  In any effective mode, effective activities and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Activities</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.ActivityList getActivities()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.financials.ActivityQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getActivitiesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.financials.ActivityQuery getQuery() {
        org.osid.financials.ActivityQuery query = this.session.getActivityQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }


    /**
     *  Gets the string match type for use in string queries.
     *
     *  @return a type
     */

    protected org.osid.type.Type getStringMatchType() {
        return (net.okapia.osid.primordium.types.search.StringMatchTypes.EXACT.getType());
    }
}
