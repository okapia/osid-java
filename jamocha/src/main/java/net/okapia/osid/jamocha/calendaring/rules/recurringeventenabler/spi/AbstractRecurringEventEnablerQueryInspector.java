//
// AbstractRecurringEventEnablerQueryInspector.java
//
//     A template for making a RecurringEventEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.recurringeventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for recurring event enablers.
 */

public abstract class AbstractRecurringEventEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.calendaring.rules.RecurringEventEnablerQueryInspector {

    private final java.util.Collection<org.osid.calendaring.rules.records.RecurringEventEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the recurring event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledRecurringEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the recurring event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.RecurringEventQueryInspector[] getRuledRecurringEventTerms() {
        return (new org.osid.calendaring.RecurringEventQueryInspector[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given recurring event enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a recurring event enabler implementing the requested record.
     *
     *  @param recurringEventEnablerRecordType a recurring event enabler record type
     *  @return the recurring event enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.RecurringEventEnablerQueryInspectorRecord getRecurringEventEnablerQueryInspectorRecord(org.osid.type.Type recurringEventEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.rules.records.RecurringEventEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(recurringEventEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recurring event enabler query. 
     *
     *  @param recurringEventEnablerQueryInspectorRecord recurring event enabler query inspector
     *         record
     *  @param recurringEventEnablerRecordType recurringEventEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRecurringEventEnablerQueryInspectorRecord(org.osid.calendaring.rules.records.RecurringEventEnablerQueryInspectorRecord recurringEventEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type recurringEventEnablerRecordType) {

        addRecordType(recurringEventEnablerRecordType);
        nullarg(recurringEventEnablerRecordType, "recurring event enabler record type");
        this.records.add(recurringEventEnablerQueryInspectorRecord);        
        return;
    }
}
