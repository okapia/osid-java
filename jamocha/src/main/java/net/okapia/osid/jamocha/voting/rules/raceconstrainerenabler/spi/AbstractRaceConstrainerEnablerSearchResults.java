//
// AbstractRaceConstrainerEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRaceConstrainerEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.voting.rules.RaceConstrainerEnablerSearchResults {

    private org.osid.voting.rules.RaceConstrainerEnablerList raceConstrainerEnablers;
    private final org.osid.voting.rules.RaceConstrainerEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRaceConstrainerEnablerSearchResults.
     *
     *  @param raceConstrainerEnablers the result set
     *  @param raceConstrainerEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnablers</code>
     *          or <code>raceConstrainerEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRaceConstrainerEnablerSearchResults(org.osid.voting.rules.RaceConstrainerEnablerList raceConstrainerEnablers,
                                            org.osid.voting.rules.RaceConstrainerEnablerQueryInspector raceConstrainerEnablerQueryInspector) {
        nullarg(raceConstrainerEnablers, "race constrainer enablers");
        nullarg(raceConstrainerEnablerQueryInspector, "race constrainer enabler query inspectpr");

        this.raceConstrainerEnablers = raceConstrainerEnablers;
        this.inspector = raceConstrainerEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the race constrainer enabler list resulting from a search.
     *
     *  @return a race constrainer enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablers() {
        if (this.raceConstrainerEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.voting.rules.RaceConstrainerEnablerList raceConstrainerEnablers = this.raceConstrainerEnablers;
        this.raceConstrainerEnablers = null;
	return (raceConstrainerEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.voting.rules.RaceConstrainerEnablerQueryInspector getRaceConstrainerEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  race constrainer enabler search record <code> Type. </code> This method must
     *  be used to retrieve a raceConstrainerEnabler implementing the requested
     *  record.
     *
     *  @param raceConstrainerEnablerSearchRecordType a raceConstrainerEnabler search 
     *         record type 
     *  @return the race constrainer enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(raceConstrainerEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerEnablerSearchResultsRecord getRaceConstrainerEnablerSearchResultsRecord(org.osid.type.Type raceConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.voting.rules.records.RaceConstrainerEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(raceConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(raceConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record race constrainer enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRaceConstrainerEnablerRecord(org.osid.voting.rules.records.RaceConstrainerEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "race constrainer enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
