//
// AbstractLeaseLookupSession.java
//
//    A starter implementation framework for providing a Lease
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Lease
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getLeases(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractLeaseLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.room.squatting.LeaseLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();
    

    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this 
     *  session.
     *
     *  @return the <code>Campus</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Lease</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupLeases() {
        return (true);
    }


    /**
     *  A complete view of the <code>Lease</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeLeaseView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Lease</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryLeaseView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include leases in campuses which are children
     *  of this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only leases whose effective dates are current are returned by
     *  methods in this session.
     */

    @OSID @Override
    public void useEffectiveLeaseView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All leases of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveLeaseView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Lease</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Lease</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Lease</code> and
     *  retained for compatibility.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @param  leaseId <code>Id</code> of the
     *          <code>Lease</code>
     *  @return the lease
     *  @throws org.osid.NotFoundException <code>leaseId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>leaseId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.Lease getLease(org.osid.id.Id leaseId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.room.squatting.LeaseList leases = getLeases()) {
            while (leases.hasNext()) {
                org.osid.room.squatting.Lease lease = leases.getNextLease();
                if (lease.getId().equals(leaseId)) {
                    return (lease);
                }
            }
        } 

        throw new org.osid.NotFoundException(leaseId + " not found");
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  leases specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Leases</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, leases are returned that are currently effective.
     *  In any effective mode, effective leases and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getLeases()</code>.
     *
     *  @param  leaseIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>leaseIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByIds(org.osid.id.IdList leaseIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.squatting.Lease> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = leaseIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getLease(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("lease " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.squatting.lease.LinkedLeaseList(ret));
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given
     *  lease genus <code>Type</code> which does not include
     *  leases of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently effective.
     *  In any effective mode, effective leases and those currently expired
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getLeases()</code>.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusType(org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseGenusFilterList(getLeases(), leaseGenusType));
    }


    /**
     *  Gets a <code>LeaseList</code> corresponding to the given
     *  lease genus <code>Type</code> and include any additional
     *  leases with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getLeases()</code>.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByParentGenusType(org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getLeasesByGenusType(leaseGenusType));
    }


    /**
     *  Gets a <code>LeaseList</code> containing the given
     *  lease record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getLeases()</code>.
     *
     *  @param  leaseRecordType a lease record type 
     *  @return the returned <code>Lease</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>leaseRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByRecordType(org.osid.type.Type leaseRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseRecordFilterList(getLeases(), leaseRecordType));
    }


    /**
     *  Gets a <code>LeaseList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *  
     *  In active mode, leases are returned that are currently
     *  active. In any status mode, active and inactive leases
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Lease</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesOnDate(org.osid.calendaring.DateTime from, 
                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.TemporalLeaseFilterList(getLeases(), from, to));
    }
        

    /**
     *  Gets a list of all leases of a lease genus type effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>LeaseList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>leaseGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeOnDate(org.osid.type.Type leaseGenusType, 
                                                                        org.osid.calendaring.DateTime from, 
                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.TemporalLeaseFilterList(getLeasesByGenusType(leaseGenusType), from, to));
    }        


    /**
     *  Gets a list of leases corresponding to a room
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.LeaseList getLeasesForRoom(org.osid.id.Id roomId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseFilterList(new RoomFilter(roomId), getLeases()));
    }


    /**
     *  Gets a list of all leases corresponding to a genus type and
     *  date range. Leases are returned with start effective dates
     *  that fall between the requested dates inclusive.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code> 
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>LeaseList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code> or
     *          <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoom(org.osid.id.Id roomId, 
                                                                         org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseGenusFilterList(getLeasesForRoom(roomId), leaseGenusType));
    }


    /**
     *  Gets a list of leases corresponding to a room
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomOnDate(org.osid.id.Id roomId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.TemporalLeaseFilterList(getLeasesForRoom(roomId), from, to));
    }


    /**
     *  Gets a list of all leases for a room and of a lease genus type and 
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code> 
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>LeaseList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>leaseGenusType</code>, <code>from</code>, or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomOnDate(org.osid.id.Id roomId, 
                                                                               org.osid.type.Type leaseGenusType, 
                                                                               org.osid.calendaring.DateTime from, 
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseGenusFilterList(getLeasesForRoomOnDate(roomId, from, to), leaseGenusType));
    }        
        

    /**
     *  Gets a list of leases corresponding to a tenant
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.room.squatting.LeaseList getLeasesForTenant(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseFilterList(new TenantFilter(resourceId), getLeases()));
    }


    /**
     *  Gets a list of all leases corresponding to a tenant
     *  <code>Id</code> and date range. Leases are returned with start
     *  effective dates that fall between the requested dates
     *  inclusive.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resourceId <code>Id</code> 
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>LeaseList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForTenant(org.osid.id.Id resourceId, 
                                                                           org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseGenusFilterList(getLeasesForTenant(resourceId), leaseGenusType));
    }        


    /**
     *  Gets a list of leases corresponding to a tenant
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForTenantOnDate(org.osid.id.Id resourceId,
                                                                      org.osid.calendaring.DateTime from,
                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.TemporalLeaseFilterList(getLeasesForTenant(resourceId), from, to));
    }


    /**
     *  Gets a list of all leases for a tenant and of a lease genus
     *  type effective during the entire given date range inclusive
     *  but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>LeaseList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code>, <code>leaseGenusType</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForTenantOnDate(org.osid.id.Id resourceId, 
                                                                                 org.osid.type.Type leaseGenusType, 
                                                                                 org.osid.calendaring.DateTime from, 
                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseGenusFilterList(getLeasesForTenantOnDate(resourceId, from, to), leaseGenusType));
    }


    /**
     *  Gets a list of leases corresponding to room and tenant
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomAndTenant(org.osid.id.Id roomId,
                                                                        org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseFilterList(new TenantFilter(resourceId), getLeasesForRoom(roomId)));
    }


    /**
     *  Gets a list of all leases corresponding for a room, tenant,
     *  and a lease genus type.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code> 
     *  @param  resourceId a tenant <code>Id</code> 
     *  @param  leaseGenusType a lease genus type 
     *  @return the returned <code>LeaseList</code> 
     *  @throws org.osid.InvalidArgumentException <code>to</code> is
     *          less than <code>from</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>resourceId</code>, or
     *          <code>leaseGenusType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomAndTenant(org.osid.id.Id roomId, 
                                                                                  org.osid.id.Id resourceId, 
                                                                                  org.osid.type.Type leaseGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseGenusFilterList(getLeasesForRoomAndTenant(roomId, resourceId), leaseGenusType));
    }


    /**
     *  Gets a list of leases corresponding to room and tenant
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible
     *  through this session.
     *
     *  In effective mode, leases are returned that are
     *  currently effective.  In any effective mode, effective
     *  leases and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the tenant
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>LeaseList</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesForRoomAndTenantOnDate(org.osid.id.Id roomId,
                                                                              org.osid.id.Id resourceId,
                                                                              org.osid.calendaring.DateTime from,
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.TemporalLeaseFilterList(getLeasesForRoomAndTenant(roomId, resourceId), from, to));
    }


    /**
     *  Gets a list of all leases for a roomm tenant, and of a genus
     *  type effective during the entire given date range inclusive
     *  but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known leases
     *  or an error results. Otherwise, the returned list may contain
     *  only those leases that are accessible through this session.
     *  
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and those
     *  currently expired are returned.
     *
     *  @param  roomId a room <code>Id</code> 
     *  @param  resourceId a resource <code>Id</code> 
     *  @param  leaseGenusType a lease genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>LeaseList</code> 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>roomId</code>,
     *          <code>resourceId</code>, <code>leaseGenusType</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.squatting.LeaseList getLeasesByGenusTypeForRoomAndTenantOnDate(org.osid.id.Id roomId, 
                                                                                        org.osid.id.Id resourceId, 
                                                                                        org.osid.type.Type leaseGenusType, 
                                                                                        org.osid.calendaring.DateTime from, 
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseGenusFilterList(getLeasesForRoomAndTenantOnDate(roomId, resourceId, from, to), leaseGenusType));
    }



    /**
     *  Gets all <code>Leases</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  leases or an error results. Otherwise, the returned list
     *  may contain only those leases that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, leases are returned that are currently
     *  effective.  In any effective mode, effective leases and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Leases</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.room.squatting.LeaseList getLeases()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the lease list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of leases
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.room.squatting.LeaseList filterLeasesOnViews(org.osid.room.squatting.LeaseList list)
        throws org.osid.OperationFailedException {

        org.osid.room.squatting.LeaseList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.room.squatting.lease.EffectiveLeaseFilterList(ret);
        }

        return (ret);
    }

    public static class RoomFilter
        implements net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseFilter {
         
        private final org.osid.id.Id roomId;
         
         
        /**
         *  Constructs a new <code>RoomFilter</code>.
         *
         *  @param roomId the room to filter
         *  @throws org.osid.NullArgumentException
         *          <code>roomId</code> is <code>null</code>
         */
        
        public RoomFilter(org.osid.id.Id roomId) {
            nullarg(roomId, "room Id");
            this.roomId = roomId;
            return;
        }

         
        /**
         *  Used by the LeaseFilterList to filter the 
         *  lease list based on room.
         *
         *  @param lease the lease
         *  @return <code>true</code> to pass the lease,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.squatting.Lease lease) {
            return (lease.getRoomId().equals(this.roomId));
        }
    }


    public static class TenantFilter
        implements net.okapia.osid.jamocha.inline.filter.room.squatting.lease.LeaseFilter {
         
        private final org.osid.id.Id resourceId;
         
         
        /**
         *  Constructs a new <code>TenantFilter</code>.
         *
         *  @param resourceId the tenant to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */
        
        public TenantFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "tenant Id");
            this.resourceId = resourceId;
            return;
        }

         
        /**
         *  Used by the LeaseFilterList to filter the 
         *  lease list based on tenant.
         *
         *  @param lease the lease
         *  @return <code>true</code> to pass the lease,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.squatting.Lease lease) {
            return (lease.getTenantId().equals(this.resourceId));
        }
    }
}
