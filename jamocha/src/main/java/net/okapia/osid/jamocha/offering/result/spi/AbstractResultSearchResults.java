//
// AbstractResultSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.result.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractResultSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.offering.ResultSearchResults {

    private org.osid.offering.ResultList results;
    private final org.osid.offering.ResultQueryInspector inspector;
    private final java.util.Collection<org.osid.offering.records.ResultSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractResultSearchResults.
     *
     *  @param results the result set
     *  @param resultQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>results</code>
     *          or <code>resultQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractResultSearchResults(org.osid.offering.ResultList results,
                                            org.osid.offering.ResultQueryInspector resultQueryInspector) {
        nullarg(results, "results");
        nullarg(resultQueryInspector, "result query inspectpr");

        this.results = results;
        this.inspector = resultQueryInspector;

        return;
    }


    /**
     *  Gets the result list resulting from a search.
     *
     *  @return a result list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.offering.ResultList getResults() {
        if (this.results == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.offering.ResultList results = this.results;
        this.results = null;
	return (results);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.offering.ResultQueryInspector getResultQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  result search record <code> Type. </code> This method must
     *  be used to retrieve a result implementing the requested
     *  record.
     *
     *  @param resultSearchRecordType a result search 
     *         record type 
     *  @return the result search
     *  @throws org.osid.NullArgumentException
     *          <code>resultSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(resultSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ResultSearchResultsRecord getResultSearchResultsRecord(org.osid.type.Type resultSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.offering.records.ResultSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(resultSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(resultSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record result search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addResultRecord(org.osid.offering.records.ResultSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "result record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
