//
// RouteProgressMiter.java
//
//     Defines a RouteProgress miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.route.routeprogress;


/**
 *  Defines a <code>RouteProgress</code> miter for use with the builders.
 */

public interface RouteProgressMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidCompendiumMiter,
            org.osid.mapping.route.RouteProgress {


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    public void setResource(org.osid.resource.Resource resource);


    /**
     *  Sets the route.
     *
     *  @param route a route
     *  @throws org.osid.NullArgumentException
     *          <code>route</code> is <code>null</code>
     */

    public void setRoute(org.osid.mapping.route.Route route);


    /**
     *  Sets the time started.
     *
     *  @param time a time started
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setTimeStarted(org.osid.calendaring.DateTime time);


    /**
     *  Sets the total distance traveled.
     *
     *  @param distance a total distance traveled
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public void setTotalDistanceTraveled(org.osid.mapping.Distance distance);


    /**
     *  Sets the total travel time.
     *
     *  @param duration a total travel time
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setTotalTravelTime(org.osid.calendaring.Duration duration);


    /**
     *  Sets the total idle time.
     *
     *  @param duration a total idle time
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setTotalIdleTime(org.osid.calendaring.Duration duration);


    /**
     *  Sets the time last moved.
     *
     *  @param time a time last moved
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setTimeLastMoved(org.osid.calendaring.DateTime time);


    /**
     *  Sets the route segment.
     *
     *  @param routeSegment a route segment
     *  @throws org.osid.NullArgumentException
     *          <code>routeSegment</code> is <code>null</code>
     */

    public void setRouteSegment(org.osid.mapping.route.RouteSegment routeSegment);


    /**
     *  Sets the estimated travel time to next segment.
     *
     *  @param eta the estimated travel time to next segment
     *  @throws org.osid.NullArgumentException <code>eta</code> is
     *          <code>null</code>
     */

    public void setETAToNextSegment(org.osid.calendaring.Duration eta);


    /**
     *  Sets the route segment traveled.
     *
     *  @param distance a route segment traveled
     *  @throws org.osid.NullArgumentException
     *          <code>distance</code> is <code>null</code>
     */

    public void setRouteSegmentTraveled(org.osid.mapping.Distance distance);


    /**
     *  Sets the time completed.
     *
     *  @param time a time completed
     *  @throws org.osid.NullArgumentException
     *          <code>time</code> is <code>null</code>
     */

    public void setTimeCompleted(org.osid.calendaring.DateTime time);


    /**
     *  Adds a RouteProgress record.
     *
     *  @param record a routeProgress record
     *  @param recordType the type of routeProgress record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRouteProgressRecord(org.osid.mapping.route.records.RouteProgressRecord record, org.osid.type.Type recordType);
}       


