//
// MutableIndexedMapCanonicalUnitEnablerLookupSession
//
//    Implements a CanonicalUnitEnabler lookup service backed by a collection of
//    canonicalUnitEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.rules;


/**
 *  Implements a CanonicalUnitEnabler lookup service backed by a collection of
 *  canonical unit enablers. The canonical unit enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some canonical unit enablers may be compatible
 *  with more types than are indicated through these canonical unit enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of canonical unit enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCanonicalUnitEnablerLookupSession
    extends net.okapia.osid.jamocha.core.offering.rules.spi.AbstractIndexedMapCanonicalUnitEnablerLookupSession
    implements org.osid.offering.rules.CanonicalUnitEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCanonicalUnitEnablerLookupSession} with no canonical unit enablers.
     *
     *  @param catalogue the catalogue
     *  @throws org.osid.NullArgumentException {@code catalogue}
     *          is {@code null}
     */

      public MutableIndexedMapCanonicalUnitEnablerLookupSession(org.osid.offering.Catalogue catalogue) {
        setCatalogue(catalogue);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCanonicalUnitEnablerLookupSession} with a
     *  single canonical unit enabler.
     *  
     *  @param catalogue the catalogue
     *  @param  canonicalUnitEnabler a single canonicalUnitEnabler
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitEnabler} is {@code null}
     */

    public MutableIndexedMapCanonicalUnitEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.rules.CanonicalUnitEnabler canonicalUnitEnabler) {
        this(catalogue);
        putCanonicalUnitEnabler(canonicalUnitEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCanonicalUnitEnablerLookupSession} using an
     *  array of canonical unit enablers.
     *
     *  @param catalogue the catalogue
     *  @param  canonicalUnitEnablers an array of canonical unit enablers
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitEnablers} is {@code null}
     */

    public MutableIndexedMapCanonicalUnitEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                                  org.osid.offering.rules.CanonicalUnitEnabler[] canonicalUnitEnablers) {
        this(catalogue);
        putCanonicalUnitEnablers(canonicalUnitEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCanonicalUnitEnablerLookupSession} using a
     *  collection of canonical unit enablers.
     *
     *  @param catalogue the catalogue
     *  @param  canonicalUnitEnablers a collection of canonical unit enablers
     *  @throws org.osid.NullArgumentException {@code catalogue} or
     *          {@code canonicalUnitEnablers} is {@code null}
     */

    public MutableIndexedMapCanonicalUnitEnablerLookupSession(org.osid.offering.Catalogue catalogue,
                                                  java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitEnabler> canonicalUnitEnablers) {

        this(catalogue);
        putCanonicalUnitEnablers(canonicalUnitEnablers);
        return;
    }
    

    /**
     *  Makes a {@code CanonicalUnitEnabler} available in this session.
     *
     *  @param  canonicalUnitEnabler a canonical unit enabler
     *  @throws org.osid.NullArgumentException {@code canonicalUnitEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putCanonicalUnitEnabler(org.osid.offering.rules.CanonicalUnitEnabler canonicalUnitEnabler) {
        super.putCanonicalUnitEnabler(canonicalUnitEnabler);
        return;
    }


    /**
     *  Makes an array of canonical unit enablers available in this session.
     *
     *  @param  canonicalUnitEnablers an array of canonical unit enablers
     *  @throws org.osid.NullArgumentException {@code canonicalUnitEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putCanonicalUnitEnablers(org.osid.offering.rules.CanonicalUnitEnabler[] canonicalUnitEnablers) {
        super.putCanonicalUnitEnablers(canonicalUnitEnablers);
        return;
    }


    /**
     *  Makes collection of canonical unit enablers available in this session.
     *
     *  @param  canonicalUnitEnablers a collection of canonical unit enablers
     *  @throws org.osid.NullArgumentException {@code canonicalUnitEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putCanonicalUnitEnablers(java.util.Collection<? extends org.osid.offering.rules.CanonicalUnitEnabler> canonicalUnitEnablers) {
        super.putCanonicalUnitEnablers(canonicalUnitEnablers);
        return;
    }


    /**
     *  Removes a CanonicalUnitEnabler from this session.
     *
     *  @param canonicalUnitEnablerId the {@code Id} of the canonical unit enabler
     *  @throws org.osid.NullArgumentException {@code canonicalUnitEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCanonicalUnitEnabler(org.osid.id.Id canonicalUnitEnablerId) {
        super.removeCanonicalUnitEnabler(canonicalUnitEnablerId);
        return;
    }    
}
