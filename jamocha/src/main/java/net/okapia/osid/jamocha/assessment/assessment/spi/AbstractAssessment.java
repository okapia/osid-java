//
// AbstractAssessment.java
//
//     Defines an Assessment.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Assessment</code>.
 */

public abstract class AbstractAssessment
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.assessment.Assessment {

    private org.osid.grading.Grade level;
    private org.osid.assessment.Assessment rubric;

    private final java.util.Collection<org.osid.assessment.records.AssessmentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of a <code> Grade </code> corresponding to 
     *  the assessment difficulty. 
     *
     *  @return a grade <code> id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLevelId() {
        return (this.level.getId());
    }


    /**
     *  Gets the <code> Grade </code> corresponding to the assessment 
     *  difficulty. 
     *
     *  @return the level 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getLevel()
        throws org.osid.OperationFailedException {

        return (this.level);
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    protected void setLevel(org.osid.grading.Grade level) {
        nullarg(level, "level");
        this.level = level;
        return;
    }


    /**
     *  Tests if a rubric assessment is associated with this assessment. 
     *
     *  @return <code> true </code> if a rubric is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasRubric() {
        if (this.rubric == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the <code> Id </code> of the rubric. 
     *
     *  @return an assessment <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRubricId() {
        if (!hasRubric()) {
            throw new org.osid.IllegalStateException("hasRubric() is false");
        }

        return (this.rubric.getId());
    }


    /**
     *  Gets the rubric. 
     *
     *  @return the assessment 
     *  @throws org.osid.IllegalStateException <code> hasRubric() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getRubric()
        throws org.osid.OperationFailedException {

        if (!hasRubric()) {
            throw new org.osid.IllegalStateException("hasRubric() is false");
        }

        return (this.rubric);
    }


    /**
     *  Sets the rubric.
     *
     *  @param assessment the rubric assessment
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    protected void setRubric(org.osid.assessment.Assessment assessment) {
        nullarg(assessment, "rubric assessment");
        this.rubric = assessment;
        return;
    }


    /**
     *  Tests if this assessment supports the given record
     *  <code>Type</code>.
     *
     *  @param  assessmentRecordType an assessment record type 
     *  @return <code>true</code> if the assessmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type assessmentRecordType) {
        for (org.osid.assessment.records.AssessmentRecord record : this.records) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Assessment</code> record <code>Type</code>.
     *
     *  @param  assessmentRecordType the assessment record type 
     *  @return the assessment record 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable
     *          to complete request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentRecord getAssessmentRecord(org.osid.type.Type assessmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AssessmentRecord record : this.records) {
            if (record.implementsRecordType(assessmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param assessmentRecord the assessment record
     *  @param assessmentRecordType assessment record type
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentRecord</code> or
     *          <code>assessmentRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addAssessmentRecord(org.osid.assessment.records.AssessmentRecord assessmentRecord, 
                                       org.osid.type.Type assessmentRecordType) {

        nullarg(assessmentRecord, "assessment record");
        addRecordType(assessmentRecordType);
        this.records.add(assessmentRecord);
        
        return;
    }
}
