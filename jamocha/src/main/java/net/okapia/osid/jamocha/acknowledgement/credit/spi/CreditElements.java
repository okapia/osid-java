//
// CreditElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.credit.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class CreditElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the CreditElement Id.
     *
     *  @return the credit element Id
     */

    public static org.osid.id.Id getCreditEntityId() {
        return (makeEntityId("osid.acknowledgement.Credit"));
    }


    /**
     *  Gets the ReferenceId element Id.
     *
     *  @return the ReferenceId element Id
     */

    public static org.osid.id.Id getReferenceId() {
        return (makeElementId("osid.acknowledgement.credit.ReferenceId"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.acknowledgement.credit.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.acknowledgement.credit.Resource"));
    }


    /**
     *  Gets the BillingId element Id.
     *
     *  @return the BillingId element Id
     */

    public static org.osid.id.Id getBillingId() {
        return (makeQueryElementId("osid.acknowledgement.credit.BillingId"));
    }


    /**
     *  Gets the Billing element Id.
     *
     *  @return the Billing element Id
     */

    public static org.osid.id.Id getBilling() {
        return (makeQueryElementId("osid.acknowledgement.credit.Billing"));
    }


    /**
     *  Gets the Reference element Id.
     *
     *  @return the Reference element Id
     */

    public static org.osid.id.Id getReference() {
        return (makeSearchOrderElementId("osid.acknowledgement.credit.Reference"));
    }
}
