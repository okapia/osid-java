//
// AbstractResponse.java
//
//     Defines a Response.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transport.response.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.locale.text.eng.us.Plain;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Response</code>.
 */

public abstract class AbstractResponse
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.transport.Response {

    private boolean ok = false;
    private org.osid.locale.DisplayText statusMessage = Plain.valueOf("");

    private final java.util.Collection<org.osid.transport.records.ResponseRecord> records = new java.util.LinkedHashSet<>();

    private final Browsable browsable = new Browsable();

    
    /**
     *  Tests if the request was successful. 
     *
     *  @return <code> true </code> if the request was successful, <code> 
     *          false </code> otherrwise 
     */

    @OSID @Override
    public boolean ok() {
        return (this.ok);
    }


    /**
     *  Sets the ok flag.
     *
     *  @param ok an ok
     */

    protected void setOk(boolean ok) {
        this.ok = ok;
        return;
    }


    /**
     *  Gets a status message. The returned string may be empty or may contain 
     *  an error message is an error occurred. 
     *
     *  @return an error message 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getStatusMessage() {
        return (this.statusMessage);
    }


    /**
     *  Sets the status message.
     *
     *  @param message a status message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    protected void setStatusMessage(org.osid.locale.DisplayText message) {
        nullarg(message, "status message");
        this.statusMessage = message;
        return;
    }


    /**
     *  Tests if this response supports the given record
     *  <code>Type</code>.
     *
     *  @param  responseRecordType a response record type 
     *  @return <code>true</code> if the responseRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type responseRecordType) {
        for (org.osid.transport.records.ResponseRecord record : this.records) {
            if (record.implementsRecordType(responseRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.browsable.getRecordTypes());
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.browsable.addRecordType(recordType);
        return;
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Response</code> record <code>Type</code>.
     *
     *  @param  responseRecordType the response record type 
     *  @return the response record 
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(responseRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.transport.records.ResponseRecord getResponseRecord(org.osid.type.Type responseRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.transport.records.ResponseRecord record : this.records) {
            if (record.implementsRecordType(responseRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(responseRecordType + " is not supported");
    }


    /**
     *  Adds a record to this response. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param responseRecord the response record
     *  @param responseRecordType response record type
     *  @throws org.osid.NullArgumentException
     *          <code>responseRecord</code> or
     *          <code>responseRecordTyperesponse</code> is
     *          <code>null</code>
     */
            
    protected void addResponseRecord(org.osid.transport.records.ResponseRecord responseRecord, 
                                     org.osid.type.Type responseRecordType) {

        nullarg(responseRecord, "response record");
        this.browsable.addRecordType(responseRecordType);
        this.records.add(responseRecord);
        
        return;
    }


    /**
     *  Gets a list of all properties of this object including those
     *  corresponding to data within this object's records. Properties
     *  provide a means for applications to display a representation
     *  of the contents of an object without understanding its record
     *  interface specifications. Applications needing to examine a
     *  specific property or perform updates should use the methods
     *  defined by the object's record <code> Type. </code>
     *
     *  @return a list of properties 
     */
    
    @OSID @Override
    public org.osid.PropertyList getProperties()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.browsable.getProperties());
    }


    /**
     *  Gets the properties by record type.
     *
     *  @param  recordType the record type corresponding to the
     *          properties set to retrieve
     *  @return a list of properties 
     *  @throws org.osid.NullArgumentException <code> recordType
     *          </code> is <code> null </code>
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(recordType) </code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.PropertyList getPropertiesByRecordType(org.osid.type.Type recordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.browsable.getPropertiesByRecordType(recordType));
    }

    
    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    protected void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        this.browsable.addProperties(set, recordType);
        return;
    }


    protected class Browsable
        extends net.okapia.osid.jamocha.spi.AbstractBrowsable
        implements org.osid.Browsable {


        /**
         *  Adds a record type.
         *
         *  @param recordType
         *  @throws org.osid.NullArgumentException <code>recordType</code>
         *          is <code>null</code>
         */
        
        protected void addRecordType(org.osid.type.Type recordType) {
            super.addRecordType(recordType);
            return;
        }


        /**
         *  Adds a property set.
         *
         *  @param set set of properties
         *  @param recordType associated type
         *  @throws org.osid.NullArgumentException <code>set</code> or
         *          <code>recordType</code> is <code>null</code>
         */
        
        protected void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
            super.addProperties(set, recordType);
            return;
        }
    }
}
