//
// AbstractImmutableCompetency.java
//
//     Wraps a mutable Competency to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.competency.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Competency</code> to hide modifiers. This
 *  wrapper provides an immutized Competency from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying competency whose state changes are visible.
 */

public abstract class AbstractImmutableCompetency
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.resourcing.Competency {

    private final org.osid.resourcing.Competency competency;


    /**
     *  Constructs a new <code>AbstractImmutableCompetency</code>.
     *
     *  @param competency the competency to immutablize
     *  @throws org.osid.NullArgumentException <code>competency</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCompetency(org.osid.resourcing.Competency competency) {
        super(competency);
        this.competency = competency;
        return;
    }


    /**
     *  Tests if learning objectives are available for this competency. 
     *
     *  @return true if learning objectives are available, false otherwise 
     */

    @OSID @Override
    public boolean hasLearningObjectives() {
        return (this.competency.hasLearningObjectives());
    }


    /**
     *  Gets the Ids of the learning objectives. 
     *
     *  @return the learning objective <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getLearningObjectiveIds() {
        return (this.competency.getLearningObjectiveIds());
    }


    /**
     *  Gets the learning objectives. 
     *
     *  @return the learning objectives 
     *  @throws org.osid.IllegalStateException <code> hasLearningObjectives() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveList getLearningObjectives()
        throws org.osid.OperationFailedException {

        return (this.competency.getLearningObjectives());
    }


    /**
     *  Gets the competency record corresponding to the given <code> 
     *  Competency </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  competencyRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(competencyRecordType) </code> is <code> true </code> . 
     *
     *  @param  competencyRecordType the type of competency record to retrieve 
     *  @return the competency record 
     *  @throws org.osid.NullArgumentException <code> competencyRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(competencyRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.records.CompetencyRecord getCompetencyRecord(org.osid.type.Type competencyRecordType)
        throws org.osid.OperationFailedException {

        return (this.competency.getCompetencyRecord(competencyRecordType));
    }
}

