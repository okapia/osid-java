//
// AbstractAssemblyPaymentQuery.java
//
//     A PaymentQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.billing.payment.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PaymentQuery that stores terms.
 */

public abstract class AbstractAssemblyPaymentQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.billing.payment.PaymentQuery,
               org.osid.billing.payment.PaymentQueryInspector,
               org.osid.billing.payment.PaymentSearchOrder {

    private final java.util.Collection<org.osid.billing.payment.records.PaymentQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.payment.records.PaymentQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.billing.payment.records.PaymentSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPaymentQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPaymentQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the payer <code> Id </code> for this query. 
     *
     *  @param  payerId a payer <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> payerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPayerId(org.osid.id.Id payerId, boolean match) {
        getAssembler().addIdTerm(getPayerIdColumn(), payerId, match);
        return;
    }


    /**
     *  Clears the payer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPayerIdTerms() {
        getAssembler().clearTerms(getPayerIdColumn());
        return;
    }


    /**
     *  Gets the payer <code> Id </code> query terms. 
     *
     *  @return the payer <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPayerIdTerms() {
        return (getAssembler().getIdTerms(getPayerIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the payer. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPayer(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPayerColumn(), style);
        return;
    }


    /**
     *  Gets the PayerId column name.
     *
     * @return the column name
     */

    protected String getPayerIdColumn() {
        return ("payer_id");
    }


    /**
     *  Tests if a <code> PayerQuery </code> is available. 
     *
     *  @return <code> true </code> if a payer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a payer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the payer query 
     *  @throws org.osid.UnimplementedException <code> supportsPayerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQuery getPayerQuery() {
        throw new org.osid.UnimplementedException("supportsPayerQuery() is false");
    }


    /**
     *  Clears the payer terms. 
     */

    @OSID @Override
    public void clearPayerTerms() {
        getAssembler().clearTerms(getPayerColumn());
        return;
    }


    /**
     *  Gets the payer query terms. 
     *
     *  @return the payer query terms 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerQueryInspector[] getPayerTerms() {
        return (new org.osid.billing.payment.PayerQueryInspector[0]);
    }


    /**
     *  Tests if a payer search order is available. 
     *
     *  @return <code> true </code> if a payer search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPayerSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the payer. 
     *
     *  @return the payer search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPayerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.payment.PayerSearchOrder getPayerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPayerSearchOrder() is false");
    }


    /**
     *  Gets the Payer column name.
     *
     * @return the column name
     */

    protected String getPayerColumn() {
        return ("payer");
    }


    /**
     *  Sets the customer <code> Id </code> for this query. 
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> customerId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCustomerId(org.osid.id.Id customerId, boolean match) {
        getAssembler().addIdTerm(getCustomerIdColumn(), customerId, match);
        return;
    }


    /**
     *  Clears the customer <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCustomerIdTerms() {
        getAssembler().clearTerms(getCustomerIdColumn());
        return;
    }


    /**
     *  Gets the customer <code> Id </code> query terms. 
     *
     *  @return the customer <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCustomerIdTerms() {
        return (getAssembler().getIdTerms(getCustomerIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the customer. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCustomer(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCustomerColumn(), style);
        return;
    }


    /**
     *  Gets the CustomerId column name.
     *
     * @return the column name
     */

    protected String getCustomerIdColumn() {
        return ("customer_id");
    }


    /**
     *  Tests if a <code> CustomereQuery </code> is available. 
     *
     *  @return <code> true </code> if a customer query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a customer. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a customer query 
     *  @throws org.osid.UnimplementedException <code> supportsCustomerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerQuery getCustomerQuery() {
        throw new org.osid.UnimplementedException("supportsCustomerQuery() is false");
    }


    /**
     *  Clears the customer terms. 
     */

    @OSID @Override
    public void clearCustomerTerms() {
        getAssembler().clearTerms(getCustomerColumn());
        return;
    }


    /**
     *  Gets the customer query terms. 
     *
     *  @return the customer query terms 
     */

    @OSID @Override
    public org.osid.billing.CustomerQueryInspector[] getCustomerTerms() {
        return (new org.osid.billing.CustomerQueryInspector[0]);
    }


    /**
     *  Tests if a customer search order is available. 
     *
     *  @return <code> true </code> if a customer search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCustomerSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the customer. 
     *
     *  @return the customer search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCustomerSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.CustomerSearchOrder getCustomerSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCustomerSearchOrder() is false");
    }


    /**
     *  Gets the Customer column name.
     *
     * @return the column name
     */

    protected String getCustomerColumn() {
        return ("customer");
    }


    /**
     *  Sets the biliing period <code> Id </code> for this query. 
     *
     *  @param  periodId a period <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> periodId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPeriodId(org.osid.id.Id periodId, boolean match) {
        getAssembler().addIdTerm(getPeriodIdColumn(), periodId, match);
        return;
    }


    /**
     *  Clears the billing period <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPeriodIdTerms() {
        getAssembler().clearTerms(getPeriodIdColumn());
        return;
    }


    /**
     *  Gets the period <code> Id </code> query terms. 
     *
     *  @return the period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPeriodIdTerms() {
        return (getAssembler().getIdTerms(getPeriodIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the billing 
     *  period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPeriod(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPeriodColumn(), style);
        return;
    }


    /**
     *  Gets the PeriodId column name.
     *
     * @return the column name
     */

    protected String getPeriodIdColumn() {
        return ("period_id");
    }


    /**
     *  Tests if a <code> PeriodQuery </code> is available. 
     *
     *  @return <code> true </code> if a period query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodQuery() {
        return (false);
    }


    /**
     *  Gets the query for a billing period. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the period query 
     *  @throws org.osid.UnimplementedException <code> supportsPeriodQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodQuery getPeriodQuery() {
        throw new org.osid.UnimplementedException("supportsPeriodQuery() is false");
    }


    /**
     *  Matches payments with any billing period. 
     *
     *  @param  match <code> true </code> to match payments with any period, 
     *          <code> false </code> to match peyments with no period 
     */

    @OSID @Override
    public void matchAnyPeriod(boolean match) {
        getAssembler().addIdWildcardTerm(getPeriodColumn(), match);
        return;
    }


    /**
     *  Clears the billing period terms. 
     */

    @OSID @Override
    public void clearPeriodTerms() {
        getAssembler().clearTerms(getPeriodColumn());
        return;
    }


    /**
     *  Gets the period query terms. 
     *
     *  @return the period query terms 
     */

    @OSID @Override
    public org.osid.billing.PeriodQueryInspector[] getPeriodTerms() {
        return (new org.osid.billing.PeriodQueryInspector[0]);
    }


    /**
     *  Tests if a billing period search order is available. 
     *
     *  @return <code> true </code> if a period search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPeriodSearchOrder() {
        return (false);
    }


    /**
     *  Specifies a preference for ordering the result set by the billing 
     *  period. 
     *
     *  @return the period search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPeriodSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.PeriodSearchOrder getPeriodSearchOrder() {
        throw new org.osid.UnimplementedException("supportsPeriodSearchOrder() is false");
    }


    /**
     *  Gets the Period column name.
     *
     * @return the column name
     */

    protected String getPeriodColumn() {
        return ("period");
    }


    /**
     *  Matches payment dates between the given date range inclusive. 
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchPaymentDate(org.osid.calendaring.DateTime from, 
                                 org.osid.calendaring.DateTime to, 
                                 boolean match) {
        getAssembler().addDateTimeRangeTerm(getPaymentDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the payment date terms. 
     */

    @OSID @Override
    public void clearPaymentDateTerms() {
        getAssembler().clearTerms(getPaymentDateColumn());
        return;
    }


    /**
     *  Gets the payment date query terms. 
     *
     *  @return the payment date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getPaymentDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getPaymentDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the payment 
     *  date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByPaymentDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getPaymentDateColumn(), style);
        return;
    }


    /**
     *  Gets the PaymentDate column name.
     *
     * @return the column name
     */

    protected String getPaymentDateColumn() {
        return ("payment_date");
    }


    /**
     *  Matches process dates between the given date range inclusive. 
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchProcessDate(org.osid.calendaring.DateTime from, 
                                   org.osid.calendaring.DateTime to, 
                                   boolean match) {
        getAssembler().addDateTimeRangeTerm(getProcessDateColumn(), from, to, match);
        return;
    }


    /**
     *  Matches payments with any process date. 
     *
     *  @param  match <code> true </code> to match payments with any process 
     *          date, <code> false </code> to match peyments with no process 
     *          date 
     */

    @OSID @Override
    public void matchAnyProcessDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getProcessDateColumn(), match);
        return;
    }


    /**
     *  Clears the process date terms. 
     */

    @OSID @Override
    public void clearProcessDateTerms() {
        getAssembler().clearTerms(getProcessDateColumn());
        return;
    }


    /**
     *  Gets the process date query terms. 
     *
     *  @return the process date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getProcessDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getProcessDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the process 
     *  date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProcessDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getProcessDateColumn(), style);
        return;
    }


    /**
     *  Gets the ProcessDate column name.
     *
     * @return the column name
     */

    protected String getProcessDateColumn() {
        return ("process_date");
    }


    /**
     *  Matches amounts between the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchAmount(org.osid.financials.Currency from, 
                            org.osid.financials.Currency to, boolean match) {
        getAssembler().addCurrencyRangeTerm(getAmountColumn(), from, to, match);
        return;
    }


    /**
     *  Matches payments with any amount. 
     *
     *  @param  match <code> true </code> to match payments with any amount, 
     *          <code> false </code> to match peyments with no amount 
     */

    @OSID @Override
    public void matchAnyAmount(boolean match) {
        getAssembler().addCurrencyRangeWildcardTerm(getAmountColumn(), match);
        return;
    }


    /**
     *  Clears the amount. 
     */

    @OSID @Override
    public void clearAmountTerms() {
        getAssembler().clearTerms(getAmountColumn());
        return;
    }


    /**
     *  Gets the amount query terms. 
     *
     *  @return the amount query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (getAssembler().getCurrencyRangeTerms(getAmountColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the amount. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAmount(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAmountColumn(), style);
        return;
    }


    /**
     *  Gets the Amount column name.
     *
     * @return the column name
     */

    protected String getAmountColumn() {
        return ("amount");
    }


    /**
     *  Sets the business <code> Id </code> for this query to match payments 
     *  assigned to businesses. 
     *
     *  @param  businessId the business <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> businessId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBusinessId(org.osid.id.Id businessId, boolean match) {
        getAssembler().addIdTerm(getBusinessIdColumn(), businessId, match);
        return;
    }


    /**
     *  Clears the business <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBusinessIdTerms() {
        getAssembler().clearTerms(getBusinessIdColumn());
        return;
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (getAssembler().getIdTerms(getBusinessIdColumn()));
    }


    /**
     *  Gets the BusinessId column name.
     *
     * @return the column name
     */

    protected String getBusinessIdColumn() {
        return ("business_id");
    }


    /**
     *  Tests if a <code> BusinessQuery </code> is available. 
     *
     *  @return <code> true </code> if a business query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBusinessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a business. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the business query 
     *  @throws org.osid.UnimplementedException <code> supportsBusinessQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.billing.BusinessQuery getBusinessQuery() {
        throw new org.osid.UnimplementedException("supportsBusinessQuery() is false");
    }


    /**
     *  Clears the business terms. 
     */

    @OSID @Override
    public void clearBusinessTerms() {
        getAssembler().clearTerms(getBusinessColumn());
        return;
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.billing.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.billing.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the Business column name.
     *
     * @return the column name
     */

    protected String getBusinessColumn() {
        return ("business");
    }


    /**
     *  Tests if this payment supports the given record
     *  <code>Type</code>.
     *
     *  @param  paymentRecordType a payment record type 
     *  @return <code>true</code> if the paymentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type paymentRecordType) {
        for (org.osid.billing.payment.records.PaymentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  paymentRecordType the payment record type 
     *  @return the payment query record 
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(paymentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentQueryRecord getPaymentQueryRecord(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PaymentQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(paymentRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  paymentRecordType the payment record type 
     *  @return the payment query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(paymentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentQueryInspectorRecord getPaymentQueryInspectorRecord(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PaymentQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(paymentRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param paymentRecordType the payment record type
     *  @return the payment search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(paymentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.billing.payment.records.PaymentSearchOrderRecord getPaymentSearchOrderRecord(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.billing.payment.records.PaymentSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(paymentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(paymentRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this payment. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param paymentQueryRecord the payment query record
     *  @param paymentQueryInspectorRecord the payment query inspector
     *         record
     *  @param paymentSearchOrderRecord the payment search order record
     *  @param paymentRecordType payment record type
     *  @throws org.osid.NullArgumentException
     *          <code>paymentQueryRecord</code>,
     *          <code>paymentQueryInspectorRecord</code>,
     *          <code>paymentSearchOrderRecord</code> or
     *          <code>paymentRecordTypepayment</code> is
     *          <code>null</code>
     */
            
    protected void addPaymentRecords(org.osid.billing.payment.records.PaymentQueryRecord paymentQueryRecord, 
                                      org.osid.billing.payment.records.PaymentQueryInspectorRecord paymentQueryInspectorRecord, 
                                      org.osid.billing.payment.records.PaymentSearchOrderRecord paymentSearchOrderRecord, 
                                      org.osid.type.Type paymentRecordType) {

        addRecordType(paymentRecordType);

        nullarg(paymentQueryRecord, "payment query record");
        nullarg(paymentQueryInspectorRecord, "payment query inspector record");
        nullarg(paymentSearchOrderRecord, "payment search odrer record");

        this.queryRecords.add(paymentQueryRecord);
        this.queryInspectorRecords.add(paymentQueryInspectorRecord);
        this.searchOrderRecords.add(paymentSearchOrderRecord);
        
        return;
    }
}
