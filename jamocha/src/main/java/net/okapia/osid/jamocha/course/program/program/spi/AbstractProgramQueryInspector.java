//
// AbstractProgramQueryInspector.java
//
//     A template for making a ProgramQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.program.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for programs.
 */

public abstract class AbstractProgramQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQueryInspector
    implements org.osid.course.program.ProgramQueryInspector {

    private final java.util.Collection<org.osid.course.program.records.ProgramQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the title query terms. 
     *
     *  @return the title query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getTitleTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the bumber query terms. 
     *
     *  @return the number query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNumberTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the sponsor <code> Id </code> query terms. 
     *
     *  @return the sponsor <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSponsorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the sponsor query terms. 
     *
     *  @return the sponsor query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getSponsorTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the completion requirements query terms. 
     *
     *  @return the prereq query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCompletionRequirementsInfoTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the completion requirements requisite <code> Id </code> query 
     *  terms. 
     *
     *  @return the requisite <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCompletionRequirementsIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the completion requirements requisite query terms. 
     *
     *  @return the requisite query terms 
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteQueryInspector[] getCompletionRequirementsTerms() {
        return (new org.osid.course.requisite.RequisiteQueryInspector[0]);
    }


    /**
     *  Gets the credential <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCredentialIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the credential query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQueryInspector[] getCredentialTerms() {
        return (new org.osid.course.program.CredentialQueryInspector[0]);
    }


    /**
     *  Gets the program offering <code> Id </code> query terms. 
     *
     *  @return the program offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProgramOfferingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the program offering query terms. 
     *
     *  @return the program offering query terms 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQueryInspector[] getProgramOfferingTerms() {
        return (new org.osid.course.program.ProgramOfferingQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given program query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a program implementing the requested record.
     *
     *  @param programRecordType a program record type
     *  @return the program query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(programRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.program.records.ProgramQueryInspectorRecord getProgramQueryInspectorRecord(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.program.records.ProgramQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(programRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programRecordType + " is not supported");
    }


    /**
     *  Adds a record to this program query. 
     *
     *  @param programQueryInspectorRecord program query inspector
     *         record
     *  @param programRecordType program record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProgramQueryInspectorRecord(org.osid.course.program.records.ProgramQueryInspectorRecord programQueryInspectorRecord, 
                                                   org.osid.type.Type programRecordType) {

        addRecordType(programRecordType);
        nullarg(programRecordType, "program record type");
        this.records.add(programQueryInspectorRecord);        
        return;
    }
}
