//
// AbstractOsidObjectSearchOrder.java
//
//     Defines a simple OSID search order to draw from.
//
//
// Tom Coppeto
// Okapia
// 14 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a simple OsidSearchOrder to extend. This class
 *  does nothing.
 */

public abstract class AbstractOsidObjectSearchOrder
    extends AbstractOsidIdentifiableSearchOrder
    implements org.osid.OsidObjectSearchOrder {

    private final OsidBrowsableSearchOrder order = new OsidBrowsableSearchOrder();


    /**
     *  Gets the record types available in this object.
     *
     *  @return the record types
     */

    @OSID @Override
    public org.osid.type.TypeList getRecordTypes() {
        return (this.order.getRecordTypes());
    }


    /**
     *  Tests if this object supports the given record <code>
     *  Type. </code>
     *
     *  @param  recordType a type 
     *  @return <code>true</code> if <code>recordType</code> is
     *          supported, <code> false </code> otherwise
     *  @throws org.osid.NullArgumentException <code> recordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recordType) {
        return (this.order.hasRecordType(recordType));
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    protected void addRecordType(org.osid.type.Type recordType) {
        this.order.addRecordType(recordType);
        return;
    }

    
    /**
     *  Specifies a preference for ordering the result set by the display 
     *  name. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisplayName(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the description. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDescription(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the genus type. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGenusType(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders by the state in a given <code> Process. </code> 
     *
     *  @param  processId a process <code> Id </code> 
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> processId </code> or 
     *          <code> style </code> is <code> null </code> 
     */

    @OSID @Override
    public void orderByState(org.osid.id.Id processId, 
                             org.osid.SearchOrderStyle style) {
        return;
    }

    
    /**
     *  Orders by the cumulative rating in a given <code> Book. </code> 
     *
     *  @param  bookId a book <code> Id </code> 
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          style </code> is <code> null </code> 
     */

    @OSID @Override
    public void orderByCumulativeRating(org.osid.id.Id bookId, 
                                        org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders by a statistic for a given <code> Meter. </code> 
     *
     *  @param  meterId a meter <code> Id </code> 
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> meterId </code> or 
     *          <code> style </code> is <code> null </code> 
     */

    @OSID @Override
    public void orderByStatistic(org.osid.id.Id meterId, 
                                 org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders by the timestamp of the first journal entry. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreateTime(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders by the timestamp of the last journal entry. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByLastModifiedTime(org.osid.SearchOrderStyle style) {
        return;
    }


    protected class OsidBrowsableSearchOrder
        extends AbstractOsidBrowsableSearchOrder
        implements org.osid.OsidBrowsableSearchOrder {
    }
}
