//
// AbstractAssemblySourceableOsidObjectQuery.java
//
//     A SourceableOsidObjectQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A SourceableOsidObjectQuery that stores terms.
 */

public abstract class AbstractAssemblySourceableOsidObjectQuery
    extends AbstractAssemblyOsidObjectQuery
    implements org.osid.OsidObjectQuery,
               org.osid.OsidObjectQueryInspector,
               org.osid.OsidObjectSearchOrder,
               org.osid.OsidSourceableQuery,
               org.osid.OsidSourceableQueryInspector,
               org.osid.OsidSourceableSearchOrder {

    private final AssemblyOsidSourceableQuery query;


    /** 
     *  Constructs a new
     *  <code>AbstractAssemblySourceableOsidObjectQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySourceableOsidObjectQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        this.query = new AssemblyOsidSourceableQuery(assembler);
        return;
    }
    

    /**
     *  Match the <code> Id </code> of the provider resource. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param match <code> true </code> if for a positive match,
     *          <code> false </code> for a negative match
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> is <code> null </code>
     */

    @OSID @Override
    public void matchProviderId(org.osid.id.Id resourceId, boolean match) {
        this.query.matchProviderId(resourceId, match);
        return;
    }


    /**
     *  Clears all provider <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearProviderIdTerms() {
        this.query.clearProviderIdTerms();
        return;
    }


    /**
     *  Gets the provider <code> Id </code> query terms. 
     *
     *  @return the provider <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProviderIdTerms() {
        return (this.query.getProviderIdTerms());
    }


    /**
     *  Gets the column name for the provider Id field.
     *
     *  @return the column name
     */

    protected String getProviderIdColumn() {
        return (this.query.getProviderIdColumn());
    }


    /**
     *  Tests if a <code> ResourceQuery </code> for the provider is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProviderQuery() {
        return (this.query.supportsProviderQuery());
    }


    /**
     *  Gets the query for the provider. Each retrieval performs a boolean 
     *  <code> OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the provider query 
     *  @throws org.osid.UnimplementedException <code> supportsProviderQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getProviderQuery(boolean match) {
        return (this.query.getProviderQuery(match));
    }


    /**
     *  Match sourceables with a provider value. 
     *
     *  @param  match <code> true </code> to match sourceables with any 
     *          provider, <code> false </code> to match sourceables with no 
     *          providers 
     */

    @OSID @Override
    public void matchAnyProvider(boolean match) {
        this.query.matchAnyProvider(match);
        return;
    }


    /**
     *  Clears all provider terms. 
     */

    @OSID @Override
    public void clearProviderTerms() {
        this.query.clearProviderTerms();
        return;
    }


    /**
     *  Gets the provider query terms. 
     *
     *  @return the provider terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getProviderTerms() {
        return (this.query.getProviderTerms());
    }


    /**
     *  Specifies a preference for ordering the results by provider. The 
     *  element of the provider to order is not specified but may be managed 
     *  through the provider ordering interface. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProvider(org.osid.SearchOrderStyle style) {
        this.query.orderByProvider(style);
        return;
    }


    /**
     *  Tests if a <code> ProviderSearchOrder </code> interface is available. 
     *
     *  @return <code> true </code> if a provider search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProviderSearchOrder() {
        return (this.query.supportsProviderSearchOrder());
    }

    
    /**
     *  Gets the search order interface for a provider. 
     *
     *  @return the provider search order interface 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProviderSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getProviderSearchOrder() {
        return (this.query.getProviderSearchOrder());
    }


    /**
     *  Gets the column name for the provider field.
     *
     *  @return the column name
     */

    protected String getProviderColumn() {
        return (this.query.getProviderColumn());
    }


    /**
     *  Match the <code> Id </code> of an asset used for branding. 
     *
     *  @param  assetId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBrandingId(org.osid.id.Id assetId, boolean match) {
        this.query.matchBrandingId(assetId, match);
        return;
    }


    /**
     *  Clears all asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBrandingIdTerms() {
        this.query.clearBrandingIdTerms();
        return;
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrandingIdTerms() {
        return (this.query.getBrandingIdTerms());
    }


    /**
     *  Gets the column name for the branding Id field.
     *
     *  @return the column name
     */

    protected String getBrandingIdColumn() {
        return (this.query.getBrandingIdColumn());
    }


    /**
     *  Tests if an <code> AssetQuery </code> for the branding is available. 
     *
     *  @return <code> true </code> if a asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrandingQuery() {
        return (this.query.supportsBrandingQuery());
    }


    /**
     *  Gets the query for an asset. Each retrieval performs a boolean <code> 
     *  OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsBrandingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getBrandingQuery(boolean match) {
        return (this.query.getBrandingQuery(match));
    }


    /**
     *  Match sourceables with any branding. 
     *
     *  @param  match <code> true </code> to match any asset, <code> false 
     *          </code> to match no assets 
     */

    @OSID @Override
    public void matchAnyBranding(boolean match) {
        this.query.matchAnyBranding(match);
        return;
    }


    /**
     *  Clears all branding terms. 
     */

    @OSID @Override
    public void clearBrandingTerms() {
        this.query.clearBrandingTerms();
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the branding terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getBrandingTerms() {
        return (this.query.getBrandingTerms());
    }

    
    /**
     *  Gets the column name for the branding field.
     *
     *  @return the column name
     */
    
    protected String getBrandingColumn() {
        return (this.query.getBrandingColumn());
    }


    /**
     *  Adds a license to match. Multiple license matches can be added to 
     *  perform a boolean <code> OR </code> among them. 
     *
     *  @param  license a string to match 
     *  @param  stringMatchType the string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> license </code> is 
     *          not of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> license </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchLicense(String license, 
                             org.osid.type.Type stringMatchType, boolean match) {
        this.query.matchLicense(license, stringMatchType, match);
        return;
    }


    /**
     *  Matches any object with a license. 
     *
     *  @param  match <code> true </code> to match any license, <code> false 
     *          </code> to match objects with no license 
     */

    @OSID @Override
    public void matchAnyLicense(boolean match) {
        this.query.matchAnyLicense(match);
        return;
    }

    
    /**
     *  Clears all license terms. 
     */

    @OSID @Override
    public void clearLicenseTerms() {
        this.query.clearLicenseTerms();
        return;
    }


    /**
     *  Gets the license query terms. 
     *
     *  @return the license terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLicenseTerms() {
        return (this.query.getLicenseTerms());
    }


    /**
     *  Gets the column name for the license field.
     *
     *  @return the column name
     */

    protected String getLicenseColumn() {
        return (this.query.getLicenseColumn());
    }


    protected class AssemblyOsidSourceableQuery
        extends AbstractAssemblyOsidSourceableQuery
        implements org.osid.OsidSourceableQuery,
                   org.osid.OsidSourceableQueryInspector,
                   org.osid.OsidSourceableSearchOrder {
        
        
        /** 
         *  Constructs a new <code>AssemblyOsidSourceableQuery</code>.
         *
         *  @param assembler the query assembler
         *  @throws org.osid.NullArgumentException <code>assembler</code>
         *          is <code>null</code>
         */
        
        protected AssemblyOsidSourceableQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
            super(assembler);
            return;
        }


        /**
         *  Gets the column name for the provider Id field.
         *
         *  @return the column name
         */

        @Override        
        protected String getProviderIdColumn() {
            return (super.getProviderIdColumn());
        }
        

        /**
         *  Gets the column name for the provider field.
         *
         *  @return the column name
         */
        
        @Override
        protected String getProviderColumn() {
            return (super.getProviderColumn());
        }


        /**
         *  Gets the column name for the branding Id field.
         *
         *  @return the column name
         */
        
        @Override
        protected String getBrandingIdColumn() {
            return (super.getBrandingIdColumn());
        }


        /**
         *  Gets the column name for the branding field.
         *
         *  @return the column name
         */
        
        @Override
        protected String getBrandingColumn() {
            return (super.getBrandingColumn());
        }


        /**
         *  Gets the column name for the license field.
         *
         *  @return the column name
         */
        
        @Override
        protected String getLicenseColumn() {
            return (super.getLicenseColumn());
        }
    }    
}
