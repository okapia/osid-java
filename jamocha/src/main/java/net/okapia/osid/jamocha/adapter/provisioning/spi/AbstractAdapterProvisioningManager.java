//
// AbstractProvisioningManager.java
//
//     An adapter for a ProvisioningManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ProvisioningManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterProvisioningManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.provisioning.ProvisioningManager>
    implements org.osid.provisioning.ProvisioningManager {


    /**
     *  Constructs a new {@code AbstractAdapterProvisioningManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterProvisioningManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterProvisioningManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterProvisioningManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a my provision service is supported for the current agent. 
     *
     *  @return <code> true </code> if my provision is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyProvision() {
        return (getAdapteeManager().supportsMyProvision());
    }


    /**
     *  Tests if a my supplier service is supported for the current agent. 
     *
     *  @return <code> true </code> if my supplier is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMySupplier() {
        return (getAdapteeManager().supportsMySupplier());
    }


    /**
     *  Tests if a my provision notification service is supported for the 
     *  current agent. 
     *
     *  @return <code> true </code> if my provision notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyProvisionNotification() {
        return (getAdapteeManager().supportsMyProvisionNotification());
    }


    /**
     *  Tests if looking up provisions is supported. 
     *
     *  @return <code> true </code> if provision lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionLookup() {
        return (getAdapteeManager().supportsProvisionLookup());
    }


    /**
     *  Tests if querying provisions is supported. 
     *
     *  @return <code> true </code> if provision query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionQuery() {
        return (getAdapteeManager().supportsProvisionQuery());
    }


    /**
     *  Tests if searching provisions is supported. 
     *
     *  @return <code> true </code> if provision search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionSearch() {
        return (getAdapteeManager().supportsProvisionSearch());
    }


    /**
     *  Tests if a provision administrative service is supported. 
     *
     *  @return <code> true </code> if provision administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionAdmin() {
        return (getAdapteeManager().supportsProvisionAdmin());
    }


    /**
     *  Tests if a provision <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if provision notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionNotification() {
        return (getAdapteeManager().supportsProvisionNotification());
    }


    /**
     *  Tests if a provision distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a provision distributor lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionDistributor() {
        return (getAdapteeManager().supportsProvisionDistributor());
    }


    /**
     *  Tests if a provision distributor assignment service is supported. 
     *
     *  @return <code> true </code> if a provision to distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionDistributorAssignment() {
        return (getAdapteeManager().supportsProvisionDistributorAssignment());
    }


    /**
     *  Tests if a provision smart distributor service is supported. 
     *
     *  @return <code> true </code> if an v smart distributor service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionSmartDistributor() {
        return (getAdapteeManager().supportsProvisionSmartDistributor());
    }


    /**
     *  Tests if returning provisions is supported. 
     *
     *  @return <code> true </code> if returning provisions is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionReturn() {
        return (getAdapteeManager().supportsProvisionReturn());
    }


    /**
     *  Tests if looking up queues is supported. 
     *
     *  @return <code> true </code> if queue lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueLookup() {
        return (getAdapteeManager().supportsQueueLookup());
    }


    /**
     *  Tests if querying queues is supported. 
     *
     *  @return <code> true </code> if queue query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (getAdapteeManager().supportsQueueQuery());
    }


    /**
     *  Tests if searching queues is supported. 
     *
     *  @return <code> true </code> if queue search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSearch() {
        return (getAdapteeManager().supportsQueueSearch());
    }


    /**
     *  Tests if queue administrative service is supported. 
     *
     *  @return <code> true </code> if queue administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueAdmin() {
        return (getAdapteeManager().supportsQueueAdmin());
    }


    /**
     *  Tests if a queue notification service is supported. 
     *
     *  @return <code> true </code> if queue notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueNotification() {
        return (getAdapteeManager().supportsQueueNotification());
    }


    /**
     *  Tests if a queue broker lookup service is supported. 
     *
     *  @return <code> true </code> if a queue broker lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueBroker() {
        return (getAdapteeManager().supportsQueueBroker());
    }


    /**
     *  Tests if a queue broker service is supported. 
     *
     *  @return <code> true </code> if queue to broker assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueBrokerAssignment() {
        return (getAdapteeManager().supportsQueueBrokerAssignment());
    }


    /**
     *  Tests if a queue smart broker lookup service is supported. 
     *
     *  @return <code> true </code> if a queue smart broker service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSmartBroker() {
        return (getAdapteeManager().supportsQueueSmartBroker());
    }


    /**
     *  Tests if looking up requests is supported. 
     *
     *  @return <code> true </code> if request lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestLookup() {
        return (getAdapteeManager().supportsRequestLookup());
    }


    /**
     *  Tests if querying requests is supported. 
     *
     *  @return <code> true </code> if request query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestQuery() {
        return (getAdapteeManager().supportsRequestQuery());
    }


    /**
     *  Tests if searching requests is supported. 
     *
     *  @return <code> true </code> if request search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestSearch() {
        return (getAdapteeManager().supportsRequestSearch());
    }


    /**
     *  Tests if request administrative service is supported. 
     *
     *  @return <code> true </code> if request administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestAdmin() {
        return (getAdapteeManager().supportsRequestAdmin());
    }


    /**
     *  Tests if a request notification service is supported. 
     *
     *  @return <code> true </code> if request notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestNotification() {
        return (getAdapteeManager().supportsRequestNotification());
    }


    /**
     *  Tests if a request distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a request distributor lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestDistributor() {
        return (getAdapteeManager().supportsRequestDistributor());
    }


    /**
     *  Tests if a request distributor service is supported. 
     *
     *  @return <code> true </code> if request to distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestDistributorAssignment() {
        return (getAdapteeManager().supportsRequestDistributorAssignment());
    }


    /**
     *  Tests if a request smart distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a request smart distributor service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestSmartDistributor() {
        return (getAdapteeManager().supportsRequestSmartDistributor());
    }


    /**
     *  Tests if looking up request transactions is supported. 
     *
     *  @return <code> true </code> if request transaction lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionLookup() {
        return (getAdapteeManager().supportsRequestTransactionLookup());
    }


    /**
     *  Tests if request transaction administrative service is supported. 
     *
     *  @return <code> true </code> if request transaction administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRequestTransactionAdmin() {
        return (getAdapteeManager().supportsRequestTransactionAdmin());
    }


    /**
     *  Tests if exchanging provisions is supported. 
     *
     *  @return <code> true </code> if exchange is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExchange() {
        return (getAdapteeManager().supportsExchange());
    }


    /**
     *  Tests if looking up pools is supported. 
     *
     *  @return <code> true </code> if pool lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolLookup() {
        return (getAdapteeManager().supportsPoolLookup());
    }


    /**
     *  Tests if querying pools is supported. 
     *
     *  @return <code> true </code> if pool query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolQuery() {
        return (getAdapteeManager().supportsPoolQuery());
    }


    /**
     *  Tests if searching pools is supported. 
     *
     *  @return <code> true </code> if pool search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolSearch() {
        return (getAdapteeManager().supportsPoolSearch());
    }


    /**
     *  Tests if a pool administrative service is supported. 
     *
     *  @return <code> true </code> if pool administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolAdmin() {
        return (getAdapteeManager().supportsPoolAdmin());
    }


    /**
     *  Tests if a pool <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if pool notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolNotification() {
        return (getAdapteeManager().supportsPoolNotification());
    }


    /**
     *  Tests if a pool distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a pool distributor lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolDistributor() {
        return (getAdapteeManager().supportsPoolDistributor());
    }


    /**
     *  Tests if a pool distributor assignment service is supported. 
     *
     *  @return <code> true </code> if a pool to distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolDistributorAssignment() {
        return (getAdapteeManager().supportsPoolDistributorAssignment());
    }


    /**
     *  Tests if a pool smart distributor service is supported. 
     *
     *  @return <code> true </code> if a smart distributor service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPoolSmartDistributor() {
        return (getAdapteeManager().supportsPoolSmartDistributor());
    }


    /**
     *  Tests if looking up provisionables is supported. 
     *
     *  @return <code> true </code> if provisionable lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableLookup() {
        return (getAdapteeManager().supportsProvisionableLookup());
    }


    /**
     *  Tests if querying provisionables is supported. 
     *
     *  @return <code> true </code> if provisionable query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableQuery() {
        return (getAdapteeManager().supportsProvisionableQuery());
    }


    /**
     *  Tests if searching provisionables is supported. 
     *
     *  @return <code> true </code> if provisionable search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableSearch() {
        return (getAdapteeManager().supportsProvisionableSearch());
    }


    /**
     *  Tests if provisionable <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if provisionable administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableAdmin() {
        return (getAdapteeManager().supportsProvisionableAdmin());
    }


    /**
     *  Tests if a provisionable <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if provisionable notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableNotification() {
        return (getAdapteeManager().supportsProvisionableNotification());
    }


    /**
     *  Tests if a provisionable distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a provisionable distributor lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableDistributor() {
        return (getAdapteeManager().supportsProvisionableDistributor());
    }


    /**
     *  Tests if a provisionable distributor assignment service is supported. 
     *
     *  @return <code> true </code> if a provisionable to distributor 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableDistributorAssignment() {
        return (getAdapteeManager().supportsProvisionableDistributorAssignment());
    }


    /**
     *  Tests if a provisionable smart distributor service is supported. 
     *
     *  @return <code> true </code> if a provisionable smart distributor 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisionableSmartDistributor() {
        return (getAdapteeManager().supportsProvisionableSmartDistributor());
    }


    /**
     *  Tests if looking up brokers is supported. 
     *
     *  @return <code> true </code> if broker lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerLookup() {
        return (getAdapteeManager().supportsBrokerLookup());
    }


    /**
     *  Tests if querying brokers is supported. 
     *
     *  @return <code> true </code> if a broker query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerQuery() {
        return (getAdapteeManager().supportsBrokerQuery());
    }


    /**
     *  Tests if searching brokers is supported. 
     *
     *  @return <code> true </code> if broker search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSearch() {
        return (getAdapteeManager().supportsBrokerSearch());
    }


    /**
     *  Tests if broker administrative service is supported. 
     *
     *  @return <code> true </code> if broker administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerAdmin() {
        return (getAdapteeManager().supportsBrokerAdmin());
    }


    /**
     *  Tests if a broker <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if broker notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerNotification() {
        return (getAdapteeManager().supportsBrokerNotification());
    }


    /**
     *  Tests if a broker distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a broker distributor lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerDistributor() {
        return (getAdapteeManager().supportsBrokerDistributor());
    }


    /**
     *  Tests if a broker distributor service is supported. 
     *
     *  @return <code> true </code> if broker to distributor assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerDistributorAssignment() {
        return (getAdapteeManager().supportsBrokerDistributorAssignment());
    }


    /**
     *  Tests if a broker smart distributor lookup service is supported. 
     *
     *  @return <code> true </code> if a broker smart distributor service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBrokerSmartDistributor() {
        return (getAdapteeManager().supportsBrokerSmartDistributor());
    }


    /**
     *  Tests if looking up distributors is supported. 
     *
     *  @return <code> true </code> if distributor lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorLookup() {
        return (getAdapteeManager().supportsDistributorLookup());
    }


    /**
     *  Tests if querying distributors is supported. 
     *
     *  @return <code> true </code> if a distributor query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (getAdapteeManager().supportsDistributorQuery());
    }


    /**
     *  Tests if searching distributors is supported. 
     *
     *  @return <code> true </code> if distributor search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorSearch() {
        return (getAdapteeManager().supportsDistributorSearch());
    }


    /**
     *  Tests if distributor administrative service is supported. 
     *
     *  @return <code> true </code> if distributor administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorAdmin() {
        return (getAdapteeManager().supportsDistributorAdmin());
    }


    /**
     *  Tests if a distributor <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if distributor notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorNotification() {
        return (getAdapteeManager().supportsDistributorNotification());
    }


    /**
     *  Tests for the availability of a distributor hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if distributor hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorHierarchy() {
        return (getAdapteeManager().supportsDistributorHierarchy());
    }


    /**
     *  Tests for the availability of a distributor hierarchy design service. 
     *
     *  @return <code> true </code> if distributor hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorHierarchyDesign() {
        return (getAdapteeManager().supportsDistributorHierarchyDesign());
    }


    /**
     *  Tests for the availability of a provisioning batch service. 
     *
     *  @return <code> true </code> if provisioning batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisioningBatch() {
        return (getAdapteeManager().supportsProvisioningBatch());
    }


    /**
     *  Tests for the availability of a provisioning rules service. 
     *
     *  @return <code> true </code> if provisioning rules service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProvisioningRules() {
        return (getAdapteeManager().supportsProvisioningRules());
    }


    /**
     *  Gets the supported <code> Provision </code> record types. 
     *
     *  @return a list containing the supported <code> Provision </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionRecordTypes() {
        return (getAdapteeManager().getProvisionRecordTypes());
    }


    /**
     *  Tests if the given <code> Provision </code> record type is supported. 
     *
     *  @param  provisionRecordType a <code> Type </code> indicating a <code> 
     *          Provision </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> provisionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionRecordType(org.osid.type.Type provisionRecordType) {
        return (getAdapteeManager().supportsProvisionRecordType(provisionRecordType));
    }


    /**
     *  Gets the supported <code> Provision </code> search types. 
     *
     *  @return a list containing the supported <code> Provision </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionSearchRecordTypes() {
        return (getAdapteeManager().getProvisionSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Provision </code> search type is supported. 
     *
     *  @param  provisionSearchRecordType a <code> Type </code> indicating a 
     *          <code> Provision </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          provisionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionSearchRecordType(org.osid.type.Type provisionSearchRecordType) {
        return (getAdapteeManager().supportsProvisionSearchRecordType(provisionSearchRecordType));
    }


    /**
     *  Gets the supported <code> ProvisionReturn </code> record types. 
     *
     *  @return a list containing the supported <code> ProvisionReturn </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionReturnRecordTypes() {
        return (getAdapteeManager().getProvisionReturnRecordTypes());
    }


    /**
     *  Tests if the given <code> ProvisionReturn </code> record type is 
     *  supported. 
     *
     *  @param  provisionReturnRecordType a <code> Type </code> indicating a 
     *          <code> ProvisionReturn </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          provisionReturnRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionReturnRecordType(org.osid.type.Type provisionReturnRecordType) {
        return (getAdapteeManager().supportsProvisionReturnRecordType(provisionReturnRecordType));
    }


    /**
     *  Gets the supported <code> Queue </code> record types. 
     *
     *  @return a list containing the supported <code> Queue </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueRecordTypes() {
        return (getAdapteeManager().getQueueRecordTypes());
    }


    /**
     *  Tests if the given <code> Queue </code> record type is supported. 
     *
     *  @param  queueRecordType a <code> Type </code> indicating a <code> 
     *          Queue </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueRecordType(org.osid.type.Type queueRecordType) {
        return (getAdapteeManager().supportsQueueRecordType(queueRecordType));
    }


    /**
     *  Gets the supported <code> Queue </code> search record types. 
     *
     *  @return a list containing the supported <code> Queue </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueSearchRecordTypes() {
        return (getAdapteeManager().getQueueSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Queue </code> search record type is 
     *  supported. 
     *
     *  @param  queueSearchRecordType a <code> Type </code> indicating a 
     *          <code> Queue </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueSearchRecordType(org.osid.type.Type queueSearchRecordType) {
        return (getAdapteeManager().supportsQueueSearchRecordType(queueSearchRecordType));
    }


    /**
     *  Gets the supported <code> Request </code> record types. 
     *
     *  @return a list containing the supported <code> Request </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequestRecordTypes() {
        return (getAdapteeManager().getRequestRecordTypes());
    }


    /**
     *  Tests if the given <code> Request </code> record type is supported. 
     *
     *  @param  requestRecordType a <code> Type </code> indicating a <code> 
     *          Request </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> requestRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequestRecordType(org.osid.type.Type requestRecordType) {
        return (getAdapteeManager().supportsRequestRecordType(requestRecordType));
    }


    /**
     *  Gets the supported <code> Request </code> search record types. 
     *
     *  @return a list containing the supported <code> Request </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequestSearchRecordTypes() {
        return (getAdapteeManager().getRequestSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Request </code> search record type is 
     *  supported. 
     *
     *  @param  requestSearchRecordType a <code> Type </code> indicating a 
     *          <code> Request </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> requestSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequestSearchRecordType(org.osid.type.Type requestSearchRecordType) {
        return (getAdapteeManager().supportsRequestSearchRecordType(requestSearchRecordType));
    }


    /**
     *  Gets the supported <code> RequestTransaction </code> record types. 
     *
     *  @return a list containing the supported <code> RequestTransaction 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequestTransactionRecordTypes() {
        return (getAdapteeManager().getRequestTransactionRecordTypes());
    }


    /**
     *  Tests if the given <code> RequestTransaction </code> record type is 
     *  supported. 
     *
     *  @param  requestTransactionRecordType a <code> Type </code> indicating 
     *          a <code> RequestTransaction </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          requestTransactionRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequestTransactionRecordType(org.osid.type.Type requestTransactionRecordType) {
        return (getAdapteeManager().supportsRequestTransactionRecordType(requestTransactionRecordType));
    }


    /**
     *  Gets the supported <code> Pool </code> record types. 
     *
     *  @return a list containing the supported <code> Pool </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolRecordTypes() {
        return (getAdapteeManager().getPoolRecordTypes());
    }


    /**
     *  Tests if the given <code> Pool </code> record type is supported. 
     *
     *  @param  poolRecordType a <code> Type </code> indicating a <code> Pool 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> poolRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPoolRecordType(org.osid.type.Type poolRecordType) {
        return (getAdapteeManager().supportsPoolRecordType(poolRecordType));
    }


    /**
     *  Gets the supported <code> Pool </code> search types. 
     *
     *  @return a list containing the supported <code> Pool </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPoolSearchRecordTypes() {
        return (getAdapteeManager().getPoolSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Pool </code> search type is supported. 
     *
     *  @param  poolSearchRecordType a <code> Type </code> indicating a <code> 
     *          Pool </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> poolSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPoolSearchRecordType(org.osid.type.Type poolSearchRecordType) {
        return (getAdapteeManager().supportsPoolSearchRecordType(poolSearchRecordType));
    }


    /**
     *  Gets the supported <code> Provisionable </code> record types. 
     *
     *  @return a list containing the supported <code> Provisionable </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionableRecordTypes() {
        return (getAdapteeManager().getProvisionableRecordTypes());
    }


    /**
     *  Tests if the given <code> Provisionable </code> record type is 
     *  supported. 
     *
     *  @param  provisionableRecordType a <code> Type </code> indicating a 
     *          <code> Provisionable </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> provisionableRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionableRecordType(org.osid.type.Type provisionableRecordType) {
        return (getAdapteeManager().supportsProvisionableRecordType(provisionableRecordType));
    }


    /**
     *  Gets the supported <code> Provisionable </code> search types. 
     *
     *  @return a list containing the supported <code> Provisionable </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProvisionableSearchRecordTypes() {
        return (getAdapteeManager().getProvisionableSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Provisionable </code> search type is 
     *  supported. 
     *
     *  @param  provisionableSearchRecordType a <code> Type </code> indicating 
     *          a <code> Provisionable </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          provisionableSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProvisionableSearchRecordType(org.osid.type.Type provisionableSearchRecordType) {
        return (getAdapteeManager().supportsProvisionableSearchRecordType(provisionableSearchRecordType));
    }


    /**
     *  Gets the supported <code> Broker </code> record types. 
     *
     *  @return a list containing the supported <code> Broker </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerRecordTypes() {
        return (getAdapteeManager().getBrokerRecordTypes());
    }


    /**
     *  Tests if the given <code> Broker </code> record type is supported. 
     *
     *  @param  brokerRecordType a <code> Type </code> indicating a <code> 
     *          Broker </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> brokerRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBrokerRecordType(org.osid.type.Type brokerRecordType) {
        return (getAdapteeManager().supportsBrokerRecordType(brokerRecordType));
    }


    /**
     *  Gets the supported <code> Broker </code> search record types. 
     *
     *  @return a list containing the supported <code> Broker </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBrokerSearchRecordTypes() {
        return (getAdapteeManager().getBrokerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Broker </code> search record type is 
     *  supported. 
     *
     *  @param  brokerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Broker </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> brokerSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBrokerSearchRecordType(org.osid.type.Type brokerSearchRecordType) {
        return (getAdapteeManager().supportsBrokerSearchRecordType(brokerSearchRecordType));
    }


    /**
     *  Gets the supported <code> Distributor </code> record types. 
     *
     *  @return a list containing the supported <code> Distributor </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDistributorRecordTypes() {
        return (getAdapteeManager().getDistributorRecordTypes());
    }


    /**
     *  Tests if the given <code> Distributor </code> record type is 
     *  supported. 
     *
     *  @param  distributorRecordType a <code> Type </code> indicating a 
     *          <code> Distributor </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> distributorRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDistributorRecordType(org.osid.type.Type distributorRecordType) {
        return (getAdapteeManager().supportsDistributorRecordType(distributorRecordType));
    }


    /**
     *  Gets the supported <code> Distributor </code> search record types. 
     *
     *  @return a list containing the supported <code> Distributor </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDistributorSearchRecordTypes() {
        return (getAdapteeManager().getDistributorSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Distributor </code> search record type is 
     *  supported. 
     *
     *  @param  distributorSearchRecordType a <code> Type </code> indicating a 
     *          <code> Distributor </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          distributorSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDistributorSearchRecordType(org.osid.type.Type distributorSearchRecordType) {
        return (getAdapteeManager().supportsDistributorSearchRecordType(distributorSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my provision 
     *  service. 
     *
     *  @return a <code> MyProvisionSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyProvision() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionSession getMyProvisionSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyProvisionSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my provision 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distrivutor 
     *  @return a <code> MyProvisionSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distriobutor </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyProvision() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionSession getMyProvisionSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyProvisionSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my supplier 
     *  service. 
     *
     *  @return a <code> MySupplierSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMySupplier() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MySupplierSession getMySupplierSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMySupplierSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my Supplier 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distrivutor 
     *  @return a <code> MySupplierSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distriobutor </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMySupplier() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MySupplierSession getMySupplierSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMySupplierSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service for resources related to the authentciated agent. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @return a <code> MyProvisionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyProvisionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionNotificationSession getMyProvisionNotificationSession(org.osid.provisioning.ProvisionReceiver provisionReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyProvisionNotificationSession(provisionReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service for the given distributor for resources related 
     *  to the authentciated agent. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> MyProvisionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyProvisionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.MyProvisionNotificationSession getMyProvisionNotificationSessionForDistributor(org.osid.provisioning.ProvisionReceiver provisionReceiver, 
                                                                                                                org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyProvisionNotificationSessionForDistributor(provisionReceiver, distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  lookup service. 
     *
     *  @return a <code> ProvisionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionLookupSession getProvisionLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionLookupSession getProvisionLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionLookupSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  query service. 
     *
     *  @return a <code> ProvisionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuerySession getProvisionQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionQuerySession getProvisionQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionQuerySessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  search service. 
     *
     *  @return a <code> ProvisionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchSession getProvisionSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  search service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionSearchSession getProvisionSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionSearchSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  administration service. 
     *
     *  @return a <code> ProvisionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionAdminSession getProvisionAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionAdminSession getProvisionAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionAdminSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @return a <code> ProvisionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionNotificationSession getProvisionNotificationSession(org.osid.provisioning.ProvisionReceiver provisionReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionNotificationSession(provisionReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  notification service for the given distributor. 
     *
     *  @param  provisionReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> provisionReceiver 
     *          </code> or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionNotificationSession getProvisionNotificationSessionForDistributor(org.osid.provisioning.ProvisionReceiver provisionReceiver, 
                                                                                                            org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionNotificationSessionForDistributor(provisionReceiver, distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup provision/distributor 
     *  mappings. 
     *
     *  @return a <code> ProvisionDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionDistributorSession getProvisionDistributorSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionDistributorSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  provisions to distributors. 
     *
     *  @return a <code> ProvisionDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionDistributorAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionDistributorAssignmentSession getProvisionDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionDistributorAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage provision smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionDistributorSession getProvisionSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionSmartDistributorSession(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  return service. 
     *
     *  @return a <code> ProvisionReturnSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionReturn() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnSession getProvisionReturnSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionReturnSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provision 
     *  return service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionReturnSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionReturn() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionReturnSession getProvisionReturnSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionReturnSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service. 
     *
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueLookupSession getQueueLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueLookupSession getQueueLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueLookupSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service. 
     *
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuerySession getQueueQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQuerySession getQueueQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueQuerySessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service. 
     *
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSearchSession getQueueSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSearchSession getQueueSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueSearchSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service. 
     *
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueAdminSession getQueueAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueAdminSession getQueueAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueAdminSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service. 
     *
     *  @param  queueReceiver the notification callback 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueNotificationSession getQueueNotificationSession(org.osid.provisioning.QueueReceiver queueReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueNotificationSession(queueReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service for the given distributor. 
     *
     *  @param  queueReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver </code> or 
     *          <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueNotificationSession getQueueNotificationSessionForDistributor(org.osid.provisioning.QueueReceiver queueReceiver, 
                                                                                                    org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueNotificationSessionForDistributor(queueReceiver, distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue/distributor 
     *  mappings. 
     *
     *  @return a <code> QueueDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueDistributorSession getQueueDistributorSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueDistributorSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queues 
     *  to distributors. 
     *
     *  @return a <code> QueueDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueDistributorAssignmentSession getQueueDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueDistributorAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @return a <code> QueueSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.QueueSmartDistributorSession getQueueSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueSmartDistributorSession(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request lookup 
     *  service. 
     *
     *  @return a <code> RequestLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getRequestLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getRequestLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestLookupSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request query 
     *  service. 
     *
     *  @return a <code> RequestQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getRequestQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> CRequestQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getRequestQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestQuerySessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request search 
     *  service. 
     *
     *  @return a <code> RequestSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getRequestSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getRequestSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestSearchSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  administrative service. 
     *
     *  @return a <code> RequestAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getRequestAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  administrative service for the given queue. 
     *
     *  @param  queueId the <code> Id </code> of the <code> Queue </code> 
     *  @return a <code> RequestAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Queue </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRequestAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getRequestAdminSessionForQueue(org.osid.id.Id queueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestAdminSessionForQueue(queueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  notification service. 
     *
     *  @param  requestReceiver the notification callback 
     *  @return a <code> RequestNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> requestReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getRequestNotificationSession(org.osid.provisioning.RequestReceiver requestReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestNotificationSession(requestReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  notification service for the given distributor. 
     *
     *  @param  requestReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> requestReceiver </code> 
     *          or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getRequestNotificationSessionForDistributor(org.osid.provisioning.RequestReceiver requestReceiver, 
                                                                                                        org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestNotificationSessionForDistributor(requestReceiver, distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup request/distributor 
     *  mappings. 
     *
     *  @return a <code> RequestDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorSession getRequestDistributorSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestDistributorSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queues 
     *  to distributors. 
     *
     *  @return a <code> RequestyDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorAssignmentSession getRequestDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestDistributorAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage request smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSmartDistributorSession getRequestSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestSmartDistributorSession(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction lookup service. 
     *
     *  @return a <code> RequestTransactionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionLookupSession getRequestTransactionLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestTransactionLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestTransactionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionLookupSession getRequestTransactionLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestTransactionLookupSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction administrative service. 
     *
     *  @return a <code> RequestTransactionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionAdminSession getRequestTransactionAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestTransactionAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the request 
     *  transaction administrative service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> RequestTransactionAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRequestTransactionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestTransactionAdminSession getRequestTransactionAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRequestTransactionAdminSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the exchange 
     *  service. 
     *
     *  @return an <code> ExchangeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsExchange() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ExchangeSession getExchangeSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getExchangeSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the exchange 
     *  service for the given queue. 
     *
     *  @param  queueId the <code> Id </code> of the <code> Queue </code> 
     *  @return an <code> ExchangeSession </code> 
     *  @throws org.osid.NotFoundException no <code> Queue </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsExchange() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.ExchangeSession getExchangeSessionForQueue(org.osid.id.Id queueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getExchangeSessionForQueue(queueId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool lookup 
     *  service. 
     *
     *  @return a <code> PoolLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolLookupSession getPoolLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolLookupSession getPoolLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolLookupSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool query 
     *  service. 
     *
     *  @return a <code> PoolQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuerySession getPoolQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolQuerySession getPoolQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolQuerySessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool search 
     *  service. 
     *
     *  @return a <code> PoolSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchSession getPoolSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSearchSession getPoolSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolSearchSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  administration service. 
     *
     *  @return a <code> PoolAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolAdminSession getPoolAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPoolAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolAdminSession getPoolAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolAdminSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  notification service. 
     *
     *  @param  poolReceiver the notification callback 
     *  @return a <code> PoolNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> poolReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolNotificationSession getPoolNotificationSession(org.osid.provisioning.PoolReceiver poolReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolNotificationSession(poolReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the pool 
     *  notification service for the given distributor. 
     *
     *  @param  poolReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> poolReceiver </code> or 
     *          <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolNotificationSession getPoolNotificationSessionForDistributor(org.osid.provisioning.PoolReceiver poolReceiver, 
                                                                                                  org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolNotificationSessionForDistributor(poolReceiver, distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup pool/distributor 
     *  mappings. 
     *
     *  @return a <code> PoolDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolDistributorSession getPoolDistributorSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolDistributorSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning pools to 
     *  distributors. 
     *
     *  @return a <code> PoolDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolDistributorAssignmentSession getPoolDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolDistributorAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage pool smart distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> PoolSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPoolSmartDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.PoolSmartDistributorSession getPoolSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPoolSmartDistributorSession(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  lookup service. 
     *
     *  @return a <code> ProvisionableLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getProvisionableLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  lookup service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the distributor 
     *  @return a <code> ProvisionableLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestLookupSession getProvisionableLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableLookupSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  query service. 
     *
     *  @return a <code> ProvisionableQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getProvisionableQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  query service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionableQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestQuerySession getProvisionableQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableQuerySessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  search service. 
     *
     *  @return a <code> ProvisionableSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getProvisionableSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  search service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionableSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSearchSession getProvisionableSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableSearchSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  administration service. 
     *
     *  @return a <code> ProvisionableAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getProvisionableAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Dostributor 
     *          </code> 
     *  @return a <code> ProvisionableAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestAdminSession getProvisionableAdminSessionForPool(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableAdminSessionForPool(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  notification service. 
     *
     *  @param  provisionableReceiver the notification callback 
     *  @return a <code> ProvisionableNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> provisionableReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getProvisionableNotificationSession(org.osid.provisioning.RequestReceiver provisionableReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableNotificationSession(provisionableReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the provisionable 
     *  notification service for the given distributor. 
     *
     *  @param  provisionableReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionableNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> provisionableReceiver 
     *          </code> or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestNotificationSession getProvisionableNotificationSessionForDistributor(org.osid.provisioning.RequestReceiver provisionableReceiver, 
                                                                                                              org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableNotificationSessionForDistributor(provisionableReceiver, distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup 
     *  provisionable/distributor mappings. 
     *
     *  @return a <code> ProvisionableDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorSession getProvisionableDistributorSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableDistributorSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  provisionables to distributors. 
     *
     *  @return a <code> ProvisionableDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableDistributorAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestDistributorAssignmentSession getProvisionableDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableDistributorAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> ProvisionableSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisionableSmartDistributor() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.RequestSmartDistributorSession getProvisionableSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisionableSmartDistributorSession(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker lookup 
     *  service. 
     *
     *  @return a <code> BrokerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerLookupSession getBrokerLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker lookup 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerLookupSession getBrokerLookupSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerLookupSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker query 
     *  service. 
     *
     *  @return a <code> BrokerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuerySession getBrokerQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker query 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerQuerySession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerQuerySession getBrokerQuerySessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerQuerySessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker search 
     *  service. 
     *
     *  @return a <code> BrokerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchSession getBrokerSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker search 
     *  service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerSearchSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSearchSession getBrokerSearchSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerSearchSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  administration service. 
     *
     *  @return a <code> BrokerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerAdminSession getBrokerAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  administration service for the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerAdminSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBrokerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerAdminSession getBrokerAdminSessionForDistributor(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerAdminSessionForDistributor(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  notification service. 
     *
     *  @param  brokerReceiver the notification callback 
     *  @return a <code> BrokerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> brokerReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerNotificationSession getBrokerNotificationSession(org.osid.provisioning.BrokerReceiver brokerReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerNotificationSession(brokerReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the broker 
     *  notification service for the given distributor. 
     *
     *  @param  brokerReceiver the notification callback 
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no distributor found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> brokerReceiver </code> 
     *          or <code> distributorId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerNotificationSession getBrokerNotificationSessionForDistributor(org.osid.provisioning.BrokerReceiver brokerReceiver, 
                                                                                                      org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerNotificationSessionForDistributor(brokerReceiver, distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup broker/distributor 
     *  mappings. 
     *
     *  @return a <code> BrokerDistributorSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerDistributor() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerDistributorSession getBrokerDistributorSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerDistributorSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning brokers 
     *  to distributors. 
     *
     *  @return a <code> BrokerDistributorAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerDistributorAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerDistributorAssignmentSession getBrokerDistributorAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerDistributorAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage broker smart 
     *  distributors. 
     *
     *  @param  distributorId the <code> Id </code> of the <code> Distributor 
     *          </code> 
     *  @return a <code> BrokerSmartDistributorSession </code> 
     *  @throws org.osid.NotFoundException no <code> Distributor </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBrokerSmartDistributor() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.BrokerSmartDistributorSession getBrokerSmartDistributorSession(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBrokerSmartDistributorSession(distributorId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  lookup service. 
     *
     *  @return a <code> DistributorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorLookupSession getDistributorLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDistributorLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  query service. 
     *
     *  @return a <code> DistributorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuerySession getDistributorQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDistributorQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  search service. 
     *
     *  @return a <code> DistributorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorSearchSession getDistributorSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDistributorSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  administrative service. 
     *
     *  @return a <code> DistributorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorAdminSession getDistributorAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDistributorAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  notification service. 
     *
     *  @param  distributorReceiver the notification callback 
     *  @return a <code> DistributorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> distributorReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorNotificationSession getDistributorNotificationSession(org.osid.provisioning.DistributorReceiver distributorReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDistributorNotificationSession(distributorReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  hierarchy service. 
     *
     *  @return a <code> DistributorHierarchySession </code> for distributors 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorHierarchySession getDistributorHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDistributorHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the distributor 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for distributors 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorHierarchyDesignSession getDistributorHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDistributorHierarchyDesignSession());
    }


    /**
     *  Gets the <code> ProvisioningBatchManager. </code> 
     *
     *  @return a <code> ProvisioningBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.batch.ProvisioningBatchManager getProvisioningBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisioningBatchManager());
    }


    /**
     *  Gets the <code> ProvisioningRulesManager. </code> 
     *
     *  @return a <code> ProvisioningRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProvisioningRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.ProvisioningRulesManager getProvisioningRulesManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProvisioningRulesManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();
	
        return;
    }
}
