//
// AbstractQueryPoolLookupSession.java
//
//    An inline adapter that maps a PoolLookupSession to
//    a PoolQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PoolLookupSession to
 *  a PoolQuerySession.
 */

public abstract class AbstractQueryPoolLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractPoolLookupSession
    implements org.osid.provisioning.PoolLookupSession {

    private boolean activeonly    = false;
    private final org.osid.provisioning.PoolQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPoolLookupSession.
     *
     *  @param querySession the underlying pool query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPoolLookupSession(org.osid.provisioning.PoolQuerySession querySession) {
        nullarg(querySession, "pool query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>Pool</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPools() {
        return (this.session.canSearchPools());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pools in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active pools are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePoolView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive pools are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Pool</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Pool</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Pool</code> and
     *  retained for compatibility.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolId <code>Id</code> of the
     *          <code>Pool</code>
     *  @return the pool
     *  @throws org.osid.NotFoundException <code>poolId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>poolId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Pool getPool(org.osid.id.Id poolId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.PoolQuery query = getQuery();
        query.matchId(poolId, true);
        org.osid.provisioning.PoolList pools = this.session.getPoolsByQuery(query);
        if (pools.hasNext()) {
            return (pools.getNextPool());
        } 
        
        throw new org.osid.NotFoundException(poolId + " not found");
    }


    /**
     *  Gets a <code>PoolList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  pools specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Pools</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Pool</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>poolIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByIds(org.osid.id.IdList poolIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.PoolQuery query = getQuery();

        try (org.osid.id.IdList ids = poolIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPoolsByQuery(query));
    }


    /**
     *  Gets a <code>PoolList</code> corresponding to the given
     *  pool genus <code>Type</code> which does not include
     *  pools of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolGenusType a pool genus type 
     *  @return the returned <code>Pool</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByGenusType(org.osid.type.Type poolGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.PoolQuery query = getQuery();
        query.matchGenusType(poolGenusType, true);
        return (this.session.getPoolsByQuery(query));
    }


    /**
     *  Gets a <code>PoolList</code> corresponding to the given
     *  pool genus <code>Type</code> and include any additional
     *  pools with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolGenusType a pool genus type 
     *  @return the returned <code>Pool</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByParentGenusType(org.osid.type.Type poolGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.PoolQuery query = getQuery();
        query.matchParentGenusType(poolGenusType, true);
        return (this.session.getPoolsByQuery(query));
    }


    /**
     *  Gets a <code>PoolList</code> containing the given
     *  pool record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  poolRecordType a pool record type 
     *  @return the returned <code>Pool</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByRecordType(org.osid.type.Type poolRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.PoolQuery query = getQuery();
        query.matchRecordType(poolRecordType, true);
        return (this.session.getPoolsByQuery(query));
    }


    /**
     *  Gets a <code>PoolList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known pools or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  pools that are accessible through this session. 
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Pool</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.PoolQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getPoolsByQuery(query));        
    }


    /**
     *  Gets a <code> PoolList </code> by broker.
     *  
     *  In plenary mode, the returned list contains all known pools or
     *  an error results. Otherwise, the returned list may contain
     *  only those pools that are accessible through this session.
     *  
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pool are
     *  returned.
     *
     *  @param  brokerId a broker <code> Id </code> 
     *  @return the returned <code> Pool </code> list 
     *  @throws org.osid.NullArgumentException <code> brokerId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPoolsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.PoolQuery query = getQuery();
        query.matchBrokerId(brokerId, true);
        return (this.session.getPoolsByQuery(query));
    }


    /**
     *  Gets all <code>Pools</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  pools or an error results. Otherwise, the returned list
     *  may contain only those pools that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pools are returned that are currently
     *  active. In any status mode, active and inactive pools
     *  are returned.
     *
     *  @return a list of <code>Pools</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getPools()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.PoolQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPoolsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.PoolQuery getQuery() {
        org.osid.provisioning.PoolQuery query = this.session.getPoolQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
