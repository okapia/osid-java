//
// AbstractAdapterEntryLookupSession.java
//
//    An Entry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.dictionary.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Entry lookup session adapter.
 */

public abstract class AbstractAdapterEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.dictionary.EntryLookupSession {

    private final org.osid.dictionary.EntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterEntryLookupSession(org.osid.dictionary.EntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Dictionary/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Dictionary Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDictionaryId() {
        return (this.session.getDictionaryId());
    }


    /**
     *  Gets the {@code Dictionary} associated with this session.
     *
     *  @return the {@code Dictionary} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Dictionary getDictionary()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getDictionary());
    }


    /**
     *  Tests if this user can perform {@code Entry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupEntries() {
        return (this.session.canLookupEntries());
    }


    /**
     *  A complete view of the {@code Entry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeEntryView() {
        this.session.useComparativeEntryView();
        return;
    }


    /**
     *  A complete view of the {@code Entry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryEntryView() {
        this.session.usePlenaryEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include entries in dictionaries which are children
     *  of this dictionary in the dictionary hierarchy.
     */

    @OSID @Override
    public void useFederatedDictionaryView() {
        this.session.useFederatedDictionaryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this dictionary only.
     */

    @OSID @Override
    public void useIsolatedDictionaryView() {
        this.session.useIsolatedDictionaryView();
        return;
    }
    

    /**
     *  Gets the <code> Dictionary </code> entry associated with the
     *  given key and types. The <code> keyType </code> indicates the
     *  key object type and the <code> valueType </code> indicates the
     *  value object return.
     *
     *  @param  key the key of the entry to retrieve 
     *  @param  keyType the key type of the entry to retrieve 
     *  @param  valueType the value type of the entry to retrieve 
     *  @return the returned <code> object </code> 
     *  @throws org.osid.InvalidArgumentException <code> key </code> is not of 
     *          <code> keyType </code> 
     *  @throws org.osid.NotFoundException no entry found 
     *  @throws org.osid.NullArgumentException <code> key, keyType, </code> or 
     *          <code> valueType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public java.lang.Object retrieveEntry(java.lang.Object key, 
                                          org.osid.type.Type keyType, 
                                          org.osid.type.Type valueType)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.retrieveEntry(key, keyType, valueType));
    }

     
    /**
     *  Gets the {@code Entry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code Entry} may
     *  have a different {@code Id} than requested, such as the case
     *  where a duplicate {@code Id} was assigned to a {@code Entry}
     *  and retained for compatibility.
     *
     *  @param entryId {@code Id} of the {@code Entry}
     *  @return the entry
     *  @throws org.osid.NotFoundException {@code entryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code entryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.Entry getEntry(org.osid.id.Id entryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntry(entryId));
    }


    /**
     *  Gets an {@code EntryList} corresponding to the given {@code
     *  IdList}.
     *
     *  In plenary mode, the returned list contains all of the entries
     *  specified in the {@code Id} list, in the order of the list,
     *  including duplicates, or an error results if an {@code Id} in
     *  the supplied list is not found or inaccessible. Otherwise,
     *  inaccessible {@code Entries} may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  @param  entryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Entry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code entryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByIds(org.osid.id.IdList entryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByIds(entryIds));
    }


    /**
     *  Gets an {@code EntryList} corresponding to the given
     *  entry genus {@code Type} which does not include
     *  entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned {@code Entry} list
     *  @throws org.osid.NullArgumentException
     *          {@code entryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByGenusType(entryGenusType));
    }


    /**
     *  Gets an {@code EntryList} corresponding to the given entry
     *  genus {@code Type} and include any additional entries with
     *  genus types derived from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known entries
     *  or an error results. Otherwise, the returned list may contain
     *  only those entries that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned {@code Entry} list
     *  @throws org.osid.NullArgumentException
     *          {@code entryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByParentGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByParentGenusType(entryGenusType));
    }


    /**
     *  Gets an {@code EntryList} containing the given
     *  entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  entryRecordType an entry record type 
     *  @return the returned {@code Entry} list
     *  @throws org.osid.NullArgumentException
     *          {@code entryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByRecordType(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByRecordType(entryRecordType));
    }


    /**
     *  Gets all the {@code Dictionary} entries matching the given key
     *  {@code Type.} In plenary mode, the returned list contains all
     *  of the entries specified in the Id list, in the order of the
     *  list, including duplicates, or an error results if an Id in
     *  the supplied list is not found or inaccessible. Otherwise,
     *  inaccessible Entry elements may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  @param  keyType the type of the key to match 
     *  @return the list of entries matching {@code keyType} 
     *  @throws org.osid.NullArgumentException {@code keyType} is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByKeyType(org.osid.type.Type keyType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByKeyType(keyType));
    }


    /**
     *  Gets all the {@code Dictionary} entries matching the given key
     *  and value {@code Type.} In plenary mode, the returned list
     *  contains all of the entries specified in the Id list, in the
     *  order of the list, including duplicates, or an error results
     *  if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible Entry elements may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  keyType the type of the key to match 
     *  @param  valueType the type of the value to match 
     *  @return the list of entries matching {@code keyType} 
     *  @throws org.osid.NullArgumentException {@code keyType} or 
     *          {@code valueType} is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    public org.osid.dictionary.EntryList getEntriesByKeyTypeAndValueType(org.osid.type.Type keyType, 
                                                                         org.osid.type.Type valueType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByKeyTypeAndValueType(keyType, valueType));
    }


    /**
     *  Gets all the {@code Dictionary} entries matching the given key
     *  and key {@code Type}. In plenary mode, the returned list
     *  contains all of the entries specified in the Id list, in the
     *  order of the list, including duplicates, or an error results
     *  if an Id in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible Entry elements may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  key the key to match 
     *  @param  keyType the type of the value to match 
     *  @return the list of entries matching {@code keyType} 
     *  @throws org.osid.NullArgumentException {@code key} or {@code 
     *          keyType} is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntriesByKeyAndKeyType(java.lang.Object key, 
                                                                   org.osid.type.Type keyType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntriesByKeyAndKeyType(key, keyType));
    }


    /**
     *  Gets all {@code Entries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  entries or an error results. Otherwise, the returned list
     *  may contain only those entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Entries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.EntryList getEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getEntries());
    }
}
