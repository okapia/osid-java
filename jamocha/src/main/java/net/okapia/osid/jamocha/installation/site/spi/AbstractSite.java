//
// AbstractSite.java
//
//     Defines a Site.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.site.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Site</code>.
 */

public abstract class AbstractSite
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.installation.Site {

    private String path;

    private final java.util.Collection<org.osid.installation.records.SiteRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the path to this site. 
     *
     *  @return the path 
     */

    @OSID @Override
    public String getPath() {
        return (this.path);
    }


    /**
     *  Sets the path.
     *
     *  @param path a path
     *  @throws org.osid.NullArgumentException
     *          <code>path</code> is <code>null</code>
     */

    protected void setPath(String path) {
        nullarg(path, "path");
        this.path = path;
        return;
    }


    /**
     *  Tests if this site supports the given record
     *  <code>Type</code>.
     *
     *  @param  siteRecordType a site record type 
     *  @return <code>true</code> if the siteRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>siteRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type siteRecordType) {
        for (org.osid.installation.records.SiteRecord record : this.records) {
            if (record.implementsRecordType(siteRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Site</code>
     *  record <code>Type</code>.
     *
     *  @param  siteRecordType the site record type 
     *  @return the site record 
     *  @throws org.osid.NullArgumentException
     *          <code>siteRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(siteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.SiteRecord getSiteRecord(org.osid.type.Type siteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.SiteRecord record : this.records) {
            if (record.implementsRecordType(siteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(siteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this site. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param siteRecord the site record
     *  @param siteRecordType site record type
     *  @throws org.osid.NullArgumentException
     *          <code>siteRecord</code> or
     *          <code>siteRecordTypesite</code> is
     *          <code>null</code>
     */
            
    protected void addSiteRecord(org.osid.installation.records.SiteRecord siteRecord, 
                                 org.osid.type.Type siteRecordType) {
        
        nullarg(siteRecord, "site record");
        addRecordType(siteRecordType);
        this.records.add(siteRecord);
        
        return;
    }
}
