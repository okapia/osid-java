//
// AbstractOfferingRulesManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractOfferingRulesManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.offering.rules.OfferingRulesManager,
               org.osid.offering.rules.OfferingRulesProxyManager {

    private final Types canonicalUnitEnablerRecordTypes    = new TypeRefSet();
    private final Types canonicalUnitEnablerSearchRecordTypes= new TypeRefSet();

    private final Types canonicalUnitProcessorRecordTypes  = new TypeRefSet();
    private final Types canonicalUnitProcessorSearchRecordTypes= new TypeRefSet();

    private final Types canonicalUnitProcessorEnablerRecordTypes= new TypeRefSet();
    private final Types canonicalUnitProcessorEnablerSearchRecordTypes= new TypeRefSet();

    private final Types offeringConstrainerRecordTypes     = new TypeRefSet();
    private final Types offeringConstrainerSearchRecordTypes= new TypeRefSet();

    private final Types offeringConstrainerEnablerRecordTypes= new TypeRefSet();
    private final Types offeringConstrainerEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractOfferingRulesManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractOfferingRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up canonical unit enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying canonical unit enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching canonical unit enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a canonical unit enabler administrative service is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a canonical unit enabler notification service is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a canonical unit enabler catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a canonical unit enabler catalogue 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerCatalogue() {
        return (false);
    }


    /**
     *  Tests if a canonical unit enabler catalogue service is supported. 
     *
     *  @return <code> true </code> if canonical unit enabler catalogue 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerCatalogueAssignment() {
        return (false);
    }


    /**
     *  Tests if a canonical unit enabler catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a canonical unit enabler catalogue 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerSmartCatalogue() {
        return (false);
    }


    /**
     *  Tests if looking up canonical unit processor is supported. 
     *
     *  @return <code> true </code> if canonical unit processor lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorLookup() {
        return (false);
    }


    /**
     *  Tests if querying canonical unit processor is supported. 
     *
     *  @return <code> true </code> if canonical unit processor query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorQuery() {
        return (false);
    }


    /**
     *  Tests if searching canonical unit processor is supported. 
     *
     *  @return <code> true </code> if canonical unit processor search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorSearch() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor administration 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorAdmin() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor notification service is supported. 
     *
     *  @return <code> true </code> if canonical unit processor notification 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorNotification() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a canonical unit processor catalogue 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorCatalogue() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor catalogue service is supported. 
     *
     *  @return <code> true </code> if canonical unit processor catalogue 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorCatalogueAssignment() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if a canonical unit processor catalogue 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorSmartCatalogue() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor rule lookup service is supported. 
     *
     *  @return <code> true </code> if a canonical unit processor rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up canonical unit processor enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler lookup 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying canonical unit processor enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler query 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching canonical unit processor enablers is supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler search 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler 
     *          notification is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor enabler catalogue lookup service 
     *  is supported. 
     *
     *  @return <code> true </code> if a canonical unit processor enabler 
     *          catalogue lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerCatalogue() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor enabler catalogue service is 
     *  supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler 
     *          catalogue assignment service is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerCatalogueAssignment() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor enabler catalogue lookup service 
     *  is supported. 
     *
     *  @return <code> true </code> if a canonical unit processor enabler 
     *          catalogue service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerSmartCatalogue() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an processor enabler rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a canonical unit processor enabler rule application service 
     *  is supported. 
     *
     *  @return <code> true </code> if canonical unit processor enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up offering constrainer is supported. 
     *
     *  @return <code> true </code> if offering constrainer lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerLookup() {
        return (false);
    }


    /**
     *  Tests if querying offering constrainer is supported. 
     *
     *  @return <code> true </code> if offering constrainer query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerQuery() {
        return (false);
    }


    /**
     *  Tests if searching offering constrainer is supported. 
     *
     *  @return <code> true </code> if offering constrainer search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerSearch() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer administrative service is supported. 
     *
     *  @return <code> true </code> if offering constrainer administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerAdmin() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer notification service is supported. 
     *
     *  @return <code> true </code> if offering constrainer notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerNotification() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer catalogue 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerCatalogue() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer catalogue service is supported. 
     *
     *  @return <code> true </code> if offering constrainer catalogue 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerCatalogueAssignment() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer catalogue 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerSmartCatalogue() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer rule lookup service is supported. 
     *
     *  @return <code> true </code> if an offering constrainer rule lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerRuleApplication() {
        return (false);
    }


    /**
     *  Tests if looking up offering constrainer enablers is supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying offering constrainer enablers is supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler query is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching offering constrainer enablers is supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer enabler administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler 
     *          administration is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer enabler notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler 
     *          notification is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer enabler catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer enabler 
     *          catalogue lookup service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerCatalogue() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer enabler catalogue service is 
     *  supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler catalogue 
     *          assignment service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerCatalogueAssignment() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer enabler catalogue lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer enabler 
     *          catalogue service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerSmartCatalogue() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer enabler rule lookup service is 
     *  supported. 
     *
     *  @return <code> true </code> if an offering constrainer enabler rule 
     *          lookup service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if an offering constrainer enabler rule application service is 
     *  supported. 
     *
     *  @return <code> true </code> if offering constrainer enabler rule 
     *          application service is supported, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> CanonicalUnitEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> CanonicalUnitEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.canonicalUnitEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CanonicalUnitEnabler </code> record type is 
     *  supported. 
     *
     *  @param  canonicalUnitEnablerRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerRecordType(org.osid.type.Type canonicalUnitEnablerRecordType) {
        return (this.canonicalUnitEnablerRecordTypes.contains(canonicalUnitEnablerRecordType));
    }


    /**
     *  Adds support for a canonical unit enabler record type.
     *
     *  @param canonicalUnitEnablerRecordType a canonical unit enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitEnablerRecordType</code> is <code>null</code>
     */

    protected void addCanonicalUnitEnablerRecordType(org.osid.type.Type canonicalUnitEnablerRecordType) {
        this.canonicalUnitEnablerRecordTypes.add(canonicalUnitEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a canonical unit enabler record type.
     *
     *  @param canonicalUnitEnablerRecordType a canonical unit enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitEnablerRecordType</code> is <code>null</code>
     */

    protected void removeCanonicalUnitEnablerRecordType(org.osid.type.Type canonicalUnitEnablerRecordType) {
        this.canonicalUnitEnablerRecordTypes.remove(canonicalUnitEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CanonicalUnitEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CanonicalUnitEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.canonicalUnitEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CanonicalUnitEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  canonicalUnitEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitEnablerSearchRecordType(org.osid.type.Type canonicalUnitEnablerSearchRecordType) {
        return (this.canonicalUnitEnablerSearchRecordTypes.contains(canonicalUnitEnablerSearchRecordType));
    }


    /**
     *  Adds support for a canonical unit enabler search record type.
     *
     *  @param canonicalUnitEnablerSearchRecordType a canonical unit enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addCanonicalUnitEnablerSearchRecordType(org.osid.type.Type canonicalUnitEnablerSearchRecordType) {
        this.canonicalUnitEnablerSearchRecordTypes.add(canonicalUnitEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a canonical unit enabler search record type.
     *
     *  @param canonicalUnitEnablerSearchRecordType a canonical unit enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeCanonicalUnitEnablerSearchRecordType(org.osid.type.Type canonicalUnitEnablerSearchRecordType) {
        this.canonicalUnitEnablerSearchRecordTypes.remove(canonicalUnitEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CanonicalUnitProcessor </code> record types. 
     *
     *  @return a list containing the supported <code> CanonicalUnitProcessor 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitProcessorRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.canonicalUnitProcessorRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CanonicalUnitProcessor </code> record type 
     *  is supported. 
     *
     *  @param  canonicalUnitProcessorRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitProcessor </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorRecordType(org.osid.type.Type canonicalUnitProcessorRecordType) {
        return (this.canonicalUnitProcessorRecordTypes.contains(canonicalUnitProcessorRecordType));
    }


    /**
     *  Adds support for a canonical unit processor record type.
     *
     *  @param canonicalUnitProcessorRecordType a canonical unit processor record type
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitProcessorRecordType</code> is <code>null</code>
     */

    protected void addCanonicalUnitProcessorRecordType(org.osid.type.Type canonicalUnitProcessorRecordType) {
        this.canonicalUnitProcessorRecordTypes.add(canonicalUnitProcessorRecordType);
        return;
    }


    /**
     *  Removes support for a canonical unit processor record type.
     *
     *  @param canonicalUnitProcessorRecordType a canonical unit processor record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitProcessorRecordType</code> is <code>null</code>
     */

    protected void removeCanonicalUnitProcessorRecordType(org.osid.type.Type canonicalUnitProcessorRecordType) {
        this.canonicalUnitProcessorRecordTypes.remove(canonicalUnitProcessorRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CanonicalUnitProcessor </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> CanonicalUnitProcessor 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitProcessorSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.canonicalUnitProcessorSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CanonicalUnitProcessor </code> search record 
     *  type is supported. 
     *
     *  @param  canonicalUnitProcessorSearchRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitProcessor </code> search 
     *          record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorSearchRecordType(org.osid.type.Type canonicalUnitProcessorSearchRecordType) {
        return (this.canonicalUnitProcessorSearchRecordTypes.contains(canonicalUnitProcessorSearchRecordType));
    }


    /**
     *  Adds support for a canonical unit processor search record type.
     *
     *  @param canonicalUnitProcessorSearchRecordType a canonical unit processor search record type
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void addCanonicalUnitProcessorSearchRecordType(org.osid.type.Type canonicalUnitProcessorSearchRecordType) {
        this.canonicalUnitProcessorSearchRecordTypes.add(canonicalUnitProcessorSearchRecordType);
        return;
    }


    /**
     *  Removes support for a canonical unit processor search record type.
     *
     *  @param canonicalUnitProcessorSearchRecordType a canonical unit processor search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitProcessorSearchRecordType</code> is <code>null</code>
     */

    protected void removeCanonicalUnitProcessorSearchRecordType(org.osid.type.Type canonicalUnitProcessorSearchRecordType) {
        this.canonicalUnitProcessorSearchRecordTypes.remove(canonicalUnitProcessorSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CanonicalUnitProcessorEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          CanonicalUnitProcessorEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitProcessorEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.canonicalUnitProcessorEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CanonicalUnitProcessorEnabler </code> record 
     *  type is supported. 
     *
     *  @param  canonicalUnitProcessorEnablerRecordType a <code> Type </code> 
     *          indicating a <code> CanonicalUnitProcessorEnabler </code> 
     *          record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerRecordType(org.osid.type.Type canonicalUnitProcessorEnablerRecordType) {
        return (this.canonicalUnitProcessorEnablerRecordTypes.contains(canonicalUnitProcessorEnablerRecordType));
    }


    /**
     *  Adds support for a canonical unit processor enabler record type.
     *
     *  @param canonicalUnitProcessorEnablerRecordType a canonical unit processor enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void addCanonicalUnitProcessorEnablerRecordType(org.osid.type.Type canonicalUnitProcessorEnablerRecordType) {
        this.canonicalUnitProcessorEnablerRecordTypes.add(canonicalUnitProcessorEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a canonical unit processor enabler record type.
     *
     *  @param canonicalUnitProcessorEnablerRecordType a canonical unit processor enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitProcessorEnablerRecordType</code> is <code>null</code>
     */

    protected void removeCanonicalUnitProcessorEnablerRecordType(org.osid.type.Type canonicalUnitProcessorEnablerRecordType) {
        this.canonicalUnitProcessorEnablerRecordTypes.remove(canonicalUnitProcessorEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> CanonicalUnitProcessorEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          CanonicalUnitProcessorEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCanonicalUnitProcessorEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.canonicalUnitProcessorEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> CanonicalUnitProcessorEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  canonicalUnitProcessorEnablerSearchRecordType a <code> Type 
     *          </code> indicating a <code> CanonicalUnitProcessorEnabler 
     *          </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorEnablerSearchRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCanonicalUnitProcessorEnablerSearchRecordType(org.osid.type.Type canonicalUnitProcessorEnablerSearchRecordType) {
        return (this.canonicalUnitProcessorEnablerSearchRecordTypes.contains(canonicalUnitProcessorEnablerSearchRecordType));
    }


    /**
     *  Adds support for a canonical unit processor enabler search record type.
     *
     *  @param canonicalUnitProcessorEnablerSearchRecordType a canonical unit processor enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addCanonicalUnitProcessorEnablerSearchRecordType(org.osid.type.Type canonicalUnitProcessorEnablerSearchRecordType) {
        this.canonicalUnitProcessorEnablerSearchRecordTypes.add(canonicalUnitProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a canonical unit processor enabler search record type.
     *
     *  @param canonicalUnitProcessorEnablerSearchRecordType a canonical unit processor enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>canonicalUnitProcessorEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeCanonicalUnitProcessorEnablerSearchRecordType(org.osid.type.Type canonicalUnitProcessorEnablerSearchRecordType) {
        this.canonicalUnitProcessorEnablerSearchRecordTypes.remove(canonicalUnitProcessorEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> OfferingConstrainer </code> record types. 
     *
     *  @return a list containing the supported <code> OfferingConstrainer 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingConstrainerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offeringConstrainerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> OfferingConstrainer </code> record type is 
     *  supported. 
     *
     *  @param  offeringConstrainerRecordType a <code> Type </code> indicating 
     *          an <code> OfferingConstrainer </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerRecordType(org.osid.type.Type offeringConstrainerRecordType) {
        return (this.offeringConstrainerRecordTypes.contains(offeringConstrainerRecordType));
    }


    /**
     *  Adds support for an offering constrainer record type.
     *
     *  @param offeringConstrainerRecordType an offering constrainer record type
     *  @throws org.osid.NullArgumentException
     *  <code>offeringConstrainerRecordType</code> is <code>null</code>
     */

    protected void addOfferingConstrainerRecordType(org.osid.type.Type offeringConstrainerRecordType) {
        this.offeringConstrainerRecordTypes.add(offeringConstrainerRecordType);
        return;
    }


    /**
     *  Removes support for an offering constrainer record type.
     *
     *  @param offeringConstrainerRecordType an offering constrainer record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offeringConstrainerRecordType</code> is <code>null</code>
     */

    protected void removeOfferingConstrainerRecordType(org.osid.type.Type offeringConstrainerRecordType) {
        this.offeringConstrainerRecordTypes.remove(offeringConstrainerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> OfferingConstrainer </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> OfferingConstrainer 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingConstrainerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offeringConstrainerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> OfferingConstrainer </code> search record 
     *  type is supported. 
     *
     *  @param  offeringConstrainerSearchRecordType a <code> Type </code> 
     *          indicating an <code> OfferingConstrainer </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerSearchRecordType(org.osid.type.Type offeringConstrainerSearchRecordType) {
        return (this.offeringConstrainerSearchRecordTypes.contains(offeringConstrainerSearchRecordType));
    }


    /**
     *  Adds support for an offering constrainer search record type.
     *
     *  @param offeringConstrainerSearchRecordType an offering constrainer search record type
     *  @throws org.osid.NullArgumentException
     *  <code>offeringConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void addOfferingConstrainerSearchRecordType(org.osid.type.Type offeringConstrainerSearchRecordType) {
        this.offeringConstrainerSearchRecordTypes.add(offeringConstrainerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an offering constrainer search record type.
     *
     *  @param offeringConstrainerSearchRecordType an offering constrainer search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offeringConstrainerSearchRecordType</code> is <code>null</code>
     */

    protected void removeOfferingConstrainerSearchRecordType(org.osid.type.Type offeringConstrainerSearchRecordType) {
        this.offeringConstrainerSearchRecordTypes.remove(offeringConstrainerSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> OfferingConstrainerEnabler </code> record 
     *  types. 
     *
     *  @return a list containing the supported <code> 
     *          OfferingConstrainerEnabler </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingConstrainerEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offeringConstrainerEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> OfferingConstrainerEnabler </code> record 
     *  type is supported. 
     *
     *  @param  offeringConstrainerEnablerRecordType a <code> Type </code> 
     *          indicating an <code> OfferingConstrainerEnabler </code> record 
     *          type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerRecordType(org.osid.type.Type offeringConstrainerEnablerRecordType) {
        return (this.offeringConstrainerEnablerRecordTypes.contains(offeringConstrainerEnablerRecordType));
    }


    /**
     *  Adds support for an offering constrainer enabler record type.
     *
     *  @param offeringConstrainerEnablerRecordType an offering constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>offeringConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void addOfferingConstrainerEnablerRecordType(org.osid.type.Type offeringConstrainerEnablerRecordType) {
        this.offeringConstrainerEnablerRecordTypes.add(offeringConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Removes support for an offering constrainer enabler record type.
     *
     *  @param offeringConstrainerEnablerRecordType an offering constrainer enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offeringConstrainerEnablerRecordType</code> is <code>null</code>
     */

    protected void removeOfferingConstrainerEnablerRecordType(org.osid.type.Type offeringConstrainerEnablerRecordType) {
        this.offeringConstrainerEnablerRecordTypes.remove(offeringConstrainerEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> OfferingConstrainerEnabler </code> search 
     *  record types. 
     *
     *  @return a list containing the supported <code> 
     *          OfferingConstrainerEnabler </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOfferingConstrainerEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.offeringConstrainerEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> OfferingConstrainerEnabler </code> search 
     *  record type is supported. 
     *
     *  @param  offeringConstrainerEnablerSearchRecordType a <code> Type 
     *          </code> indicating an <code> OfferingConstrainerEnabler 
     *          </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerSearchRecordType </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public boolean supportsOfferingConstrainerEnablerSearchRecordType(org.osid.type.Type offeringConstrainerEnablerSearchRecordType) {
        return (this.offeringConstrainerEnablerSearchRecordTypes.contains(offeringConstrainerEnablerSearchRecordType));
    }


    /**
     *  Adds support for an offering constrainer enabler search record type.
     *
     *  @param offeringConstrainerEnablerSearchRecordType an offering constrainer enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>offeringConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addOfferingConstrainerEnablerSearchRecordType(org.osid.type.Type offeringConstrainerEnablerSearchRecordType) {
        this.offeringConstrainerEnablerSearchRecordTypes.add(offeringConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for an offering constrainer enabler search record type.
     *
     *  @param offeringConstrainerEnablerSearchRecordType an offering constrainer enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>offeringConstrainerEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeOfferingConstrainerEnablerSearchRecordType(org.osid.type.Type offeringConstrainerEnablerSearchRecordType) {
        this.offeringConstrainerEnablerSearchRecordTypes.remove(offeringConstrainerEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler lookup service. 
     *
     *  @return a <code> CanonicalUnitEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerLookupSession getCanonicalUnitEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerLookupSession getCanonicalUnitEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerLookupSession getCanonicalUnitEnablerLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerLookupSession getCanonicalUnitEnablerLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler query service. 
     *
     *  @return a <code> CanonicalUnitEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerQuerySession getCanonicalUnitEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerQuerySession getCanonicalUnitEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerQuerySession getCanonicalUnitEnablerQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerQuerySession getCanonicalUnitEnablerQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler search service. 
     *
     *  @return a <code> CanonicalUnitEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerSearchSession getCanonicalUnitEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerSearchSession getCanonicalUnitEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enablers earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerSearchSession getCanonicalUnitEnablerSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enablers earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerSearchSession getCanonicalUnitEnablerSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler administration service. 
     *
     *  @return a <code> CanonicalUnitEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerAdminSession getCanonicalUnitEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerAdminSession getCanonicalUnitEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerAdminSession getCanonicalUnitEnablerAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerAdminSession getCanonicalUnitEnablerAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler notification service. 
     *
     *  @param  canonicalUnitEnablerReceiver the notification callback 
     *  @return a <code> CanonicalUnitEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerNotificationSession getCanonicalUnitEnablerNotificationSession(org.osid.offering.rules.CanonicalUnitEnablerReceiver canonicalUnitEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler notification service. 
     *
     *  @param  canonicalUnitEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerNotificationSession getCanonicalUnitEnablerNotificationSession(org.osid.offering.rules.CanonicalUnitEnablerReceiver canonicalUnitEnablerReceiver, 
                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler notification service for the given catalogue. 
     *
     *  @param  canonicalUnitEnablerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerReceiver </code> or <code> catalogueId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerNotificationSession getCanonicalUnitEnablerNotificationSessionForCatalogue(org.osid.offering.rules.CanonicalUnitEnablerReceiver canonicalUnitEnablerReceiver, 
                                                                                                                                  org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler notification service for the given catalogue. 
     *
     *  @param  canonicalUnitEnablerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitEnablerReceiver, catalogueId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerNotificationSession getCanonicalUnitEnablerNotificationSessionForCatalogue(org.osid.offering.rules.CanonicalUnitEnablerReceiver canonicalUnitEnablerReceiver, 
                                                                                                                                  org.osid.id.Id catalogueId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup canonical unit 
     *  enabler/catalogue mappings for canonical unit enablers. 
     *
     *  @return a <code> CanonicalUnitEnablerCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerCatalogueSession getCanonicalUnitEnablerCatalogueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup canonical unit 
     *  enabler/catalogue mappings for canonical unit enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerCatalogueSession getCanonicalUnitEnablerCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  canonical unit enablers to catalogues. 
     *
     *  @return a <code> CanonicalUnitEnablerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerCatalogueAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerCatalogueAssignmentSession getCanonicalUnitEnablerCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  canonical unit enablers to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerCatalogueAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerCatalogueAssignmentSession getCanonicalUnitEnablerCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage canonical unit enabler 
     *  smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitEnablerSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerSmartCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerSmartCatalogueSession getCanonicalUnitEnablerSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage canonical unit enabler 
     *  smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerSmartCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerSmartCatalogueSession getCanonicalUnitEnablerSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> CanonicalUnitEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleLookupSession getCanonicalUnitEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleLookupSession getCanonicalUnitEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler mapping lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleLookupSession getCanonicalUnitEnablerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler mapping lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleLookupSession getCanonicalUnitEnablerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler assignment service. 
     *
     *  @return a <code> CanonicalUnitEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleApplicationSession getCanonicalUnitEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleApplicationSession getCanonicalUnitEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler assignment service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleApplicationSession getCanonicalUnitEnablerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitEnablerRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  enabler assignment service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitEnablerRuleApplicationSession getCanonicalUnitEnablerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitEnablerRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor lookup service. 
     *
     *  @return a <code> CanonicalUnitProcessorLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorLookupSession getCanonicalUnitProcessorLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorLookupSession getCanonicalUnitProcessorLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorLookupSession getCanonicalUnitProcessorLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorLookupSession getCanonicalUnitProcessorLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor query service. 
     *
     *  @return a <code> CanonicalUnitProcessorQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQuerySession getCanonicalUnitProcessorQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQuerySession getCanonicalUnitProcessorQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQuerySession getCanonicalUnitProcessorQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorQuerySession getCanonicalUnitProcessorQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor search service. 
     *
     *  @return a <code> CanonicalUnitProcessorSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorSearchSession getCanonicalUnitProcessorSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorSearchSession getCanonicalUnitProcessorSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorSearchSession getCanonicalUnitProcessorSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorSearchSession getCanonicalUnitProcessorSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor administration service. 
     *
     *  @return a <code> CanonicalUnitProcessorAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorAdminSession getCanonicalUnitProcessorAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorAdminSession getCanonicalUnitProcessorAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorAdminSession getCanonicalUnitProcessorAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorAdminSession getCanonicalUnitProcessorAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor notification service. 
     *
     *  @param  canonicalUnitProcessorReceiver the notification callback 
     *  @return a <code> CanonicalUnitProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorNotificationSession getCanonicalUnitProcessorNotificationSession(org.osid.offering.rules.CanonicalUnitProcessorReceiver canonicalUnitProcessorReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor notification service. 
     *
     *  @param  canonicalUnitProcessorReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessoReceiver </code> or <code> proxy </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorNotificationSession getCanonicalUnitProcessorNotificationSession(org.osid.offering.rules.CanonicalUnitProcessorReceiver canonicalUnitProcessorReceiver, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor notification service for the given catalogue. 
     *
     *  @param  canonicalUnitProcessorReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorReceiver </code> or <code> catalogueId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorNotificationSession getCanonicalUnitProcessorNotificationSessionForCatalogue(org.osid.offering.rules.CanonicalUnitProcessorReceiver canonicalUnitProcessorReceiver, 
                                                                                                                                      org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor notification service for the given catalogue. 
     *
     *  @param  canonicalUnitProcessoReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessoReceiver, catalogueId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorNotificationSession getCanonicalUnitProcessorNotificationSessionForCatalogue(org.osid.offering.rules.CanonicalUnitProcessorReceiver canonicalUnitProcessoReceiver, 
                                                                                                                                      org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup canonical unit 
     *  processor/catalogue mappings for canonical unit processors. 
     *
     *  @return a <code> CanonicalUnitProcessorCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorCatalogueSession getCanonicalUnitProcessorCatalogueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup canonical unit 
     *  processor/catalogue mappings for canonical unit processors. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorCatalogueSession getCanonicalUnitProcessorCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  canonical unit processor to catalogues. 
     *
     *  @return a <code> CanonicalUnitProcessorCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorCatalogueAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorCatalogueAssignmentSession getCanonicalUnitProcessorCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  canonical unit processor to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorCatalogueAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorCatalogueAssignmentSession getCanonicalUnitProcessorCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage canonical unit processor 
     *  smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorSmartCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorSmartCatalogueSession getCanonicalUnitProcessorSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage canonical unit processor 
     *  smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorSmartCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorSmartCatalogueSession getCanonicalUnitProcessorSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor canonical unit mapping lookup service for looking up the 
     *  rules applied to the catalogue. 
     *
     *  @return a <code> CanonicalUnitProcessorRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleLookupSession getCanonicalUnitProcessorRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor mapping lookup service for looking up the rules applied to 
     *  the catalogue. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleLookupSession getCanonicalUnitProcessorRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor mapping lookup service for the given catalogue for looking 
     *  up rules applied to a catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleLookupSession getCanonicalUnitProcessorRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor mapping lookup service for the given catalogue for looking 
     *  up rules applied to a catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleLookupSession getCanonicalUnitProcessorRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor assignment service. 
     *
     *  @return a <code> CanonicalUnitProcessorRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleApplicationSession getCanonicalUnitProcessorRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor assignment service to apply to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleApplicationSession getCanonicalUnitProcessorRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor mapping application service for the given catalogue for looking 
     *  up rules applied to a catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleApplicationSession getCanonicalUnitProcessorRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor mapping application service for the given catalogue for looking 
     *  up rules applied to a catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorRuleApplicationSession getCanonicalUnitProcessorRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler lookup service. 
     *
     *  @return a <code> CanonicalUnitProcessorEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession getCanonicalUnitProcessorEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession getCanonicalUnitProcessorEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession getCanonicalUnitProcessorEnablerLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerLookupSession getCanonicalUnitProcessorEnablerLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler query service. 
     *
     *  @return a <code> CanonicalUnitProcessorEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerQuerySession getCanonicalUnitProcessorEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerQuerySession getCanonicalUnitProcessorEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerQuerySession getCanonicalUnitProcessorEnablerQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerQuerySession getCanonicalUnitProcessorEnablerQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler search service. 
     *
     *  @return a <code> CanonicalUnitProcessorEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerSearch() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerSearchSession getCanonicalUnitProcessorEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerSearch() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerSearchSession getCanonicalUnitProcessorEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enablers earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerSearch() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerSearchSession getCanonicalUnitProcessorEnablerSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enablers earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerSearch() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerSearchSession getCanonicalUnitProcessorEnablerSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler administration service. 
     *
     *  @return a <code> CanonicalUnitProcessorEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerAdminSession getCanonicalUnitProcessorEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerAdminSession getCanonicalUnitProcessorEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerAdminSession getCanonicalUnitProcessorEnablerAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerAdminSession getCanonicalUnitProcessorEnablerAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler notification service. 
     *
     *  @param  canonicalUnitProcessorEnablerReceiver the notification 
     *          callback 
     *  @return a <code> CanonicalUnitProcessorEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorEnablerReceiver </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerNotificationSession getCanonicalUnitProcessorEnablerNotificationSession(org.osid.offering.rules.CanonicalUnitProcessorEnablerReceiver canonicalUnitProcessorEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler notification service. 
     *
     *  @param  canonicalUnitProcessoEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessoEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerNotificationSession getCanonicalUnitProcessorEnablerNotificationSession(org.osid.offering.rules.CanonicalUnitProcessorEnablerReceiver canonicalUnitProcessoEnablerReceiver, 
                                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler notification service for the given catalogue. 
     *
     *  @param  canonicalUnitProcessorEnablerReceiver the notification 
     *          callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessorEnablerReceiver </code> or <code> 
     *          catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerNotificationSession getCanonicalUnitProcessorEnablerNotificationSessionForCatalogue(org.osid.offering.rules.CanonicalUnitProcessorEnablerReceiver canonicalUnitProcessorEnablerReceiver, 
                                                                                                                                                    org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler notification service for the given catalogue. 
     *
     *  @param  canonicalUnitProcessoEnablerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          canonicalUnitProcessoEnablerReceiver, catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerNotificationSession getCanonicalUnitProcessorEnablerNotificationSessionForCatalogue(org.osid.offering.rules.CanonicalUnitProcessorEnablerReceiver canonicalUnitProcessoEnablerReceiver, 
                                                                                                                                                    org.osid.id.Id catalogueId, 
                                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup canonical unit processor 
     *  enabler/catalogue mappings for canonical unit processor enablers. 
     *
     *  @return a <code> CanonicalUnitProcessorEnablerCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerCatalogueSession getCanonicalUnitProcessorEnablerCatalogueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup canonical unit processor 
     *  enabler/catalogue mappings for canonical unit processor enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerCatalogueSession getCanonicalUnitProcessorEnablerCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  canonical unit processor enablers to catalogues. 
     *
     *  @return a <code> 
     *          CanonicalUnitProcessorEnablerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerCatalogueAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerCatalogueAssignmentSession getCanonicalUnitProcessorEnablerCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  canonical unit processor enablers to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> 
     *          CanonicalUnitProcessorEnablerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerCatalogueAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerCatalogueAssignmentSession getCanonicalUnitProcessorEnablerCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage canonical unit processor 
     *  enabler smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorEnablerSmartCatalogueSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerSmartCatalogue() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerSmartCatalogueSession getCanonicalUnitProcessorEnablerSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage canonical unit processor 
     *  enabler smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerSmartCatalogueSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerSmartCatalogue() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerSmartCatalogueSession getCanonicalUnitProcessorEnablerSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler mapping lookup service. 
     *
     *  @return a <code> CanonicalUnitProcessorEnablerRuleLookupSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleLookupSession getCanonicalUnitProcessorEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerRuleLookupSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleLookupSession getCanonicalUnitProcessorEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler mapping lookup service. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return a <code> CanonicalUnitProcessorEnablerRuleLookupSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleLookupSession getCanonicalUnitProcessorEnablerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler mapping lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerRuleLookupSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleLookupSession getCanonicalUnitProcessorEnablerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler assignment service. 
     *
     *  @return a <code> CanonicalUnitProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleApplication() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleApplicationSession getCanonicalUnitProcessorEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleApplication() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleApplicationSession getCanonicalUnitProcessorEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler assignment service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return z <code> CanonicalUnitProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleApplication() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleApplicationSession getCanonicalUnitProcessorEnablerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getCanonicalUnitProcessorEnablerRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the canonical unit 
     *  processor enabler assignment service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CanonicalUnitProcessorEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCanonicalUnitProcessorEnablerRuleApplication() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.CanonicalUnitProcessorEnablerRuleApplicationSession getCanonicalUnitProcessorEnablerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getCanonicalUnitProcessorEnablerRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer lookup service. 
     *
     *  @return an <code> OfferingConstrainerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerLookupSession getOfferingConstrainerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerLookupSession getOfferingConstrainerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerLookupSession getOfferingConstrainerLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerLookupSession getOfferingConstrainerLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer query service. 
     *
     *  @return an <code> OfferingConstrainerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQuerySession getOfferingConstrainerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQuerySession getOfferingConstrainerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQuerySession getOfferingConstrainerQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerQuerySession getOfferingConstrainerQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer search service. 
     *
     *  @return an <code> OfferingConstrainerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerSearchSession getOfferingConstrainerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerSearchSession getOfferingConstrainerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerSearchSession getOfferingConstrainerSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerSearchSession getOfferingConstrainerSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer administration service. 
     *
     *  @return an <code> OfferingConstrainerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerAdminSession getOfferingConstrainerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerAdminSession getOfferingConstrainerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerAdminSession getOfferingConstrainerAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerAdminSession getOfferingConstrainerAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer notification service. 
     *
     *  @param  offeringConstrainerReceiver the notification callback 
     *  @return an <code> OfferingConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerNotificationSession getOfferingConstrainerNotificationSession(org.osid.offering.rules.OfferingConstrainerReceiver offeringConstrainerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer notification service. 
     *
     *  @param  offeringConstrainerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerNotificationSession getOfferingConstrainerNotificationSession(org.osid.offering.rules.OfferingConstrainerReceiver offeringConstrainerReceiver, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer notification service for the given catalogue. 
     *
     *  @param  offeringConstrainerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerReceiver </code> or <code> catalogueId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerNotificationSession getOfferingConstrainerNotificationSessionForCatalogue(org.osid.offering.rules.OfferingConstrainerReceiver offeringConstrainerReceiver, 
                                                                                                                                org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer notification service for the given catalogue. 
     *
     *  @param  offeringConstrainerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerReceiver, catalogueId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerNotificationSession getOfferingConstrainerNotificationSessionForCatalogue(org.osid.offering.rules.OfferingConstrainerReceiver offeringConstrainerReceiver, 
                                                                                                                                org.osid.id.Id catalogueId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup offering 
     *  constrainer/catalogue mappings for offering constrainers. 
     *
     *  @return an <code> OfferingConstrainerCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerCatalogueSession getOfferingConstrainerCatalogueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup offering 
     *  constrainer/catalogue mappings for offering constrainers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerCatalogue() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerCatalogueSession getOfferingConstrainerCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning offering 
     *  constrainer to catalogues. 
     *
     *  @return an <code> OfferingConstrainerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerCatalogueAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerCatalogueAssignmentSession getOfferingConstrainerCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning offering 
     *  constrainer to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerCatalogueAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerCatalogueAssignmentSession getOfferingConstrainerCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage offering constrainer 
     *  smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerSmartCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerSmartCatalogueSession getOfferingConstrainerSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage offering constrainer 
     *  smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerSmartCatalogueSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerSmartCatalogue() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerSmartCatalogueSession getOfferingConstrainerSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer canonical unit mapping lookup service for looking up the 
     *  rules applied to the catalogue. 
     *
     *  @return an <code> OfferingConstrainerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleLookupSession getOfferingConstrainerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer mapping lookup service for looking up the rules applied to 
     *  the catalogue. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleLookupSession getOfferingConstrainerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer mapping lookup service for the given catalogue for looking 
     *  up rules applied to a catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleLookupSession getOfferingConstrainerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer mapping lookup service for the given catalogue for looking 
     *  up rules applied to a catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleLookupSession getOfferingConstrainerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer assignment service to apply to catalogues. 
     *
     *  @return an <code> OfferingConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleApplicationSession getOfferingConstrainerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer assignment service to apply to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleApplicationSession getOfferingConstrainerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer assignment service for the given catalogue to apply to 
     *  catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleApplicationSession getOfferingConstrainerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer assignment service for the given catalogue to apply to 
     *  catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerRuleApplicationSession getOfferingConstrainerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler lookup service. 
     *
     *  @return an <code> OfferingConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerLookupSession getOfferingConstrainerEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerLookup() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerLookupSession getOfferingConstrainerEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerLookupSession getOfferingConstrainerEnablerLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerLookupSession getOfferingConstrainerEnablerLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler query service. 
     *
     *  @return an <code> OfferingConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerQuerySession getOfferingConstrainerEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerQuery() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerQuerySession getOfferingConstrainerEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerQuerySession getOfferingConstrainerEnablerQuerySessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler query service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerQuerySession getOfferingConstrainerEnablerQuerySessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerQuerySessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler search service. 
     *
     *  @return an <code> OfferingConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerSearchSession getOfferingConstrainerEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerSearch() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerSearchSession getOfferingConstrainerEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enablers earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerSearchSession getOfferingConstrainerEnablerSearchSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enablers earch service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerSearchSession getOfferingConstrainerEnablerSearchSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerSearchSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler administration service. 
     *
     *  @return an <code> OfferingConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerAdminSession getOfferingConstrainerEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerAdmin() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerAdminSession getOfferingConstrainerEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerAdminSession getOfferingConstrainerEnablerAdminSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler administration service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerAdminSession getOfferingConstrainerEnablerAdminSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerAdminSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler notification service. 
     *
     *  @param  offeringConstrainerEnablerReceiver the notification callback 
     *  @return an <code> OfferingConstrainerEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerReceiver </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerNotificationSession getOfferingConstrainerEnablerNotificationSession(org.osid.offering.rules.OfferingConstrainerEnablerReceiver offeringConstrainerEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler notification service. 
     *
     *  @param  offeringConstrainerEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerReceiver </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerNotification() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerNotificationSession getOfferingConstrainerEnablerNotificationSession(org.osid.offering.rules.OfferingConstrainerEnablerReceiver offeringConstrainerEnablerReceiver, 
                                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler notification service for the given catalogue. 
     *
     *  @param  offeringConstrainerEnablerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerReceiver </code> or <code> 
     *          catalogueId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerNotificationSession getOfferingConstrainerEnablerNotificationSessionForCatalogue(org.osid.offering.rules.OfferingConstrainerEnablerReceiver offeringConstrainerEnablerReceiver, 
                                                                                                                                              org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler notification service for the given catalogue. 
     *
     *  @param  offeringConstrainerEnablerReceiver the notification callback 
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerNotificationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no catalogue found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          offeringConstrainerEnablerReceiver, catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerNotification() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerNotificationSession getOfferingConstrainerEnablerNotificationSessionForCatalogue(org.osid.offering.rules.OfferingConstrainerEnablerReceiver offeringConstrainerEnablerReceiver, 
                                                                                                                                              org.osid.id.Id catalogueId, 
                                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerNotificationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup offering constrainer 
     *  enabler/catalogue mappings for offering constrainer enablers. 
     *
     *  @return an <code> OfferingConstrainerEnablerCatalogueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerCatalogueSession getOfferingConstrainerEnablerCatalogueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup offering constrainer 
     *  enabler/catalogue mappings for offering constrainer enablers. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerCatalogueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerCatalogueSession getOfferingConstrainerEnablerCatalogueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning offering 
     *  constrainer enablers to catalogues. 
     *
     *  @return an <code> OfferingConstrainerEnablerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerCatalogueAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerCatalogueAssignmentSession getOfferingConstrainerEnablerCatalogueAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning offering 
     *  constrainer enablers to catalogues. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerCatalogueAssignmentSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerCatalogueAssignment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerCatalogueAssignmentSession getOfferingConstrainerEnablerCatalogueAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerCatalogueAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage offering constrainer 
     *  enabler smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerEnablerSmartCatalogueSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerSmartCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerSmartCatalogueSession getOfferingConstrainerEnablerSmartCatalogueSession(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage offering constrainer 
     *  enabler smart catalogues. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerSmartCatalogueSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerSmartCatalogue() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerSmartCatalogueSession getOfferingConstrainerEnablerSmartCatalogueSession(org.osid.id.Id catalogueId, 
                                                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerSmartCatalogueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler mapping lookup service. 
     *
     *  @return an <code> OfferingConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleLookupSession getOfferingConstrainerEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler mapping lookup service . 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleLookup() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleLookupSession getOfferingConstrainerEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler mapping lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleLookupSession getOfferingConstrainerEnablerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler mapping lookup service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleLookup() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleLookupSession getOfferingConstrainerEnablerRuleLookupSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerRuleLookupSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler assignment service. 
     *
     *  @return an <code> OfferingConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleApplicationSession getOfferingConstrainerEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleApplication() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleApplicationSession getOfferingConstrainerEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler assignment service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @return an <code> OfferingConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleApplicationSession getOfferingConstrainerEnablerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesManager.getOfferingConstrainerEnablerRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offering 
     *  constrainer enabler assignment service for the given catalogue. 
     *
     *  @param  catalogueId the <code> Id </code> of the <code> Catalogue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> OfferingConstrainerEnablerRuleApplicationSession 
     *          </code> 
     *  @throws org.osid.NotFoundException no <code> Catalogue </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOfferingConstrainerEnablerRuleApplication() </code> or 
     *          <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.rules.OfferingConstrainerEnablerRuleApplicationSession getOfferingConstrainerEnablerRuleApplicationSessionForCatalogue(org.osid.id.Id catalogueId, 
                                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.offering.rules.OfferingRulesProxyManager.getOfferingConstrainerEnablerRuleApplicationSessionForCatalogue not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.canonicalUnitEnablerRecordTypes.clear();
        this.canonicalUnitEnablerRecordTypes.clear();

        this.canonicalUnitEnablerSearchRecordTypes.clear();
        this.canonicalUnitEnablerSearchRecordTypes.clear();

        this.canonicalUnitProcessorRecordTypes.clear();
        this.canonicalUnitProcessorRecordTypes.clear();

        this.canonicalUnitProcessorSearchRecordTypes.clear();
        this.canonicalUnitProcessorSearchRecordTypes.clear();

        this.canonicalUnitProcessorEnablerRecordTypes.clear();
        this.canonicalUnitProcessorEnablerRecordTypes.clear();

        this.canonicalUnitProcessorEnablerSearchRecordTypes.clear();
        this.canonicalUnitProcessorEnablerSearchRecordTypes.clear();

        this.offeringConstrainerRecordTypes.clear();
        this.offeringConstrainerRecordTypes.clear();

        this.offeringConstrainerSearchRecordTypes.clear();
        this.offeringConstrainerSearchRecordTypes.clear();

        this.offeringConstrainerEnablerRecordTypes.clear();
        this.offeringConstrainerEnablerRecordTypes.clear();

        this.offeringConstrainerEnablerSearchRecordTypes.clear();
        this.offeringConstrainerEnablerSearchRecordTypes.clear();

        return;
    }
}
