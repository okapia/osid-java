//
// AbstractMapEntryLookupSession
//
//    A simple framework for providing an Entry lookup service
//    backed by a fixed collection of entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.blogging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Entry lookup service backed by a
 *  fixed collection of entries. The entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Entries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapEntryLookupSession
    extends net.okapia.osid.jamocha.blogging.spi.AbstractEntryLookupSession
    implements org.osid.blogging.EntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.blogging.Entry> entries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.blogging.Entry>());


    /**
     *  Makes an <code>Entry</code> available in this session.
     *
     *  @param  entry an entry
     *  @throws org.osid.NullArgumentException <code>entry<code>
     *          is <code>null</code>
     */

    protected void putEntry(org.osid.blogging.Entry entry) {
        this.entries.put(entry.getId(), entry);
        return;
    }


    /**
     *  Makes an array of entries available in this session.
     *
     *  @param  entries an array of entries
     *  @throws org.osid.NullArgumentException <code>entries<code>
     *          is <code>null</code>
     */

    protected void putEntries(org.osid.blogging.Entry[] entries) {
        putEntries(java.util.Arrays.asList(entries));
        return;
    }


    /**
     *  Makes a collection of entries available in this session.
     *
     *  @param  entries a collection of entries
     *  @throws org.osid.NullArgumentException <code>entries<code>
     *          is <code>null</code>
     */

    protected void putEntries(java.util.Collection<? extends org.osid.blogging.Entry> entries) {
        for (org.osid.blogging.Entry entry : entries) {
            this.entries.put(entry.getId(), entry);
        }

        return;
    }


    /**
     *  Removes an Entry from this session.
     *
     *  @param  entryId the <code>Id</code> of the entry
     *  @throws org.osid.NullArgumentException <code>entryId<code> is
     *          <code>null</code>
     */

    protected void removeEntry(org.osid.id.Id entryId) {
        this.entries.remove(entryId);
        return;
    }


    /**
     *  Gets the <code>Entry</code> specified by its <code>Id</code>.
     *
     *  @param  entryId <code>Id</code> of the <code>Entry</code>
     *  @return the entry
     *  @throws org.osid.NotFoundException <code>entryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>entryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.Entry getEntry(org.osid.id.Id entryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.blogging.Entry entry = this.entries.get(entryId);
        if (entry == null) {
            throw new org.osid.NotFoundException("entry not found: " + entryId);
        }

        return (entry);
    }


    /**
     *  Gets all <code>Entries</code>. In plenary mode, the returned
     *  list contains all known entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Entries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.blogging.EntryList getEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.blogging.entry.ArrayEntryList(this.entries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.entries.clear();
        super.close();
        return;
    }
}
