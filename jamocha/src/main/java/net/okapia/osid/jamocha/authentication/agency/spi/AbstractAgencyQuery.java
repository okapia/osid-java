//
// AbstractAgencyQuery.java
//
//     A template for making an Agency Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.agency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for agencies.
 */

public abstract class AbstractAgencyQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.authentication.AgencyQuery {

    private final java.util.Collection<org.osid.authentication.records.AgencyQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Matches agencies with any agent. 
     *
     *  @param  match <code> true </code> to match agencies with any agent. 
     *          <code> false </code> to match agencies with no agents 
     */

    @OSID @Override
    public void matchAnyAgent(boolean match) {
        return;
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        return;
    }


    /**
     *  Sets the agency <code> Id </code> for this query to match agencies 
     *  that have the specified agency as an ancestor. 
     *
     *  @param  agencyId an agency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAgencyId(org.osid.id.Id agencyId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor agency <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorAgencyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgencyQuery </code> is available. 
     *
     *  @return <code> true </code> if an agency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAgencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAgencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAncestorAgencyQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAgencyQuery() is false");
    }


    /**
     *  Matches agencies with any ancestor. 
     *
     *  @param  match <code> true </code> to match agencies with any ancestor, 
     *          <code> false </code> to match root agencies 
     */

    @OSID @Override
    public void matchAnyAncestorAgency(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor agency terms. 
     */

    @OSID @Override
    public void clearAncestorAgencyTerms() {
        return;
    }


    /**
     *  Sets the agency <code> Id </code> for this query to match agencies 
     *  that have the specified agency as an descendant. 
     *
     *  @param  agencyId an agency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAgencyId(org.osid.id.Id agencyId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant agency <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantAgencyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgencyQuery </code> is available. 
     *
     *  @return <code> true </code> if an agency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAgencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAgencyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getDescendantAgencyQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAgencyQuery() is false");
    }


    /**
     *  Matches agencies with any descendant. 
     *
     *  @param  match <code> true </code> to match agencies with any 
     *          descendant, <code> false </code> to match leaf agencies 
     */

    @OSID @Override
    public void matchAnyDescendantAgency(boolean match) {
        return;
    }


    /**
     *  Clears the descendant agency terms. 
     */

    @OSID @Override
    public void clearDescendantAgencyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given agency query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an agency implementing the requested record.
     *
     *  @param agencyRecordType an agency record type
     *  @return the agency query record
     *  @throws org.osid.NullArgumentException
     *          <code>agencyRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agencyRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgencyQueryRecord getAgencyQueryRecord(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgencyQueryRecord record : this.records) {
            if (record.implementsRecordType(agencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agencyRecordType + " is not supported");
    }


    /**
     *  Adds a record to this agency query. 
     *
     *  @param agencyQueryRecord agency query record
     *  @param agencyRecordType agency record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAgencyQueryRecord(org.osid.authentication.records.AgencyQueryRecord agencyQueryRecord, 
                                          org.osid.type.Type agencyRecordType) {

        addRecordType(agencyRecordType);
        nullarg(agencyQueryRecord, "agency query record");
        this.records.add(agencyQueryRecord);        
        return;
    }
}
