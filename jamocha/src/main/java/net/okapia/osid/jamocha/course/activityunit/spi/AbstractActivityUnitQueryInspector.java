//
// AbstractActivityUnitQueryInspector.java
//
//     A template for making an ActivityUnitQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activityunit.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for activity units.
 */

public abstract class AbstractActivityUnitQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOperableOsidObjectQueryInspector
    implements org.osid.course.ActivityUnitQueryInspector {

    private final java.util.Collection<org.osid.course.records.ActivityUnitQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course query terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the total effort query terms. 
     *
     *  @return the total effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalTargetEffortTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the contact query terms. 
     *
     *  @return the contact query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getContactTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the total contact time query terms. 
     *
     *  @return the total contact time query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalTargetContactTimeTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the total individual effort query terms. 
     *
     *  @return the total individual effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getTotalTargetIndividualEffortTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the recurring weekly query terms. 
     *
     *  @return the recurring weekly query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRecurringWeeklyTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the weekly effort query terms. 
     *
     *  @return the weekly effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getWeeklyEffortTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the weekly contact time query terms. 
     *
     *  @return the weekly contact time query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getWeeklyContactTimeTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the weekly individual effort query terms. 
     *
     *  @return the weekly individual effort query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getWeeklyIndividualEffortTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getLearningObjectiveIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective query terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getLearningObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given activity unit query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an activity unit implementing the requested record.
     *
     *  @param activityUnitRecordType an activity unit record type
     *  @return the activity unit query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityUnitRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityUnitQueryInspectorRecord getActivityUnitQueryInspectorRecord(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityUnitQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(activityUnitRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityUnitRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity unit query. 
     *
     *  @param activityUnitQueryInspectorRecord activity unit query inspector
     *         record
     *  @param activityUnitRecordType activityUnit record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityUnitQueryInspectorRecord(org.osid.course.records.ActivityUnitQueryInspectorRecord activityUnitQueryInspectorRecord, 
                                                   org.osid.type.Type activityUnitRecordType) {

        addRecordType(activityUnitRecordType);
        nullarg(activityUnitRecordType, "activity unit record type");
        this.records.add(activityUnitQueryInspectorRecord);        
        return;
    }
}
