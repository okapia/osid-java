//
// AbstractSupersedingEventQueryInspector.java
//
//     A template for making a SupersedingEventQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.supersedingevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for superseding events.
 */

public abstract class AbstractSupersedingEventQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQueryInspector
    implements org.osid.calendaring.SupersedingEventQueryInspector {

    private final java.util.Collection<org.osid.calendaring.records.SupersedingEventQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the superseded event <code> Id </code> terms. 
     *
     *  @return the superseded event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupersededEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the superseded event terms. 
     *
     *  @return the superseded event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getSupersededEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the superseding event <code> Id </code> terms. 
     *
     *  @return the superseding event <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSupersedingEventIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the superseding event terms. 
     *
     *  @return the superseding event terms 
     */

    @OSID @Override
    public org.osid.calendaring.EventQueryInspector[] getSupersedingEventTerms() {
        return (new org.osid.calendaring.EventQueryInspector[0]);
    }


    /**
     *  Gets the superseded date range terms. 
     *
     *  @return the superseded date range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getSupersededDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the superseded event position terms. 
     *
     *  @return the superseded event position terms 
     */

    @OSID @Override
    public org.osid.search.terms.IntegerRangeTerm[] getSupersededEventPositionTerms() {
        return (new org.osid.search.terms.IntegerRangeTerm[0]);
    }


    /**
     *  Gets the calendar <code> Id </code> terms. 
     *
     *  @return the calendar <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCalendarIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the calendar terms. 
     *
     *  @return the calendar terms 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQueryInspector[] getCalendarTerms() {
        return (new org.osid.calendaring.CalendarQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given superseding event query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a superseding event implementing the requested record.
     *
     *  @param supersedingEventRecordType a superseding event record type
     *  @return the superseding event query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>supersedingEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(supersedingEventRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.SupersedingEventQueryInspectorRecord getSupersedingEventQueryInspectorRecord(org.osid.type.Type supersedingEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.SupersedingEventQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(supersedingEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(supersedingEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this superseding event query. 
     *
     *  @param supersedingEventQueryInspectorRecord superseding event query inspector
     *         record
     *  @param supersedingEventRecordType supersedingEvent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSupersedingEventQueryInspectorRecord(org.osid.calendaring.records.SupersedingEventQueryInspectorRecord supersedingEventQueryInspectorRecord, 
                                                   org.osid.type.Type supersedingEventRecordType) {

        addRecordType(supersedingEventRecordType);
        nullarg(supersedingEventRecordType, "superseding event record type");
        this.records.add(supersedingEventQueryInspectorRecord);        
        return;
    }
}
