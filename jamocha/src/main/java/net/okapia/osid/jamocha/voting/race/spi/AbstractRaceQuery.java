//
// AbstractRaceQuery.java
//
//     A template for making a Race Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.race.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for races.
 */

public abstract class AbstractRaceQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidGovernatorQuery
    implements org.osid.voting.RaceQuery {

    private final java.util.Collection<org.osid.voting.records.RaceQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the ballot <code> Id </code> for this query. 
     *
     *  @param  ballotId a ballot <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ballotId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchBallotId(org.osid.id.Id ballotId, boolean match) {
        return;
    }


    /**
     *  Clears the ballot <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearBallotIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BallotQuery </code> is available. 
     *
     *  @return <code> true </code> if a ballot query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ballot. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ballot query 
     *  @throws org.osid.UnimplementedException <code> supportsBallotQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.BallotQuery getBallotQuery() {
        throw new org.osid.UnimplementedException("supportsBallotQuery() is false");
    }


    /**
     *  Clears the ballot terms. 
     */

    @OSID @Override
    public void clearBallotTerms() {
        return;
    }


    /**
     *  Sets the candidate <code> Id </code> for this query. 
     *
     *  @param  candidateId a candidate <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> candidateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCandidateId(org.osid.id.Id candidateId, boolean match) {
        return;
    }


    /**
     *  Clears the candidate <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCandidateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CandidateQuery </code> is available. 
     *
     *  @return <code> true </code> if a candidate query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a candidate. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the candidate query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuery getCandidateQuery() {
        throw new org.osid.UnimplementedException("supportsCandidateQuery() is false");
    }


    /**
     *  Matches polls with any candidate. 
     *
     *  @param  match <code> true </code> to match polls with any candidate, 
     *          <code> false </code> to match polls with no candidates 
     */

    @OSID @Override
    public void matchAnyCandidate(boolean match) {
        return;
    }


    /**
     *  Clears the candidate terms. 
     */

    @OSID @Override
    public void clearCandidateTerms() {
        return;
    }


    /**
     *  Sets the polls <code> Id </code> for this query. 
     *
     *  @param  pollsid a polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsid, boolean match) {
        return;
    }


    /**
     *  Clears the polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given race query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a race implementing the requested record.
     *
     *  @param raceRecordType a race record type
     *  @return the race query record
     *  @throws org.osid.NullArgumentException
     *          <code>raceRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.RaceQueryRecord getRaceQueryRecord(org.osid.type.Type raceRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.RaceQueryRecord record : this.records) {
            if (record.implementsRecordType(raceRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race query. 
     *
     *  @param raceQueryRecord race query record
     *  @param raceRecordType race record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceQueryRecord(org.osid.voting.records.RaceQueryRecord raceQueryRecord, 
                                          org.osid.type.Type raceRecordType) {

        addRecordType(raceRecordType);
        nullarg(raceQueryRecord, "race query record");
        this.records.add(raceQueryRecord);        
        return;
    }
}
