//
// AbstractAdapterJobProcessorEnablerLookupSession.java
//
//    A JobProcessorEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A JobProcessorEnabler lookup session adapter.
 */

public abstract class AbstractAdapterJobProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resourcing.rules.JobProcessorEnablerLookupSession {

    private final org.osid.resourcing.rules.JobProcessorEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterJobProcessorEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterJobProcessorEnablerLookupSession(org.osid.resourcing.rules.JobProcessorEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Foundry/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Foundry Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.session.getFoundryId());
    }


    /**
     *  Gets the {@code Foundry} associated with this session.
     *
     *  @return the {@code Foundry} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getFoundry());
    }


    /**
     *  Tests if this user can perform {@code JobProcessorEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupJobProcessorEnablers() {
        return (this.session.canLookupJobProcessorEnablers());
    }


    /**
     *  A complete view of the {@code JobProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJobProcessorEnablerView() {
        this.session.useComparativeJobProcessorEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code JobProcessorEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJobProcessorEnablerView() {
        this.session.usePlenaryJobProcessorEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job processor enablers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        this.session.useFederatedFoundryView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        this.session.useIsolatedFoundryView();
        return;
    }
    

    /**
     *  Only active job processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobProcessorEnablerView() {
        this.session.useActiveJobProcessorEnablerView();
        return;
    }


    /**
     *  Active and inactive job processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobProcessorEnablerView() {
        this.session.useAnyStatusJobProcessorEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code JobProcessorEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code JobProcessorEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code JobProcessorEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @param jobProcessorEnablerId {@code Id} of the {@code JobProcessorEnabler}
     *  @return the job processor enabler
     *  @throws org.osid.NotFoundException {@code jobProcessorEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code jobProcessorEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnabler getJobProcessorEnabler(org.osid.id.Id jobProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorEnabler(jobProcessorEnablerId));
    }


    /**
     *  Gets a {@code JobProcessorEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  jobProcessorEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code JobProcessorEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @param  jobProcessorEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code JobProcessorEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code jobProcessorEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByIds(org.osid.id.IdList jobProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorEnablersByIds(jobProcessorEnablerIds));
    }


    /**
     *  Gets a {@code JobProcessorEnablerList} corresponding to the given
     *  job processor enabler genus {@code Type} which does not include
     *  job processor enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @param  jobProcessorEnablerGenusType a jobProcessorEnabler genus type 
     *  @return the returned {@code JobProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByGenusType(org.osid.type.Type jobProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorEnablersByGenusType(jobProcessorEnablerGenusType));
    }


    /**
     *  Gets a {@code JobProcessorEnablerList} corresponding to the given
     *  job processor enabler genus {@code Type} and include any additional
     *  job processor enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @param  jobProcessorEnablerGenusType a jobProcessorEnabler genus type 
     *  @return the returned {@code JobProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobProcessorEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByParentGenusType(org.osid.type.Type jobProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorEnablersByParentGenusType(jobProcessorEnablerGenusType));
    }


    /**
     *  Gets a {@code JobProcessorEnablerList} containing the given
     *  job processor enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @param  jobProcessorEnablerRecordType a jobProcessorEnabler record type 
     *  @return the returned {@code JobProcessorEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code jobProcessorEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByRecordType(org.osid.type.Type jobProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorEnablersByRecordType(jobProcessorEnablerRecordType));
    }


    /**
     *  Gets a {@code JobProcessorEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible
     *  through this session.
     *  
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code JobProcessorEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code JobProcessorEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible
     *  through this session.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code JobProcessorEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getJobProcessorEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code JobProcessorEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  job processor enablers or an error results. Otherwise, the returned list
     *  may contain only those job processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processor enablers are returned that are currently
     *  active. In any status mode, active and inactive job processor enablers
     *  are returned.
     *
     *  @return a list of {@code JobProcessorEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getJobProcessorEnablers());
    }
}
