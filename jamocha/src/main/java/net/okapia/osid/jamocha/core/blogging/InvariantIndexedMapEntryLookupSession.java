//
// InvariantIndexedMapEntryLookupSession
//
//    Implements an Entry lookup service backed by a fixed
//    collection of entries indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.blogging;


/**
 *  Implements an Entry lookup service backed by a fixed
 *  collection of entries. The entries are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some entries may be compatible
 *  with more types than are indicated through these entry
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapEntryLookupSession
    extends net.okapia.osid.jamocha.core.blogging.spi.AbstractIndexedMapEntryLookupSession
    implements org.osid.blogging.EntryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapEntryLookupSession} using an
     *  array of entries.
     *
     *  @param blog the blog
     *  @param entries an array of entries
     *  @throws org.osid.NullArgumentException {@code blog},
     *          {@code entries} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapEntryLookupSession(org.osid.blogging.Blog blog,
                                                    org.osid.blogging.Entry[] entries) {

        setBlog(blog);
        putEntries(entries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapEntryLookupSession} using a
     *  collection of entries.
     *
     *  @param blog the blog
     *  @param entries a collection of entries
     *  @throws org.osid.NullArgumentException {@code blog},
     *          {@code entries} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapEntryLookupSession(org.osid.blogging.Blog blog,
                                                    java.util.Collection<? extends org.osid.blogging.Entry> entries) {

        setBlog(blog);
        putEntries(entries);
        return;
    }
}
