//
// AbstractInstructionSearch.java
//
//     A template for making an Instruction Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.rules.check.instruction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing instruction searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractInstructionSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.rules.check.InstructionSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.rules.check.records.InstructionSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.rules.check.InstructionSearchOrder instructionSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of instructions. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  instructionIds list of instructions
     *  @throws org.osid.NullArgumentException
     *          <code>instructionIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongInstructions(org.osid.id.IdList instructionIds) {
        while (instructionIds.hasNext()) {
            try {
                this.ids.add(instructionIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongInstructions</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of instruction Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getInstructionIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  instructionSearchOrder instruction search order 
     *  @throws org.osid.NullArgumentException
     *          <code>instructionSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>instructionSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderInstructionResults(org.osid.rules.check.InstructionSearchOrder instructionSearchOrder) {
	this.instructionSearchOrder = instructionSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.rules.check.InstructionSearchOrder getInstructionSearchOrder() {
	return (this.instructionSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given instruction search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an instruction implementing the requested record.
     *
     *  @param instructionSearchRecordType an instruction search record
     *         type
     *  @return the instruction search record
     *  @throws org.osid.NullArgumentException
     *          <code>instructionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(instructionSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.rules.check.records.InstructionSearchRecord getInstructionSearchRecord(org.osid.type.Type instructionSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.rules.check.records.InstructionSearchRecord record : this.records) {
            if (record.implementsRecordType(instructionSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(instructionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this instruction search. 
     *
     *  @param instructionSearchRecord instruction search record
     *  @param instructionSearchRecordType instruction search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInstructionSearchRecord(org.osid.rules.check.records.InstructionSearchRecord instructionSearchRecord, 
                                           org.osid.type.Type instructionSearchRecordType) {

        addRecordType(instructionSearchRecordType);
        this.records.add(instructionSearchRecord);        
        return;
    }
}
