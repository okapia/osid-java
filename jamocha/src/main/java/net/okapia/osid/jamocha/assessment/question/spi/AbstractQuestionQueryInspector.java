//
// AbstractQuestionQueryInspector.java
//
//     A template for making a QuestionQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.question.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for questions.
 */

public abstract class AbstractQuestionQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.assessment.QuestionQueryInspector {

    private final java.util.Collection<org.osid.assessment.records.QuestionQueryInspectorRecord> records = new java.util.ArrayList<>();

    

    /**
     *  Gets the record corresponding to the given question query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a question implementing the requested record.
     *
     *  @param questionRecordType a question record type
     *  @return the question query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>questionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(questionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.QuestionQueryInspectorRecord getQuestionQueryInspectorRecord(org.osid.type.Type questionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.QuestionQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(questionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(questionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this question query. 
     *
     *  @param questionQueryInspectorRecord question query inspector
     *         record
     *  @param questionRecordType question record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQuestionQueryInspectorRecord(org.osid.assessment.records.QuestionQueryInspectorRecord questionQueryInspectorRecord, 
                                                   org.osid.type.Type questionRecordType) {

        addRecordType(questionRecordType);
        nullarg(questionRecordType, "question record type");
        this.records.add(questionQueryInspectorRecord);        
        return;
    }
}
