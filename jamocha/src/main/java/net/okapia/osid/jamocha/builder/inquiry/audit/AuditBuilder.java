//
// Audit.java
//
//     Defines an Audit builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.inquiry.audit;


/**
 *  Defines an <code>Audit</code> builder.
 */

public final class AuditBuilder
    extends net.okapia.osid.jamocha.builder.inquiry.audit.spi.AbstractAuditBuilder<AuditBuilder> {
    

    /**
     *  Constructs a new <code>AuditBuilder</code> using a
     *  <code>MutableAudit</code>.
     */

    public AuditBuilder() {
        super(new MutableAudit());
        return;
    }


    /**
     *  Constructs a new <code>AuditBuilder</code> using the given
     *  mutable audit.
     * 
     *  @param audit
     */

    public AuditBuilder(AuditMiter audit) {
        super(audit);
        return;
    }


    /**
     *  Gets the reference to this instance of the builder.
     *
     *  @return AuditBuilder
     */

    @Override
    protected AuditBuilder self() {
        return (this);
    }
}       


