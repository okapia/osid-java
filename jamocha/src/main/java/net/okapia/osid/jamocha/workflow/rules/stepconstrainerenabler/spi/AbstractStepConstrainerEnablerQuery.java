//
// AbstractStepConstrainerEnablerQuery.java
//
//     A template for making a StepConstrainerEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.stepconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for step constrainer enablers.
 */

public abstract class AbstractStepConstrainerEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.workflow.rules.StepConstrainerEnablerQuery {

    private final java.util.Collection<org.osid.workflow.rules.records.StepConstrainerEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the step constrainer. 
     *
     *  @param  stepConstrainerId the step constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stepConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledStepConstrainerId(org.osid.id.Id stepConstrainerId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the step constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledStepConstrainerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StepConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a step constrainer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledStepConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a step constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the step constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledStepConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerQuery getRuledStepConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledStepConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any step constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any step 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no step constrainers 
     */

    @OSID @Override
    public void matchAnyRuledStepConstrainer(boolean match) {
        return;
    }


    /**
     *  Clears the step constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledStepConstrainerTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the office. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if an office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given step constrainer enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a step constrainer enabler implementing the requested record.
     *
     *  @param stepConstrainerEnablerRecordType a step constrainer enabler record type
     *  @return the step constrainer enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(stepConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.StepConstrainerEnablerQueryRecord getStepConstrainerEnablerQueryRecord(org.osid.type.Type stepConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.StepConstrainerEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(stepConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(stepConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this step constrainer enabler query. 
     *
     *  @param stepConstrainerEnablerQueryRecord step constrainer enabler query record
     *  @param stepConstrainerEnablerRecordType stepConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStepConstrainerEnablerQueryRecord(org.osid.workflow.rules.records.StepConstrainerEnablerQueryRecord stepConstrainerEnablerQueryRecord, 
                                          org.osid.type.Type stepConstrainerEnablerRecordType) {

        addRecordType(stepConstrainerEnablerRecordType);
        nullarg(stepConstrainerEnablerQueryRecord, "step constrainer enabler query record");
        this.records.add(stepConstrainerEnablerQueryRecord);        
        return;
    }
}
