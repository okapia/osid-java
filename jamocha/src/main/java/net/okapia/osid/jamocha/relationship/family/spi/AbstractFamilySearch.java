//
// AbstractFamilySearch.java
//
//     A template for making a Family Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.family.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing family searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractFamilySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.relationship.FamilySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.relationship.records.FamilySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.relationship.FamilySearchOrder familySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of families. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  familyIds list of families
     *  @throws org.osid.NullArgumentException
     *          <code>familyIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongFamilies(org.osid.id.IdList familyIds) {
        while (familyIds.hasNext()) {
            try {
                this.ids.add(familyIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongFamilies</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of family Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getFamilyIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  familySearchOrder family search order 
     *  @throws org.osid.NullArgumentException
     *          <code>familySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>familySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderFamilyResults(org.osid.relationship.FamilySearchOrder familySearchOrder) {
	this.familySearchOrder = familySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.relationship.FamilySearchOrder getFamilySearchOrder() {
	return (this.familySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given family search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a family implementing the requested record.
     *
     *  @param familySearchRecordType a family search record
     *         type
     *  @return the family search record
     *  @throws org.osid.NullArgumentException
     *          <code>familySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(familySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.relationship.records.FamilySearchRecord getFamilySearchRecord(org.osid.type.Type familySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.relationship.records.FamilySearchRecord record : this.records) {
            if (record.implementsRecordType(familySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(familySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this family search. 
     *
     *  @param familySearchRecord family search record
     *  @param familySearchRecordType family search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFamilySearchRecord(org.osid.relationship.records.FamilySearchRecord familySearchRecord, 
                                           org.osid.type.Type familySearchRecordType) {

        addRecordType(familySearchRecordType);
        this.records.add(familySearchRecord);        
        return;
    }
}
