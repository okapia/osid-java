//
// AbstractVoteQuery.java
//
//     A template for making a Vote Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.vote.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for votes.
 */

public abstract class AbstractVoteQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.voting.VoteQuery {

    private final java.util.Collection<org.osid.voting.records.VoteQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the candidate <code> Id </code> for this query. 
     *
     *  @param  candidateId a candidate <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> candidateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCandidateId(org.osid.id.Id candidateId, boolean match) {
        return;
    }


    /**
     *  Clears the candidate <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCandidateIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CandidateQuery </code> is available for querying 
     *  candidates. 
     *
     *  @return <code> true </code> if a candidate query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a candidate. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the candidate query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuery getCandidateQuery() {
        throw new org.osid.UnimplementedException("supportsCandidateQuery() is false");
    }


    /**
     *  Clears the candidate terms. 
     */

    @OSID @Override
    public void clearCandidateTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVoterId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the voter agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearVoterIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available for querying 
     *  candidates. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVoterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a voter. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsVoterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getVoterQuery() {
        throw new org.osid.UnimplementedException("supportsVoterQuery() is false");
    }


    /**
     *  Clears the voter terms. 
     */

    @OSID @Override
    public void clearVoterTerms() {
        return;
    }


    /**
     *  Sets the voting agent <code> Id </code> for this query. 
     *
     *  @param  agentId a voting agent agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchVotingAgentId(org.osid.id.Id agentId, boolean match) {
        return;
    }


    /**
     *  Clears the voting agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearVotingAgentIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available for querying 
     *  candidates. 
     *
     *  @return <code> true </code> if a voting agent query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a voter. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingAgentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getVotingAgentQuery() {
        throw new org.osid.UnimplementedException("supportsVotingAgentQuery() is false");
    }


    /**
     *  Clears the voter terms. 
     */

    @OSID @Override
    public void clearVotingAgentTerms() {
        return;
    }


    /**
     *  Matches the number of votes within the given range inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchVotes(long from, long to, boolean match) {
        return;
    }


    /**
     *  Clears the votes terms. 
     */

    @OSID @Override
    public void clearVotesTerms() {
        return;
    }


    /**
     *  Matches the number of minimum votes inclusive. 
     *
     *  @param  votes the numbe rof votes 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumVotes(long votes, boolean match) {
        return;
    }


    /**
     *  Clears the minimum votes terms. 
     */

    @OSID @Override
    public void clearMinimumVotesTerms() {
        return;
    }


    /**
     *  Sets the polls <code> Id </code> for this query to match terms 
     *  assigned to polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        return;
    }


    /**
     *  Clears the polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given vote query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a vote implementing the requested record.
     *
     *  @param voteRecordType a vote record type
     *  @return the vote query record
     *  @throws org.osid.NullArgumentException
     *          <code>voteRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(voteRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.VoteQueryRecord getVoteQueryRecord(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.VoteQueryRecord record : this.records) {
            if (record.implementsRecordType(voteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(voteRecordType + " is not supported");
    }


    /**
     *  Adds a record to this vote query. 
     *
     *  @param voteQueryRecord vote query record
     *  @param voteRecordType vote record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addVoteQueryRecord(org.osid.voting.records.VoteQueryRecord voteQueryRecord, 
                                          org.osid.type.Type voteRecordType) {

        addRecordType(voteRecordType);
        nullarg(voteQueryRecord, "vote query record");
        this.records.add(voteQueryRecord);        
        return;
    }
}
