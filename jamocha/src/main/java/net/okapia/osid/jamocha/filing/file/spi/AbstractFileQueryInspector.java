//
// AbstractFileQueryInspector.java
//
//     A template for making a FileQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.filing.file.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for files.
 */

public abstract class AbstractFileQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.filing.FileQueryInspector {

    private final java.util.Collection<org.osid.filing.records.FileQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the name query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getNameTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the path query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getPathTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the directory query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQueryInspector[] getDirectoryTerms() {
        return (new org.osid.filing.DirectoryQueryInspector[0]);
    }


    /**
     *  Gets the aliases query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAliasesTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the owner <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOwnerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the owner query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getOwnerTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the created time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCreatedTimeTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the modified time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getModifiedTimeTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the last access time query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getLastAccessTimeTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the size query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getSizeTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the data string query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDataStringTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the data query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BytesTerm[] getDataTerms() {
        return (new org.osid.search.terms.BytesTerm[0]);
    }



    /**
     *  Gets the record corresponding to the given file query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a file implementing the requested record.
     *
     *  @param fileRecordType a file record type
     *  @return the file query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>fileRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(fileRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.filing.records.FileQueryInspectorRecord getFileQueryInspectorRecord(org.osid.type.Type fileRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.filing.records.FileQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(fileRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(fileRecordType + " is not supported");
    }


    /**
     *  Adds a record to this file query. 
     *
     *  @param fileQueryInspectorRecord file query inspector
     *         record
     *  @param fileRecordType file record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFileQueryInspectorRecord(org.osid.filing.records.FileQueryInspectorRecord fileQueryInspectorRecord, 
                                                   org.osid.type.Type fileRecordType) {

        addRecordType(fileRecordType);
        nullarg(fileRecordType, "file record type");
        this.records.add(fileQueryInspectorRecord);        
        return;
    }
}
