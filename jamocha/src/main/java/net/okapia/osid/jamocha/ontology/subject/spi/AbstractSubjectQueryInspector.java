//
// AbstractSubjectQueryInspector.java
//
//     A template for making a SubjectQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ontology.subject.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for subjects.
 */

public abstract class AbstractSubjectQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.ontology.SubjectQueryInspector {

    private final java.util.Collection<org.osid.ontology.records.SubjectQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the ancestor subject <code> Id </code> terms. 
     *
     *  @return the ancestor subject <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorSubjectIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor subject terms. 
     *
     *  @return the ancestor subject terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getAncestorSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Gets the descendant subject <code> Id </code> terms. 
     *
     *  @return the descendant subject <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantSubjectIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant subject terms. 
     *
     *  @return the descendant subject terms 
     */

    @OSID @Override
    public org.osid.ontology.SubjectQueryInspector[] getDescendantSubjectTerms() {
        return (new org.osid.ontology.SubjectQueryInspector[0]);
    }


    /**
     *  Gets the relevancy <code> Id </code> terms. 
     *
     *  @return the relevancy <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRelevancyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the relevancy terms. 
     *
     *  @return the relevancy terms 
     */

    @OSID @Override
    public org.osid.ontology.RelevancyQueryInspector[] getRelevancyTerms() {
        return (new org.osid.ontology.RelevancyQueryInspector[0]);
    }


    /**
     *  Gets the ontology <code> Id </code> terms. 
     *
     *  @return the ontology <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOntologyIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ontology terms. 
     *
     *  @return the ontology terms 
     */

    @OSID @Override
    public org.osid.ontology.OntologyQueryInspector[] getOntologyTerms() {
        return (new org.osid.ontology.OntologyQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given subject query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a subject implementing the requested record.
     *
     *  @param subjectRecordType a subject record type
     *  @return the subject query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>subjectRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subjectRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ontology.records.SubjectQueryInspectorRecord getSubjectQueryInspectorRecord(org.osid.type.Type subjectRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ontology.records.SubjectQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(subjectRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subjectRecordType + " is not supported");
    }


    /**
     *  Adds a record to this subject query. 
     *
     *  @param subjectQueryInspectorRecord subject query inspector
     *         record
     *  @param subjectRecordType subject record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSubjectQueryInspectorRecord(org.osid.ontology.records.SubjectQueryInspectorRecord subjectQueryInspectorRecord, 
                                                   org.osid.type.Type subjectRecordType) {

        addRecordType(subjectRecordType);
        nullarg(subjectRecordType, "subject record type");
        this.records.add(subjectQueryInspectorRecord);        
        return;
    }
}
