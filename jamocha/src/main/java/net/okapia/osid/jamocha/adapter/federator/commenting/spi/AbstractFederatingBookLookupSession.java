//
// AbstractFederatingBookLookupSession.java
//
//     An abstract federating adapter for a BookLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  BookLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingBookLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.commenting.BookLookupSession>
    implements org.osid.commenting.BookLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingBookLookupSession</code>.
     */

    protected AbstractFederatingBookLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.commenting.BookLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Book</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBooks() {
        for (org.osid.commenting.BookLookupSession session : getSessions()) {
            if (session.canLookupBooks()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Book</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBookView() {
        for (org.osid.commenting.BookLookupSession session : getSessions()) {
            session.useComparativeBookView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Book</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBookView() {
        for (org.osid.commenting.BookLookupSession session : getSessions()) {
            session.usePlenaryBookView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Book</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Book</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Book</code> and
     *  retained for compatibility.
     *
     *  @param  bookId <code>Id</code> of the
     *          <code>Book</code>
     *  @return the book
     *  @throws org.osid.NotFoundException <code>bookId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>bookId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.commenting.BookLookupSession session : getSessions()) {
            try {
                return (session.getBook(bookId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(bookId + " not found");
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  books specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Books</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  bookIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>bookIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByIds(org.osid.id.IdList bookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.commenting.book.MutableBookList ret = new net.okapia.osid.jamocha.commenting.book.MutableBookList();

        try (org.osid.id.IdList ids = bookIds) {
            while (ids.hasNext()) {
                ret.addBook(getBook(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  book genus <code>Type</code> which does not include
     *  books of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  bookGenusType a book genus type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByGenusType(org.osid.type.Type bookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.book.FederatingBookList ret = getBookList();

        for (org.osid.commenting.BookLookupSession session : getSessions()) {
            ret.addBookList(session.getBooksByGenusType(bookGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BookList</code> corresponding to the given
     *  book genus <code>Type</code> and include any additional
     *  books with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  bookGenusType a book genus type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByParentGenusType(org.osid.type.Type bookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.book.FederatingBookList ret = getBookList();

        for (org.osid.commenting.BookLookupSession session : getSessions()) {
            ret.addBookList(session.getBooksByParentGenusType(bookGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BookList</code> containing the given
     *  book record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  bookRecordType a book record type 
     *  @return the returned <code>Book</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>bookRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByRecordType(org.osid.type.Type bookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.book.FederatingBookList ret = getBookList();

        for (org.osid.commenting.BookLookupSession session : getSessions()) {
            ret.addBookList(session.getBooksByRecordType(bookRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>BookList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known books or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  books that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Book</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.commenting.book.FederatingBookList ret = getBookList();

        for (org.osid.commenting.BookLookupSession session : getSessions()) {
            ret.addBookList(session.getBooksByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Books</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  books or an error results. Otherwise, the returned list
     *  may contain only those books that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Books</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.BookList getBooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.commenting.book.FederatingBookList ret = getBookList();

        for (org.osid.commenting.BookLookupSession session : getSessions()) {
            ret.addBookList(session.getBooks());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.commenting.book.FederatingBookList getBookList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.commenting.book.ParallelBookList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.commenting.book.CompositeBookList());
        }
    }
}
