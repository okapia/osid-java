//
// AbstractNodeAccountHierarchySession.java
//
//     Defines an Account hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an account hierarchy session for delivering a hierarchy
 *  of accounts using the AccountNode interface.
 */

public abstract class AbstractNodeAccountHierarchySession
    extends net.okapia.osid.jamocha.financials.spi.AbstractAccountHierarchySession
    implements org.osid.financials.AccountHierarchySession {

    private java.util.Collection<org.osid.financials.AccountNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root account <code> Ids </code> in this hierarchy.
     *
     *  @return the root account <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootAccountIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.financials.accountnode.AccountNodeToIdList(this.roots));
    }


    /**
     *  Gets the root accounts in the account hierarchy. A node
     *  with no parents is an orphan. While all account <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root accounts 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getRootAccounts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.accountnode.AccountNodeToAccountList(new net.okapia.osid.jamocha.financials.accountnode.ArrayAccountNodeList(this.roots)));
    }


    /**
     *  Adds a root account node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootAccount(org.osid.financials.AccountNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root account nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootAccounts(java.util.Collection<org.osid.financials.AccountNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root account node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootAccount(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.financials.AccountNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Account </code> has any parents. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @return <code> true </code> if the account has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentAccounts(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getAccountNode(accountId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of an
     *  account.
     *
     *  @param  id an <code> Id </code> 
     *  @param  accountId the <code> Id </code> of an account 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> accountId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> accountId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfAccount(org.osid.id.Id id, org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.financials.AccountNodeList parents = getAccountNode(accountId).getParentAccountNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextAccountNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given account. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @return the parent <code> Ids </code> of the account 
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentAccountIds(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.account.AccountToIdList(getParentAccounts(accountId)));
    }


    /**
     *  Gets the parents of the given account. 
     *
     *  @param  accountId the <code> Id </code> to query 
     *  @return the parents of the account 
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getParentAccounts(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.accountnode.AccountNodeToAccountList(getAccountNode(accountId).getParentAccountNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of an
     *  account.
     *
     *  @param  id an <code> Id </code> 
     *  @param  accountId the Id of an account 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> accountId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfAccount(org.osid.id.Id id, org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAccount(id, accountId)) {
            return (true);
        }

        try (org.osid.financials.AccountList parents = getParentAccounts(accountId)) {
            while (parents.hasNext()) {
                if (isAncestorOfAccount(id, parents.getNextAccount().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if an account has any children. 
     *
     *  @param  accountId an account <code> Id </code> 
     *  @return <code> true </code> if the <code> accountId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildAccounts(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAccountNode(accountId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of an
     *  account.
     *
     *  @param  id an <code> Id </code> 
     *  @param accountId the <code> Id </code> of an 
     *         account
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> accountId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> accountId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfAccount(org.osid.id.Id id, org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfAccount(accountId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  account.
     *
     *  @param  accountId the <code> Id </code> to query 
     *  @return the children of the account 
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildAccountIds(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.account.AccountToIdList(getChildAccounts(accountId)));
    }


    /**
     *  Gets the children of the given account. 
     *
     *  @param  accountId the <code> Id </code> to query 
     *  @return the children of the account 
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountList getChildAccounts(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.financials.accountnode.AccountNodeToAccountList(getAccountNode(accountId).getChildAccountNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of an
     *  account.
     *
     *  @param  id an <code> Id </code> 
     *  @param accountId the <code> Id </code> of an 
     *         account
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> accountId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfAccount(org.osid.id.Id id, org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfAccount(accountId, id)) {
            return (true);
        }

        try (org.osid.financials.AccountList children = getChildAccounts(accountId)) {
            while (children.hasNext()) {
                if (isDescendantOfAccount(id, children.getNextAccount().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  account.
     *
     *  @param  accountId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified account node 
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getAccountNodeIds(org.osid.id.Id accountId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.financials.accountnode.AccountNodeToNode(getAccountNode(accountId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given account.
     *
     *  @param  accountId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified account node 
     *  @throws org.osid.NotFoundException <code> accountId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> accountId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.financials.AccountNode getAccountNodes(org.osid.id.Id accountId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAccountNode(accountId));
    }


    /**
     *  Closes this <code>AccountHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets an account node.
     *
     *  @param accountId the id of the account node
     *  @throws org.osid.NotFoundException <code>accountId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>accountId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.financials.AccountNode getAccountNode(org.osid.id.Id accountId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(accountId, "account Id");
        for (org.osid.financials.AccountNode account : this.roots) {
            if (account.getId().equals(accountId)) {
                return (account);
            }

            org.osid.financials.AccountNode r = findAccount(account, accountId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(accountId + " is not found");
    }


    protected org.osid.financials.AccountNode findAccount(org.osid.financials.AccountNode node, 
                                                          org.osid.id.Id accountId) 
	throws org.osid.OperationFailedException {

        try (org.osid.financials.AccountNodeList children = node.getChildAccountNodes()) {
            while (children.hasNext()) {
                org.osid.financials.AccountNode account = children.getNextAccountNode();
                if (account.getId().equals(accountId)) {
                    return (account);
                }
                
                account = findAccount(account, accountId);
                if (account != null) {
                    return (account);
                }
            }
        }

        return (null);
    }
}
