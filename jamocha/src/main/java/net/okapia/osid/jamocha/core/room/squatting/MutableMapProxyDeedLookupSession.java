//
// MutableMapProxyDeedLookupSession
//
//    Implements a Deed lookup service backed by a collection of
//    deeds that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.squatting;


/**
 *  Implements a Deed lookup service backed by a collection of
 *  deeds. The deeds are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of deeds can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyDeedLookupSession
    extends net.okapia.osid.jamocha.core.room.squatting.spi.AbstractMapDeedLookupSession
    implements org.osid.room.squatting.DeedLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyDeedLookupSession}
     *  with no deeds.
     *
     *  @param campus the campus
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyDeedLookupSession(org.osid.room.Campus campus,
                                                  org.osid.proxy.Proxy proxy) {
        setCampus(campus);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyDeedLookupSession} with a
     *  single deed.
     *
     *  @param campus the campus
     *  @param deed a deed
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code deed}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyDeedLookupSession(org.osid.room.Campus campus,
                                                org.osid.room.squatting.Deed deed, org.osid.proxy.Proxy proxy) {
        this(campus, proxy);
        putDeed(deed);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDeedLookupSession} using an
     *  array of deeds.
     *
     *  @param campus the campus
     *  @param deeds an array of deeds
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code deeds}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyDeedLookupSession(org.osid.room.Campus campus,
                                                org.osid.room.squatting.Deed[] deeds, org.osid.proxy.Proxy proxy) {
        this(campus, proxy);
        putDeeds(deeds);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDeedLookupSession} using a
     *  collection of deeds.
     *
     *  @param campus the campus
     *  @param deeds a collection of deeds
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code campus},
     *          {@code deeds}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyDeedLookupSession(org.osid.room.Campus campus,
                                                java.util.Collection<? extends org.osid.room.squatting.Deed> deeds,
                                                org.osid.proxy.Proxy proxy) {
   
        this(campus, proxy);
        setSessionProxy(proxy);
        putDeeds(deeds);
        return;
    }

    
    /**
     *  Makes a {@code Deed} available in this session.
     *
     *  @param deed an deed
     *  @throws org.osid.NullArgumentException {@code deed{@code 
     *          is {@code null}
     */

    @Override
    public void putDeed(org.osid.room.squatting.Deed deed) {
        super.putDeed(deed);
        return;
    }


    /**
     *  Makes an array of deeds available in this session.
     *
     *  @param deeds an array of deeds
     *  @throws org.osid.NullArgumentException {@code deeds{@code 
     *          is {@code null}
     */

    @Override
    public void putDeeds(org.osid.room.squatting.Deed[] deeds) {
        super.putDeeds(deeds);
        return;
    }


    /**
     *  Makes collection of deeds available in this session.
     *
     *  @param deeds
     *  @throws org.osid.NullArgumentException {@code deed{@code 
     *          is {@code null}
     */

    @Override
    public void putDeeds(java.util.Collection<? extends org.osid.room.squatting.Deed> deeds) {
        super.putDeeds(deeds);
        return;
    }


    /**
     *  Removes a Deed from this session.
     *
     *  @param deedId the {@code Id} of the deed
     *  @throws org.osid.NullArgumentException {@code deedId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDeed(org.osid.id.Id deedId) {
        super.removeDeed(deedId);
        return;
    }    
}
