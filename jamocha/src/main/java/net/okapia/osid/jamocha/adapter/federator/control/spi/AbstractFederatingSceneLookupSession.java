//
// AbstractFederatingSceneLookupSession.java
//
//     An abstract federating adapter for a SceneLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.control.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  SceneLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingSceneLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.control.SceneLookupSession>
    implements org.osid.control.SceneLookupSession {

    private boolean parallel = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();


    /**
     *  Constructs a new <code>AbstractFederatingSceneLookupSession</code>.
     */

    protected AbstractFederatingSceneLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.control.SceneLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>Scene</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupScenes() {
        for (org.osid.control.SceneLookupSession session : getSessions()) {
            if (session.canLookupScenes()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Scene</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeSceneView() {
        for (org.osid.control.SceneLookupSession session : getSessions()) {
            session.useComparativeSceneView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Scene</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenarySceneView() {
        for (org.osid.control.SceneLookupSession session : getSessions()) {
            session.usePlenarySceneView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include scenes in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        for (org.osid.control.SceneLookupSession session : getSessions()) {
            session.useFederatedSystemView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        for (org.osid.control.SceneLookupSession session : getSessions()) {
            session.useIsolatedSystemView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Scene</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Scene</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Scene</code> and
     *  retained for compatibility.
     *
     *  @param  sceneId <code>Id</code> of the
     *          <code>Scene</code>
     *  @return the scene
     *  @throws org.osid.NotFoundException <code>sceneId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>sceneId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.Scene getScene(org.osid.id.Id sceneId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.control.SceneLookupSession session : getSessions()) {
            try {
                return (session.getScene(sceneId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(sceneId + " not found");
    }


    /**
     *  Gets a <code>SceneList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  scenes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Scenes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getScenes()</code>.
     *
     *  @param  sceneIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Scene</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>sceneIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByIds(org.osid.id.IdList sceneIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.control.scene.MutableSceneList ret = new net.okapia.osid.jamocha.control.scene.MutableSceneList();

        try (org.osid.id.IdList ids = sceneIds) {
            while (ids.hasNext()) {
                ret.addScene(getScene(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>SceneList</code> corresponding to the given
     *  scene genus <code>Type</code> which does not include
     *  scenes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getScenes()</code>.
     *
     *  @param  sceneGenusType a scene genus type 
     *  @return the returned <code>Scene</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sceneGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByGenusType(org.osid.type.Type sceneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.scene.FederatingSceneList ret = getSceneList();

        for (org.osid.control.SceneLookupSession session : getSessions()) {
            ret.addSceneList(session.getScenesByGenusType(sceneGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SceneList</code> corresponding to the given
     *  scene genus <code>Type</code> and include any additional
     *  scenes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getScenes()</code>.
     *
     *  @param  sceneGenusType a scene genus type 
     *  @return the returned <code>Scene</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sceneGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByParentGenusType(org.osid.type.Type sceneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.scene.FederatingSceneList ret = getSceneList();

        for (org.osid.control.SceneLookupSession session : getSessions()) {
            ret.addSceneList(session.getScenesByParentGenusType(sceneGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>SceneList</code> containing the given
     *  scene record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getScenes()</code>.
     *
     *  @param  sceneRecordType a scene record type 
     *  @return the returned <code>Scene</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByRecordType(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.scene.FederatingSceneList ret = getSceneList();

        for (org.osid.control.SceneLookupSession session : getSessions()) {
            ret.addSceneList(session.getScenesByRecordType(sceneRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of scenes by a setting. <code> </code>
     *
     *  In plenary mode, the returned list contains all known scenes
     *  or an error results. Otherwise, the returned list may contain
     *  only those scenes that are accessible through this session.
     *
     *  @param  settingId a setting <code> Id </code>
     *  @return the returned <code> Scene </code> list
     *  @throws org.osid.NullArgumentException <code> settingId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesBySetting(org.osid.id.Id settingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.scene.FederatingSceneList ret = getSceneList();

        for (org.osid.control.SceneLookupSession session : getSessions()) {
            ret.addSceneList(session.getScenesBySetting(settingId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Scenes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  scenes or an error results. Otherwise, the returned list
     *  may contain only those scenes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Scenes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.scene.FederatingSceneList ret = getSceneList();

        for (org.osid.control.SceneLookupSession session : getSessions()) {
            ret.addSceneList(session.getScenes());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.control.scene.FederatingSceneList getSceneList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.control.scene.ParallelSceneList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.control.scene.CompositeSceneList());
        }
    }
}
