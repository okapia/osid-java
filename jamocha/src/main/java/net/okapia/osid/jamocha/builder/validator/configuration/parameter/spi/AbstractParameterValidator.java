//
// AbstractParameterValidator.java
//
//     Validates a Parameter.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.configuration.parameter.spi;


/**
 *  Validates a Parameter.
 */

public abstract class AbstractParameterValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRuleValidator {


    /**
     *  Constructs a new <code>AbstractParameterValidator</code>.
     */

    protected AbstractParameterValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractParameterValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractParameterValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Parameter.
     *
     *  @param parameter a parameter to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>parameter</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.configuration.Parameter parameter) {
        super.validate(parameter);

        test(parameter.getValueSyntax(), "getValueSyntax()");
        testConditionalMethod(parameter, "getValueCoordinateType", parameter.getValueSyntax() == org.osid.Syntax.COORDINATE, "COORDINATE");
        testConditionalMethod(parameter, "getValueSpatialUnitType", parameter.getValueSyntax() == org.osid.Syntax.SPATIALUNIT, "SPATIALUNIT");
        testConditionalMethod(parameter, "getValueObjectType", parameter.getValueSyntax() == org.osid.Syntax.OBJECT, "OBJECT");

        switch (parameter.getValueSyntax()) {
        case COORDINATE:
            if (!parameter.implementsValueCoordinateType(parameter.getValueCoordinateType())) {
                throw new org.osid.BadLogicException("parameter does not support getValueCoordinateType()");
            }

            break;

        case SPATIALUNIT:
            if (!parameter.implementsValueSpatialUnitRecordType(parameter.getValueSpatialUnitRecordType())) {
                throw new org.osid.BadLogicException("parameter does not support getValueSpatialUnitRecordType()");
            }

            break;

        case OBJECT:
            if (!parameter.implementsValueObjectType(parameter.getValueObjectType())) {
                throw new org.osid.BadLogicException("parameter does not support getValueObjectType()");
            }

            break;
        }

        return;
    }
}
