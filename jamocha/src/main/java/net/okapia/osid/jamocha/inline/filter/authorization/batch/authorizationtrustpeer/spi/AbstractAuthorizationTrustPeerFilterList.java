//
// AbstractAuthorizationTrustPeerList
//
//     Implements a filter for an AuthorizationTrustPeerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.authorization.batch.authorizationtrustpeer.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an AuthorizationTrustPeerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedAuthorizationTrustPeerList
 *  to improve performance.
 */

public abstract class AbstractAuthorizationTrustPeerFilterList
    extends net.okapia.osid.jamocha.authorization.batch.authorizationtrustpeer.spi.AbstractAuthorizationTrustPeerList
    implements org.osid.authorization.batch.AuthorizationTrustPeerList,
               net.okapia.osid.jamocha.inline.filter.authorization.batch.authorizationtrustpeer.AuthorizationTrustPeerFilter {

    private org.osid.authorization.batch.AuthorizationTrustPeer authorizationTrustPeer;
    private final org.osid.authorization.batch.AuthorizationTrustPeerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractAuthorizationTrustPeerFilterList</code>.
     *
     *  @param authorizationTrustPeerList an <code>AuthorizationTrustPeerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>authorizationTrustPeerList</code> is <code>null</code>
     */

    protected AbstractAuthorizationTrustPeerFilterList(org.osid.authorization.batch.AuthorizationTrustPeerList authorizationTrustPeerList) {
        nullarg(authorizationTrustPeerList, "authorization trust peer list");
        this.list = authorizationTrustPeerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.authorizationTrustPeer == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> AuthorizationTrustPeer </code> in this list. 
     *
     *  @return the next <code> AuthorizationTrustPeer </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> AuthorizationTrustPeer </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authorization.batch.AuthorizationTrustPeer getNextAuthorizationTrustPeer()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.authorization.batch.AuthorizationTrustPeer authorizationTrustPeer = this.authorizationTrustPeer;
            this.authorizationTrustPeer = null;
            return (authorizationTrustPeer);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in authorization trust peer list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.authorizationTrustPeer = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters AuthorizationTrustPeers.
     *
     *  @param authorizationTrustPeer the authorization trust peer to filter
     *  @return <code>true</code> if the authorization trust peer passes the filter,
     *          <code>false</code> if the authorization trust peer should be filtered
     */

    public abstract boolean pass(org.osid.authorization.batch.AuthorizationTrustPeer authorizationTrustPeer);


    protected void prime() {
        if (this.authorizationTrustPeer != null) {
            return;
        }

        org.osid.authorization.batch.AuthorizationTrustPeer authorizationTrustPeer = null;

        while (this.list.hasNext()) {
            try {
                authorizationTrustPeer = this.list.getNextAuthorizationTrustPeer();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(authorizationTrustPeer)) {
                this.authorizationTrustPeer = authorizationTrustPeer;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
