//
// AbstractTimePeriodQuery.java
//
//     A template for making a TimePeriod Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.timeperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for time periods.
 */

public abstract class AbstractTimePeriodQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.calendaring.TimePeriodQuery {

    private final java.util.Collection<org.osid.calendaring.records.TimePeriodQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches the time period start time between the given range inclusive. 
     *
     *  @param  low low time range 
     *  @param  high high time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchStart(org.osid.calendaring.DateTime low, 
                           org.osid.calendaring.DateTime high, boolean match) {
        return;
    }


    /**
     *  Matches a time period that has any start time assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any start 
     *          time, <code> false </code> to match time periods with no start 
     *          time 
     */

    @OSID @Override
    public void matchAnyStart(boolean match) {
        return;
    }


    /**
     *  Clears the time period start terms. 
     */

    @OSID @Override
    public void clearStartTerms() {
        return;
    }


    /**
     *  Matches the time period end time between the given range inclusive. 
     *
     *  @param  low low time range 
     *  @param  high high time range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchEnd(org.osid.calendaring.DateTime low, 
                         org.osid.calendaring.DateTime high, boolean match) {
        return;
    }


    /**
     *  Matches a time period that has any end time assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any end 
     *          time, <code> false </code> to match time periods with no end 
     *          time 
     */

    @OSID @Override
    public void matchAnyEnd(boolean match) {
        return;
    }


    /**
     *  Clears the time period end terms. 
     */

    @OSID @Override
    public void clearEndTerms() {
        return;
    }


    /**
     *  Matches time periods that include the given time. 
     *
     *  @param  time date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchTime(org.osid.calendaring.DateTime time, boolean match) {
        return;
    }


    /**
     *  Matches a time period that has any time assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any time, 
     *          <code> false </code> to match time periods with no time 
     */

    @OSID @Override
    public void matchAnyTime(boolean match) {
        return;
    }


    /**
     *  Clears the time terms. 
     */

    @OSID @Override
    public void clearTimeTerms() {
        return;
    }


    /**
     *  Matches time periods with start and end times between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchTimeInclusive(org.osid.calendaring.DateTime start, 
                                   org.osid.calendaring.DateTime end, 
                                   boolean match) {
        return;
    }


    /**
     *  Clears the time inclusive terms. 
     */

    @OSID @Override
    public void clearTimeInclusiveTerms() {
        return;
    }


    /**
     *  Matches the time period duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     *  @throws org.osid.NullArgumentException <code> high </code> or <code> 
     *          low </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDuration(org.osid.calendaring.Duration low, 
                              org.osid.calendaring.Duration high, 
                              boolean match) {
        return;
    }


    /**
     *  Clears the duration terms. 
     */

    @OSID @Override
    public void clearDurationTerms() {
        return;
    }


    /**
     *  Sets the event <code> Id </code> for this query to match exceptions. 
     *
     *  @param  eventId an exception event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchExceptionId(org.osid.id.Id eventId, boolean match) {
        return;
    }


    /**
     *  Clears the exception event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearExceptionIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying 
     *  exception events. 
     *
     *  @return <code> true </code> if a exception query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsExceptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for an exception event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsExceptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getExceptionQuery() {
        throw new org.osid.UnimplementedException("supportsExceptionQuery() is false");
    }


    /**
     *  Matches a time period that has any exception event assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any 
     *          exception, <code> false </code> to match time periods with no 
     *          exception 
     */

    @OSID @Override
    public void matchAnyException(boolean match) {
        return;
    }


    /**
     *  Clears the exception event terms. 
     */

    @OSID @Override
    public void clearExceptionTerms() {
        return;
    }


    /**
     *  Sets the event <code> Id </code> for this query. 
     *
     *  @param  eventId an event or recurring event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        return;
    }


    /**
     *  Clears the event <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEventIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EventQuery </code> is available for querying 
     *  events. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for an event or recurring event. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery() {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Matches a time period that has any event assigned. 
     *
     *  @param  match <code> true </code> to match time periods with any 
     *          event, <code> false </code> to match time periods with no 
     *          events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        return;
    }


    /**
     *  Clears the event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        return;
    }


    /**
     *  Sets the calendar <code> Id </code> for this query. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCalendarId(org.osid.id.Id calendarId, boolean match) {
        return;
    }


    /**
     *  Clears the calendar <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCalendarIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CalendarQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if a calendar query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCalendarQuery() {
        return (false);
    }


    /**
     *  Gets the query for a calendar. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the calendar query 
     *  @throws org.osid.UnimplementedException <code> supportsCalendarQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarQuery getCalendarQuery() {
        throw new org.osid.UnimplementedException("supportsCalendarQuery() is false");
    }


    /**
     *  Clears the calendar terms. 
     */

    @OSID @Override
    public void clearCalendarTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given time period query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a time period implementing the requested record.
     *
     *  @param timePeriodRecordType a time period record type
     *  @return the time period query record
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(timePeriodRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodQueryRecord getTimePeriodQueryRecord(org.osid.type.Type timePeriodRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.TimePeriodQueryRecord record : this.records) {
            if (record.implementsRecordType(timePeriodRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(timePeriodRecordType + " is not supported");
    }


    /**
     *  Adds a record to this time period query. 
     *
     *  @param timePeriodQueryRecord time period query record
     *  @param timePeriodRecordType timePeriod record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTimePeriodQueryRecord(org.osid.calendaring.records.TimePeriodQueryRecord timePeriodQueryRecord, 
                                          org.osid.type.Type timePeriodRecordType) {

        addRecordType(timePeriodRecordType);
        nullarg(timePeriodQueryRecord, "time period query record");
        this.records.add(timePeriodQueryRecord);        
        return;
    }
}
