//
// AbstractBrokerProcessorEnablerQuery.java
//
//     A template for making a BrokerProcessorEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for broker processor enablers.
 */

public abstract class AbstractBrokerProcessorEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.provisioning.rules.BrokerProcessorEnablerQuery {

    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the broker processor. 
     *
     *  @param  brokerProcessorId the broker processor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> brokerProcessorId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBrokerProcessorId(org.osid.id.Id brokerProcessorId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the broker processor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerProcessorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BrokerProcessorQuery </code> is available. 
     *
     *  @return <code> true </code> if a broker processor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBrokerProcessorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a broker processor. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the broker processor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBrokerProcessorQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorQuery getRuledBrokerProcessorQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBrokerProcessorQuery() is false");
    }


    /**
     *  Matches enablers mapped to any broker processor. 
     *
     *  @param  match <code> true </code> for enablers mapped to any broker 
     *          processor, <code> false </code> to match enablers mapped to no 
     *          broker processors 
     */

    @OSID @Override
    public void matchAnyRuledBrokerProcessor(boolean match) {
        return;
    }


    /**
     *  Clears the broker processor query terms. 
     */

    @OSID @Override
    public void clearRuledBrokerProcessorTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the distributor. 
     *
     *  @param  distributorId the distributor <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> distributorId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDistributorId(org.osid.id.Id distributorId, boolean match) {
        return;
    }


    /**
     *  Clears the distributor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDistributorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> DistributorQuery </code> is available. 
     *
     *  @return <code> true </code> if a distributor query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDistributorQuery() {
        return (false);
    }


    /**
     *  Gets the query for a distributor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the distributor query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDistributorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQuery getDistributorQuery() {
        throw new org.osid.UnimplementedException("supportsDistributorQuery() is false");
    }


    /**
     *  Clears the distributor query terms. 
     */

    @OSID @Override
    public void clearDistributorTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given broker processor enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a broker processor enabler implementing the requested record.
     *
     *  @param brokerProcessorEnablerRecordType a broker processor enabler record type
     *  @return the broker processor enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(brokerProcessorEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryRecord getBrokerProcessorEnablerQueryRecord(org.osid.type.Type brokerProcessorEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(brokerProcessorEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(brokerProcessorEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this broker processor enabler query. 
     *
     *  @param brokerProcessorEnablerQueryRecord broker processor enabler query record
     *  @param brokerProcessorEnablerRecordType brokerProcessorEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBrokerProcessorEnablerQueryRecord(org.osid.provisioning.rules.records.BrokerProcessorEnablerQueryRecord brokerProcessorEnablerQueryRecord, 
                                          org.osid.type.Type brokerProcessorEnablerRecordType) {

        addRecordType(brokerProcessorEnablerRecordType);
        nullarg(brokerProcessorEnablerQueryRecord, "broker processor enabler query record");
        this.records.add(brokerProcessorEnablerQueryRecord);        
        return;
    }
}
