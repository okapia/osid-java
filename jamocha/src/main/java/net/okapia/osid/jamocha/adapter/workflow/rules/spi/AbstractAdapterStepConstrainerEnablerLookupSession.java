//
// AbstractAdapterStepConstrainerEnablerLookupSession.java
//
//    A StepConstrainerEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A StepConstrainerEnabler lookup session adapter.
 */

public abstract class AbstractAdapterStepConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.rules.StepConstrainerEnablerLookupSession {

    private final org.osid.workflow.rules.StepConstrainerEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterStepConstrainerEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStepConstrainerEnablerLookupSession(org.osid.workflow.rules.StepConstrainerEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Office/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Office Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the {@code Office} associated with this session.
     *
     *  @return the {@code Office} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform {@code StepConstrainerEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupStepConstrainerEnablers() {
        return (this.session.canLookupStepConstrainerEnablers());
    }


    /**
     *  A complete view of the {@code StepConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepConstrainerEnablerView() {
        this.session.useComparativeStepConstrainerEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code StepConstrainerEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepConstrainerEnablerView() {
        this.session.usePlenaryStepConstrainerEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step constrainer enablers in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    

    /**
     *  Only active step constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepConstrainerEnablerView() {
        this.session.useActiveStepConstrainerEnablerView();
        return;
    }


    /**
     *  Active and inactive step constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepConstrainerEnablerView() {
        this.session.useAnyStatusStepConstrainerEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code StepConstrainerEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code StepConstrainerEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code StepConstrainerEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @param stepConstrainerEnablerId {@code Id} of the {@code StepConstrainerEnabler}
     *  @return the step constrainer enabler
     *  @throws org.osid.NotFoundException {@code stepConstrainerEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code stepConstrainerEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnabler getStepConstrainerEnabler(org.osid.id.Id stepConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainerEnabler(stepConstrainerEnablerId));
    }


    /**
     *  Gets a {@code StepConstrainerEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stepConstrainerEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code StepConstrainerEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @param  stepConstrainerEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code StepConstrainerEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code stepConstrainerEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByIds(org.osid.id.IdList stepConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainerEnablersByIds(stepConstrainerEnablerIds));
    }


    /**
     *  Gets a {@code StepConstrainerEnablerList} corresponding to the given
     *  step constrainer enabler genus {@code Type} which does not include
     *  step constrainer enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @param  stepConstrainerEnablerGenusType a stepConstrainerEnabler genus type 
     *  @return the returned {@code StepConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByGenusType(org.osid.type.Type stepConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainerEnablersByGenusType(stepConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code StepConstrainerEnablerList} corresponding to the given
     *  step constrainer enabler genus {@code Type} and include any additional
     *  step constrainer enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @param  stepConstrainerEnablerGenusType a stepConstrainerEnabler genus type 
     *  @return the returned {@code StepConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepConstrainerEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByParentGenusType(org.osid.type.Type stepConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainerEnablersByParentGenusType(stepConstrainerEnablerGenusType));
    }


    /**
     *  Gets a {@code StepConstrainerEnablerList} containing the given
     *  step constrainer enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @param  stepConstrainerEnablerRecordType a stepConstrainerEnabler record type 
     *  @return the returned {@code StepConstrainerEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code stepConstrainerEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByRecordType(org.osid.type.Type stepConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainerEnablersByRecordType(stepConstrainerEnablerRecordType));
    }


    /**
     *  Gets a {@code StepConstrainerEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code StepConstrainerEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainerEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code StepConstrainerEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code StepConstrainerEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getStepConstrainerEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code StepConstrainerEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @return a list of {@code StepConstrainerEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStepConstrainerEnablers());
    }
}
