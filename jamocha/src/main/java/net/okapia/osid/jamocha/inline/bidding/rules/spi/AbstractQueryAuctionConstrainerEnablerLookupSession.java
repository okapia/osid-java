//
// AbstractQueryAuctionConstrainerEnablerLookupSession.java
//
//    An inline adapter that maps an AuctionConstrainerEnablerLookupSession to
//    an AuctionConstrainerEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AuctionConstrainerEnablerLookupSession to
 *  an AuctionConstrainerEnablerQuerySession.
 */

public abstract class AbstractQueryAuctionConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.bidding.rules.spi.AbstractAuctionConstrainerEnablerLookupSession
    implements org.osid.bidding.rules.AuctionConstrainerEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;    
    private final org.osid.bidding.rules.AuctionConstrainerEnablerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAuctionConstrainerEnablerLookupSession.
     *
     *  @param querySession the underlying auction constrainer enabler query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAuctionConstrainerEnablerLookupSession(org.osid.bidding.rules.AuctionConstrainerEnablerQuerySession querySession) {
        nullarg(querySession, "auction constrainer enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform <code>AuctionConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionConstrainerEnablers() {
        return (this.session.canSearchAuctionConstrainerEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction constrainer enablers in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only active auction constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive auction constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>AuctionConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionConstrainerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AuctionConstrainerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, auction constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainer enablers
     *  are returned.
     *
     *  @param  auctionConstrainerEnablerId <code>Id</code> of the
     *          <code>AuctionConstrainerEnabler</code>
     *  @return the auction constrainer enabler
     *  @throws org.osid.NotFoundException <code>auctionConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnabler getAuctionConstrainerEnabler(org.osid.id.Id auctionConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerEnablerQuery query = getQuery();
        query.matchId(auctionConstrainerEnablerId, true);
        org.osid.bidding.rules.AuctionConstrainerEnablerList auctionConstrainerEnablers = this.session.getAuctionConstrainerEnablersByQuery(query);
        if (auctionConstrainerEnablers.hasNext()) {
            return (auctionConstrainerEnablers.getNextAuctionConstrainerEnabler());
        } 
        
        throw new org.osid.NotFoundException(auctionConstrainerEnablerId + " not found");
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionConstrainerEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AuctionConstrainerEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, auction constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainer enablers
     *  are returned.
     *
     *  @param  auctionConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByIds(org.osid.id.IdList auctionConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = auctionConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAuctionConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code> corresponding to the given
     *  auction constrainer enabler genus <code>Type</code> which does not include
     *  auction constrainer enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  auction constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainer enablers
     *  are returned.
     *
     *  @param  auctionConstrainerEnablerGenusType an auctionConstrainerEnabler genus type 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByGenusType(org.osid.type.Type auctionConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerEnablerQuery query = getQuery();
        query.matchGenusType(auctionConstrainerEnablerGenusType, true);
        return (this.session.getAuctionConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code> corresponding to the given
     *  auction constrainer enabler genus <code>Type</code> and include any additional
     *  auction constrainer enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  auction constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainer enablers
     *  are returned.
     *
     *  @param  auctionConstrainerEnablerGenusType an auctionConstrainerEnabler genus type 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByParentGenusType(org.osid.type.Type auctionConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerEnablerQuery query = getQuery();
        query.matchParentGenusType(auctionConstrainerEnablerGenusType, true);
        return (this.session.getAuctionConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code> containing the given
     *  auction constrainer enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auction constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainer enablers
     *  are returned.
     *
     *  @param  auctionConstrainerEnablerRecordType an auctionConstrainerEnabler record type 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersByRecordType(org.osid.type.Type auctionConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerEnablerQuery query = getQuery();
        query.matchRecordType(auctionConstrainerEnablerRecordType, true);
        return (this.session.getAuctionConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets an <code>AuctionConstrainerEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  auction constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, auction constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainer enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AuctionConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getAuctionConstrainerEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>AuctionConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those auction constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive auction constrainer enablers
     *  are returned.
     *
     *  @return a list of <code>AuctionConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnablerList getAuctionConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionConstrainerEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAuctionConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.bidding.rules.AuctionConstrainerEnablerQuery getQuery() {
        org.osid.bidding.rules.AuctionConstrainerEnablerQuery query = this.session.getAuctionConstrainerEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
