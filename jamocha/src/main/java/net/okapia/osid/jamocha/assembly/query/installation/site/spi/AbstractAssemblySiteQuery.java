//
// AbstractAssemblySiteQuery.java
//
//     A SiteQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.installation.site.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SiteQuery that stores terms.
 */

public abstract class AbstractAssemblySiteQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.installation.SiteQuery,
               org.osid.installation.SiteQueryInspector,
               org.osid.installation.SiteSearchOrder {

    private final java.util.Collection<org.osid.installation.records.SiteQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.records.SiteQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.installation.records.SiteSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySiteQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySiteQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the installation <code> Id </code> for this query. 
     *
     *  @param  installationId a site <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> installationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchInstallationId(org.osid.id.Id installationId, 
                                    boolean match) {
        getAssembler().addIdTerm(getInstallationIdColumn(), installationId, match);
        return;
    }


    /**
     *  Clears the installation <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInstallationIdTerms() {
        getAssembler().clearTerms(getInstallationIdColumn());
        return;
    }


    /**
     *  Gets the installation <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInstallationIdTerms() {
        return (getAssembler().getIdTerms(getInstallationIdColumn()));
    }


    /**
     *  Gets the InstallationId column name.
     *
     * @return the column name
     */

    protected String getInstallationIdColumn() {
        return ("installation_id");
    }


    /**
     *  Tests if an <code> InstallationQuery </code> is available for querying 
     *  installations. 
     *
     *  @return <code> true </code> if an installation query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstallationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an installation. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the site query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstallationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.installation.SiteQuery getInstallationQuery() {
        throw new org.osid.UnimplementedException("supportsInstallationQuery() is false");
    }


    /**
     *  Matches sites with any installation. 
     *
     *  @param  match <code> true </code> to match sites with any package, 
     *          <code> false </code> to match sites with no packages 
     */

    @OSID @Override
    public void matchAnyInstallation(boolean match) {
        getAssembler().addIdWildcardTerm(getInstallationColumn(), match);
        return;
    }


    /**
     *  Clears the installation query terms. 
     */

    @OSID @Override
    public void clearInstallationTerms() {
        getAssembler().clearTerms(getInstallationColumn());
        return;
    }


    /**
     *  Gets the installation query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.installation.InstallationQueryInspector[] getInstallationTerms() {
        return (new org.osid.installation.InstallationQueryInspector[0]);
    }


    /**
     *  Gets the Installation column name.
     *
     * @return the column name
     */

    protected String getInstallationColumn() {
        return ("installation");
    }


    /**
     *  Tests if this site supports the given record
     *  <code>Type</code>.
     *
     *  @param  siteRecordType a site record type 
     *  @return <code>true</code> if the siteRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>siteRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type siteRecordType) {
        for (org.osid.installation.records.SiteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(siteRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  siteRecordType the site record type 
     *  @return the site query record 
     *  @throws org.osid.NullArgumentException
     *          <code>siteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(siteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.SiteQueryRecord getSiteQueryRecord(org.osid.type.Type siteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.SiteQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(siteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(siteRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  siteRecordType the site record type 
     *  @return the site query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>siteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(siteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.SiteQueryInspectorRecord getSiteQueryInspectorRecord(org.osid.type.Type siteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.SiteQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(siteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(siteRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param siteRecordType the site record type
     *  @return the site search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>siteRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(siteRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.SiteSearchOrderRecord getSiteSearchOrderRecord(org.osid.type.Type siteRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.SiteSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(siteRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(siteRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this site. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param siteQueryRecord the site query record
     *  @param siteQueryInspectorRecord the site query inspector
     *         record
     *  @param siteSearchOrderRecord the site search order record
     *  @param siteRecordType site record type
     *  @throws org.osid.NullArgumentException
     *          <code>siteQueryRecord</code>,
     *          <code>siteQueryInspectorRecord</code>,
     *          <code>siteSearchOrderRecord</code> or
     *          <code>siteRecordTypesite</code> is
     *          <code>null</code>
     */
            
    protected void addSiteRecords(org.osid.installation.records.SiteQueryRecord siteQueryRecord, 
                                      org.osid.installation.records.SiteQueryInspectorRecord siteQueryInspectorRecord, 
                                      org.osid.installation.records.SiteSearchOrderRecord siteSearchOrderRecord, 
                                      org.osid.type.Type siteRecordType) {

        addRecordType(siteRecordType);

        nullarg(siteQueryRecord, "site query record");
        nullarg(siteQueryInspectorRecord, "site query inspector record");
        nullarg(siteSearchOrderRecord, "site search odrer record");

        this.queryRecords.add(siteQueryRecord);
        this.queryInspectorRecords.add(siteQueryInspectorRecord);
        this.searchOrderRecords.add(siteSearchOrderRecord);
        
        return;
    }
}
