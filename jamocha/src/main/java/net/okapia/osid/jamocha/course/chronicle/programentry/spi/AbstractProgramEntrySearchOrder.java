//
// AbstractProgramEntrySearchOdrer.java
//
//     Defines a ProgramEntrySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.programentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code ProgramEntrySearchOrder}.
 */

public abstract class AbstractProgramEntrySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.course.chronicle.ProgramEntrySearchOrder {

    private final java.util.Collection<org.osid.course.chronicle.records.ProgramEntrySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStudent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a resource order is available. 
     *
     *  @return <code> true </code> if a resource order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStudentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStudentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getStudentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStudentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the program. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByProgram(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a program order is available. 
     *
     *  @return <code> true </code> if a program order is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearchOrder() {
        return (false);
    }


    /**
     *  Gets the program order. 
     *
     *  @return the program search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchOrder getProgramSearchOrder() {
        throw new org.osid.UnimplementedException("supportsProgramSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the admission 
     *  date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAdmissionDate(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by completed 
     *  programs. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByComplete(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the term. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTerm(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a term order is available. 
     *
     *  @return <code> true </code> if a term order is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSearchOrder() {
        return (false);
    }


    /**
     *  Gets the term order. 
     *
     *  @return the term search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchOrder getTermSearchOrder() {
        throw new org.osid.UnimplementedException("supportsTermSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for credits. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditScale(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade system order is available. 
     *
     *  @return <code> true </code> if a grade system order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCreditScaleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system order. 
     *
     *  @return the credit scale search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCreditScaleSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getCreditScaleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCreditScaleSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the earned 
     *  credits. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreditsEarned(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the grade system 
     *  for GPAs. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGPAScale(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a grade system order is available. 
     *
     *  @return <code> true </code> if a grade system order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGPAScaleSearchOrder() {
        return (false);
    }


    /**
     *  Gets the grade system order. 
     *
     *  @return the GPA scale search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGPAScaleSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemSearchOrder getGPAScaleSearchOrder() {
        throw new org.osid.UnimplementedException("supportsGPAScaleSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the gpa. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByGPA(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  programEntryRecordType a program entry record type 
     *  @return {@code true} if the programEntryRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code programEntryRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type programEntryRecordType) {
        for (org.osid.course.chronicle.records.ProgramEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(programEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  programEntryRecordType the program entry record type 
     *  @return the program entry search order record
     *  @throws org.osid.NullArgumentException
     *          {@code programEntryRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(programEntryRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.course.chronicle.records.ProgramEntrySearchOrderRecord getProgramEntrySearchOrderRecord(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.ProgramEntrySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(programEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(programEntryRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this program entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param programEntryRecord the program entry search odrer record
     *  @param programEntryRecordType program entry record type
     *  @throws org.osid.NullArgumentException
     *          {@code programEntryRecord} or
     *          {@code programEntryRecordTypeprogramEntry} is
     *          {@code null}
     */
            
    protected void addProgramEntryRecord(org.osid.course.chronicle.records.ProgramEntrySearchOrderRecord programEntrySearchOrderRecord, 
                                     org.osid.type.Type programEntryRecordType) {

        addRecordType(programEntryRecordType);
        this.records.add(programEntrySearchOrderRecord);
        
        return;
    }
}
