//
// AbstractAdapterStockLookupSession.java
//
//    A Stock lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.inventory.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Stock lookup session adapter.
 */

public abstract class AbstractAdapterStockLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.inventory.StockLookupSession {

    private final org.osid.inventory.StockLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterStockLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterStockLookupSession(org.osid.inventory.StockLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Warehouse/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Warehouse Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.session.getWarehouseId());
    }


    /**
     *  Gets the {@code Warehouse} associated with this session.
     *
     *  @return the {@code Warehouse} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getWarehouse());
    }


    /**
     *  Tests if this user can perform {@code Stock} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupStocks() {
        return (this.session.canLookupStocks());
    }


    /**
     *  A complete view of the {@code Stock} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStockView() {
        this.session.useComparativeStockView();
        return;
    }


    /**
     *  A complete view of the {@code Stock} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStockView() {
        this.session.usePlenaryStockView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include stocks in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        this.session.useFederatedWarehouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        this.session.useIsolatedWarehouseView();
        return;
    }
    
     
    /**
     *  Gets the {@code Stock} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Stock} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Stock} and
     *  retained for compatibility.
     *
     *  @param stockId {@code Id} of the {@code Stock}
     *  @return the stock
     *  @throws org.osid.NotFoundException {@code stockId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code stockId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock(org.osid.id.Id stockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStock(stockId));
    }


    /**
     *  Gets a {@code StockList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stocks specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Stocks} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  stockIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Stock} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code stockIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByIds(org.osid.id.IdList stockIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStocksByIds(stockIds));
    }


    /**
     *  Gets a {@code StockList} corresponding to the given
     *  stock genus {@code Type} which does not include
     *  stocks of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stockGenusType a stock genus type 
     *  @return the returned {@code Stock} list
     *  @throws org.osid.NullArgumentException
     *          {@code stockGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByGenusType(org.osid.type.Type stockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStocksByGenusType(stockGenusType));
    }


    /**
     *  Gets a {@code StockList} corresponding to the given
     *  stock genus {@code Type} and include any additional
     *  stocks with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stockGenusType a stock genus type 
     *  @return the returned {@code Stock} list
     *  @throws org.osid.NullArgumentException
     *          {@code stockGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByParentGenusType(org.osid.type.Type stockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStocksByParentGenusType(stockGenusType));
    }


    /**
     *  Gets a {@code StockList} containing the given
     *  stock record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  stockRecordType a stock record type 
     *  @return the returned {@code Stock} list
     *  @throws org.osid.NullArgumentException
     *          {@code stockRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksByRecordType(org.osid.type.Type stockRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStocksByRecordType(stockRecordType));
    }


    /**
     *  Gets a {@code StockList} containing for the given SKU. In
     *  plenary mode, the returned list contains all known stocks or
     *  an error results. Otherwise, the returned list may contain
     *  only those stocks that are accessible through this session.
     *
     *  @param  sku a stock keeping unit 
     *  @return the returned {@code Stock} list 
     *  @throws org.osid.NullArgumentException {@code sku} is {@code 
     *          null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocksBySKU(String sku)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getStocksBySKU(sku));
    }
        

    /**
     *  Gets all {@code Stocks}. 
     *
     *  In plenary mode, the returned list contains all known
     *  stocks or an error results. Otherwise, the returned list
     *  may contain only those stocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Stocks} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.StockList getStocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getStocks());
    }
}
