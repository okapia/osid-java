//
// ImmutableGradebookColumnSummary.java
//
//     Wraps a mutable GradebookColumnSummary to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary;


/**
 *  Wraps a mutable <code>GradebookColumnSummary</code> to hide modifiers. This
 *  wrapper provides an immutized GradebookColumnSummary from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying gradebookColumnSummary whose state changes are visible.
 */

public final class ImmutableGradebookColumnSummary
    extends net.okapia.osid.jamocha.builder.grading.gradebookcolumnsummary.spi.AbstractImmutableGradebookColumnSummary
    implements org.osid.grading.GradebookColumnSummary {


    /**
     *  Constructs a new <code>ImmutableGradebookColumnSummary</code>.
     *
     *  @param gradebookColumnSummary the gradebook column summary
     *  @throws org.osid.NullArgumentException <code>gradebookColumnSummary</code>
     *          is <code>null</code>
     */

    public ImmutableGradebookColumnSummary(org.osid.grading.GradebookColumnSummary gradebookColumnSummary) {
        super(gradebookColumnSummary);
        return;
    }
}

