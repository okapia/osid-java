//
// AbstractMapQueueProcessorEnablerLookupSession
//
//    A simple framework for providing a QueueProcessorEnabler lookup service
//    backed by a fixed collection of queue processor enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.tracking.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a QueueProcessorEnabler lookup service backed by a
 *  fixed collection of queue processor enablers. The queue processor enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>QueueProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapQueueProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.tracking.rules.spi.AbstractQueueProcessorEnablerLookupSession
    implements org.osid.tracking.rules.QueueProcessorEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.tracking.rules.QueueProcessorEnabler> queueProcessorEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.tracking.rules.QueueProcessorEnabler>());


    /**
     *  Makes a <code>QueueProcessorEnabler</code> available in this session.
     *
     *  @param  queueProcessorEnabler a queue processor enabler
     *  @throws org.osid.NullArgumentException <code>queueProcessorEnabler<code>
     *          is <code>null</code>
     */

    protected void putQueueProcessorEnabler(org.osid.tracking.rules.QueueProcessorEnabler queueProcessorEnabler) {
        this.queueProcessorEnablers.put(queueProcessorEnabler.getId(), queueProcessorEnabler);
        return;
    }


    /**
     *  Makes an array of queue processor enablers available in this session.
     *
     *  @param  queueProcessorEnablers an array of queue processor enablers
     *  @throws org.osid.NullArgumentException <code>queueProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putQueueProcessorEnablers(org.osid.tracking.rules.QueueProcessorEnabler[] queueProcessorEnablers) {
        putQueueProcessorEnablers(java.util.Arrays.asList(queueProcessorEnablers));
        return;
    }


    /**
     *  Makes a collection of queue processor enablers available in this session.
     *
     *  @param  queueProcessorEnablers a collection of queue processor enablers
     *  @throws org.osid.NullArgumentException <code>queueProcessorEnablers<code>
     *          is <code>null</code>
     */

    protected void putQueueProcessorEnablers(java.util.Collection<? extends org.osid.tracking.rules.QueueProcessorEnabler> queueProcessorEnablers) {
        for (org.osid.tracking.rules.QueueProcessorEnabler queueProcessorEnabler : queueProcessorEnablers) {
            this.queueProcessorEnablers.put(queueProcessorEnabler.getId(), queueProcessorEnabler);
        }

        return;
    }


    /**
     *  Removes a QueueProcessorEnabler from this session.
     *
     *  @param  queueProcessorEnablerId the <code>Id</code> of the queue processor enabler
     *  @throws org.osid.NullArgumentException <code>queueProcessorEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeQueueProcessorEnabler(org.osid.id.Id queueProcessorEnablerId) {
        this.queueProcessorEnablers.remove(queueProcessorEnablerId);
        return;
    }


    /**
     *  Gets the <code>QueueProcessorEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  queueProcessorEnablerId <code>Id</code> of the <code>QueueProcessorEnabler</code>
     *  @return the queueProcessorEnabler
     *  @throws org.osid.NotFoundException <code>queueProcessorEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>queueProcessorEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnabler getQueueProcessorEnabler(org.osid.id.Id queueProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.tracking.rules.QueueProcessorEnabler queueProcessorEnabler = this.queueProcessorEnablers.get(queueProcessorEnablerId);
        if (queueProcessorEnabler == null) {
            throw new org.osid.NotFoundException("queueProcessorEnabler not found: " + queueProcessorEnablerId);
        }

        return (queueProcessorEnabler);
    }


    /**
     *  Gets all <code>QueueProcessorEnablers</code>. In plenary mode, the returned
     *  list contains all known queueProcessorEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  queueProcessorEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>QueueProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.tracking.rules.QueueProcessorEnablerList getQueueProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.tracking.rules.queueprocessorenabler.ArrayQueueProcessorEnablerList(this.queueProcessorEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.queueProcessorEnablers.clear();
        super.close();
        return;
    }
}
