//
// AbstractOsidEnablerQuery
//
//     Defines an OsidEnablerQuery.
//
//
// Tom Coppeto
// Okapia
// 20 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an OsidEnablerQuery.
 */

public abstract class AbstractOsidEnablerQuery
    extends AbstractOsidRuleQuery
    implements org.osid.OsidEnablerQuery {

    private final OsidTemporalQuery query = new OsidTemporalQuery();


    /**
     *  Match the <code> Id </code> of an associated schedule. 
     *
     *  @param  scheduleId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleId(org.osid.id.Id scheduleId, boolean match) {
        return;
    }


    /**
     *  Clears all schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleIdTerms() {
        return;
    }

    
    /**
     *  Tests if a <code> ScheduleQuery </code> for the rule is available. 
     *
     *  @return <code> true </code> if a schedule query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for the schedule. Each retrieval performs a boolean 
     *  <code> OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the schedule query 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuery getScheduleQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsScheduleQuery() is false");
    }


    /**
     *  Match any associated schedule. 
     *
     *  @param  match <code> true </code> to match any schedule, <code> false 
     *          </code> to match no schedules 
     */

    @OSID @Override
    public void matchAnySchedule(boolean match) {
        return;
    }


    /**
     *  Clears all schedule terms. 
     */

    @OSID @Override
    public void clearScheduleTerms() {
        return;
    }


    /**
     *  Match the <code> Id </code> of an associated event. 
     *
     *  @param  eventId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> eventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEventId(org.osid.id.Id eventId, boolean match) {
        return;
    }
    
    
    /**
     *  Clears all event <code> Id </code> terms. 
     */
    
    @OSID @Override
    public void clearEventIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> EventQuery </code> for the rule is available. 
     *
     *  @return <code> true </code> if an event query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for the event. Each retrieval performs a boolean <code> 
     *  OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the event query 
     *  @throws org.osid.UnimplementedException <code> supportsEventQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.EventQuery getEventQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsEventQuery() is false");
    }


    /**
     *  Match any associated event. 
     *
     *  @param  match <code> true </code> to match any event, <code> false 
     *          </code> to match no events 
     */

    @OSID @Override
    public void matchAnyEvent(boolean match) {
        return;
    }


    /**
     *  Clears all recurirng event terms. 
     */

    @OSID @Override
    public void clearEventTerms() {
        return;
    }


    /**
     *  Sets the cyclic event <code> Id </code> for this query. 
     *
     *  @param  cyclicEventId the cyclic event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cyclicEventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCyclicEventId(org.osid.id.Id cyclicEventId, boolean match) {
        return;
    }


    /**
     *  Clears the cyclic event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCyclicEventIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CyclicEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a cyclic event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cyclic event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the cyclic event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuery getCyclicEventQuery() {
        throw new org.osid.UnimplementedException("supportsCyclicEventQuery() is false");
    }


    /**
     *  Matches any enabler with a cyclic event. 
     *
     *  @param  match <code> true </code> to match any enablers with a cyclic 
     *          event, <code> false </code> to match enablers with no cyclic 
     *          events 
     */

    @OSID @Override
    public void matchAnyCyclicEvent(boolean match) {
        return;
    }


    /**
     *  Clears the cyclic event query terms. 
     */

    @OSID @Override
    public void clearCyclicEventTerms() {
        return;
    }


    /**
     *  Match the <code> Id </code> of the demographic resource. 
     *
     *  @param  resourceId <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDemographicId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears all resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDemographicIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> for the demographic is 
     *  available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicQuery() {
        return (false);
    }


    /**
     *  Gets the query for the resource. Each retrieval performs a boolean 
     *  <code> OR. </code> 
     *
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getDemographicQuery(boolean match) {
        throw new org.osid.UnimplementedException("supportsDemographicQuery() is false");
    }


    /**
     *  Match any associated resource. 
     *
     *  @param  match <code> true </code> to match any demographic, <code> 
     *          false </code> to match no rules 
     */

    @OSID @Override
    public void matchAnyDemographic(boolean match) {
        return;
    }

    
    /**
     *  Clears all demographic terms. 
     */

    @OSID @Override
    public void clearDemographicTerms() {
        return;
    }

    
    /**
     *  Match effective objects where the current date falls within the start 
     *  and end dates inclusive. 
     *
     *  @param  match <code> true </code> to match any effective, <code> false 
     *          </code> to match ineffective 
     */

    @OSID @Override
    public void matchEffective(boolean match) {
        this.query.matchEffective(match);
        return;
    }


    /**
     *  Clears the effective query terms. 
     */

    @OSID @Override
    public void clearEffectiveTerms() {
        this.query.clearEffectiveTerms();
        return;
    }

    
    /**
     *  Matches temporals whose start date falls in between the given dates 
     *  inclusive. 
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchStartDate(org.osid.calendaring.DateTime start, 
                               org.osid.calendaring.DateTime end, 
                               boolean match) {
        this.query.matchStartDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any start date set. 
     *
     *  @param  match <code> true </code> to match any start date, <code> 
     *          false </code> to match no start date 
     */

    @OSID @Override
    public void matchAnyStartDate(boolean match) {
        this.query.matchAnyStartDate(match);
        return;
    }


    /**
     *  Clears the start date query terms. 
     */

    @OSID @Override
    public void clearStartDateTerms() {
        this.query.clearStartDateTerms();
        return;
    }

    
    /**
     *  Matches temporals whose effective end date falls in between the given 
     *  dates inclusive. 
     *
     *  @param  start start of date range 
     *  @param  end end of date range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
     *          than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEndDate(org.osid.calendaring.DateTime start, 
                             org.osid.calendaring.DateTime end, 
                             boolean match) {

        this.query.matchEndDate(start, end, match);
        return;
    }


    /**
     *  Matches temporals with any end date set. 
     *
     *  @param match <code> true </code> to match any end date, <code>
     *         false </code> to match no start date
     */

    @OSID @Override
    public void matchAnyEndDate(boolean match) {
        this.query.matchAnyEndDate(match);
        return;
    }


    /**
     *  Clears the end date query terms. 
     */

    @OSID @Override
    public void clearEndDateTerms() {
        this.query.clearEndDateTerms();
        return;
    }


    /**
     *  Matches temporals where the given date is included within the
     *  start and end dates inclusive.
     *
     *  @param from start date
     *  @param to end datedate
     *  @param match <code>true</code> if a positive match,
     *          <code>false</code> for a negative match
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          less than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code> null </code>
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {
        this.query.matchDate(from, to, match);
        return;
    }


    /**
     *  Clears the date query terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        this.query.clearDateTerms();
        return;
    }


    protected class OsidTemporalQuery
        extends AbstractOsidTemporalQuery
        implements org.osid.OsidTemporalQuery {

        /**
         *  Match effective objects where the current date falls within the start 
         *  and end dates inclusive. 
         *
         *  @param  match <code> true </code> to match any effective, <code> false 
         *          </code> to match ineffective 
         */

        @OSID @Override
        public void matchEffective(boolean match) {
            super.matchEffective(match);
            return;
        }


        /**
         *  Clears the effective query terms. 
         */

        @OSID @Override
        public void clearEffectiveTerms() {
            super.clearEffectiveTerms();
            return;
        }

    
        /**
         *  Matches temporals whose start date falls in between the given dates 
         *  inclusive. 
         *
         *  @param  start start of date range 
         *  @param  end end of date range 
         *  @param  match <code> true </code> if a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
         *          than <code> end </code> 
         *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
         *          end </code> is <code> null </code> 
         */

        @OSID @Override
        public void matchStartDate(org.osid.calendaring.DateTime start, 
                                   org.osid.calendaring.DateTime end, 
                                   boolean match) {
            super.matchStartDate(start, end, match);
            return;
        }


        /**
         *  Matches temporals with any start date set. 
         *
         *  @param  match <code> true </code> to match any start date, <code> 
         *          false </code> to match no start date 
         */

        @OSID @Override
        public void matchAnyStartDate(boolean match) {
            super.matchAnyStartDate(match);
            return;
        }


        /**
         *  Clears the start date query terms. 
         */

        @OSID @Override
        public void clearStartDateTerms() {
            super.clearStartDateTerms();
            return;
        }

    
        /**
         *  Matches temporals whose effective end date falls in between the given 
         *  dates inclusive. 
         *
         *  @param  start start of date range 
         *  @param  end end of date range 
         *  @param  match <code> true </code> if a positive match, <code> false 
         *          </code> for negative match 
         *  @throws org.osid.InvalidArgumentException <code> start </code> is less 
         *          than <code> end </code> 
         *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
         *          end </code> is <code> null </code> 
         */

        @OSID @Override
        public void matchEndDate(org.osid.calendaring.DateTime start, 
                                 org.osid.calendaring.DateTime end, 
                                 boolean match) {

            super.matchEndDate(start, end, match);
            return;
        }



        /**
         *  Matches temporals with any end date set. 
         *
         *  @param match <code> true </code> to match any end date, <code>
         *         false </code> to match no start date
         */

        @OSID @Override
        public void matchAnyEndDate(boolean match) {
            super.matchAnyEndDate(match);
            return;
        }


        /**
         *  Clears the end date query terms. 
         */

        @OSID @Override
        public void clearEndDateTerms() {
            super.clearEndDateTerms();
            return;
        }


        /**
         *  Matches temporals where the given date range is included
         *  entirely within the start and end dates inclusive.
         *
         *  @param  from start date
         *  @param  to end date
         *  @param  match <code> true </code> if a positive match, <code> false 
         *          </code> for a negative match 
         *  @throws org.osid.InvalidArgumentException <code>from</code>
         *          is less than or <code>to</code>
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code> null </code>
         */

        @OSID @Override
        public void matchDate(org.osid.calendaring.DateTime from,
                              org.osid.calendaring.DateTime to, boolean match) {
            super.matchDate(from, to, match);
            return;
        }


        /**
         *  Clears the date query terms. 
         */

        @OSID @Override
        public void clearDateTerms() {
            super.clearDateTerms();
            return;
        }
    }
}
