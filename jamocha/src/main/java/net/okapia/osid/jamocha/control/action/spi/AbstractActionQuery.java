//
// AbstractActionQuery.java
//
//     A template for making an Action Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.action.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for actions.
 */

public abstract class AbstractActionQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.control.ActionQuery {

    private final java.util.Collection<org.osid.control.records.ActionQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the action group <code> Id </code> for this query. 
     *
     *  @param  actionGroupId an action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActionGroupId(org.osid.id.Id actionGroupId, boolean match) {
        return;
    }


    /**
     *  Clears the action group <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearActionGroupIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActionGroupQuery </code> is available. 
     *
     *  @return <code> true </code> if an action group query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for an action group. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the action group query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuery getActionGroupQuery() {
        throw new org.osid.UnimplementedException("supportsActionGroupQuery() is false");
    }


    /**
     *  Clears the action group query terms. 
     */

    @OSID @Override
    public void clearActionGroupTerms() {
        return;
    }


    /**
     *  Matches delays between the given range inclusive. 
     *
     *  @param  start start of range 
     *  @param  end end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start or end </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDelay(org.osid.calendaring.Duration start, 
                           org.osid.calendaring.Duration end, boolean match) {
        return;
    }


    /**
     *  Matches any delay. 
     *
     *  @param  match <code> true </code> to match actions with delays, <code> 
     *          false </code> to match actions with no delays 
     */

    @OSID @Override
    public void matchAnyDelay(boolean match) {
        return;
    }


    /**
     *  Clears the delay query terms. 
     */

    @OSID @Override
    public void clearDelayTerms() {
        return;
    }


    /**
     *  Matches blocking actions. 
     *
     *  @param  match <code> true </code> to match blocking actions, <code> 
     *          false </code> to match unblocking actions 
     */

    @OSID @Override
    public void matchBlocking(boolean match) {
        return;
    }


    /**
     *  Clears the blocking query terms. 
     */

    @OSID @Override
    public void clearBlockingTerms() {
        return;
    }


    /**
     *  Sets the executed action group <code> Id </code> for this query. 
     *
     *  @param  actionGroupId an action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchNextActionGroupId(org.osid.id.Id actionGroupId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the executed action group <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearNextActionGroupIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActionGroupQuery </code> is available. 
     *
     *  @return <code> true </code> if an action group query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNextActionGroupQuery() {
        return (false);
    }


    /**
     *  Gets the query for the action group to execute.. Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the action group query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsNextActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuery getNextActionGroupQuery() {
        throw new org.osid.UnimplementedException("supportsNextActionGroupQuery() is false");
    }


    /**
     *  Matches actions with any executable action group. 
     *
     *  @param  match <code> true </code> to match actions with any action 
     *          group, <code> false </code> to match actions with no action 
     *          groups 
     */

    @OSID @Override
    public void matchAnyNextActionGroup(boolean match) {
        return;
    }


    /**
     *  Clears the action group query terms. 
     */

    @OSID @Override
    public void clearNextActionGroupTerms() {
        return;
    }


    /**
     *  Sets the scene <code> Id </code> for this query. 
     *
     *  @param  sceneId a scene <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sceneId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSceneId(org.osid.id.Id sceneId, boolean match) {
        return;
    }


    /**
     *  Clears the scene <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSceneIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SceneQuery </code> is available. 
     *
     *  @return <code> true </code> if a scene query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneQuery() {
        return (false);
    }


    /**
     *  Gets the query for a scene. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the scene query 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuery getSceneQuery() {
        throw new org.osid.UnimplementedException("supportsSceneQuery() is false");
    }


    /**
     *  Matches actions with any scene. 
     *
     *  @param  match <code> true </code> to match actions with any scene, 
     *          <code> false </code> to match actions with no scene 
     */

    @OSID @Override
    public void matchAnyScene(boolean match) {
        return;
    }


    /**
     *  Clears the scene query terms. 
     */

    @OSID @Override
    public void clearSceneTerms() {
        return;
    }


    /**
     *  Sets the setting <code> Id </code> for this query. 
     *
     *  @param  settingId a setting <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> settingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSettingId(org.osid.id.Id settingId, boolean match) {
        return;
    }


    /**
     *  Clears the setting <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSettingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> SettingQuery </code> is available. 
     *
     *  @return <code> true </code> if a setting query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a setting. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the setting query 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuery getSettingQuery() {
        throw new org.osid.UnimplementedException("supportsSettingQuery() is false");
    }


    /**
     *  Matches actions with any setting. 
     *
     *  @param  match <code> true </code> to match actions with any setting, 
     *          <code> false </code> to match actions with no setting 
     */

    @OSID @Override
    public void matchAnySetting(boolean match) {
        return;
    }


    /**
     *  Clears the setting query terms. 
     */

    @OSID @Override
    public void clearSettingTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given action query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an action implementing the requested record.
     *
     *  @param actionRecordType an action record type
     *  @return the action query record
     *  @throws org.osid.NullArgumentException
     *          <code>actionRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(actionRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ActionQueryRecord getActionQueryRecord(org.osid.type.Type actionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.ActionQueryRecord record : this.records) {
            if (record.implementsRecordType(actionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(actionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this action query. 
     *
     *  @param actionQueryRecord action query record
     *  @param actionRecordType action record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActionQueryRecord(org.osid.control.records.ActionQueryRecord actionQueryRecord, 
                                          org.osid.type.Type actionRecordType) {

        addRecordType(actionRecordType);
        nullarg(actionQueryRecord, "action query record");
        this.records.add(actionQueryRecord);        
        return;
    }
}
