//
// LogEntryElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.logentry.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class LogEntryElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the LogEntryElement Id.
     *
     *  @return the log entry element Id
     */

    public static org.osid.id.Id getLogEntryEntityId() {
        return (makeEntityId("osid.tracking.LogEntry"));
    }


    /**
     *  Gets the AgentId element Id.
     *
     *  @return the AgentId element Id
     */

    public static org.osid.id.Id getAgentId() {
        return (makeElementId("osid.tracking.logentry.AgentId"));
    }


    /**
     *  Gets the Agent element Id.
     *
     *  @return the Agent element Id
     */

    public static org.osid.id.Id getAgent() {
        return (makeElementId("osid.tracking.logentry.Agent"));
    }


    /**
     *  Gets the IssueId element Id.
     *
     *  @return the IssueId element Id
     */

    public static org.osid.id.Id getIssueId() {
        return (makeElementId("osid.tracking.logentry.IssueId"));
    }


    /**
     *  Gets the Issue element Id.
     *
     *  @return the Issue element Id
     */

    public static org.osid.id.Id getIssue() {
        return (makeElementId("osid.tracking.logentry.Issue"));
    }


    /**
     *  Gets the Date element Id.
     *
     *  @return the Date element Id
     */

    public static org.osid.id.Id getDate() {
        return (makeElementId("osid.tracking.logentry.Date"));
    }


    /**
     *  Gets the Action element Id.
     *
     *  @return the Action element Id
     */

    public static org.osid.id.Id getAction() {
        return (makeElementId("osid.tracking.logentry.Action"));
    }


    /**
     *  Gets the Summary element Id.
     *
     *  @return the Summary element Id
     */

    public static org.osid.id.Id getSummary() {
        return (makeElementId("osid.tracking.logentry.Summary"));
    }


    /**
     *  Gets the Message element Id.
     *
     *  @return the Message element Id
     */

    public static org.osid.id.Id getMessage() {
        return (makeElementId("osid.tracking.logentry.Message"));
    }


    /**
     *  Gets the FrontOfficeId element Id.
     *
     *  @return the FrontOfficeId element Id
     */

    public static org.osid.id.Id getFrontOfficeId() {
        return (makeQueryElementId("osid.tracking.logentry.FrontOfficeId"));
    }


    /**
     *  Gets the FrontOffice element Id.
     *
     *  @return the FrontOffice element Id
     */

    public static org.osid.id.Id getFrontOffice() {
        return (makeQueryElementId("osid.tracking.logentry.FrontOffice"));
    }
}
