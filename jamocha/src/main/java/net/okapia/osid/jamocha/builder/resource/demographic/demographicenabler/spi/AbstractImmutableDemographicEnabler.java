//
// AbstractImmutableDemographicEnabler.java
//
//     Wraps a mutable DemographicEnabler to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resource.demographic.demographicenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>DemographicEnabler</code> to hide modifiers. This
 *  wrapper provides an immutized DemographicEnabler from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying demographicEnabler whose state changes are visible.
 */

public abstract class AbstractImmutableDemographicEnabler
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidEnabler
    implements org.osid.resource.demographic.DemographicEnabler {

    private final org.osid.resource.demographic.DemographicEnabler demographicEnabler;


    /**
     *  Constructs a new <code>AbstractImmutableDemographicEnabler</code>.
     *
     *  @param demographicEnabler the demographic enabler to immutablize
     *  @throws org.osid.NullArgumentException <code>demographicEnabler</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDemographicEnabler(org.osid.resource.demographic.DemographicEnabler demographicEnabler) {
        super(demographicEnabler);
        this.demographicEnabler = demographicEnabler;
        return;
    }


    /**
     *  Gets the demographic enabler record corresponding to the given <code> 
     *  DemographicEnabler </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> demographicEnablerRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(demographicEnablerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  demographicEnablerRecordType the type of demographic enabler 
     *          record to retrieve 
     *  @return the demographic enabler record 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(demographicEnablerRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.records.DemographicEnablerRecord getDemographicEnablerRecord(org.osid.type.Type demographicEnablerRecordType)
        throws org.osid.OperationFailedException {

        return (this.demographicEnabler.getDemographicEnablerRecord(demographicEnablerRecordType));
    }
}

