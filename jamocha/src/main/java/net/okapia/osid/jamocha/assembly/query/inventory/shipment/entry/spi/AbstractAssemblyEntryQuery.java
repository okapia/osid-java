//
// AbstractAssemblyEntryQuery.java
//
//     An EntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.inventory.shipment.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An EntryQuery that stores terms.
 */

public abstract class AbstractAssemblyEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.inventory.shipment.EntryQuery,
               org.osid.inventory.shipment.EntryQueryInspector,
               org.osid.inventory.shipment.EntrySearchOrder {

    private final java.util.Collection<org.osid.inventory.shipment.records.EntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.shipment.records.EntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.inventory.shipment.records.EntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the stock <code> Id </code> for this query. 
     *
     *  @param  stockId a stock <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStockId(org.osid.id.Id stockId, boolean match) {
        getAssembler().addIdTerm(getStockIdColumn(), stockId, match);
        return;
    }


    /**
     *  Clears the stock <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStockIdTerms() {
        getAssembler().clearTerms(getStockIdColumn());
        return;
    }


    /**
     *  Gets the stock <code> Id </code> query terms. 
     *
     *  @return the stock <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStockIdTerms() {
        return (getAssembler().getIdTerms(getStockIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by stock. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStock(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStockColumn(), style);
        return;
    }


    /**
     *  Gets the StockId column name.
     *
     * @return the column name
     */

    protected String getStockIdColumn() {
        return ("stock_id");
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getStockQuery() {
        throw new org.osid.UnimplementedException("supportsStockQuery() is false");
    }


    /**
     *  Clears the stock terms. 
     */

    @OSID @Override
    public void clearStockTerms() {
        getAssembler().clearTerms(getStockColumn());
        return;
    }


    /**
     *  Gets the stock query terms. 
     *
     *  @return the stock query terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Tests if a stock search order is available. 
     *
     *  @return <code> true </code> if a stock search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockSearchOrder() {
        return (false);
    }


    /**
     *  Gets the shipment order. 
     *
     *  @return the stock search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchOrder getStockSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStockSearchOrder() is false");
    }


    /**
     *  Gets the Stock column name.
     *
     * @return the column name
     */

    protected String getStockColumn() {
        return ("stock");
    }


    /**
     *  Sets the model <code> Id </code> for this query. 
     *
     *  @param  modelId a model <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> modelId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchModelId(org.osid.id.Id modelId, boolean match) {
        getAssembler().addIdTerm(getModelIdColumn(), modelId, match);
        return;
    }


    /**
     *  Clears the model <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearModelIdTerms() {
        getAssembler().clearTerms(getModelIdColumn());
        return;
    }


    /**
     *  Gets the model <code> Id </code> query terms. 
     *
     *  @return the model <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getModelIdTerms() {
        return (getAssembler().getIdTerms(getModelIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the model. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByModel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getModelColumn(), style);
        return;
    }


    /**
     *  Gets the ModelId column name.
     *
     * @return the column name
     */

    protected String getModelIdColumn() {
        return ("model_id");
    }


    /**
     *  Tests if a <code> ModelQuery </code> is available. 
     *
     *  @return <code> true </code> if a model query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelQuery() {
        return (false);
    }


    /**
     *  Gets the query for a model. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the model query 
     *  @throws org.osid.UnimplementedException <code> supportsModelQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelQuery getModelQuery() {
        throw new org.osid.UnimplementedException("supportsModelQuery() is false");
    }


    /**
     *  Matches entries that have any model set. 
     *
     *  @param  match <code> true </code> to match entries with any model, 
     *          <code> false </code> to match entries with no model 
     */

    @OSID @Override
    public void matchAnyModel(boolean match) {
        getAssembler().addIdWildcardTerm(getModelColumn(), match);
        return;
    }


    /**
     *  Clears the model terms. 
     */

    @OSID @Override
    public void clearModelTerms() {
        getAssembler().clearTerms(getModelColumn());
        return;
    }


    /**
     *  Gets the model query terms. 
     *
     *  @return the model query terms 
     */

    @OSID @Override
    public org.osid.inventory.ModelQueryInspector[] getModelTerms() {
        return (new org.osid.inventory.ModelQueryInspector[0]);
    }


    /**
     *  Tests if a model search order is available. 
     *
     *  @return <code> true </code> if a model search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsModelSearchOrder() {
        return (false);
    }


    /**
     *  Gets the model search order. 
     *
     *  @return the item search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsModelSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ModelSearchOrder getModelSearchOrder() {
        throw new org.osid.UnimplementedException("supportsModelSearchOrder() is false");
    }


    /**
     *  Gets the Model column name.
     *
     * @return the column name
     */

    protected String getModelColumn() {
        return ("model");
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        getAssembler().addIdTerm(getItemIdColumn(), itemId, match);
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        getAssembler().clearTerms(getItemIdColumn());
        return;
    }


    /**
     *  Gets the item <code> Id </code> query terms. 
     *
     *  @return the item <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getItemIdTerms() {
        return (getAssembler().getIdTerms(getItemIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the item. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByItem(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getItemColumn(), style);
        return;
    }


    /**
     *  Gets the ItemId column name.
     *
     * @return the column name
     */

    protected String getItemIdColumn() {
        return ("item_id");
    }


    /**
     *  Tests if an <code> ItemQuery </code> is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches entries that have any item set. 
     *
     *  @param  match <code> true </code> to match entries with any item, 
     *          <code> false </code> to match entries with no item 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        getAssembler().addIdWildcardTerm(getItemColumn(), match);
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        getAssembler().clearTerms(getItemColumn());
        return;
    }


    /**
     *  Gets the item query terms. 
     *
     *  @return the item query terms 
     */

    @OSID @Override
    public org.osid.inventory.ItemQueryInspector[] getItemTerms() {
        return (new org.osid.inventory.ItemQueryInspector[0]);
    }


    /**
     *  Tests if an item search order is available. 
     *
     *  @return <code> true </code> if an item search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearchOrder() {
        return (false);
    }


    /**
     *  Gets the item search order. 
     *
     *  @return the item search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.ItemSearchOrder getItemSearchOrder() {
        throw new org.osid.UnimplementedException("supportsItemSearchOrder() is false");
    }


    /**
     *  Gets the Item column name.
     *
     * @return the column name
     */

    protected String getItemColumn() {
        return ("item");
    }


    /**
     *  Matches entries with a quantity between the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchQuantity(java.math.BigDecimal low, 
                              java.math.BigDecimal high, boolean match) {
        getAssembler().addDecimalRangeTerm(getQuantityColumn(), low, high, match);
        return;
    }


    /**
     *  Matches entries that have any quantity set. 
     *
     *  @param  match <code> true </code> to match entries with any quantity, 
     *          <code> false </code> to match entries with no quantity 
     */

    @OSID @Override
    public void matchAnyQuantity(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getQuantityColumn(), match);
        return;
    }


    /**
     *  Clears the quantity terms. 
     */

    @OSID @Override
    public void clearQuantityTerms() {
        getAssembler().clearTerms(getQuantityColumn());
        return;
    }


    /**
     *  Gets the quantity terms. 
     *
     *  @return the quantity terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getQuantityTerms() {
        return (getAssembler().getDecimalRangeTerms(getQuantityColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by quantity. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQuantity(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getQuantityColumn(), style);
        return;
    }


    /**
     *  Gets the Quantity column name.
     *
     * @return the column name
     */

    protected String getQuantityColumn() {
        return ("quantity");
    }


    /**
     *  Matches the a unit type. 
     *
     *  @param  type a unit tytpe 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> type </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchUnitType(org.osid.type.Type type, boolean match) {
        getAssembler().addTypeTerm(getUnitTypeColumn(), type, match);
        return;
    }


    /**
     *  Matches entries that have any unit type set. 
     *
     *  @param  match <code> true </code> to match entries with any unit type, 
     *          <code> false </code> to match entries with no unit type 
     */

    @OSID @Override
    public void matchAnyUnitType(boolean match) {
        getAssembler().addTypeWildcardTerm(getUnitTypeColumn(), match);
        return;
    }


    /**
     *  Clears the unit type terms. 
     */

    @OSID @Override
    public void clearUnitTypeTerms() {
        getAssembler().clearTerms(getUnitTypeColumn());
        return;
    }


    /**
     *  Gets the amount query terms. 
     *
     *  @return the unit type query terms 
     */

    @OSID @Override
    public org.osid.search.terms.TypeTerm[] getUnitTypeTerms() {
        return (getAssembler().getTypeTerms(getUnitTypeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the unit type. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByUnitType(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getUnitTypeColumn(), style);
        return;
    }


    /**
     *  Gets the UnitType column name.
     *
     * @return the column name
     */

    protected String getUnitTypeColumn() {
        return ("unit_type");
    }


    /**
     *  Sets the warehouse <code> Id </code> for this query to match entries 
     *  assigned to warehouses. 
     *
     *  @param  warehouseId the warehouse <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> warehouseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchWarehouseId(org.osid.id.Id warehouseId, boolean match) {
        getAssembler().addIdTerm(getWarehouseIdColumn(), warehouseId, match);
        return;
    }


    /**
     *  Clears the warehouse <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearWarehouseIdTerms() {
        getAssembler().clearTerms(getWarehouseIdColumn());
        return;
    }


    /**
     *  Gets the warehouse <code> Id </code> query terms. 
     *
     *  @return the warehouse <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getWarehouseIdTerms() {
        return (getAssembler().getIdTerms(getWarehouseIdColumn()));
    }


    /**
     *  Gets the WarehouseId column name.
     *
     * @return the column name
     */

    protected String getWarehouseIdColumn() {
        return ("warehouse_id");
    }


    /**
     *  Tests if a <code> WarehouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a warehouse query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsWarehouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a warehouse. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the warehouse query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsWarehouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQuery getWarehouseQuery() {
        throw new org.osid.UnimplementedException("supportsWarehouseQuery() is false");
    }


    /**
     *  Clears the warehouse terms. 
     */

    @OSID @Override
    public void clearWarehouseTerms() {
        getAssembler().clearTerms(getWarehouseColumn());
        return;
    }


    /**
     *  Gets the warehouse query terms. 
     *
     *  @return the warehouse query terms 
     */

    @OSID @Override
    public org.osid.inventory.WarehouseQueryInspector[] getWarehouseTerms() {
        return (new org.osid.inventory.WarehouseQueryInspector[0]);
    }


    /**
     *  Gets the Warehouse column name.
     *
     * @return the column name
     */

    protected String getWarehouseColumn() {
        return ("warehouse");
    }


    /**
     *  Tests if this entry supports the given record
     *  <code>Type</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return <code>true</code> if the entryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type entryRecordType) {
        for (org.osid.inventory.shipment.records.EntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.EntryQueryRecord getEntryQueryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.EntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.EntryQueryInspectorRecord getEntryQueryInspectorRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.EntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param entryRecordType the entry record type
     *  @return the entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.EntrySearchOrderRecord getEntrySearchOrderRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.EntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param entryQueryRecord the entry query record
     *  @param entryQueryInspectorRecord the entry query inspector
     *         record
     *  @param entrySearchOrderRecord the entry search order record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>entryQueryRecord</code>,
     *          <code>entryQueryInspectorRecord</code>,
     *          <code>entrySearchOrderRecord</code> or
     *          <code>entryRecordTypeentry</code> is
     *          <code>null</code>
     */
            
    protected void addEntryRecords(org.osid.inventory.shipment.records.EntryQueryRecord entryQueryRecord, 
                                      org.osid.inventory.shipment.records.EntryQueryInspectorRecord entryQueryInspectorRecord, 
                                      org.osid.inventory.shipment.records.EntrySearchOrderRecord entrySearchOrderRecord, 
                                      org.osid.type.Type entryRecordType) {

        addRecordType(entryRecordType);

        nullarg(entryQueryRecord, "entry query record");
        nullarg(entryQueryInspectorRecord, "entry query inspector record");
        nullarg(entrySearchOrderRecord, "entry search odrer record");

        this.queryRecords.add(entryQueryRecord);
        this.queryInspectorRecords.add(entryQueryInspectorRecord);
        this.searchOrderRecords.add(entrySearchOrderRecord);
        
        return;
    }
}
