//
// AbstractQueueProcessorEnabler.java
//
//     Defines a QueueProcessorEnabler builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.queueprocessorenabler.spi;


/**
 *  Defines a <code>QueueProcessorEnabler</code> builder.
 */

public abstract class AbstractQueueProcessorEnablerBuilder<T extends AbstractQueueProcessorEnablerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidEnablerBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.provisioning.rules.queueprocessorenabler.QueueProcessorEnablerMiter queueProcessorEnabler;


    /**
     *  Constructs a new <code>AbstractQueueProcessorEnablerBuilder</code>.
     *
     *  @param queueProcessorEnabler the queue processor enabler to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractQueueProcessorEnablerBuilder(net.okapia.osid.jamocha.builder.provisioning.rules.queueprocessorenabler.QueueProcessorEnablerMiter queueProcessorEnabler) {
        super(queueProcessorEnabler);
        this.queueProcessorEnabler = queueProcessorEnabler;
        return;
    }


    /**
     *  Builds the queue processor enabler.
     *
     *  @return the new queue processor enabler
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.provisioning.rules.QueueProcessorEnabler build() {
        (new net.okapia.osid.jamocha.builder.validator.provisioning.rules.queueprocessorenabler.QueueProcessorEnablerValidator(getValidations())).validate(this.queueProcessorEnabler);
        return (new net.okapia.osid.jamocha.builder.provisioning.rules.queueprocessorenabler.ImmutableQueueProcessorEnabler(this.queueProcessorEnabler));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the queue processor enabler miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.provisioning.rules.queueprocessorenabler.QueueProcessorEnablerMiter getMiter() {
        return (this.queueProcessorEnabler);
    }


    /**
     *  Adds a QueueProcessorEnabler record.
     *
     *  @param record a queue processor enabler record
     *  @param recordType the type of queue processor enabler record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.provisioning.rules.records.QueueProcessorEnablerRecord record, org.osid.type.Type recordType) {
        getMiter().addQueueProcessorEnablerRecord(record, recordType);
        return (self());
    }
}       


