//
// BuildingElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.building.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class BuildingElements
    extends net.okapia.osid.jamocha.spi.TemporalOsidObjectElements {


    /**
     *  Gets the BuildingElement Id.
     *
     *  @return the building element Id
     */

    public static org.osid.id.Id getBuildingEntityId() {
        return (makeEntityId("osid.room.Building"));
    }


    /**
     *  Gets the AddressId element Id.
     *
     *  @return the AddressId element Id
     */

    public static org.osid.id.Id getAddressId() {
        return (makeElementId("osid.room.building.AddressId"));
    }


    /**
     *  Gets the Address element Id.
     *
     *  @return the Address element Id
     */

    public static org.osid.id.Id getAddress() {
        return (makeElementId("osid.room.building.Address"));
    }


    /**
     *  Gets the OfficialName element Id.
     *
     *  @return the OfficialName element Id
     */

    public static org.osid.id.Id getOfficialName() {
        return (makeElementId("osid.room.building.OfficialName"));
    }


    /**
     *  Gets the Number element Id.
     *
     *  @return the Number element Id
     */

    public static org.osid.id.Id getNumber() {
        return (makeElementId("osid.room.building.Number"));
    }


    /**
     *  Gets the EnclosingBuildingId element Id.
     *
     *  @return the EnclosingBuildingId element Id
     */

    public static org.osid.id.Id getEnclosingBuildingId() {
        return (makeElementId("osid.room.building.EnclosingBuildingId"));
    }


    /**
     *  Gets the EnclosingBuilding element Id.
     *
     *  @return the EnclosingBuilding element Id
     */

    public static org.osid.id.Id getEnclosingBuilding() {
        return (makeElementId("osid.room.building.EnclosingBuilding"));
    }


    /**
     *  Gets the SubdivisionIds element Id.
     *
     *  @return the SubdivisionIds element Id
     */

    public static org.osid.id.Id getSubdivisionIds() {
        return (makeElementId("osid.room.building.SubdivisionIds"));
    }


    /**
     *  Gets the Subdivisions element Id.
     *
     *  @return the Subdivisions element Id
     */

    public static org.osid.id.Id getSubdivisions() {
        return (makeElementId("osid.room.building.Subdivisions"));
    }


    /**
     *  Gets the GrossArea element Id.
     *
     *  @return the GrossArea element Id
     */

    public static org.osid.id.Id getGrossArea() {
        return (makeElementId("osid.room.building.GrossArea"));
    }


    /**
     *  Gets the RoomId element Id.
     *
     *  @return the RoomId element Id
     */

    public static org.osid.id.Id getRoomId() {
        return (makeQueryElementId("osid.room.building.RoomId"));
    }


    /**
     *  Gets the Room element Id.
     *
     *  @return the Room element Id
     */

    public static org.osid.id.Id getRoom() {
        return (makeQueryElementId("osid.room.building.Room"));
    }


    /**
     *  Gets the CampusId element Id.
     *
     *  @return the CampusId element Id
     */

    public static org.osid.id.Id getCampusId() {
        return (makeQueryElementId("osid.room.building.CampusId"));
    }


    /**
     *  Gets the Campus element Id.
     *
     *  @return the Campus element Id
     */

    public static org.osid.id.Id getCampus() {
        return (makeQueryElementId("osid.room.building.Campus"));
    }
}
