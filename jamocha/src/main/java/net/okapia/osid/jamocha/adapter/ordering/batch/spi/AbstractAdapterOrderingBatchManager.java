//
// AbstractOrderingBatchManager.java
//
//     An adapter for a OrderingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ordering.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a OrderingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterOrderingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.ordering.batch.OrderingBatchManager>
    implements org.osid.ordering.batch.OrderingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterOrderingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterOrderingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterOrderingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterOrderingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of orders is available. 
     *
     *  @return <code> true </code> if an order bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrderBatchAdmin() {
        return (getAdapteeManager().supportsOrderBatchAdmin());
    }


    /**
     *  Tests if bulk administration of items is available. 
     *
     *  @return <code> true </code> if an item bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBatchAdmin() {
        return (getAdapteeManager().supportsItemBatchAdmin());
    }


    /**
     *  Tests if bulk administration of price schedules is available. 
     *
     *  @return <code> true </code> if a price schedule bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleBatchAdmin() {
        return (getAdapteeManager().supportsPriceScheduleBatchAdmin());
    }


    /**
     *  Tests if bulk administration of products is available. 
     *
     *  @return <code> true </code> if a product bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProductBatchAdmin() {
        return (getAdapteeManager().supportsProductBatchAdmin());
    }


    /**
     *  Tests if bulk administration of stores is available. 
     *
     *  @return <code> true </code> if a store bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreBatchAdmin() {
        return (getAdapteeManager().supportsStoreBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk order 
     *  administration service. 
     *
     *  @return a <code> OrderBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.OrderBatchAdminSession getOrderBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk order 
     *  administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> OrderBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrderBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.OrderBatchAdminSession getOrderBatchAdminSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getOrderBatchAdminSessionForStore(storeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service. 
     *
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.ItemBatchAdminSession getItemBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk item 
     *  administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return an <code> ItemBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.ItemBatchAdminSession getItemBatchAdminSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBatchAdminSessionForStore(storeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk product 
     *  administration service. 
     *
     *  @return a <code> ProductBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.ProductBatchAdminSession getProductBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getProductBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk product 
     *  administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> ProductBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProductBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.ProductBatchAdminSession getProductBatchAdminSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getProductBatchAdminSessionForStore(storeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk price 
     *  schedule administration service. 
     *
     *  @return a <code> PriceScheduleBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.PriceScheduleBatchAdminSession getPriceScheduleBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk price 
     *  schedule administration service for the given store. 
     *
     *  @param  storeId the <code> Id </code> of the <code> Store </code> 
     *  @return a <code> PriceScheduleBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Store </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.PriceScheduleBatchAdminSession getPriceScheduleBatchAdminSessionForStore(org.osid.id.Id storeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getPriceScheduleBatchAdminSessionForStore(storeId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk store 
     *  administration service. 
     *
     *  @return a <code> StoreBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStoreBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.batch.StoreBatchAdminSession getStoreBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getStoreBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
