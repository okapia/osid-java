//
// AbstractQueryOntologyLookupSession.java
//
//    An inline adapter that maps an OntologyLookupSession to
//    an OntologyQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an OntologyLookupSession to
 *  an OntologyQuerySession.
 */

public abstract class AbstractQueryOntologyLookupSession
    extends net.okapia.osid.jamocha.ontology.spi.AbstractOntologyLookupSession
    implements org.osid.ontology.OntologyLookupSession {

    private final org.osid.ontology.OntologyQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryOntologyLookupSession.
     *
     *  @param querySession the underlying ontology query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryOntologyLookupSession(org.osid.ontology.OntologyQuerySession querySession) {
        nullarg(querySession, "ontology query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Ontology</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOntologies() {
        return (this.session.canSearchOntologies());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Ontology</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Ontology</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Ontology</code> and
     *  retained for compatibility.
     *
     *  @param  ontologyId <code>Id</code> of the
     *          <code>Ontology</code>
     *  @return the ontology
     *  @throws org.osid.NotFoundException <code>ontologyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ontologyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology(org.osid.id.Id ontologyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.OntologyQuery query = getQuery();
        query.matchId(ontologyId, true);
        org.osid.ontology.OntologyList ontologies = this.session.getOntologiesByQuery(query);
        if (ontologies.hasNext()) {
            return (ontologies.getNextOntology());
        } 
        
        throw new org.osid.NotFoundException(ontologyId + " not found");
    }


    /**
     *  Gets an <code>OntologyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  ontologies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Ontologies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  ontologyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Ontology</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByIds(org.osid.id.IdList ontologyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.OntologyQuery query = getQuery();

        try (org.osid.id.IdList ids = ontologyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getOntologiesByQuery(query));
    }


    /**
     *  Gets an <code>OntologyList</code> corresponding to the given
     *  ontology genus <code>Type</code> which does not include
     *  ontologies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  ontologyGenusType an ontology genus type 
     *  @return the returned <code>Ontology</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByGenusType(org.osid.type.Type ontologyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.OntologyQuery query = getQuery();
        query.matchGenusType(ontologyGenusType, true);
        return (this.session.getOntologiesByQuery(query));
    }


    /**
     *  Gets an <code>OntologyList</code> corresponding to the given
     *  ontology genus <code>Type</code> and include any additional
     *  ontologies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  ontologyGenusType an ontology genus type 
     *  @return the returned <code>Ontology</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByParentGenusType(org.osid.type.Type ontologyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.OntologyQuery query = getQuery();
        query.matchParentGenusType(ontologyGenusType, true);
        return (this.session.getOntologiesByQuery(query));
    }


    /**
     *  Gets an <code>OntologyList</code> containing the given
     *  ontology record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  ontologyRecordType an ontology record type 
     *  @return the returned <code>Ontology</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ontologyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByRecordType(org.osid.type.Type ontologyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.OntologyQuery query = getQuery();
        query.matchRecordType(ontologyRecordType, true);
        return (this.session.getOntologiesByQuery(query));
    }


    /**
     *  Gets an <code>OntologyList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known ontologies or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  ontologies that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Ontology</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.OntologyQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getOntologiesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Ontologies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  ontologies or an error results. Otherwise, the returned list
     *  may contain only those ontologies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Ontologies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.OntologyList getOntologies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.ontology.OntologyQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getOntologiesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.ontology.OntologyQuery getQuery() {
        org.osid.ontology.OntologyQuery query = this.session.getOntologyQuery();
        return (query);
    }
}
