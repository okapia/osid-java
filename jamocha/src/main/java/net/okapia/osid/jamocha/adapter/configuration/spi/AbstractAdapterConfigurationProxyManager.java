//
// AbstractConfigurationProxyManager.java
//
//     An adapter for a ConfigurationProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ConfigurationProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterConfigurationProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.configuration.ConfigurationProxyManager>
    implements org.osid.configuration.ConfigurationProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterConfigurationProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterConfigurationProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterConfigurationProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterConfigurationProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible for this service. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a configuration value retrieval service. 
     *
     *  @return <code> true </code> if value retrieval is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueRetrieval() {
        return (getAdapteeManager().supportsValueRetrieval());
    }


    /**
     *  Tests for the availability of a configuration value lookup service. 
     *
     *  @return <code> true </code> if value lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueLookup() {
        return (getAdapteeManager().supportsValueLookup());
    }


    /**
     *  Tests for the availability of a configuration value query service. 
     *
     *  @return <code> true </code> if value query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueQuery() {
        return (getAdapteeManager().supportsValueQuery());
    }


    /**
     *  Tests for the availability of a configuration value search service. 
     *
     *  @return <code> true </code> if value search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueSearch() {
        return (getAdapteeManager().supportsValueSearch());
    }


    /**
     *  Tests for the availability of a configuration value administration 
     *  service. 
     *
     *  @return <code> true </code> if value administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueAdmin() {
        return (getAdapteeManager().supportsValueAdmin());
    }


    /**
     *  Tests for the availability of a configuration value notification 
     *  service. 
     *
     *  @return <code> true </code> if value notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsValueNotification() {
        return (getAdapteeManager().supportsValueNotification());
    }


    /**
     *  Tests for the availability of a parameter lookup service. 
     *
     *  @return <code> true </code> if parameter lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterLookup() {
        return (getAdapteeManager().supportsParameterLookup());
    }


    /**
     *  Tests for the availability of a parameter query service. 
     *
     *  @return <code> true </code> if parameter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterQuery() {
        return (getAdapteeManager().supportsParameterQuery());
    }


    /**
     *  Tests for the availability of a parameter search service. 
     *
     *  @return <code> true </code> if parameter search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterSearch() {
        return (getAdapteeManager().supportsParameterSearch());
    }


    /**
     *  Tests for the availability of a parameter update service. 
     *
     *  @return <code> true </code> if parameter update is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterAdmin() {
        return (getAdapteeManager().supportsParameterAdmin());
    }


    /**
     *  Tests for the availability of a parameter notification service. 
     *
     *  @return <code> true </code> if parameter notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterNotification() {
        return (getAdapteeManager().supportsParameterNotification());
    }


    /**
     *  Tests for the availability of a service to lookup mappings of 
     *  parameters to configurations. 
     *
     *  @return <code> true </code> if parameter configuration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterConfiguration() {
        return (getAdapteeManager().supportsParameterConfiguration());
    }


    /**
     *  Tests for the availability of a service to map parameters to 
     *  configurations. 
     *
     *  @return <code> true </code> if parameter configuration assignment is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterConfigurationAssignment() {
        return (getAdapteeManager().supportsParameterConfigurationAssignment());
    }


    /**
     *  Tests for the availability of a parameter smart configuration service. 
     *
     *  @return <code> true </code> if parameter smart configuration service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsParameterSmartConfiguration() {
        return (getAdapteeManager().supportsParameterSmartConfiguration());
    }


    /**
     *  Tests for the availability of a configuration lookup service. 
     *
     *  @return <code> true </code> if configuration lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationLookup() {
        return (getAdapteeManager().supportsConfigurationLookup());
    }


    /**
     *  Tests for the availability of a configuration query service. 
     *
     *  @return <code> true </code> if configuration query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationQuery() {
        return (getAdapteeManager().supportsConfigurationQuery());
    }


    /**
     *  Tests for the availability of a configuration search service. 
     *
     *  @return <code> true </code> if configuration search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationSearch() {
        return (getAdapteeManager().supportsConfigurationSearch());
    }


    /**
     *  Tests for the availability of a configuration admin service. 
     *
     *  @return <code> true </code> if configuration admin is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationAdmin() {
        return (getAdapteeManager().supportsConfigurationAdmin());
    }


    /**
     *  Tests for the availability of a notification service for subscribing 
     *  to changes to configurations. 
     *
     *  @return <code> true </code> if a configuration notification service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationNotification() {
        return (getAdapteeManager().supportsConfigurationNotification());
    }


    /**
     *  Tests for the availability of a configuration hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if a configuration hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationHierarchy() {
        return (getAdapteeManager().supportsConfigurationHierarchy());
    }


    /**
     *  Tests for the availability of a configuration hierarchy design 
     *  service. 
     *
     *  @return <code> true </code> if a configuration hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationHierarchyDesign() {
        return (getAdapteeManager().supportsConfigurationHierarchyDesign());
    }


    /**
     *  Tests for the availability of a configuration batch service. 
     *
     *  @return <code> true </code> if a configuration batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationBatch() {
        return (getAdapteeManager().supportsConfigurationBatch());
    }


    /**
     *  Tests for the availability of a configuration rules service. 
     *
     *  @return <code> true </code> if a configuration rules service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConfigurationRules() {
        return (getAdapteeManager().supportsConfigurationRules());
    }


    /**
     *  Gets the supported value condition record types. 
     *
     *  @return a list containing the supported <code> ValueCondition </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueConditionRecordTypes() {
        return (getAdapteeManager().getValueConditionRecordTypes());
    }


    /**
     *  Tests if the given <code> ValueCondition </code> record type is 
     *  supported. 
     *
     *  @param  valueConditionRecordType a <code> Type </code> indicating a 
     *          <code> ValueCondition </code> record type 
     *  @return <code> true </code> if the given value condition record <code> 
     *          Type </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> valueConditionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueConditionRecordType(org.osid.type.Type valueConditionRecordType) {
        return (getAdapteeManager().supportsValueConditionRecordType(valueConditionRecordType));
    }


    /**
     *  Gets all the value record types supported. 
     *
     *  @return the list of supported value record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueRecordTypes() {
        return (getAdapteeManager().getValueRecordTypes());
    }


    /**
     *  Tests if a given value record type is supported. 
     *
     *  @param  valueRecordType the value record type 
     *  @return <code> true </code> if the value record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> valueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueRecordType(org.osid.type.Type valueRecordType) {
        return (getAdapteeManager().supportsValueRecordType(valueRecordType));
    }


    /**
     *  Gets all the value search record types supported. 
     *
     *  @return the list of supported value search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getValueSearchRecordTypes() {
        return (getAdapteeManager().getValueSearchRecordTypes());
    }


    /**
     *  Tests if a given value search type is supported. 
     *
     *  @param  valueSearchRecordType the value search record type 
     *  @return <code> true </code> if the value search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> valueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsValueSearchRecordType(org.osid.type.Type valueSearchRecordType) {
        return (getAdapteeManager().supportsValueSearchRecordType(valueSearchRecordType));
    }


    /**
     *  Gets all the parameter record types supported. 
     *
     *  @return the list of supported parameter record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterRecordTypes() {
        return (getAdapteeManager().getParameterRecordTypes());
    }


    /**
     *  Tests if a given parameter record type is supported. 
     *
     *  @param  parameterRecordType a parameter record type 
     *  @return <code> true </code> if the parameter record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> parameterRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParameterRecordType(org.osid.type.Type parameterRecordType) {
        return (getAdapteeManager().supportsParameterRecordType(parameterRecordType));
    }


    /**
     *  Gets all the parameter search record types supported. 
     *
     *  @return the list of supported parameter search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getParameterSearchRecordTypes() {
        return (getAdapteeManager().getParameterSearchRecordTypes());
    }


    /**
     *  Tests if a given parameter search record type is supported. 
     *
     *  @param  parameterSearchRecordType the value search type 
     *  @return <code> true </code> if the parameter search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          parameterSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsParameterSearchRecordType(org.osid.type.Type parameterSearchRecordType) {
        return (getAdapteeManager().supportsParameterSearchRecordType(parameterSearchRecordType));
    }


    /**
     *  Gets all the configuration record types supported. 
     *
     *  @return the list of supported configuration record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConfigurationRecordTypes() {
        return (getAdapteeManager().getConfigurationRecordTypes());
    }


    /**
     *  Tests if a given configuration record type is supported. 
     *
     *  @param  configurationRecordType a configuration record type 
     *  @return <code> true </code> if the configuration record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> configurationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConfigurationRecordType(org.osid.type.Type configurationRecordType) {
        return (getAdapteeManager().supportsConfigurationRecordType(configurationRecordType));
    }


    /**
     *  Gets all the configuration search record types supported. 
     *
     *  @return the list of supported configuration search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConfigurationSearchRecordTypes() {
        return (getAdapteeManager().getConfigurationSearchRecordTypes());
    }


    /**
     *  Tests if a given configuration search record type is supported. 
     *
     *  @param  configurationSearchRecordType the configuration search record 
     *          type 
     *  @return <code> true </code> if the configuration search record type is 
     *          support <code> e </code> d, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          configurationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConfigurationSearchRecordType(org.osid.type.Type configurationSearchRecordType) {
        return (getAdapteeManager().supportsConfigurationSearchRecordType(configurationSearchRecordType));
    }


    /**
     *  Gets a configuration value retrieval session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueRetrievalSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueRetrieval() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueRetrievalSession getValueRetrievalSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueRetrievalSession(proxy));
    }


    /**
     *  Gets a configuration value retrieval session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueRetrievalSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueRetrieval() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> False </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueRetrievalSession getValueRetrievalSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueRetrievalSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets a configuration value lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueLookupSession(proxy));
    }


    /**
     *  Gets a configuration value lookup session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> False </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueLookupSession getValueLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueLookupSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets a configuration value query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuerySession getValueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueQuerySession(proxy));
    }


    /**
     *  Gets a configuration value query session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> False </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueQuerySession getValueQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueQuerySessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets a configuration value search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueSearchSession getValueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueSearchSession(proxy));
    }


    /**
     *  Gets a configuration value search session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> False </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueSearchSession getValueSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueSearchSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets a value notification session. 
     *
     *  @param  valueReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ValueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> valueReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueNotificationSession getValueNotificationSession(org.osid.configuration.ValueReceiver valueReceiver, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueNotificationSession(valueReceiver, proxy));
    }


    /**
     *  Gets a value notification session using the specified configuration 
     *
     *  @param  valueReceiver notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> valueReceiver, 
     *          configurationId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsValueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueNotificationSession getValueNotificationSessionForConfiguration(org.osid.configuration.ValueReceiver valueReceiver, 
                                                                                                       org.osid.id.Id configurationId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueNotificationSessionForConfiguration(valueReceiver, configurationId, proxy));
    }


    /**
     *  Gets a configuration value administration session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ValueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueAdminSession getValueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getValueAdminSession(proxy));
    }


    /**
     *  Gets a value administration session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ValueAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsValueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ValueAdminSession getValueAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getValueAdminSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets a parameter lookup session 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterLookupSession getParameterLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterLookupSession(proxy));
    }


    /**
     *  Gets a parameter lookup session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParamaterLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.configuration.ParameterLookupSession getParameterLookupSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterLookupSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets a parameter query session 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuerySession getParameterQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterQuerySession(proxy));
    }


    /**
     *  Gets a parameter query session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParamaterQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.configuration.ParameterQuerySession getParameterQuerySessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterQuerySessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets a parameter search session 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSearchSession getParameterSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterSearchSession(proxy));
    }


    /**
     *  Gets a parameter search session using the supplied configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParamaterSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSearchSession getParameterSearchSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterSearchSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets a parameter administration session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterAdminSession getParameterAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterAdminSession(proxy));
    }


    /**
     *  Gets a parameter administration session using the supplied 
     *  configuration. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configurationId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterAdminSession getParameterAdminSessionForConfiguration(org.osid.id.Id configurationId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterAdminSessionForConfiguration(configurationId, proxy));
    }


    /**
     *  Gets a parameter notification session. 
     *
     *  @param  parameterReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> parameterReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterNotificationSession getParameterNotificationSession(org.osid.configuration.ParameterReceiver parameterReceiver, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterNotificationSession(parameterReceiver, proxy));
    }


    /**
     *  Gets a parameter notification session using the specified 
     *  configuration. 
     *
     *  @param  parameterReceiver notification callback 
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> registryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> parameterReceiver, 
     *          configurationId, </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterNotificationSession getParameterNotificationSessionForConfiguration(org.osid.configuration.ParameterReceiver parameterReceiver, 
                                                                                                               org.osid.id.Id configurationId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterNotificationSessionForConfiguration(parameterReceiver, configurationId, proxy));
    }


    /**
     *  Gets a session for examining mappings of parameters to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterConfigurationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterConfiguration() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterConfigurationSession getParameterConfigurationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterConfigurationSession(proxy));
    }


    /**
     *  Gets a session for managing mappings of parameters to configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ParameterConfigurationAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterConfigurationAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterConfigurationAssignmentSession getParameterConfigurationAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterConfigurationAssignmentSession(proxy));
    }


    /**
     *  Gets a session for managing smart configurations of parameters. 
     *
     *  @param  configurationId the <code> Id </code> of the <code> 
     *          Configuration </code> to use 
     *  @param  proxy a proxy 
     *  @return a <code> ParameterSmartConfigurationSession </code> 
     *  @throws org.osid.NotFoundException <code> configurationId </code> is 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> configuratinId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsParameterSmartConfiguration() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ParameterSmartConfigurationSession getParameterSmartConfigurationSession(org.osid.id.Id configurationId, 
                                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getParameterSmartConfigurationSession(configurationId, proxy));
    }


    /**
     *  Gets a configuration lookup session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationLookupSession getConfigurationLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationLookupSession(proxy));
    }


    /**
     *  Gets a configuration query session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationQuerySession getConfigurationQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationQuerySession(proxy));
    }


    /**
     *  Gets a configuration search session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationSearchSession </code> 
     *  @throws org.osid.OperationFailedException <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.NullArgumentException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationSearchSession getConfigurationSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationSearchSession(proxy));
    }


    /**
     *  Gets the notification session for subscribing to changes to 
     *  configurations. 
     *
     *  @param  configurationReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> configurationReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationNotificationSession getConfigurationNotificationSession(org.osid.configuration.ConfigurationReceiver configurationReceiver, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationNotificationSession(configurationReceiver, proxy));
    }


    /**
     *  Gets a configuration administration session. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationAdminSession getConfigurationAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationAdminSession(proxy));
    }


    /**
     *  Gets a hierarchy traversal service for configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfiguraqtionHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationHierarchy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationHierarchySession getConfigurationHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationHierarchySession(proxy));
    }


    /**
     *  Gets a hierarchy design service for configurations. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ConfigurationHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.ConfigurationHierarchyDesignSession getConfigurationHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> ConfigurationProxyManager. </code> 
     *
     *  @return a <code> ConfigurationBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.batch.ConfigurationBatchProxyManager getConfigurationBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationBatchProxyManager());
    }


    /**
     *  Gets a <code> ConfigurationProxyManager. </code> 
     *
     *  @return a <code> ConfigurationRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConfigurationRules() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.configuration.rules.ConfigurationRulesProxyManager getConfigurationRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConfigurationRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
