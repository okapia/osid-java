//
// InvariantMapProxySignalLookupSession
//
//    Implements a Signal lookup service backed by a fixed
//    collection of signals. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.mapping.path;


/**
 *  Implements a Signal lookup service backed by a fixed
 *  collection of signals. The signals are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySignalLookupSession
    extends net.okapia.osid.jamocha.core.mapping.path.spi.AbstractMapSignalLookupSession
    implements org.osid.mapping.path.SignalLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySignalLookupSession} with no
     *  signals.
     *
     *  @param map the map
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySignalLookupSession(org.osid.mapping.Map map,
                                                  org.osid.proxy.Proxy proxy) {
        setMap(map);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxySignalLookupSession} with a single
     *  signal.
     *
     *  @param map the map
     *  @param signal a single signal
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signal} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySignalLookupSession(org.osid.mapping.Map map,
                                                  org.osid.mapping.path.Signal signal, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putSignal(signal);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySignalLookupSession} using
     *  an array of signals.
     *
     *  @param map the map
     *  @param signals an array of signals
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signals} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySignalLookupSession(org.osid.mapping.Map map,
                                                  org.osid.mapping.path.Signal[] signals, org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putSignals(signals);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySignalLookupSession} using a
     *  collection of signals.
     *
     *  @param map the map
     *  @param signals a collection of signals
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code map},
     *          {@code signals} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySignalLookupSession(org.osid.mapping.Map map,
                                                  java.util.Collection<? extends org.osid.mapping.path.Signal> signals,
                                                  org.osid.proxy.Proxy proxy) {

        this(map, proxy);
        putSignals(signals);
        return;
    }
}
