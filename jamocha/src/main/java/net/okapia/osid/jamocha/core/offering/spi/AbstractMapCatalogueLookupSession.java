//
// AbstractMapCatalogueLookupSession
//
//    A simple framework for providing a Catalogue lookup service
//    backed by a fixed collection of catalogues.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.offering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Catalogue lookup service backed by a
 *  fixed collection of catalogues. The catalogues are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Catalogues</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCatalogueLookupSession
    extends net.okapia.osid.jamocha.offering.spi.AbstractCatalogueLookupSession
    implements org.osid.offering.CatalogueLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.offering.Catalogue> catalogues = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.offering.Catalogue>());


    /**
     *  Makes a <code>Catalogue</code> available in this session.
     *
     *  @param  catalogue a catalogue
     *  @throws org.osid.NullArgumentException <code>catalogue<code>
     *          is <code>null</code>
     */

    protected void putCatalogue(org.osid.offering.Catalogue catalogue) {
        this.catalogues.put(catalogue.getId(), catalogue);
        return;
    }


    /**
     *  Makes an array of catalogues available in this session.
     *
     *  @param  catalogues an array of catalogues
     *  @throws org.osid.NullArgumentException <code>catalogues<code>
     *          is <code>null</code>
     */

    protected void putCatalogues(org.osid.offering.Catalogue[] catalogues) {
        putCatalogues(java.util.Arrays.asList(catalogues));
        return;
    }


    /**
     *  Makes a collection of catalogues available in this session.
     *
     *  @param  catalogues a collection of catalogues
     *  @throws org.osid.NullArgumentException <code>catalogues<code>
     *          is <code>null</code>
     */

    protected void putCatalogues(java.util.Collection<? extends org.osid.offering.Catalogue> catalogues) {
        for (org.osid.offering.Catalogue catalogue : catalogues) {
            this.catalogues.put(catalogue.getId(), catalogue);
        }

        return;
    }


    /**
     *  Removes a Catalogue from this session.
     *
     *  @param  catalogueId the <code>Id</code> of the catalogue
     *  @throws org.osid.NullArgumentException <code>catalogueId<code> is
     *          <code>null</code>
     */

    protected void removeCatalogue(org.osid.id.Id catalogueId) {
        this.catalogues.remove(catalogueId);
        return;
    }


    /**
     *  Gets the <code>Catalogue</code> specified by its <code>Id</code>.
     *
     *  @param  catalogueId <code>Id</code> of the <code>Catalogue</code>
     *  @return the catalogue
     *  @throws org.osid.NotFoundException <code>catalogueId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>catalogueId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.Catalogue getCatalogue(org.osid.id.Id catalogueId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.offering.Catalogue catalogue = this.catalogues.get(catalogueId);
        if (catalogue == null) {
            throw new org.osid.NotFoundException("catalogue not found: " + catalogueId);
        }

        return (catalogue);
    }


    /**
     *  Gets all <code>Catalogues</code>. In plenary mode, the returned
     *  list contains all known catalogues or an error
     *  results. Otherwise, the returned list may contain only those
     *  catalogues that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Catalogues</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.offering.CatalogueList getCatalogues()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.offering.catalogue.ArrayCatalogueList(this.catalogues.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.catalogues.clear();
        super.close();
        return;
    }
}
