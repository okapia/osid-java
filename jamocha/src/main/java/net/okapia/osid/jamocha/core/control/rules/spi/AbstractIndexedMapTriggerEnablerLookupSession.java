//
// AbstractIndexedMapTriggerEnablerLookupSession.java
//
//    A simple framework for providing a TriggerEnabler lookup service
//    backed by a fixed collection of trigger enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a TriggerEnabler lookup service backed by a
 *  fixed collection of trigger enablers. The trigger enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some trigger enablers may be compatible
 *  with more types than are indicated through these trigger enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>TriggerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapTriggerEnablerLookupSession
    extends AbstractMapTriggerEnablerLookupSession
    implements org.osid.control.rules.TriggerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.rules.TriggerEnabler> triggerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.rules.TriggerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.control.rules.TriggerEnabler> triggerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.rules.TriggerEnabler>());


    /**
     *  Makes a <code>TriggerEnabler</code> available in this session.
     *
     *  @param  triggerEnabler a trigger enabler
     *  @throws org.osid.NullArgumentException <code>triggerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putTriggerEnabler(org.osid.control.rules.TriggerEnabler triggerEnabler) {
        super.putTriggerEnabler(triggerEnabler);

        this.triggerEnablersByGenus.put(triggerEnabler.getGenusType(), triggerEnabler);
        
        try (org.osid.type.TypeList types = triggerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.triggerEnablersByRecord.put(types.getNextType(), triggerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of trigger enablers available in this session.
     *
     *  @param  triggerEnablers an array of trigger enablers
     *  @throws org.osid.NullArgumentException <code>triggerEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putTriggerEnablers(org.osid.control.rules.TriggerEnabler[] triggerEnablers) {
        for (org.osid.control.rules.TriggerEnabler triggerEnabler : triggerEnablers) {
            putTriggerEnabler(triggerEnabler);
        }

        return;
    }


    /**
     *  Makes a collection of trigger enablers available in this session.
     *
     *  @param  triggerEnablers a collection of trigger enablers
     *  @throws org.osid.NullArgumentException <code>triggerEnablers<code>
     *          is <code>null</code>
     */

    @Override
    protected void putTriggerEnablers(java.util.Collection<? extends org.osid.control.rules.TriggerEnabler> triggerEnablers) {
        for (org.osid.control.rules.TriggerEnabler triggerEnabler : triggerEnablers) {
            putTriggerEnabler(triggerEnabler);
        }

        return;
    }


    /**
     *  Removes a trigger enabler from this session.
     *
     *  @param triggerEnablerId the <code>Id</code> of the trigger enabler
     *  @throws org.osid.NullArgumentException <code>triggerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeTriggerEnabler(org.osid.id.Id triggerEnablerId) {
        org.osid.control.rules.TriggerEnabler triggerEnabler;
        try {
            triggerEnabler = getTriggerEnabler(triggerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.triggerEnablersByGenus.remove(triggerEnabler.getGenusType());

        try (org.osid.type.TypeList types = triggerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.triggerEnablersByRecord.remove(types.getNextType(), triggerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeTriggerEnabler(triggerEnablerId);
        return;
    }


    /**
     *  Gets a <code>TriggerEnablerList</code> corresponding to the given
     *  trigger enabler genus <code>Type</code> which does not include
     *  trigger enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known trigger enablers or an error results. Otherwise,
     *  the returned list may contain only those trigger enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  triggerEnablerGenusType a trigger enabler genus type 
     *  @return the returned <code>TriggerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByGenusType(org.osid.type.Type triggerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.triggerenabler.ArrayTriggerEnablerList(this.triggerEnablersByGenus.get(triggerEnablerGenusType)));
    }


    /**
     *  Gets a <code>TriggerEnablerList</code> containing the given
     *  trigger enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known trigger enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  trigger enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  triggerEnablerRecordType a trigger enabler record type 
     *  @return the returned <code>triggerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>triggerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.TriggerEnablerList getTriggerEnablersByRecordType(org.osid.type.Type triggerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.rules.triggerenabler.ArrayTriggerEnablerList(this.triggerEnablersByRecord.get(triggerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.triggerEnablersByGenus.clear();
        this.triggerEnablersByRecord.clear();

        super.close();

        return;
    }
}
