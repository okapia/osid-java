//
// AbstractImmutableAssessmentSection.java
//
//     Wraps a mutable AssessmentSection to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.assessmentsection.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>AssessmentSection</code> to hide modifiers. This
 *  wrapper provides an immutized AssessmentSection from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying assessmentSection whose state changes are visible.
 */

public abstract class AbstractImmutableAssessmentSection
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.assessment.AssessmentSection {

    private final org.osid.assessment.AssessmentSection assessmentSection;


    /**
     *  Constructs a new <code>AbstractImmutableAssessmentSection</code>.
     *
     *  @param assessmentSection the assessment section to immutablize
     *  @throws org.osid.NullArgumentException <code>assessmentSection</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAssessmentSection(org.osid.assessment.AssessmentSection assessmentSection) {
        super(assessmentSection);
        this.assessmentSection = assessmentSection;
        return;
    }


    /**
     *  Tests if this section must be completed within an allocated time. 
     *
     *  @return <code> true </code> if this section has an allocated time, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasAllocatedTime() {
        return (this.assessmentSection.hasAllocatedTime());
    }


    /**
     *  Gets the allocated time for this section. 
     *
     *  @return allocated time 
     *  @throws org.osid.IllegalStateException <code> hasAllocatedTime() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getAllocatedTime() {
        return (this.assessmentSection.getAllocatedTime());
    }


    /**
     *  Tests if the items or parts in this section are taken
     *  sequentially.
     *
     *  @return <code> true </code> if the items are taken
     *          sequentially, <code> false </code> if the items can be
     *          skipped and revisited
     */

    @OSID @Override
    public boolean areItemsSequential() {
        return (this.assessmentSection.areItemsSequential());
    }


    /**
     *  Tests if the items or parts appear in a random order. 
     *
     *  @return <code> true </code> if the items appear in a random order, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean areItemsShuffled() {
        return (this.assessmentSection.areItemsShuffled());
    }


    /**
     *  Gets the assessment section record corresponding to the given <code> 
     *  AssessmentSection </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> assessmentSectionRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(assessmentSectionRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  assessmentSectionRecordType an assessment section record type 
     *  @return the assessment section record 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentSectionRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(assessmentSectionRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentSectionRecord getAssessmentSectionRecord(org.osid.type.Type assessmentSectionRecordType)
        throws org.osid.OperationFailedException {

        return (this.assessmentSection.getAssessmentSectionRecord(assessmentSectionRecordType));
    }
}

