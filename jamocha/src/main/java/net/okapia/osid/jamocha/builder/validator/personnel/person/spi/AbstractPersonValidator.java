//
// AbstractPersonValidator.java
//
//     Validates a Person.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.personnel.person.spi;


/**
 *  Validates a Person.
 */

public abstract class AbstractPersonValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidObjectValidator {


    /**
     *  Constructs a new <code>AbstractPersonValidator</code>.
     */

    protected AbstractPersonValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractPersonValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractPersonValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a Person.
     *
     *  @param person a person to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>person</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.personnel.Person person) {
        super.validate(person);

        test(person.getSalutation(), "getSalutation()");
        test(person.getGivenName(), "getGivenName()");
        test(person.getPreferredName(), "getPreferredName()");
        test(person.getForenameAliases(), "getForenameAliases()");
        test(person.getMiddleNames(), "getMiddleNames()");
        test(person.getSurname(), "getSurname()");
        test(person.getSurnameAliases(), "getSurnameAliases()");
        test(person.getGenerationQualifier(), "getGenerationQualifier()");
        test(person.getQualificationSuffix(), "getQualificationSuffix()");
       
        testConditionalMethod(person, "getBirthDate", person.hasBirthDate(), "hasBirthDate");
        testConditionalMethod(person, "getDeathDate", person.isDeceased(), "isDeceased");

        test(person.getInstitutionalIdentifier(), "getInstitutionalIdentifier()");

        return;
    }
}
