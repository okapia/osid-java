//
// CourseMiter.java
//
//     Defines a Course miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.course;


/**
 *  Defines a <code>Course</code> miter for use with the builders.
 */

public interface CourseMiter
    extends net.okapia.osid.jamocha.builder.spi.OperableOsidObjectMiter,
            org.osid.course.Course {


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    public void setTitle(org.osid.locale.DisplayText title);


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     */

    public void setNumber(String number);


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public void addSponsor(org.osid.resource.Resource sponsor);


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors);


    /**
     *  Adds a credit smount.
     *
     *  @param credit a credit amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    public void addCreditAmount(org.osid.grading.Grade amount);


    /**
     *  Sets all the credit amount options.
     *
     *  @param amounts a collection of credit amounts
     *  @throws org.osid.NullArgumentException <code>amounts</code>
     *          is <code>null</code>
     */

    public void setCreditAmounts(java.util.Collection<org.osid.grading.Grade> amounts);


    /**
     *  Sets the prerequisites info.
     *
     *  @param info a prerequisites info
     *  @throws org.osid.NullArgumentException <code>info</code> is
     *          <code>null</code>
     */

    public void setPrerequisitesInfo(org.osid.locale.DisplayText info);


    /**
     *  Adds a prerequisite.
     *
     *  @param prerequisite a prerequisite
     *  @throws org.osid.NullArgumentException
     *          <code>prerequisite</code> is <code>null</code>
     */

    public void addPrerequisite(org.osid.course.requisite.Requisite prerequisite);


    /**
     *  Sets all the prerequisites.
     *
     *  @param prerequisites a collection of prerequisites
     *  @throws org.osid.NullArgumentException
     *          <code>prerequisites</code> is <code>null</code>
     */

    public void setPrerequisites(java.util.Collection<org.osid.course.requisite.Requisite> prerequisites);


    /**
     *  Adds a level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    public void addLevel(org.osid.grading.Grade level);


    /**
     *  Sets all the levels.
     *
     *  @param levels a collection of levels
     *  @throws org.osid.NullArgumentException <code>levels</code> is
     *          <code>null</code>
     */

    public void setLevels(java.util.Collection<org.osid.grading.Grade> levels);


    /**
     *  Adds a grading option.
     *
     *  @param option a grading option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public void addGradingOption(org.osid.grading.GradeSystem option);


    /**
     *  Sets all the grading options.
     *
     *  @param options a collection of grading options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public void setGradingOptions(java.util.Collection<org.osid.grading.GradeSystem> options);


    /**
     *  Adds a learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException <code>objective</code>
     *          is <code>null</code>
     */

    public void addLearningObjective(org.osid.learning.Objective objective);


    /**
     *  Sets all the learning objectives.
     *
     *  @param objectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException <code>objectives</code>
     *          is <code>null</code>
     */

    public void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> objectives);


    /**
     *  Adds a Course record.
     *
     *  @param record a course record
     *  @param recordType the type of course record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addCourseRecord(org.osid.course.records.CourseRecord record, org.osid.type.Type recordType);
}       


