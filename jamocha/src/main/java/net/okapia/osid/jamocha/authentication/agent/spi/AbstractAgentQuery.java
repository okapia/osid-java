//
// AbstractAgentQuery.java
//
//     A template for making an Agent Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authentication.agent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for agents.
 */

public abstract class AbstractAgentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.authentication.AgentQuery {

    private final java.util.Collection<org.osid.authentication.records.AgentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the resource <code> Id </code> for this query. 
     *
     *  @param  agencyId a resource <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchResourceId(org.osid.id.Id agencyId, boolean match) {
        return;
    }


    /**
     *  Clears the resource <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearResourceIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceQuery() {
        return (false);
    }


    /**
     *  Gets the query for a resource. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the resource query 
     *  @throws org.osid.UnimplementedException <code> supportsResourceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getResourceQuery() {
        throw new org.osid.UnimplementedException("supportsResourceQuery() is false");
    }


    /**
     *  Matches agents with any resource. 
     *
     *  @param  match <code> true </code> if to match agents with a resource, 
     *          <code> false </code> to match agents with no resource 
     */

    @OSID @Override
    public void matchAnyResource(boolean match) {
        return;
    }


    /**
     *  Clears the resource terms. 
     */

    @OSID @Override
    public void clearResourceTerms() {
        return;
    }


    /**
     *  Sets the agency <code> Id </code> for this query. 
     *
     *  @param  agencyId an agency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgencyId(org.osid.id.Id agencyId, boolean match) {
        return;
    }


    /**
     *  Clears the agency <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgencyIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AgencyQuery </code> is available. 
     *
     *  @return <code> true </code> if an agency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agency query 
     *  @throws org.osid.UnimplementedException <code> supportsAgencyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgencyQuery getAgencyQuery() {
        throw new org.osid.UnimplementedException("supportsAgencyQuery() is false");
    }


    /**
     *  Clears the agency terms. 
     */

    @OSID @Override
    public void clearAgencyTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given agent query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an agent implementing the requested record.
     *
     *  @param agentRecordType an agent record type
     *  @return the agent query record
     *  @throws org.osid.NullArgumentException
     *          <code>agentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgentQueryRecord getAgentQueryRecord(org.osid.type.Type agentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgentQueryRecord record : this.records) {
            if (record.implementsRecordType(agentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this agent query. 
     *
     *  @param agentQueryRecord agent query record
     *  @param agentRecordType agent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAgentQueryRecord(org.osid.authentication.records.AgentQueryRecord agentQueryRecord, 
                                          org.osid.type.Type agentRecordType) {

        addRecordType(agentRecordType);
        nullarg(agentQueryRecord, "agent query record");
        this.records.add(agentQueryRecord);        
        return;
    }
}
