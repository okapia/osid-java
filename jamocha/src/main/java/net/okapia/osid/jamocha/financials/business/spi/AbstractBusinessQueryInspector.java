//
// AbstractBusinessQueryInspector.java
//
//     A template for making a BusinessQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.business.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for businesses.
 */

public abstract class AbstractBusinessQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.financials.BusinessQueryInspector {

    private final java.util.Collection<org.osid.financials.records.BusinessQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the account <code> Id </code> query terms. 
     *
     *  @return the account <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAccountIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the account query terms. 
     *
     *  @return the account query terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.financials.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.financials.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the fiscal period <code> Id </code> query terms. 
     *
     *  @return the fiscal period <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFiscalPeriodIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the fiscal period query terms. 
     *
     *  @return the fiscal period query terms 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriodQueryInspector[] getFiscalPeriodTerms() {
        return (new org.osid.financials.FiscalPeriodQueryInspector[0]);
    }


    /**
     *  Gets the ancestor business <code> Id </code> query terms. 
     *
     *  @return the ancestor business <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor business query terms. 
     *
     *  @return the ancestor business terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getAncestorBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }


    /**
     *  Gets the descendant business <code> Id </code> query terms. 
     *
     *  @return the descendant business <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant business query terms. 
     *
     *  @return the descendant business terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getDescendantBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given business query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a business implementing the requested record.
     *
     *  @param businessRecordType a business record type
     *  @return the business query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>businessRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(businessRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.records.BusinessQueryInspectorRecord getBusinessQueryInspectorRecord(org.osid.type.Type businessRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.records.BusinessQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(businessRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(businessRecordType + " is not supported");
    }


    /**
     *  Adds a record to this business query. 
     *
     *  @param businessQueryInspectorRecord business query inspector
     *         record
     *  @param businessRecordType business record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBusinessQueryInspectorRecord(org.osid.financials.records.BusinessQueryInspectorRecord businessQueryInspectorRecord, 
                                                   org.osid.type.Type businessRecordType) {

        addRecordType(businessRecordType);
        nullarg(businessRecordType, "business record type");
        this.records.add(businessQueryInspectorRecord);        
        return;
    }
}
