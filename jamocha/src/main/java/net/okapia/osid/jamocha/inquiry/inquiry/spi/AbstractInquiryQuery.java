//
// AbstractInquiryQuery.java
//
//     A template for making an Inquiry Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.inquiry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for inquiries.
 */

public abstract class AbstractInquiryQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRuleQuery
    implements org.osid.inquiry.InquiryQuery {

    private final java.util.Collection<org.osid.inquiry.records.InquiryQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the audit <code> Id </code> for this query. 
     *
     *  @param  auditId the audit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auditId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAuditId(org.osid.id.Id auditId, boolean match) {
        return;
    }


    /**
     *  Clears the audit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuditIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuditQuery </code> is available. 
     *
     *  @return <code> true </code> if an audit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an <code> Audit, </code> Multiple retrievals 
     *  produce a nested <code> OR </code> term. 
     *
     *  @return the audit query 
     *  @throws org.osid.UnimplementedException <code> supportsAuditQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuery getAuditQuery() {
        throw new org.osid.UnimplementedException("supportsAuditQuery() is false");
    }


    /**
     *  Clears the audit query terms. 
     */

    @OSID @Override
    public void clearAuditTerms() {
        return;
    }


    /**
     *  Sets the question for this query. 
     *
     *  @param  question the question 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> question </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchQuestion(String question, 
                              org.osid.type.Type stringMatchType, 
                              boolean match) {
        return;
    }


    /**
     *  Matches inquiries with any question. 
     *
     *  @param  match <code> true </code> to match inquiries with a question, 
     *          <code> false </code> to match inquiries with no question 
     */

    @OSID @Override
    public void matchAnyQuestion(boolean match) {
        return;
    }


    /**
     *  Clears the question query terms. 
     */

    @OSID @Override
    public void clearQuestionTerms() {
        return;
    }


    /**
     *  Matches inquiries that are required. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRequired(boolean match) {
        return;
    }


    /**
     *  Matches inquiries with any required flag set. 
     *
     *  @param  match <code> true </code> to match inquiries with a required 
     *          flag set, <code> false </code> to match inquiries with no 
     *          required flag set 
     */

    @OSID @Override
    public void matchAnyRequired(boolean match) {
        return;
    }


    /**
     *  Clears the required query terms. 
     */

    @OSID @Override
    public void clearRequiredTerms() {
        return;
    }


    /**
     *  Matches inquiries that require a positive response. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchAffirmationRequired(boolean match) {
        return;
    }


    /**
     *  Matches inquiries with any affirmation required flag set. 
     *
     *  @param  match <code> true </code> to match inquiries with an 
     *          affirmation required flag set, <code> false </code> to match 
     *          inquiries with no affirmation required flag set 
     */

    @OSID @Override
    public void matchAnyAffirmationRequired(boolean match) {
        return;
    }


    /**
     *  Clears the affirmation required query terms. 
     */

    @OSID @Override
    public void clearAffirmationRequiredTerms() {
        return;
    }


    /**
     *  Matches inquiries that require a single response. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchNeedsOneResponse(boolean match) {
        return;
    }


    /**
     *  Matches inquiries with any needs one response set. 
     *
     *  @param  match <code> true </code> to match inquiries with a need one 
     *          response flag set, <code> false </code> to match inquiries 
     *          with no needs one response flag set 
     */

    @OSID @Override
    public void matchAnyNeedsOneResponse(boolean match) {
        return;
    }


    /**
     *  Clears the needs one response query terms. 
     */

    @OSID @Override
    public void clearNeedsOneResponseTerms() {
        return;
    }


    /**
     *  Sets the inquest <code> Id </code> for this query to match inquiries 
     *  assigned to inquests. 
     *
     *  @param  inquestId the inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquestId(org.osid.id.Id inquestId, boolean match) {
        return;
    }


    /**
     *  Clears the inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquestIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getInquestQuery() {
        throw new org.osid.UnimplementedException("supportsInquestQuery() is false");
    }


    /**
     *  Clears the inquest query terms. 
     */

    @OSID @Override
    public void clearInquestTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given inquiry query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an inquiry implementing the requested record.
     *
     *  @param inquiryRecordType an inquiry record type
     *  @return the inquiry query record
     *  @throws org.osid.NullArgumentException
     *          <code>inquiryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(inquiryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.records.InquiryQueryRecord getInquiryQueryRecord(org.osid.type.Type inquiryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.records.InquiryQueryRecord record : this.records) {
            if (record.implementsRecordType(inquiryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(inquiryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this inquiry query. 
     *
     *  @param inquiryQueryRecord inquiry query record
     *  @param inquiryRecordType inquiry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInquiryQueryRecord(org.osid.inquiry.records.InquiryQueryRecord inquiryQueryRecord, 
                                          org.osid.type.Type inquiryRecordType) {

        addRecordType(inquiryRecordType);
        nullarg(inquiryQueryRecord, "inquiry query record");
        this.records.add(inquiryQueryRecord);        
        return;
    }
}
