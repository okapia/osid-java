//
// AbstractIndexedMapAuctionConstrainerLookupSession.java
//
//    A simple framework for providing an AuctionConstrainer lookup service
//    backed by a fixed collection of auction constrainers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an AuctionConstrainer lookup service backed by a
 *  fixed collection of auction constrainers. The auction constrainers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some auction constrainers may be compatible
 *  with more types than are indicated through these auction constrainer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>AuctionConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapAuctionConstrainerLookupSession
    extends AbstractMapAuctionConstrainerLookupSession
    implements org.osid.bidding.rules.AuctionConstrainerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.bidding.rules.AuctionConstrainer> auctionConstrainersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.rules.AuctionConstrainer>());
    private final MultiMap<org.osid.type.Type, org.osid.bidding.rules.AuctionConstrainer> auctionConstrainersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.bidding.rules.AuctionConstrainer>());


    /**
     *  Makes an <code>AuctionConstrainer</code> available in this session.
     *
     *  @param  auctionConstrainer an auction constrainer
     *  @throws org.osid.NullArgumentException <code>auctionConstrainer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putAuctionConstrainer(org.osid.bidding.rules.AuctionConstrainer auctionConstrainer) {
        super.putAuctionConstrainer(auctionConstrainer);

        this.auctionConstrainersByGenus.put(auctionConstrainer.getGenusType(), auctionConstrainer);
        
        try (org.osid.type.TypeList types = auctionConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionConstrainersByRecord.put(types.getNextType(), auctionConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an auction constrainer from this session.
     *
     *  @param auctionConstrainerId the <code>Id</code> of the auction constrainer
     *  @throws org.osid.NullArgumentException <code>auctionConstrainerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeAuctionConstrainer(org.osid.id.Id auctionConstrainerId) {
        org.osid.bidding.rules.AuctionConstrainer auctionConstrainer;
        try {
            auctionConstrainer = getAuctionConstrainer(auctionConstrainerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.auctionConstrainersByGenus.remove(auctionConstrainer.getGenusType());

        try (org.osid.type.TypeList types = auctionConstrainer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.auctionConstrainersByRecord.remove(types.getNextType(), auctionConstrainer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeAuctionConstrainer(auctionConstrainerId);
        return;
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> corresponding to the given
     *  auction constrainer genus <code>Type</code> which does not include
     *  auction constrainers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known auction constrainers or an error results. Otherwise,
     *  the returned list may contain only those auction constrainers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  auctionConstrainerGenusType an auction constrainer genus type 
     *  @return the returned <code>AuctionConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByGenusType(org.osid.type.Type auctionConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionconstrainer.ArrayAuctionConstrainerList(this.auctionConstrainersByGenus.get(auctionConstrainerGenusType)));
    }


    /**
     *  Gets an <code>AuctionConstrainerList</code> containing the given
     *  auction constrainer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known auction constrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  auction constrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  auctionConstrainerRecordType an auction constrainer record type 
     *  @return the returned <code>auctionConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerList getAuctionConstrainersByRecordType(org.osid.type.Type auctionConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.bidding.rules.auctionconstrainer.ArrayAuctionConstrainerList(this.auctionConstrainersByRecord.get(auctionConstrainerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionConstrainersByGenus.clear();
        this.auctionConstrainersByRecord.clear();

        super.close();

        return;
    }
}
