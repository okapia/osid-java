//
// AbstractSystemQueryInspector.java
//
//     A template for making a SystemQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.system.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for systems.
 */

public abstract class AbstractSystemQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.control.SystemQueryInspector {

    private final java.util.Collection<org.osid.control.records.SystemQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the device <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDeviceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the device query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.DeviceQueryInspector[] getDeviceTerms() {
        return (new org.osid.control.DeviceQueryInspector[0]);
    }


    /**
     *  Gets the controller <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getControllerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the controller query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ControllerQueryInspector[] getControllerTerms() {
        return (new org.osid.control.ControllerQueryInspector[0]);
    }


    /**
     *  Gets the input <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInputIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the input query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.InputQueryInspector[] getInputTerms() {
        return (new org.osid.control.InputQueryInspector[0]);
    }


    /**
     *  Gets the setting <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSettingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the setting query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SettingQueryInspector[] getSettingTerms() {
        return (new org.osid.control.SettingQueryInspector[0]);
    }


    /**
     *  Gets the scene <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSceneIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the scene query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SceneQueryInspector[] getSceneTerms() {
        return (new org.osid.control.SceneQueryInspector[0]);
    }


    /**
     *  Gets the trigger <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getTriggerIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the trigger query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.TriggerQueryInspector[] getTriggerTerms() {
        return (new org.osid.control.TriggerQueryInspector[0]);
    }


    /**
     *  Gets the action group <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActionGroupIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the action group query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQueryInspector[] getActionGroupTerms() {
        return (new org.osid.control.ActionGroupQueryInspector[0]);
    }


    /**
     *  Gets the ancestor system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getAncestorSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the descendant system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getDescendantSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given system query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a system implementing the requested record.
     *
     *  @param systemRecordType a system record type
     *  @return the system query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>systemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(systemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SystemQueryInspectorRecord getSystemQueryInspectorRecord(org.osid.type.Type systemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SystemQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(systemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(systemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this system query. 
     *
     *  @param systemQueryInspectorRecord system query inspector
     *         record
     *  @param systemRecordType system record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addSystemQueryInspectorRecord(org.osid.control.records.SystemQueryInspectorRecord systemQueryInspectorRecord, 
                                                   org.osid.type.Type systemRecordType) {

        addRecordType(systemRecordType);
        nullarg(systemRecordType, "system record type");
        this.records.add(systemQueryInspectorRecord);        
        return;
    }
}
