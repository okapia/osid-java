//
// AbstractImmutableOsidCompendium
//
//     Defines an immutable wrapper for an OsidCompendium.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an immutable wrapper for an OsidCompendium.
 */

public abstract class AbstractImmutableOsidCompendium
    extends AbstractImmutableOsidObject
    implements org.osid.OsidCompendium {

    private final org.osid.OsidCompendium compendium;


    /**
     *  Constructs a new <code>AbstractImmutableOsidCompendium</code>.
     *
     *  @param compendium
     *  @throws org.osid.NullArgumentException <code>compendium</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableOsidCompendium(org.osid.OsidCompendium compendium) {
        super(compendium);
        this.compendium = compendium;
        return;
    }


    /**
     *  Gets the start date used in the evaluation of the
     *  transactional data on which this report is based.
     *
     *  @return the date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.compendium.getStartDate());
    }


    /**
     *  Gets the end date used in the evaluation of the transactional
     *  data on which this report is based.
     *
     *  @return the date
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.compendium.getEndDate());
    }


    /**
     *  Tests if this report is interpolated within measured data or
     *  known transactions. Interpolation may occur if the start or
     *  end date fall between two known facts or managed time period.
     *
     *  @return <code> true </code> if this report is interpolated,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isInterpolated() {
        return (this.compendium.isInterpolated());
    }


    /**
     *  Tests if this report is extrapolated outside measured data or
     *  known transactions. Extrapolation may occur if the start or
     *  end date fall outside two known facts or managed time
     *  period. Extrapolation may occur within a managed time period
     *  in progress where the results of the entire time period are
     *  projected.
     *
     *  @return <code> true </code> if this report is extrapolated,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isExtrapolated() {
        return (this.compendium.isExtrapolated());
    }
}
