//
// AbstractQueueProcessorQueryInspector.java
//
//     A template for making a QueueProcessorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for queue processors.
 */

public abstract class AbstractQueueProcessorQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQueryInspector
    implements org.osid.provisioning.rules.QueueProcessorQueryInspector {

    private final java.util.Collection<org.osid.provisioning.rules.records.QueueProcessorQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the automatic query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getAutomaticTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the fifo query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getFifoTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the removes processed queue entries terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getRemovesProcessedQueueEntriesTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledQueueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.QueueQueryInspector[] getRuledQueueTerms() {
        return (new org.osid.provisioning.QueueQueryInspector[0]);
    }


    /**
     *  Gets the distributor <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDistributorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the distributor query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorQueryInspector[] getDistributorTerms() {
        return (new org.osid.provisioning.DistributorQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given queue processor query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a queue processor implementing the requested record.
     *
     *  @param queueProcessorRecordType a queue processor record type
     *  @return the queue processor query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(queueProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueProcessorQueryInspectorRecord getQueueProcessorQueryInspectorRecord(org.osid.type.Type queueProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.QueueProcessorQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(queueProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this queue processor query. 
     *
     *  @param queueProcessorQueryInspectorRecord queue processor query inspector
     *         record
     *  @param queueProcessorRecordType queueProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addQueueProcessorQueryInspectorRecord(org.osid.provisioning.rules.records.QueueProcessorQueryInspectorRecord queueProcessorQueryInspectorRecord, 
                                                   org.osid.type.Type queueProcessorRecordType) {

        addRecordType(queueProcessorRecordType);
        nullarg(queueProcessorRecordType, "queue processor record type");
        this.records.add(queueProcessorQueryInspectorRecord);        
        return;
    }
}
