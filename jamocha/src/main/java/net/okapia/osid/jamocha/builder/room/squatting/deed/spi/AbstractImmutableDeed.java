//
// AbstractImmutableDeed.java
//
//     Wraps a mutable Deed to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.squatting.deed.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Deed</code> to hide modifiers. This
 *  wrapper provides an immutized Deed from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying deed whose state changes are visible.
 */

public abstract class AbstractImmutableDeed
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.room.squatting.Deed {

    private final org.osid.room.squatting.Deed deed;


    /**
     *  Constructs a new <code>AbstractImmutableDeed</code>.
     *
     *  @param deed the deed to immutablize
     *  @throws org.osid.NullArgumentException <code>deed</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableDeed(org.osid.room.squatting.Deed deed) {
        super(deed);
        this.deed = deed;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the owner building. 
     *
     *  @return the building <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBuildingId() {
        return (this.deed.getBuildingId());
    }


    /**
     *  Gets the owner building. 
     *
     *  @return the building 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding()
        throws org.osid.OperationFailedException {

        return (this.deed.getBuilding());
    }


    /**
     *  Gets the <code> Id </code> of the owner resource. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOwnerId() {
        return (this.deed.getOwnerId());
    }


    /**
     *  Gets the owner resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getOwner()
        throws org.osid.OperationFailedException {

        return (this.deed.getOwner());
    }


    /**
     *  Gets the deed record corresponding to the given <code> Deed </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> DeedRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(deedRecordType) </code> is <code> true </code> . 
     *
     *  @param  deedRecordType the type of deed record to retrieve 
     *  @return the deed record 
     *  @throws org.osid.NullArgumentException <code> deedRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(deedRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.squatting.records.DeedRecord getDeedRecord(org.osid.type.Type deedRecordType)
        throws org.osid.OperationFailedException {

        return (this.deed.getDeedRecord(deedRecordType));
    }
}

