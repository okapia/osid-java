//
// MutableIndexedMapProcessLookupSession
//
//    Implements a Process lookup service backed by a collection of
//    processes indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow;


/**
 *  Implements a Process lookup service backed by a collection of
 *  processes. The processes are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some processes may be compatible
 *  with more types than are indicated through these process
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of processes can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProcessLookupSession
    extends net.okapia.osid.jamocha.core.workflow.spi.AbstractIndexedMapProcessLookupSession
    implements org.osid.workflow.ProcessLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapProcessLookupSession} with no processes.
     *
     *  @param office the office
     *  @throws org.osid.NullArgumentException {@code office}
     *          is {@code null}
     */

      public MutableIndexedMapProcessLookupSession(org.osid.workflow.Office office) {
        setOffice(office);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProcessLookupSession} with a
     *  single process.
     *  
     *  @param office the office
     *  @param  process a single process
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code process} is {@code null}
     */

    public MutableIndexedMapProcessLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.Process process) {
        this(office);
        putProcess(process);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProcessLookupSession} using an
     *  array of processes.
     *
     *  @param office the office
     *  @param  processes an array of processes
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code processes} is {@code null}
     */

    public MutableIndexedMapProcessLookupSession(org.osid.workflow.Office office,
                                                  org.osid.workflow.Process[] processes) {
        this(office);
        putProcesses(processes);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProcessLookupSession} using a
     *  collection of processes.
     *
     *  @param office the office
     *  @param  processes a collection of processes
     *  @throws org.osid.NullArgumentException {@code office} or
     *          {@code processes} is {@code null}
     */

    public MutableIndexedMapProcessLookupSession(org.osid.workflow.Office office,
                                                  java.util.Collection<? extends org.osid.workflow.Process> processes) {

        this(office);
        putProcesses(processes);
        return;
    }
    

    /**
     *  Makes a {@code Process} available in this session.
     *
     *  @param  process a process
     *  @throws org.osid.NullArgumentException {@code process{@code  is
     *          {@code null}
     */

    @Override
    public void putProcess(org.osid.workflow.Process process) {
        super.putProcess(process);
        return;
    }


    /**
     *  Makes an array of processes available in this session.
     *
     *  @param  processes an array of processes
     *  @throws org.osid.NullArgumentException {@code processes{@code 
     *          is {@code null}
     */

    @Override
    public void putProcesses(org.osid.workflow.Process[] processes) {
        super.putProcesses(processes);
        return;
    }


    /**
     *  Makes collection of processes available in this session.
     *
     *  @param  processes a collection of processes
     *  @throws org.osid.NullArgumentException {@code process{@code  is
     *          {@code null}
     */

    @Override
    public void putProcesses(java.util.Collection<? extends org.osid.workflow.Process> processes) {
        super.putProcesses(processes);
        return;
    }


    /**
     *  Removes a Process from this session.
     *
     *  @param processId the {@code Id} of the process
     *  @throws org.osid.NullArgumentException {@code processId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProcess(org.osid.id.Id processId) {
        super.removeProcess(processId);
        return;
    }    
}
