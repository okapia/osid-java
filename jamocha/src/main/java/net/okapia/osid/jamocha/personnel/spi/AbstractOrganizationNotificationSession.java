//
// AbstractOrganizationNotificationSession.java
//
//     A template for making OrganizationNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Organization} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Organization} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for organization entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractOrganizationNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.personnel.OrganizationNotificationSession {

    private boolean federated = false;
    private org.osid.personnel.Realm realm = new net.okapia.osid.jamocha.nil.personnel.realm.UnknownRealm();


    /**
     *  Gets the {@code Realm/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Realm Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getRealmId() {
        return (this.realm.getId());
    }

    
    /**
     *  Gets the {@code Realm} associated with this session.
     *
     *  @return the {@code Realm} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.personnel.Realm getRealm()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.realm);
    }


    /**
     *  Sets the {@code Realm}.
     *
     *  @param realm the realm for this session
     *  @throws org.osid.NullArgumentException {@code realm}
     *          is {@code null}
     */

    protected void setRealm(org.osid.personnel.Realm realm) {
        nullarg(realm, "realm");
        this.realm = realm;
        return;
    }


    /**
     *  Tests if this user can register for {@code Organization}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForOrganizationNotifications() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include organizations in realms which are children
     *  of this realm in the realm hierarchy.
     */

    @OSID @Override
    public void useFederatedRealmView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this realm only.
     */

    @OSID @Override
    public void useIsolatedRealmView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new organizations. {@code
     *  OrganizationReceiver.newOrganization()} is invoked when an new
     *  {@code Organization} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewOrganizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new ancestor of the specified
     *  organization. {@code
     *  OrganizationReceiver.newAncestorOrganization()} is invoked
     *  when the specified organization node gets a new ancestor.
     *
     *  @param organizationId the {@code Id} of the
     *         {@code Organization} node to monitor
     *  @throws org.osid.NullArgumentException {@code organizationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewOrganizationAncestors(org.osid.id.Id organizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that introduces a new descendant of the specified
     *  organization. {@code
     *  OrganizationReceiver.newDescendantOrganization()} is invoked
     *  when the specified organization node gets a new descendant.
     *
     *  @param organizationId the {@code Id} of the
     *         {@code Organization} node to monitor
     *  @throws org.osid.NullArgumentException {@code organizationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewOrganizationDescendants(org.osid.id.Id organizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated organizations. {@code
     *  OrganizationReceiver.changedOrganization()} is invoked when an
     *  organization is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedOrganizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated organization. {@code
     *  OrganizationReceiver.changedOrganization()} is invoked when
     *  the specified organization is changed.
     *
     *  @param organizationId the {@code Id} of the {@code Organization} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code organizationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedOrganization(org.osid.id.Id organizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted organizations. {@code
     *  OrganizationReceiver.deletedOrganization()} is invoked when an
     *  organization is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedOrganizations()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted organization. {@code
     *  OrganizationReceiver.deletedOrganization()} is invoked when
     *  the specified organization is deleted.
     *
     *  @param organizationId the {@code Id} of the
     *          {@code Organization} to monitor
     *  @throws org.osid.NullArgumentException {@code organizationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedOrganization(org.osid.id.Id organizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes an ancestor of the specified organization. {@code
     *  OrganizationReceiver.deletedAncestor()} is invoked when the
     *  specified organization node loses an ancestor.
     *
     *  @param organizationId the {@code Id} of the
     *         {@code Organization} node to monitor
     *  @throws org.osid.NullArgumentException {@code organizationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedOrganizationAncestors(org.osid.id.Id organizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy structure
     *  that removes a descendant of the specified
     *  organization. {@code OrganizationReceiver.deletedDescendant()}
     *  is invoked when the specified organization node loses a
     *  descendant.
     *
     *  @param organizationId the {@code Id} of the
     *          {@code Organization} node to monitor
     *  @throws org.osid.NullArgumentException {@code organizationId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedOrganizationDescendants(org.osid.id.Id organizationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
