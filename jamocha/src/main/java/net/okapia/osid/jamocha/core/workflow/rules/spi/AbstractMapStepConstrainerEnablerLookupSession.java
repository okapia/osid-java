//
// AbstractMapStepConstrainerEnablerLookupSession
//
//    A simple framework for providing a StepConstrainerEnabler lookup service
//    backed by a fixed collection of step constrainer enablers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a StepConstrainerEnabler lookup service backed by a
 *  fixed collection of step constrainer enablers. The step constrainer enablers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>StepConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapStepConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.workflow.rules.spi.AbstractStepConstrainerEnablerLookupSession
    implements org.osid.workflow.rules.StepConstrainerEnablerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.workflow.rules.StepConstrainerEnabler> stepConstrainerEnablers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.workflow.rules.StepConstrainerEnabler>());


    /**
     *  Makes a <code>StepConstrainerEnabler</code> available in this session.
     *
     *  @param  stepConstrainerEnabler a step constrainer enabler
     *  @throws org.osid.NullArgumentException <code>stepConstrainerEnabler<code>
     *          is <code>null</code>
     */

    protected void putStepConstrainerEnabler(org.osid.workflow.rules.StepConstrainerEnabler stepConstrainerEnabler) {
        this.stepConstrainerEnablers.put(stepConstrainerEnabler.getId(), stepConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of step constrainer enablers available in this session.
     *
     *  @param  stepConstrainerEnablers an array of step constrainer enablers
     *  @throws org.osid.NullArgumentException <code>stepConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putStepConstrainerEnablers(org.osid.workflow.rules.StepConstrainerEnabler[] stepConstrainerEnablers) {
        putStepConstrainerEnablers(java.util.Arrays.asList(stepConstrainerEnablers));
        return;
    }


    /**
     *  Makes a collection of step constrainer enablers available in this session.
     *
     *  @param  stepConstrainerEnablers a collection of step constrainer enablers
     *  @throws org.osid.NullArgumentException <code>stepConstrainerEnablers<code>
     *          is <code>null</code>
     */

    protected void putStepConstrainerEnablers(java.util.Collection<? extends org.osid.workflow.rules.StepConstrainerEnabler> stepConstrainerEnablers) {
        for (org.osid.workflow.rules.StepConstrainerEnabler stepConstrainerEnabler : stepConstrainerEnablers) {
            this.stepConstrainerEnablers.put(stepConstrainerEnabler.getId(), stepConstrainerEnabler);
        }

        return;
    }


    /**
     *  Removes a StepConstrainerEnabler from this session.
     *
     *  @param  stepConstrainerEnablerId the <code>Id</code> of the step constrainer enabler
     *  @throws org.osid.NullArgumentException <code>stepConstrainerEnablerId<code> is
     *          <code>null</code>
     */

    protected void removeStepConstrainerEnabler(org.osid.id.Id stepConstrainerEnablerId) {
        this.stepConstrainerEnablers.remove(stepConstrainerEnablerId);
        return;
    }


    /**
     *  Gets the <code>StepConstrainerEnabler</code> specified by its <code>Id</code>.
     *
     *  @param  stepConstrainerEnablerId <code>Id</code> of the <code>StepConstrainerEnabler</code>
     *  @return the stepConstrainerEnabler
     *  @throws org.osid.NotFoundException <code>stepConstrainerEnablerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>stepConstrainerEnablerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnabler getStepConstrainerEnabler(org.osid.id.Id stepConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.workflow.rules.StepConstrainerEnabler stepConstrainerEnabler = this.stepConstrainerEnablers.get(stepConstrainerEnablerId);
        if (stepConstrainerEnabler == null) {
            throw new org.osid.NotFoundException("stepConstrainerEnabler not found: " + stepConstrainerEnablerId);
        }

        return (stepConstrainerEnabler);
    }


    /**
     *  Gets all <code>StepConstrainerEnablers</code>. In plenary mode, the returned
     *  list contains all known stepConstrainerEnablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  stepConstrainerEnablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>StepConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.rules.stepconstrainerenabler.ArrayStepConstrainerEnablerList(this.stepConstrainerEnablers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.stepConstrainerEnablers.clear();
        super.close();
        return;
    }
}
