//
// DemographicElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.demographic.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class DemographicElements
    extends net.okapia.osid.jamocha.spi.OsidRuleElements {


    /**
     *  Gets the DemographicElement Id.
     *
     *  @return the demographic element Id
     */

    public static org.osid.id.Id getDemographicEntityId() {
        return (makeEntityId("osid.resource.demographic.Demographic"));
    }


    /**
     *  Gets the IncludedDemographicIds element Id.
     *
     *  @return the IncludedDemographicIds element Id
     */

    public static org.osid.id.Id getIncludedDemographicIds() {
        return (makeElementId("osid.resource.demographic.demographic.IncludedDemographicIds"));
    }


    /**
     *  Gets the IncludedDemographics element Id.
     *
     *  @return the IncludedDemographics element Id
     */

    public static org.osid.id.Id getIncludedDemographics() {
        return (makeElementId("osid.resource.demographic.demographic.IncludedDemographics"));
    }


    /**
     *  Gets the IncludedIntersectingDemographicIds element Id.
     *
     *  @return the IncludedIntersectingDemographicIds element Id
     */

    public static org.osid.id.Id getIncludedIntersectingDemographicIds() {
        return (makeElementId("osid.resource.demographic.demographic.IncludedIntersectingDemographicIds"));
    }


    /**
     *  Gets the IncludedIntersectingDemographics element Id.
     *
     *  @return the IncludedIntersectingDemographics element Id
     */

    public static org.osid.id.Id getIncludedIntersectingDemographics() {
        return (makeElementId("osid.resource.demographic.demographic.IncludedIntersectingDemographics"));
    }


    /**
     *  Gets the IncludedExclusiveDemographicIds element Id.
     *
     *  @return the IncludedExclusiveDemographicIds element Id
     */

    public static org.osid.id.Id getIncludedExclusiveDemographicIds() {
        return (makeElementId("osid.resource.demographic.demographic.IncludedExclusiveDemographicIds"));
    }


    /**
     *  Gets the IncludedExclusiveDemographics element Id.
     *
     *  @return the IncludedExclusiveDemographics element Id
     */

    public static org.osid.id.Id getIncludedExclusiveDemographics() {
        return (makeElementId("osid.resource.demographic.demographic.IncludedExclusiveDemographics"));
    }


    /**
     *  Gets the ExcludedDemographicIds element Id.
     *
     *  @return the ExcludedDemographicIds element Id
     */

    public static org.osid.id.Id getExcludedDemographicIds() {
        return (makeElementId("osid.resource.demographic.demographic.ExcludedDemographicIds"));
    }


    /**
     *  Gets the ExcludedDemographics element Id.
     *
     *  @return the ExcludedDemographics element Id
     */

    public static org.osid.id.Id getExcludedDemographics() {
        return (makeElementId("osid.resource.demographic.demographic.ExcludedDemographics"));
    }


    /**
     *  Gets the IncludedResourceIds element Id.
     *
     *  @return the IncludedResourceIds element Id
     */

    public static org.osid.id.Id getIncludedResourceIds() {
        return (makeElementId("osid.resource.demographic.demographic.IncludedResourceIds"));
    }


    /**
     *  Gets the IncludedResources element Id.
     *
     *  @return the IncludedResources element Id
     */

    public static org.osid.id.Id getIncludedResources() {
        return (makeElementId("osid.resource.demographic.demographic.IncludedResources"));
    }


    /**
     *  Gets the ExcludedResourceIds element Id.
     *
     *  @return the ExcludedResourceIds element Id
     */

    public static org.osid.id.Id getExcludedResourceIds() {
        return (makeElementId("osid.resource.demographic.demographic.ExcludedResourceIds"));
    }


    /**
     *  Gets the ExcludedResources element Id.
     *
     *  @return the ExcludedResources element Id
     */

    public static org.osid.id.Id getExcludedResources() {
        return (makeElementId("osid.resource.demographic.demographic.ExcludedResources"));
    }


    /**
     *  Gets the ResultingResourceId element Id.
     *
     *  @return the ResultingResourceId element Id
     */

    public static org.osid.id.Id getResultingResourceId() {
        return (makeQueryElementId("osid.resource.demographic.demographic.ResultingResourceId"));
    }


    /**
     *  Gets the ResultingResource element Id.
     *
     *  @return the ResultingResource element Id
     */

    public static org.osid.id.Id getResultingResource() {
        return (makeQueryElementId("osid.resource.demographic.demographic.ResultingResource"));
    }


    /**
     *  Gets the BinId element Id.
     *
     *  @return the BinId element Id
     */

    public static org.osid.id.Id getBinId() {
        return (makeQueryElementId("osid.resource.demographic.demographic.BinId"));
    }


    /**
     *  Gets the Bin element Id.
     *
     *  @return the Bin element Id
     */

    public static org.osid.id.Id getBin() {
        return (makeQueryElementId("osid.resource.demographic.demographic.Bin"));
    }
}
