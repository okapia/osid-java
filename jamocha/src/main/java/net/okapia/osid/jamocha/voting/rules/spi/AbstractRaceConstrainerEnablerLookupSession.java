//
// AbstractRaceConstrainerEnablerLookupSession.java
//
//    A starter implementation framework for providing a RaceConstrainerEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a RaceConstrainerEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getRaceConstrainerEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractRaceConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.voting.rules.RaceConstrainerEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();
    

    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>RaceConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaceConstrainerEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>RaceConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceConstrainerEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>RaceConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceConstrainerEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race constrainer enablers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active race constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveRaceConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive race constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>RaceConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RaceConstrainerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>RaceConstrainerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, race constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive race constrainer enablers
     *  are returned.
     *
     *  @param  raceConstrainerEnablerId <code>Id</code> of the
     *          <code>RaceConstrainerEnabler</code>
     *  @return the race constrainer enabler
     *  @throws org.osid.NotFoundException <code>raceConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnabler getRaceConstrainerEnabler(org.osid.id.Id raceConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.voting.rules.RaceConstrainerEnablerList raceConstrainerEnablers = getRaceConstrainerEnablers()) {
            while (raceConstrainerEnablers.hasNext()) {
                org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler = raceConstrainerEnablers.getNextRaceConstrainerEnabler();
                if (raceConstrainerEnabler.getId().equals(raceConstrainerEnablerId)) {
                    return (raceConstrainerEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(raceConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  raceConstrainerEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>RaceConstrainerEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, race constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive race constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getRaceConstrainerEnablers()</code>.
     *
     *  @param  raceConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByIds(org.osid.id.IdList raceConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.voting.rules.RaceConstrainerEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = raceConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getRaceConstrainerEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("race constrainer enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.voting.rules.raceconstrainerenabler.LinkedRaceConstrainerEnablerList(ret));
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> corresponding to the given
     *  race constrainer enabler genus <code>Type</code> which does not include
     *  race constrainer enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  race constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those race constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive race constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getRaceConstrainerEnablers()</code>.
     *
     *  @param  raceConstrainerEnablerGenusType a raceConstrainerEnabler genus type 
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByGenusType(org.osid.type.Type raceConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.raceconstrainerenabler.RaceConstrainerEnablerGenusFilterList(getRaceConstrainerEnablers(), raceConstrainerEnablerGenusType));
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> corresponding to the given
     *  race constrainer enabler genus <code>Type</code> and include any additional
     *  race constrainer enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  race constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those race constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive race constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRaceConstrainerEnablers()</code>.
     *
     *  @param  raceConstrainerEnablerGenusType a raceConstrainerEnabler genus type 
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByParentGenusType(org.osid.type.Type raceConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getRaceConstrainerEnablersByGenusType(raceConstrainerEnablerGenusType));
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> containing the given
     *  race constrainer enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  race constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those race constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, race constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive race constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getRaceConstrainerEnablers()</code>.
     *
     *  @param  raceConstrainerEnablerRecordType a raceConstrainerEnabler record type 
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersByRecordType(org.osid.type.Type raceConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.raceconstrainerenabler.RaceConstrainerEnablerRecordFilterList(getRaceConstrainerEnablers(), raceConstrainerEnablerRecordType));
    }


    /**
     *  Gets a <code>RaceConstrainerEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  race constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those race constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, race constrainer enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive race constrainer enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RaceConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.voting.rules.raceconstrainerenabler.TemporalRaceConstrainerEnablerFilterList(getRaceConstrainerEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>RaceConstrainerEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  race constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those race constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, race constrainer enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive race constrainer enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>RaceConstrainerEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getRaceConstrainerEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>RaceConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  race constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those race constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, race constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive race constrainer enablers
     *  are returned.
     *
     *  @return a list of <code>RaceConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.voting.rules.RaceConstrainerEnablerList getRaceConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the race constrainer enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of race constrainer enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.voting.rules.RaceConstrainerEnablerList filterRaceConstrainerEnablersOnViews(org.osid.voting.rules.RaceConstrainerEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.voting.rules.RaceConstrainerEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.voting.rules.raceconstrainerenabler.ActiveRaceConstrainerEnablerFilterList(ret);
        }

        return (ret);
    }
}
