//
// AbstractAssemblyCompositionEnablerQuery.java
//
//     A CompositionEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.repository.rules.compositionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CompositionEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyCompositionEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.repository.rules.CompositionEnablerQuery,
               org.osid.repository.rules.CompositionEnablerQueryInspector,
               org.osid.repository.rules.CompositionEnablerSearchOrder {

    private final java.util.Collection<org.osid.repository.rules.records.CompositionEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.rules.records.CompositionEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.rules.records.CompositionEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyCompositionEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyCompositionEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the composition. 
     *
     *  @param  compositionId the composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCompositionId(org.osid.id.Id compositionId, 
                                        boolean match) {
        getAssembler().addIdTerm(getRuledCompositionIdColumn(), compositionId, match);
        return;
    }


    /**
     *  Clears the composition <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCompositionIdTerms() {
        getAssembler().clearTerms(getRuledCompositionIdColumn());
        return;
    }


    /**
     *  Gets the composition <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCompositionIdTerms() {
        return (getAssembler().getIdTerms(getRuledCompositionIdColumn()));
    }


    /**
     *  Gets the RuledCompositionId column name.
     *
     * @return the column name
     */

    protected String getRuledCompositionIdColumn() {
        return ("ruled_composition_id");
    }


    /**
     *  Tests if a <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCompositionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getRuledCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCompositionQuery() is false");
    }


    /**
     *  Matches enablers mapped to any composition. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          composition, <code> false </code> to match enablers mapped to 
     *          no composition 
     */

    @OSID @Override
    public void matchAnyRuledComposition(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledCompositionColumn(), match);
        return;
    }


    /**
     *  Clears the composition query terms. 
     */

    @OSID @Override
    public void clearRuledCompositionTerms() {
        getAssembler().clearTerms(getRuledCompositionColumn());
        return;
    }


    /**
     *  Gets the composition query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.CompositionQueryInspector[] getRuledCompositionTerms() {
        return (new org.osid.repository.CompositionQueryInspector[0]);
    }


    /**
     *  Gets the RuledComposition column name.
     *
     * @return the column name
     */

    protected String getRuledCompositionColumn() {
        return ("ruled_composition");
    }


    /**
     *  Matches enablers mapped to the composition. 
     *
     *  @param  repositoryId the repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRepositoryId(org.osid.id.Id repositoryId, boolean match) {
        getAssembler().addIdTerm(getRepositoryIdColumn(), repositoryId, match);
        return;
    }


    /**
     *  Clears the repository <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRepositoryIdTerms() {
        getAssembler().clearTerms(getRepositoryIdColumn());
        return;
    }


    /**
     *  Gets the repository <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRepositoryIdTerms() {
        return (getAssembler().getIdTerms(getRepositoryIdColumn()));
    }


    /**
     *  Gets the RepositoryId column name.
     *
     * @return the column name
     */

    protected String getRepositoryIdColumn() {
        return ("repository_id");
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsRepositoryQuery() is false");
    }


    /**
     *  Clears the repository query terms. 
     */

    @OSID @Override
    public void clearRepositoryTerms() {
        getAssembler().clearTerms(getRepositoryColumn());
        return;
    }


    /**
     *  Gets the repository query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQueryInspector[] getRepositoryTerms() {
        return (new org.osid.repository.RepositoryQueryInspector[0]);
    }


    /**
     *  Gets the Repository column name.
     *
     * @return the column name
     */

    protected String getRepositoryColumn() {
        return ("repository");
    }


    /**
     *  Tests if this compositionEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  compositionEnablerRecordType a composition enabler record type 
     *  @return <code>true</code> if the compositionEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type compositionEnablerRecordType) {
        for (org.osid.repository.rules.records.CompositionEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(compositionEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  compositionEnablerRecordType the composition enabler record type 
     *  @return the composition enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.rules.records.CompositionEnablerQueryRecord getCompositionEnablerQueryRecord(org.osid.type.Type compositionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.rules.records.CompositionEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(compositionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  compositionEnablerRecordType the composition enabler record type 
     *  @return the composition enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.rules.records.CompositionEnablerQueryInspectorRecord getCompositionEnablerQueryInspectorRecord(org.osid.type.Type compositionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.rules.records.CompositionEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(compositionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param compositionEnablerRecordType the composition enabler record type
     *  @return the composition enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.rules.records.CompositionEnablerSearchOrderRecord getCompositionEnablerSearchOrderRecord(org.osid.type.Type compositionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.rules.records.CompositionEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(compositionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this composition enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param compositionEnablerQueryRecord the composition enabler query record
     *  @param compositionEnablerQueryInspectorRecord the composition enabler query inspector
     *         record
     *  @param compositionEnablerSearchOrderRecord the composition enabler search order record
     *  @param compositionEnablerRecordType composition enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerQueryRecord</code>,
     *          <code>compositionEnablerQueryInspectorRecord</code>,
     *          <code>compositionEnablerSearchOrderRecord</code> or
     *          <code>compositionEnablerRecordTypecompositionEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addCompositionEnablerRecords(org.osid.repository.rules.records.CompositionEnablerQueryRecord compositionEnablerQueryRecord, 
                                      org.osid.repository.rules.records.CompositionEnablerQueryInspectorRecord compositionEnablerQueryInspectorRecord, 
                                      org.osid.repository.rules.records.CompositionEnablerSearchOrderRecord compositionEnablerSearchOrderRecord, 
                                      org.osid.type.Type compositionEnablerRecordType) {

        addRecordType(compositionEnablerRecordType);

        nullarg(compositionEnablerQueryRecord, "composition enabler query record");
        nullarg(compositionEnablerQueryInspectorRecord, "composition enabler query inspector record");
        nullarg(compositionEnablerSearchOrderRecord, "composition enabler search odrer record");

        this.queryRecords.add(compositionEnablerQueryRecord);
        this.queryInspectorRecords.add(compositionEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(compositionEnablerSearchOrderRecord);
        
        return;
    }
}
