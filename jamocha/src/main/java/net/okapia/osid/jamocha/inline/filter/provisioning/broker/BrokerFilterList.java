//
// BrokerFilterList.java
//
//     Implements a filtering BrokerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.provisioning.broker;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filtering BrokerList.
 */

public final class BrokerFilterList
    extends net.okapia.osid.jamocha.inline.filter.provisioning.broker.spi.AbstractBrokerFilterList
    implements org.osid.provisioning.BrokerList,
               BrokerFilter {

    private final BrokerFilter filter;


    /**
     *  Creates a new <code>BrokerFilterList</code>.
     *
     *  @param filter an inline query filter
     *  @param list a <code>BrokerList</code>
     *  @throws org.osid.NullArgumentException <code>filter</code> or
     *          <code>list</code> is <code>null</code>
     */

    public BrokerFilterList(BrokerFilter filter, org.osid.provisioning.BrokerList list) {
        super(list);

        nullarg(filter, "inline query filter");
        this.filter = filter;

        return;
    }    

    
    /**
     *  Filters Brokers.
     *
     *  @param broker the broker to filter
     *  @return <code>true</code> if the broker passes the filter,
     *          <code>false</code> if the broker should be filtered
     */

    @Override
    public boolean pass(org.osid.provisioning.Broker broker) {
        return (this.filter.pass(broker));
    }
}
