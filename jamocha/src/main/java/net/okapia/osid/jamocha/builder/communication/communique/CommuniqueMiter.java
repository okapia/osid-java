//
// CommuniqueMiter.java
//
//     Defines a Communique miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.communication.communique;


/**
 *  Defines a <code>Communique</code> miter for use with the builders.
 */

public interface CommuniqueMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.communication.Communique {


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @throws org.osid.NullArgumentException <code>message</code> is
     *          <code>null</code>
     */

    public void setMessage(org.osid.locale.DisplayText message);


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    public void setLevel(org.osid.communication.CommuniqueLevel level);


    /**
     *  Sets the response required flag.
     *
     *  @param responseRequired {@code true} if a response is
     *         required, {@code false} otherwise
     */

    public void setResponseRequired(boolean responseRequired);


    /**
     *  Adds a response options.
     *
     *  @param option a response option
     *  @throws org.osid.NullArgumentException <code>option</code> is
     *          <code>null</code>
     */

    public void addResponseOption(org.osid.communication.ResponseOption option);


    /**
     *  Sets the response options.
     *
     *  @param options a collection of response options
     *  @throws org.osid.NullArgumentException <code>options</code> is
     *          <code>null</code>
     */

    public void setResponseOptions(java.util.Collection<org.osid.communication.ResponseOption> options);


    /**
     *  Sets the respond via form flag.
     *
     *  @param respondViaForm {@code true} if the response should use
     *         a form, @code false} otherwise
     */

    public void setRespondViaForm(boolean respondViaForm);
}       


