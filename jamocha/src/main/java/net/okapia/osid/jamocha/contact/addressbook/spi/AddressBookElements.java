//
// AddressBookElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.addressbook.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class AddressBookElements
    extends net.okapia.osid.jamocha.spi.OsidCatalogElements {


    /**
     *  Gets the AddressBookElement Id.
     *
     *  @return the address book element Id
     */

    public static org.osid.id.Id getAddressBookEntityId() {
        return (makeEntityId("osid.contact.AddressBook"));
    }


    /**
     *  Gets the ContactId element Id.
     *
     *  @return the ContactId element Id
     */

    public static org.osid.id.Id getContactId() {
        return (makeQueryElementId("osid.contact.addressbook.ContactId"));
    }


    /**
     *  Gets the Contact element Id.
     *
     *  @return the Contact element Id
     */

    public static org.osid.id.Id getContact() {
        return (makeQueryElementId("osid.contact.addressbook.Contact"));
    }


    /**
     *  Gets the AddressId element Id.
     *
     *  @return the AddressId element Id
     */

    public static org.osid.id.Id getAddressId() {
        return (makeQueryElementId("osid.contact.addressbook.AddressId"));
    }


    /**
     *  Gets the Address element Id.
     *
     *  @return the Address element Id
     */

    public static org.osid.id.Id getAddress() {
        return (makeQueryElementId("osid.contact.addressbook.Address"));
    }


    /**
     *  Gets the AncestorAddressBookId element Id.
     *
     *  @return the AncestorAddressBookId element Id
     */

    public static org.osid.id.Id getAncestorAddressBookId() {
        return (makeQueryElementId("osid.contact.addressbook.AncestorAddressBookId"));
    }


    /**
     *  Gets the AncestorAddressBook element Id.
     *
     *  @return the AncestorAddressBook element Id
     */

    public static org.osid.id.Id getAncestorAddressBook() {
        return (makeQueryElementId("osid.contact.addressbook.AncestorAddressBook"));
    }


    /**
     *  Gets the DescendantAddressBookId element Id.
     *
     *  @return the DescendantAddressBookId element Id
     */

    public static org.osid.id.Id getDescendantAddressBookId() {
        return (makeQueryElementId("osid.contact.addressbook.DescendantAddressBookId"));
    }


    /**
     *  Gets the DescendantAddressBook element Id.
     *
     *  @return the DescendantAddressBook element Id
     */

    public static org.osid.id.Id getDescendantAddressBook() {
        return (makeQueryElementId("osid.contact.addressbook.DescendantAddressBook"));
    }
}
