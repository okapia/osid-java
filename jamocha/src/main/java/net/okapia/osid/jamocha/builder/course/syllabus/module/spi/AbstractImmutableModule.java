//
// AbstractImmutableModule.java
//
//     Wraps a mutable Module to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.syllabus.module.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Module</code> to hide modifiers. This
 *  wrapper provides an immutized Module from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying module whose state changes are visible.
 */

public abstract class AbstractImmutableModule
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.course.syllabus.Module {

    private final org.osid.course.syllabus.Module module;


    /**
     *  Constructs a new <code>AbstractImmutableModule</code>.
     *
     *  @param module the module to immutablize
     *  @throws org.osid.NullArgumentException <code>module</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableModule(org.osid.course.syllabus.Module module) {
        super(module);
        this.module = module;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the syllabus. 
     *
     *  @return the syllabus <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSyllabusId() {
        return (this.module.getSyllabusId());
    }


    /**
     *  Gets the syllabus. 
     *
     *  @return the syllabus 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.syllabus.Syllabus getSyllabus()
        throws org.osid.OperationFailedException {

        return (this.module.getSyllabus());
    }


    /**
     *  Gets the module record corresponding to the given <code> Module 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> moduleRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(moduleRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  moduleRecordType the type of module record to retrieve 
     *  @return the module record 
     *  @throws org.osid.NullArgumentException <code> moduleRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(moduleRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.records.ModuleRecord getModuleRecord(org.osid.type.Type moduleRecordType)
        throws org.osid.OperationFailedException {

        return (this.module.getModuleRecord(moduleRecordType));
    }
}

