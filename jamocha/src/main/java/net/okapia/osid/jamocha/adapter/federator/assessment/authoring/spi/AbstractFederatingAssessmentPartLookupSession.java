//
// AbstractFederatingAssessmentPartLookupSession.java
//
//     An abstract federating adapter for an AssessmentPartLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AssessmentPartLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAssessmentPartLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.assessment.authoring.AssessmentPartLookupSession>
    implements org.osid.assessment.authoring.AssessmentPartLookupSession {

    private boolean parallel = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();


    /**
     *  Constructs a new <code>AbstractFederatingAssessmentPartLookupSession</code>.
     */

    protected AbstractFederatingAssessmentPartLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.assessment.authoring.AssessmentPartLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }


    /**
     *  Tests if this user can perform <code>AssessmentPart</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessmentParts() {
        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            if (session.canLookupAssessmentParts()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AssessmentPart</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentPartView() {
        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            session.useComparativeAssessmentPartView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AssessmentPart</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentPartView() {
        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            session.usePlenaryAssessmentPartView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessment parts in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            session.useFederatedBankView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            session.useIsolatedBankView();
        }

        return;
    }


    /**
     *  Only active assessment parts are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAssessmentPartView() {
        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            session.useActiveAssessmentPartView();
        }

        return;
    }


    /**
     *  Active and inactive assessment parts are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAssessmentPartView() {
        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            session.useAnyStatusAssessmentPartView();
        }

        return;
    }
    

    /**
     *  The returns from the lookup methods omit sequestered
     *  assessment parts.
     */

    @OSID @Override
    public void useSequesteredAssessmentPartView() {
        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            session.useSequesteredAssessmentPartView();
        }

        return;
    }


    /**
     *  All assessment parts are returned including sequestered assessment parts.
     */

    @OSID @Override
    public void useUnsequesteredAssessmentPartView() {
        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            session.useUnsequesteredAssessmentPartView();
        }

        return;
    }

     
    /**
     *  Gets the <code>AssessmentPart</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AssessmentPart</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AssessmentPart</code> and
     *  retained for compatibility.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @param  assessmentPartId <code>Id</code> of the
     *          <code>AssessmentPart</code>
     *  @return the assessment part
     *  @throws org.osid.NotFoundException <code>assessmentPartId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentPartId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPart getAssessmentPart(org.osid.id.Id assessmentPartId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            try {
                return (session.getAssessmentPart(assessmentPartId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(assessmentPartId + " not found");
    }


    /**
     *  Gets an <code>AssessmentPartList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentParts specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AssessmentParts</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, assessment parts are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment parts are returned.
     *
     *  @param  assessmentPartIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentPart</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByIds(org.osid.id.IdList assessmentPartIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.assessment.authoring.assessmentpart.MutableAssessmentPartList ret = new net.okapia.osid.jamocha.assessment.authoring.assessmentpart.MutableAssessmentPartList();

        try (org.osid.id.IdList ids = assessmentPartIds) {
            while (ids.hasNext()) {
                ret.addAssessmentPart(getAssessmentPart(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentPartList</code> corresponding to the given
     *  assessment part genus <code>Type</code> which does not include
     *  assessment parts of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned list
     *  may contain only those assessment parts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, assessment parts are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment parts are returned.
     *
     *  @param  assessmentPartGenusType an assessmentPart genus type 
     *  @return the returned <code>AssessmentPart</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByGenusType(org.osid.type.Type assessmentPartGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.assessmentpart.FederatingAssessmentPartList ret = getAssessmentPartList();

        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            ret.addAssessmentPartList(session.getAssessmentPartsByGenusType(assessmentPartGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentPartList</code> corresponding to the given
     *  assessment part genus <code>Type</code> and include any additional
     *  assessment parts with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned list
     *  may contain only those assessment parts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @param  assessmentPartGenusType an assessmentPart genus type 
     *  @return the returned <code>AssessmentPart</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByParentGenusType(org.osid.type.Type assessmentPartGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.assessmentpart.FederatingAssessmentPartList ret = getAssessmentPartList();

        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            ret.addAssessmentPartList(session.getAssessmentPartsByParentGenusType(assessmentPartGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentPartList</code> containing the given
     *  assessment part record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned
     *  list may contain only those assessment parts that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In active mode, assessment parts are returned that are
     *  currently active. In any status mode, active and inactive
     *  assessment parts are returned.
     *
     *  @param  assessmentPartRecordType an assessmentPart record type 
     *  @return the returned <code>AssessmentPart</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPartRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsByRecordType(org.osid.type.Type assessmentPartRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.assessmentpart.FederatingAssessmentPartList ret = getAssessmentPartList();

        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            ret.addAssessmentPartList(session.getAssessmentPartsByRecordType(assessmentPartRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AssessmentPart</code> for the given
     *  assessmeng.
     *  
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned
     *  list may contain only those assessment parts that are
     *  accessible through this session.
     *  
     *  In active mode, assessment parts are returned that are
     *  currently active. In any effective mode, active and inactive
     *  assessment parts are returned.
     *  
     *  In sequestered mode, no sequestered assessment parts are
     *  returned. In unsequestered mode, all assessment parts are
     *  returned.
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @return the returned <code>AssessmentPart</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentPartsForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.assessmentpart.FederatingAssessmentPartList ret = getAssessmentPartList();

        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            ret.addAssessmentPartList(session.getAssessmentPartsForAssessment(assessmentId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AssessmentParts</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessment parts or an error results. Otherwise, the returned list
     *  may contain only those assessment parts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, assessment parts are returned that are currently
     *  active. In any status mode, active and inactive assessment parts
     *  are returned.
     *
     *  @return a list of <code>AssessmentParts</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentPartList getAssessmentParts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.assessment.authoring.assessmentpart.FederatingAssessmentPartList ret = getAssessmentPartList();

        for (org.osid.assessment.authoring.AssessmentPartLookupSession session : getSessions()) {
            ret.addAssessmentPartList(session.getAssessmentParts());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.assessment.authoring.assessmentpart.FederatingAssessmentPartList getAssessmentPartList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.authoring.assessmentpart.ParallelAssessmentPartList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.assessment.authoring.assessmentpart.CompositeAssessmentPartList());
        }
    }
}
