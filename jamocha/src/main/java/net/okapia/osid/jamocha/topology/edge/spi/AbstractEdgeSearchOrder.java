//
// AbstractEdgeSearchOdrer.java
//
//     Defines an EdgeSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.edge.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code EdgeSearchOrder}.
 */

public abstract class AbstractEdgeSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.topology.EdgeSearchOrder {

    private final java.util.Collection<org.osid.topology.records.EdgeSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the source node. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySourceNode(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a source node search order is available. 
     *
     *  @return <code> true </code> if a node search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSourceNodeSearchOrder() {
        return (false);
    }


    /**
     *  Gets a source node search order. 
     *
     *  @return a node search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSourceNodeSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchOrder getSourceNodeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsSourceNodeSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the destination 
     *  node. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDestinationNode(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a destination node search order is available. 
     *
     *  @return <code> true </code> if a node search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDestinationNodeSearchOrder() {
        return (false);
    }


    /**
     *  Gets a destination node search order. 
     *
     *  @return a node search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDestinationNodeSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeSearchOrder getDestinationNodeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsDestinationNodeSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  directionality. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDirectional(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the 
     *  directionality. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByBiDirectional(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the edge cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCost(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the edge 
     *  distance. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDistance(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  edgeRecordType an edge record type 
     *  @return {@code true} if the edgeRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code edgeRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type edgeRecordType) {
        for (org.osid.topology.records.EdgeSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  edgeRecordType the edge record type 
     *  @return the edge search order record
     *  @throws org.osid.NullArgumentException
     *          {@code edgeRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(edgeRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.topology.records.EdgeSearchOrderRecord getEdgeSearchOrderRecord(org.osid.type.Type edgeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.EdgeSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(edgeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(edgeRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this edge. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param edgeRecord the edge search odrer record
     *  @param edgeRecordType edge record type
     *  @throws org.osid.NullArgumentException
     *          {@code edgeRecord} or
     *          {@code edgeRecordTypeedge} is
     *          {@code null}
     */
            
    protected void addEdgeRecord(org.osid.topology.records.EdgeSearchOrderRecord edgeSearchOrderRecord, 
                                     org.osid.type.Type edgeRecordType) {

        addRecordType(edgeRecordType);
        this.records.add(edgeSearchOrderRecord);
        
        return;
    }
}
