//
// DictionaryNode.java
//
//     Defines a Dictionary node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary;


/**
 *  A class for managing a hierarchy of dictionary nodes in core.
 */

public final class DictionaryNode
    extends net.okapia.osid.jamocha.core.dictionary.spi.AbstractDictionaryNode
    implements org.osid.dictionary.DictionaryNode {


    /**
     *  Constructs a new <code>DictionaryNode</code> from a single
     *  dictionary.
     *
     *  @param dictionary the dictionary
     *  @throws org.osid.NullArgumentException <code>dictionary</code> is 
     *          <code>null</code>.
     */

    public DictionaryNode(org.osid.dictionary.Dictionary dictionary) {
        super(dictionary);
        return;
    }


    /**
     *  Constructs a new <code>DictionaryNode</code>.
     *
     *  @param dictionary the dictionary
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>dictionary</code>
     *          is <code>null</code>.
     */

    public DictionaryNode(org.osid.dictionary.Dictionary dictionary, boolean root, boolean leaf) {
        super(dictionary, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this dictionary.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.dictionary.DictionaryNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this dictionary.
     *
     *  @param dictionary the dictionary to add as a parent
     *  @throws org.osid.NullArgumentException <code>dictionary</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.dictionary.Dictionary dictionary) {
        addParent(new DictionaryNode(dictionary));
        return;
    }


    /**
     *  Adds a child to this dictionary.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.dictionary.DictionaryNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this dictionary.
     *
     *  @param dictionary the dictionary to add as a child
     *  @throws org.osid.NullArgumentException <code>dictionary</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.dictionary.Dictionary dictionary) {
        addChild(new DictionaryNode(dictionary));
        return;
    }
}
