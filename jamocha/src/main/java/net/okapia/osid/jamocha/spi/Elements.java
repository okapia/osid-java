//
// Elements.java
//
//     Id definitions for OsidForm and OsidQuery fields.
//
//
// Tom Coppeto
// Okapia
// 15 May 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import net.okapia.osid.primordium.id.BasicId;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Ids for OsidForm and OsidQuery elements. These are used in
 *  Metadata and may also be used as a key for relational mapping.
 */

public class Elements {
    

    /**
     *  Gets the Id for the comment field.
     *
     *  @return the comment Id
     */

    protected org.osid.id.Id getJournalCommentId() {
        return (makeFormElementId("osid.OsidForm.JournalComment"));
    }



    /**
     *  Makes an Id for a entity Id.
     *
     *  @param identifier the entity
     *  @return the Id
     */

    protected static org.osid.id.Id makeEntityId(String identifier) {
        return (new net.okapia.osid.primordium.id.BasicId("okapia.net", "OSID Entity", identifier));
    }


    /**
     *  Makes an Id for a element Id.
     *
     *  @param identifier the element
     *  @return the Id
     */

    protected static org.osid.id.Id makeElementId(String identifier) {
        return (new net.okapia.osid.primordium.id.BasicId("okapia.net", "OSID Entity Element", identifier));
    }


    /**
     *  Makes an Id for a form element Id for queries not based on
     *  entity elements.
     *
     *  @param identifier the element
     *  @return the Id
     */

    protected static org.osid.id.Id makeFormElementId(String identifier) {
        return (new net.okapia.osid.primordium.id.BasicId("okapia.net", "OSID Form Element", identifier));
    }


    /**
     *  Makes an Id for a query element Id for queries not based on
     *  entity elements.
     *
     *  @param identifier the element
     *  @return the Id
     */

    protected static org.osid.id.Id makeQueryElementId(String identifier) {
        return (new net.okapia.osid.primordium.id.BasicId("okapia.net", "OSID Query Element", identifier));
    }


    /**
     *  Makes an Id for a search order element Id for orders not based
     *  on entity elememts.
     *
     *  @param identifier the element
     *  @return the Id
     */

    protected static org.osid.id.Id makeSearchOrderElementId(String identifier) {
        return (new net.okapia.osid.primordium.id.BasicId("okapia.net", "OSID Search Order Element", identifier));
    }
}
