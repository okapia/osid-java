//
// AbstractActivitySearchOdrer.java
//
//     Defines an ActivitySearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.activity.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code ActivitySearchOrder}.
 */

public abstract class AbstractActivitySearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectSearchOrder
    implements org.osid.learning.ActivitySearchOrder {

    private final java.util.Collection<org.osid.learning.records.ActivitySearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the objective. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByObjective(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an objective search order is available. 
     *
     *  @return <code> true </code> if an objective search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSearchOrder() {
        return (false);
    }


    /**
     *  Gets an objective search order. 
     *
     *  @return an objective search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchOrder getObjectiveSearchOrder() {
        throw new org.osid.UnimplementedException("supportsObjectiveSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  activityRecordType an activity record type 
     *  @return {@code true} if the activityRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code activityRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRecordType) {
        for (org.osid.learning.records.ActivitySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity search order record
     *  @throws org.osid.NullArgumentException
     *          {@code activityRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(activityRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.learning.records.ActivitySearchOrderRecord getActivitySearchOrderRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ActivitySearchOrderRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this activity. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityRecord the activity search odrer record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException
     *          {@code activityRecord} or
     *          {@code activityRecordTypeactivity} is
     *          {@code null}
     */
            
    protected void addActivityRecord(org.osid.learning.records.ActivitySearchOrderRecord activitySearchOrderRecord, 
                                     org.osid.type.Type activityRecordType) {

        addRecordType(activityRecordType);
        this.records.add(activitySearchOrderRecord);
        
        return;
    }
}
