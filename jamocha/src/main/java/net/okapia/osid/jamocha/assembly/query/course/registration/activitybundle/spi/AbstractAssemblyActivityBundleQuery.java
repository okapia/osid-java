//
// AbstractAssemblyActivityBundleQuery.java
//
//     An ActivityBundleQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.registration.activitybundle.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActivityBundleQuery that stores terms.
 */

public abstract class AbstractAssemblyActivityBundleQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.course.registration.ActivityBundleQuery,
               org.osid.course.registration.ActivityBundleQueryInspector,
               org.osid.course.registration.ActivityBundleSearchOrder {

    private final java.util.Collection<org.osid.course.registration.records.ActivityBundleQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.registration.records.ActivityBundleQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.registration.records.ActivityBundleSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyActivityBundleQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyActivityBundleQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the course offering <code> Id </code> for this query. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        getAssembler().addIdTerm(getCourseOfferingIdColumn(), courseOfferingId, match);
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        getAssembler().clearTerms(getCourseOfferingIdColumn());
        return;
    }


    /**
     *  Gets the course offering <code> Id </code> query terms. 
     *
     *  @return the course offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (getAssembler().getIdTerms(getCourseOfferingIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course offering. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCourseOffering(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCourseOfferingColumn(), style);
        return;
    }


    /**
     *  Gets the CourseOfferingId column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingIdColumn() {
        return ("course_offering_id");
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Clears the course offering terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        getAssembler().clearTerms(getCourseOfferingColumn());
        return;
    }


    /**
     *  Gets the course offering query terms. 
     *
     *  @return the course offering query terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Tests if a course offering order is available. 
     *
     *  @return <code> true </code> if a course offering order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingSearchOrder() {
        return (false);
    }


    /**
     *  Gets the course offering order. 
     *
     *  @return the course offering search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSearchOrder getCourseOfferingSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingSearchOrder() is false");
    }


    /**
     *  Gets the CourseOffering column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingColumn() {
        return ("course_offering");
    }


    /**
     *  Sets the activity <code> Id </code> for this query to match activity 
     *  bundles that have a related course. 
     *
     *  @param  activityId an activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchActivityId(org.osid.id.Id activityId, boolean match) {
        getAssembler().addIdTerm(getActivityIdColumn(), activityId, match);
        return;
    }


    /**
     *  Clears the activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityIdTerms() {
        getAssembler().clearTerms(getActivityIdColumn());
        return;
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (getAssembler().getIdTerms(getActivityIdColumn()));
    }


    /**
     *  Gets the ActivityId column name.
     *
     * @return the column name
     */

    protected String getActivityIdColumn() {
        return ("activity_id");
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the activity query 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getActivityQuery() {
        throw new org.osid.UnimplementedException("supportsActivityQuery() is false");
    }


    /**
     *  Matches activity bundles that have any activity. 
     *
     *  @param  match <code> true </code> to match activity bundles with any 
     *          activity, <code> false </code> to match activity bundles with 
     *          no activities 
     */

    @OSID @Override
    public void matchAnyActivity(boolean match) {
        getAssembler().addIdWildcardTerm(getActivityColumn(), match);
        return;
    }


    /**
     *  Clears the activity terms. 
     */

    @OSID @Override
    public void clearActivityTerms() {
        getAssembler().clearTerms(getActivityColumn());
        return;
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the Activity column name.
     *
     * @return the column name
     */

    protected String getActivityColumn() {
        return ("activity");
    }


    /**
     *  Matches activity bundles with credits between the given numbers 
     *  inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchCredits(java.math.BigDecimal min, 
                             java.math.BigDecimal max, boolean match) {
        getAssembler().addDecimalRangeTerm(getCreditsColumn(), min, max, match);
        return;
    }


    /**
     *  Matches an activity bundle that has any credits assigned. 
     *
     *  @param  match <code> true </code> to match activity bundles with any 
     *          credits, <code> false </code> to match activity bundles with 
     *          no credits 
     */

    @OSID @Override
    public void matchAnyCredits(boolean match) {
        getAssembler().addDecimalRangeWildcardTerm(getCreditsColumn(), match);
        return;
    }


    /**
     *  Clears the credit terms. 
     */

    @OSID @Override
    public void clearCreditsTerms() {
        getAssembler().clearTerms(getCreditsColumn());
        return;
    }


    /**
     *  Gets the credits query terms. 
     *
     *  @return the credits query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalTerm[] getCreditsTerms() {
        return (getAssembler().getDecimalTerms(getCreditsColumn()));
    }


    /**
     *  Gets the Credits column name.
     *
     * @return the column name
     */

    protected String getCreditsColumn() {
        return ("credits");
    }


    /**
     *  Sets the grade system <code> Id </code> for this query. 
     *
     *  @param  gradeSystemId a grade system <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> gradeSystemId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchGradingOptionId(org.osid.id.Id gradeSystemId, 
                                     boolean match) {
        getAssembler().addIdTerm(getGradingOptionIdColumn(), gradeSystemId, match);
        return;
    }


    /**
     *  Clears the grade system <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearGradingOptionIdTerms() {
        getAssembler().clearTerms(getGradingOptionIdColumn());
        return;
    }


    /**
     *  Gets the grade system <code> Id </code> query terms. 
     *
     *  @return the grade system <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradingOptionIdTerms() {
        return (getAssembler().getIdTerms(getGradingOptionIdColumn()));
    }


    /**
     *  Gets the GradingOptionId column name.
     *
     * @return the column name
     */

    protected String getGradingOptionIdColumn() {
        return ("grading_option_id");
    }


    /**
     *  Tests if a <code> GradeSystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a grade system query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsGradingOptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a grading option. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return a grade system query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsGradingOptionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQuery getGradingOptionQuery() {
        throw new org.osid.UnimplementedException("supportsGradingOptionQuery() is false");
    }


    /**
     *  Matches activity bundles that have any grading option. 
     *
     *  @param  match <code> true </code> to match activity bundles with any 
     *          grading option, <code> false </code> to match activity bundles 
     *          with no grading options 
     */

    @OSID @Override
    public void matchAnyGradingOption(boolean match) {
        getAssembler().addIdWildcardTerm(getGradingOptionColumn(), match);
        return;
    }


    /**
     *  Clears the grading option terms. 
     */

    @OSID @Override
    public void clearGradingOptionTerms() {
        getAssembler().clearTerms(getGradingOptionColumn());
        return;
    }


    /**
     *  Gets the grade system query terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradingOptionTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the GradingOption column name.
     *
     * @return the column name
     */

    protected String getGradingOptionColumn() {
        return ("grading_option");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  activity bundles assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this activityBundle supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityBundleRecordType an activity bundle record type 
     *  @return <code>true</code> if the activityBundleRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityBundleRecordType) {
        for (org.osid.course.registration.records.ActivityBundleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityBundleRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  activityBundleRecordType the activity bundle record type 
     *  @return the activity bundle query record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityBundleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityBundleQueryRecord getActivityBundleQueryRecord(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityBundleQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityBundleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityBundleRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  activityBundleRecordType the activity bundle record type 
     *  @return the activity bundle query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityBundleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityBundleQueryInspectorRecord getActivityBundleQueryInspectorRecord(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityBundleQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(activityBundleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityBundleRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param activityBundleRecordType the activity bundle record type
     *  @return the activity bundle search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityBundleRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityBundleSearchOrderRecord getActivityBundleSearchOrderRecord(org.osid.type.Type activityBundleRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityBundleSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(activityBundleRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityBundleRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this activity bundle. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityBundleQueryRecord the activity bundle query record
     *  @param activityBundleQueryInspectorRecord the activity bundle query inspector
     *         record
     *  @param activityBundleSearchOrderRecord the activity bundle search order record
     *  @param activityBundleRecordType activity bundle record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityBundleQueryRecord</code>,
     *          <code>activityBundleQueryInspectorRecord</code>,
     *          <code>activityBundleSearchOrderRecord</code> or
     *          <code>activityBundleRecordTypeactivityBundle</code> is
     *          <code>null</code>
     */
            
    protected void addActivityBundleRecords(org.osid.course.registration.records.ActivityBundleQueryRecord activityBundleQueryRecord, 
                                      org.osid.course.registration.records.ActivityBundleQueryInspectorRecord activityBundleQueryInspectorRecord, 
                                      org.osid.course.registration.records.ActivityBundleSearchOrderRecord activityBundleSearchOrderRecord, 
                                      org.osid.type.Type activityBundleRecordType) {

        addRecordType(activityBundleRecordType);

        nullarg(activityBundleQueryRecord, "activity bundle query record");
        nullarg(activityBundleQueryInspectorRecord, "activity bundle query inspector record");
        nullarg(activityBundleSearchOrderRecord, "activity bundle search odrer record");

        this.queryRecords.add(activityBundleQueryRecord);
        this.queryInspectorRecords.add(activityBundleQueryInspectorRecord);
        this.searchOrderRecords.add(activityBundleSearchOrderRecord);
        
        return;
    }
}
