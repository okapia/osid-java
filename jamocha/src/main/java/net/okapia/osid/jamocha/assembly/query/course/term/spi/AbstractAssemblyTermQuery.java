//
// AbstractAssemblyTermQuery.java
//
//     A TermQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.course.term.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A TermQuery that stores terms.
 */

public abstract class AbstractAssemblyTermQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.course.TermQuery,
               org.osid.course.TermQueryInspector,
               org.osid.course.TermSearchOrder {

    private final java.util.Collection<org.osid.course.records.TermQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.TermQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.records.TermSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyTermQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyTermQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Adds a display label for this query. 
     *
     *  @param  label label string to match 
     *  @param  stringMatchType the label match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> label </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> label </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchDisplayLabel(String label, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        getAssembler().addStringTerm(getDisplayLabelColumn(), label, stringMatchType, match);
        return;
    }


    /**
     *  Matches a display label that has any value. 
     *
     *  @param  match <code> true </code> to match courses with any label, 
     *          <code> false </code> to match assets with no title 
     */

    @OSID @Override
    public void matchAnyDisplayLabel(boolean match) {
        getAssembler().addStringWildcardTerm(getDisplayLabelColumn(), match);
        return;
    }


    /**
     *  Clears the display label query terms. 
     */

    @OSID @Override
    public void clearDisplayLabelTerms() {
        getAssembler().clearTerms(getDisplayLabelColumn());
        return;
    }


    /**
     *  Gets the display label query terms. 
     *
     *  @return the display labelquery terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayLabelTerms() {
        return (getAssembler().getStringTerms(getDisplayLabelColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by course title. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDisplayLabel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDisplayLabelColumn(), style);
        return;
    }


    /**
     *  Gets the DisplayLabel column name.
     *
     * @return the column name
     */

    protected String getDisplayLabelColumn() {
        return ("display_label");
    }


    /**
     *  Matches the open date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchOpenDate(org.osid.calendaring.DateTime low, 
                              org.osid.calendaring.DateTime high, 
                              boolean match) {
        getAssembler().addDateTimeRangeTerm(getOpenDateColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any open date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any open date, 
     *          <code> false </code> to match terms with no open date 
     */

    @OSID @Override
    public void matchAnyOpenDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getOpenDateColumn(), match);
        return;
    }


    /**
     *  Clears the open date query terms. 
     */

    @OSID @Override
    public void clearOpenDateTerms() {
        getAssembler().clearTerms(getOpenDateColumn());
        return;
    }


    /**
     *  Gets the open date query terms. 
     *
     *  @return the open date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getOpenDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getOpenDateColumn()));
    }


    /**
     *  Specified a preference for ordering results by the open date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOpenDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOpenDateColumn(), style);
        return;
    }


    /**
     *  Gets the OpenDate column name.
     *
     * @return the column name
     */

    protected String getOpenDateColumn() {
        return ("open_date");
    }


    /**
     *  Matches the registration start date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchRegistrationStart(org.osid.calendaring.DateTime low, 
                                       org.osid.calendaring.DateTime high, 
                                       boolean match) {
        getAssembler().addDateTimeRangeTerm(getRegistrationStartColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any registration start date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any registration 
     *          start date, <code> false </code> to match terms with no 
     *          registration start date 
     */

    @OSID @Override
    public void matchAnyRegistrationStart(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getRegistrationStartColumn(), match);
        return;
    }


    /**
     *  Clears the registration start date query terms. 
     */

    @OSID @Override
    public void clearRegistrationStartTerms() {
        getAssembler().clearTerms(getRegistrationStartColumn());
        return;
    }


    /**
     *  Gets the registration start date query terms. 
     *
     *  @return the registration start date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getRegistrationStartTerms() {
        return (getAssembler().getDateTimeRangeTerms(getRegistrationStartColumn()));
    }


    /**
     *  Specified a preference for ordering results by registration start. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRegistrationStart(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRegistrationStartColumn(), style);
        return;
    }


    /**
     *  Gets the RegistrationStart column name.
     *
     * @return the column name
     */

    protected String getRegistrationStartColumn() {
        return ("registration_start");
    }


    /**
     *  Matches the registration end date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchRegistrationEnd(org.osid.calendaring.DateTime low, 
                                     org.osid.calendaring.DateTime high, 
                                     boolean match) {
        getAssembler().addDateTimeRangeTerm(getRegistrationEndColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any registration end date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any registration 
     *          end date, <code> false </code> to match terms with no 
     *          registration end date 
     */

    @OSID @Override
    public void matchAnyRegistrationEnd(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getRegistrationEndColumn(), match);
        return;
    }


    /**
     *  Clears the registration end date query terms. 
     */

    @OSID @Override
    public void clearRegistrationEndTerms() {
        getAssembler().clearTerms(getRegistrationEndColumn());
        return;
    }


    /**
     *  Gets the registration end date query terms. 
     *
     *  @return the registration end date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getRegistrationEndTerms() {
        return (getAssembler().getDateTimeRangeTerms(getRegistrationEndColumn()));
    }


    /**
     *  Specified a preference for ordering results by registration end. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRegistrationEnd(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRegistrationEndColumn(), style);
        return;
    }


    /**
     *  Gets the RegistrationEnd column name.
     *
     * @return the column name
     */

    protected String getRegistrationEndColumn() {
        return ("registration_end");
    }


    /**
     *  Matches the registration period that incudes the given date. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchRegistrationPeriod(org.osid.calendaring.DateTime date, 
                                        boolean match) {
        getAssembler().addDateTimeTerm(getRegistrationPeriodColumn(), date, match);
        return;
    }


    /**
     *  Clears the registration period query terms. 
     */

    @OSID @Override
    public void clearRegistrationPeriodTerms() {
        getAssembler().clearTerms(getRegistrationPeriodColumn());
        return;
    }


    /**
     *  Gets the registration period query terms. 
     *
     *  @return the registration period query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getRegistrationPeriodTerms() {
        return (getAssembler().getDateTimeRangeTerms(getRegistrationPeriodColumn()));
    }


    /**
     *  Gets the RegistrationPeriod column name.
     *
     * @return the column name
     */

    protected String getRegistrationPeriodColumn() {
        return ("registration_period");
    }


    /**
     *  Matches the registration period duration between the given range 
     *  inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchRegistrationDuration(org.osid.calendaring.Duration low, 
                                          org.osid.calendaring.Duration high, 
                                          boolean match) {
        getAssembler().addDurationRangeTerm(getRegistrationDurationColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the registration period duration query terms. 
     */

    @OSID @Override
    public void clearRegistrationDurationTerms() {
        getAssembler().clearTerms(getRegistrationDurationColumn());
        return;
    }


    /**
     *  Gets the registration duration query terms. 
     *
     *  @return the registration duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getRegistrationDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getRegistrationDurationColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the duration of 
     *  the registration period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRegistrationDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRegistrationDurationColumn(), style);
        return;
    }


    /**
     *  Gets the RegistrationDuration column name.
     *
     * @return the column name
     */

    protected String getRegistrationDurationColumn() {
        return ("registration_duration");
    }


    /**
     *  Matches the class start date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchClassesStart(org.osid.calendaring.DateTime low, 
                                  org.osid.calendaring.DateTime high, 
                                  boolean match) {
        getAssembler().addDateTimeRangeTerm(getClassesStartColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any class start date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any class start 
     *          date, <code> false </code> to match terms with no class start 
     *          date 
     */

    @OSID @Override
    public void matchAnyClassesStart(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getClassesStartColumn(), match);
        return;
    }


    /**
     *  Clears the class start date query terms. 
     */

    @OSID @Override
    public void clearClassesStartTerms() {
        getAssembler().clearTerms(getClassesStartColumn());
        return;
    }


    /**
     *  Gets the class start date query terms. 
     *
     *  @return the class start date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClassesStartTerms() {
        return (getAssembler().getDateTimeRangeTerms(getClassesStartColumn()));
    }


    /**
     *  Specified a preference for ordering results by classes start. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClassesStart(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getClassesStartColumn(), style);
        return;
    }


    /**
     *  Gets the ClassesStart column name.
     *
     * @return the column name
     */

    protected String getClassesStartColumn() {
        return ("classes_start");
    }


    /**
     *  Matches the class end date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchClassesEnd(org.osid.calendaring.DateTime low, 
                                org.osid.calendaring.DateTime high, 
                                boolean match) {
        getAssembler().addDateTimeRangeTerm(getClassesEndColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any class end date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any class end 
     *          date, <code> false </code> to match terms with no class end 
     *          date 
     */

    @OSID @Override
    public void matchAnyClassesEnd(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getClassesEndColumn(), match);
        return;
    }


    /**
     *  Clears the class end date query terms. 
     */

    @OSID @Override
    public void clearClassesEndTerms() {
        getAssembler().clearTerms(getClassesEndColumn());
        return;
    }


    /**
     *  Gets the class end date query terms. 
     *
     *  @return the class end date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClassesEndTerms() {
        return (getAssembler().getDateTimeRangeTerms(getClassesEndColumn()));
    }


    /**
     *  Specified a preference for ordering results by classes end. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClassesEnd(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getClassesEndColumn(), style);
        return;
    }


    /**
     *  Gets the ClassesEnd column name.
     *
     * @return the column name
     */

    protected String getClassesEndColumn() {
        return ("classes_end");
    }


    /**
     *  Matches the class period that incudes the given date. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchClassesPeriod(org.osid.calendaring.DateTime date, 
                                   boolean match) {
        getAssembler().addDateTimeTerm(getClassesPeriodColumn(), date, match);
        return;
    }


    /**
     *  Clears the class period query terms. 
     */

    @OSID @Override
    public void clearClassesPeriodTerms() {
        getAssembler().clearTerms(getClassesPeriodColumn());
        return;
    }


    /**
     *  Gets the class period query terms. 
     *
     *  @return the class period query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClassesPeriodTerms() {
        return (getAssembler().getDateTimeRangeTerms(getClassesPeriodColumn()));
    }


    /**
     *  Gets the ClassesPeriod column name.
     *
     * @return the column name
     */

    protected String getClassesPeriodColumn() {
        return ("classes_period");
    }


    /**
     *  Matches the classes period duration between the given range inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchClassesDuration(org.osid.calendaring.Duration low, 
                                     org.osid.calendaring.Duration high, 
                                     boolean match) {
        getAssembler().addDurationRangeTerm(getClassesDurationColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the classes period duration query terms. 
     */

    @OSID @Override
    public void clearClassesDurationTerms() {
        getAssembler().clearTerms(getClassesDurationColumn());
        return;
    }


    /**
     *  Gets the classes duration query terms. 
     *
     *  @return the classes duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getClassesDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getClassesDurationColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the duration of 
     *  the classes period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByClassesDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getClassesDurationColumn(), style);
        return;
    }


    /**
     *  Gets the ClassesDuration column name.
     *
     * @return the column name
     */

    protected String getClassesDurationColumn() {
        return ("classes_duration");
    }


    /**
     *  Matches the add date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchAddDate(org.osid.calendaring.DateTime low, 
                             org.osid.calendaring.DateTime high, boolean match) {
        getAssembler().addDateTimeRangeTerm(getAddDateColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any add date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any add date, 
     *          <code> false </code> to match terms with no add date 
     */

    @OSID @Override
    public void matchAnyAddDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getAddDateColumn(), match);
        return;
    }


    /**
     *  Clears the add date query terms. 
     */

    @OSID @Override
    public void clearAddDateTerms() {
        getAssembler().clearTerms(getAddDateColumn());
        return;
    }


    /**
     *  Gets the add date query terms. 
     *
     *  @return the add date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getAddDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getAddDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the add date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAddDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAddDateColumn(), style);
        return;
    }


    /**
     *  Gets the AddDate column name.
     *
     * @return the column name
     */

    protected String getAddDateColumn() {
        return ("add_date");
    }


    /**
     *  Matches the drop date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchDropDate(org.osid.calendaring.DateTime low, 
                              org.osid.calendaring.DateTime high, 
                              boolean match) {
        getAssembler().addDateTimeRangeTerm(getDropDateColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any drop date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any drop date, 
     *          <code> false </code> to match terms with no drop date 
     */

    @OSID @Override
    public void matchAnyDropDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getDropDateColumn(), match);
        return;
    }


    /**
     *  Clears the drop date query terms. 
     */

    @OSID @Override
    public void clearDropDateTerms() {
        getAssembler().clearTerms(getDropDateColumn());
        return;
    }


    /**
     *  Gets the drop date query terms. 
     *
     *  @return the drop date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDropDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDropDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the drop date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDropDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDropDateColumn(), style);
        return;
    }


    /**
     *  Gets the DropDate column name.
     *
     * @return the column name
     */

    protected String getDropDateColumn() {
        return ("drop_date");
    }


    /**
     *  Matches the final exam period start date between the given range 
     *  inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchFinalExamStart(org.osid.calendaring.DateTime low, 
                                    org.osid.calendaring.DateTime high, 
                                    boolean match) {
        getAssembler().addDateTimeRangeTerm(getFinalExamStartColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any final exam period start date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any final exam 
     *          start date, <code> false </code> to match terms with no final 
     *          exam start date 
     */

    @OSID @Override
    public void matchAnyFinalExamStart(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getFinalExamStartColumn(), match);
        return;
    }


    /**
     *  Clears the final exam start date query terms. 
     */

    @OSID @Override
    public void clearFinalExamStartTerms() {
        getAssembler().clearTerms(getFinalExamStartColumn());
        return;
    }


    /**
     *  Gets the final exam start date query terms. 
     *
     *  @return the final exam start date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getFinalExamStartTerms() {
        return (getAssembler().getDateTimeRangeTerms(getFinalExamStartColumn()));
    }


    /**
     *  Specified a preference for ordering results by final exam start. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFinalExamStart(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFinalExamStartColumn(), style);
        return;
    }


    /**
     *  Gets the FinalExamStart column name.
     *
     * @return the column name
     */

    protected String getFinalExamStartColumn() {
        return ("final_exam_start");
    }


    /**
     *  Matches the final exam period end date between the given range 
     *  inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchFinalExamEnd(org.osid.calendaring.DateTime low, 
                                  org.osid.calendaring.DateTime high, 
                                  boolean match) {
        getAssembler().addDateTimeRangeTerm(getFinalExamEndColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any final exam period end date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any final exam 
     *          end date, <code> false </code> to match terms with no final 
     *          exam end date 
     */

    @OSID @Override
    public void matchAnyFinalExamEnd(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getFinalExamEndColumn(), match);
        return;
    }


    /**
     *  Clears the final exam period end date query terms. 
     */

    @OSID @Override
    public void clearFinalExamEndTerms() {
        getAssembler().clearTerms(getFinalExamEndColumn());
        return;
    }


    /**
     *  Gets the final exam end date query terms. 
     *
     *  @return the final exam end date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getFinalExamEndTerms() {
        return (getAssembler().getDateTimeRangeTerms(getFinalExamEndColumn()));
    }


    /**
     *  Specified a preference for ordering results by final exam end. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFinalExamEnd(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFinalExamEndColumn(), style);
        return;
    }


    /**
     *  Gets the FinalExamEnd column name.
     *
     * @return the column name
     */

    protected String getFinalExamEndColumn() {
        return ("final_exam_end");
    }


    /**
     *  Matches the final exam period that incudes the given date. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchFinalExamPeriod(org.osid.calendaring.DateTime date, 
                                     boolean match) {
        getAssembler().addDateTimeTerm(getFinalExamPeriodColumn(), date, match);
        return;
    }


    /**
     *  Clears the final exam period query terms. 
     */

    @OSID @Override
    public void clearFinalExamPeriodTerms() {
        getAssembler().clearTerms(getFinalExamPeriodColumn());
        return;
    }


    /**
     *  Gets the final exam period query terms. 
     *
     *  @return the final exam period query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getFinalExamPeriodTerms() {
        return (getAssembler().getDateTimeRangeTerms(getFinalExamPeriodColumn()));
    }


    /**
     *  Gets the FinalExamPeriod column name.
     *
     * @return the column name
     */

    protected String getFinalExamPeriodColumn() {
        return ("final_exam_period");
    }


    /**
     *  Matches the final exam period duration between the given range 
     *  inclusive. 
     *
     *  @param  low low duration range 
     *  @param  high high duration range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchFinalExamDuration(org.osid.calendaring.Duration low, 
                                       org.osid.calendaring.Duration high, 
                                       boolean match) {
        getAssembler().addDurationRangeTerm(getFinalExamDurationColumn(), low, high, match);
        return;
    }


    /**
     *  Clears the final exam period duration query terms. 
     */

    @OSID @Override
    public void clearFinalExamDurationTerms() {
        getAssembler().clearTerms(getFinalExamDurationColumn());
        return;
    }


    /**
     *  Gets the final exam duration query terms. 
     *
     *  @return the final exam duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getFinalExamDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getFinalExamDurationColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the duration of 
     *  the final exam period. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFinalExamDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getFinalExamDurationColumn(), style);
        return;
    }


    /**
     *  Gets the FinalExamDuration column name.
     *
     * @return the column name
     */

    protected String getFinalExamDurationColumn() {
        return ("final_exam_duration");
    }


    /**
     *  Matches the close date between the given range inclusive. 
     *
     *  @param  low low date range 
     *  @param  high high date range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> high </code> is less 
     *          than <code> low </code> 
     */

    @OSID @Override
    public void matchCloseDate(org.osid.calendaring.DateTime low, 
                               org.osid.calendaring.DateTime high, 
                               boolean match) {
        getAssembler().addDateTimeRangeTerm(getCloseDateColumn(), low, high, match);
        return;
    }


    /**
     *  Matches a term that has any close date assigned. 
     *
     *  @param  match <code> true </code> to match terms with any close date, 
     *          <code> false </code> to match terms with no close date 
     */

    @OSID @Override
    public void matchAnyCloseDate(boolean match) {
        getAssembler().addDateTimeRangeWildcardTerm(getCloseDateColumn(), match);
        return;
    }


    /**
     *  Clears the close date query terms. 
     */

    @OSID @Override
    public void clearCloseDateTerms() {
        getAssembler().clearTerms(getCloseDateColumn());
        return;
    }


    /**
     *  Gets the close date query terms. 
     *
     *  @return the close date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCloseDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getCloseDateColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the close date. 
     *
     *  @param  style a search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCloseDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCloseDateColumn(), style);
        return;
    }


    /**
     *  Gets the CloseDate column name.
     *
     * @return the column name
     */

    protected String getCloseDateColumn() {
        return ("close_date");
    }


    /**
     *  Sets the term <code> Id </code> for this query to match terms that 
     *  have the specified term as an ancestor. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorTermId(org.osid.id.Id termId, boolean match) {
        getAssembler().addIdTerm(getAncestorTermIdColumn(), termId, match);
        return;
    }


    /**
     *  Clears the ancestor term <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorTermIdTerms() {
        getAssembler().clearTerms(getAncestorTermIdColumn());
        return;
    }


    /**
     *  Gets the ancestor term <code> Id </code> query terms. 
     *
     *  @return the ancestor term <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorTermIdTerms() {
        return (getAssembler().getIdTerms(getAncestorTermIdColumn()));
    }


    /**
     *  Gets the AncestorTermId column name.
     *
     * @return the column name
     */

    protected String getAncestorTermIdColumn() {
        return ("ancestor_term_id");
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a termency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorTermQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getAncestorTermQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorTermQuery() is false");
    }


    /**
     *  Matches terms with any ancestor. 
     *
     *  @param  match <code> true </code> to match terms with any ancestor, 
     *          <code> false </code> to match root terms 
     */

    @OSID @Override
    public void matchAnyAncestorTerm(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorTermColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor term query terms. 
     */

    @OSID @Override
    public void clearAncestorTermTerms() {
        getAssembler().clearTerms(getAncestorTermColumn());
        return;
    }


    /**
     *  Gets the ancestor term query terms. 
     *
     *  @return the ancestor term terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getAncestorTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Gets the AncestorTerm column name.
     *
     * @return the column name
     */

    protected String getAncestorTermColumn() {
        return ("ancestor_term");
    }


    /**
     *  Sets the term <code> Id </code> for this query to match terms that 
     *  have the specified term as a descendant. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantTermId(org.osid.id.Id termId, boolean match) {
        getAssembler().addIdTerm(getDescendantTermIdColumn(), termId, match);
        return;
    }


    /**
     *  Clears the descendant term <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantTermIdTerms() {
        getAssembler().clearTerms(getDescendantTermIdColumn());
        return;
    }


    /**
     *  Gets the descendant term <code> Id </code> query terms. 
     *
     *  @return the descendant term <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantTermIdTerms() {
        return (getAssembler().getIdTerms(getDescendantTermIdColumn()));
    }


    /**
     *  Gets the DescendantTermId column name.
     *
     * @return the column name
     */

    protected String getDescendantTermIdColumn() {
        return ("descendant_term_id");
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a termency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a term. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantTermQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getDescendantTermQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantTermQuery() is false");
    }


    /**
     *  Matches terms with any descendant. 
     *
     *  @param  match <code> true </code> to match terms with any Ddscendant, 
     *          <code> false </code> to match leaf terms 
     */

    @OSID @Override
    public void matchAnyDescendantTerm(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantTermColumn(), match);
        return;
    }


    /**
     *  Clears the descendant term query terms. 
     */

    @OSID @Override
    public void clearDescendantTermTerms() {
        getAssembler().clearTerms(getDescendantTermColumn());
        return;
    }


    /**
     *  Gets the descendant term query terms. 
     *
     *  @return the descendant term terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getDescendantTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Gets the DescendantTerm column name.
     *
     * @return the column name
     */

    protected String getDescendantTermColumn() {
        return ("descendant_term");
    }


    /**
     *  Sets the course offering <code> Id </code> for this query to match 
     *  terms assigned to course offerings. 
     *
     *  @param  courseOfferingId the course offering <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        getAssembler().addIdTerm(getCourseOfferingIdColumn(), courseOfferingId, match);
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        getAssembler().clearTerms(getCourseOfferingIdColumn());
        return;
    }


    /**
     *  Gets the course offering <code> Id </code> query terms. 
     *
     *  @return the course offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (getAssembler().getIdTerms(getCourseOfferingIdColumn()));
    }


    /**
     *  Gets the CourseOfferingId column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingIdColumn() {
        return ("course_offering_id");
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Matches courses that have any course offering. 
     *
     *  @param  match <code> true </code> to match terms with any course 
     *          offering, <code> false </code> to match terms with no course 
     *          offerings 
     */

    @OSID @Override
    public void matchAnyCourseOffering(boolean match) {
        getAssembler().addIdWildcardTerm(getCourseOfferingColumn(), match);
        return;
    }


    /**
     *  Clears the course offering query terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        getAssembler().clearTerms(getCourseOfferingColumn());
        return;
    }


    /**
     *  Gets the course offering query terms. 
     *
     *  @return the course offering query terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Gets the CourseOffering column name.
     *
     * @return the column name
     */

    protected String getCourseOfferingColumn() {
        return ("course_offering");
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  terms assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        getAssembler().addIdTerm(getCourseCatalogIdColumn(), courseCatalogId, match);
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        getAssembler().clearTerms(getCourseCatalogIdColumn());
        return;
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (getAssembler().getIdTerms(getCourseCatalogIdColumn()));
    }


    /**
     *  Gets the CourseCatalogId column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogIdColumn() {
        return ("course_catalog_id");
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog query terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        getAssembler().clearTerms(getCourseCatalogColumn());
        return;
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }


    /**
     *  Gets the CourseCatalog column name.
     *
     * @return the column name
     */

    protected String getCourseCatalogColumn() {
        return ("course_catalog");
    }


    /**
     *  Tests if this term supports the given record
     *  <code>Type</code>.
     *
     *  @param  termRecordType a term record type 
     *  @return <code>true</code> if the termRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type termRecordType) {
        for (org.osid.course.records.TermQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(termRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  termRecordType the term record type 
     *  @return the term query record 
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(termRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.TermQueryRecord getTermQueryRecord(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.TermQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(termRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(termRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  termRecordType the term record type 
     *  @return the term query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(termRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.TermQueryInspectorRecord getTermQueryInspectorRecord(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.TermQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(termRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(termRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param termRecordType the term record type
     *  @return the term search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(termRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.TermSearchOrderRecord getTermSearchOrderRecord(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.TermSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(termRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(termRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this term. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param termQueryRecord the term query record
     *  @param termQueryInspectorRecord the term query inspector
     *         record
     *  @param termSearchOrderRecord the term search order record
     *  @param termRecordType term record type
     *  @throws org.osid.NullArgumentException
     *          <code>termQueryRecord</code>,
     *          <code>termQueryInspectorRecord</code>,
     *          <code>termSearchOrderRecord</code> or
     *          <code>termRecordTypeterm</code> is
     *          <code>null</code>
     */
            
    protected void addTermRecords(org.osid.course.records.TermQueryRecord termQueryRecord, 
                                      org.osid.course.records.TermQueryInspectorRecord termQueryInspectorRecord, 
                                      org.osid.course.records.TermSearchOrderRecord termSearchOrderRecord, 
                                      org.osid.type.Type termRecordType) {

        addRecordType(termRecordType);

        nullarg(termQueryRecord, "term query record");
        nullarg(termQueryInspectorRecord, "term query inspector record");
        nullarg(termSearchOrderRecord, "term search odrer record");

        this.queryRecords.add(termQueryRecord);
        this.queryInspectorRecords.add(termQueryInspectorRecord);
        this.searchOrderRecords.add(termSearchOrderRecord);
        
        return;
    }
}
