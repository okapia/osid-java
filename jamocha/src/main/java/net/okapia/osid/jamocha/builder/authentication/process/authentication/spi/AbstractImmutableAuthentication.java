//
// AbstractImmutableAuthentication.java
//
//     Wraps a mutable Authentication to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.authentication.process.authentication.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Authentication</code> to hide modifiers. This
 *  wrapper provides an immutized Authentication from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying authentication whose state changes are visible.
 */

public abstract class AbstractImmutableAuthentication
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.authentication.process.Authentication {

    private final org.osid.authentication.process.Authentication authentication;


    /**
     *  Constructs a new <code>AbstractImmutableAuthentication</code>.
     *
     *  @param authentication the authentication to immutablize
     *  @throws org.osid.NullArgumentException <code>authentication</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAuthentication(org.osid.authentication.process.Authentication authentication) {
        super(authentication);
        this.authentication = authentication;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Agent </code> identified in 
     *  this authentication credential. 
     *
     *  @return the <code> Agent Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.authentication.getAgentId());
    }


    /**
     *  Gets the <code> Agent </code> identified in this authentication 
     *  credential. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.authentication.getAgent());
    }


    /**
     *  Tests whether or not the credential represented by this <code> 
     *  Authentication </code> is currently valid. A credential may be invalid 
     *  because it has been destroyed, expired, or is somehow no longer able 
     *  to be used. 
     *
     *  @return <code> true </code> if this authentication credential is 
     *          valid, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isValid() {
        return (this.authentication.isValid());
    }


    /**
     *  Tests if this authentication has an expiration. 
     *
     *  @return <code> true </code> if this authentication has an expiration, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasExpiration() {
        return (this.authentication.hasExpiration());
    }


    /**
     *  Gets the expiration date associated with this authentication 
     *  credential. Consumers should check for the existence of a an 
     *  expiration mechanism via <code> hasExpiration(). </code> 
     *
     *  @return the expiration date of this authentication credential 
     *  @throws org.osid.IllegalStateException <code> hasExpiration() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public java.util.Date getExpiration() {
        return (this.authentication.getExpiration());
    }


    /**
     *  Tests if this authentication has a credential for export. 
     *
     *  @return <code> true </code> if this authentication has a credential, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCredential() {
        return (this.authentication.hasCredential());
    }


    /**
     *  Gets the credential represented by the given <code> Type
     *  </code> for transport to a remote service.
     *
     *  @param  credentialType the credential format <code> Type </code> 
     *  @return the credential 
     *  @throws org.osid.IllegalStateException <code> hasCredential() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.NullArgumentException <code> credentialType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.UnsupportedException the given <code> credentialType 
     *          </code> is not supported 
     */

    @OSID @Override
    public java.lang.Object getCredential(org.osid.type.Type credentialType) {
        return (this.authentication.getCredential(credentialType));
    }


    /**
     *  Gets the authentication record corresponding to the given
     *  authentication record <code> Type. </code> This method is used
     *  to retrieve an object implementing the requested record. The
     *  <code> authenticationRecordType </code> may be the <code> Type
     *  </code> returned in <code> getRecordTypes() </code> or any of
     *  its parents in a <code> Type </code> hierarchy where <code>
     *  hasRecordType(authenticationRecordType) </code> is <code> true
     *  </code>.
     *
     *  @param  authenticationRecordType the type of authentication record to 
     *          retrieve 
     *  @return the authentication record 
     *  @throws org.osid.NullArgumentException <code> authenticationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(authenticatonRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.process.records.AuthenticationRecord getAuthenticationRecord(org.osid.type.Type authenticationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.authentication.getAuthenticationRecord(authenticationRecordType));
    }
}

