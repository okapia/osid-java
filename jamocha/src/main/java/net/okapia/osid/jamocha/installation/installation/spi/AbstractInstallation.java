//
// AbstractInstallation.java
//
//     Defines an Installation.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installation.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Installation</code>.
 */

public abstract class AbstractInstallation
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.installation.Installation {

    private org.osid.installation.Site site;
    private org.osid.installation.Package pkg;
    private org.osid.installation.Depot depot;
    private org.osid.calendaring.DateTime installDate;
    private org.osid.authentication.Agent agent;
    private org.osid.calendaring.DateTime lastCheckDate;

    private final java.util.Collection<org.osid.installation.records.InstallationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Site Id </code> in which this installation is 
     *  installed. 
     *
     *  @return the site <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSiteId() {
        return (this.site.getId());
    }


    /**
     *  Gets the <code> Site </code> in which this installation is installed. 
     *
     *  @return the package site 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.Site getSite()
        throws org.osid.OperationFailedException {

        return (this.site);
    }


    /**
     *  Sets the site.
     *
     *  @param site a site
     *  @throws org.osid.NullArgumentException
     *          <code>site</code> is <code>null</code>
     */

    protected void setSite(org.osid.installation.Site site) {
        nullarg(site, "site");
        this.site = site;
        return;
    }


    /**
     *  Gets the package <code> Id </code> of this installation. 
     *
     *  @return the package <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getPackageId() {
        return (this.pkg.getId());
    }


    /**
     *  Gets the package. 
     *
     *  @return the package 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.Package getPackage()
        throws org.osid.OperationFailedException {

        return (this.pkg);
    }


    /**
     *  Sets the package.
     *
     *  @param pkg a package
     *  @throws org.osid.NullArgumentException
     *          <code>pkg</code> is <code>null</code>
     */

    protected void setPackage(org.osid.installation.Package pkg) {
        nullarg(pkg, "package");
        this.pkg = pkg;
        return;
    }


    /**
     *  Gets the <code> Id </code> of depot from which the package was 
     *  installed. 
     *
     *  @return the depot <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDepotId() {
        return (this.depot.getId());
    }


    /**
     *  Gets the depot from which the package was installed. 
     *
     *  @return the depot 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.installation.Depot getDepot()
        throws org.osid.OperationFailedException {

        return (this.depot);
    }


    /**
     *  Sets the depot.
     *
     *  @param depot a depot
     *  @throws org.osid.NullArgumentException
     *          <code>depot</code> is <code>null</code>
     */

    protected void setDepot(org.osid.installation.Depot depot) {
        nullarg(depot, "depot");
        this.depot = depot;
        return;
    }


    /**
     *  Gets the date the package was installed. 
     *
     *  @return the installation date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getInstallDate() {
        return (this.installDate);
    }


    /**
     *  Sets the install date.
     *
     *  @param date an install date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setInstallDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "install date");
        this.installDate = date;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the agent who installed this package. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.agent.getId());
    }


    /**
     *  Gets the agent who installed this package. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.agent);
    }


    /**
     *  Sets the agent.
     *
     *  @param agent an agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "agent");
        this.agent = agent;
        return;
    }


    /**
     *  Gets the date the installation was last checked for updates. 
     *
     *  @return the last check date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getLastCheckDate() {
        return (this.lastCheckDate);
    }


    /**
     *  Sets the last check date.
     *
     *  @param date a last check date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setLastCheckDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "last check date");
        this.lastCheckDate = date;
        return;
    }


    /**
     *  Tests if this installation supports the given record
     *  <code>Type</code>.
     *
     *  @param  installationRecordType an installation record type 
     *  @return <code>true</code> if the installationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type installationRecordType) {
        for (org.osid.installation.records.InstallationRecord record : this.records) {
            if (record.implementsRecordType(installationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Installation</code> record <code>Type</code>.
     *
     *  @param  installationRecordType the installation record type 
     *  @return the installation record 
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationRecord getInstallationRecord(org.osid.type.Type installationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationRecord record : this.records) {
            if (record.implementsRecordType(installationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this installation. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param installationRecord the installation record
     *  @param installationRecordType installation record type
     *  @throws org.osid.NullArgumentException
     *          <code>installationRecord</code> or
     *          <code>installationRecordTypeinstallation</code> is
     *          <code>null</code>
     */
            
    protected void addInstallationRecord(org.osid.installation.records.InstallationRecord installationRecord, 
                                         org.osid.type.Type installationRecordType) {
        
        nullarg(installationRecord, "installation record");
        addRecordType(installationRecordType);
        this.records.add(installationRecord);
        
        return;
    }
}
