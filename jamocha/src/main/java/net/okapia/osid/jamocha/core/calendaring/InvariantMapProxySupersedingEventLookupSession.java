//
// InvariantMapProxySupersedingEventLookupSession
//
//    Implements a SupersedingEvent lookup service backed by a fixed
//    collection of supersedingEvents. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a SupersedingEvent lookup service backed by a fixed
 *  collection of superseding events. The superseding events are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxySupersedingEventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapSupersedingEventLookupSession
    implements org.osid.calendaring.SupersedingEventLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySupersedingEventLookupSession} with no
     *  superseding events.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxySupersedingEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxySupersedingEventLookupSession} with a single
     *  superseding event.
     *
     *  @param calendar the calendar
     *  @param supersedingEvent a single superseding event
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code supersedingEvent} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySupersedingEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.SupersedingEvent supersedingEvent, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putSupersedingEvent(supersedingEvent);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxySupersedingEventLookupSession} using
     *  an array of superseding events.
     *
     *  @param calendar the calendar
     *  @param supersedingEvents an array of superseding events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code supersedingEvents} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySupersedingEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.calendaring.SupersedingEvent[] supersedingEvents, org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putSupersedingEvents(supersedingEvents);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxySupersedingEventLookupSession} using a
     *  collection of superseding events.
     *
     *  @param calendar the calendar
     *  @param supersedingEvents a collection of superseding events
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code supersedingEvents} or {@code proxy} is {@code null}
     */

    public InvariantMapProxySupersedingEventLookupSession(org.osid.calendaring.Calendar calendar,
                                                  java.util.Collection<? extends org.osid.calendaring.SupersedingEvent> supersedingEvents,
                                                  org.osid.proxy.Proxy proxy) {

        this(calendar, proxy);
        putSupersedingEvents(supersedingEvents);
        return;
    }
}
