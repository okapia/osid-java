//
// AbstractOsidOperableForm.java
//
//     Defines a simple OSID form to draw from.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A basic OsidOperableForm.
 */

public abstract class AbstractOsidOperableForm
    extends AbstractOsidForm
    implements org.osid.OsidOperableForm {
    
    private boolean enabled;
    private boolean enabledSet = false;
    private boolean enabledCleared = false;
    private org.osid.Metadata enabledMetadata = getDefaultBooleanMetadata(getEnabledId(), "enabled");    
    
    private boolean disabled;
    private boolean disabledSet = false;
    private boolean disabledCleared = false;
    private org.osid.Metadata disabledMetadata = getDefaultBooleanMetadata(getDisabledId(), "disabled");


    /** 
     *  Constructs a new {@code AbstractOsidOperableForm}.
     *
     *  @param locale this serves as the default Locale for this form
     *         which generally is the Locale of the associated
     *         session. Additional locales may be set.
     *  @param update {@code true} if for update, {@code false} if for
     *         create
     */

    protected AbstractOsidOperableForm(org.osid.locale.Locale locale, boolean update) {
        super(locale, update);
        return;
    }


    /**
     *  Gets the metadata for the enabled.
     *
     *  @return metadata for the enabled 
     */

    @OSID @Override
    public org.osid.Metadata getEnabledMetadata() {
        return (this.enabledMetadata);
    }


    /**
     *  Sets the enabled metadata.
     *
     *  @param metadata the enabled metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setEnabledMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "enabled metadata");
        this.enabledMetadata = metadata;
        return;
    }

    
    /**
     *  Gets the Id for the enabled field.
     *
     *  @return the enabled field Id
     */

    protected org.osid.id.Id getEnabledId() {
        return (OperableElements.getEnabled());
    }

    
    /**
     *  Sets the enabled flag. 
     *
     *  @param  enabled the new enabled  flag
     *  @throws org.osid.InvalidArgumentException <code> enabled </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public void setEnabled(boolean enabled) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(enabled, getEnabledMetadata());

        this.enabled = enabled;
        this.enabledSet = true;
        this.enabledCleared = false;

        return;
    }


    /**
     *  Clears the enabled flag. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearEnabled() {
        if (getEnabledMetadata().isRequired()) {
            throw new org.osid.NoAccessException("enabled is required");
        }

        this.enabledSet = false;;
        this.enabledCleared = true;

        return;
    }


    /**
     *  Tests if the enabled flag has been set in this form.
     *
     *  @return {@code true} if the enabled has been set, {@code
     *          false} otherwise
     */

    protected boolean isEnabledSet() {
        return (this.enabledSet);
    }


    /**
     *  Tests if the enabled flag has been cleared in this form.
     *
     *  @return {@code true} if the enabled has been cleared,
     *          {@code false} otherwise
     */

    protected boolean isEnabledCleared() {
        return (this.enabledCleared);
    }


    /**
     *  Returns the current enabled value.
     *
     *  @return the enabled value or {@code null} if {@code
     *          isEnabledSet()} is {@code false}
     */

    protected boolean getEnabled() {
        return (this.enabled);
    }


    /**
     *  Gets the metadata for the enabled.
     *
     *  @return metadata for the enabled 
     */

    @OSID @Override
    public org.osid.Metadata getDisabledMetadata() {
        return (this.enabledMetadata);
    }


    /**
     *  Sets the disabled metadata.
     *
     *  @param metadata the disabled metadata
     *  @throws org.osid.NullArgumentException {@code metadata} is
     *          {@code null}
     */

    protected void setDisabledMetadata(org.osid.Metadata metadata) {
        nullarg(metadata, "disabled metadata");
        this.disabledMetadata = metadata;
        return;
    }

    
    /**
     *  Gets the Id for the disabled field.
     *
     *  @return the disabled field Id
     */

    protected org.osid.id.Id getDisabledId() {
        return (OperableElements.getDisabled());
    }

    
    /**
     *  Sets the disabled flag. 
     *
     *  @param  disabled the new disabled  flag
     *  @throws org.osid.InvalidArgumentException <code> disabled </code> is 
     *          invalid 
     *  @throws org.osid.NoAccessException <code> Metadata.isReadonly() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public void setDisabled(boolean disabled) {
        net.okapia.osid.jamocha.metadata.Validator validator = new net.okapia.osid.jamocha.metadata.Validator();
        validator.validate(disabled, getDisabledMetadata());

        this.disabled = disabled;
        this.disabledSet = true;
        this.disabledCleared = false;
        
        return;
    }


    /**
     *  Clears the disabled flag. 
     *
     *  @throws org.osid.NoAccessException <code> Metadata.isRequired() 
     *          </code> or <code> Metadata.isReadOnly() </code> is <code> true 
     *          </code> 
     */

    @OSID @Override
    public void clearDisabled() {
        if (getDisabledMetadata().isRequired()) {
            throw new org.osid.NoAccessException("disabled is required");
        }

        this.disabledSet = false;
        this.disabledCleared = true;

        return;
    }


    /**
     *  Tests if the disabled flag has been set in this form.
     *
     *  @return {@code true} if the disabled has been set, {@code
     *          false} otherwise
     */

    protected boolean isDisabledSet() {
        return (this.disabledSet);
    }


    /**
     *  Tests if the disabled flag has been cleared in this form.
     *
     *  @return {@code true} if the disabled has been cleared,
     *          {@code false} otherwise
     */

    protected boolean isDisabledCleared() {
        return (this.disabledCleared);
    }


    /**
     *  Returns the current disabled value.
     *
     *  @return the disabled value or {@code null} if {@code
     *          isDisabledSet()} is {@code false}
     */
    
    protected boolean getDisabled() {
        return (this.disabled);
    }
}
