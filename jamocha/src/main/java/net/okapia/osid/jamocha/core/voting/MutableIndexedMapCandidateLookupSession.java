//
// MutableIndexedMapCandidateLookupSession
//
//    Implements a Candidate lookup service backed by a collection of
//    candidates indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Candidate lookup service backed by a collection of
 *  candidates. The candidates are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some candidates may be compatible
 *  with more types than are indicated through these candidate
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of candidates can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapCandidateLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractIndexedMapCandidateLookupSession
    implements org.osid.voting.CandidateLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapCandidateLookupSession} with no candidates.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

      public MutableIndexedMapCandidateLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCandidateLookupSession} with a
     *  single candidate.
     *  
     *  @param polls the polls
     *  @param  candidate a single candidate
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code candidate} is {@code null}
     */

    public MutableIndexedMapCandidateLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Candidate candidate) {
        this(polls);
        putCandidate(candidate);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCandidateLookupSession} using an
     *  array of candidates.
     *
     *  @param polls the polls
     *  @param  candidates an array of candidates
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code candidates} is {@code null}
     */

    public MutableIndexedMapCandidateLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.Candidate[] candidates) {
        this(polls);
        putCandidates(candidates);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapCandidateLookupSession} using a
     *  collection of candidates.
     *
     *  @param polls the polls
     *  @param  candidates a collection of candidates
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code candidates} is {@code null}
     */

    public MutableIndexedMapCandidateLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.Candidate> candidates) {

        this(polls);
        putCandidates(candidates);
        return;
    }
    

    /**
     *  Makes a {@code Candidate} available in this session.
     *
     *  @param  candidate a candidate
     *  @throws org.osid.NullArgumentException {@code candidate{@code  is
     *          {@code null}
     */

    @Override
    public void putCandidate(org.osid.voting.Candidate candidate) {
        super.putCandidate(candidate);
        return;
    }


    /**
     *  Makes an array of candidates available in this session.
     *
     *  @param  candidates an array of candidates
     *  @throws org.osid.NullArgumentException {@code candidates{@code 
     *          is {@code null}
     */

    @Override
    public void putCandidates(org.osid.voting.Candidate[] candidates) {
        super.putCandidates(candidates);
        return;
    }


    /**
     *  Makes collection of candidates available in this session.
     *
     *  @param  candidates a collection of candidates
     *  @throws org.osid.NullArgumentException {@code candidate{@code  is
     *          {@code null}
     */

    @Override
    public void putCandidates(java.util.Collection<? extends org.osid.voting.Candidate> candidates) {
        super.putCandidates(candidates);
        return;
    }


    /**
     *  Removes a Candidate from this session.
     *
     *  @param candidateId the {@code Id} of the candidate
     *  @throws org.osid.NullArgumentException {@code candidateId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCandidate(org.osid.id.Id candidateId) {
        super.removeCandidate(candidateId);
        return;
    }    
}
