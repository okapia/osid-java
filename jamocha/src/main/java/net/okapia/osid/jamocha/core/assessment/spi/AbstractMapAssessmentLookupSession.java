//
// AbstractMapAssessmentLookupSession
//
//    A simple framework for providing an Assessment lookup service
//    backed by a fixed collection of assessments.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Assessment lookup service backed by a
 *  fixed collection of assessments. The assessments are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Assessments</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAssessmentLookupSession
    extends net.okapia.osid.jamocha.assessment.spi.AbstractAssessmentLookupSession
    implements org.osid.assessment.AssessmentLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.assessment.Assessment> assessments = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.assessment.Assessment>());


    /**
     *  Makes an <code>Assessment</code> available in this session.
     *
     *  @param  assessment an assessment
     *  @throws org.osid.NullArgumentException <code>assessment<code>
     *          is <code>null</code>
     */

    protected void putAssessment(org.osid.assessment.Assessment assessment) {
        this.assessments.put(assessment.getId(), assessment);
        return;
    }


    /**
     *  Makes an array of assessments available in this session.
     *
     *  @param  assessments an array of assessments
     *  @throws org.osid.NullArgumentException <code>assessments<code>
     *          is <code>null</code>
     */

    protected void putAssessments(org.osid.assessment.Assessment[] assessments) {
        putAssessments(java.util.Arrays.asList(assessments));
        return;
    }


    /**
     *  Makes a collection of assessments available in this session.
     *
     *  @param  assessments a collection of assessments
     *  @throws org.osid.NullArgumentException <code>assessments<code>
     *          is <code>null</code>
     */

    protected void putAssessments(java.util.Collection<? extends org.osid.assessment.Assessment> assessments) {
        for (org.osid.assessment.Assessment assessment : assessments) {
            this.assessments.put(assessment.getId(), assessment);
        }

        return;
    }


    /**
     *  Removes an Assessment from this session.
     *
     *  @param  assessmentId the <code>Id</code> of the assessment
     *  @throws org.osid.NullArgumentException <code>assessmentId<code> is
     *          <code>null</code>
     */

    protected void removeAssessment(org.osid.id.Id assessmentId) {
        this.assessments.remove(assessmentId);
        return;
    }


    /**
     *  Gets the <code>Assessment</code> specified by its <code>Id</code>.
     *
     *  @param  assessmentId <code>Id</code> of the <code>Assessment</code>
     *  @return the assessment
     *  @throws org.osid.NotFoundException <code>assessmentId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>assessmentId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Assessment getAssessment(org.osid.id.Id assessmentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.assessment.Assessment assessment = this.assessments.get(assessmentId);
        if (assessment == null) {
            throw new org.osid.NotFoundException("assessment not found: " + assessmentId);
        }

        return (assessment);
    }


    /**
     *  Gets all <code>Assessments</code>. In plenary mode, the returned
     *  list contains all known assessments or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Assessments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentList getAssessments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.assessment.assessment.ArrayAssessmentList(this.assessments.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.assessments.clear();
        super.close();
        return;
    }
}
