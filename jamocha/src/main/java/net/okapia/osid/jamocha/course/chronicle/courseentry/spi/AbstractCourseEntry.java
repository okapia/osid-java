//
// AbstractCourseEntry.java
//
//     Defines a CourseEntry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.chronicle.courseentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>CourseEntry</code>.
 */

public abstract class AbstractCourseEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.chronicle.CourseEntry {

    private org.osid.resource.Resource student;
    private org.osid.course.Course course;
    private org.osid.course.Term term;
    private boolean complete = false;

    private org.osid.grading.GradeSystem creditScale;
    private java.math.BigDecimal creditsEarned;
    private org.osid.grading.Grade grade;
    private org.osid.grading.GradeSystem scoreScale;
    private java.math.BigDecimal score;

    private boolean hasRegistrations = false;
    private final java.util.Collection<org.osid.course.registration.Registration> registrations = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.course.chronicle.records.CourseEntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.student.getId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.student);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException
     *          <code>student</code> is <code>null</code>
     */

    protected void setStudent(org.osid.resource.Resource student) {
        nullarg(student, "student");
        this.student = student;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Course. </code> 
     *
     *  @return the course <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCourseId() {
        return (this.course.getId());
    }


    /**
     *  Gets the <code> Course. </code> 
     *
     *  @return the course 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Course getCourse()
        throws org.osid.OperationFailedException {

        return (this.course);
    }


    /**
     *  Sets the course.
     *
     *  @param course a course
     *  @throws org.osid.NullArgumentException <code>course</code> is
     *          <code>null</code>
     */

    protected void setCourse(org.osid.course.Course course) {
        nullarg(course, "course");
        this.course = course;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Term. </code> 
     *
     *  @return the term <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTermId() {
        return (this.term.getId());
    }


    /**
     *  Gets the <code> Term. </code> 
     *
     *  @return the term 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Term getTerm()
        throws org.osid.OperationFailedException {

        return (this.term);
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    protected void setTerm(org.osid.course.Term term) {
        nullarg(term, "term");
        this.term = term;
        return;
    }


    /**
     *  Tests if the course has been completed. 
     *
     *  @return <code> true </code> if the course has been completed,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isComplete() {
        return (this.complete);
    }


    /**
     *  Sets the complete flag.
     *
     *  @param complete <code> true </code> if the course has been
     *          completed, <code> false </code> otherwise
     */

    protected void setComplete(boolean complete) {
        this.complete = complete;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCreditScaleId() {
        return (this.creditScale.getId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getCreditScale()
        throws org.osid.OperationFailedException {

        return (this.creditScale);
    }


    /**
     *  Sets the credit scale.
     *
     *  @param creditScale a credit scale
     *  @throws org.osid.NullArgumentException
     *          <code>creditScale</code> is <code>null</code>
     */

    protected void setCreditScale(org.osid.grading.GradeSystem creditScale) {
        nullarg(creditScale, "credit scale");
        this.creditScale = creditScale;
        return;
    }


    /**
     *  Gets the number of credits earned in this course. 
     *
     *  @return the credits earned 
     */

    @OSID @Override
    public java.math.BigDecimal getCreditsEarned() {
        return (this.creditsEarned);
    }


    /**
     *  Sets the credits earned.
     *
     *  @param credits a credits earned
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    protected void setCreditsEarned(java.math.BigDecimal credits) {
        nullarg(credits, "credits");
        this.creditsEarned = credits;
        return;
    }


    /**
     *  Tests if a grade is available. 
     *
     *  @return <code> true </code> if a grade is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean hasGrade() {
        return (this.grade != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Grade. </code> 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasGrade() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeId() {
        if (!hasGrade()) {
            throw new org.osid.IllegalStateException("hasGrade() is false");
        }

        return (this.grade.getId());
    }


    /**
     *  Gets the <code> Grade. </code> 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasGrade()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getGrade()
        throws org.osid.OperationFailedException {

        if (!hasGrade()) {
            throw new org.osid.IllegalStateException("hasGrade() is false");
        }

        return (this.grade);
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException <code>grade</code> is
     *          <code>null</code>
     */

    protected void setGrade(org.osid.grading.Grade grade) {
        nullarg(grade, "grade");
        this.grade = grade;
        return;
    }


    /**
     *  Tests if a score is available. 
     *
     *  @return <code> true </code> if a score is available, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean hasScore() {
        return ((this.scoreScale != null) && (this.score != null));
    }


    /**
     *  Gets the <code> Id </code> of the <code> GradeSystem. </code> 
     *
     *  @return the grade system <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getScoreScaleId() {
        if (!hasScore()) {
            throw new org.osid.IllegalStateException("hasScore() is false");
        }

        return (this.scoreScale.getId());
    }


    /**
     *  Gets the <code> GradeSystem. </code> 
     *
     *  @return the grade system 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getScoreScale()
        throws org.osid.OperationFailedException {

        if (!hasScore()) {
            throw new org.osid.IllegalStateException("hasScore() is false");
        }

        return (this.scoreScale);
    }


    /**
     *  Sets the score scale.
     *
     *  @param scoreScale a score scale
     *  @throws org.osid.NullArgumentException <code>scoreScale</code>
     *          is <code>null</code>
     */

    protected void setScoreScale(org.osid.grading.GradeSystem scoreScale) {
        nullarg(scoreScale, "score scale");
        this.scoreScale = scoreScale;
        return;
    }


    /**
     *  Gets the cumulative score. 
     *
     *  @return the score 
     *  @throws org.osid.IllegalStateException <code> hasScore() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getScore() {
        if (!hasScore()) {
            throw new org.osid.IllegalStateException("hasScore() is false");
        }

        return (this.score);
    }


    /**
     *  Sets the score.
     *
     *  @param score a score
     *  @throws org.osid.NullArgumentException <code>score</code> is
     *          <code>null</code>
     */

    protected void setScore(java.math.BigDecimal score) {
        nullarg(score, "score");
        this.score = score;
        return;
    }


    /**
     *  Tests if <code> Registrations </code> are available. 
     *
     *  @return <code> true </code> if registrations are available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasRegistrations() {
        return (this.hasRegistrations);
    }


    /**
     *  Gets the <code> Ids </code> of the <code> Registrations. </code> 
     *
     *  @return the registrations <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasRegistrations() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getRegistrationIds() {
        if (!hasRegistrations()) {
            throw new org.osid.IllegalStateException("hasRegistrations() is false");
        }

        try {
            org.osid.course.registration.RegistrationList registrations = getRegistrations();
            return (new net.okapia.osid.jamocha.adapter.converter.course.registration.registration.RegistrationToIdList(registrations));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> Registrations. </code> 
     *
     *  @return the registrations 
     *  @throws org.osid.IllegalStateException <code> hasRegistrations() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationList getRegistrations()
        throws org.osid.OperationFailedException {

        if (!hasRegistrations()) {
            throw new org.osid.IllegalStateException("hasRegistrations() is false");
        }

        return (new net.okapia.osid.jamocha.course.registration.registration.ArrayRegistrationList(this.registrations));
    }


    /**
     *  Adds a registration.
     *
     *  @param registration a registration
     *  @throws org.osid.NullArgumentException
     *          <code>registration</code> is <code>null</code>
     */

    protected void addRegistration(org.osid.course.registration.Registration registration) {
        nullarg(registration, "registration");

        this.registrations.add(registration);
        this.hasRegistrations = true;

        return;
    }


    /**
     *  Sets all the registrations.
     *
     *  @param registrations a collection of registrations
     *  @throws org.osid.NullArgumentException
     *          <code>registrations</code> is <code>null</code>
     */

    protected void setRegistrations(java.util.Collection<org.osid.course.registration.Registration> registrations) {
        nullarg(registrations, "registrations");

        this.registrations.clear();
        this.registrations.addAll(registrations);
        this.hasRegistrations = true;

        return;
    }

    /**
     *  Tests if this courseEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  courseEntryRecordType a course entry record type 
     *  @return <code>true</code> if the courseEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type courseEntryRecordType) {
        for (org.osid.course.chronicle.records.CourseEntryRecord record : this.records) {
            if (record.implementsRecordType(courseEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>CourseEntry</code> record <code>Type</code>.
     *
     *  @param  courseEntryRecordType the course entry record type 
     *  @return the course entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(courseEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CourseEntryRecord getCourseEntryRecord(org.osid.type.Type courseEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.chronicle.records.CourseEntryRecord record : this.records) {
            if (record.implementsRecordType(courseEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(courseEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this course entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param courseEntryRecord the course entry record
     *  @param courseEntryRecordType course entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>courseEntryRecord</code> or
     *          <code>courseEntryRecordTypecourseEntry</code> is
     *          <code>null</code>
     */
            
    protected void addCourseEntryRecord(org.osid.course.chronicle.records.CourseEntryRecord courseEntryRecord, 
                                        org.osid.type.Type courseEntryRecordType) {

        nullarg(courseEntryRecord, "course entry record");
        addRecordType(courseEntryRecordType);
        this.records.add(courseEntryRecord);
        
        return;
    }
}
