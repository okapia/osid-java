//
// AbstractChainQuery.java
//
//     A template for making a Chain Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.sequencing.chain.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for chains.
 */

public abstract class AbstractChainQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.sequencing.ChainQuery {

    private final java.util.Collection<org.osid.sequencing.records.ChainQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches fifo chains. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchFifo(boolean match) {
        return;
    }


    /**
     *  Clears the fifo query terms. 
     */

    @OSID @Override
    public void clearFifoTerms() {
        return;
    }


    /**
     *  Sets the element <code> Id </code> for this query. 
     *
     *  @param  elementId the element <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> elementId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchElement(org.osid.id.Id elementId, boolean match) {
        return;
    }


    /**
     *  Matches chains with any element. 
     *
     *  @param match <code> true </code> to match chains with any
     *          element, <code>false</code> to match chains with no
     *          elements
     */

    @OSID @Override
    public void matchAnyElement(boolean match) {
        return;
    }


    /**
     *  Clears the element query terms. 
     */

    @OSID @Override
    public void clearElementTerms() {
        return;
    }


    /**
     *  Sets the action group <code> Id </code> for this query to match 
     *  sequencinglers assigned to action groups. 
     *
     *  @param  actionGroupId the action group <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> actionGroupId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAntimatroidId(org.osid.id.Id actionGroupId, boolean match) {
        return;
    }


    /**
     *  Clears the antimatroid <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAntimatroidIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AntimatroidQuery </code> is available. 
     *
     *  @return <code> true </code> if an antimatroid query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAntimatroidQuery() {
        return (false);
    }


    /**
     *  Gets the query for an antimatroid. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the antimatroid query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAntimatroidQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.sequencing.AntimatroidQuery getAntimatroidQuery() {
        throw new org.osid.UnimplementedException("supportsAntimatroidQuery() is false");
    }


    /**
     *  Clears the antimatroid query terms. 
     */

    @OSID @Override
    public void clearAntimatroidTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given chain query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a chain implementing the requested record.
     *
     *  @param chainRecordType a chain record type
     *  @return the chain query record
     *  @throws org.osid.NullArgumentException
     *          <code>chainRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(chainRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.sequencing.records.ChainQueryRecord getChainQueryRecord(org.osid.type.Type chainRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.sequencing.records.ChainQueryRecord record : this.records) {
            if (record.implementsRecordType(chainRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(chainRecordType + " is not supported");
    }


    /**
     *  Adds a record to this chain query. 
     *
     *  @param chainQueryRecord chain query record
     *  @param chainRecordType chain record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addChainQueryRecord(org.osid.sequencing.records.ChainQueryRecord chainQueryRecord, 
                                          org.osid.type.Type chainRecordType) {

        addRecordType(chainRecordType);
        nullarg(chainQueryRecord, "chain query record");
        this.records.add(chainQueryRecord);        
        return;
    }
}
