//
// AbstractMapStepLookupSession
//
//    A simple framework for providing a Step lookup service
//    backed by a fixed collection of steps.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.workflow.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Step lookup service backed by a
 *  fixed collection of steps. The steps are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Steps</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapStepLookupSession
    extends net.okapia.osid.jamocha.workflow.spi.AbstractStepLookupSession
    implements org.osid.workflow.StepLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.workflow.Step> steps = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.workflow.Step>());


    /**
     *  Makes a <code>Step</code> available in this session.
     *
     *  @param  step a step
     *  @throws org.osid.NullArgumentException <code>step<code>
     *          is <code>null</code>
     */

    protected void putStep(org.osid.workflow.Step step) {
        this.steps.put(step.getId(), step);
        return;
    }


    /**
     *  Makes an array of steps available in this session.
     *
     *  @param  steps an array of steps
     *  @throws org.osid.NullArgumentException <code>steps<code>
     *          is <code>null</code>
     */

    protected void putSteps(org.osid.workflow.Step[] steps) {
        putSteps(java.util.Arrays.asList(steps));
        return;
    }


    /**
     *  Makes a collection of steps available in this session.
     *
     *  @param  steps a collection of steps
     *  @throws org.osid.NullArgumentException <code>steps<code>
     *          is <code>null</code>
     */

    protected void putSteps(java.util.Collection<? extends org.osid.workflow.Step> steps) {
        for (org.osid.workflow.Step step : steps) {
            this.steps.put(step.getId(), step);
        }

        return;
    }


    /**
     *  Removes a Step from this session.
     *
     *  @param  stepId the <code>Id</code> of the step
     *  @throws org.osid.NullArgumentException <code>stepId<code> is
     *          <code>null</code>
     */

    protected void removeStep(org.osid.id.Id stepId) {
        this.steps.remove(stepId);
        return;
    }


    /**
     *  Gets the <code>Step</code> specified by its <code>Id</code>.
     *
     *  @param  stepId <code>Id</code> of the <code>Step</code>
     *  @return the step
     *  @throws org.osid.NotFoundException <code>stepId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>stepId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Step getStep(org.osid.id.Id stepId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.workflow.Step step = this.steps.get(stepId);
        if (step == null) {
            throw new org.osid.NotFoundException("step not found: " + stepId);
        }

        return (step);
    }


    /**
     *  Gets all <code>Steps</code>. In plenary mode, the returned
     *  list contains all known steps or an error
     *  results. Otherwise, the returned list may contain only those
     *  steps that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Steps</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.StepList getSteps()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.workflow.step.ArrayStepList(this.steps.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.steps.clear();
        super.close();
        return;
    }
}
