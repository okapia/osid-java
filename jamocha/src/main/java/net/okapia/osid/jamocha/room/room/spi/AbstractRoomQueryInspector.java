//
// AbstractRoomQueryInspector.java
//
//     A template for making a RoomQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for rooms.
 */

public abstract class AbstractRoomQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQueryInspector
    implements org.osid.room.RoomQueryInspector {

    private final java.util.Collection<org.osid.room.records.RoomQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the building <code> Id </code> terms. 
     *
     *  @return the building <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBuildingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the building terms. 
     *
     *  @return the building terms 
     */

    @OSID @Override
    public org.osid.room.BuildingQueryInspector[] getBuildingTerms() {
        return (new org.osid.room.BuildingQueryInspector[0]);
    }


    /**
     *  Gets the floor <code> Id </code> terms. 
     *
     *  @return the floor <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFloorIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the floor terms. 
     *
     *  @return the floor terms 
     */

    @OSID @Override
    public org.osid.room.FloorQueryInspector[] getFloorTerms() {
        return (new org.osid.room.FloorQueryInspector[0]);
    }


    /**
     *  Gets the enclosing room <code> Id </code> terms. 
     *
     *  @return the enclosing room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getEnclosingRoomIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the enclosing room terms. 
     *
     *  @return the enclosing room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getEnclosingRoomTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the subdivision room <code> Id </code> terms. 
     *
     *  @return the subdivision room <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSubdivisionIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the subdivision room terms. 
     *
     *  @return the subdivision room terms 
     */

    @OSID @Override
    public org.osid.room.RoomQueryInspector[] getSubdivisionTerms() {
        return (new org.osid.room.RoomQueryInspector[0]);
    }


    /**
     *  Gets the offical name terms. 
     *
     *  @return the name terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDesignatedNameTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the room building number terms. 
     *
     *  @return the room number terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getRoomNumberTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the room code number terms. 
     *
     *  @return the room code terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getCodeTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the area terms. 
     *
     *  @return the area terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getAreaTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the occupancy limit terms. 
     *
     *  @return the occupancy limit terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getOccupancyLimitTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the resource <code> Id </code> terms. 
     *
     *  @return the resource <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getResourceIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the resource terms. 
     *
     *  @return the resource terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getResourceTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the campus <code> Id </code> terms. 
     *
     *  @return the campus <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCampusIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the campus terms. 
     *
     *  @return the campus terms 
     */

    @OSID @Override
    public org.osid.room.CampusQueryInspector[] getCampusTerms() {
        return (new org.osid.room.CampusQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given room query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a room implementing the requested record.
     *
     *  @param roomRecordType a room record type
     *  @return the room query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(roomRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.RoomQueryInspectorRecord getRoomQueryInspectorRecord(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.RoomQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(roomRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(roomRecordType + " is not supported");
    }


    /**
     *  Adds a record to this room query. 
     *
     *  @param roomQueryInspectorRecord room query inspector
     *         record
     *  @param roomRecordType room record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRoomQueryInspectorRecord(org.osid.room.records.RoomQueryInspectorRecord roomQueryInspectorRecord, 
                                                   org.osid.type.Type roomRecordType) {

        addRecordType(roomRecordType);
        nullarg(roomRecordType, "room record type");
        this.records.add(roomQueryInspectorRecord);        
        return;
    }
}
