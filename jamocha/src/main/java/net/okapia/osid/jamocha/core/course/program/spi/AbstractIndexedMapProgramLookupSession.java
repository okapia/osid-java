//
// AbstractIndexedMapProgramLookupSession.java
//
//    A simple framework for providing a Program lookup service
//    backed by a fixed collection of programs with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Program lookup service backed by a
 *  fixed collection of programs. The programs are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some programs may be compatible
 *  with more types than are indicated through these program
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Programs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProgramLookupSession
    extends AbstractMapProgramLookupSession
    implements org.osid.course.program.ProgramLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.course.program.Program> programsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.program.Program>());
    private final MultiMap<org.osid.type.Type, org.osid.course.program.Program> programsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.course.program.Program>());


    /**
     *  Makes a <code>Program</code> available in this session.
     *
     *  @param  program a program
     *  @throws org.osid.NullArgumentException <code>program<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProgram(org.osid.course.program.Program program) {
        super.putProgram(program);

        this.programsByGenus.put(program.getGenusType(), program);
        
        try (org.osid.type.TypeList types = program.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.programsByRecord.put(types.getNextType(), program);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a program from this session.
     *
     *  @param programId the <code>Id</code> of the program
     *  @throws org.osid.NullArgumentException <code>programId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProgram(org.osid.id.Id programId) {
        org.osid.course.program.Program program;
        try {
            program = getProgram(programId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.programsByGenus.remove(program.getGenusType());

        try (org.osid.type.TypeList types = program.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.programsByRecord.remove(types.getNextType(), program);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProgram(programId);
        return;
    }


    /**
     *  Gets a <code>ProgramList</code> corresponding to the given
     *  program genus <code>Type</code> which does not include
     *  programs of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known programs or an error results. Otherwise,
     *  the returned list may contain only those programs that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  programGenusType a program genus type 
     *  @return the returned <code>Program</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByGenusType(org.osid.type.Type programGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.program.ArrayProgramList(this.programsByGenus.get(programGenusType)));
    }


    /**
     *  Gets a <code>ProgramList</code> containing the given
     *  program record <code>Type</code>. In plenary mode, the
     *  returned list contains all known programs or an error
     *  results. Otherwise, the returned list may contain only those
     *  programs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  programRecordType a program record type 
     *  @return the returned <code>program</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>programRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.program.ProgramList getProgramsByRecordType(org.osid.type.Type programRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.program.program.ArrayProgramList(this.programsByRecord.get(programRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.programsByGenus.clear();
        this.programsByRecord.clear();

        super.close();

        return;
    }
}
