//
// AbstractGradebookColumnSearch.java
//
//     A template for making a GradebookColumn Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebookcolumn.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing gradebook column searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractGradebookColumnSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.grading.GradebookColumnSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.grading.records.GradebookColumnSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.grading.GradebookColumnSearchOrder gradebookColumnSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of gradebook columns. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  gradebookColumnIds list of gradebook columns
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongGradebookColumns(org.osid.id.IdList gradebookColumnIds) {
        while (gradebookColumnIds.hasNext()) {
            try {
                this.ids.add(gradebookColumnIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongGradebookColumns</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of gradebook column Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getGradebookColumnIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  gradebookColumnSearchOrder gradebook column search order 
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>gradebookColumnSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderGradebookColumnResults(org.osid.grading.GradebookColumnSearchOrder gradebookColumnSearchOrder) {
	this.gradebookColumnSearchOrder = gradebookColumnSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.grading.GradebookColumnSearchOrder getGradebookColumnSearchOrder() {
	return (this.gradebookColumnSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given gradebook column search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a gradebook column implementing the requested record.
     *
     *  @param gradebookColumnSearchRecordType a gradebook column search record
     *         type
     *  @return the gradebook column search record
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookColumnSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradebookColumnSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookColumnSearchRecord getGradebookColumnSearchRecord(org.osid.type.Type gradebookColumnSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.grading.records.GradebookColumnSearchRecord record : this.records) {
            if (record.implementsRecordType(gradebookColumnSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookColumnSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this gradebook column search. 
     *
     *  @param gradebookColumnSearchRecord gradebook column search record
     *  @param gradebookColumnSearchRecordType gradebookColumn search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradebookColumnSearchRecord(org.osid.grading.records.GradebookColumnSearchRecord gradebookColumnSearchRecord, 
                                           org.osid.type.Type gradebookColumnSearchRecordType) {

        addRecordType(gradebookColumnSearchRecordType);
        this.records.add(gradebookColumnSearchRecord);        
        return;
    }
}
