//
// AbstractIndexedMapDictionaryLookupSession.java
//
//    A simple framework for providing a Dictionary lookup service
//    backed by a fixed collection of dictionaries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.dictionary.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Dictionary lookup service backed by a
 *  fixed collection of dictionaries. The dictionaries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some dictionaries may be compatible
 *  with more types than are indicated through these dictionary
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Dictionaries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDictionaryLookupSession
    extends AbstractMapDictionaryLookupSession
    implements org.osid.dictionary.DictionaryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.dictionary.Dictionary> dictionariesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.dictionary.Dictionary>());
    private final MultiMap<org.osid.type.Type, org.osid.dictionary.Dictionary> dictionariesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.dictionary.Dictionary>());


    /**
     *  Makes a <code>Dictionary</code> available in this session.
     *
     *  @param  dictionary a dictionary
     *  @throws org.osid.NullArgumentException <code>dictionary<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDictionary(org.osid.dictionary.Dictionary dictionary) {
        super.putDictionary(dictionary);

        this.dictionariesByGenus.put(dictionary.getGenusType(), dictionary);
        
        try (org.osid.type.TypeList types = dictionary.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.dictionariesByRecord.put(types.getNextType(), dictionary);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a dictionary from this session.
     *
     *  @param dictionaryId the <code>Id</code> of the dictionary
     *  @throws org.osid.NullArgumentException <code>dictionaryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDictionary(org.osid.id.Id dictionaryId) {
        org.osid.dictionary.Dictionary dictionary;
        try {
            dictionary = getDictionary(dictionaryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.dictionariesByGenus.remove(dictionary.getGenusType());

        try (org.osid.type.TypeList types = dictionary.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.dictionariesByRecord.remove(types.getNextType(), dictionary);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDictionary(dictionaryId);
        return;
    }


    /**
     *  Gets a <code>DictionaryList</code> corresponding to the given
     *  dictionary genus <code>Type</code> which does not include
     *  dictionaries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known dictionaries or an error
     *  results. Otherwise, the returned list may contain only those
     *  dictionaries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  dictionaryGenusType a dictionary genus type 
     *  @return the returned <code>Dictionary</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByGenusType(org.osid.type.Type dictionaryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.dictionary.dictionary.ArrayDictionaryList(this.dictionariesByGenus.get(dictionaryGenusType)));
    }


    /**
     *  Gets a <code>DictionaryList</code> containing the given
     *  dictionary record <code>Type</code>. In plenary mode, the
     *  returned list contains all known dictionaries or an error
     *  results. Otherwise, the returned list may contain only those
     *  dictionaries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  dictionaryRecordType a dictionary record type 
     *  @return the returned <code>dictionary</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dictionaryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.dictionary.DictionaryList getDictionariesByRecordType(org.osid.type.Type dictionaryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.dictionary.dictionary.ArrayDictionaryList(this.dictionariesByRecord.get(dictionaryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.dictionariesByGenus.clear();
        this.dictionariesByRecord.clear();

        super.close();

        return;
    }
}
