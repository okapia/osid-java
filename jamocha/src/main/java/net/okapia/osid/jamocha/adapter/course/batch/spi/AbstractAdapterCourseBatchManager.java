//
// AbstractCourseBatchManager.java
//
//     An adapter for a CourseBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.course.batch.CourseBatchManager>
    implements org.osid.course.batch.CourseBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk administration of courses is available. 
     *
     *  @return <code> true </code> if a course bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseBatchAdmin() {
        return (getAdapteeManager().supportsCourseBatchAdmin());
    }


    /**
     *  Tests if bulk administration of activity units is available. 
     *
     *  @return <code> true </code> if an activity unit bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitBatchAdmin() {
        return (getAdapteeManager().supportsActivityUnitBatchAdmin());
    }


    /**
     *  Tests if bulk administration of course offerings is available. 
     *
     *  @return <code> true </code> if a course offering bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingBatchAdmin() {
        return (getAdapteeManager().supportsCourseOfferingBatchAdmin());
    }


    /**
     *  Tests if bulk administration of activities is available. 
     *
     *  @return <code> true </code> if an activity bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityBatchAdmin() {
        return (getAdapteeManager().supportsActivityBatchAdmin());
    }


    /**
     *  Tests if bulk administration of terms is available. 
     *
     *  @return <code> true </code> if a term bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermBatchAdmin() {
        return (getAdapteeManager().supportsTermBatchAdmin());
    }


    /**
     *  Tests if bulk administration of course catalogs is available. 
     *
     *  @return <code> true </code> if a course catalog bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogBatchAdmin() {
        return (getAdapteeManager().supportsCourseCatalogBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk course 
     *  administration service. 
     *
     *  @return a <code> CourseBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.CourseBatchAdminSession getCourseBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk course 
     *  administration service for the given course catalog 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.CourseBatchAdminSession getCourseBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  unit administration service. 
     *
     *  @return an <code> ActivityUnitBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.batch.ActivityUnitBatchAdminSession getActivityUnitBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  unit administration service for the given course catalog 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.ActivityUnitBatchAdminSession getActivityUnitBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk course 
     *  offering administration service. 
     *
     *  @return a <code> CourseOfferingBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.batch.CourseOfferingBatchAdminSession getCourseOfferingBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk course 
     *  offering administration service for the given course catalog 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.CourseOfferingBatchAdminSession getCourseOfferingBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  administration service. 
     *
     *  @return an <code> ActivityBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.ActivityBatchAdminSession getActivityBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk activity 
     *  administration service for the given course catalog 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.ActivityBatchAdminSession getActivityBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk term 
     *  administration service. 
     *
     *  @return a <code> TermBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.TermBatchAdminSession getTermBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk term 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.TermBatchAdminSession getTermBatchAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTermBatchAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk course 
     *  catalog administration service. 
     *
     *  @return a <code> CourseCatalogBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.batch.CourseCatalogBatchAdminSession getCourseCatalogBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCatalogBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
