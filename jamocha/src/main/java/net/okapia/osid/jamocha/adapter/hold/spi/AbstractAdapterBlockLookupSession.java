//
// AbstractAdapterBlockLookupSession.java
//
//    A Block lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.hold.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Block lookup session adapter.
 */

public abstract class AbstractAdapterBlockLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.hold.BlockLookupSession {

    private final org.osid.hold.BlockLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterBlockLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterBlockLookupSession(org.osid.hold.BlockLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Oubliette/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Oubliette Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.session.getOublietteId());
    }


    /**
     *  Gets the {@code Oubliette} associated with this session.
     *
     *  @return the {@code Oubliette} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOubliette());
    }


    /**
     *  Tests if this user can perform {@code Block} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupBlocks() {
        return (this.session.canLookupBlocks());
    }


    /**
     *  A complete view of the {@code Block} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBlockView() {
        this.session.useComparativeBlockView();
        return;
    }


    /**
     *  A complete view of the {@code Block} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBlockView() {
        this.session.usePlenaryBlockView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include blocks in oubliettes which are children
     *  of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.session.useFederatedOublietteView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        this.session.useIsolatedOublietteView();
        return;
    }
    
     
    /**
     *  Gets the {@code Block} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Block} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Block} and
     *  retained for compatibility.
     *
     *  @param blockId {@code Id} of the {@code Block}
     *  @return the block
     *  @throws org.osid.NotFoundException {@code blockId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code blockId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Block getBlock(org.osid.id.Id blockId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlock(blockId));
    }


    /**
     *  Gets a {@code BlockList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  blocks specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Blocks} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  blockIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Block} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code blockIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByIds(org.osid.id.IdList blockIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlocksByIds(blockIds));
    }


    /**
     *  Gets a {@code BlockList} corresponding to the given
     *  block genus {@code Type} which does not include
     *  blocks of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blockGenusType a block genus type 
     *  @return the returned {@code Block} list
     *  @throws org.osid.NullArgumentException
     *          {@code blockGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByGenusType(org.osid.type.Type blockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlocksByGenusType(blockGenusType));
    }


    /**
     *  Gets a {@code BlockList} corresponding to the given
     *  block genus {@code Type} and include any additional
     *  blocks with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blockGenusType a block genus type 
     *  @return the returned {@code Block} list
     *  @throws org.osid.NullArgumentException
     *          {@code blockGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByParentGenusType(org.osid.type.Type blockGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlocksByParentGenusType(blockGenusType));
    }


    /**
     *  Gets a {@code BlockList} containing the given
     *  block record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  blockRecordType a block record type 
     *  @return the returned {@code Block} list
     *  @throws org.osid.NullArgumentException
     *          {@code blockRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksByRecordType(org.osid.type.Type blockRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlocksByRecordType(blockRecordType));
    }


    /**
     *  Gets a {@code BlockList} mapped to the given {@code Issue} In
     *  plenary mode, the returned list contains all known blocks or
     *  an error results. Otherwise, the returned list may contain
     *  only those blocks that are accessible through this session.
     *
     *  @param  issueId an issue {@code Id} 
     *  @return the returned {@code Block} list 
     *  @throws org.osid.NullArgumentException {@code issueId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocksForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlocksForIssue(issueId));
    }


    /**
     *  Gets all {@code Blocks}. 
     *
     *  In plenary mode, the returned list contains all known
     *  blocks or an error results. Otherwise, the returned list
     *  may contain only those blocks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Blocks} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.BlockList getBlocks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getBlocks());
    }
}
