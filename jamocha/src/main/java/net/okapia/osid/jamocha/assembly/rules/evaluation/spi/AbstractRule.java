//
// AbstractRule.java
//
//    An abstract class for an evaluating rule.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.rules.evaluation.spi;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract class for an evaluating rule.
 */

public abstract class AbstractRule
    extends net.okapia.osid.jamocha.rules.rule.spi.AbstractRule
    implements org.osid.rules.Rule,
               net.okapia.osid.jamocha.assembly.rules.evaluation.EvaluatingRule {

    private net.okapia.osid.jamocha.assembly.rules.evaluation.Term term;


    /**
     *  Sets the root term for this rule.
     *
     *  @param term the root term
     *  @throws org.osid.NullArgumentException <code>term</code>
     *          is <code>null</code>
     */

    protected void setTerm(net.okapia.osid.jamocha.assembly.rules.evaluation.Term term) {
        nullarg(term, "term");
        this.term = term;
        return;
    }

    
    /**
     *  Evaluates this rule.
     *
     *  @param condition
     *  @return result of evaluation
     *  @throws org.osid.NullArgumentException <code>condition</code>
     *          is <code>null</code>
     */

    @Override
    public boolean eval(org.osid.rules.Condition condition) {
        if (this.term == null) {
            return (false);
        }

        return (this.term.eval(condition));
    }
}


