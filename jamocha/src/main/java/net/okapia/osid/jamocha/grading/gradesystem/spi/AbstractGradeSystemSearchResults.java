//
// AbstractGradeSystemSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradesystem.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractGradeSystemSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.grading.GradeSystemSearchResults {

    private org.osid.grading.GradeSystemList gradeSystems;
    private final org.osid.grading.GradeSystemQueryInspector inspector;
    private final java.util.Collection<org.osid.grading.records.GradeSystemSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractGradeSystemSearchResults.
     *
     *  @param gradeSystems the result set
     *  @param gradeSystemQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>gradeSystems</code>
     *          or <code>gradeSystemQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractGradeSystemSearchResults(org.osid.grading.GradeSystemList gradeSystems,
                                            org.osid.grading.GradeSystemQueryInspector gradeSystemQueryInspector) {
        nullarg(gradeSystems, "grade systems");
        nullarg(gradeSystemQueryInspector, "grade system query inspectpr");

        this.gradeSystems = gradeSystems;
        this.inspector = gradeSystemQueryInspector;

        return;
    }


    /**
     *  Gets the grade system list resulting from a search.
     *
     *  @return a grade system list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getGradeSystems() {
        if (this.gradeSystems == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.grading.GradeSystemList gradeSystems = this.gradeSystems;
        this.gradeSystems = null;
	return (gradeSystems);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.grading.GradeSystemQueryInspector getGradeSystemQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  grade system search record <code> Type. </code> This method must
     *  be used to retrieve a gradeSystem implementing the requested
     *  record.
     *
     *  @param gradeSystemSearchRecordType a gradeSystem search 
     *         record type 
     *  @return the grade system search
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(gradeSystemSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeSystemSearchResultsRecord getGradeSystemSearchResultsRecord(org.osid.type.Type gradeSystemSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.grading.records.GradeSystemSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(gradeSystemSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record grade system search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addGradeSystemRecord(org.osid.grading.records.GradeSystemSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "grade system record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
