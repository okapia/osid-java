//
// AbstractAdapterCookbookLookupSession.java
//
//    A Cookbook lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recipe.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Cookbook lookup session adapter.
 */

public abstract class AbstractAdapterCookbookLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recipe.CookbookLookupSession {

    private final org.osid.recipe.CookbookLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCookbookLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCookbookLookupSession(org.osid.recipe.CookbookLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Cookbook} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCookbooks() {
        return (this.session.canLookupCookbooks());
    }


    /**
     *  A complete view of the {@code Cookbook} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCookbookView() {
        this.session.useComparativeCookbookView();
        return;
    }


    /**
     *  A complete view of the {@code Cookbook} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCookbookView() {
        this.session.usePlenaryCookbookView();
        return;
    }

     
    /**
     *  Gets the {@code Cookbook} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Cookbook} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Cookbook} and
     *  retained for compatibility.
     *
     *  @param cookbookId {@code Id} of the {@code Cookbook}
     *  @return the cookbook
     *  @throws org.osid.NotFoundException {@code cookbookId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code cookbookId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.Cookbook getCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCookbook(cookbookId));
    }


    /**
     *  Gets a {@code CookbookList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cookbooks specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Cookbooks} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  cookbookIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Cookbook} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code cookbookIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByIds(org.osid.id.IdList cookbookIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCookbooksByIds(cookbookIds));
    }


    /**
     *  Gets a {@code CookbookList} corresponding to the given
     *  cookbook genus {@code Type} which does not include
     *  cookbooks of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  cookbooks or an error results. Otherwise, the returned list
     *  may contain only those cookbooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cookbookGenusType a cookbook genus type 
     *  @return the returned {@code Cookbook} list
     *  @throws org.osid.NullArgumentException
     *          {@code cookbookGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByGenusType(org.osid.type.Type cookbookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCookbooksByGenusType(cookbookGenusType));
    }


    /**
     *  Gets a {@code CookbookList} corresponding to the given
     *  cookbook genus {@code Type} and include any additional
     *  cookbooks with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  cookbooks or an error results. Otherwise, the returned list
     *  may contain only those cookbooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cookbookGenusType a cookbook genus type 
     *  @return the returned {@code Cookbook} list
     *  @throws org.osid.NullArgumentException
     *          {@code cookbookGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByParentGenusType(org.osid.type.Type cookbookGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCookbooksByParentGenusType(cookbookGenusType));
    }


    /**
     *  Gets a {@code CookbookList} containing the given
     *  cookbook record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  cookbooks or an error results. Otherwise, the returned list
     *  may contain only those cookbooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cookbookRecordType a cookbook record type 
     *  @return the returned {@code Cookbook} list
     *  @throws org.osid.NullArgumentException
     *          {@code cookbookRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByRecordType(org.osid.type.Type cookbookRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCookbooksByRecordType(cookbookRecordType));
    }


    /**
     *  Gets a {@code CookbookList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  cookbooks or an error results. Otherwise, the returned list
     *  may contain only those cookbooks that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Cookbook} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCookbooksByProvider(resourceId));
    }


    /**
     *  Gets all {@code Cookbooks}. 
     *
     *  In plenary mode, the returned list contains all known
     *  cookbooks or an error results. Otherwise, the returned list
     *  may contain only those cookbooks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Cookbooks} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCookbooks());
    }
}
