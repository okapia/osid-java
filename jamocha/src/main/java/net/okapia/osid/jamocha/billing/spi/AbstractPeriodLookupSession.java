//
// AbstractPeriodLookupSession.java
//
//    A starter implementation framework for providing a Period
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Period
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPeriods(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPeriodLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.billing.PeriodLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.billing.Business business = new net.okapia.osid.jamocha.nil.billing.business.UnknownBusiness();
    

    /**
     *  Gets the <code>Business/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.business.getId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.business);
    }


    /**
     *  Sets the <code>Business</code>.
     *
     *  @param  business the business for this session
     *  @throws org.osid.NullArgumentException <code>business</code>
     *          is <code>null</code>
     */

    protected void setBusiness(org.osid.billing.Business business) {
        nullarg(business, "business");
        this.business = business;
        return;
    }

    /**
     *  Tests if this user can perform <code>Period</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPeriods() {
        return (true);
    }


    /**
     *  A complete view of the <code>Period</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePeriodView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Period</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPeriodView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include periods in businesses which are
     *  children of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>Period</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Period</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Period</code> and
     *  retained for compatibility.
     *
     *  @param  periodId <code>Id</code> of the
     *          <code>Period</code>
     *  @return the period
     *  @throws org.osid.NotFoundException <code>periodId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>periodId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Period getPeriod(org.osid.id.Id periodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.billing.PeriodList periods = getPeriods()) {
            while (periods.hasNext()) {
                org.osid.billing.Period period = periods.getNextPeriod();
                if (period.getId().equals(periodId)) {
                    return (period);
                }
            }
        } 

        throw new org.osid.NotFoundException(periodId + " not found");
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  periods specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Periods</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPeriods()</code>.
     *
     *  @param  periodIds the list of <code>Ids</code> to rerieve 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>periodIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByIds(org.osid.id.IdList periodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.billing.Period> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = periodIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPeriod(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("period " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.billing.period.LinkedPeriodList(ret));
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  period genus <code>Type</code> which does not include
     *  periods of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPeriods()</code>.
     *
     *  @param  periodGenusType a period genus type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByGenusType(org.osid.type.Type periodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.period.PeriodGenusFilterList(getPeriods(), periodGenusType));
    }


    /**
     *  Gets a <code>PeriodList</code> corresponding to the given
     *  period genus <code>Type</code> and include any additional
     *  periods with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPeriods()</code>.
     *
     *  @param  periodGenusType a period genus type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByParentGenusType(org.osid.type.Type periodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPeriodsByGenusType(periodGenusType));
    }


    /**
     *  Gets a <code>PeriodList</code> containing the given
     *  period record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPeriods()</code>.
     *
     *  @param  periodRecordType a period record type 
     *  @return the returned <code>Period</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>periodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsByRecordType(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.period.PeriodRecordFilterList(getPeriods(), periodRecordType));
    }


    /**
     *  Gets a <code> PeriodList </code> where to the given <code>
     *  DateTime </code> range falls within the period
     *  inclusive. Periods containing the given date are matched.
     *
     *  In plenary mode, the returned list contains all of the periods
     *  specified in the <code> Id </code> list, in the order of the
     *  list, including duplicates, or an error results if an <code>
     *  Id </code> in the supplied list is not found or inaccessible.
     *  Otherwise, inaccessible <code> Periods </code> may be omitted
     *  from the list including returning a unique set.
     *
     *  @param from start of date range 
     *  @param to end of date range 
     *  @return the returned <code> Period </code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.billing.PeriodList getPeriodsOnDate(org.osid.calendaring.DateTime from,
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.billing.period.PeriodFilterList(new DateFilter(from, to), getPeriods()));
    }


    /**
     *  Gets all <code>Periods</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  periods or an error results. Otherwise, the returned list
     *  may contain only those periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Periods</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.billing.PeriodList getPeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the period list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of periods
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.billing.PeriodList filterPeriodsOnViews(org.osid.billing.PeriodList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }

    
    public static class DateFilter
        implements net.okapia.osid.jamocha.inline.filter.billing.period.PeriodFilter {
        
        private final org.osid.calendaring.DateTime from;
        private final org.osid.calendaring.DateTime to;
        
        
        /**
         *  Constructs a new <code>DateFilter</code>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */
        
        public DateFilter(org.osid.calendaring.DateTime from, org.osid.calendaring.DateTime to) {
            nullarg(from, "from");
            nullarg(to, "to");

            this.from = from;
            this.to = to;

            return;
        }
        
        
        /**
         *  Used by the PeriodFilterList to filter the
         *  period list based on date.
         *
         *  @param period the period
         *  @return <code>true</code> to pass the period,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.billing.Period period) {
            if (!period.hasOpenDate() || !period.hasCloseDate()) {
                return (false);
            }

            if (period.getOpenDate().isGreater(this.from)) {
                return (false);
            }

            if (period.getCloseDate().isLess(this.to)) {
                return (false);
            }

            return (true);
        }
    }
}
