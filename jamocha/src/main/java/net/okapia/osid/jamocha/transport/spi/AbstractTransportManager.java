//
// AbstractTransportManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.transport.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTransportManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.transport.TransportManager,
               org.osid.transport.TransportProxyManager {

    private final Types endpointRecordTypes                = new TypeRefSet();
    private final Types requestRecordTypes                 = new TypeRefSet();
    private final Types responseRecordTypes                = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractTransportManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTransportManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any transport endpoint federation is exposed. Federation is 
     *  exposed when a specific endpoint may be used. Federation is not 
     *  exposed when a set of endpoints appears as a single endpoint. 
     *
     *  @return <code> true </code> if federation is visible <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if outbound stream transport is supported. 
     *
     *  @return <code> true </code> if outbound stream transport is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOutboundStream() {
        return (false);
    }


    /**
     *  Tests if inbound stream transport is supported. 
     *
     *  @return <code> true </code> if incoming stream transport is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInboundStream() {
        return (false);
    }


    /**
     *  Tests if outbound message transport is supported. 
     *
     *  @return <code> true </code> if outbound message transport is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOutboundMessage() {
        return (false);
    }


    /**
     *  Tests if inbound message transport is supported. 
     *
     *  @return <code> true </code> if incoming message transport is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInboundMessage() {
        return (false);
    }


    /**
     *  Tests if endpoint lookup is supported. 
     *
     *  @return <code> true </code> if endpoint lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndpointLookup() {
        return (false);
    }


    /**
     *  Gets a list of supported endpoint record types. 
     *
     *  @return a list of supported endpoint record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEndpointRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.endpointRecordTypes.toCollection()));
    }


    /**
     *  Tests if an endpoint record type is supported. 
     *
     *  @param  endpointRecordType an endpoint record type 
     *  @return <code> true </code> if the endpoint record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> endpointRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEndpointRecordType(org.osid.type.Type endpointRecordType) {
        return (this.endpointRecordTypes.contains(endpointRecordType));
    }


    /**
     *  Adds support for an endpoint record type.
     *
     *  @param endpointRecordType an endpoint record type
     *  @throws org.osid.NullArgumentException
     *  <code>endpointRecordType</code> is <code>null</code>
     */

    protected void addEndpointRecordType(org.osid.type.Type endpointRecordType) {
        this.endpointRecordTypes.add(endpointRecordType);
        return;
    }


    /**
     *  Removes support for an endpoint record type.
     *
     *  @param endpointRecordType an endpoint record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>endpointRecordType</code> is <code>null</code>
     */

    protected void removeEndpointRecordType(org.osid.type.Type endpointRecordType) {
        this.endpointRecordTypes.remove(endpointRecordType);
        return;
    }


    /**
     *  Gets a list of supported request record types. 
     *
     *  @return a list of supported request record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRequestRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.requestRecordTypes.toCollection()));
    }


    /**
     *  Tests if a request record type is supported. 
     *
     *  @param  requestRecordType a request record type 
     *  @return <code> true </code> if the request record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> requestRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRequestRecordType(org.osid.type.Type requestRecordType) {
        return (this.requestRecordTypes.contains(requestRecordType));
    }


    /**
     *  Adds support for a request record type.
     *
     *  @param requestRecordType a request record type
     *  @throws org.osid.NullArgumentException
     *  <code>requestRecordType</code> is <code>null</code>
     */

    protected void addRequestRecordType(org.osid.type.Type requestRecordType) {
        this.requestRecordTypes.add(requestRecordType);
        return;
    }


    /**
     *  Removes support for a request record type.
     *
     *  @param requestRecordType a request record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>requestRecordType</code> is <code>null</code>
     */

    protected void removeRequestRecordType(org.osid.type.Type requestRecordType) {
        this.requestRecordTypes.remove(requestRecordType);
        return;
    }


    /**
     *  Gets a list of supported response record types. 
     *
     *  @return a list of supported response record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getResponseRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.responseRecordTypes.toCollection()));
    }


    /**
     *  Tests if a response record type is supported. 
     *
     *  @param  responseRecordType a response record type 
     *  @return <code> true </code> if the response record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> responseRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsResponseRecordType(org.osid.type.Type responseRecordType) {
        return (this.responseRecordTypes.contains(responseRecordType));
    }


    /**
     *  Adds support for a response record type.
     *
     *  @param responseRecordType a response record type
     *  @throws org.osid.NullArgumentException
     *  <code>responseRecordType</code> is <code>null</code>
     */

    protected void addResponseRecordType(org.osid.type.Type responseRecordType) {
        this.responseRecordTypes.add(responseRecordType);
        return;
    }


    /**
     *  Removes support for a response record type.
     *
     *  @param responseRecordType a response record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>responseRecordType</code> is <code>null</code>
     */

    protected void removeResponseRecordType(org.osid.type.Type responseRecordType) {
        this.responseRecordTypes.remove(responseRecordType);
        return;
    }


    /**
     *  Gets a service for outbound stream transport. 
     *
     *  @return an <code> OutboundStreamSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundStream() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundStreamSession getOutboundStreamSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportManager.getOutboundStreamSession not implemented");
    }


    /**
     *  Gets a service for outbound stream transport. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OutboundStreamSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundStream() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundStreamSession getOutboundStreamSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportProxyManager.getOutboundStreamSession not implemented");
    }


    /**
     *  Gets a service for outbound stream transport using a specified <code> 
     *  Endpoint. </code> 
     *
     *  @param  endpointId a transport endpoint 
     *  @return an <code> OutboundStreamSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> endpointId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundStream() </code> or <code> 
     *          supportsVisibleFederationI() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundStreamSession getOutboundStreamSessionForEndpoint(org.osid.id.Id endpointId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportManager.getOutboundStreamSessionForEndpoint not implemented");
    }


    /**
     *  Gets a service for outbound stream transport using a specified <code> 
     *  Endpoint. </code> 
     *
     *  @param  endpointId a transport endpoint 
     *  @param  proxy a proxy 
     *  @return an <code> OutboundStreamSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> or <code> 
     *          proxy </code> is not found 
     *  @throws org.osid.NullArgumentException <code> endpointId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundStream() </code> or <code> 
     *          supportsVisibleFederationI() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundStreamSession getOutboundStreamSessionForEndpoint(org.osid.id.Id endpointId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportProxyManager.getOutboundStreamSessionForEndpoint not implemented");
    }


    /**
     *  Gets a service for inbound stream transport. 
     *
     *  @param  streamReceiver a stream receiver 
     *  @return an <code> InboundStreamSession </code> 
     *  @throws org.osid.NullArgumentException <code> streamReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInboundStream() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundStreamSession getInboundStreamSession(org.osid.transport.StreamReceiver streamReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportManager.getInboundStreamSession not implemented");
    }


    /**
     *  Gets a service for inbound stream transport. 
     *
     *  @param  streamReceiver a stream receiver 
     *  @param  proxy a proxy 
     *  @return an <code> InboundStreamSession </code> 
     *  @throws org.osid.NullArgumentException <code> streamReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInboundStream() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundStreamSession getInboundStreamSession(org.osid.transport.StreamReceiver streamReceiver, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportProxyManager.getInboundStreamSession not implemented");
    }


    /**
     *  Gets a service for inbound stream transport using a specified <code> 
     *  Endpoint. </code> 
     *
     *  @param  streamReceiver a stream receiver 
     *  @param  endpointId a transport endpoint 
     *  @return an <code> InboundStreamSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> streamReceiver </code> 
     *          or <code> endpointId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsInboundStream() 
     *          </code> or <code> supportsVisibleFederationI() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundStreamSession getInboundStreamSessionForEndpoint(org.osid.transport.StreamReceiver streamReceiver, 
                                                                                      org.osid.id.Id endpointId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportManager.getInboundStreamSessionForEndpoint not implemented");
    }


    /**
     *  Gets a service for inbound stream transport using a specified <code> 
     *  Endpoint. </code> 
     *
     *  @param  streamReceiver a stream receiver 
     *  @param  endpointId a transport endpoint 
     *  @param  proxy a proxy 
     *  @return an <code> InboundStreamSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> streamReceiver, 
     *          endpointId </code> or <code> porxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsInboundStream() 
     *          </code> or <code> supportsVisibleFederationI() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundStreamSession getInboundStreamSessionForEndpoint(org.osid.transport.StreamReceiver streamReceiver, 
                                                                                      org.osid.id.Id endpointId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportProxyManager.getInboundStreamSessionForEndpoint not implemented");
    }


    /**
     *  Gets a service for outbound message transport. 
     *
     *  @return an <code> OutboundMessageSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundMessage() is false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundMessageSession getOutboundMessageSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportManager.getOutboundMessageSession not implemented");
    }


    /**
     *  Gets a service for outbound message transport. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> OutboundMessageSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundMessage() is false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundMessageSession getOutboundMessageSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportProxyManager.getOutboundMessageSession not implemented");
    }


    /**
     *  Gets a service for outbound message transport using a specified 
     *  Endpoint. 
     *
     *  @param  endpointId a transport endpoint 
     *  @return an <code> OutboundMessageSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> endpointId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundMessage() or supportsVisibleFederationI() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundMessageSession getOutboundMessageSessionForEndpoint(org.osid.id.Id endpointId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportManager.getOutboundMessageSessionForEndpoint not implemented");
    }


    /**
     *  Gets a service for outbound message transport using a specified 
     *  Endpoint. 
     *
     *  @param  endpointId a transport endpoint 
     *  @param  proxy a proxy 
     *  @return an <code> OutboundMessageSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> endpointId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOutboundMessage() or supportsVisibleFederationI() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.transport.OutboundMessageSession getOutboundMessageSessionForEndpoint(org.osid.id.Id endpointId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportProxyManager.getOutboundMessageSessionForEndpoint not implemented");
    }


    /**
     *  Gets a service for inbound message transport. 
     *
     *  @param  messageReceiver a message receiver 
     *  @return an <code> InboundMessageSession </code> 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInboundMessage() is false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundMessageSession getInboundMessageSession(org.osid.transport.MessageReceiver messageReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportManager.getInboundMessageSession not implemented");
    }


    /**
     *  Gets a service for inbound message transport. 
     *
     *  @param  messageReceiver a message receiver 
     *  @param  proxy a proxy 
     *  @return an <code> InboundMessageSession </code> 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInboundMessage() is false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundMessageSession getInboundMessageSession(org.osid.transport.MessageReceiver messageReceiver, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportProxyManager.getInboundMessageSession not implemented");
    }


    /**
     *  Gets a service for inbound message transport using a specified 
     *  Endpoint. 
     *
     *  @param  messageReceiver a message receiver 
     *  @param  endpointId a transport endpoint 
     *  @return an <code> InboundMessageSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          or <code> endpointId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInboundMessage() or supportsVisibleFederationI() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundMessageSession getInboundMessageSessionForEndpoint(org.osid.transport.MessageReceiver messageReceiver, 
                                                                                        org.osid.id.Id endpointId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportManager.getInboundMessageSessionForEndpoint not implemented");
    }


    /**
     *  Gets a service for inbound message transport using a specified 
     *  Endpoint. 
     *
     *  @param  messageReceiver a message receiver 
     *  @param  endpointId a transport endpoint 
     *  @param  proxy a proxy 
     *  @return an <code> InboundMessageSession </code> 
     *  @throws org.osid.NotFoundException <code> endpointId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> messageReceiver, 
     *          endpointId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInboundMessage() or supportsVisibleFederationI() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.transport.InboundMessageSession getInboundMessageSessionForEndpoint(org.osid.transport.MessageReceiver messageReceiver, 
                                                                                        org.osid.id.Id endpointId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportProxyManager.getInboundMessageSessionForEndpoint not implemented");
    }


    /**
     *  Gets the endpoint lookup service. 
     *
     *  @return an <code> EndpointLookupSesson </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInbound() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.EndpointLookupSession getEndpointLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportManager.getEndpointLookupSession not implemented");
    }


    /**
     *  Gets the endpoint lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> EndpointLookupSesson </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInbound() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.transport.EndpointLookupSession getEndpointLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.transport.TransportProxyManager.getEndpointLookupSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.endpointRecordTypes.clear();
        this.endpointRecordTypes.clear();

        this.requestRecordTypes.clear();
        this.requestRecordTypes.clear();

        this.responseRecordTypes.clear();
        this.responseRecordTypes.clear();

        return;
    }
}
