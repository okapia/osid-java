//
// AbstractAssessmentTakenLookupSession.java
//
//    A starter implementation framework for providing an AssessmentTaken
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an AssessmentTaken
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAssessmentsTaken(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAssessmentTakenLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.assessment.AssessmentTakenLookupSession {

    private boolean pedantic  = false;
    private boolean federated = false;
    private org.osid.assessment.Bank bank = new net.okapia.osid.jamocha.nil.assessment.bank.UnknownBank();
    

    /**
     *  Gets the <code>Bank/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.bank.getId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.bank);
    }


    /**
     *  Sets the <code>Bank</code>.
     *
     *  @param  bank the bank for this session
     *  @throws org.osid.NullArgumentException <code>bank</code>
     *          is <code>null</code>
     */

    protected void setBank(org.osid.assessment.Bank bank) {
        nullarg(bank, "bank");
        this.bank = bank;
        return;
    }

    /**
     *  Tests if this user can perform <code>AssessmentTaken</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAssessmentsTaken() {
        return (true);
    }


    /**
     *  A complete view of the <code>AssessmentTaken</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAssessmentTakenView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>AssessmentTaken</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAssessmentTakenView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include assessments taken in banks which are
     *  children of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }

     
    /**
     *  Gets the <code>AssessmentTaken</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code> Id </code> is found or a
     *  <code> NOT_FOUND </code> results. Otherwise, the returned
     *  <code>AssessmentTaken</code> may have a different <code> Id
     *  </code> than requested, such as the case where a duplicate
     *  <code> Id </code> was assigned to a
     *  <code>AssessmentTaken</code> and retained for compatibility.
     *
     *  @param  assessmentTakenId <code>Id</code> of the
     *          <code>AssessmentTaken</code>
     *  @return the assessment taken
     *  @throws org.osid.NotFoundException <code>assessmentTakenId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>assessmentTakenId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTaken getAssessmentTaken(org.osid.id.Id assessmentTakenId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.assessment.AssessmentTakenList assessmentsTaken = getAssessmentsTaken()) {
            while (assessmentsTaken.hasNext()) {
                org.osid.assessment.AssessmentTaken assessmentTaken = assessmentsTaken.getNextAssessmentTaken();
                if (assessmentTaken.getId().equals(assessmentTakenId)) {
                    return (assessmentTaken);
                }
            }
        } 

        throw new org.osid.NotFoundException(assessmentTakenId + " not found");
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  assessmentsTaken specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AssessmentsTaken</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAssessmentsTaken()</code>.
     *
     *  @param  assessmentTakenIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByIds(org.osid.id.IdList assessmentTakenIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.AssessmentTaken> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = assessmentTakenIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAssessmentTaken(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("assessment taken " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.assessment.assessmenttaken.LinkedAssessmentTakenList(ret));
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the
     *  given assessment taken genus <code>Type</code> which does not
     *  include assessments taken of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned
     *  list may contain only those assessments taken that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAssessmentsTaken()</code>.
     *
     *  @param  assessmentTakenGenusType an assessmentTaken genus type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByGenusType(org.osid.type.Type assessmentTakenGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenGenusFilterList(getAssessmentsTaken(), assessmentTakenGenusType));
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> corresponding to the given
     *  assessment taken genus <code>Type</code> and include any additional
     *  assessments taken with genus types derived from the specified
     *  <code>Type</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssessmentsTaken()</code>.
     *
     *  @param  assessmentTakenGenusType an assessmentTaken genus type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByParentGenusType(org.osid.type.Type assessmentTakenGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAssessmentsTakenByGenusType(assessmentTakenGenusType));
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> containing the given
     *  assessment taken record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned
     *  list may contain only those assessments taken that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAssessmentsTaken()</code>.
     *
     *  @param  assessmentTakenRecordType an assessmentTaken record type 
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByRecordType(org.osid.type.Type assessmentTakenRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenRecordFilterList(getAssessmentsTaken(), assessmentTakenRecordType));
    }


    /**
     *  Gets an <code>AssessmentTakenList</code> started in the given
     *  date range inclusive. In plenary mode, the returned list
     *  contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> from </code> or <code>
     *          to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDate(org.osid.calendaring.DateTime from,
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilterList(new ActualStartTimeFilter(from, to), getAssessmentsTaken()));
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> for the given resource. In
     *  plenary mode, the returned list contains all known assessments offered
     *  or an error results. Otherwise, the returned list may contain only
     *  those assessments offered that are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTaker(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilterList(new TakerFilter(resourceId), getAssessmentsTaken()));
    }        


    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given resource. In plenary
     *  mode, the returned list contains all known assessments offered
     *  or an error results.  Otherwise, the returned list may contain
     *  only those assessments offered that are accessible through
     *  this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId, from </code>
     *          or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTaker(org.osid.id.Id resourceId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilterList(new ActualStartTimeFilter(from, to), getAssessmentsTakenForTaker(resourceId)));
    }


    /**
     *  Gets an <code> AssessmentTakenList </code> for the given
     *  assessment. In plenary mode, the returned list contains all
     *  known assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session.
     *
     *  @param  assessmentId <code> Id </code> of an <code>Assessment</code>
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForAssessment(org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.AssessmentTaken> ret = new java.util.ArrayList<>();

        try (org.osid.assessment.AssessmentTakenList assessmentsTaken = getAssessmentsTaken()) {
            while (assessmentsTaken.hasNext()) {
                org.osid.assessment.AssessmentTaken assessmentTaken = assessmentsTaken.getNextAssessmentTaken();
                try {
                    if (assessmentTaken.getAssessmentOffered().getAssessmentId().equals(assessmentId)) {
                        ret.add(assessmentTaken);
                    }
                } catch (org.osid.OperationFailedException ofe) {
                    if (!isComparative()) {
                        throw ofe;
                    }
                }
            }
        }
        
        return (new net.okapia.osid.jamocha.assessment.assessmenttaken.LinkedAssessmentTakenList(ret));
    }        


    /**
     *  Gets an <code>AssessmentTakenList</code> started in the given
     *  date range inclusive for the given assessment. In plenary
     *  mode, the returned list contains all known assessments offered
     *  or an error results. Otherwise, the returned list may contain
     *  only those assessments offered that are accessible through
     *  this session.
     *
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> assessmentId, from
     *          </code> or <code> to </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForAssessment(org.osid.id.Id assessmentId,
                                                                                          org.osid.calendaring.DateTime from,
                                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.AssessmentTaken> ret = new java.util.ArrayList<>();

        try (org.osid.assessment.AssessmentTakenList assessmentsTaken = getAssessmentsTakenForAssessment(assessmentId)) {
            while (assessmentsTaken.hasNext()) {
                org.osid.assessment.AssessmentTaken assessmentTaken = assessmentsTaken.getNextAssessmentTaken();
                if (assessmentTaken.hasStarted()) {
                    if (from.isGreater(assessmentTaken.getActualStartTime())) {
                        continue;
                    }

                    if (to.isLess(assessmentTaken.getActualStartTime())) {
                        continue;
                    }

                    ret.add(assessmentTaken);
                }
            }
        }
        
        return (new net.okapia.osid.jamocha.assessment.assessmenttaken.LinkedAssessmentTakenList(ret));
    }        


    /**
     *  Gets an <code> AssessmentTakenList </code> for the given
     *  resource and assessment. In plenary mode, the returned list
     *  contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code>
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.NullArgumentException <code> resourceId
     *          </code> or <code> assessmentId </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */
    
    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTakerAndAssessment(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id assessmentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.AssessmentTaken> ret = new java.util.ArrayList<>();

        try (org.osid.assessment.AssessmentTakenList assessmentsTaken = getAssessmentsTakenForTaker(resourceId)) {
            while (assessmentsTaken.hasNext()) {
                org.osid.assessment.AssessmentTaken assessmentTaken = assessmentsTaken.getNextAssessmentTaken();
                try {
                    if (assessmentTaken.getAssessmentOffered().getAssessmentId().equals(assessmentId)) {
                        ret.add(assessmentTaken);
                    }
                } catch (org.osid.OperationFailedException ofe) {
                    if (!isComparative()) {
                        throw ofe;
                    }
                }
            }
        }
        
        return (new net.okapia.osid.jamocha.assessment.assessmenttaken.LinkedAssessmentTakenList(ret));
    }        


    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given resource and
     *  assessment. In plenary mode, the returned list contains all
     *  known assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param  assessmentId <code> Id </code> of an <code> Assessment </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          assessmentId, from </code> or <code> to </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTakerAndAssessment(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id assessmentId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.assessment.AssessmentTaken> ret = new java.util.ArrayList<>();

        try (org.osid.assessment.AssessmentTakenList assessmentsTaken = getAssessmentsTakenForTakerAndAssessment(resourceId, assessmentId)) {
            while (assessmentsTaken.hasNext()) {
                org.osid.assessment.AssessmentTaken assessmentTaken = assessmentsTaken.getNextAssessmentTaken();
                if (assessmentTaken.hasStarted()) {
                    if (from.isGreater(assessmentTaken.getActualStartTime())) {
                        continue;
                    }

                    if (to.isLess(assessmentTaken.getActualStartTime())) {
                        continue;
                    }

                    ret.add(assessmentTaken);
                }
            }
        }
        
        return (new net.okapia.osid.jamocha.assessment.assessmenttaken.LinkedAssessmentTakenList(ret));
    }        


    /**
     *  Gets an <code> AssessmentTakenList </code> by the given
     *  assessment offered. In plenary mode, the returned list
     *  contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this session.
     *
     *  @param  assessmentOfferedId <code> Id </code> of an <code>
     *          AssessmentOffered </code>
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.NullArgumentException <code> assessmentOfferedId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForAssessmentOffered(org.osid.id.Id assessmentOfferedId) 
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilterList(new AssessmentOfferedFilter(assessmentOfferedId), getAssessmentsTaken()));
    }                


    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given assessment
     *  offered. In plenary mode, the returned list contains all known
     *  assessments offered or an error results. Otherwise, the
     *  returned list may contain only those assessments offered that
     *  are accessible through this session.
     *
     *  @param assessmentOfferedId <code> Id </code> of an <code>
     *          AssessmentOffered </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code>
     *          assessmentOfferedId, from </code> or <code> to </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForAssessmentOffered(org.osid.id.Id assessmentOfferedId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilterList(new ActualStartTimeFilter(from, to), getAssessmentsTakenForAssessmentOffered(assessmentOfferedId)));
    }        


    /**
     *  Gets an <code>AssessmentTakenList</code> for the given
     *  resource and assessment offered. In plenary mode, the returned
     *  list contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this session.
     *
     *  @param  resourceId <code>Id</code> of a <code>Resource</code>
     *  @param  assessmentOfferedId <code>Id</code> of an 
     *          <code>AssessmentOffered</code>
     *  @return the returned <code>AssessmentTaken</code> list
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or <code>assessmenOfferedtId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     *          occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenForTakerAndAssessmentOffered(org.osid.id.Id resourceId,
                                                                                                   org.osid.id.Id assessmentOfferedId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilterList(new TakerFilter(resourceId), getAssessmentsTakenForAssessmentOffered(assessmentOfferedId)));
    }        
    

    /**
     *  Gets an <code> AssessmentTakenList </code> started in the
     *  given date range inclusive for the given resource and
     *  assessment offered. In plenary mode, the returned list
     *  contains all known assessments offered or an error
     *  results. Otherwise, the returned list may contain only those
     *  assessments offered that are accessible through this session.
     *
     *  @param  resourceId <code> Id </code> of a <code> Resource </code>
     *  @param assessmentOfferedId <code> Id </code> of an <code>
     *         AssessmentOffered </code>
     *  @param  from start date
     *  @param  to end date
     *  @return the returned <code> AssessmentTaken </code> list
     *  @throws org.osid.InvalidArgumentException <code> from </code> is
     *          greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> resourceId,
     *          assessmentOfferedId, from </code> or <code> to </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization
     *          failure occurred
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenList getAssessmentsTakenByDateForTakerAndAssessmentOffered(org.osid.id.Id resourceId,
                                                                                                         org.osid.id.Id assessmentOfferedId,
                                                                                                         org.osid.calendaring.DateTime from,
                                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilterList(new ActualStartTimeFilter(from, to), getAssessmentsTakenForTakerAndAssessmentOffered(resourceId, assessmentOfferedId)));
    }        


    /**
     *  Gets all <code>AssessmentsTaken</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  assessments taken or an error results. Otherwise, the returned list
     *  may contain only those assessments taken that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>AssessmentsTaken</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.assessment.AssessmentTakenList getAssessmentsTaken()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the assessment taken list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of assessments taken
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.assessment.AssessmentTakenList filterAssessmentsTakenOnViews(org.osid.assessment.AssessmentTakenList list)
        throws org.osid.OperationFailedException {
            
        return (list);
    }


    public static class ActualStartTimeFilter
        implements net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilter {

        private final org.osid.calendaring.DateTime start;
        private final org.osid.calendaring.DateTime end;

        
        /**
         *  Constructs a new <code>ActualStartTimeFilter</code>.
         *
         *  @param from start of date range
         *  @param to end of date range
         *  @throws org.osid.NullArgumentException <code>from</code>
         *          or <code>to</code> is <code>null</code>
         */

        public ActualStartTimeFilter(org.osid.calendaring.DateTime from,
                                     org.osid.calendaring.DateTime to) {

            nullarg(from, "start date");
            nullarg(to, "end date");

            this.start = from;
            this.end = to;

            return;
        }


        /**
         *  Used by the AssessmentTakenFilterList to filter the list
         *  based on start date.
         *
         *  @param assessmentTaken the assessment taken
         *  @return <code>true</code> to pass the assessment taken,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.assessment.AssessmentTaken assessmentTaken) {
            if (!assessmentTaken.hasStarted()) {
                return (false);
            }

            if (assessmentTaken.getActualStartTime().isLess(this.start)) {
                return (false);
            }

            if (assessmentTaken.getActualStartTime().isGreater(this.end)) {
                return (false);
            }

            return (true);
        }
    }


    public static class TakerFilter
        implements net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilter {

        private final org.osid.id.Id resourceId;

        
        /**
         *  Constructs a new <code>TakerFilter</code>.
         *
         *  @param resourceId the resource to filter
         *  @throws org.osid.NullArgumentException
         *          <code>resourceId</code> is <code>null</code>
         */

        public TakerFilter(org.osid.id.Id resourceId) {
            nullarg(resourceId, "resource Id");
            this.resourceId = resourceId;
            return;
        }


        /**
         *  Used by the AssessmentTakenFilterList to filter the list
         *  based on start date.
         *
         *  @param assessmentTaken the assessment taken
         *  @return <code>true</code> to pass the assessment taken,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.assessment.AssessmentTaken assessmentTaken) {
            if (assessmentTaken.getTakerId().equals(this.resourceId)) {
                return (true);
            } else {
                return (false);
            }
        }
    }


    public static class AssessmentOfferedFilter
        implements net.okapia.osid.jamocha.inline.filter.assessment.assessmenttaken.AssessmentTakenFilter {

        private final org.osid.id.Id assessmentOfferedId;

        
        /**
         *  Constructs a new <code>TakerFilter</code>.
         *
         *  @param assessmentOfferedId the assessmentOffered to filter
         *  @throws org.osid.NullArgumentException
         *          <code>assessmentOfferedId</code> is <code>null</code>
         */

        public AssessmentOfferedFilter(org.osid.id.Id assessmentOfferedId) {
            nullarg(assessmentOfferedId, "assessmentOffered Id");
            this.assessmentOfferedId = assessmentOfferedId;
            return;
        }


        /**
         *  Used by the AssessmentTakenFilterList to filter the list
         *  based on start date.
         *
         *  @param assessmentTaken the assessment taken
         *  @return <code>true</code> to pass the assessment taken,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.assessment.AssessmentTaken assessmentTaken) {
            if (assessmentTaken.getAssessmentOfferedId().equals(this.assessmentOfferedId)) {
                return (true);
            } else {
                return (false);
            }
        }
    }
}
