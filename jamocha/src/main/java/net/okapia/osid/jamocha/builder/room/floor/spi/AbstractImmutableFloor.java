//
// AbstractImmutableFloor.java
//
//     Wraps a mutable Floor to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.room.floor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Floor</code> to hide modifiers. This
 *  wrapper provides an immutized Floor from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying floor whose state changes are visible.
 */

public abstract class AbstractImmutableFloor
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableTemporalOsidObject
    implements org.osid.room.Floor {

    private final org.osid.room.Floor floor;


    /**
     *  Constructs a new <code>AbstractImmutableFloor</code>.
     *
     *  @param floor the floor to immutablize
     *  @throws org.osid.NullArgumentException <code>floor</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableFloor(org.osid.room.Floor floor) {
        super(floor);
        this.floor = floor;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the building. 
     *
     *  @return the building <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBuildingId() {
        return (this.floor.getBuildingId());
    }


    /**
     *  Gets the building. 
     *
     *  @return the building 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding()
        throws org.osid.OperationFailedException {

        return (this.floor.getBuilding());
    }


    /**
     *  Gets the floor number. 
     *
     *  @return the floor number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.floor.getNumber());
    }


    /**
     *  Tests if an area is available. 
     *
     *  @return <code> true </code> if an area is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasArea() {
        return (this.floor.hasArea());
    }


    /**
     *  Gets the gross square footage of this floor. 
     *
     *  @return the gross area 
     *  @throws org.osid.IllegalStateException <code> hasArea() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getGrossArea() {
        return (this.floor.getGrossArea());
    }


    /**
     *  Gets the floor record corresponding to the given <code> Floor </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> floorRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(floorRecordType) </code> is <code> true </code> . 
     *
     *  @param  floorRecordType the type of floor record to retrieve 
     *  @return the floor record 
     *  @throws org.osid.NullArgumentException <code> floorRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(floorRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.records.FloorRecord getFloorRecord(org.osid.type.Type floorRecordType)
        throws org.osid.OperationFailedException {

        return (this.floor.getFloorRecord(floorRecordType));
    }
}

