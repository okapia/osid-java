//
// AbstractRealmQuery.java
//
//     A template for making a Realm Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.realm.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for realms.
 */

public abstract class AbstractRealmQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.personnel.RealmQuery {

    private final java.util.Collection<org.osid.personnel.records.RealmQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the person <code> Id </code> for this query to match persons 
     *  assigned to realms. 
     *
     *  @param  personId a person <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> personId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPersonId(org.osid.id.Id personId, boolean match) {
        return;
    }


    /**
     *  Clears all person <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPersonIdTerms() {
        return;
    }


    /**
     *  Tests if a person query is available. 
     *
     *  @return <code> true </code> if a person query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonQuery() {
        return (false);
    }


    /**
     *  Gets the query for a person. 
     *
     *  @return the person query 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuery getPersonQuery() {
        throw new org.osid.UnimplementedException("supportsPersonQuery() is false");
    }


    /**
     *  Matches realms with any person. 
     *
     *  @param  match <code> true </code> to match realms with any person, 
     *          <code> false </code> to match realms with no persons 
     */

    @OSID @Override
    public void matchAnyPerson(boolean match) {
        return;
    }


    /**
     *  Clears all person terms. 
     */

    @OSID @Override
    public void clearPersonTerms() {
        return;
    }


    /**
     *  Sets the organization <code> Id </code> for this query to match 
     *  organizations assigned to realms. 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchOrganizationId(org.osid.id.Id organizationId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears all organization <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrganizationIdTerms() {
        return;
    }


    /**
     *  Tests if a organization query is available. 
     *
     *  @return <code> true </code> if a organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization. 
     *
     *  @return the organization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuery getOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsOrganizationQuery() is false");
    }


    /**
     *  Matches realms with any organization. 
     *
     *  @param  match <code> true </code> to match realms with any 
     *          organization, <code> false </code> to match realms with no 
     *          organizations 
     */

    @OSID @Override
    public void matchAnyOrganization(boolean match) {
        return;
    }


    /**
     *  Clears all organization terms. 
     */

    @OSID @Override
    public void clearOrganizationTerms() {
        return;
    }


    /**
     *  Sets the position <code> Id </code> for this query to match positions 
     *  assigned to realms. 
     *
     *  @param  positionId a position <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> positionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPositionId(org.osid.id.Id positionId, boolean match) {
        return;
    }


    /**
     *  Clears all position <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPositionIdTerms() {
        return;
    }


    /**
     *  Tests if a position query is available. 
     *
     *  @return <code> true </code> if a position query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a position. 
     *
     *  @return the position query 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuery getPositionQuery() {
        throw new org.osid.UnimplementedException("supportsPositionQuery() is false");
    }


    /**
     *  Matches realms with any position. 
     *
     *  @param  match <code> true </code> to match realms with any position, 
     *          <code> false </code> to match realms with no positions 
     */

    @OSID @Override
    public void matchAnyPosition(boolean match) {
        return;
    }


    /**
     *  Clears all position terms. 
     */

    @OSID @Override
    public void clearPositionTerms() {
        return;
    }


    /**
     *  Sets the appointment <code> Id </code> for this query to match 
     *  positions assigned to realms. 
     *
     *  @param  appointmentId an appointment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> appointmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAppointmentId(org.osid.id.Id appointmentId, boolean match) {
        return;
    }


    /**
     *  Clears all appointment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAppointmentIdTerms() {
        return;
    }


    /**
     *  Tests if an appointment query is available. 
     *
     *  @return <code> true </code> if an appointment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an appointment. 
     *
     *  @return the appointment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQuery getAppointmentQuery() {
        throw new org.osid.UnimplementedException("supportsAppointmentQuery() is false");
    }


    /**
     *  Matches realms with any appointment. 
     *
     *  @param  match <code> true </code> to match realms with any 
     *          appointment, <code> false </code> to match realms with no 
     *          appointments 
     */

    @OSID @Override
    public void matchAnyAppointment(boolean match) {
        return;
    }


    /**
     *  Clears all appointment terms. 
     */

    @OSID @Override
    public void clearAppointmentTerms() {
        return;
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match realms that 
     *  have the specified realm as an ancestor. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorRealmId(org.osid.id.Id realmId, boolean match) {
        return;
    }


    /**
     *  Clears all ancestor realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorRealmIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorRealmQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getAncestorRealmQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorRealmQuery() is false");
    }


    /**
     *  Matches realms with any ancestor. 
     *
     *  @param  match <code> true </code> to match realms with any ancestor, 
     *          <code> false </code> to match root realms 
     */

    @OSID @Override
    public void matchAnyAncestorRealm(boolean match) {
        return;
    }


    /**
     *  Clears all ancestor realm terms. 
     */

    @OSID @Override
    public void clearAncestorRealmTerms() {
        return;
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match realms that 
     *  have the specified realm as a descendant. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantRealmId(org.osid.id.Id realmId, boolean match) {
        return;
    }


    /**
     *  Clears all descendant realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantRealmIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantRealmQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getDescendantRealmQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantRealmQuery() is false");
    }


    /**
     *  Matches realms with any descendant. 
     *
     *  @param  match <code> true </code> to match realms with any descendant, 
     *          <code> false </code> to match leaf realms 
     */

    @OSID @Override
    public void matchAnyDescendantRealm(boolean match) {
        return;
    }


    /**
     *  Clears all descendant realm terms. 
     */

    @OSID @Override
    public void clearDescendantRealmTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given realm query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a realm implementing the requested record.
     *
     *  @param realmRecordType a realm record type
     *  @return the realm query record
     *  @throws org.osid.NullArgumentException
     *          <code>realmRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(realmRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.RealmQueryRecord getRealmQueryRecord(org.osid.type.Type realmRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.RealmQueryRecord record : this.records) {
            if (record.implementsRecordType(realmRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(realmRecordType + " is not supported");
    }


    /**
     *  Adds a record to this realm query. 
     *
     *  @param realmQueryRecord realm query record
     *  @param realmRecordType realm record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRealmQueryRecord(org.osid.personnel.records.RealmQueryRecord realmQueryRecord, 
                                          org.osid.type.Type realmRecordType) {

        addRecordType(realmRecordType);
        nullarg(realmQueryRecord, "realm query record");
        this.records.add(realmQueryRecord);        
        return;
    }
}
