//
// AbstractAssemblyActivityQuery.java
//
//     An ActivityQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.learning.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An ActivityQuery that stores terms.
 */

public abstract class AbstractAssemblyActivityQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.learning.ActivityQuery,
               org.osid.learning.ActivityQueryInspector,
               org.osid.learning.ActivitySearchOrder {

    private final java.util.Collection<org.osid.learning.records.ActivityQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.learning.records.ActivityQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.learning.records.ActivitySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyActivityQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyActivityQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the objective <code> Id </code> for this query. 
     *
     *  @param  objectiveId an objective <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveId(org.osid.id.Id objectiveId, boolean match) {
        getAssembler().addIdTerm(getObjectiveIdColumn(), objectiveId, match);
        return;
    }


    /**
     *  Clears the objective <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveIdTerms() {
        getAssembler().clearTerms(getObjectiveIdColumn());
        return;
    }


    /**
     *  Gets the objective <code> Id </code> query terms. 
     *
     *  @return the objective <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveIdTerms() {
        return (getAssembler().getIdTerms(getObjectiveIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the objective. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByObjective(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getObjectiveColumn(), style);
        return;
    }


    /**
     *  Gets the ObjectiveId column name.
     *
     * @return the column name
     */

    protected String getObjectiveIdColumn() {
        return ("objective_id");
    }


    /**
     *  Tests if an <code> ObjectiveQuery </code> is available for querying 
     *  objectives. 
     *
     *  @return <code> true </code> if an objective query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the objective query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQuery getObjectiveQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveQuery() is false");
    }


    /**
     *  Clears the objective terms. 
     */

    @OSID @Override
    public void clearObjectiveTerms() {
        getAssembler().clearTerms(getObjectiveColumn());
        return;
    }


    /**
     *  Gets the objective query terms. 
     *
     *  @return the objective terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveQueryInspector[] getObjectiveTerms() {
        return (new org.osid.learning.ObjectiveQueryInspector[0]);
    }


    /**
     *  Tests if an objective search order is available. 
     *
     *  @return <code> true </code> if an objective search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveSearchOrder() {
        return (false);
    }


    /**
     *  Gets an objective search order. 
     *
     *  @return an objective search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveSearchOrder getObjectiveSearchOrder() {
        throw new org.osid.UnimplementedException("supportsObjectiveSearchOrder() is false");
    }


    /**
     *  Gets the Objective column name.
     *
     * @return the column name
     */

    protected String getObjectiveColumn() {
        return ("objective");
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId an asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        getAssembler().addIdTerm(getAssetIdColumn(), assetId, match);
        return;
    }


    /**
     *  Clears the asset <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        getAssembler().clearTerms(getAssetIdColumn());
        return;
    }


    /**
     *  Gets the asset <code> Id </code> query terms. 
     *
     *  @return the asset <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (getAssembler().getIdTerms(getAssetIdColumn()));
    }


    /**
     *  Gets the AssetId column name.
     *
     * @return the column name
     */

    protected String getAssetIdColumn() {
        return ("asset_id");
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available for querying 
     *  objectives. 
     *
     *  @return <code> true </code> if an robjective query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches an activity that has any objective assigned. 
     *
     *  @param  match <code> true </code> to match activities with any asset, 
     *          <code> false </code> to match activities with no asset 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        getAssembler().addIdWildcardTerm(getAssetColumn(), match);
        return;
    }


    /**
     *  Clears the asset terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        getAssembler().clearTerms(getAssetColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the asset terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the Asset column name.
     *
     * @return the column name
     */

    protected String getAssetColumn() {
        return ("asset");
    }


    /**
     *  Sets the course <code> Id </code> for this query. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCourseId(org.osid.id.Id courseId, boolean match) {
        getAssembler().addIdTerm(getCourseIdColumn(), courseId, match);
        return;
    }


    /**
     *  Clears the course <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseIdTerms() {
        getAssembler().clearTerms(getCourseIdColumn());
        return;
    }


    /**
     *  Gets the course <code> Id </code> query terms. 
     *
     *  @return the course <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseIdTerms() {
        return (getAssembler().getIdTerms(getCourseIdColumn()));
    }


    /**
     *  Gets the CourseId column name.
     *
     * @return the column name
     */

    protected String getCourseIdColumn() {
        return ("course_id");
    }


    /**
     *  Tests if a <code> CourseQuery </code> is available for querying 
     *  courses. 
     *
     *  @return <code> true </code> if a course query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the course query 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuery getCourseQuery() {
        throw new org.osid.UnimplementedException("supportsCourseQuery() is false");
    }


    /**
     *  Matches an activity that has any course assigned. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          courses, <code> false </code> to match activities with no 
     *          courses 
     */

    @OSID @Override
    public void matchAnyCourse(boolean match) {
        getAssembler().addIdWildcardTerm(getCourseColumn(), match);
        return;
    }


    /**
     *  Clears the course terms. 
     */

    @OSID @Override
    public void clearCourseTerms() {
        getAssembler().clearTerms(getCourseColumn());
        return;
    }


    /**
     *  Gets the course query terms. 
     *
     *  @return the course terms 
     */

    @OSID @Override
    public org.osid.course.CourseQueryInspector[] getCourseTerms() {
        return (new org.osid.course.CourseQueryInspector[0]);
    }


    /**
     *  Gets the Course column name.
     *
     * @return the column name
     */

    protected String getCourseColumn() {
        return ("course");
    }


    /**
     *  Sets the assessment <code> Id </code> for this query. 
     *
     *  @param  assessmentId an assessment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assessmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssessmentId(org.osid.id.Id assessmentId, boolean match) {
        getAssembler().addIdTerm(getAssessmentIdColumn(), assessmentId, match);
        return;
    }


    /**
     *  Clears the assessment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAssessmentIdTerms() {
        getAssembler().clearTerms(getAssessmentIdColumn());
        return;
    }


    /**
     *  Gets the assessment <code> Id </code> query terms. 
     *
     *  @return the assessment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssessmentIdTerms() {
        return (getAssembler().getIdTerms(getAssessmentIdColumn()));
    }


    /**
     *  Gets the AssessmentId column name.
     *
     * @return the column name
     */

    protected String getAssessmentIdColumn() {
        return ("assessment_id");
    }


    /**
     *  Tests if an <code> AssessmentQuery </code> is available for querying 
     *  assessments. 
     *
     *  @return <code> true </code> if an assessment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for a assessment. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the assessment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuery getAssessmentQuery() {
        throw new org.osid.UnimplementedException("supportsAssessmentQuery() is false");
    }


    /**
     *  Matches an activity that has any assessment assigned. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          assessments, <code> false </code> to match activities with no 
     *          assessments 
     */

    @OSID @Override
    public void matchAnyAssessment(boolean match) {
        getAssembler().addIdWildcardTerm(getAssessmentColumn(), match);
        return;
    }


    /**
     *  Clears the assessment terms. 
     */

    @OSID @Override
    public void clearAssessmentTerms() {
        getAssembler().clearTerms(getAssessmentColumn());
        return;
    }


    /**
     *  Gets the assessment query terms. 
     *
     *  @return the assessment terms 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQueryInspector[] getAssessmentTerms() {
        return (new org.osid.assessment.AssessmentQueryInspector[0]);
    }


    /**
     *  Gets the Assessment column name.
     *
     * @return the column name
     */

    protected String getAssessmentColumn() {
        return ("assessment");
    }


    /**
     *  Sets the objective bank <code> Id </code> for this query. 
     *
     *  @param  objectiveBankId an objective bank <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectiveBankId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchObjectiveBankId(org.osid.id.Id objectiveBankId, 
                                     boolean match) {
        getAssembler().addIdTerm(getObjectiveBankIdColumn(), objectiveBankId, match);
        return;
    }


    /**
     *  Clears the objective bank <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearObjectiveBankIdTerms() {
        getAssembler().clearTerms(getObjectiveBankIdColumn());
        return;
    }


    /**
     *  Gets the objective bank <code> Id </code> query terms. 
     *
     *  @return the objective bank <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getObjectiveBankIdTerms() {
        return (getAssembler().getIdTerms(getObjectiveBankIdColumn()));
    }


    /**
     *  Gets the ObjectiveBankId column name.
     *
     * @return the column name
     */

    protected String getObjectiveBankIdColumn() {
        return ("objective_bank_id");
    }


    /**
     *  Tests if a <code> ObjectiveBankQuery </code> is available for querying 
     *  resources. 
     *
     *  @return <code> true </code> if an objective bank query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsObjectiveBankQuery() {
        return (false);
    }


    /**
     *  Gets the query for an objective bank. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the objective bank query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsObjectiveBankQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQuery getObjectiveBankQuery() {
        throw new org.osid.UnimplementedException("supportsObjectiveBankQuery() is false");
    }


    /**
     *  Clears the objective bank terms. 
     */

    @OSID @Override
    public void clearObjectiveBankTerms() {
        getAssembler().clearTerms(getObjectiveBankColumn());
        return;
    }


    /**
     *  Gets the objective bank query terms. 
     *
     *  @return the objective bank terms 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankQueryInspector[] getObjectiveBankTerms() {
        return (new org.osid.learning.ObjectiveBankQueryInspector[0]);
    }


    /**
     *  Gets the ObjectiveBank column name.
     *
     * @return the column name
     */

    protected String getObjectiveBankColumn() {
        return ("objective_bank");
    }


    /**
     *  Tests if this activity supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityRecordType an activity record type 
     *  @return <code>true</code> if the activityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRecordType) {
        for (org.osid.learning.records.ActivityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity query record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ActivityQueryRecord getActivityQueryRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ActivityQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  activityRecordType the activity record type 
     *  @return the activity query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ActivityQueryInspectorRecord getActivityQueryInspectorRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ActivityQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param activityRecordType the activity record type
     *  @return the activity search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.learning.records.ActivitySearchOrderRecord getActivitySearchOrderRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.learning.records.ActivitySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this activity. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityQueryRecord the activity query record
     *  @param activityQueryInspectorRecord the activity query inspector
     *         record
     *  @param activitySearchOrderRecord the activity search order record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityQueryRecord</code>,
     *          <code>activityQueryInspectorRecord</code>,
     *          <code>activitySearchOrderRecord</code> or
     *          <code>activityRecordTypeactivity</code> is
     *          <code>null</code>
     */
            
    protected void addActivityRecords(org.osid.learning.records.ActivityQueryRecord activityQueryRecord, 
                                      org.osid.learning.records.ActivityQueryInspectorRecord activityQueryInspectorRecord, 
                                      org.osid.learning.records.ActivitySearchOrderRecord activitySearchOrderRecord, 
                                      org.osid.type.Type activityRecordType) {

        addRecordType(activityRecordType);

        nullarg(activityQueryRecord, "activity query record");
        nullarg(activityQueryInspectorRecord, "activity query inspector record");
        nullarg(activitySearchOrderRecord, "activity search odrer record");

        this.queryRecords.add(activityQueryRecord);
        this.queryInspectorRecords.add(activityQueryInspectorRecord);
        this.searchOrderRecords.add(activitySearchOrderRecord);
        
        return;
    }
}
