//
// AbstractAuditEnablerQueryInspector.java
//
//     A template for making an AuditEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.rules.auditenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for audit enablers.
 */

public abstract class AbstractAuditEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.inquiry.rules.AuditEnablerQueryInspector {

    private final java.util.Collection<org.osid.inquiry.rules.records.AuditEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the audit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAuditIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the audit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQueryInspector[] getRuledAuditTerms() {
        return (new org.osid.inquiry.AuditQueryInspector[0]);
    }


    /**
     *  Gets the inquest <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getInquestIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the inquest query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQueryInspector[] getInquestTerms() {
        return (new org.osid.inquiry.InquestQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given audit enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an audit enabler implementing the requested record.
     *
     *  @param auditEnablerRecordType an audit enabler record type
     *  @return the audit enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>auditEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auditEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.rules.records.AuditEnablerQueryInspectorRecord getAuditEnablerQueryInspectorRecord(org.osid.type.Type auditEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.rules.records.AuditEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(auditEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auditEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this audit enabler query. 
     *
     *  @param auditEnablerQueryInspectorRecord audit enabler query inspector
     *         record
     *  @param auditEnablerRecordType auditEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuditEnablerQueryInspectorRecord(org.osid.inquiry.rules.records.AuditEnablerQueryInspectorRecord auditEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type auditEnablerRecordType) {

        addRecordType(auditEnablerRecordType);
        nullarg(auditEnablerRecordType, "audit enabler record type");
        this.records.add(auditEnablerQueryInspectorRecord);        
        return;
    }
}
