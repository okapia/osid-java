//
// AbstractImmutableCredentialEntry.java
//
//     Wraps a mutable CredentialEntry to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.credentialentry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>CredentialEntry</code> to hide modifiers. This
 *  wrapper provides an immutized CredentialEntry from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying credentialEntry whose state changes are visible.
 */

public abstract class AbstractImmutableCredentialEntry
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.course.chronicle.CredentialEntry {

    private final org.osid.course.chronicle.CredentialEntry credentialEntry;


    /**
     *  Constructs a new <code>AbstractImmutableCredentialEntry</code>.
     *
     *  @param credentialEntry the credential entry to immutablize
     *  @throws org.osid.NullArgumentException <code>credentialEntry</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCredentialEntry(org.osid.course.chronicle.CredentialEntry credentialEntry) {
        super(credentialEntry);
        this.credentialEntry = credentialEntry;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> Student. </code> 
     *
     *  @return the student <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.credentialEntry.getStudentId());
    }


    /**
     *  Gets the <code> Student. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.credentialEntry.getStudent());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Credential. </code> 
     *
     *  @return the credential <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCredentialId() {
        return (this.credentialEntry.getCredentialId());
    }


    /**
     *  Gets the <code> Credential. </code> 
     *
     *  @return the credential 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Credential getCredential()
        throws org.osid.OperationFailedException {

        return (this.credentialEntry.getCredential());
    }


    /**
     *  Gets the award date. 
     *
     *  @return the date awarded 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateAwarded() {
        return (this.credentialEntry.getDateAwarded());
    }


    /**
     *  Tests if a program is associated with this credential. 
     *
     *  @return <code> true </code> if a program is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasProgram() {
        return (this.credentialEntry.hasProgram());
    }


    /**
     *  Gets the <code> Id </code> of the <code> Program. </code> 
     *
     *  @return the program <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getProgramId() {
        return (this.credentialEntry.getProgramId());
    }


    /**
     *  Gets the <code> Program. </code> 
     *
     *  @return the program 
     *  @throws org.osid.IllegalStateException <code> hasProgram() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.program.Program getProgram()
        throws org.osid.OperationFailedException {

        return (this.credentialEntry.getProgram());
    }


    /**
     *  Gets the credential entry record corresponding to the given <code> 
     *  CredentialEntry </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> credentialEntryRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(credentialEntryRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  credentialEntryRecordType the type of credential entry record 
     *          to retrieve 
     *  @return the credential entry record 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialEntryRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(credentialEntryRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.records.CredentialEntryRecord getCredentialEntryRecord(org.osid.type.Type credentialEntryRecordType)
        throws org.osid.OperationFailedException {

        return (this.credentialEntry.getCredentialEntryRecord(credentialEntryRecordType));
    }
}

