//
// AbstractBudgetEntryQueryInspector.java
//
//     A template for making a BudgetEntryQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budgetentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for budget entries.
 */

public abstract class AbstractBudgetEntryQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.financials.budgeting.BudgetEntryQueryInspector {

    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetEntryQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the budget <code> Id </code> query terms. 
     *
     *  @return the v <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBudgetIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the budget query terms. 
     *
     *  @return the budget query terms 
     */

    @OSID @Override
    public org.osid.financials.budgeting.BudgetQueryInspector[] getBudgetTerms() {
        return (new org.osid.financials.budgeting.BudgetQueryInspector[0]);
    }


    /**
     *  Gets the account <code> Id </code> query terms. 
     *
     *  @return the account <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAccountIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the account query terms. 
     *
     *  @return the account query terms 
     */

    @OSID @Override
    public org.osid.financials.AccountQueryInspector[] getAccountTerms() {
        return (new org.osid.financials.AccountQueryInspector[0]);
    }


    /**
     *  Gets the amount query terms. 
     *
     *  @return the date range query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getAmountTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the debit query terms. 
     *
     *  @return the debit query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDebitTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the business <code> Id </code> query terms. 
     *
     *  @return the business <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBusinessIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the business query terms. 
     *
     *  @return the business query terms 
     */

    @OSID @Override
    public org.osid.financials.BusinessQueryInspector[] getBusinessTerms() {
        return (new org.osid.financials.BusinessQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given budget entry query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a budget entry implementing the requested record.
     *
     *  @param budgetEntryRecordType a budget entry record type
     *  @return the budget entry query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>budgetEntryRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetEntryRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetEntryQueryInspectorRecord getBudgetEntryQueryInspectorRecord(org.osid.type.Type budgetEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetEntryQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(budgetEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this budget entry query. 
     *
     *  @param budgetEntryQueryInspectorRecord budget entry query inspector
     *         record
     *  @param budgetEntryRecordType budgetEntry record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBudgetEntryQueryInspectorRecord(org.osid.financials.budgeting.records.BudgetEntryQueryInspectorRecord budgetEntryQueryInspectorRecord, 
                                                   org.osid.type.Type budgetEntryRecordType) {

        addRecordType(budgetEntryRecordType);
        nullarg(budgetEntryRecordType, "budget entry record type");
        this.records.add(budgetEntryQueryInspectorRecord);        
        return;
    }
}
