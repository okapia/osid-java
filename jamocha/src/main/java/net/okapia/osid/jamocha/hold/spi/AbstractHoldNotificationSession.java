//
// AbstractHoldNotificationSession.java
//
//     A template for making HoldNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Hold} objects. This session is intended for
 *  consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Hold} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for hold entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractHoldNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.hold.HoldNotificationSession {

    private boolean federated = false;
    private org.osid.hold.Oubliette oubliette = new net.okapia.osid.jamocha.nil.hold.oubliette.UnknownOubliette();


    /**
     *  Gets the {@code Oubliette/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code Oubliette Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getOublietteId() {
        return (this.oubliette.getId());
    }

    
    /**
     *  Gets the {@code Oubliette} associated with this session.
     *
     *  @return the {@code Oubliette} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.oubliette);
    }


    /**
     *  Sets the {@code Oubliette}.
     *
     *  @param oubliette the oubliette for this session
     *  @throws org.osid.NullArgumentException {@code oubliette}
     *          is {@code null}
     */

    protected void setOubliette(org.osid.hold.Oubliette oubliette) {
        nullarg(oubliette, "oubliette");
        this.oubliette = oubliette;
        return;
    }


    /**
     *  Tests if this user can register for {@code Hold}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForHoldNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeHoldNotification() </code>.
     */

    @OSID @Override
    public void reliableHoldNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableHoldNotifications() {
        return;
    }


    /**
     *  Acknowledge a hold notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeHoldNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for holds in oubliettes which
     *  are children of this oubliette in the oubliette hierarchy.
     */

    @OSID @Override
    public void useFederatedOublietteView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this oubliette only.
     */

    @OSID @Override
    public void useIsolatedOublietteView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new holds. {@code
     *  HoldReceiver.newHold()} is invoked when a new {@code Hold} is
     *  created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewHolds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new holds for the given issue
     *  {@code Id}. {@code HoldReceiver.newHold()} is invoked when a
     *  new {@code Hold} is created.
     *
     *  @param  issueId the {@code Id} of the issue to monitor
     *  @throws org.osid.NullArgumentException {@code issueId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewHoldsForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new holds for the given resource
     *  {@code Id}. {@code HoldReceiver.newHold()} is invoked when a
     *  new {@code Hold} is created.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewHoldsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated holds. {@code
     *  HoldReceiver.changedHold()} is invoked when a hold is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedHolds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated holds for the given
     *  issue {@code Id}. {@code HoldReceiver.changedHold()} is
     *  invoked when a {@code Hold} in this oubliette is changed.
     *
     *  @param  issueId the {@code Id} of the issue to monitor
     *  @throws org.osid.NullArgumentException {@code issueId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedHoldsForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated holds for the given
     *  resource {@code Id}. {@code HoldReceiver.changedHold()} is
     *  invoked when a {@code Hold} in this oubliette is changed.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedHoldsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hold. {@code
     *  HoldReceiver.changedHold()} is invoked when the specified hold
     *  is changed.
     *
     *  @param holdId the {@code Id} of the {@code Hold} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code holdId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedHold(org.osid.id.Id holdId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted holds. {@code
     *  HoldReceiver.deletedHold()} is invoked when a hold is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedHolds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted holds for the given
     *  issue {@code Id}. {@code HoldReceiver.deletedHold()} is
     *  invoked when a {@code Hold} is deleted or removed from this
     *  oubliette.
     *
     *  @param  issueId the {@code Id} of the issue to monitor
     *  @throws org.osid.NullArgumentException {@code issueId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedHoldsForIssue(org.osid.id.Id issueId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted holds for the given
     *  resource {@code Id}. {@code HoldReceiver.deletedHold()} is
     *  invoked when a {@code Hold} is deleted or removed from this
     *  oubliette.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedHoldsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted hold. {@code
     *  HoldReceiver.deletedHold()} is invoked when the specified hold
     *  is deleted.
     *
     *  @param holdId the {@code Id} of the
     *          {@code Hold} to monitor
     *  @throws org.osid.NullArgumentException {@code holdId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedHold(org.osid.id.Id holdId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
