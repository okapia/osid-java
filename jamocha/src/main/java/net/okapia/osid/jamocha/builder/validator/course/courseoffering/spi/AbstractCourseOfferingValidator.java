//
// AbstractCourseOfferingValidator.java
//
//     Validates a CourseOffering.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.courseoffering.spi;


/**
 *  Validates a CourseOffering.
 */

public abstract class AbstractCourseOfferingValidator
    extends net.okapia.osid.jamocha.builder.validator.spi.AbstractOsidRelationshipValidator {


    /**
     *  Constructs a new <code>AbstractCourseOfferingValidator</code>.
     */

    protected AbstractCourseOfferingValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AbstractCourseOfferingValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    protected AbstractCourseOfferingValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a CourseOffering.
     *
     *  @param courseOffering a course offering to validate
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>courseOffering</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in
     *          assembly
     */
    
    public void validate(org.osid.course.CourseOffering courseOffering) {
        super.validate(courseOffering);

        testNestedObject(courseOffering, "getCourse");
        testNestedObject(courseOffering, "getTerm");
        test(courseOffering.getTitle(), "getTitle()");
        test(courseOffering.getNumber(), "getNumber()");
        testNestedObjects(courseOffering, "getInstructorIds", "getInstructors");

        testConditionalMethod(courseOffering, "getSponsorIds", courseOffering.hasSponsors(), "hasSponors()");
        testConditionalMethod(courseOffering, "getSponsors", courseOffering.hasSponsors(), "hasSponors()");
        if (courseOffering.hasSponsors()) {
            testNestedObjects(courseOffering, "getSponsorIds", "getSponsors");
        }

        testNestedObjects(courseOffering, "getCreditAmountIds", "getCreditAmounts");

        testConditionalMethod(courseOffering, "getGradingOptionIds", courseOffering.isGraded(), "isGraded()");
        testConditionalMethod(courseOffering, "getGradingOptions", courseOffering.isGraded(), "isGraded()");
        if (courseOffering.isGraded()) {
            testNestedObjects(courseOffering, "getGradingOptionIds", "getGradingOptions");
        }

        testConditionalMethod(courseOffering, "getMaximumSeats", courseOffering.isSeatingLimited(), "isSeatingLimited()");
        if (courseOffering.isSeatingLimited()) {
            testCardinalRange(courseOffering.getMinimumSeats(), courseOffering.getMaximumSeats(), "minimum and maximum seats");
        }

        test(courseOffering.getURL(), "getURL()");
        test(courseOffering.getScheduleInfo(), "getScheduleInfo()");

        testConditionalMethod(courseOffering, "getEventId", courseOffering.hasEvent(), "hasEvent()");
        testConditionalMethod(courseOffering, "getEvent", courseOffering.hasEvent(), "hasEvent()");
        if (courseOffering.hasEvent()) {
            testNestedObject(courseOffering, "getEvent");
        }

        return;
    }
}
