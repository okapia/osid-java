//
// AbstractBudget.java
//
//     Defines a Budget.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.financials.budgeting.budget.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Budget</code>.
 */

public abstract class AbstractBudget
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.financials.budgeting.Budget {

    private org.osid.financials.Activity activity;
    private org.osid.financials.FiscalPeriod fiscalPeriod;

    private final java.util.Collection<org.osid.financials.budgeting.records.BudgetRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the actvity <code> Id. </code> 
     *
     *  @return the activity <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        return (this.activity.getId());
    }


    /**
     *  Gets the activity. 
     *
     *  @return the activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.Activity getActivity()
        throws org.osid.OperationFailedException {

        return (this.activity);
    }


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException
     *          <code>activity</code> is <code>null</code>
     */

    protected void setActivity(org.osid.financials.Activity activity) {
        nullarg(activity, "activity");
        this.activity = activity;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> FiscalPeriod. </code> 
     *
     *  @return the <code> FiscalPeriod </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFiscalPeriodId() {
        return (this.fiscalPeriod.getId());
    }


    /**
     *  Gets the <code> FiscalPeriod. </code> 
     *
     *  @return the fiscal period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod()
        throws org.osid.OperationFailedException {

        return (this.fiscalPeriod);
    }


    /**
     *  Sets the fiscal period.
     *
     *  @param period a fiscal period
     *  @throws org.osid.NullArgumentException <code>period</code> is
     *          <code>null</code>
     */

    protected void setFiscalPeriod(org.osid.financials.FiscalPeriod period) {
        nullarg(period, "fiscal period");
        this.fiscalPeriod = period;
        return;
    }


    /**
     *  Tests if this budget supports the given record
     *  <code>Type</code>.
     *
     *  @param  budgetRecordType a budget record type 
     *  @return <code>true</code> if the budgetRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type budgetRecordType) {
        for (org.osid.financials.budgeting.records.BudgetRecord record : this.records) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Budget</code> record <code>Type</code>.
     *
     *  @param  budgetRecordType the budget record type 
     *  @return the budget record 
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(budgetRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.financials.budgeting.records.BudgetRecord getBudgetRecord(org.osid.type.Type budgetRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.financials.budgeting.records.BudgetRecord record : this.records) {
            if (record.implementsRecordType(budgetRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(budgetRecordType + " is not supported");
    }


    /**
     *  Adds a record to this budget. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param budgetRecord the budget record
     *  @param budgetRecordType budget record type
     *  @throws org.osid.NullArgumentException
     *          <code>budgetRecord</code> or
     *          <code>budgetRecordTypebudget</code> is
     *          <code>null</code>
     */
            
    protected void addBudgetRecord(org.osid.financials.budgeting.records.BudgetRecord budgetRecord, 
                                   org.osid.type.Type budgetRecordType) {
        
        nullarg(budgetRecord, "budget record");
        addRecordType(budgetRecordType);
        this.records.add(budgetRecord);
        
        return;
    }
}
