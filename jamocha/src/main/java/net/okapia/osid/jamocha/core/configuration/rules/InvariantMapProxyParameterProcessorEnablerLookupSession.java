//
// InvariantMapProxyParameterProcessorEnablerLookupSession
//
//    Implements a ParameterProcessorEnabler lookup service backed by a fixed
//    collection of parameterProcessorEnablers. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules;


/**
 *  Implements a ParameterProcessorEnabler lookup service backed by a fixed
 *  collection of parameter processor enablers. The parameter processor enablers are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyParameterProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.core.configuration.rules.spi.AbstractMapParameterProcessorEnablerLookupSession
    implements org.osid.configuration.rules.ParameterProcessorEnablerLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyParameterProcessorEnablerLookupSession} with no
     *  parameter processor enablers.
     *
     *  @param configuration the configuration
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.proxy.Proxy proxy) {
        setConfiguration(configuration);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyParameterProcessorEnablerLookupSession} with a single
     *  parameter processor enabler.
     *
     *  @param configuration the configuration
     *  @param parameterProcessorEnabler a single parameter processor enabler
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameterProcessorEnabler} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.rules.ParameterProcessorEnabler parameterProcessorEnabler, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameterProcessorEnabler(parameterProcessorEnabler);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyParameterProcessorEnablerLookupSession} using
     *  an array of parameter processor enablers.
     *
     *  @param configuration the configuration
     *  @param parameterProcessorEnablers an array of parameter processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameterProcessorEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                  org.osid.configuration.rules.ParameterProcessorEnabler[] parameterProcessorEnablers, org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameterProcessorEnablers(parameterProcessorEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyParameterProcessorEnablerLookupSession} using a
     *  collection of parameter processor enablers.
     *
     *  @param configuration the configuration
     *  @param parameterProcessorEnablers a collection of parameter processor enablers
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code configuration},
     *          {@code parameterProcessorEnablers} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyParameterProcessorEnablerLookupSession(org.osid.configuration.Configuration configuration,
                                                  java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessorEnabler> parameterProcessorEnablers,
                                                  org.osid.proxy.Proxy proxy) {

        this(configuration, proxy);
        putParameterProcessorEnablers(parameterProcessorEnablers);
        return;
    }
}
