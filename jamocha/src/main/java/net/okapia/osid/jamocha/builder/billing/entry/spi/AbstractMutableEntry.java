//
// AbstractMutableEntry.java
//
//     Defines a mutable Entry.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.entry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Entry</code>.
 */

public abstract class AbstractMutableEntry
    extends net.okapia.osid.jamocha.billing.entry.spi.AbstractEntry
    implements org.osid.billing.Entry,
               net.okapia.osid.jamocha.builder.billing.entry.EntryMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this entry. 
     *
     *  @param record entry record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addEntryRecord(org.osid.billing.records.EntryRecord record, org.osid.type.Type recordType) {
        super.addEntryRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this entry is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this entry ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this entry.
     *
     *  @param displayName the name for this entry
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this entry.
     *
     *  @param description the description of this entry
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the end state for the expiration of this entry
     *  relationship.
     *
     *  @param state the end state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    @Override
    public void setEndReason(org.osid.process.State state) {
        super.setEndReason(state);
        return;
    }


    /**
     *  Sets the customer.
     *
     *  @param customer a customer
     *  @throws org.osid.NullArgumentException
     *          <code>customer</code> is <code>null</code>
     */

    @Override
    public void setCustomer(org.osid.billing.Customer customer) {
        super.setCustomer(customer);
        return;
    }


    /**
     *  Sets the item.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException
     *          <code>item</code> is <code>null</code>
     */

    @Override
    public void setItem(org.osid.billing.Item item) {
        super.setItem(item);
        return;
    }


    /**
     *  Sets the period.
     *
     *  @param period a period
     *  @throws org.osid.NullArgumentException
     *          <code>period</code> is <code>null</code>
     */

    @Override
    public void setPeriod(org.osid.billing.Period period) {
        super.setPeriod(period);
        return;
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    @Override
    public void setQuantity(long quantity) {
        super.setQuantity(quantity);
        return;
    }


    /**
     *  Sets the amount.
     *
     *  @param amount an amount
     *  @throws org.osid.NullArgumentException
     *          <code>amount</code> is <code>null</code>
     */

    @Override
    public void setAmount(org.osid.financials.Currency amount) {
        super.setAmount(amount);
        return;
    }

    
    /**
     *  Sets the debit flag.
     *
     *  @param debit <code> true </code> if this item amount is a
     *          debit, <code> false </code> if it is a credit
     */

    @Override
    public void setDebit(boolean debit) {
        super.setDebit(debit);
        return;
    }
}

