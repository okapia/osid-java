//
// AbstractQueryBallotConstrainerEnablerLookupSession.java
//
//    An inline adapter that maps a BallotConstrainerEnablerLookupSession to
//    a BallotConstrainerEnablerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a BallotConstrainerEnablerLookupSession to
 *  a BallotConstrainerEnablerQuerySession.
 */

public abstract class AbstractQueryBallotConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.voting.rules.spi.AbstractBallotConstrainerEnablerLookupSession
    implements org.osid.voting.rules.BallotConstrainerEnablerLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;
    private final org.osid.voting.rules.BallotConstrainerEnablerQuerySession session;
    

    /**
     *  Constructs a new
     *  AbstractQueryBallotConstrainerEnablerLookupSession.
     *
     *  @param querySession the underlying ballot constrainer enabler
     *         query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryBallotConstrainerEnablerLookupSession(org.osid.voting.rules.BallotConstrainerEnablerQuerySession querySession) {
        nullarg(querySession, "ballot constrainer enabler query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Polls</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this session.
     *
     *  @return the <code>Polls</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform
     *  <code>BallotConstrainerEnabler</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBallotConstrainerEnablers() {
        return (this.session.canSearchBallotConstrainerEnablers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include ballot constrainer enablers in pollses which
     *  are children of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only active ballot constrainer enablers are returned by
     *  methods in this session.
     */
     
    @OSID @Override
    public void useActiveBallotConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive ballot constrainer enablers are returned
     *  by methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusBallotConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>BallotConstrainerEnabler</code> specified by
     *  its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>BallotConstrainerEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>BallotConstrainerEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, ballot constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  ballot constrainer enablers are returned.
     *
     *  @param  ballotConstrainerEnablerId <code>Id</code> of the
     *          <code>BallotConstrainerEnabler</code>
     *  @return the ballot constrainer enabler
     *  @throws org.osid.NotFoundException <code>ballotConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>ballotConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnabler getBallotConstrainerEnabler(org.osid.id.Id ballotConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerEnablerQuery query = getQuery();
        query.matchId(ballotConstrainerEnablerId, true);
        org.osid.voting.rules.BallotConstrainerEnablerList ballotConstrainerEnablers = this.session.getBallotConstrainerEnablersByQuery(query);
        if (ballotConstrainerEnablers.hasNext()) {
            return (ballotConstrainerEnablers.getNextBallotConstrainerEnabler());
        } 
        
        throw new org.osid.NotFoundException(ballotConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  ballotConstrainerEnablers specified in the <code>Id</code>
     *  list, in the order of the list, including duplicates, or an
     *  error results if an <code>Id</code> in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible
     *  <code>BallotConstrainerEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, ballot constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  ballot constrainer enablers are returned.
     *
     *  @param  ballotConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByIds(org.osid.id.IdList ballotConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerEnablerQuery query = getQuery();

        try (org.osid.id.IdList ids = ballotConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getBallotConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> corresponding
     *  to the given ballot constrainer enabler genus
     *  <code>Type</code> which does not include ballot constrainer
     *  enablers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known ballot
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those ballot constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, ballot constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  ballot constrainer enablers are returned.
     *
     *  @param  ballotConstrainerEnablerGenusType a ballotConstrainerEnabler genus type 
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByGenusType(org.osid.type.Type ballotConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerEnablerQuery query = getQuery();
        query.matchGenusType(ballotConstrainerEnablerGenusType, true);
        return (this.session.getBallotConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> corresponding
     *  to the given ballot constrainer enabler genus
     *  <code>Type</code> and include any additional ballot
     *  constrainer enablers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known ballot
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those ballot constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, ballot constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  ballot constrainer enablers are returned.
     *
     *  @param  ballotConstrainerEnablerGenusType a ballotConstrainerEnabler genus type 
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerGenusType</code> is
     *          <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByParentGenusType(org.osid.type.Type ballotConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerEnablerQuery query = getQuery();
        query.matchParentGenusType(ballotConstrainerEnablerGenusType, true);
        return (this.session.getBallotConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> containing
     *  the given ballot constrainer enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known ballot
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those ballot constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, ballot constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  ballot constrainer enablers are returned.
     *
     *  @param  ballotConstrainerEnablerRecordType a ballotConstrainerEnabler record type 
     *  @return the returned <code>BallotConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersByRecordType(org.osid.type.Type ballotConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerEnablerQuery query = getQuery();
        query.matchRecordType(ballotConstrainerEnablerRecordType, true);
        return (this.session.getBallotConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets a <code>BallotConstrainerEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known ballot
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those ballot constrainer
     *  enablers that are accessible through this session.
     *  
     *  In active mode, ballot constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  ballot constrainer enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>BallotConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerEnablerQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getBallotConstrainerEnablersByQuery(query));
    }
        
    
    /**
     *  Gets all <code>BallotConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known ballot
     *  constrainer enablers or an error results. Otherwise, the
     *  returned list may contain only those ballot constrainer
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, ballot constrainer enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  ballot constrainer enablers are returned.
     *
     *  @return a list of <code>BallotConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerEnablerList getBallotConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.voting.rules.BallotConstrainerEnablerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getBallotConstrainerEnablersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.voting.rules.BallotConstrainerEnablerQuery getQuery() {
        org.osid.voting.rules.BallotConstrainerEnablerQuery query = this.session.getBallotConstrainerEnablerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
