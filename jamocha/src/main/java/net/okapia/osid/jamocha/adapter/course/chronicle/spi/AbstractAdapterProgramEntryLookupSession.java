//
// AbstractAdapterProgramEntryLookupSession.java
//
//    A ProgramEntry lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A ProgramEntry lookup session adapter.
 */

public abstract class AbstractAdapterProgramEntryLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.chronicle.ProgramEntryLookupSession {

    private final org.osid.course.chronicle.ProgramEntryLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProgramEntryLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProgramEntryLookupSession(org.osid.course.chronicle.ProgramEntryLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code ProgramEntry} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProgramEntries() {
        return (this.session.canLookupProgramEntries());
    }


    /**
     *  A complete view of the {@code ProgramEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProgramEntryView() {
        this.session.useComparativeProgramEntryView();
        return;
    }


    /**
     *  A complete view of the {@code ProgramEntry} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProgramEntryView() {
        this.session.usePlenaryProgramEntryView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include program entries in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only program entries whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProgramEntryView() {
        this.session.useEffectiveProgramEntryView();
        return;
    }
    

    /**
     *  All program entries of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProgramEntryView() {
        this.session.useAnyEffectiveProgramEntryView();
        return;
    }

     
    /**
     *  Gets the {@code ProgramEntry} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ProgramEntry} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ProgramEntry} and
     *  retained for compatibility.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param programEntryId {@code Id} of the {@code ProgramEntry}
     *  @return the program entry
     *  @throws org.osid.NotFoundException {@code programEntryId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code programEntryId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntry getProgramEntry(org.osid.id.Id programEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntry(programEntryId));
    }


    /**
     *  Gets a {@code ProgramEntryList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  programEntries specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ProgramEntries} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param  programEntryIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ProgramEntry} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code programEntryIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByIds(org.osid.id.IdList programEntryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesByIds(programEntryIds));
    }


    /**
     *  Gets a {@code ProgramEntryList} corresponding to the given
     *  program entry genus {@code Type} which does not include
     *  program entries of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param  programEntryGenusType a programEntry genus type 
     *  @return the returned {@code ProgramEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code programEntryGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByGenusType(org.osid.type.Type programEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesByGenusType(programEntryGenusType));
    }


    /**
     *  Gets a {@code ProgramEntryList} corresponding to the given
     *  program entry genus {@code Type} and include any additional
     *  program entries with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param  programEntryGenusType a programEntry genus type 
     *  @return the returned {@code ProgramEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code programEntryGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByParentGenusType(org.osid.type.Type programEntryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesByParentGenusType(programEntryGenusType));
    }


    /**
     *  Gets a {@code ProgramEntryList} containing the given
     *  program entry record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param  programEntryRecordType a programEntry record type 
     *  @return the returned {@code ProgramEntry} list
     *  @throws org.osid.NullArgumentException
     *          {@code programEntryRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesByRecordType(org.osid.type.Type programEntryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesByRecordType(programEntryRecordType));
    }


    /**
     *  Gets a {@code ProgramEntryList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *  
     *  In active mode, program entries are returned that are currently
     *  active. In any status mode, active and inactive program entries
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ProgramEntry} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesOnDate(org.osid.calendaring.DateTime from, 
                                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesOnDate(from, to));
    }
        

    /**
     *  Gets a list of program entries corresponding to a student
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible through
     *  this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @return the returned {@code ProgramEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudent(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesForStudent(resourceId));
    }


    /**
     *  Gets a list of program entries corresponding to a student
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProgramEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudentOnDate(org.osid.id.Id resourceId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesForStudentOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of program entries corresponding to a program
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  programId the {@code Id} of the program
     *  @return the returned {@code ProgramEntryList}
     *  @throws org.osid.NullArgumentException {@code programId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForProgram(org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesForProgram(programId));
    }


    /**
     *  Gets a list of program entries corresponding to a program
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  programId the {@code Id} of the program
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProgramEntryList}
     *  @throws org.osid.NullArgumentException {@code programId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForProgramOnDate(org.osid.id.Id programId,
                                                                                        org.osid.calendaring.DateTime from,
                                                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesForProgramOnDate(programId, from, to));
    }


    /**
     *  Gets a list of program entries corresponding to student and program
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are
     *  currently effective.  In any effective mode, effective
     *  program entries and those currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the student
     *  @param  programId the {@code Id} of the program
     *  @return the returned {@code ProgramEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code programId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudentAndProgram(org.osid.id.Id resourceId,
                                                                                            org.osid.id.Id programId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesForStudentAndProgram(resourceId, programId));
    }


    /**
     *  Gets a list of program entries corresponding to student and program
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible
     *  through this session.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective. In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @param  programId the {@code Id} of the program
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ProgramEntryList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code programId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntriesForStudentAndProgramOnDate(org.osid.id.Id resourceId,
                                                                                                  org.osid.id.Id programId,
                                                                                                  org.osid.calendaring.DateTime from,
                                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntriesForStudentAndProgramOnDate(resourceId, programId, from, to));
    }


    /**
     *  Gets all {@code ProgramEntries}. 
     *
     *  In plenary mode, the returned list contains all known
     *  program entries or an error results. Otherwise, the returned list
     *  may contain only those program entries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, program entries are returned that are currently
     *  effective.  In any effective mode, effective program entries and
     *  those currently expired are returned.
     *
     *  @return a list of {@code ProgramEntries} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.ProgramEntryList getProgramEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProgramEntries());
    }
}
