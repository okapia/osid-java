//
// AbstractMapCreditLookupSession
//
//    A simple framework for providing a Credit lookup service
//    backed by a fixed collection of credits.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Credit lookup service backed by a
 *  fixed collection of credits. The credits are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Credits</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCreditLookupSession
    extends net.okapia.osid.jamocha.acknowledgement.spi.AbstractCreditLookupSession
    implements org.osid.acknowledgement.CreditLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.acknowledgement.Credit> credits = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.acknowledgement.Credit>());


    /**
     *  Makes a <code>Credit</code> available in this session.
     *
     *  @param  credit a credit
     *  @throws org.osid.NullArgumentException <code>credit<code>
     *          is <code>null</code>
     */

    protected void putCredit(org.osid.acknowledgement.Credit credit) {
        this.credits.put(credit.getId(), credit);
        return;
    }


    /**
     *  Makes an array of credits available in this session.
     *
     *  @param  credits an array of credits
     *  @throws org.osid.NullArgumentException <code>credits<code>
     *          is <code>null</code>
     */

    protected void putCredits(org.osid.acknowledgement.Credit[] credits) {
        putCredits(java.util.Arrays.asList(credits));
        return;
    }


    /**
     *  Makes a collection of credits available in this session.
     *
     *  @param  credits a collection of credits
     *  @throws org.osid.NullArgumentException <code>credits<code>
     *          is <code>null</code>
     */

    protected void putCredits(java.util.Collection<? extends org.osid.acknowledgement.Credit> credits) {
        for (org.osid.acknowledgement.Credit credit : credits) {
            this.credits.put(credit.getId(), credit);
        }

        return;
    }


    /**
     *  Removes a Credit from this session.
     *
     *  @param  creditId the <code>Id</code> of the credit
     *  @throws org.osid.NullArgumentException <code>creditId<code> is
     *          <code>null</code>
     */

    protected void removeCredit(org.osid.id.Id creditId) {
        this.credits.remove(creditId);
        return;
    }


    /**
     *  Gets the <code>Credit</code> specified by its <code>Id</code>.
     *
     *  @param  creditId <code>Id</code> of the <code>Credit</code>
     *  @return the credit
     *  @throws org.osid.NotFoundException <code>creditId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>creditId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Credit getCredit(org.osid.id.Id creditId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.acknowledgement.Credit credit = this.credits.get(creditId);
        if (credit == null) {
            throw new org.osid.NotFoundException("credit not found: " + creditId);
        }

        return (credit);
    }


    /**
     *  Gets all <code>Credits</code>. In plenary mode, the returned
     *  list contains all known credits or an error
     *  results. Otherwise, the returned list may contain only those
     *  credits that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Credits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.CreditList getCredits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.acknowledgement.credit.ArrayCreditList(this.credits.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.credits.clear();
        super.close();
        return;
    }
}
