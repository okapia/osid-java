//
// AbstractEntry.java
//
//     Defines an Entry.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.entry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Entry</code>.
 */

public abstract class AbstractEntry
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.inventory.shipment.Entry {

    private org.osid.inventory.Stock stock;
    private org.osid.inventory.Model model;
    private org.osid.inventory.Item item;
    private java.math.BigDecimal quantity;
    private org.osid.type.Type unitType;

    private final java.util.Collection<org.osid.inventory.shipment.records.EntryRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the stock <code> Id </code> associated with this entry. 
     *
     *  @return the stock <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStockId() {
        return (this.stock.getId());
    }


    /**
     *  Gets the stock associated with this entry. 
     *
     *  @return the stock 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Stock getStock()
        throws org.osid.OperationFailedException {

        return (this.stock);
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @throws org.osid.NullArgumentException
     *          <code>stock</code> is <code>null</code>
     */

    protected void setStock(org.osid.inventory.Stock stock) {
        nullarg(stock, "stock");
        this.stock = stock;
        return;
    }


    /**
     *  Tests if this entry applied to a specific model. 
     *
     *  @return <code> true </code> if applies to a specific model, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasModel() {
        return (this.model != null);
    }


    /**
     *  Gets the model <code> Id </code> associated with this entry. 
     *
     *  @return the model <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasModel() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getModelId() {
        if (!hasModel()) {
            throw new org.osid.IllegalStateException("hasModel() is false");
        }

        return (this.model.getId());
    }


    /**
     *  Gets the model associated with this entry. 
     *
     *  @return the model 
     *  @throws org.osid.IllegalStateException <code> hasModel() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Model getModel()
        throws org.osid.OperationFailedException {

        if (!hasModel()) {
            throw new org.osid.IllegalStateException("hasModel() is false");
        }

        return (this.model);
    }


    /**
     *  Sets the model.
     *
     *  @param model a model
     *  @throws org.osid.NullArgumentException
     *          <code>model</code> is <code>null</code>
     */

    protected void setModel(org.osid.inventory.Model model) {
        nullarg(model, "model");
        this.model = model;
        return;
    }


    /**
     *  Tests if this entry applied to a specific item. 
     *
     *  @return <code> true </code> if applies to a specific item, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasItem() {
        return (this.item != null);
    }


    /**
     *  Gets the item <code> Id </code> associated with this entry. 
     *
     *  @return the model <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasModel() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getItemId() {
        if (!hasItem()) {
            throw new org.osid.IllegalStateException("hasItem() is false");
        }

        return (this.item.getId());
    }


    /**
     *  Gets the item associated with this entry. 
     *
     *  @return the item 
     *  @throws org.osid.IllegalStateException <code> hasItem() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Item getItem()
        throws org.osid.OperationFailedException {

        if (!hasItem()) {
            throw new org.osid.IllegalStateException("hasItem() is false");
        }

        return (this.item);
    }


    /**
     *  Sets the item.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException
     *          <code>item</code> is <code>null</code>
     */

    protected void setItem(org.osid.inventory.Item item) {
        nullarg(item, "item");
        this.item = item;
        return;
    }


    /**
     *  Gets the quantity of the item. 
     *
     *  @return the quantity 
     */

    @OSID @Override
    public java.math.BigDecimal getQuantity() {
        return (this.quantity);
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @throws org.osid.NullArgumentException
     *          <code>quantity</code> is <code>null</code>
     */

    protected void setQuantity(java.math.BigDecimal quantity) {
        nullarg(quantity, "quantity");
        this.quantity = quantity;
        return;
    }


    /**
     *  Gets the units of the quantity. 
     *
     *  @return the unit type 
     */

    @OSID @Override
    public org.osid.type.Type getUnitType() {
        return (this.unitType);
    }


    /**
     *  Sets the unit type.
     *
     *  @param unitType an unit type
     *  @throws org.osid.NullArgumentException
     *          <code>unitType</code> is <code>null</code>
     */

    protected void setUnitType(org.osid.type.Type unitType) {
        nullarg(unitType, "unit type");
        this.unitType = unitType;
        return;
    }


    /**
     *  Tests if this entry supports the given record
     *  <code>Type</code>.
     *
     *  @param  entryRecordType an entry record type 
     *  @return <code>true</code> if the entryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type entryRecordType) {
        for (org.osid.inventory.shipment.records.EntryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Entry</code>
     *  record <code>Type</code>.
     *
     *  @param  entryRecordType the entry record type 
     *  @return the entry record 
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(entryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.EntryRecord getEntryRecord(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.EntryRecord record : this.records) {
            if (record.implementsRecordType(entryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(entryRecordType + " is not supported");
    }


    /**
     *  Adds a record to this entry. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param entryRecord the entry record
     *  @param entryRecordType entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecord</code> or
     *          <code>entryRecordTypeentry</code> is
     *          <code>null</code>
     */
            
    protected void addEntryRecord(org.osid.inventory.shipment.records.EntryRecord entryRecord, 
                                  org.osid.type.Type entryRecordType) {

        nullarg(entryRecord, "entry record");
        addRecordType(entryRecordType);
        this.records.add(entryRecord);
        
        return;
    }
}
