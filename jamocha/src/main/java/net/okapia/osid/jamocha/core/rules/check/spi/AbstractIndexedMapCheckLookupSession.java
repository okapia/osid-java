//
// AbstractIndexedMapCheckLookupSession.java
//
//    A simple framework for providing a Check lookup service
//    backed by a fixed collection of checks with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Check lookup service backed by a
 *  fixed collection of checks. The checks are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some checks may be compatible
 *  with more types than are indicated through these check
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Checks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCheckLookupSession
    extends AbstractMapCheckLookupSession
    implements org.osid.rules.check.CheckLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.rules.check.Check> checksByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.rules.check.Check>());
    private final MultiMap<org.osid.type.Type, org.osid.rules.check.Check> checksByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.rules.check.Check>());


    /**
     *  Makes a <code>Check</code> available in this session.
     *
     *  @param  check a check
     *  @throws org.osid.NullArgumentException <code>check<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCheck(org.osid.rules.check.Check check) {
        super.putCheck(check);

        this.checksByGenus.put(check.getGenusType(), check);
        
        try (org.osid.type.TypeList types = check.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.checksByRecord.put(types.getNextType(), check);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a check from this session.
     *
     *  @param checkId the <code>Id</code> of the check
     *  @throws org.osid.NullArgumentException <code>checkId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCheck(org.osid.id.Id checkId) {
        org.osid.rules.check.Check check;
        try {
            check = getCheck(checkId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.checksByGenus.remove(check.getGenusType());

        try (org.osid.type.TypeList types = check.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.checksByRecord.remove(types.getNextType(), check);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCheck(checkId);
        return;
    }


    /**
     *  Gets a <code>CheckList</code> corresponding to the given
     *  check genus <code>Type</code> which does not include
     *  checks of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known checks or an error results. Otherwise,
     *  the returned list may contain only those checks that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  checkGenusType a check genus type 
     *  @return the returned <code>Check</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checkGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByGenusType(org.osid.type.Type checkGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.check.check.ArrayCheckList(this.checksByGenus.get(checkGenusType)));
    }


    /**
     *  Gets a <code>CheckList</code> containing the given
     *  check record <code>Type</code>. In plenary mode, the
     *  returned list contains all known checks or an error
     *  results. Otherwise, the returned list may contain only those
     *  checks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  checkRecordType a check record type 
     *  @return the returned <code>check</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>checkRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.CheckList getChecksByRecordType(org.osid.type.Type checkRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.check.check.ArrayCheckList(this.checksByRecord.get(checkRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.checksByGenus.clear();
        this.checksByRecord.clear();

        super.close();

        return;
    }
}
