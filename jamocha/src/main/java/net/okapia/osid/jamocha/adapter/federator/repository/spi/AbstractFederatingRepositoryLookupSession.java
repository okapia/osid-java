//
// AbstractFederatingRepositoryLookupSession.java
//
//     An abstract federating adapter for a RepositoryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.repository.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RepositoryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRepositoryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.repository.RepositoryLookupSession>
    implements org.osid.repository.RepositoryLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingRepositoryLookupSession</code>.
     */

    protected AbstractFederatingRepositoryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.repository.RepositoryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Repository</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRepositories() {
        for (org.osid.repository.RepositoryLookupSession session : getSessions()) {
            if (session.canLookupRepositories()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Repository</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRepositoryView() {
        for (org.osid.repository.RepositoryLookupSession session : getSessions()) {
            session.useComparativeRepositoryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Repository</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRepositoryView() {
        for (org.osid.repository.RepositoryLookupSession session : getSessions()) {
            session.usePlenaryRepositoryView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Repository</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Repository</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Repository</code> and
     *  retained for compatibility.
     *
     *  @param  repositoryId <code>Id</code> of the
     *          <code>Repository</code>
     *  @return the repository
     *  @throws org.osid.NotFoundException <code>repositoryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>repositoryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.Repository getRepository(org.osid.id.Id repositoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.repository.RepositoryLookupSession session : getSessions()) {
            try {
                return (session.getRepository(repositoryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(repositoryId + " not found");
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  repositories specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Repositories</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  repositoryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByIds(org.osid.id.IdList repositoryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.repository.repository.MutableRepositoryList ret = new net.okapia.osid.jamocha.repository.repository.MutableRepositoryList();

        try (org.osid.id.IdList ids = repositoryIds) {
            while (ids.hasNext()) {
                ret.addRepository(getRepository(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  repository genus <code>Type</code> which does not include
     *  repositories of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  repositoryGenusType a repository genus type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByGenusType(org.osid.type.Type repositoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.repository.FederatingRepositoryList ret = getRepositoryList();

        for (org.osid.repository.RepositoryLookupSession session : getSessions()) {
            ret.addRepositoryList(session.getRepositoriesByGenusType(repositoryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  repository genus <code>Type</code> and include any additional
     *  repositories with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  repositoryGenusType a repository genus type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByParentGenusType(org.osid.type.Type repositoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.repository.FederatingRepositoryList ret = getRepositoryList();

        for (org.osid.repository.RepositoryLookupSession session : getSessions()) {
            ret.addRepositoryList(session.getRepositoriesByParentGenusType(repositoryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RepositoryList</code> containing the given
     *  repository record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  repositoryRecordType a repository record type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByRecordType(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.repository.FederatingRepositoryList ret = getRepositoryList();

        for (org.osid.repository.RepositoryLookupSession session : getSessions()) {
            ret.addRepositoryList(session.getRepositoriesByRecordType(repositoryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RepositoryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known repositories or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  repositories that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Repository</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.repository.repository.FederatingRepositoryList ret = getRepositoryList();

        for (org.osid.repository.RepositoryLookupSession session : getSessions()) {
            ret.addRepositoryList(session.getRepositoriesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Repositories</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  repositories or an error results. Otherwise, the returned list
     *  may contain only those repositories that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Repositories</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositories()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.repository.repository.FederatingRepositoryList ret = getRepositoryList();

        for (org.osid.repository.RepositoryLookupSession session : getSessions()) {
            ret.addRepositoryList(session.getRepositories());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.repository.repository.FederatingRepositoryList getRepositoryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.repository.repository.ParallelRepositoryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.repository.repository.CompositeRepositoryList());
        }
    }
}
