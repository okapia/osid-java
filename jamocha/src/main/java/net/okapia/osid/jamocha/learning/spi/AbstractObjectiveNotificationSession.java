//
// AbstractObjectiveNotificationSession.java
//
//     A template for making ObjectiveNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Objective} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Objective} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for objective entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractObjectiveNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.learning.ObjectiveNotificationSession {

    private boolean federated = false;
    private org.osid.learning.ObjectiveBank objectiveBank = new net.okapia.osid.jamocha.nil.learning.objectivebank.UnknownObjectiveBank();


    /**
     *  Gets the {@code ObjectiveBank/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code ObjectiveBank Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getObjectiveBankId() {
        return (this.objectiveBank.getId());
    }

    
    /**
     *  Gets the {@code ObjectiveBank} associated with this session.
     *
     *  @return the {@code ObjectiveBank} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.objectiveBank);
    }


    /**
     *  Sets the {@code ObjectiveBank}.
     *
     *  @param objectiveBank the objective bank for this session
     *  @throws org.osid.NullArgumentException {@code objectiveBank}
     *          is {@code null}
     */

    protected void setObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        nullarg(objectiveBank, "objective bank");
        this.objectiveBank = objectiveBank;
        return;
    }


    /**
     *  Tests if this user can register for {@code Objective}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForObjectiveNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeObjectiveNotification() </code>.
     */

    @OSID @Override
    public void reliableObjectiveNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableObjectiveNotifications() {
        return;
    }


    /**
     *  Acknowledge an objective notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeObjectiveNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include objectives in objective banks which are
     *  children of this objective bank in the objective bank
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedObjectiveBankView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this objective bank only.
     */

    @OSID @Override
    public void useIsolatedObjectiveBankView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new objectives. {@code
     *  ObjectiveReceiver.newObjective()} is invoked when an new
     *  {@code Objective} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewObjectives()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated objectives. {@code
     *  ObjectiveReceiver.changedObjective()} is invoked when an
     *  objective is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedObjectives()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated objective. {@code
     *  ObjectiveReceiver.changedObjective()} is invoked when the
     *  specified objective is changed.
     *
     *  @param objectiveId the {@code Id} of the {@code Objective} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code objectiveId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedObjective(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted objectives. {@code
     *  ObjectiveReceiver.deletedObjective()} is invoked when an
     *  objective is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedObjectives()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted objective. {@code
     *  ObjectiveReceiver.deletedObjective()} is invoked when the
     *  specified objective is deleted.
     *
     *  @param objectiveId the {@code Id} of the
     *          {@code Objective} to monitor
     *  @throws org.osid.NullArgumentException {@code objectiveId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedObjective(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated objective hierarchy
     *  structure.  <code> ObjectiveReceiver.changedChildOfObjectives()
     *  </code> is invoked when a node experiences a change in its
     *  children.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedObjectiveHierarchy()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated objective hierarchy
     *  structure.  <code> ObjectiveReceiver.changedChildOfObjectives()
     *  </code> is invoked when the specified node or any of its
     *  ancestors experiences a change in its children.
     *
     *  @param objectiveId the <code> Id </code> of the <code> Objective
     *          </code> node to monitor
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedObjectiveHierarchyForAncestors(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated objective hierarchy
     *  structure.  <code> ObjectiveReceiver.changedChildOfObjectives()
     *  </code> is invoked when the specified node or any of its
     *  descendants experiences a change in its children.
     *
     *  @param objectiveId the <code> Id </code> of the <code> Objective
     *          </code> node to monitor
     *  @throws org.osid.NullArgumentException <code> objectiveId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedObjectiveHierarchyForDescendants(org.osid.id.Id objectiveId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }    
}
