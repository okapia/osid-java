//
// AbstractProgramEntry.java
//
//     Defines a ProgramEntry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.chronicle.programentry.spi;


/**
 *  Defines a <code>ProgramEntry</code> builder.
 */

public abstract class AbstractProgramEntryBuilder<T extends AbstractProgramEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRelationshipBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.course.chronicle.programentry.ProgramEntryMiter programEntry;


    /**
     *  Constructs a new <code>AbstractProgramEntryBuilder</code>.
     *
     *  @param programEntry the program entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractProgramEntryBuilder(net.okapia.osid.jamocha.builder.course.chronicle.programentry.ProgramEntryMiter programEntry) {
        super(programEntry);
        this.programEntry = programEntry;
        return;
    }


    /**
     *  Builds the program entry.
     *
     *  @return the new program entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.course.chronicle.ProgramEntry build() {
        (new net.okapia.osid.jamocha.builder.validator.course.chronicle.programentry.ProgramEntryValidator(getValidations())).validate(this.programEntry);
        return (new net.okapia.osid.jamocha.builder.course.chronicle.programentry.ImmutableProgramEntry(this.programEntry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the program entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.course.chronicle.programentry.ProgramEntryMiter getMiter() {
        return (this.programEntry);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    public T student(org.osid.resource.Resource student) {
        getMiter().setStudent(student);
        return (self());
    }


    /**
     *  Sets the program.
     *
     *  @param program a program
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>program</code> is
     *          <code>null</code>
     */

    public T program(org.osid.course.program.Program program) {
        getMiter().setProgram(program);
        return (self());
    }


    /**
     *  Sets the admission date.
     *
     *  @param date an admission date
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public T admissionDate(org.osid.calendaring.DateTime date) {
        getMiter().setAdmissionDate(date);
        return (self());
    }


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public T term(org.osid.course.Term term) {
        getMiter().setTerm(term);
        return (self());
    }


    /**
     *  Sets the credit scale.
     *
     *  @param scale a credit scale
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    public T creditScale(org.osid.grading.GradeSystem scale) {
        getMiter().setCreditScale(scale);
        return (self());
    }


    /**
     *  Sets the credits earned.
     *
     *  @param credits a credits earned
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>credits</code> is
     *          <code>null</code>
     */

    public T creditsEarned(java.math.BigDecimal credits) {
        getMiter().setCreditsEarned(credits);
        return (self());
    }


    /**
     *  Sets the g pa scale.
     *
     *  @param scale a GPA scale
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>scale</code> is
     *          <code>null</code>
     */

    public T gpaScale(org.osid.grading.GradeSystem scale) {
        getMiter().setGPAScale(scale);
        return (self());
    }


    /**
     *  Sets the GPA.
     *
     *  @param gpa a gpa
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>gpa</code> is
     *          <code>null</code>
     */

    public T gpa(java.math.BigDecimal gpa) {
        getMiter().setGPA(gpa);
        return (self());
    }


    /**
     *  Adds an enrollment.
     *
     *  @param enrollment an enrollment
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>enrollment</code>
     *          is <code>null</code>
     */

    public T enrollment(org.osid.course.program.Enrollment enrollment) {
        getMiter().addEnrollment(enrollment);
        return (self());
    }


    /**
     *  Sets all the enrollments.
     *
     *  @param enrollments a collection of enrollments
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>enrollments</code> is <code>null</code>
     */

    public T enrollments(java.util.Collection<org.osid.course.program.Enrollment> enrollments) {
        getMiter().setEnrollments(enrollments);
        return (self());
    }


    /**
     *  Adds a ProgramEntry record.
     *
     *  @param record a program entry record
     *  @param recordType the type of program entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.course.chronicle.records.ProgramEntryRecord record, org.osid.type.Type recordType) {
        getMiter().addProgramEntryRecord(record, recordType);
        return (self());
    }
}       


