//
// AbstractHierarchyNotificationSession.java
//
//     A template for making HierarchyNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hierarchy.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Hierarchy} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Hierarchy} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for hierarchy entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractHierarchyNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.hierarchy.HierarchyNotificationSession {


    /**
     *  Tests if this user can register for {@code Hierarchy}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForHierarchyNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode, notifications 
     *  are to be acknowledged using <code> acknowledgeHierarchyNotification() 
     *  </code>. 
     */

    @OSID @Override
    public void reliableHierarchyNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode, 
     *  notifications do not need to be acknowledged. 
     */

    @OSID @Override
    public void unreliableHierarchyNotifications() {
        return;
    }


    /**
     *  Acknowledge a hierarchy notification. 
     *
     *  @param  notificationId the <code> Id </code> of the notification 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void acknowledgeHierarchyNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of new hierarchies. {@code
     *  HierarchyReceiver.newHierarchy()} is invoked when a new {@code
     *  Hierarchy} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewHierarchies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated hierarchies. {@code
     *  HierarchyReceiver.changedHierarchy()} is invoked when a
     *  hierarchy is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedHierarchies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated hierarchy. {@code
     *  HierarchyReceiver.changedHierarchy()} is invoked when the
     *  specified hierarchy is changed.
     *
     *  @param hierarchyId the {@code Id} of the {@code Hierarchy} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code hierarchyId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted hierarchies. {@code
     *  HierarchyReceiver.deletedHierarchy()} is invoked when a
     *  hierarchy is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedHierarchies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted hierarchy. {@code
     *  HierarchyReceiver.deletedHierarchy()} is invoked when the
     *  specified hierarchy is deleted.
     *
     *  @param hierarchyId the {@code Id} of the
     *          {@code Hierarchy} to monitor
     *  @throws org.osid.NullArgumentException {@code hierarchyId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedHierarchy(org.osid.id.Id hierarchyId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
