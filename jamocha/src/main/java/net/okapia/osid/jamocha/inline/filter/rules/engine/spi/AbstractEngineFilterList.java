//
// AbstractEngineList
//
//     Implements a filter for an EngineList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.rules.engine.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an EngineList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedEngineList
 *  to improve performance.
 */

public abstract class AbstractEngineFilterList
    extends net.okapia.osid.jamocha.rules.engine.spi.AbstractEngineList
    implements org.osid.rules.EngineList,
               net.okapia.osid.jamocha.inline.filter.rules.engine.EngineFilter {

    private org.osid.rules.Engine engine;
    private final org.osid.rules.EngineList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractEngineFilterList</code>.
     *
     *  @param engineList an <code>EngineList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>engineList</code> is <code>null</code>
     */

    protected AbstractEngineFilterList(org.osid.rules.EngineList engineList) {
        nullarg(engineList, "engine list");
        this.list = engineList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.engine == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Engine </code> in this list. 
     *
     *  @return the next <code> Engine </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Engine </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.rules.Engine getNextEngine()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.rules.Engine engine = this.engine;
            this.engine = null;
            return (engine);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in engine list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.engine = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Engines.
     *
     *  @param engine the engine to filter
     *  @return <code>true</code> if the engine passes the filter,
     *          <code>false</code> if the engine should be filtered
     */

    public abstract boolean pass(org.osid.rules.Engine engine);


    protected void prime() {
        if (this.engine != null) {
            return;
        }

        org.osid.rules.Engine engine = null;

        while (this.list.hasNext()) {
            try {
                engine = this.list.getNextEngine();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(engine)) {
                this.engine = engine;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
