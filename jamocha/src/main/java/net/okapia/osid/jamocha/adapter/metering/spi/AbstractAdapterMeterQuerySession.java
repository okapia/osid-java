//
// AbstractQueryMeterLookupSession.java
//
//    A MeterQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.metering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A MeterQuerySession adapter.
 */

public abstract class AbstractAdapterMeterQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.metering.MeterQuerySession {

    private final org.osid.metering.MeterQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterMeterQuerySession.
     *
     *  @param session the underlying meter query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterMeterQuerySession(org.osid.metering.MeterQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeUtility</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeUtility Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getUtilityId() {
        return (this.session.getUtilityId());
    }


    /**
     *  Gets the {@codeUtility</code> associated with this 
     *  session.
     *
     *  @return the {@codeUtility</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.metering.Utility getUtility()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getUtility());
    }


    /**
     *  Tests if this user can perform {@codeMeter</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchMeters() {
        return (this.session.canSearchMeters());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include meters in utilities which are children
     *  of this utility in the utility hierarchy.
     */

    @OSID @Override
    public void useFederatedUtilityView() {
        this.session.useFederatedUtilityView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this utility only.
     */
    
    @OSID @Override
    public void useIsolatedUtilityView() {
        this.session.useIsolatedUtilityView();
        return;
    }
    
      
    /**
     *  Gets a meter query. The returned query will not have an
     *  extension query.
     *
     *  @return the meter query 
     */
      
    @OSID @Override
    public org.osid.metering.MeterQuery getMeterQuery() {
        return (this.session.getMeterQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  meterQuery the meter query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code meterQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code meterQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.metering.MeterList getMetersByQuery(org.osid.metering.MeterQuery meterQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getMetersByQuery(meterQuery));
    }
}
