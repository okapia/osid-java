//
// AbstractAssemblyRealmQuery.java
//
//     A RealmQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.personnel.realm.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RealmQuery that stores terms.
 */

public abstract class AbstractAssemblyRealmQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.personnel.RealmQuery,
               org.osid.personnel.RealmQueryInspector,
               org.osid.personnel.RealmSearchOrder {

    private final java.util.Collection<org.osid.personnel.records.RealmQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.RealmQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.personnel.records.RealmSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyRealmQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyRealmQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the person <code> Id </code> for this query to match persons 
     *  assigned to realms. 
     *
     *  @param  personId a person <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> personId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPersonId(org.osid.id.Id personId, boolean match) {
        getAssembler().addIdTerm(getPersonIdColumn(), personId, match);
        return;
    }


    /**
     *  Clears all person <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPersonIdTerms() {
        getAssembler().clearTerms(getPersonIdColumn());
        return;
    }


    /**
     *  Gets the person <code> Id </code> query terms. 
     *
     *  @return the person <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPersonIdTerms() {
        return (getAssembler().getIdTerms(getPersonIdColumn()));
    }


    /**
     *  Gets the PersonId column name.
     *
     * @return the column name
     */

    protected String getPersonIdColumn() {
        return ("person_id");
    }


    /**
     *  Tests if a person query is available. 
     *
     *  @return <code> true </code> if a person query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPersonQuery() {
        return (false);
    }


    /**
     *  Gets the query for a person. 
     *
     *  @return the person query 
     *  @throws org.osid.UnimplementedException <code> supportsPersonQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PersonQuery getPersonQuery() {
        throw new org.osid.UnimplementedException("supportsPersonQuery() is false");
    }


    /**
     *  Matches realms with any person. 
     *
     *  @param  match <code> true </code> to match realms with any person, 
     *          <code> false </code> to match realms with no persons 
     */

    @OSID @Override
    public void matchAnyPerson(boolean match) {
        getAssembler().addIdWildcardTerm(getPersonColumn(), match);
        return;
    }


    /**
     *  Clears all person terms. 
     */

    @OSID @Override
    public void clearPersonTerms() {
        getAssembler().clearTerms(getPersonColumn());
        return;
    }


    /**
     *  Gets the person query terms. 
     *
     *  @return the person terms 
     */

    @OSID @Override
    public org.osid.personnel.PersonQueryInspector[] getPersonTerms() {
        return (new org.osid.personnel.PersonQueryInspector[0]);
    }


    /**
     *  Gets the Person column name.
     *
     * @return the column name
     */

    protected String getPersonColumn() {
        return ("person");
    }


    /**
     *  Sets the organization <code> Id </code> for this query to match 
     *  organizations assigned to realms. 
     *
     *  @param  organizationId an organization <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> organizationId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchOrganizationId(org.osid.id.Id organizationId, 
                                    boolean match) {
        getAssembler().addIdTerm(getOrganizationIdColumn(), organizationId, match);
        return;
    }


    /**
     *  Clears all organization <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearOrganizationIdTerms() {
        getAssembler().clearTerms(getOrganizationIdColumn());
        return;
    }


    /**
     *  Gets the organziation <code> Id </code> query terms. 
     *
     *  @return the organziation <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrganizationIdTerms() {
        return (getAssembler().getIdTerms(getOrganizationIdColumn()));
    }


    /**
     *  Gets the OrganizationId column name.
     *
     * @return the column name
     */

    protected String getOrganizationIdColumn() {
        return ("organization_id");
    }


    /**
     *  Tests if a organization query is available. 
     *
     *  @return <code> true </code> if a organization query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOrganizationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an organization. 
     *
     *  @return the organization query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOrganizationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQuery getOrganizationQuery() {
        throw new org.osid.UnimplementedException("supportsOrganizationQuery() is false");
    }


    /**
     *  Matches realms with any organization. 
     *
     *  @param  match <code> true </code> to match realms with any 
     *          organization, <code> false </code> to match realms with no 
     *          organizations 
     */

    @OSID @Override
    public void matchAnyOrganization(boolean match) {
        getAssembler().addIdWildcardTerm(getOrganizationColumn(), match);
        return;
    }


    /**
     *  Clears all organization terms. 
     */

    @OSID @Override
    public void clearOrganizationTerms() {
        getAssembler().clearTerms(getOrganizationColumn());
        return;
    }


    /**
     *  Gets the organziation query terms. 
     *
     *  @return the organziation terms 
     */

    @OSID @Override
    public org.osid.personnel.OrganizationQueryInspector[] getOrganizationTerms() {
        return (new org.osid.personnel.OrganizationQueryInspector[0]);
    }


    /**
     *  Gets the Organization column name.
     *
     * @return the column name
     */

    protected String getOrganizationColumn() {
        return ("organization");
    }


    /**
     *  Sets the position <code> Id </code> for this query to match positions 
     *  assigned to realms. 
     *
     *  @param  positionId a position <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> positionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPositionId(org.osid.id.Id positionId, boolean match) {
        getAssembler().addIdTerm(getPositionIdColumn(), positionId, match);
        return;
    }


    /**
     *  Clears all position <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPositionIdTerms() {
        getAssembler().clearTerms(getPositionIdColumn());
        return;
    }


    /**
     *  Gets the position <code> Id </code> query terms. 
     *
     *  @return the position <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPositionIdTerms() {
        return (getAssembler().getIdTerms(getPositionIdColumn()));
    }


    /**
     *  Gets the PositionId column name.
     *
     * @return the column name
     */

    protected String getPositionIdColumn() {
        return ("position_id");
    }


    /**
     *  Tests if a position query is available. 
     *
     *  @return <code> true </code> if a position query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a position. 
     *
     *  @return the position query 
     *  @throws org.osid.UnimplementedException <code> supportsPositionQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.PositionQuery getPositionQuery() {
        throw new org.osid.UnimplementedException("supportsPositionQuery() is false");
    }


    /**
     *  Matches realms with any position. 
     *
     *  @param  match <code> true </code> to match realms with any position, 
     *          <code> false </code> to match realms with no positions 
     */

    @OSID @Override
    public void matchAnyPosition(boolean match) {
        getAssembler().addIdWildcardTerm(getPositionColumn(), match);
        return;
    }


    /**
     *  Clears all position terms. 
     */

    @OSID @Override
    public void clearPositionTerms() {
        getAssembler().clearTerms(getPositionColumn());
        return;
    }


    /**
     *  Gets the position query terms. 
     *
     *  @return the position terms 
     */

    @OSID @Override
    public org.osid.personnel.PositionQueryInspector[] getPositionTerms() {
        return (new org.osid.personnel.PositionQueryInspector[0]);
    }


    /**
     *  Gets the Position column name.
     *
     * @return the column name
     */

    protected String getPositionColumn() {
        return ("position");
    }


    /**
     *  Sets the appointment <code> Id </code> for this query to match 
     *  positions assigned to realms. 
     *
     *  @param  appointmentId an appointment <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> appointmentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAppointmentId(org.osid.id.Id appointmentId, boolean match) {
        getAssembler().addIdTerm(getAppointmentIdColumn(), appointmentId, match);
        return;
    }


    /**
     *  Clears all appointment <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAppointmentIdTerms() {
        getAssembler().clearTerms(getAppointmentIdColumn());
        return;
    }


    /**
     *  Gets the appointment <code> Id </code> query terms. 
     *
     *  @return the appointment <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAppointmentIdTerms() {
        return (getAssembler().getIdTerms(getAppointmentIdColumn()));
    }


    /**
     *  Gets the AppointmentId column name.
     *
     * @return the column name
     */

    protected String getAppointmentIdColumn() {
        return ("appointment_id");
    }


    /**
     *  Tests if an appointment query is available. 
     *
     *  @return <code> true </code> if an appointment query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAppointmentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an appointment. 
     *
     *  @return the appointment query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAppointmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQuery getAppointmentQuery() {
        throw new org.osid.UnimplementedException("supportsAppointmentQuery() is false");
    }


    /**
     *  Matches realms with any appointment. 
     *
     *  @param  match <code> true </code> to match realms with any 
     *          appointment, <code> false </code> to match realms with no 
     *          appointments 
     */

    @OSID @Override
    public void matchAnyAppointment(boolean match) {
        getAssembler().addIdWildcardTerm(getAppointmentColumn(), match);
        return;
    }


    /**
     *  Clears all appointment terms. 
     */

    @OSID @Override
    public void clearAppointmentTerms() {
        getAssembler().clearTerms(getAppointmentColumn());
        return;
    }


    /**
     *  Gets the appointment query terms. 
     *
     *  @return the appointment terms 
     */

    @OSID @Override
    public org.osid.personnel.AppointmentQueryInspector[] getAppointmentTerms() {
        return (new org.osid.personnel.AppointmentQueryInspector[0]);
    }


    /**
     *  Gets the Appointment column name.
     *
     * @return the column name
     */

    protected String getAppointmentColumn() {
        return ("appointment");
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match realms that 
     *  have the specified realm as an ancestor. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorRealmId(org.osid.id.Id realmId, boolean match) {
        getAssembler().addIdTerm(getAncestorRealmIdColumn(), realmId, match);
        return;
    }


    /**
     *  Clears all ancestor realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorRealmIdTerms() {
        getAssembler().clearTerms(getAncestorRealmIdColumn());
        return;
    }


    /**
     *  Gets the ancestor realm <code> Id </code> query terms. 
     *
     *  @return the ancestor realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorRealmIdTerms() {
        return (getAssembler().getIdTerms(getAncestorRealmIdColumn()));
    }


    /**
     *  Gets the AncestorRealmId column name.
     *
     * @return the column name
     */

    protected String getAncestorRealmIdColumn() {
        return ("ancestor_realm_id");
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorRealmQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getAncestorRealmQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorRealmQuery() is false");
    }


    /**
     *  Matches realms with any ancestor. 
     *
     *  @param  match <code> true </code> to match realms with any ancestor, 
     *          <code> false </code> to match root realms 
     */

    @OSID @Override
    public void matchAnyAncestorRealm(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorRealmColumn(), match);
        return;
    }


    /**
     *  Clears all ancestor realm terms. 
     */

    @OSID @Override
    public void clearAncestorRealmTerms() {
        getAssembler().clearTerms(getAncestorRealmColumn());
        return;
    }


    /**
     *  Gets the ancestor realm query terms. 
     *
     *  @return the ancestor realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getAncestorRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }


    /**
     *  Gets the AncestorRealm column name.
     *
     * @return the column name
     */

    protected String getAncestorRealmColumn() {
        return ("ancestor_realm");
    }


    /**
     *  Sets the realm <code> Id </code> for this query to match realms that 
     *  have the specified realm as a descendant. 
     *
     *  @param  realmId a realm <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> realmId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantRealmId(org.osid.id.Id realmId, boolean match) {
        getAssembler().addIdTerm(getDescendantRealmIdColumn(), realmId, match);
        return;
    }


    /**
     *  Clears all descendant realm <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantRealmIdTerms() {
        getAssembler().clearTerms(getDescendantRealmIdColumn());
        return;
    }


    /**
     *  Gets the descendant realm <code> Id </code> query terms. 
     *
     *  @return the descendant realm <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantRealmIdTerms() {
        return (getAssembler().getIdTerms(getDescendantRealmIdColumn()));
    }


    /**
     *  Gets the DescendantRealmId column name.
     *
     * @return the column name
     */

    protected String getDescendantRealmIdColumn() {
        return ("descendant_realm_id");
    }


    /**
     *  Tests if a <code> RealmQuery </code> is available. 
     *
     *  @return <code> true </code> if a realm query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantRealmQuery() {
        return (false);
    }


    /**
     *  Gets the query for a realm. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the realm query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantRealmQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.personnel.RealmQuery getDescendantRealmQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantRealmQuery() is false");
    }


    /**
     *  Matches realms with any descendant. 
     *
     *  @param  match <code> true </code> to match realms with any descendant, 
     *          <code> false </code> to match leaf realms 
     */

    @OSID @Override
    public void matchAnyDescendantRealm(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantRealmColumn(), match);
        return;
    }


    /**
     *  Clears all descendant realm terms. 
     */

    @OSID @Override
    public void clearDescendantRealmTerms() {
        getAssembler().clearTerms(getDescendantRealmColumn());
        return;
    }


    /**
     *  Gets the descendant realm query terms. 
     *
     *  @return the descendant realm terms 
     */

    @OSID @Override
    public org.osid.personnel.RealmQueryInspector[] getDescendantRealmTerms() {
        return (new org.osid.personnel.RealmQueryInspector[0]);
    }


    /**
     *  Gets the DescendantRealm column name.
     *
     * @return the column name
     */

    protected String getDescendantRealmColumn() {
        return ("descendant_realm");
    }


    /**
     *  Tests if this realm supports the given record
     *  <code>Type</code>.
     *
     *  @param  realmRecordType a realm record type 
     *  @return <code>true</code> if the realmRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>realmRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type realmRecordType) {
        for (org.osid.personnel.records.RealmQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(realmRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  realmRecordType the realm record type 
     *  @return the realm query record 
     *  @throws org.osid.NullArgumentException
     *          <code>realmRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(realmRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.RealmQueryRecord getRealmQueryRecord(org.osid.type.Type realmRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.RealmQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(realmRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(realmRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  realmRecordType the realm record type 
     *  @return the realm query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>realmRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(realmRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.RealmQueryInspectorRecord getRealmQueryInspectorRecord(org.osid.type.Type realmRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.RealmQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(realmRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(realmRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param realmRecordType the realm record type
     *  @return the realm search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>realmRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(realmRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.RealmSearchOrderRecord getRealmSearchOrderRecord(org.osid.type.Type realmRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.personnel.records.RealmSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(realmRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(realmRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this realm. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param realmQueryRecord the realm query record
     *  @param realmQueryInspectorRecord the realm query inspector
     *         record
     *  @param realmSearchOrderRecord the realm search order record
     *  @param realmRecordType realm record type
     *  @throws org.osid.NullArgumentException
     *          <code>realmQueryRecord</code>,
     *          <code>realmQueryInspectorRecord</code>,
     *          <code>realmSearchOrderRecord</code> or
     *          <code>realmRecordTyperealm</code> is
     *          <code>null</code>
     */
            
    protected void addRealmRecords(org.osid.personnel.records.RealmQueryRecord realmQueryRecord, 
                                      org.osid.personnel.records.RealmQueryInspectorRecord realmQueryInspectorRecord, 
                                      org.osid.personnel.records.RealmSearchOrderRecord realmSearchOrderRecord, 
                                      org.osid.type.Type realmRecordType) {

        addRecordType(realmRecordType);

        nullarg(realmQueryRecord, "realm query record");
        nullarg(realmQueryInspectorRecord, "realm query inspector record");
        nullarg(realmSearchOrderRecord, "realm search odrer record");

        this.queryRecords.add(realmQueryRecord);
        this.queryInspectorRecords.add(realmQueryInspectorRecord);
        this.searchOrderRecords.add(realmSearchOrderRecord);
        
        return;
    }
}
