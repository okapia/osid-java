//
// AbstractInstallationContentQuery.java
//
//     A template for making an InstallationContent Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.installation.installationcontent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for installation contents.
 */

public abstract class AbstractInstallationContentQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.installation.InstallationContentQuery {

    private final java.util.Collection<org.osid.installation.records.InstallationContentQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches content whose length of the data in bytes are inclusive of the 
     *  given range. 
     *
     *  @param  low low range 
     *  @param  high high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchDataLength(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches content that has any data length. 
     *
     *  @param  match <code> true </code> to match content with any data 
     *          length, <code> false </code> to match content with no data 
     *          length 
     */

    @OSID @Override
    public void matchAnyDataLength(boolean match) {
        return;
    }


    /**
     *  Clears the data length terms. 
     */

    @OSID @Override
    public void clearDataLengthTerms() {
        return;
    }


    /**
     *  Matches data in this content. 
     *
     *  @param  data list of matching strings 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @param  partial <code> true </code> for a partial match, <code> false 
     *          </code> for a complete match 
     *  @throws org.osid.NullArgumentException <code> data </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchData(byte[] data, boolean match, boolean partial) {
        return;
    }


    /**
     *  Matches content that has any data. 
     *
     *  @param  match <code> true </code> to match content with any data, 
     *          <code> false </code> to match content with no data 
     */

    @OSID @Override
    public void matchAnyData(boolean match) {
        return;
    }


    /**
     *  Clears the data terms. 
     */

    @OSID @Override
    public void clearDataTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given installation content query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an installation content implementing the requested record.
     *
     *  @param installationContentRecordType an installation content record type
     *  @return the installation content query record
     *  @throws org.osid.NullArgumentException
     *          <code>installationContentRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(installationContentRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.installation.records.InstallationContentQueryRecord getInstallationContentQueryRecord(org.osid.type.Type installationContentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.installation.records.InstallationContentQueryRecord record : this.records) {
            if (record.implementsRecordType(installationContentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(installationContentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this installation content query. 
     *
     *  @param installationContentQueryRecord installation content query record
     *  @param installationContentRecordType installationContent record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addInstallationContentQueryRecord(org.osid.installation.records.InstallationContentQueryRecord installationContentQueryRecord, 
                                          org.osid.type.Type installationContentRecordType) {

        addRecordType(installationContentRecordType);
        nullarg(installationContentQueryRecord, "installation content query record");
        this.records.add(installationContentQueryRecord);        
        return;
    }
}
