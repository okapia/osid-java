//
// AbstractQueryMessageLookupSession.java
//
//    A MessageQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A MessageQuerySession adapter.
 */

public abstract class AbstractAdapterMessageQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.messaging.MessageQuerySession {

    private final org.osid.messaging.MessageQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterMessageQuerySession.
     *
     *  @param session the underlying message query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterMessageQuerySession(org.osid.messaging.MessageQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeMailbox</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeMailbox Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMailboxId() {
        return (this.session.getMailboxId());
    }


    /**
     *  Gets the {@codeMailbox</code> associated with this 
     *  session.
     *
     *  @return the {@codeMailbox</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getMailbox());
    }


    /**
     *  Tests if this user can perform {@codeMessage</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchMessages() {
        return (this.session.canSearchMessages());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include messages in mailboxes which are children
     *  of this mailbox in the mailbox hierarchy.
     */

    @OSID @Override
    public void useFederatedMailboxView() {
        this.session.useFederatedMailboxView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this mailbox only.
     */
    
    @OSID @Override
    public void useIsolatedMailboxView() {
        this.session.useIsolatedMailboxView();
        return;
    }
    
      
    /**
     *  Gets a message query. The returned query will not have an
     *  extension query.
     *
     *  @return the message query 
     */
      
    @OSID @Override
    public org.osid.messaging.MessageQuery getMessageQuery() {
        return (this.session.getMessageQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  messageQuery the message query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code messageQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code messageQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.messaging.MessageList getMessagesByQuery(org.osid.messaging.MessageQuery messageQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getMessagesByQuery(messageQuery));
    }
}
