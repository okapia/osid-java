//
// AbstractFederatingProfileItemLookupSession.java
//
//     An abstract federating adapter for a ProfileItemLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.profile.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ProfileItemLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingProfileItemLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.profile.ProfileItemLookupSession>
    implements org.osid.profile.ProfileItemLookupSession {

    private boolean parallel = false;
    private org.osid.profile.Profile profile = new net.okapia.osid.jamocha.nil.profile.profile.UnknownProfile();


    /**
     *  Constructs a new <code>AbstractFederatingProfileItemLookupSession</code>.
     */

    protected AbstractFederatingProfileItemLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.profile.ProfileItemLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Profile/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Profile Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getProfileId() {
        return (this.profile.getId());
    }


    /**
     *  Gets the <code>Profile</code> associated with this 
     *  session.
     *
     *  @return the <code>Profile</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.Profile getProfile()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.profile);
    }


    /**
     *  Sets the <code>Profile</code>.
     *
     *  @param  profile the profile for this session
     *  @throws org.osid.NullArgumentException <code>profile</code>
     *          is <code>null</code>
     */

    protected void setProfile(org.osid.profile.Profile profile) {
        nullarg(profile, "profile");
        this.profile = profile;
        return;
    }


    /**
     *  Tests if this user can perform <code>ProfileItem</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProfileItems() {
        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            if (session.canLookupProfileItems()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ProfileItem</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProfileItemView() {
        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            session.useComparativeProfileItemView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ProfileItem</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProfileItemView() {
        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            session.usePlenaryProfileItemView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include profile items in profiles which are children
     *  of this profile in the profile hierarchy.
     */

    @OSID @Override
    public void useFederatedProfileView() {
        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            session.useFederatedProfileView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this profile only.
     */

    @OSID @Override
    public void useIsolatedProfileView() {
        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            session.useIsolatedProfileView();
        }

        return;
    }

     
    /**
     *  Gets the <code>ProfileItem</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ProfileItem</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ProfileItem</code> and
     *  retained for compatibility.
     *
     *  @param  profileItemId <code>Id</code> of the
     *          <code>ProfileItem</code>
     *  @return the profile item
     *  @throws org.osid.NotFoundException <code>profileItemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>profileItemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItem getProfileItem(org.osid.id.Id profileItemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            try {
                return (session.getProfileItem(profileItemId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(profileItemId + " not found");
    }


    /**
     *  Gets a <code>ProfileItemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  profileItems specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ProfileItems</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  profileItemIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ProfileItem</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByIds(org.osid.id.IdList profileItemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.profile.profileitem.MutableProfileItemList ret = new net.okapia.osid.jamocha.profile.profileitem.MutableProfileItemList();

        try (org.osid.id.IdList ids = profileItemIds) {
            while (ids.hasNext()) {
                ret.addProfileItem(getProfileItem(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ProfileItemList</code> corresponding to the given
     *  profile item genus <code>Type</code> which does not include
     *  profile items of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileItemGenusType a profileItem genus type 
     *  @return the returned <code>ProfileItem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByGenusType(org.osid.type.Type profileItemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileitem.FederatingProfileItemList ret = getProfileItemList();

        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            ret.addProfileItemList(session.getProfileItemsByGenusType(profileItemGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProfileItemList</code> corresponding to the given
     *  profile item genus <code>Type</code> and include any additional
     *  profile items with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileItemGenusType a profileItem genus type 
     *  @return the returned <code>ProfileItem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByParentGenusType(org.osid.type.Type profileItemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileitem.FederatingProfileItemList ret = getProfileItemList();

        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            ret.addProfileItemList(session.getProfileItemsByParentGenusType(profileItemGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProfileItemList</code> containing the given
     *  profile item record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  profileItemRecordType a profileItem record type 
     *  @return the returned <code>ProfileItem</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>profileItemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItemsByRecordType(org.osid.type.Type profileItemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileitem.FederatingProfileItemList ret = getProfileItemList();

        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            ret.addProfileItemList(session.getProfileItemsByRecordType(profileItemRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ProfileItems</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  profile items or an error results. Otherwise, the returned list
     *  may contain only those profile items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ProfileItems</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.profile.ProfileItemList getProfileItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.profile.profileitem.FederatingProfileItemList ret = getProfileItemList();

        for (org.osid.profile.ProfileItemLookupSession session : getSessions()) {
            ret.addProfileItemList(session.getProfileItems());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.profile.profileitem.FederatingProfileItemList getProfileItemList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.profile.profileitem.ParallelProfileItemList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.profile.profileitem.CompositeProfileItemList());
        }
    }
}
