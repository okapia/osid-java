//
// AbstractTimePeriodSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.timeperiod.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractTimePeriodSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.TimePeriodSearchResults {

    private org.osid.calendaring.TimePeriodList timePeriods;
    private final org.osid.calendaring.TimePeriodQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.records.TimePeriodSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractTimePeriodSearchResults.
     *
     *  @param timePeriods the result set
     *  @param timePeriodQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>timePeriods</code>
     *          or <code>timePeriodQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractTimePeriodSearchResults(org.osid.calendaring.TimePeriodList timePeriods,
                                            org.osid.calendaring.TimePeriodQueryInspector timePeriodQueryInspector) {
        nullarg(timePeriods, "time periods");
        nullarg(timePeriodQueryInspector, "time period query inspectpr");

        this.timePeriods = timePeriods;
        this.inspector = timePeriodQueryInspector;

        return;
    }


    /**
     *  Gets the time period list resulting from a search.
     *
     *  @return a time period list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriodList getTimePeriods() {
        if (this.timePeriods == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.TimePeriodList timePeriods = this.timePeriods;
        this.timePeriods = null;
	return (timePeriods);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.TimePeriodQueryInspector getTimePeriodQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  time period search record <code> Type. </code> This method must
     *  be used to retrieve a timePeriod implementing the requested
     *  record.
     *
     *  @param timePeriodSearchRecordType a timePeriod search 
     *         record type 
     *  @return the time period search
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriodSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(timePeriodSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.TimePeriodSearchResultsRecord getTimePeriodSearchResultsRecord(org.osid.type.Type timePeriodSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.records.TimePeriodSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(timePeriodSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(timePeriodSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record time period search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addTimePeriodRecord(org.osid.calendaring.records.TimePeriodSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "time period record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
