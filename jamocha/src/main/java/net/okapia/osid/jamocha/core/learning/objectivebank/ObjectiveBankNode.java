//
// ObjectiveBankNode.java
//
//     Defines an ObjectiveBank node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning;


/**
 *  A class for managing a hierarchy of objective bank nodes in core.
 */

public final class ObjectiveBankNode
    extends net.okapia.osid.jamocha.core.learning.spi.AbstractObjectiveBankNode
    implements org.osid.learning.ObjectiveBankNode {


    /**
     *  Constructs a new <code>ObjectiveBankNode</code> from a single
     *  objective bank.
     *
     *  @param objectiveBank the objective bank
     *  @throws org.osid.NullArgumentException <code>objectiveBank</code> is 
     *          <code>null</code>.
     */

    public ObjectiveBankNode(org.osid.learning.ObjectiveBank objectiveBank) {
        super(objectiveBank);
        return;
    }


    /**
     *  Constructs a new <code>ObjectiveBankNode</code>.
     *
     *  @param objectiveBank the objective bank
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>objectiveBank</code>
     *          is <code>null</code>.
     */

    public ObjectiveBankNode(org.osid.learning.ObjectiveBank objectiveBank, boolean root, boolean leaf) {
        super(objectiveBank, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this objective bank.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.learning.ObjectiveBankNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this objective bank.
     *
     *  @param objectiveBank the objective bank to add as a parent
     *  @throws org.osid.NullArgumentException <code>objectiveBank</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.learning.ObjectiveBank objectiveBank) {
        addParent(new ObjectiveBankNode(objectiveBank));
        return;
    }


    /**
     *  Adds a child to this objective bank.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.learning.ObjectiveBankNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this objective bank.
     *
     *  @param objectiveBank the objective bank to add as a child
     *  @throws org.osid.NullArgumentException <code>objectiveBank</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.learning.ObjectiveBank objectiveBank) {
        addChild(new ObjectiveBankNode(objectiveBank));
        return;
    }
}
