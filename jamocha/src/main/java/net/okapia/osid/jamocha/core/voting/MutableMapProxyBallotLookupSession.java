//
// MutableMapProxyBallotLookupSession
//
//    Implements a Ballot lookup service backed by a collection of
//    ballots that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting;


/**
 *  Implements a Ballot lookup service backed by a collection of
 *  ballots. The ballots are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of ballots can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyBallotLookupSession
    extends net.okapia.osid.jamocha.core.voting.spi.AbstractMapBallotLookupSession
    implements org.osid.voting.BallotLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyBallotLookupSession}
     *  with no ballots.
     *
     *  @param polls the polls
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyBallotLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.proxy.Proxy proxy) {
        setPolls(polls);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBallotLookupSession} with a
     *  single ballot.
     *
     *  @param polls the polls
     *  @param ballot a ballot
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballot}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBallotLookupSession(org.osid.voting.Polls polls,
                                                org.osid.voting.Ballot ballot, org.osid.proxy.Proxy proxy) {
        this(polls, proxy);
        putBallot(ballot);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBallotLookupSession} using an
     *  array of ballots.
     *
     *  @param polls the polls
     *  @param ballots an array of ballots
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballots}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBallotLookupSession(org.osid.voting.Polls polls,
                                                org.osid.voting.Ballot[] ballots, org.osid.proxy.Proxy proxy) {
        this(polls, proxy);
        putBallots(ballots);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBallotLookupSession} using a
     *  collection of ballots.
     *
     *  @param polls the polls
     *  @param ballots a collection of ballots
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code polls},
     *          {@code ballots}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBallotLookupSession(org.osid.voting.Polls polls,
                                                java.util.Collection<? extends org.osid.voting.Ballot> ballots,
                                                org.osid.proxy.Proxy proxy) {
   
        this(polls, proxy);
        setSessionProxy(proxy);
        putBallots(ballots);
        return;
    }

    
    /**
     *  Makes a {@code Ballot} available in this session.
     *
     *  @param ballot an ballot
     *  @throws org.osid.NullArgumentException {@code ballot{@code 
     *          is {@code null}
     */

    @Override
    public void putBallot(org.osid.voting.Ballot ballot) {
        super.putBallot(ballot);
        return;
    }


    /**
     *  Makes an array of ballots available in this session.
     *
     *  @param ballots an array of ballots
     *  @throws org.osid.NullArgumentException {@code ballots{@code 
     *          is {@code null}
     */

    @Override
    public void putBallots(org.osid.voting.Ballot[] ballots) {
        super.putBallots(ballots);
        return;
    }


    /**
     *  Makes collection of ballots available in this session.
     *
     *  @param ballots
     *  @throws org.osid.NullArgumentException {@code ballot{@code 
     *          is {@code null}
     */

    @Override
    public void putBallots(java.util.Collection<? extends org.osid.voting.Ballot> ballots) {
        super.putBallots(ballots);
        return;
    }


    /**
     *  Removes a Ballot from this session.
     *
     *  @param ballotId the {@code Id} of the ballot
     *  @throws org.osid.NullArgumentException {@code ballotId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBallot(org.osid.id.Id ballotId) {
        super.removeBallot(ballotId);
        return;
    }    
}
