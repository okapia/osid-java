//
// AbstractImmutableController.java
//
//     Wraps a mutable Controller to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.controller.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Controller</code> to hide modifiers. This
 *  wrapper provides an immutized Controller from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying controller whose state changes are visible.
 */

public abstract class AbstractImmutableController
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOperableOsidObject
    implements org.osid.control.Controller {

    private final org.osid.control.Controller controller;


    /**
     *  Constructs a new <code>AbstractImmutableController</code>.
     *
     *  @param controller the controller to immutablize
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableController(org.osid.control.Controller controller) {
        super(controller);
        this.controller = controller;
        return;
    }


    /**
     *  Gets the controller address. 
     *
     *  @return the address 
     */

    @OSID @Override
    public String getAddress() {
        return (this.controller.getAddress());
    }


    /**
     *  Gets the controller model <code> Id. </code> 
     *
     *  @return the model <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getModelId() {
        return (this.controller.getModelId());
    }


    /**
     *  Gets the controller model. 
     *
     *  @return the model 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.Model getModel()
        throws org.osid.OperationFailedException {

        return (this.controller.getModel());
    }


    /**
     *  Gets the controller version. 
     *
     *  @return the version 
     */

    @OSID @Override
    public org.osid.installation.Version getVersion() {
        return (this.controller.getVersion());
    }


    /**
     *  Tests if this controller can be turned on and off. A
     *  controller that may also be variable if the minimum and
     *  maximum settings correspond to ON and OFF. A controller that
     *  defines discreet states may also presnet itself as toggleable
     *  if there are ON and OFF states.
     *
     *  @return <code> true </code> if this controller can be toggled, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean isToggleable() {
        return (this.controller.isToggleable());
    }


    /**
     *  Tests if this controller supports levels between on and off. A
     *  variable controller may also be toggleable but does not define
     *  discreet states.
     *
     *  @return <code> true </code> if this controller has levels,
     *          false otherwise
     */

    @OSID @Override
    public boolean isVariable() {
        return (this.controller.isVariable());
    }


    /**
     *  Tests if the levels represent a percentage. 
     *
     *  @return <code> true </code> if this controller has levels as a 
     *          percentage, false otherwise 
     *  @throws org.osid.IllegalStateException <code> isVariable() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public boolean isVariableByPercentage() {
        return (this.controller.isVariableByPercentage());
    }


    /**
     *  Gets the minimum level. 
     *
     *  @return the minimum level 
     *  @throws org.osid.IllegalStateException <code> isVariable()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal getVariableMinimum() {
        return (this.controller.getVariableMinimum());
    }


    /**
     *  Gets the maximum level. 
     *
     *  @return the maximum level 
     *  @throws org.osid.IllegalStateException <code> isVariable()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public java.math.BigDecimal getVariableMaximum() {
        return (this.controller.getVariableMaximum());
    }


    /**
     *  Gets the increments in the level. 
     *
     *  @return the increment 
     *  @throws org.osid.IllegalStateException <code> isVariable()
     *          </code> is <code> false </code> or <code>
     *          isVariableByPercentage() </code> is <code> true
     *          </code>
     */

    @OSID @Override
    public java.math.BigDecimal getVariableIncrement() {
        return (this.controller.getVariableIncrement());
    }


    /**
     *  Tests if this controller supports discreet states. A state
     *  controller may also be toggleable but not variable.
     *
     *  @return <code> true </code> if this controller has discreet
     *          states, false otherwise
     */

    @OSID @Override
    public boolean hasDiscreetStates() {
        return (this.controller.hasDiscreetStates());
    }


    /**
     *  Gets the discreet <code> State </code> <code> Ids. </code> 
     *
     *  @return a list of state <code> Ids </code>
     *  @throws org.osid.IllegalStateException <code>
     *          hasDiscreetStates() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.IdList getDiscreetStateIds() {
        return (this.controller.getDiscreetStateIds());
    }


    /**
     *  Gets the discreet <code> States. </code> 
     *
     *  @return a list of states 
     *  @throws org.osid.IllegalStateException <code>
     *          hasDiscreetStates() </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.StateList getDiscreetStates()
        throws org.osid.OperationFailedException {

        return (this.controller.getDiscreetStates());
    }


    /**
     *  Tests if this controller supports a ramp rate for a transition
     *  from off to on.
     *
     *  @return <code> true </code> if this controller supports ramp
     *          rates, false otherwise
     */

    @OSID @Override
    public boolean isRampable() {
        return (this.controller.isRampable());
    }


    /**
     *  Gets the controller record corresponding to the given <code>
     *  Controller </code> record <code> Type. </code> This method is
     *  used to retrieve an object implementing the requested
     *  record. The <code> controllerRecordType </code> may be the
     *  <code> Type </code> returned in <code> getRecordTypes()
     *  </code> or any of its parents in a <code> Type </code>
     *  hierarchy where <code> hasRecordType(controllerRecordType)
     *  </code> is <code> true </code> .
     *
     *  @param  controllerRecordType the type of controller record to retrieve 
     *  @return the controller record 
     *  @throws org.osid.NullArgumentException <code> controllerRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(controllerRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.records.ControllerRecord getControllerRecord(org.osid.type.Type controllerRecordType)
        throws org.osid.OperationFailedException {

        return (this.controller.getControllerRecord(controllerRecordType));
    }
}

