//
// AbstractRequestTransaction.java
//
//     Defines a RequestTransaction.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.requesttransaction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>RequestTransaction</code>.
 */

public abstract class AbstractRequestTransaction
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.provisioning.RequestTransaction {

    private org.osid.provisioning.Broker broker;
    private org.osid.calendaring.DateTime submitDate;
    private org.osid.resource.Resource submitter;
    private org.osid.authentication.Agent submittingAgent;
    private final java.util.Collection<org.osid.provisioning.Request> requests = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.provisioning.records.RequestTransactionRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the broker. 
     *
     *  @return the broker <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBrokerId() {
        return (this.broker.getId());
    }


    /**
     *  Gets the broker. 
     *
     *  @return the broker 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.Broker getBroker()
        throws org.osid.OperationFailedException {

        return (this.broker);
    }


    /**
     *  Sets the broker.
     *
     *  @param broker a broker
     *  @throws org.osid.NullArgumentException
     *          <code>broker</code> is <code>null</code>
     */

    protected void setBroker(org.osid.provisioning.Broker broker) {
        nullarg(broker, "broker");
        this.broker = broker;
        return;
    }


    /**
     *  Gets the date this transaction was submitted. The submitted date may 
     *  differ from the effective dates of this provision. 
     *
     *  @return the transaction submit date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getSubmitDate() {
        return (this.submitDate);
    }


    /**
     *  Sets the submit date.
     *
     *  @param date a submit date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setSubmitDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "submit date");
        this.submitDate = date;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the submitter. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubmitterId() {
        return (this.submitter.getId());
    }


    /**
     *  Gets the submitter. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSubmitter()
        throws org.osid.OperationFailedException {

        return (this.submitter);
    }


    /**
     *  Sets the submitter.
     *
     *  @param submitter a submitter
     *  @throws org.osid.NullArgumentException
     *          <code>submitter</code> is <code>null</code>
     */

    protected void setSubmitter(org.osid.resource.Resource submitter) {
        nullarg(submitter, "submitter");
        this.submitter = submitter;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the submitting agent. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSubmittingAgentId() {
        return (this.submittingAgent.getId());
    }


    /**
     *  Gets the submitting agent. 
     *
     *  @return the submitting agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getSubmittingAgent()
        throws org.osid.OperationFailedException {

        return (this.submittingAgent);
    }


    /**
     *  Sets the submitting agent.
     *
     *  @param agent a submitting agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setSubmittingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "submitting agent");
        this.submittingAgent = agent;
        return;
    }


    /**
     *  Gets the <code> Ids </code> of the requests. 
     *
     *  @return the request <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.id.IdList getRequestIds()
        throws org.osid.OperationFailedException {

        try {
            org.osid.provisioning.RequestList requests = getRequests();
            return (new net.okapia.osid.jamocha.adapter.converter.provisioning.request.RequestToIdList(requests));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the the requests. 
     *
     *  @return the requests 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.RequestList getRequests()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.provisioning.request.ArrayRequestList(this.requests));
    }


    /**
     *  Adds a request.
     *
     *  @param request a request
     *  @throws org.osid.NullArgumentException
     *          <code>request</code> is <code>null</code>
     */

    protected void addRequest(org.osid.provisioning.Request request) {
        nullarg(request, "request");
        this.requests.add(request);
        return;
    }


    /**
     *  Sets all the requests.
     *
     *  @param requests a collection of requests
     *  @throws org.osid.NullArgumentException
     *          <code>requests</code> is <code>null</code>
     */

    protected void setRequests(java.util.Collection<org.osid.provisioning.Request> requests) {
        nullarg(requests, "requests");
        this.requests.clear();
        this.requests.addAll(requests);
        return;
    }


    /**
     *  Tests if this requestTransaction supports the given record
     *  <code>Type</code>.
     *
     *  @param  requestTransactionRecordType a request transaction record type 
     *  @return <code>true</code> if the requestTransactionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type requestTransactionRecordType) {
        for (org.osid.provisioning.records.RequestTransactionRecord record : this.records) {
            if (record.implementsRecordType(requestTransactionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>RequestTransaction</code> record <code>Type</code>.
     *
     *  @param  requestTransactionRecordType the request transaction record type 
     *  @return the request transaction record 
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(requestTransactionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.records.RequestTransactionRecord getRequestTransactionRecord(org.osid.type.Type requestTransactionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.records.RequestTransactionRecord record : this.records) {
            if (record.implementsRecordType(requestTransactionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(requestTransactionRecordType + " is not supported");
    }


    /**
     *  Adds a record to this request transaction. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param requestTransactionRecord the request transaction record
     *  @param requestTransactionRecordType request transaction record type
     *  @throws org.osid.NullArgumentException
     *          <code>requestTransactionRecord</code> or
     *          <code>requestTransactionRecordTyperequestTransaction</code> is
     *          <code>null</code>
     */
            
    protected void addRequestTransactionRecord(org.osid.provisioning.records.RequestTransactionRecord requestTransactionRecord, 
                                               org.osid.type.Type requestTransactionRecordType) {
        
        nullarg(requestTransactionRecord, "request transaction record");
        addRecordType(requestTransactionRecordType);
        this.records.add(requestTransactionRecord);
        
        return;
    }
}
