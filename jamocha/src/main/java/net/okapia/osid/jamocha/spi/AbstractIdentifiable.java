//
// AbstractIdentifiable.java
//
//     An Id-based object.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.unsetvar;


/**
 *  Defines a simple Id based object.
 */

public abstract class AbstractIdentifiable
    implements org.osid.Identifiable {

    private org.osid.id.Id id;
    private boolean current = false;


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @return the <code> Id </code> 
     *  @throws org.osid.IllegalStateException the Id has not been set
     */
    
    @OSID @Override
    public org.osid.id.Id getId() {
        unsetvar(this.id, "Id");
        return (this.id);
    }


    /**
     *  Sets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is 
     *          <code>null</code>
     */
    
    protected void setId(org.osid.id.Id id) {
        nullarg(id, "id");
        this.id = id;
        return;
    }


    /**
     *  Tests to see if the last method invoked retrieved up-to-date
     *  data.  Simple retrieval methods do not specify errors as,
     *  generally, the data is retrieved once at the time this object
     *  is instantiated. Some implementations may provide real-time
     *  data though the application may not always care. An
     *  implementation providing a real-time service may fall back to
     *  a previous snapshot in case of error. This method returns
     *  false if the data last retrieved was stale.
     *
     *  @return <code> true </code> if the object is up to date,
     *          <code>false</code> otherwise
     */

    @OSID @Override
    public boolean isCurrent() {
        return (this.current);
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */

    protected void current() {
        this.current = true;
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    protected void stale() {
        this.current = false;
        return;
    }


    /**
     *  Determines if the given <code> Id </code> is equal to this
     *  one. Two Ids are equal if the namespace, authority and
     *  identifier components are equal. The identifier is case
     *  sensitive while the namespace and authority strings are not
     *  case sensitive.
     *
     *  @param  obj an object to compare
     *  @return <code> true </code> if the given object is equal to
     *          this <code>Id</code>, <code> false </code> otherwise
     */

    @Override
    public final boolean equals(Object obj) {
        if (obj == null) {
            return (false);
        }

        if (this == obj) {
            return (true);
        }

        if (!(obj instanceof org.osid.OsidObject)) {
            return (false);
        }

        org.osid.OsidObject oo =  (org.osid.OsidObject) obj;
        if (getId().equals(oo.getId())) {
            return (true);
        } else {
            return (false);
        }
    }


    /**
     *  Returns a hash code value for this <code>OsidObject</code>
     *  based on the <code>Id</code>.
     *
     *  @return a hash code value for this object
     */

    @Override
    public int hashCode() { 
        return (getId().hashCode());
    }


    /**
     *  Returns a string representation of this OsidObject.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        return ("(" + getId() + ")");
    }
}
