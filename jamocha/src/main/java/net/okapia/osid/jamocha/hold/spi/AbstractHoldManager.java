//
// AbstractHoldManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractHoldManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.hold.HoldManager,
               org.osid.hold.HoldProxyManager {

    private final Types blockRecordTypes                   = new TypeRefSet();
    private final Types blockSearchRecordTypes             = new TypeRefSet();

    private final Types issueRecordTypes                   = new TypeRefSet();
    private final Types issueSearchRecordTypes             = new TypeRefSet();

    private final Types holdRecordTypes                    = new TypeRefSet();
    private final Types holdSearchRecordTypes              = new TypeRefSet();

    private final Types oublietteRecordTypes               = new TypeRefSet();
    private final Types oublietteSearchRecordTypes         = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractHoldManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractHoldManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any oubliette federation is exposed. Federation is exposed 
     *  when a specific oubliette may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of oubliettes appears as a single oubliette. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if a blocking service is supported for the current agent. 
     *
     *  @return <code> true </code> if blockiings service is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlocking() {
        return (false);
    }


    /**
     *  Tests if a my hold service is supported for the current agent. 
     *
     *  @return <code> true </code> if my hold service is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyHold() {
        return (false);
    }


    /**
     *  Tests if looking up blocks is supported. 
     *
     *  @return <code> true </code> if block lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockLookup() {
        return (false);
    }


    /**
     *  Tests if querying blocks is supported. 
     *
     *  @return <code> true </code> if block query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockQuery() {
        return (false);
    }


    /**
     *  Tests if searching blocks is supported. 
     *
     *  @return <code> true </code> if block search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockSearch() {
        return (false);
    }


    /**
     *  Tests if block administrative service is supported. 
     *
     *  @return <code> true </code> if block administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockAdmin() {
        return (false);
    }


    /**
     *  Tests if a block notification service is supported. 
     *
     *  @return <code> true </code> if block notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockNotification() {
        return (false);
    }


    /**
     *  Tests if a block oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if a block oubliette lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockOubliette() {
        return (false);
    }


    /**
     *  Tests if a block oubliette service is supported. 
     *
     *  @return <code> true </code> if block to oubliette assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockOublietteAssignment() {
        return (false);
    }


    /**
     *  Tests if a block smart oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if a block smart oubliette service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockSmartOubliette() {
        return (false);
    }


    /**
     *  Tests if looking up issues is supported. 
     *
     *  @return <code> true </code> if issue lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueLookup() {
        return (false);
    }


    /**
     *  Tests if querying issues is supported. 
     *
     *  @return <code> true </code> if issue query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Tests if searching issues is supported. 
     *
     *  @return <code> true </code> if issue search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSearch() {
        return (false);
    }


    /**
     *  Tests if issue <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if issue administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueAdmin() {
        return (false);
    }


    /**
     *  Tests if an issue <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if issue notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueNotification() {
        return (false);
    }


    /**
     *  Tests if an issue oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if an issue oubliette lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueOubliette() {
        return (false);
    }


    /**
     *  Tests if an issue oubliette assignment service is supported. 
     *
     *  @return <code> true </code> if an issue to oubliette assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueOublietteAssignment() {
        return (false);
    }


    /**
     *  Tests if an issue smart oubliette service is supported. 
     *
     *  @return <code> true </code> if an issue smart oubliette service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSmartOubliette() {
        return (false);
    }


    /**
     *  Tests if looking up holds is supported. 
     *
     *  @return <code> true </code> if hold lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldLookup() {
        return (false);
    }


    /**
     *  Tests if querying holds is supported. 
     *
     *  @return <code> true </code> if hold query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldQuery() {
        return (false);
    }


    /**
     *  Tests if searching holds is supported. 
     *
     *  @return <code> true </code> if hold search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldSearch() {
        return (false);
    }


    /**
     *  Tests if hold administrative service is supported. 
     *
     *  @return <code> true </code> if hold administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldAdmin() {
        return (false);
    }


    /**
     *  Tests if a hold notification service is supported. 
     *
     *  @return <code> true </code> if hold notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldNotification() {
        return (false);
    }


    /**
     *  Tests if a hold oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if a hold oubliette lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldOubliette() {
        return (false);
    }


    /**
     *  Tests if a hold oubliette service is supported. 
     *
     *  @return <code> true </code> if hold to oubliette assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldOublietteAssignment() {
        return (false);
    }


    /**
     *  Tests if a hold smart oubliette lookup service is supported. 
     *
     *  @return <code> true </code> if a hold smart oubliette service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldSmartOubliette() {
        return (false);
    }


    /**
     *  Tests if looking up oubliettes is supported. 
     *
     *  @return <code> true </code> if oubliette lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteLookup() {
        return (false);
    }


    /**
     *  Tests if querying oubliettes is supported. 
     *
     *  @return <code> true </code> if an oubliette query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteQuery() {
        return (false);
    }


    /**
     *  Tests if searching oubliettes is supported. 
     *
     *  @return <code> true </code> if oubliette search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteSearch() {
        return (false);
    }


    /**
     *  Tests if oubliette administrative service is supported. 
     *
     *  @return <code> true </code> if oubliette administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteAdmin() {
        return (false);
    }


    /**
     *  Tests if an oubliette <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if oubliette notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of an oubliette hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if oubliette hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of an oubliette hierarchy design service. 
     *
     *  @return <code> true </code> if oubliette hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOublietteHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a hold batch service. 
     *
     *  @return <code> true </code> if a hold batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldBatch() {
        return (false);
    }


    /**
     *  Tests for the availability of a hold rules service. 
     *
     *  @return <code> true </code> if a hold rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsHoldRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Block </code> record types. 
     *
     *  @return a list containing the supported <code> Block </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBlockRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.blockRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Block </code> record type is supported. 
     *
     *  @param  blockRecordType a <code> Type </code> indicating a <code> 
     *          Block </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> blockRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBlockRecordType(org.osid.type.Type blockRecordType) {
        return (this.blockRecordTypes.contains(blockRecordType));
    }


    /**
     *  Adds support for a block record type.
     *
     *  @param blockRecordType a block record type
     *  @throws org.osid.NullArgumentException
     *  <code>blockRecordType</code> is <code>null</code>
     */

    protected void addBlockRecordType(org.osid.type.Type blockRecordType) {
        this.blockRecordTypes.add(blockRecordType);
        return;
    }


    /**
     *  Removes support for a block record type.
     *
     *  @param blockRecordType a block record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>blockRecordType</code> is <code>null</code>
     */

    protected void removeBlockRecordType(org.osid.type.Type blockRecordType) {
        this.blockRecordTypes.remove(blockRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Block </code> search record types. 
     *
     *  @return a list containing the supported <code> Block </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBlockSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.blockSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Block </code> search record type is 
     *  supported. 
     *
     *  @param  blockSearchRecordType a <code> Type </code> indicating a 
     *          <code> Block </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> blockSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBlockSearchRecordType(org.osid.type.Type blockSearchRecordType) {
        return (this.blockSearchRecordTypes.contains(blockSearchRecordType));
    }


    /**
     *  Adds support for a block search record type.
     *
     *  @param blockSearchRecordType a block search record type
     *  @throws org.osid.NullArgumentException
     *  <code>blockSearchRecordType</code> is <code>null</code>
     */

    protected void addBlockSearchRecordType(org.osid.type.Type blockSearchRecordType) {
        this.blockSearchRecordTypes.add(blockSearchRecordType);
        return;
    }


    /**
     *  Removes support for a block search record type.
     *
     *  @param blockSearchRecordType a block search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>blockSearchRecordType</code> is <code>null</code>
     */

    protected void removeBlockSearchRecordType(org.osid.type.Type blockSearchRecordType) {
        this.blockSearchRecordTypes.remove(blockSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Issue </code> record types. 
     *
     *  @return a list containing the supported <code> Issue </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIssueRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.issueRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Issue </code> record type is supported. 
     *
     *  @param  issueRecordType a <code> Type </code> indicating an <code> 
     *          Issue </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> issueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIssueRecordType(org.osid.type.Type issueRecordType) {
        return (this.issueRecordTypes.contains(issueRecordType));
    }


    /**
     *  Adds support for an issue record type.
     *
     *  @param issueRecordType an issue record type
     *  @throws org.osid.NullArgumentException
     *  <code>issueRecordType</code> is <code>null</code>
     */

    protected void addIssueRecordType(org.osid.type.Type issueRecordType) {
        this.issueRecordTypes.add(issueRecordType);
        return;
    }


    /**
     *  Removes support for an issue record type.
     *
     *  @param issueRecordType an issue record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>issueRecordType</code> is <code>null</code>
     */

    protected void removeIssueRecordType(org.osid.type.Type issueRecordType) {
        this.issueRecordTypes.remove(issueRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Issue </code> search types. 
     *
     *  @return a list containing the supported <code> Issue </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIssueSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.issueSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Issue </code> search type is supported. 
     *
     *  @param  issueSearchRecordType a <code> Type </code> indicating an 
     *          <code> Issue </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> issueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIssueSearchRecordType(org.osid.type.Type issueSearchRecordType) {
        return (this.issueSearchRecordTypes.contains(issueSearchRecordType));
    }


    /**
     *  Adds support for an issue search record type.
     *
     *  @param issueSearchRecordType an issue search record type
     *  @throws org.osid.NullArgumentException
     *  <code>issueSearchRecordType</code> is <code>null</code>
     */

    protected void addIssueSearchRecordType(org.osid.type.Type issueSearchRecordType) {
        this.issueSearchRecordTypes.add(issueSearchRecordType);
        return;
    }


    /**
     *  Removes support for an issue search record type.
     *
     *  @param issueSearchRecordType an issue search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>issueSearchRecordType</code> is <code>null</code>
     */

    protected void removeIssueSearchRecordType(org.osid.type.Type issueSearchRecordType) {
        this.issueSearchRecordTypes.remove(issueSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Hold </code> record types. 
     *
     *  @return a list containing the supported <code> Hold </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHoldRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.holdRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Hold </code> record type is supported. 
     *
     *  @param  holdRecordType a <code> Type </code> indicating a <code> Hold 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> holdRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHoldRecordType(org.osid.type.Type holdRecordType) {
        return (this.holdRecordTypes.contains(holdRecordType));
    }


    /**
     *  Adds support for a hold record type.
     *
     *  @param holdRecordType a hold record type
     *  @throws org.osid.NullArgumentException
     *  <code>holdRecordType</code> is <code>null</code>
     */

    protected void addHoldRecordType(org.osid.type.Type holdRecordType) {
        this.holdRecordTypes.add(holdRecordType);
        return;
    }


    /**
     *  Removes support for a hold record type.
     *
     *  @param holdRecordType a hold record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>holdRecordType</code> is <code>null</code>
     */

    protected void removeHoldRecordType(org.osid.type.Type holdRecordType) {
        this.holdRecordTypes.remove(holdRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Hold </code> search record types. 
     *
     *  @return a list containing the supported <code> Hold </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getHoldSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.holdSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Hold </code> search record type is 
     *  supported. 
     *
     *  @param  holdSearchRecordType a <code> Type </code> indicating a <code> 
     *          Hold </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> holdSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsHoldSearchRecordType(org.osid.type.Type holdSearchRecordType) {
        return (this.holdSearchRecordTypes.contains(holdSearchRecordType));
    }


    /**
     *  Adds support for a hold search record type.
     *
     *  @param holdSearchRecordType a hold search record type
     *  @throws org.osid.NullArgumentException
     *  <code>holdSearchRecordType</code> is <code>null</code>
     */

    protected void addHoldSearchRecordType(org.osid.type.Type holdSearchRecordType) {
        this.holdSearchRecordTypes.add(holdSearchRecordType);
        return;
    }


    /**
     *  Removes support for a hold search record type.
     *
     *  @param holdSearchRecordType a hold search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>holdSearchRecordType</code> is <code>null</code>
     */

    protected void removeHoldSearchRecordType(org.osid.type.Type holdSearchRecordType) {
        this.holdSearchRecordTypes.remove(holdSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Oubliette </code> record types. 
     *
     *  @return a list containing the supported <code> Oubliette </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOublietteRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.oublietteRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Oubliette </code> record type is supported. 
     *
     *  @param  oublietteRecordType a <code> Type </code> indicating a <code> 
     *          Oubliette </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> oublietteRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOublietteRecordType(org.osid.type.Type oublietteRecordType) {
        return (this.oublietteRecordTypes.contains(oublietteRecordType));
    }


    /**
     *  Adds support for an oubliette record type.
     *
     *  @param oublietteRecordType an oubliette record type
     *  @throws org.osid.NullArgumentException
     *  <code>oublietteRecordType</code> is <code>null</code>
     */

    protected void addOublietteRecordType(org.osid.type.Type oublietteRecordType) {
        this.oublietteRecordTypes.add(oublietteRecordType);
        return;
    }


    /**
     *  Removes support for an oubliette record type.
     *
     *  @param oublietteRecordType an oubliette record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>oublietteRecordType</code> is <code>null</code>
     */

    protected void removeOublietteRecordType(org.osid.type.Type oublietteRecordType) {
        this.oublietteRecordTypes.remove(oublietteRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Oubliette </code> search types. 
     *
     *  @return a list containing the supported <code> Oubliette </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getOublietteSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.oublietteSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Oubliette </code> search record type is 
     *  supported. 
     *
     *  @param  oublietteSearchRecordType a <code> Type </code> indicating a 
     *          <code> Oubliette </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          oublietteSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsOublietteSearchRecordType(org.osid.type.Type oublietteSearchRecordType) {
        return (this.oublietteSearchRecordTypes.contains(oublietteSearchRecordType));
    }


    /**
     *  Adds support for an oubliette search record type.
     *
     *  @param oublietteSearchRecordType an oubliette search record type
     *  @throws org.osid.NullArgumentException
     *  <code>oublietteSearchRecordType</code> is <code>null</code>
     */

    protected void addOublietteSearchRecordType(org.osid.type.Type oublietteSearchRecordType) {
        this.oublietteSearchRecordTypes.add(oublietteSearchRecordType);
        return;
    }


    /**
     *  Removes support for an oubliette search record type.
     *
     *  @param oublietteSearchRecordType an oubliette search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>oublietteSearchRecordType</code> is <code>null</code>
     */

    protected void removeOublietteSearchRecordType(org.osid.type.Type oublietteSearchRecordType) {
        this.oublietteSearchRecordTypes.remove(oublietteSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block service 
     *  to check for blocks. 
     *
     *  @return a <code> BlockSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlock() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSession getBlockSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block service 
     *  to check for blocks. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlock() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSession getBlockSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block service 
     *  for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the oubliette 
     *  @return a <code> BlockSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlock() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSession getBlockSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block service 
     *  for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the oubliette 
     *  @param  proxy a proxy 
     *  @return a <code> BlockSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlock() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSession getBlockSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my hold 
     *  service. 
     *
     *  @return a <code> MyHoldSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyHold() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.MyHoldSession getMyHoldSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getMyHoldSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my hold 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyHoldSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyHold() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.MyHoldSession getMyHoldSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getMyHoldSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my hold 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the oubliette 
     *  @return a <code> MyHoldSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyHold() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.MyHoldSession getMyHoldSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getMyHoldSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my hold 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the oubliette 
     *  @param  proxy a proxy 
     *  @return a <code> MyHoldSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyHold() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.MyHoldSession getMyHoldSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getMyHoldSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block lookup 
     *  service. 
     *
     *  @return a <code> BlockLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockLookupSession getBlockLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockLookupSession getBlockLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block lookup 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> BlockLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockLookupSession getBlockLookupSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block lookup 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockLookupSession getBlockLookupSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block query 
     *  service. 
     *
     *  @return a <code> BlockQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuerySession getBlockQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuerySession getBlockQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block query 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> BlockQuerySession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuerySession getBlockQuerySessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockQuerySessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block query 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockQuerySession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockQuerySession getBlockQuerySessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockQuerySessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block search 
     *  service. 
     *
     *  @return a <code> BlockSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSearchSession getBlockSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSearchSession getBlockSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block search 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> BlockSearchSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSearchSession getBlockSearchSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockSearchSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block search 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockSearchSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSearchSession getBlockSearchSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockSearchSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  administration service. 
     *
     *  @return a <code> BlockAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockAdminSession getBlockAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockAdminSession getBlockAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> BlockAdminSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockAdminSession getBlockAdminSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockAdminSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBlockAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockAdminSession getBlockAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  notification service. 
     *
     *  @param  blockReceiver the notification callback 
     *  @return a <code> BlockNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> blockReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockNotificationSession getBlockNotificationSession(org.osid.hold.BlockReceiver blockReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  notification service. 
     *
     *  @param  blockReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> BlockNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> blockReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockNotificationSession getBlockNotificationSession(org.osid.hold.BlockReceiver blockReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  notification service for the given oubliette. 
     *
     *  @param  blockReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> BlockNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> blockReceiver </code> or 
     *          <code> oublietteId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockNotificationSession getBlockNotificationSessionForOubliette(org.osid.hold.BlockReceiver blockReceiver, 
                                                                                          org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockNotificationSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the block 
     *  notification service for the given oubliette. 
     *
     *  @param  blockReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> blockReceiver, 
     *          oublietteId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockNotificationSession getBlockNotificationSessionForOubliette(org.osid.hold.BlockReceiver blockReceiver, 
                                                                                          org.osid.id.Id oublietteId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockNotificationSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup block/oubliette 
     *  mappings. 
     *
     *  @return a <code> BlockOublietteSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockOublietteSession getBlockOublietteSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup block/oubliette 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockOublietteSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockOublietteSession getBlockOublietteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning blocks 
     *  to oubliettes. 
     *
     *  @return a <code> BlockOublietteAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockOublietteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockOublietteAssignmentSession getBlockOublietteAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockOublietteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning blocks 
     *  to oubliettes. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockOublietteAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockOublietteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockOublietteAssignmentSession getBlockOublietteAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockOublietteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage block smart oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> BlockSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockSmartOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSmartOublietteSession getBlockSmartOublietteSession(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getBlockSmartOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage block smart oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> BlockSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockSmartOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.BlockSmartOublietteSession getBlockSmartOublietteSession(org.osid.id.Id oublietteId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getBlockSmartOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service. 
     *
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueLookupSession getIssueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueLookupSession getIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the oubliette 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueLookupSession getIssueLookupSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the oubliette 
     *  @param  proxy a proxy 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueLookupSession getIssueLookupSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service. 
     *
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuerySession getIssueQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuerySession getIssueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuerySession getIssueQuerySessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueQuerySessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueQuerySession getIssueQuerySessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueQuerySessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service. 
     *
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSearchSession getIssueSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSearchSession getIssueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSearchSession getIssueSearchSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueSearchSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSearchSession getIssueSearchSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueSearchSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service. 
     *
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueAdminSession getIssueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueAdminSession getIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueAdminSession getIssueAdminSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueAdminSession getIssueAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service. 
     *
     *  @param  issueReceiver the notification callback 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueNotificationSession getIssueNotificationSession(org.osid.hold.IssueReceiver issueReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueNotificationSession getIssueNotificationSession(org.osid.hold.IssueReceiver issueReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service for the given oubliette. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver </code> or 
     *          <code> oublietteId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueNotificationSession getIssueNotificationSessionForOubliette(org.osid.hold.IssueReceiver issueReceiver, 
                                                                                          org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueNotificationSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service for the given oubliette. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver, </code> 
     *          <code> oublietteId </code> or <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueNotificationSession getIssueNotificationSessionForOubliette(org.osid.hold.IssueReceiver issueReceiver, 
                                                                                          org.osid.id.Id oublietteId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueNotificationSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup issue/oubliette holds. 
     *
     *  @return an <code> IssueOublietteSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueOublietteSession getIssueOublietteSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup issue/oubliette holds. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueOublietteSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueOublietteSession getIssueOublietteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning issues 
     *  to oubliettes. 
     *
     *  @return an <code> IssueOublietteAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueOublietteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueOublietteAssignmentSession getIssueOublietteAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueOublietteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning mappings 
     *  to oubliettes. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueOublietteAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueOublietteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueOublietteAssignmentSession getIssueOublietteAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueOublietteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return an <code> IssueSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueSmartOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSmartOublietteSession getIssueSmartOublietteSession(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getIssueSmartOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueSmartOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.IssueSmartOublietteSession getIssueSmartOublietteSession(org.osid.id.Id oublietteId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getIssueSmartOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold lookup 
     *  service. 
     *
     *  @return a <code> HoldLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldLookupSession getHoldLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldLookupSession getHoldLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold lookup 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldLookupSession getHoldLookupSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold lookup 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldLookupSession getHoldLookupSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldLookupSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold query 
     *  service. 
     *
     *  @return a <code> HoldQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuerySession getHoldQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuerySession getHoldQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold query 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> CHoldQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuerySession getHoldQuerySessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldQuerySessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold query 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldQuerySession getHoldQuerySessionForOubliette(org.osid.id.Id oublietteId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldQuerySessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold search 
     *  service. 
     *
     *  @return a <code> HoldSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldSearchSession getHoldSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldSearchSession getHoldSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold search 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldSearchSession getHoldSearchSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldSearchSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold search 
     *  service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldSearchSession getHoldSearchSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldSearchSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  administrative service. 
     *
     *  @return a <code> HoldAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldAdminSession getHoldAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldAdminSession getHoldAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  administrative service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldAdminSession getHoldAdminSessionForOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  administrative service for the given oubliette. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldAdminSession getHoldAdminSessionForOubliette(org.osid.id.Id oublietteId, 
                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldAdminSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  notification service. 
     *
     *  @param  holdReceiver the notification callback 
     *  @return a <code> HoldNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> holdReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldNotificationSession getHoldNotificationSession(org.osid.hold.HoldReceiver holdReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  notification service. 
     *
     *  @param  holdReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> HoldNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> holdReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldNotificationSession getHoldNotificationSession(org.osid.hold.HoldReceiver holdReceiver, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  notification service for the given oubliette. 
     *
     *  @param  holdReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> holdReceiver </code> or 
     *          <code> oublietteId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldNotificationSession getHoldNotificationSessionForOubliette(org.osid.hold.HoldReceiver holdReceiver, 
                                                                                        org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldNotificationSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the hold 
     *  notification service for the given oubliette. 
     *
     *  @param  holdReceiver the notification callback 
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldNotificationSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> holdReceiver, 
     *          oublietteId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldNotificationSession getHoldNotificationSessionForOubliette(org.osid.hold.HoldReceiver holdReceiver, 
                                                                                        org.osid.id.Id oublietteId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldNotificationSessionForOubliette not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup hold/oubliette mappings. 
     *
     *  @return a <code> HoldOublietteSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldOubliette() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldOublietteSession getHoldOublietteSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup hold/oubliette mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldOublietteSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldOubliette() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldOublietteSession getHoldOublietteSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to oubliettes. 
     *
     *  @return a <code> HoldOublietteAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldOublietteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldOublietteAssignmentSession getHoldOublietteAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldOublietteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  competencies to oubliettes. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HoldOublietteAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldOublietteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldOublietteAssignmentSession getHoldOublietteAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldOublietteAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage hold smart oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @return a <code> HoldSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no <code> Oubliette </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldSmartOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldSmartOublietteSession getHoldSmartOublietteSession(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldSmartOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage hold smart oubliettes. 
     *
     *  @param  oublietteId the <code> Id </code> of the <code> Oubliette 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> HoldSmartOublietteSession </code> 
     *  @throws org.osid.NotFoundException no oubliette found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsHoldSmartOubliette() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.HoldSmartOublietteSession getHoldSmartOublietteSession(org.osid.id.Id oublietteId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldSmartOublietteSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  lookup service. 
     *
     *  @return a <code> OublietteLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteLookupSession getOublietteLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getOublietteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteLookupSession getOublietteLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getOublietteLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  query service. 
     *
     *  @return a <code> OublietteQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuerySession getOublietteQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getOublietteQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteQuerySession getOublietteQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getOublietteQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  search service. 
     *
     *  @return a <code> OublietteSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteSearchSession getOublietteSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getOublietteSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteSearchSession getOublietteSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getOublietteSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  administrative service. 
     *
     *  @return a <code> OublietteAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteAdminSession getOublietteAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getOublietteAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteAdminSession getOublietteAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getOublietteAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  notification service. 
     *
     *  @param  oublietteReceiver the notification callback 
     *  @return a <code> OublietteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteNotificationSession getOublietteNotificationSession(org.osid.hold.OublietteReceiver oublietteReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getOublietteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  notification service. 
     *
     *  @param  oublietteReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> OublietteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> oublietteReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteNotificationSession getOublietteNotificationSession(org.osid.hold.OublietteReceiver oublietteReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getOublietteNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  hierarchy service. 
     *
     *  @return a <code> OublietteHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteHierarchySession getOublietteHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getOublietteHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> OublietteHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteHierarchySession getOublietteHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getOublietteHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for oubliettes 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteHierarchyDesignSession getOublietteHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getOublietteHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the oubliette 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for oubliettes 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsOublietteHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.hold.OublietteHierarchyDesignSession getOublietteHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getOublietteHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> HoldBatchManager. </code> 
     *
     *  @return a <code> HoldBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.HoldBatchManager getHoldBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldBatchManager not implemented");
    }


    /**
     *  Gets a <code> HoldBatchProxyManager. </code> 
     *
     *  @return a <code> HoldBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.batch.HoldBatchProxyManager getHoldBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldBatchProxyManager not implemented");
    }


    /**
     *  Gets a <code> HoldRulesManager. </code> 
     *
     *  @return a <code> HoldRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldRulesManager getHoldRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldManager.getHoldRulesManager not implemented");
    }


    /**
     *  Gets a <code> HoldRulesProxyManager. </code> 
     *
     *  @return a <code> HoldRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsHoldRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.hold.rules.HoldRulesProxyManager getHoldRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.hold.HoldProxyManager.getHoldRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.blockRecordTypes.clear();
        this.blockRecordTypes.clear();

        this.blockSearchRecordTypes.clear();
        this.blockSearchRecordTypes.clear();

        this.issueRecordTypes.clear();
        this.issueRecordTypes.clear();

        this.issueSearchRecordTypes.clear();
        this.issueSearchRecordTypes.clear();

        this.holdRecordTypes.clear();
        this.holdRecordTypes.clear();

        this.holdSearchRecordTypes.clear();
        this.holdSearchRecordTypes.clear();

        this.oublietteRecordTypes.clear();
        this.oublietteRecordTypes.clear();

        this.oublietteSearchRecordTypes.clear();
        this.oublietteSearchRecordTypes.clear();

        return;
    }
}
