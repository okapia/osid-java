//
// AbstractQuerySequenceRuleLookupSession.java
//
//    An inline adapter that maps a SequenceRuleLookupSession to
//    a SequenceRuleQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.assessment.authoring.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a SequenceRuleLookupSession to
 *  a SequenceRuleQuerySession.
 */

public abstract class AbstractQuerySequenceRuleLookupSession
    extends net.okapia.osid.jamocha.assessment.authoring.spi.AbstractSequenceRuleLookupSession
    implements org.osid.assessment.authoring.SequenceRuleLookupSession {

    private boolean activeonly    = false;
    private final org.osid.assessment.authoring.SequenceRuleQuerySession session;
    

    /**
     *  Constructs a new AbstractQuerySequenceRuleLookupSession.
     *
     *  @param querySession the underlying sequence rule query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQuerySequenceRuleLookupSession(org.osid.assessment.authoring.SequenceRuleQuerySession querySession) {
        nullarg(querySession, "sequence rule query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Bank</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Bank Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBankId() {
        return (this.session.getBankId());
    }


    /**
     *  Gets the <code>Bank</code> associated with this 
     *  session.
     *
     *  @return the <code>Bank</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.Bank getBank()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBank());
    }


    /**
     *  Tests if this user can perform <code>SequenceRule</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSequenceRules() {
        return (this.session.canSearchSequenceRules());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include sequence rules in banks which are children
     *  of this bank in the bank hierarchy.
     */

    @OSID @Override
    public void useFederatedBankView() {
        this.session.useFederatedBankView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bank only.
     */

    @OSID @Override
    public void useIsolatedBankView() {
        this.session.useIsolatedBankView();
        return;
    }
    

    /**
     *  Only active sequence rules are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveSequenceRuleView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive sequence rules are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusSequenceRuleView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>SequenceRule</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>SequenceRule</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>SequenceRule</code> and
     *  retained for compatibility.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleId <code>Id</code> of the
     *          <code>SequenceRule</code>
     *  @return the sequence rule
     *  @throws org.osid.NotFoundException <code>sequenceRuleId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>sequenceRuleId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRule getSequenceRule(org.osid.id.Id sequenceRuleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.SequenceRuleQuery query = getQuery();
        query.matchId(sequenceRuleId, true);
        org.osid.assessment.authoring.SequenceRuleList sequenceRules = this.session.getSequenceRulesByQuery(query);
        if (sequenceRules.hasNext()) {
            return (sequenceRules.getNextSequenceRule());
        } 
        
        throw new org.osid.NotFoundException(sequenceRuleId + " not found");
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  sequenceRules specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>SequenceRules</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByIds(org.osid.id.IdList sequenceRuleIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.SequenceRuleQuery query = getQuery();

        try (org.osid.id.IdList ids = sequenceRuleIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getSequenceRulesByQuery(query));
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  sequence rule genus <code>Type</code> which does not include
     *  sequence rules of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleGenusType a sequenceRule genus type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByGenusType(org.osid.type.Type sequenceRuleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.SequenceRuleQuery query = getQuery();
        query.matchGenusType(sequenceRuleGenusType, true);
        return (this.session.getSequenceRulesByQuery(query));
    }


    /**
     *  Gets a <code>SequenceRuleList</code> corresponding to the given
     *  sequence rule genus <code>Type</code> and include any additional
     *  sequence rules with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleGenusType a sequenceRule genus type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByParentGenusType(org.osid.type.Type sequenceRuleGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.SequenceRuleQuery query = getQuery();
        query.matchParentGenusType(sequenceRuleGenusType, true);
        return (this.session.getSequenceRulesByQuery(query));
    }


    /**
     *  Gets a <code>SequenceRuleList</code> containing the given
     *  sequence rule record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @param  sequenceRuleRecordType a sequenceRule record type 
     *  @return the returned <code>SequenceRule</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sequenceRuleRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRulesByRecordType(org.osid.type.Type sequenceRuleRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.SequenceRuleQuery query = getQuery();
        query.matchRecordType(sequenceRuleRecordType, true);
        return (this.session.getSequenceRulesByQuery(query));
    }

    
    /**
     *  Gets all <code>SequenceRules</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  sequence rules or an error results. Otherwise, the returned list
     *  may contain only those sequence rules that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, sequence rules are returned that are currently
     *  active. In any status mode, active and inactive sequence rules
     *  are returned.
     *
     *  @return a list of <code>SequenceRules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.assessment.authoring.SequenceRuleList getSequenceRules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.assessment.authoring.SequenceRuleQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getSequenceRulesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.assessment.authoring.SequenceRuleQuery getQuery() {
        org.osid.assessment.authoring.SequenceRuleQuery query = this.session.getSequenceRuleQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
