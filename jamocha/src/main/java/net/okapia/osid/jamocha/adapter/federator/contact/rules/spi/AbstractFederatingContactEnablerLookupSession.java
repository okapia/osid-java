//
// AbstractFederatingContactEnablerLookupSession.java
//
//     An abstract federating adapter for a ContactEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.contact.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ContactEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingContactEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.contact.rules.ContactEnablerLookupSession>
    implements org.osid.contact.rules.ContactEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.contact.AddressBook addressBook = new net.okapia.osid.jamocha.nil.contact.addressbook.UnknownAddressBook();


    /**
     *  Constructs a new <code>AbstractFederatingContactEnablerLookupSession</code>.
     */

    protected AbstractFederatingContactEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.contact.rules.ContactEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>AddressBook/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AddressBook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.addressBook.getId());
    }


    /**
     *  Gets the <code>AddressBook</code> associated with this 
     *  session.
     *
     *  @return the <code>AddressBook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.addressBook);
    }


    /**
     *  Sets the <code>AddressBook</code>.
     *
     *  @param  addressBook the address book for this session
     *  @throws org.osid.NullArgumentException <code>addressBook</code>
     *          is <code>null</code>
     */

    protected void setAddressBook(org.osid.contact.AddressBook addressBook) {
        nullarg(addressBook, "address book");
        this.addressBook = addressBook;
        return;
    }


    /**
     *  Tests if this user can perform <code>ContactEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupContactEnablers() {
        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            if (session.canLookupContactEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ContactEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeContactEnablerView() {
        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            session.useComparativeContactEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ContactEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryContactEnablerView() {
        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            session.usePlenaryContactEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include contact enablers in address books which are children
     *  of this address book in the address book hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            session.useFederatedAddressBookView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this address book only.
     */

    @OSID @Override
    public void useIsolatedAddressBookView() {
        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            session.useIsolatedAddressBookView();
        }

        return;
    }


    /**
     *  Only active contact enablers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveContactEnablerView() {
        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            session.useActiveContactEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive contact enablers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusContactEnablerView() {
        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            session.useAnyStatusContactEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>ContactEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ContactEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>ContactEnabler</code> and retained for compatibility.
     *
     *  In active mode, contact enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  contact enablers are returned.
     *
     *  @param  contactEnablerId <code>Id</code> of the
     *          <code>ContactEnabler</code>
     *  @return the contact enabler
     *  @throws org.osid.NotFoundException <code>contactEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>contactEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnabler getContactEnabler(org.osid.id.Id contactEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            try {
                return (session.getContactEnabler(contactEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(contactEnablerId + " not found");
    }


    /**
     *  Gets a <code>ContactEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  contactEnablers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>ContactEnablers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, contact enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  contact enablers are returned.
     *
     *  @param  contactEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByIds(org.osid.id.IdList contactEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.contact.rules.contactenabler.MutableContactEnablerList ret = new net.okapia.osid.jamocha.contact.rules.contactenabler.MutableContactEnablerList();

        try (org.osid.id.IdList ids = contactEnablerIds) {
            while (ids.hasNext()) {
                ret.addContactEnabler(getContactEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ContactEnablerList</code> corresponding to the given
     *  contact enabler genus <code>Type</code> which does not include
     *  contact enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known contact
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those contact enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, contact enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  contact enablers are returned.
     *
     *  @param  contactEnablerGenusType a contactEnabler genus type 
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByGenusType(org.osid.type.Type contactEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.rules.contactenabler.FederatingContactEnablerList ret = getContactEnablerList();

        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            ret.addContactEnablerList(session.getContactEnablersByGenusType(contactEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ContactEnablerList</code> corresponding to the given
     *  contact enabler genus <code>Type</code> and include any additional
     *  contact enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known contact
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those contact enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, contact enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  contact enablers are returned.
     *
     *  @param  contactEnablerGenusType a contactEnabler genus type 
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByParentGenusType(org.osid.type.Type contactEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.rules.contactenabler.FederatingContactEnablerList ret = getContactEnablerList();

        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            ret.addContactEnablerList(session.getContactEnablersByParentGenusType(contactEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ContactEnablerList</code> containing the given
     *  contact enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known contact
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those contact enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, contact enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  contact enablers are returned.
     *
     *  @param  contactEnablerRecordType a contactEnabler record type 
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>contactEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersByRecordType(org.osid.type.Type contactEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.rules.contactenabler.FederatingContactEnablerList ret = getContactEnablerList();

        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            ret.addContactEnablerList(session.getContactEnablersByRecordType(contactEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ContactEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known contact
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those contact enablers that are accessible
     *  through this session.
     *  
     *  In active mode, contact enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  contact enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ContactEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.rules.contactenabler.FederatingContactEnablerList ret = getContactEnablerList();

        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            ret.addContactEnablerList(session.getContactEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>ContactEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined to
     *  the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known contact
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those contact enablers that are accessible
     *  through this session.
     *
     *  In active mode, contact enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  contact enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>ContactEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.rules.contactenabler.FederatingContactEnablerList ret = getContactEnablerList();

        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            ret.addContactEnablerList(session.getContactEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ContactEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known contact
     *  enablers or an error results. Otherwise, the returned list may
     *  contain only those contact enablers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, contact enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  contact enablers are returned.
     *
     *  @return a list of <code>ContactEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.rules.ContactEnablerList getContactEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.contact.rules.contactenabler.FederatingContactEnablerList ret = getContactEnablerList();

        for (org.osid.contact.rules.ContactEnablerLookupSession session : getSessions()) {
            ret.addContactEnablerList(session.getContactEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.contact.rules.contactenabler.FederatingContactEnablerList getContactEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.contact.rules.contactenabler.ParallelContactEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.contact.rules.contactenabler.CompositeContactEnablerList());
        }
    }
}
