//
// AbstractRecurringEvent.java
//
//     Defines a RecurringEvent builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.recurringevent.spi;


/**
 *  Defines a <code>RecurringEvent</code> builder.
 */

public abstract class AbstractRecurringEventBuilder<T extends AbstractRecurringEventBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.calendaring.recurringevent.RecurringEventMiter recurringEvent;


    /**
     *  Constructs a new <code>AbstractRecurringEventBuilder</code>.
     *
     *  @param recurringEvent the recurring event to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractRecurringEventBuilder(net.okapia.osid.jamocha.builder.calendaring.recurringevent.RecurringEventMiter recurringEvent) {
        super(recurringEvent);
        this.recurringEvent = recurringEvent;
        return;
    }


    /**
     *  Builds the recurring event.
     *
     *  @return the new recurring event
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.calendaring.RecurringEvent build() {
        (new net.okapia.osid.jamocha.builder.validator.calendaring.recurringevent.RecurringEventValidator(getValidations())).validate(this.recurringEvent);
        return (new net.okapia.osid.jamocha.builder.calendaring.recurringevent.ImmutableRecurringEvent(this.recurringEvent));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the recurring event miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.calendaring.recurringevent.RecurringEventMiter getMiter() {
        return (this.recurringEvent);
    }


    /**
     *  Sets the sequestered flag.
     */

    public T sequestered() {
        getMiter().setSequestered(true);
        return (self());
    }


    /**
     *  Sets the unsequestered flag.
     */

    public T unsequestered() {
        getMiter().setSequestered(false);
        return (self());
    }


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>schedule</code> is <code>null</code>
     */

    public T schedule(org.osid.calendaring.Schedule schedule) {
        getMiter().addSchedule(schedule);
        return (self());
    }


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>schedules</code> is <code>null</code>
     */

    public T schedules(java.util.Collection<org.osid.calendaring.Schedule> schedules) {
        getMiter().setSchedules(schedules);
        return (self());
    }


    /**
     *  Adds a superseding event.
     *
     *  @param event a superseding event
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public T supersedingEvent(org.osid.calendaring.SupersedingEvent event) {
        getMiter().addSupersedingEvent(event);
        return (self());
    }


    /**
     *  Sets all the superseding events.
     *
     *  @param events a collection of superseding events
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>events</code> is
     *          <code>null</code>
     */

    public T supersedingEvents(java.util.Collection<org.osid.calendaring.SupersedingEvent> events) {
        getMiter().setSupersedingEvents(events);
        return (self());
    }


    /**
     *  Adds a specific meeting time.
     *
     *  @param time a specific meeting time
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public T specificMeetingTime(org.osid.calendaring.MeetingTime time) {
        getMiter().addSpecificMeetingTime(time);
        return (self());
    }


    /**
     *  Sets all the specific meeting times.
     *
     *  @param times a collection of specific meeting times
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>times</code> is
     *          <code>null</code>
     */

    public T specificMeetingTimes(java.util.Collection<org.osid.calendaring.MeetingTime> times) {
        getMiter().setSpecificMeetingTimes(times);
        return (self());
    }


    /**
     *  Adds a blackout.
     *
     *  @param blackout a blackout
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>blackout</code> is <code>null</code>
     */

    public T blackout(org.osid.calendaring.DateTimeInterval blackout) {
        getMiter().addBlackout(blackout);
        return (self());
    }


    /**
     *  Sets all the blackouts.
     *
     *  @param blackouts a collection of blackouts
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>blackouts</code> is <code>null</code>
     */

    public T blackouts(java.util.Collection<org.osid.calendaring.DateTimeInterval> blackouts) {
        getMiter().setBlackouts(blackouts);
        return (self());
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    public T sponsor(org.osid.resource.Resource sponsor) {
        getMiter().addSponsor(sponsor);
        return (self());
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    public T sponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        getMiter().setSponsors(sponsors);
        return (self());
    }


    /**
     *  Adds a RecurringEvent record.
     *
     *  @param record a recurring event record
     *  @param recordType the type of recurring event record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.calendaring.records.RecurringEventRecord record, org.osid.type.Type recordType) {
        getMiter().addRecurringEventRecord(record, recordType);
        return (self());
    }
}       


