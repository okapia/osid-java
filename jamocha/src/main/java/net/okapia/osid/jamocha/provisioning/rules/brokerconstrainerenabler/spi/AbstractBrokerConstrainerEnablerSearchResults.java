//
// AbstractBrokerConstrainerEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBrokerConstrainerEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerSearchResults {

    private org.osid.provisioning.rules.BrokerConstrainerEnablerList brokerConstrainerEnablers;
    private final org.osid.provisioning.rules.BrokerConstrainerEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBrokerConstrainerEnablerSearchResults.
     *
     *  @param brokerConstrainerEnablers the result set
     *  @param brokerConstrainerEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>brokerConstrainerEnablers</code>
     *          or <code>brokerConstrainerEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBrokerConstrainerEnablerSearchResults(org.osid.provisioning.rules.BrokerConstrainerEnablerList brokerConstrainerEnablers,
                                            org.osid.provisioning.rules.BrokerConstrainerEnablerQueryInspector brokerConstrainerEnablerQueryInspector) {
        nullarg(brokerConstrainerEnablers, "broker constrainer enablers");
        nullarg(brokerConstrainerEnablerQueryInspector, "broker constrainer enabler query inspectpr");

        this.brokerConstrainerEnablers = brokerConstrainerEnablers;
        this.inspector = brokerConstrainerEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the broker constrainer enabler list resulting from a search.
     *
     *  @return a broker constrainer enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablers() {
        if (this.brokerConstrainerEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.rules.BrokerConstrainerEnablerList brokerConstrainerEnablers = this.brokerConstrainerEnablers;
        this.brokerConstrainerEnablers = null;
	return (brokerConstrainerEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.rules.BrokerConstrainerEnablerQueryInspector getBrokerConstrainerEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  broker constrainer enabler search record <code> Type. </code> This method must
     *  be used to retrieve a brokerConstrainerEnabler implementing the requested
     *  record.
     *
     *  @param brokerConstrainerEnablerSearchRecordType a brokerConstrainerEnabler search 
     *         record type 
     *  @return the broker constrainer enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(brokerConstrainerEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchResultsRecord getBrokerConstrainerEnablerSearchResultsRecord(org.osid.type.Type brokerConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(brokerConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(brokerConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record broker constrainer enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBrokerConstrainerEnablerRecord(org.osid.provisioning.rules.records.BrokerConstrainerEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "broker constrainer enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
