//
// AbstractQueryObjectiveBankLookupSession.java
//
//    An inline adapter that maps an ObjectiveBankLookupSession to
//    an ObjectiveBankQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.learning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an ObjectiveBankLookupSession to
 *  an ObjectiveBankQuerySession.
 */

public abstract class AbstractQueryObjectiveBankLookupSession
    extends net.okapia.osid.jamocha.learning.spi.AbstractObjectiveBankLookupSession
    implements org.osid.learning.ObjectiveBankLookupSession {

    private final org.osid.learning.ObjectiveBankQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryObjectiveBankLookupSession.
     *
     *  @param querySession the underlying objective bank query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryObjectiveBankLookupSession(org.osid.learning.ObjectiveBankQuerySession querySession) {
        nullarg(querySession, "objective bank query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>ObjectiveBank</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupObjectiveBanks() {
        return (this.session.canSearchObjectiveBanks());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>ObjectiveBank</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ObjectiveBank</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ObjectiveBank</code> and
     *  retained for compatibility.
     *
     *  @param  objectiveBankId <code>Id</code> of the
     *          <code>ObjectiveBank</code>
     *  @return the objective bank
     *  @throws org.osid.NotFoundException <code>objectiveBankId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>objectiveBankId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveBankQuery query = getQuery();
        query.matchId(objectiveBankId, true);
        org.osid.learning.ObjectiveBankList objectiveBanks = this.session.getObjectiveBanksByQuery(query);
        if (objectiveBanks.hasNext()) {
            return (objectiveBanks.getNextObjectiveBank());
        } 
        
        throw new org.osid.NotFoundException(objectiveBankId + " not found");
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  objectiveBanks specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ObjectiveBanks</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  objectiveBankIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ObjectiveBank</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByIds(org.osid.id.IdList objectiveBankIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveBankQuery query = getQuery();

        try (org.osid.id.IdList ids = objectiveBankIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getObjectiveBanksByQuery(query));
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> corresponding to the given
     *  objective bank genus <code>Type</code> which does not include
     *  objective banks of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveBankGenusType an objectiveBank genus type 
     *  @return the returned <code>ObjectiveBank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByGenusType(org.osid.type.Type objectiveBankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveBankQuery query = getQuery();
        query.matchGenusType(objectiveBankGenusType, true);
        return (this.session.getObjectiveBanksByQuery(query));
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> corresponding to the given
     *  objective bank genus <code>Type</code> and include any additional
     *  objective banks with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveBankGenusType an objectiveBank genus type 
     *  @return the returned <code>ObjectiveBank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByParentGenusType(org.osid.type.Type objectiveBankGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveBankQuery query = getQuery();
        query.matchParentGenusType(objectiveBankGenusType, true);
        return (this.session.getObjectiveBanksByQuery(query));
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> containing the given
     *  objective bank record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  objectiveBankRecordType an objectiveBank record type 
     *  @return the returned <code>ObjectiveBank</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>objectiveBankRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByRecordType(org.osid.type.Type objectiveBankRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveBankQuery query = getQuery();
        query.matchRecordType(objectiveBankRecordType, true);
        return (this.session.getObjectiveBanksByQuery(query));
    }


    /**
     *  Gets an <code>ObjectiveBankList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known objective banks or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  objective banks that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>ObjectiveBank</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanksByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveBankQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getObjectiveBanksByQuery(query));        
    }

    
    /**
     *  Gets all <code>ObjectiveBanks</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  objective banks or an error results. Otherwise, the returned list
     *  may contain only those objective banks that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>ObjectiveBanks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.learning.ObjectiveBankQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getObjectiveBanksByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.learning.ObjectiveBankQuery getQuery() {
        org.osid.learning.ObjectiveBankQuery query = this.session.getObjectiveBankQuery();
        return (query);
    }
}
