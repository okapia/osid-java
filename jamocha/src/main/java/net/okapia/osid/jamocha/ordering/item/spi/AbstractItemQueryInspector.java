//
// AbstractItemQueryInspector.java
//
//     A template for making an ItemQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.item.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for items.
 */

public abstract class AbstractItemQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.ordering.ItemQueryInspector {

    private final java.util.Collection<org.osid.ordering.records.ItemQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the order <code> Id </code> terms. 
     *
     *  @return the order <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOrderIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the order terms. 
     *
     *  @return the order terms 
     */

    @OSID @Override
    public org.osid.ordering.OrderQueryInspector[] getOrderTerms() {
        return (new org.osid.ordering.OrderQueryInspector[0]);
    }


    /**
     *  Gets the derived terms. 
     *
     *  @return the derived terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getDerivedTerms() {
        return (new org.osid.search.terms.BooleanTerm[0]);
    }


    /**
     *  Gets the product <code> Id </code> terms. 
     *
     *  @return the product <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProductIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the product terms. 
     *
     *  @return the product terms 
     */

    @OSID @Override
    public org.osid.ordering.ProductQueryInspector[] getProductTerms() {
        return (new org.osid.ordering.ProductQueryInspector[0]);
    }


    /**
     *  Gets the quantity terms. 
     *
     *  @return the quantity terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getQuantityTerms() {
        return (new org.osid.search.terms.CardinalRangeTerm[0]);
    }


    /**
     *  Gets the cost terms. 
     *
     *  @return the cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyRangeTerm[] getCostTerms() {
        return (new org.osid.search.terms.CurrencyRangeTerm[0]);
    }


    /**
     *  Gets the minimum cost terms. 
     *
     *  @return the minimum cost terms 
     */

    @OSID @Override
    public org.osid.search.terms.CurrencyTerm[] getMinimumCostTerms() {
        return (new org.osid.search.terms.CurrencyTerm[0]);
    }


    /**
     *  Gets the store <code> Id </code> terms. 
     *
     *  @return the store <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStoreIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the store terms. 
     *
     *  @return the store terms 
     */

    @OSID @Override
    public org.osid.ordering.StoreQueryInspector[] getStoreTerms() {
        return (new org.osid.ordering.StoreQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given item query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an item implementing the requested record.
     *
     *  @param itemRecordType an item record type
     *  @return the item query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(itemRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ItemQueryInspectorRecord getItemQueryInspectorRecord(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ItemQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(itemRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(itemRecordType + " is not supported");
    }


    /**
     *  Adds a record to this item query. 
     *
     *  @param itemQueryInspectorRecord item query inspector
     *         record
     *  @param itemRecordType item record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addItemQueryInspectorRecord(org.osid.ordering.records.ItemQueryInspectorRecord itemQueryInspectorRecord, 
                                                   org.osid.type.Type itemRecordType) {

        addRecordType(itemRecordType);
        nullarg(itemRecordType, "item record type");
        this.records.add(itemQueryInspectorRecord);        
        return;
    }
}
