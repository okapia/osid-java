//
// AbstractAdapterPlanLookupSession.java
//
//    A Plan lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.plan.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Plan lookup session adapter.
 */

public abstract class AbstractAdapterPlanLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.plan.PlanLookupSession {

    private final org.osid.course.plan.PlanLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterPlanLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterPlanLookupSession(org.osid.course.plan.PlanLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code CourseCatalog/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code CourseCatalog Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@code CourseCatalog} associated with this session.
     *
     *  @return the {@code CourseCatalog} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@code Plan} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupPlans() {
        return (this.session.canLookupPlans());
    }


    /**
     *  A complete view of the {@code Plan} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePlanView() {
        this.session.useComparativePlanView();
        return;
    }


    /**
     *  A complete view of the {@code Plan} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPlanView() {
        this.session.usePlenaryPlanView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include plans in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    

    /**
     *  Only plans whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectivePlanView() {
        this.session.useEffectivePlanView();
        return;
    }
    

    /**
     *  All plans of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectivePlanView() {
        this.session.useAnyEffectivePlanView();
        return;
    }

     
    /**
     *  Gets the {@code Plan} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Plan} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Plan} and
     *  retained for compatibility.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param planId {@code Id} of the {@code Plan}
     *  @return the plan
     *  @throws org.osid.NotFoundException {@code planId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code planId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.Plan getPlan(org.osid.id.Id planId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlan(planId));
    }


    /**
     *  Gets a {@code PlanList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  plans specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Plans} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param  planIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Plan} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code planIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByIds(org.osid.id.IdList planIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansByIds(planIds));
    }


    /**
     *  Gets a {@code PlanList} corresponding to the given
     *  plan genus {@code Type} which does not include
     *  plans of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param  planGenusType a plan genus type 
     *  @return the returned {@code Plan} list
     *  @throws org.osid.NullArgumentException
     *          {@code planGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByGenusType(org.osid.type.Type planGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansByGenusType(planGenusType));
    }


    /**
     *  Gets a {@code PlanList} corresponding to the given
     *  plan genus {@code Type} and include any additional
     *  plans with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param  planGenusType a plan genus type 
     *  @return the returned {@code Plan} list
     *  @throws org.osid.NullArgumentException
     *          {@code planGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByParentGenusType(org.osid.type.Type planGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansByParentGenusType(planGenusType));
    }


    /**
     *  Gets a {@code PlanList} containing the given
     *  plan record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param  planRecordType a plan record type 
     *  @return the returned {@code Plan} list
     *  @throws org.osid.NullArgumentException
     *          {@code planRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansByRecordType(org.osid.type.Type planRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansByRecordType(planRecordType));
    }


    /**
     *  Gets a {@code PlanList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *  
     *  In active mode, plans are returned that are currently
     *  active. In any status mode, active and inactive plans
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Plan} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.course.plan.PlanList getPlansOnDate(org.osid.calendaring.DateTime from, 
                                                        org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansOnDate(from, to));
    }
        

    /**
     *  Gets a list of plans corresponding to a syllabus
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  syllabusId the {@code Id} of the syllabus
     *  @return the returned {@code PlanList}
     *  @throws org.osid.NullArgumentException {@code syllabusId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabus(org.osid.id.Id syllabusId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansForSyllabus(syllabusId));
    }


    /**
     *  Gets a list of plans corresponding to a syllabus
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  syllabusId the {@code Id} of the syllabus
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code PlanList}
     *  @throws org.osid.NullArgumentException {@code syllabusId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabusOnDate(org.osid.id.Id syllabusId,
                                                                   org.osid.calendaring.DateTime from,
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansForSyllabusOnDate(syllabusId, from, to));
    }


    /**
     *  Gets a list of plans corresponding to a course offering
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @return the returned {@code PlanList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForCourseOffering(org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansForCourseOffering(courseOfferingId));
    }


    /**
     *  Gets a list of plans corresponding to a course offering
     *  {@code Id} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code PlanList}
     *  @throws org.osid.NullArgumentException {@code courseOfferingId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForCourseOfferingOnDate(org.osid.id.Id courseOfferingId,
                                                                         org.osid.calendaring.DateTime from,
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansForCourseOfferingOnDate(courseOfferingId, from, to));
    }


    /**
     *  Gets a list of plans corresponding to syllabus and course offering
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are
     *  currently effective.  In any effective mode, effective
     *  plans and those currently expired are returned.
     *
     *  @param  syllabusId the {@code Id} of the syllabus
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @return the returned {@code PlanList}
     *  @throws org.osid.NullArgumentException {@code syllabusId},
     *          {@code courseOfferingId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabusAndCourseOffering(org.osid.id.Id syllabusId,
                                                                              org.osid.id.Id courseOfferingId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansForSyllabusAndCourseOffering(syllabusId, courseOfferingId));
    }


    /**
     *  Gets a list of plans corresponding to syllabus and course offering
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible
     *  through this session.
     *
     *  In effective mode, plans are returned that are currently
     *  effective. In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @param  courseOfferingId the {@code Id} of the course offering
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code PlanList}
     *  @throws org.osid.NullArgumentException {@code syllabusId},
     *          {@code courseOfferingId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlansForSyllabusAndCourseOfferingOnDate(org.osid.id.Id syllabusId,
                                                                                    org.osid.id.Id courseOfferingId,
                                                                                    org.osid.calendaring.DateTime from,
                                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlansForSyllabusAndCourseOfferingOnDate(syllabusId, courseOfferingId, from, to));
    }


    /**
     *  Gets all {@code Plans}. 
     *
     *  In plenary mode, the returned list contains all known
     *  plans or an error results. Otherwise, the returned list
     *  may contain only those plans that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, plans are returned that are currently
     *  effective.  In any effective mode, effective plans and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Plans} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.plan.PlanList getPlans()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getPlans());
    }
}
