//
// AbstractFamilyLookupSession.java
//
//    A starter implementation framework for providing a Family
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.relationship.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Family lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getFamilies(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractFamilyLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.relationship.FamilyLookupSession {

    private boolean pedantic = false;
    private org.osid.relationship.Family family = new net.okapia.osid.jamocha.nil.relationship.family.UnknownFamily();
    


    /**
     *  Tests if this user can perform <code>Family</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFamilies() {
        return (true);
    }


    /**
     *  A complete view of the <code>Family</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFamilyView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Family</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFamilyView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Family</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Family</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Family</code> and retained for
     *  compatibility.
     *
     *  @param  familyId <code>Id</code> of the
     *          <code>Family</code>
     *  @return the family
     *  @throws org.osid.NotFoundException <code>familyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>familyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.Family getFamily(org.osid.id.Id familyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.relationship.FamilyList families = getFamilies()) {
            while (families.hasNext()) {
                org.osid.relationship.Family family = families.getNextFamily();
                if (family.getId().equals(familyId)) {
                    return (family);
                }
            }
        } 

        throw new org.osid.NotFoundException(familyId + " not found");
    }


    /**
     *  Gets a <code>FamilyList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  families specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Families</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getFamilies()</code>.
     *
     *  @param  familyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Family</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>familyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByIds(org.osid.id.IdList familyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.relationship.Family> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = familyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getFamily(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("family " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.relationship.family.LinkedFamilyList(ret));
    }


    /**
     *  Gets a <code>FamilyList</code> corresponding to the given
     *  family genus <code>Type</code> which does not include families
     *  of types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known families
     *  or an error results. Otherwise, the returned list may contain
     *  only those families that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getFamilies()</code>.
     *
     *  @param  familyGenusType a family genus type 
     *  @return the returned <code>Family</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>familyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByGenusType(org.osid.type.Type familyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.family.FamilyGenusFilterList(getFamilies(), familyGenusType));
    }


    /**
     *  Gets a <code>FamilyList</code> corresponding to the given
     *  family genus <code>Type</code> and include any additional
     *  families with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known families
     *  or an error results. Otherwise, the returned list may contain
     *  only those families that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getFamilies()</code>.
     *
     *  @param  familyGenusType a family genus type 
     *  @return the returned <code>Family</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>familyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByParentGenusType(org.osid.type.Type familyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getFamiliesByGenusType(familyGenusType));
    }


    /**
     *  Gets a <code>FamilyList</code> containing the given family
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known families
     *  or an error results. Otherwise, the returned list may contain
     *  only those families that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getFamilies()</code>.
     *
     *  @param  familyRecordType a family record type 
     *  @return the returned <code>Family</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>familyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByRecordType(org.osid.type.Type familyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.relationship.family.FamilyRecordFilterList(getFamilies(), familyRecordType));
    }


    /**
     *  Gets a <code>FamilyList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known families
     *  or an error results. Otherwise, the returned list may contain
     *  only those families that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Family</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.relationship.FamilyList getFamiliesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.relationship.family.FamilyProviderFilterList(getFamilies(), resourceId));
    }


    /**
     *  Gets all <code>Families</code>.
     *
     *  In plenary mode, the returned list contains all known families
     *  or an error results. Otherwise, the returned list may contain
     *  only those families that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Families</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.relationship.FamilyList getFamilies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the family list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of families
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.relationship.FamilyList filterFamiliesOnViews(org.osid.relationship.FamilyList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
