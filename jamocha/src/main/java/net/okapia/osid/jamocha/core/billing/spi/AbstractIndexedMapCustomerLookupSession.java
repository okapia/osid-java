//
// AbstractIndexedMapCustomerLookupSession.java
//
//    A simple framework for providing a Customer lookup service
//    backed by a fixed collection of customers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Customer lookup service backed by a
 *  fixed collection of customers. The customers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some customers may be compatible
 *  with more types than are indicated through these customer
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Customers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCustomerLookupSession
    extends AbstractMapCustomerLookupSession
    implements org.osid.billing.CustomerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.billing.Customer> customersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Customer>());
    private final MultiMap<org.osid.type.Type, org.osid.billing.Customer> customersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Customer>());


    /**
     *  Makes a <code>Customer</code> available in this session.
     *
     *  @param  customer a customer
     *  @throws org.osid.NullArgumentException <code>customer<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCustomer(org.osid.billing.Customer customer) {
        super.putCustomer(customer);

        this.customersByGenus.put(customer.getGenusType(), customer);
        
        try (org.osid.type.TypeList types = customer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.customersByRecord.put(types.getNextType(), customer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a customer from this session.
     *
     *  @param customerId the <code>Id</code> of the customer
     *  @throws org.osid.NullArgumentException <code>customerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCustomer(org.osid.id.Id customerId) {
        org.osid.billing.Customer customer;
        try {
            customer = getCustomer(customerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.customersByGenus.remove(customer.getGenusType());

        try (org.osid.type.TypeList types = customer.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.customersByRecord.remove(types.getNextType(), customer);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCustomer(customerId);
        return;
    }


    /**
     *  Gets a <code>CustomerList</code> corresponding to the given
     *  customer genus <code>Type</code> which does not include
     *  customers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known customers or an error results. Otherwise,
     *  the returned list may contain only those customers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  customerGenusType a customer genus type 
     *  @return the returned <code>Customer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>customerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByGenusType(org.osid.type.Type customerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.customer.ArrayCustomerList(this.customersByGenus.get(customerGenusType)));
    }


    /**
     *  Gets a <code>CustomerList</code> containing the given
     *  customer record <code>Type</code>. In plenary mode, the
     *  returned list contains all known customers or an error
     *  results. Otherwise, the returned list may contain only those
     *  customers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  customerRecordType a customer record type 
     *  @return the returned <code>customer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>customerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.CustomerList getCustomersByRecordType(org.osid.type.Type customerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.customer.ArrayCustomerList(this.customersByRecord.get(customerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.customersByGenus.clear();
        this.customersByRecord.clear();

        super.close();

        return;
    }
}
