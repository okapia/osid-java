//
// InvariantIndexedMapPriceScheduleLookupSession
//
//    Implements a PriceSchedule lookup service backed by a fixed
//    collection of priceSchedules indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering;


/**
 *  Implements a PriceSchedule lookup service backed by a fixed
 *  collection of price schedules. The price schedules are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some price schedules may be compatible
 *  with more types than are indicated through these price schedule
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapPriceScheduleLookupSession
    extends net.okapia.osid.jamocha.core.ordering.spi.AbstractIndexedMapPriceScheduleLookupSession
    implements org.osid.ordering.PriceScheduleLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapPriceScheduleLookupSession} using an
     *  array of priceSchedules.
     *
     *  @param store the store
     *  @param priceSchedules an array of price schedules
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code priceSchedules} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapPriceScheduleLookupSession(org.osid.ordering.Store store,
                                                    org.osid.ordering.PriceSchedule[] priceSchedules) {

        setStore(store);
        putPriceSchedules(priceSchedules);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapPriceScheduleLookupSession} using a
     *  collection of price schedules.
     *
     *  @param store the store
     *  @param priceSchedules a collection of price schedules
     *  @throws org.osid.NullArgumentException {@code store},
     *          {@code priceSchedules} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapPriceScheduleLookupSession(org.osid.ordering.Store store,
                                                    java.util.Collection<? extends org.osid.ordering.PriceSchedule> priceSchedules) {

        setStore(store);
        putPriceSchedules(priceSchedules);
        return;
    }
}
