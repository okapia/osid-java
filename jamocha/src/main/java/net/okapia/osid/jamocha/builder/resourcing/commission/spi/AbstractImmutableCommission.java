//
// AbstractImmutableCommission.java
//
//     Wraps a mutable Commission to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.resourcing.commission.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Commission</code> to hide modifiers. This
 *  wrapper provides an immutized Commission from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying commission whose state changes are visible.
 */

public abstract class AbstractImmutableCommission
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.resourcing.Commission {

    private final org.osid.resourcing.Commission commission;


    /**
     *  Constructs a new <code>AbstractImmutableCommission</code>.
     *
     *  @param commission the commission to immutablize
     *  @throws org.osid.NullArgumentException <code>commission</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCommission(org.osid.resourcing.Commission commission) {
        super(commission);
        this.commission = commission;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the commissioned resource. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.commission.getResourceId());
    }


    /**
     *  Gets the commissioned resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.commission.getResource());
    }


    /**
     *  Gets the <code> Id </code> of the work. 
     *
     *  @return the work <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getWorkId() {
        return (this.commission.getWorkId());
    }


    /**
     *  Gets the work. 
     *
     *  @return the work 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Work getWork()
        throws org.osid.OperationFailedException {

        return (this.commission.getWork());
    }


    /**
     *  Tests if a competency is specified for this commission. 
     *
     *  @return <code> true </code> if a competency is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isCompetent() {
        return (this.commission.isCompetent());
    }


    /**
     *  Gets the competency <code> Id. </code> 
     *
     *  @return the competency <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isCompetent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCompetencyId() {
        return (this.commission.getCompetencyId());
    }


    /**
     *  Gets the competency. 
     *
     *  @return the competency 
     *  @throws org.osid.IllegalStateException <code> isCompetent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Competency getCompetency()
        throws org.osid.OperationFailedException {

        return (this.commission.getCompetency());
    }


    /**
     *  Gets the percentage commitment. 
     *
     *  @return the percentage commitment 
     */

    @OSID @Override
    public long getPercentage() {
        return (this.commission.getPercentage());
    }


    /**
     *  Gets the commission record corresponding to the given <code> 
     *  Commission </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  commissionRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(commissionRecordType) </code> is <code> true </code> . 
     *
     *  @param  commissionRecordType the type of commission record to retrieve 
     *  @return the commission record 
     *  @throws org.osid.NullArgumentException <code> commissionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(commissionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.records.CommissionRecord getCommissionRecord(org.osid.type.Type commissionRecordType)
        throws org.osid.OperationFailedException {

        return (this.commission.getCommissionRecord(commissionRecordType));
    }
}

