//
// AbstractIndexedMapCommitmentEnablerLookupSession.java
//
//    A simple framework for providing a CommitmentEnabler lookup service
//    backed by a fixed collection of commitment enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CommitmentEnabler lookup service backed by a
 *  fixed collection of commitment enablers. The commitment enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some commitment enablers may be compatible
 *  with more types than are indicated through these commitment enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CommitmentEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCommitmentEnablerLookupSession
    extends AbstractMapCommitmentEnablerLookupSession
    implements org.osid.calendaring.rules.CommitmentEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.rules.CommitmentEnabler> commitmentEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.rules.CommitmentEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.rules.CommitmentEnabler> commitmentEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.rules.CommitmentEnabler>());


    /**
     *  Makes a <code>CommitmentEnabler</code> available in this session.
     *
     *  @param  commitmentEnabler a commitment enabler
     *  @throws org.osid.NullArgumentException <code>commitmentEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCommitmentEnabler(org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler) {
        super.putCommitmentEnabler(commitmentEnabler);

        this.commitmentEnablersByGenus.put(commitmentEnabler.getGenusType(), commitmentEnabler);
        
        try (org.osid.type.TypeList types = commitmentEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commitmentEnablersByRecord.put(types.getNextType(), commitmentEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a commitment enabler from this session.
     *
     *  @param commitmentEnablerId the <code>Id</code> of the commitment enabler
     *  @throws org.osid.NullArgumentException <code>commitmentEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCommitmentEnabler(org.osid.id.Id commitmentEnablerId) {
        org.osid.calendaring.rules.CommitmentEnabler commitmentEnabler;
        try {
            commitmentEnabler = getCommitmentEnabler(commitmentEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.commitmentEnablersByGenus.remove(commitmentEnabler.getGenusType());

        try (org.osid.type.TypeList types = commitmentEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.commitmentEnablersByRecord.remove(types.getNextType(), commitmentEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCommitmentEnabler(commitmentEnablerId);
        return;
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> corresponding to the given
     *  commitment enabler genus <code>Type</code> which does not include
     *  commitment enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known commitment enablers or an error results. Otherwise,
     *  the returned list may contain only those commitment enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  commitmentEnablerGenusType a commitment enabler genus type 
     *  @return the returned <code>CommitmentEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByGenusType(org.osid.type.Type commitmentEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.commitmentenabler.ArrayCommitmentEnablerList(this.commitmentEnablersByGenus.get(commitmentEnablerGenusType)));
    }


    /**
     *  Gets a <code>CommitmentEnablerList</code> containing the given
     *  commitment enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known commitment enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  commitment enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  commitmentEnablerRecordType a commitment enabler record type 
     *  @return the returned <code>commitmentEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>commitmentEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.rules.CommitmentEnablerList getCommitmentEnablersByRecordType(org.osid.type.Type commitmentEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.rules.commitmentenabler.ArrayCommitmentEnablerList(this.commitmentEnablersByRecord.get(commitmentEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.commitmentEnablersByGenus.clear();
        this.commitmentEnablersByRecord.clear();

        super.close();

        return;
    }
}
