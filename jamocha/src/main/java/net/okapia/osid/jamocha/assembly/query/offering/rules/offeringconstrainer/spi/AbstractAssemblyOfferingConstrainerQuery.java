//
// AbstractAssemblyOfferingConstrainerQuery.java
//
//     An OfferingConstrainerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.offering.rules.offeringconstrainer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An OfferingConstrainerQuery that stores terms.
 */

public abstract class AbstractAssemblyOfferingConstrainerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidConstrainerQuery
    implements org.osid.offering.rules.OfferingConstrainerQuery,
               org.osid.offering.rules.OfferingConstrainerQueryInspector,
               org.osid.offering.rules.OfferingConstrainerSearchOrder {

    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.offering.rules.records.OfferingConstrainerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyOfferingConstrainerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyOfferingConstrainerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to the canonical unit. 
     *
     *  @param  canonicalUnitId the canonical unit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> canonicalUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCanonicalUnitId(org.osid.id.Id canonicalUnitId, 
                                          boolean match) {
        getAssembler().addIdTerm(getRuledCanonicalUnitIdColumn(), canonicalUnitId, match);
        return;
    }


    /**
     *  Clears the canonical unit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitIdTerms() {
        getAssembler().clearTerms(getRuledCanonicalUnitIdColumn());
        return;
    }


    /**
     *  Gets the canonical unit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCanonicalUnitIdTerms() {
        return (getAssembler().getIdTerms(getRuledCanonicalUnitIdColumn()));
    }


    /**
     *  Gets the RuledCanonicalUnitId column name.
     *
     * @return the column name
     */

    protected String getRuledCanonicalUnitIdColumn() {
        return ("ruled_canonical_unit_id");
    }


    /**
     *  Tests if an <code> CanonicalUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if a canonical unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCanonicalUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for a canonical unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the canonical unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCanonicalUnitQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQuery getRuledCanonicalUnitQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCanonicalUnitQuery() is false");
    }


    /**
     *  Matches rules mapped to any canonical unit. 
     *
     *  @param  match <code> true </code> for mapped to any canonical unit, 
     *          <code> false </code> to match mapped to no canonicalUnit 
     */

    @OSID @Override
    public void matchAnyRuledCanonicalUnit(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledCanonicalUnitColumn(), match);
        return;
    }


    /**
     *  Clears the canonical unit query terms. 
     */

    @OSID @Override
    public void clearRuledCanonicalUnitTerms() {
        getAssembler().clearTerms(getRuledCanonicalUnitColumn());
        return;
    }


    /**
     *  Gets the canonical unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQueryInspector[] getRuledCanonicalUnitTerms() {
        return (new org.osid.offering.CanonicalUnitQueryInspector[0]);
    }


    /**
     *  Gets the RuledCanonicalUnit column name.
     *
     * @return the column name
     */

    protected String getRuledCanonicalUnitColumn() {
        return ("ruled_canonical_unit");
    }


    /**
     *  Matches description overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideDescription(boolean match) {
        getAssembler().addBooleanTerm(getOverrideDescriptionColumn(), match);
        return;
    }


    /**
     *  Matches any description override. 
     *
     *  @param  match <code> true </code> for any description override, <code> 
     *          false </code> to match no description override 
     */

    @OSID @Override
    public void matchAnyOverrideDescription(boolean match) {
        getAssembler().addBooleanWildcardTerm(getOverrideDescriptionColumn(), match);
        return;
    }


    /**
     *  Clears the description override query terms. 
     */

    @OSID @Override
    public void clearOverrideDescriptionTerms() {
        getAssembler().clearTerms(getOverrideDescriptionColumn());
        return;
    }


    /**
     *  Gets the override description query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOverrideDescriptionTerms() {
        return (getAssembler().getBooleanTerms(getOverrideDescriptionColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the description 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideDescription(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOverrideDescriptionColumn(), style);
        return;
    }


    /**
     *  Gets the OverrideDescription column name.
     *
     * @return the column name
     */

    protected String getOverrideDescriptionColumn() {
        return ("override_description");
    }


    /**
     *  Matches title overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideTitle(boolean match) {
        getAssembler().addBooleanTerm(getOverrideTitleColumn(), match);
        return;
    }


    /**
     *  Matches any title override. 
     *
     *  @param  match <code> true </code> for any title override, <code> false 
     *          </code> to match no title override 
     */

    @OSID @Override
    public void matchAnyOverrideTitle(boolean match) {
        getAssembler().addBooleanWildcardTerm(getOverrideTitleColumn(), match);
        return;
    }


    /**
     *  Clears the title override query terms. 
     */

    @OSID @Override
    public void clearOverrideTitleTerms() {
        getAssembler().clearTerms(getOverrideTitleColumn());
        return;
    }


    /**
     *  Gets the override title query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOverrideTitleTerms() {
        return (getAssembler().getBooleanTerms(getOverrideTitleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the title 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideTitle(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOverrideTitleColumn(), style);
        return;
    }


    /**
     *  Gets the OverrideTitle column name.
     *
     * @return the column name
     */

    protected String getOverrideTitleColumn() {
        return ("override_title");
    }


    /**
     *  Matches code overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideCode(boolean match) {
        getAssembler().addBooleanTerm(getOverrideCodeColumn(), match);
        return;
    }


    /**
     *  Matches any code override. 
     *
     *  @param  match <code> true </code> for any code override, <code> false 
     *          </code> to match no code override 
     */

    @OSID @Override
    public void matchAnyOverrideCode(boolean match) {
        getAssembler().addBooleanWildcardTerm(getOverrideCodeColumn(), match);
        return;
    }


    /**
     *  Clears the code override query terms. 
     */

    @OSID @Override
    public void clearOverrideCodeTerms() {
        getAssembler().clearTerms(getOverrideCodeColumn());
        return;
    }


    /**
     *  Gets the override code query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOverrideCodeTerms() {
        return (getAssembler().getBooleanTerms(getOverrideCodeColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the code 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideCode(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOverrideCodeColumn(), style);
        return;
    }


    /**
     *  Gets the OverrideCode column name.
     *
     * @return the column name
     */

    protected String getOverrideCodeColumn() {
        return ("override_code");
    }


    /**
     *  Matches time periods overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideTimePeriods(boolean match) {
        getAssembler().addBooleanTerm(getOverrideTimePeriodsColumn(), match);
        return;
    }


    /**
     *  Matches any time periods override. 
     *
     *  @param  match <code> true </code> for any time periods override, 
     *          <code> false </code> to match no time periods override 
     */

    @OSID @Override
    public void matchAnyOverrideTimePeriods(boolean match) {
        getAssembler().addBooleanWildcardTerm(getOverrideTimePeriodsColumn(), match);
        return;
    }


    /**
     *  Clears the time periods override query terms. 
     */

    @OSID @Override
    public void clearOverrideTimePeriodsTerms() {
        getAssembler().clearTerms(getOverrideTimePeriodsColumn());
        return;
    }


    /**
     *  Gets the override time periods query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOverrideTimePeriodsTerms() {
        return (getAssembler().getBooleanTerms(getOverrideTimePeriodsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the time periods 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideTimePeriods(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOverrideTimePeriodsColumn(), style);
        return;
    }


    /**
     *  Gets the OverrideTimePeriods column name.
     *
     * @return the column name
     */

    protected String getOverrideTimePeriodsColumn() {
        return ("override_time_periods");
    }


    /**
     *  Matches constrain time periods. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchConstrainTimePeriods(boolean match) {
        getAssembler().addBooleanTerm(getConstrainTimePeriodsColumn(), match);
        return;
    }


    /**
     *  Matches any constrain time periods. 
     *
     *  @param  match <code> true </code> for any time periods constraints, 
     *          <code> false </code> to match no time periods constraints 
     */

    @OSID @Override
    public void matchAnyConstrainTimePeriods(boolean match) {
        getAssembler().addBooleanWildcardTerm(getConstrainTimePeriodsColumn(), match);
        return;
    }


    /**
     *  Clears the constrain time periods query terms. 
     */

    @OSID @Override
    public void clearConstrainTimePeriodsTerms() {
        getAssembler().clearTerms(getConstrainTimePeriodsColumn());
        return;
    }


    /**
     *  Gets the override time periods query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getConstrainTimePeriodsTerms() {
        return (getAssembler().getBooleanTerms(getConstrainTimePeriodsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the constrain 
     *  time periods flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByConstrainTimePeriods(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getConstrainTimePeriodsColumn(), style);
        return;
    }


    /**
     *  Gets the ConstrainTimePeriods column name.
     *
     * @return the column name
     */

    protected String getConstrainTimePeriodsColumn() {
        return ("constrain_time_periods");
    }


    /**
     *  Matches result options overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideResultOptions(boolean match) {
        getAssembler().addBooleanTerm(getOverrideResultOptionsColumn(), match);
        return;
    }


    /**
     *  Matches any result options override. 
     *
     *  @param  match <code> true </code> for any result options override, 
     *          <code> false </code> to match no result options override 
     */

    @OSID @Override
    public void matchAnyOverrideResultOptions(boolean match) {
        getAssembler().addBooleanWildcardTerm(getOverrideResultOptionsColumn(), match);
        return;
    }


    /**
     *  Clears the result options override query terms. 
     */

    @OSID @Override
    public void clearOverrideResultOptionsTerms() {
        getAssembler().clearTerms(getOverrideResultOptionsColumn());
        return;
    }


    /**
     *  Gets the override result options query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOverrideResultOptionsTerms() {
        return (getAssembler().getBooleanTerms(getOverrideResultOptionsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the description 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideResultOptions(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOverrideResultOptionsColumn(), style);
        return;
    }


    /**
     *  Gets the OverrideResultOptions column name.
     *
     * @return the column name
     */

    protected String getOverrideResultOptionsColumn() {
        return ("override_result_options");
    }


    /**
     *  Matches constrain result options. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchConstrainResultOptions(boolean match) {
        getAssembler().addBooleanTerm(getConstrainResultOptionsColumn(), match);
        return;
    }


    /**
     *  Matches any constrain result options. 
     *
     *  @param  match <code> true </code> for any result options constraints, 
     *          <code> false </code> to match no result options constraints 
     */

    @OSID @Override
    public void matchAnyConstrainResultOptions(boolean match) {
        getAssembler().addBooleanWildcardTerm(getConstrainResultOptionsColumn(), match);
        return;
    }


    /**
     *  Clears the constrain result options query terms. 
     */

    @OSID @Override
    public void clearConstrainResultOptionsTerms() {
        getAssembler().clearTerms(getConstrainResultOptionsColumn());
        return;
    }


    /**
     *  Gets the constrain result options query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getConstrainResultOptionsTerms() {
        return (getAssembler().getBooleanTerms(getConstrainResultOptionsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the constrain 
     *  result options flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByConstrainResultOptions(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getConstrainResultOptionsColumn(), style);
        return;
    }


    /**
     *  Gets the ConstrainResultOptions column name.
     *
     * @return the column name
     */

    protected String getConstrainResultOptionsColumn() {
        return ("constrain_result_options");
    }


    /**
     *  Matches result options overrides. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchOverrideSponsors(boolean match) {
        getAssembler().addBooleanTerm(getOverrideSponsorsColumn(), match);
        return;
    }


    /**
     *  Matches any sponsors override. 
     *
     *  @param  match <code> true </code> for any sponsors override, <code> 
     *          false </code> to match no sponsors override 
     */

    @OSID @Override
    public void matchAnyOverrideSponsors(boolean match) {
        getAssembler().addBooleanWildcardTerm(getOverrideSponsorsColumn(), match);
        return;
    }


    /**
     *  Clears the sponsors override query terms. 
     */

    @OSID @Override
    public void clearOverrideSponsorsTerms() {
        getAssembler().clearTerms(getOverrideSponsorsColumn());
        return;
    }


    /**
     *  Gets the override sponsors query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getOverrideSponsorsTerms() {
        return (getAssembler().getBooleanTerms(getOverrideSponsorsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the description 
     *  override. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByOverrideSponsors(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getOverrideSponsorsColumn(), style);
        return;
    }


    /**
     *  Gets the OverrideSponsors column name.
     *
     * @return the column name
     */

    protected String getOverrideSponsorsColumn() {
        return ("override_sponsors");
    }


    /**
     *  Matches constrain sponsors. 
     *
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchConstrainSponsors(boolean match) {
        getAssembler().addBooleanTerm(getConstrainSponsorsColumn(), match);
        return;
    }


    /**
     *  Matches any constrain sponsors. 
     *
     *  @param  match <code> true </code> for any sponsors constraints, <code> 
     *          false </code> to match no sponsors constraints 
     */

    @OSID @Override
    public void matchAnyConstrainSponsors(boolean match) {
        getAssembler().addBooleanWildcardTerm(getConstrainSponsorsColumn(), match);
        return;
    }


    /**
     *  Clears the constrain sponsors query terms. 
     */

    @OSID @Override
    public void clearConstrainSponsorsTerms() {
        getAssembler().clearTerms(getConstrainSponsorsColumn());
        return;
    }


    /**
     *  Gets the constrain sponsors query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getConstrainSponsorsTerms() {
        return (getAssembler().getBooleanTerms(getConstrainSponsorsColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the constrain 
     *  sponsors flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByConstrainSponsors(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getConstrainSponsorsColumn(), style);
        return;
    }


    /**
     *  Gets the ConstrainSponsors column name.
     *
     * @return the column name
     */

    protected String getConstrainSponsorsColumn() {
        return ("constrain_sponsors");
    }


    /**
     *  Matches mapped to the catalogue. 
     *
     *  @param  catalogueId the catalogue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCatalogueId(org.osid.id.Id catalogueId, boolean match) {
        getAssembler().addIdTerm(getCatalogueIdColumn(), catalogueId, match);
        return;
    }


    /**
     *  Clears the catalogue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCatalogueIdTerms() {
        getAssembler().clearTerms(getCatalogueIdColumn());
        return;
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (getAssembler().getIdTerms(getCatalogueIdColumn()));
    }


    /**
     *  Gets the CatalogueId column name.
     *
     * @return the column name
     */

    protected String getCatalogueIdColumn() {
        return ("catalogue_id");
    }


    /**
     *  Tests if a <code> CatalogueQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalogue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCatalogueQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalogue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalogue query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCatalogueQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQuery getCatalogueQuery() {
        throw new org.osid.UnimplementedException("supportsCatalogueQuery() is false");
    }


    /**
     *  Clears the catalogue query terms. 
     */

    @OSID @Override
    public void clearCatalogueTerms() {
        getAssembler().clearTerms(getCatalogueColumn());
        return;
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }


    /**
     *  Gets the Catalogue column name.
     *
     * @return the column name
     */

    protected String getCatalogueColumn() {
        return ("catalogue");
    }


    /**
     *  Tests if this offeringConstrainer supports the given record
     *  <code>Type</code>.
     *
     *  @param  offeringConstrainerRecordType an offering constrainer record type 
     *  @return <code>true</code> if the offeringConstrainerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type offeringConstrainerRecordType) {
        for (org.osid.offering.rules.records.OfferingConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(offeringConstrainerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  offeringConstrainerRecordType the offering constrainer record type 
     *  @return the offering constrainer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerQueryRecord getOfferingConstrainerQueryRecord(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(offeringConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  offeringConstrainerRecordType the offering constrainer record type 
     *  @return the offering constrainer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerQueryInspectorRecord getOfferingConstrainerQueryInspectorRecord(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(offeringConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param offeringConstrainerRecordType the offering constrainer record type
     *  @return the offering constrainer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringConstrainerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.OfferingConstrainerSearchOrderRecord getOfferingConstrainerSearchOrderRecord(org.osid.type.Type offeringConstrainerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.OfferingConstrainerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(offeringConstrainerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringConstrainerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this offering constrainer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param offeringConstrainerQueryRecord the offering constrainer query record
     *  @param offeringConstrainerQueryInspectorRecord the offering constrainer query inspector
     *         record
     *  @param offeringConstrainerSearchOrderRecord the offering constrainer search order record
     *  @param offeringConstrainerRecordType offering constrainer record type
     *  @throws org.osid.NullArgumentException
     *          <code>offeringConstrainerQueryRecord</code>,
     *          <code>offeringConstrainerQueryInspectorRecord</code>,
     *          <code>offeringConstrainerSearchOrderRecord</code> or
     *          <code>offeringConstrainerRecordTypeofferingConstrainer</code> is
     *          <code>null</code>
     */
            
    protected void addOfferingConstrainerRecords(org.osid.offering.rules.records.OfferingConstrainerQueryRecord offeringConstrainerQueryRecord, 
                                      org.osid.offering.rules.records.OfferingConstrainerQueryInspectorRecord offeringConstrainerQueryInspectorRecord, 
                                      org.osid.offering.rules.records.OfferingConstrainerSearchOrderRecord offeringConstrainerSearchOrderRecord, 
                                      org.osid.type.Type offeringConstrainerRecordType) {

        addRecordType(offeringConstrainerRecordType);

        nullarg(offeringConstrainerQueryRecord, "offering constrainer query record");
        nullarg(offeringConstrainerQueryInspectorRecord, "offering constrainer query inspector record");
        nullarg(offeringConstrainerSearchOrderRecord, "offering constrainer search odrer record");

        this.queryRecords.add(offeringConstrainerQueryRecord);
        this.queryInspectorRecords.add(offeringConstrainerQueryInspectorRecord);
        this.searchOrderRecords.add(offeringConstrainerSearchOrderRecord);
        
        return;
    }
}
