//
// InvariantMapProxyCredentialEntryLookupSession
//
//    Implements a CredentialEntry lookup service backed by a fixed
//    collection of credentialEntries. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle;


/**
 *  Implements a CredentialEntry lookup service backed by a fixed
 *  collection of credential entries. The credential entries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyCredentialEntryLookupSession
    extends net.okapia.osid.jamocha.core.course.chronicle.spi.AbstractMapCredentialEntryLookupSession
    implements org.osid.course.chronicle.CredentialEntryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCredentialEntryLookupSession} with no
     *  credential entries.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyCredentialEntryLookupSession} with a single
     *  credential entry.
     *
     *  @param courseCatalog the course catalog
     *  @param credentialEntry a single credential entry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code credentialEntry} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.chronicle.CredentialEntry credentialEntry, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCredentialEntry(credentialEntry);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyCredentialEntryLookupSession} using
     *  an array of credential entries.
     *
     *  @param courseCatalog the course catalog
     *  @param credentialEntries an array of credential entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code credentialEntries} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  org.osid.course.chronicle.CredentialEntry[] credentialEntries, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCredentialEntries(credentialEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyCredentialEntryLookupSession} using a
     *  collection of credential entries.
     *
     *  @param courseCatalog the course catalog
     *  @param credentialEntries a collection of credential entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code credentialEntries} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyCredentialEntryLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                  java.util.Collection<? extends org.osid.course.chronicle.CredentialEntry> credentialEntries,
                                                  org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putCredentialEntries(credentialEntries);
        return;
    }
}
