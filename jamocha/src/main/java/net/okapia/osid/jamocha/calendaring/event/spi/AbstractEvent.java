//
// AbstractEvent.java
//
//     Defines an Event.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.event.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Event</code>.
 */

public abstract class AbstractEvent
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.calendaring.Event {

    private org.osid.calendaring.Duration duration;
    private org.osid.locale.DisplayText locationDescription;
    private org.osid.mapping.Location location;

    private boolean implicit    = false;
    private boolean recurring   = false;
    private boolean superseding = false;
    private boolean offset      = false;
    private boolean hasSponsors = false;

    private final java.util.Collection<org.osid.resource.Resource> sponsors = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.records.EventRecord> records = new java.util.LinkedHashSet<>();

    private final Containable containable = new Containable();


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.containable.isSequestered());
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    protected void setSequestered(boolean sequestered) {
        this.containable.setSequestered(sequestered);
        return;
    }


    /**
     *  Tests if this event is implicit. An implicit event is one that
     *  is generated from an offset or recurring series.
     *
     *  @return <code> true </code> if this event is implicit, <code> false 
     *          </code> if explicitly defined 
     */

    @OSID @Override
    public boolean isImplicit() {
        return (this.implicit);
    }


    /**
     *  Sets the implicit flag.
     *
     *  @param implicit <code> true </code> if this event is implicit,
     *         <code> false </code> if explicitly defined
     */

    protected void setImplicit(boolean implicit) {
        this.implicit = implicit;
        return;
    }


    /**
     *  Tests if this event is an implclit event part of a recurring
     *  event. If true, <code> isImplicit() </code> must also be true.
     *
     *  @return <code> true </code> if this event is part of a recurring 
     *          series, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isInRecurringSeries() {
        return (this.recurring);
    }


    /**
     *  Sets the in recurring series flag.
     *
     *  @param recurring <code> true </code> if this event is part of
     *         a recurring series, <code> false </code> otherwise
     */

    protected void setInRecurringSeries(boolean recurring) {
        this.recurring = recurring;
        setImplicit(true);
        return;
    }


    /**
     *  Tests if this event is a superseding event. If true, <code>
     *  isImplicit() </code> must also be true.
     *
     *  @return <code> true </code> if this event is superseding event, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean isSupersedingEvent() {
        return (this.superseding);
    }


    /**
     *  Sets the superseding event flag.
     *
     *  @param superseding <code> true </code> if this event is
     *         superseding event, <code> false </code> otherwise
     */

    protected void setSupersedingEvent(boolean superseding) {
        this.superseding = superseding;
        setImplicit(true);
        return;
    }


    /**
     *  Tests if this event is an event calculated from an offset. If
     *  true, <code> isImplicit() </code> must also be true.
     *
     *  @return <code> true </code> if this event is an offset event,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isOffsetEvent() {
        return (this.offset);
    }


    /**
     *  Sets the offset event flag.
     *
     *  @param offset <code> true </code> if this event is an offset
     *         event, <code> false </code> otherwise
     */

    protected void setOffsetEvent(boolean offset) {
        this.offset = offset;
        setImplicit(true);
        return;
    }


    /**
     *  Gets the duration of the event. 
     *
     *  @param  units the units of the duration 
     *  @return the duration 
     *  @throws org.osid.NullArgumentException <code> units </code> is
     *          <code> null </code>
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDuration(org.osid.calendaring.DateTimeResolution units) {
        return (this.duration);
    }


    /**
     *  Sets the duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    protected void setDuration(org.osid.calendaring.Duration duration) {
        nullarg(duration, "duration");
        this.duration = duration;
        return;
    }


    /**
     *  Gets a descriptive location. 
     *
     *  @return the location 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getLocationDescription() {
        return (this.locationDescription);
    }


    /**
     *  Sets the location description.
     *
     *  @param location a location description
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    protected void setLocationDescription(org.osid.locale.DisplayText location) {
        nullarg(location, "location description");
        this.locationDescription = location;
        return;
    }


    /**
     *  Tests if a location is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated location, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasLocation() {
        return (this.location != null);
    }


    /**
     *  Gets the location <code> Id </code>. 
     *
     *  @return a location <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLocation()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getLocationId() {
        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location.getId());
    }


    /**
     *  Gets the <code> Location. </code> 
     *
     *  @return a location 
     *  @throws org.osid.IllegalStateException <code> hasLocation()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.Location getLocation()
        throws org.osid.OperationFailedException {

        if (!hasLocation()) {
            throw new org.osid.IllegalStateException("hasLocation() is false");
        }

        return (this.location);
    }


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    protected void setLocation(org.osid.mapping.Location location) {
        nullarg(location, "location");
        this.location = location;
        return;
    }


    /**
     *  Tests if a sponsor is associated with this event. 
     *
     *  @return <code> true </code> if there is an associated sponsor. <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.hasSponsors);
    }


    /**
     *  Gets the <code> Id </code> of the event sponsors. 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        try {
            org.osid.resource.ResourceList sponsors = getSponsors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(sponsors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> Sponsors. </code> 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors()
     *          </code> is <code> false </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.sponsors));
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    protected void addSponsor(org.osid.resource.Resource sponsor) {
        nullarg(sponsor, "sponsors");

        this.sponsors.add(sponsor);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    protected void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        nullarg(sponsors, "sponsors");

        this.sponsors.clear();
        this.sponsors.addAll(sponsors);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Tests if this event supports the given record
     *  <code>Type</code>.
     *
     *  @param  eventRecordType an event record type 
     *  @return <code>true</code> if the eventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type eventRecordType) {
        for (org.osid.calendaring.records.EventRecord record : this.records) {
            if (record.implementsRecordType(eventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Event</code>
     *  record <code>Type</code>.
     *
     *  @param  eventRecordType the event record type 
     *  @return the event record 
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(eventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.EventRecord getEventRecord(org.osid.type.Type eventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.EventRecord record : this.records) {
            if (record.implementsRecordType(eventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(eventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this event. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param eventRecord the event record
     *  @param eventRecordType event record type
     *  @throws org.osid.NullArgumentException
     *          <code>eventRecord</code> or
     *          <code>eventRecordType</code> is <code>null</code>
     */
            
    protected void addEventRecord(org.osid.calendaring.records.EventRecord eventRecord, 
                                  org.osid.type.Type eventRecordType) {

        nullarg(eventRecord, "event record");
        addRecordType(eventRecordType);
        this.records.add(eventRecord);
        
        return;
    }


    protected class Containable
        extends net.okapia.osid.jamocha.spi.AbstractContainable
        implements org.osid.Containable {


        /**
         *  Sets the sequestered flag.
         *
         *  @param sequestered <code> true </code> if this containable is
         *         sequestered, <code> false </code> if this containable
         *         may appear outside its aggregate
         */

        @Override
        protected void setSequestered(boolean sequestered) {
            super.setSequestered(sequestered);
            return;
        }
    }
}
