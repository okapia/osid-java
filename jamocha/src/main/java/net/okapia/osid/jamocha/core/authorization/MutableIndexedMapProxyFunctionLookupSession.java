//
// MutableIndexedMapProxyFunctionLookupSession
//
//    Implements a Function lookup service backed by a collection of
//    functions indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements a Function lookup service backed by a collection of
 *  functions. The functions are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some functions may be compatible
 *  with more types than are indicated through these function
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of functions can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyFunctionLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractIndexedMapFunctionLookupSession
    implements org.osid.authorization.FunctionLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyFunctionLookupSession} with
     *  no function.
     *
     *  @param vault the vault
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyFunctionLookupSession(org.osid.authorization.Vault vault,
                                                       org.osid.proxy.Proxy proxy) {
        setVault(vault);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyFunctionLookupSession} with
     *  a single function.
     *
     *  @param vault the vault
     *  @param  function an function
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code function}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyFunctionLookupSession(org.osid.authorization.Vault vault,
                                                       org.osid.authorization.Function function, org.osid.proxy.Proxy proxy) {

        this(vault, proxy);
        putFunction(function);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyFunctionLookupSession} using
     *  an array of functions.
     *
     *  @param vault the vault
     *  @param  functions an array of functions
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code functions}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyFunctionLookupSession(org.osid.authorization.Vault vault,
                                                       org.osid.authorization.Function[] functions, org.osid.proxy.Proxy proxy) {

        this(vault, proxy);
        putFunctions(functions);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyFunctionLookupSession} using
     *  a collection of functions.
     *
     *  @param vault the vault
     *  @param  functions a collection of functions
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code functions}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyFunctionLookupSession(org.osid.authorization.Vault vault,
                                                       java.util.Collection<? extends org.osid.authorization.Function> functions,
                                                       org.osid.proxy.Proxy proxy) {
        this(vault, proxy);
        putFunctions(functions);
        return;
    }

    
    /**
     *  Makes a {@code Function} available in this session.
     *
     *  @param  function a function
     *  @throws org.osid.NullArgumentException {@code function{@code 
     *          is {@code null}
     */

    @Override
    public void putFunction(org.osid.authorization.Function function) {
        super.putFunction(function);
        return;
    }


    /**
     *  Makes an array of functions available in this session.
     *
     *  @param  functions an array of functions
     *  @throws org.osid.NullArgumentException {@code functions{@code 
     *          is {@code null}
     */

    @Override
    public void putFunctions(org.osid.authorization.Function[] functions) {
        super.putFunctions(functions);
        return;
    }


    /**
     *  Makes collection of functions available in this session.
     *
     *  @param  functions a collection of functions
     *  @throws org.osid.NullArgumentException {@code function{@code 
     *          is {@code null}
     */

    @Override
    public void putFunctions(java.util.Collection<? extends org.osid.authorization.Function> functions) {
        super.putFunctions(functions);
        return;
    }


    /**
     *  Removes a Function from this session.
     *
     *  @param functionId the {@code Id} of the function
     *  @throws org.osid.NullArgumentException {@code functionId{@code  is
     *          {@code null}
     */

    @Override
    public void removeFunction(org.osid.id.Id functionId) {
        super.removeFunction(functionId);
        return;
    }    
}
