//
// MutableIndexedMapProxyHierarchyLookupSession
//
//    Implements a Hierarchy lookup service backed by a collection of
//    hierarchies indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.hierarchy;


/**
 *  Implements a Hierarchy lookup service backed by a collection of
 *  hierarchies. The hierarchies are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some hierarchies may be compatible
 *  with more types than are indicated through these hierarchy
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of hierarchies can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyHierarchyLookupSession
    extends net.okapia.osid.jamocha.core.hierarchy.spi.AbstractIndexedMapHierarchyLookupSession
    implements org.osid.hierarchy.HierarchyLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyHierarchyLookupSession} with
     *  no hierarchy.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableIndexedMapProxyHierarchyLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyHierarchyLookupSession} with
     *  a single hierarchy.
     *
     *  @param  hierarchy an hierarchy
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code hierarchy} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyHierarchyLookupSession(org.osid.hierarchy.Hierarchy hierarchy, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyHierarchyLookupSession} using
     *  an array of hierarchies.
     *
     *  @param  hierarchies an array of hierarchies
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code hierarchies} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyHierarchyLookupSession(org.osid.hierarchy.Hierarchy[] hierarchies, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putHierarchies(hierarchies);
        return;
    }


    /**
     *  Constructs a new {@code MutableIndexedMapProxyHierarchyLookupSession} using
     *  a collection of hierarchies.
     *
     *  @param  hierarchies a collection of hierarchies
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code hierarchies} or
     *          {@code proxy} is {@code null}
     */

    public MutableIndexedMapProxyHierarchyLookupSession(java.util.Collection<? extends org.osid.hierarchy.Hierarchy> hierarchies,
                                                       org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putHierarchies(hierarchies);
        return;
    }

    
    /**
     *  Makes a {@code Hierarchy} available in this session.
     *
     *  @param  hierarchy a hierarchy
     *  @throws org.osid.NullArgumentException {@code hierarchy{@code 
     *          is {@code null}
     */

    @Override
    public void putHierarchy(org.osid.hierarchy.Hierarchy hierarchy) {
        super.putHierarchy(hierarchy);
        return;
    }


    /**
     *  Makes an array of hierarchies available in this session.
     *
     *  @param  hierarchies an array of hierarchies
     *  @throws org.osid.NullArgumentException {@code hierarchies{@code 
     *          is {@code null}
     */

    @Override
    public void putHierarchies(org.osid.hierarchy.Hierarchy[] hierarchies) {
        super.putHierarchies(hierarchies);
        return;
    }


    /**
     *  Makes collection of hierarchies available in this session.
     *
     *  @param  hierarchies a collection of hierarchies
     *  @throws org.osid.NullArgumentException {@code hierarchy{@code 
     *          is {@code null}
     */

    @Override
    public void putHierarchies(java.util.Collection<? extends org.osid.hierarchy.Hierarchy> hierarchies) {
        super.putHierarchies(hierarchies);
        return;
    }


    /**
     *  Removes a Hierarchy from this session.
     *
     *  @param hierarchyId the {@code Id} of the hierarchy
     *  @throws org.osid.NullArgumentException {@code hierarchyId{@code  is
     *          {@code null}
     */

    @Override
    public void removeHierarchy(org.osid.id.Id hierarchyId) {
        super.removeHierarchy(hierarchyId);
        return;
    }    
}
