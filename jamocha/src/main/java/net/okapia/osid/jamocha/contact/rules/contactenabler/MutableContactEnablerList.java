//
// MutableContactEnablerList.java
//
//     Implements a ContactEnablerList. This list allows ContactEnablers to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.rules.contactenabler;


/**
 *  <p>Implements a ContactEnablerList. This list allows ContactEnablers to be
 *  added after this contactEnabler has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this contactEnabler must
 *  invoke <code>eol()</code> when there are no more contactEnablers to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>ContactEnablerList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more contactEnablers are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more contactEnablers to be added.</p>
 */

public final class MutableContactEnablerList
    extends net.okapia.osid.jamocha.contact.rules.contactenabler.spi.AbstractMutableContactEnablerList
    implements org.osid.contact.rules.ContactEnablerList {


    /**
     *  Creates a new empty <code>MutableContactEnablerList</code>.
     */

    public MutableContactEnablerList() {
        super();
    }


    /**
     *  Creates a new <code>MutableContactEnablerList</code>.
     *
     *  @param contactEnabler a <code>ContactEnabler</code>
     *  @throws org.osid.NullArgumentException <code>contactEnabler</code>
     *          is <code>null</code>
     */

    public MutableContactEnablerList(org.osid.contact.rules.ContactEnabler contactEnabler) {
        super(contactEnabler);
        return;
    }


    /**
     *  Creates a new <code>MutableContactEnablerList</code>.
     *
     *  @param array an array of contactenablers
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableContactEnablerList(org.osid.contact.rules.ContactEnabler[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableContactEnablerList</code>.
     *
     *  @param collection a java.util.Collection of contactenablers
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableContactEnablerList(java.util.Collection<org.osid.contact.rules.ContactEnabler> collection) {
        super(collection);
        return;
    }
}
