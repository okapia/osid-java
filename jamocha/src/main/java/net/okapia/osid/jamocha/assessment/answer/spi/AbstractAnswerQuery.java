//
// AbstractAnswerQuery.java
//
//     A template for making an Answer Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.answer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for answers.
 */

public abstract class AbstractAnswerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.assessment.AnswerQuery {

    private final java.util.Collection<org.osid.assessment.records.AnswerQueryRecord> records = new java.util.ArrayList<>();

    

    /**
     *  Gets the record corresponding to the given answer query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an answer implementing the requested record.
     *
     *  @param answerRecordType an answer record type
     *  @return the answer query record
     *  @throws org.osid.NullArgumentException
     *          <code>answerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(answerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AnswerQueryRecord getAnswerQueryRecord(org.osid.type.Type answerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.assessment.records.AnswerQueryRecord record : this.records) {
            if (record.implementsRecordType(answerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(answerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this answer query. 
     *
     *  @param answerQueryRecord answer query record
     *  @param answerRecordType answer record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAnswerQueryRecord(org.osid.assessment.records.AnswerQueryRecord answerQueryRecord, 
                                          org.osid.type.Type answerRecordType) {

        addRecordType(answerRecordType);
        nullarg(answerQueryRecord, "answer query record");
        this.records.add(answerQueryRecord);        
        return;
    }
}
