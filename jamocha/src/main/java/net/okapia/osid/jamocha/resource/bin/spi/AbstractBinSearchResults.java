//
// AbstractBinSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.bin.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBinSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resource.BinSearchResults {

    private org.osid.resource.BinList bins;
    private final org.osid.resource.BinQueryInspector inspector;
    private final java.util.Collection<org.osid.resource.records.BinSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBinSearchResults.
     *
     *  @param bins the result set
     *  @param binQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>bins</code>
     *          or <code>binQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBinSearchResults(org.osid.resource.BinList bins,
                                            org.osid.resource.BinQueryInspector binQueryInspector) {
        nullarg(bins, "bins");
        nullarg(binQueryInspector, "bin query inspectpr");

        this.bins = bins;
        this.inspector = binQueryInspector;

        return;
    }


    /**
     *  Gets the bin list resulting from a search.
     *
     *  @return a bin list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resource.BinList getBins() {
        if (this.bins == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resource.BinList bins = this.bins;
        this.bins = null;
	return (bins);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resource.BinQueryInspector getBinQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  bin search record <code> Type. </code> This method must
     *  be used to retrieve a bin implementing the requested
     *  record.
     *
     *  @param binSearchRecordType a bin search 
     *         record type 
     *  @return the bin search
     *  @throws org.osid.NullArgumentException
     *          <code>binSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(binSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.BinSearchResultsRecord getBinSearchResultsRecord(org.osid.type.Type binSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resource.records.BinSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(binSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(binSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record bin search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBinRecord(org.osid.resource.records.BinSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "bin record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
