//
// AbstractImmutablePost.java
//
//     Wraps a mutable Post to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.financials.posting.post.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Post</code> to hide modifiers. This
 *  wrapper provides an immutized Post from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying post whose state changes are visible.
 */

public abstract class AbstractImmutablePost
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.financials.posting.Post {

    private final org.osid.financials.posting.Post post;


    /**
     *  Constructs a new <code>AbstractImmutablePost</code>.
     *
     *  @param post the post to immutablize
     *  @throws org.osid.NullArgumentException <code>post</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePost(org.osid.financials.posting.Post post) {
        super(post);
        this.post = post;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> FiscalPeriod. </code> 
     *
     *  @return the <code> FiscalPeriod </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getFiscalPeriodId() {
        return (this.post.getFiscalPeriodId());
    }


    /**
     *  Gets the <code> FiscalPeriod. </code> 
     *
     *  @return the fiscal period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.FiscalPeriod getFiscalPeriod()
        throws org.osid.OperationFailedException {

        return (this.post.getFiscalPeriod());
    }


    /**
     *  Tests if this has been posted. 
     *
     *  @return <code> true </code> if this has been posted, <code> false 
     *          </code> if just lying around 
     */

    @OSID @Override
    public boolean isPosted() {
        return (this.post.isPosted());
    }


    /**
     *  Gets the posting date. 
     *
     *  @return the posting date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.post.getDate());
    }


    /**
     *  Gets the <code> Ids </code> of the <code> PostEntries. </code> 
     *
     *  @return the <code> PostEntry </code> <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getPostEntryIds() {
        return (this.post.getPostEntryIds());
    }


    /**
     *  Gets the <code> PostEntries. </code> 
     *
     *  @return the post entries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.posting.PostEntryList getPostEntries()
        throws org.osid.OperationFailedException {

        return (this.post.getPostEntries());
    }


    /**
     *  Tests if this <code> Post </code> is a correction to a previous post. 
     *
     *  @return <code> true </code> if this post is a correction, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isCorrection() {
        return (this.post.isCorrection());
    }


    /**
     *  Gets the <code> Id </code> of the corrected <code> Post. </code> 
     *
     *  @return the corrected <code> Post </code> <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isCorrection() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCorrectedPostId() {
        return (this.post.getCorrectedPostId());
    }


    /**
     *  Gets the corrected <code> Post. </code> 
     *
     *  @return the corrected post 
     *  @throws org.osid.IllegalStateException <code> isCorrection() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.financials.posting.Post getCorrectedPost()
        throws org.osid.OperationFailedException {

        return (this.post.getCorrectedPost());
    }


    /**
     *  Gets the post record corresponding to the given <code> Post </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> postRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(postRecordType) </code> is <code> true </code> . 
     *
     *  @param  postRecordType the type of post record to retrieve 
     *  @return the post record 
     *  @throws org.osid.NullArgumentException <code> postRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(postRecordType) </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.posting.records.PostRecord getPostRecord(org.osid.type.Type postRecordType)
        throws org.osid.OperationFailedException {

        return (this.post.getPostRecord(postRecordType));
    }
}

