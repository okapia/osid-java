//
// AbstractQuerySyllabusLookupSession.java
//
//    An inline adapter that maps a SyllabusLookupSession to
//    a SyllabusQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.syllabus.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a SyllabusLookupSession to
 *  a SyllabusQuerySession.
 */

public abstract class AbstractQuerySyllabusLookupSession
    extends net.okapia.osid.jamocha.course.syllabus.spi.AbstractSyllabusLookupSession
    implements org.osid.course.syllabus.SyllabusLookupSession {

    private final org.osid.course.syllabus.SyllabusQuerySession session;
    

    /**
     *  Constructs a new AbstractQuerySyllabusLookupSession.
     *
     *  @param querySession the underlying syllabus query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQuerySyllabusLookupSession(org.osid.course.syllabus.SyllabusQuerySession querySession) {
        nullarg(querySession, "syllabus query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>CourseCatalog</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform <code>Syllabus</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupSyllabi() {
        return (this.session.canSearchSyllabi());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include syllabi in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    
     
    /**
     *  Gets the <code>Syllabus</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Syllabus</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Syllabus</code> and
     *  retained for compatibility.
     *
     *  @param  syllabusId <code>Id</code> of the
     *          <code>Syllabus</code>
     *  @return the syllabus
     *  @throws org.osid.NotFoundException <code>syllabusId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>syllabusId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.Syllabus getSyllabus(org.osid.id.Id syllabusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.SyllabusQuery query = getQuery();
        query.matchId(syllabusId, true);
        org.osid.course.syllabus.SyllabusList syllabi = this.session.getSyllabiByQuery(query);
        if (syllabi.hasNext()) {
            return (syllabi.getNextSyllabus());
        } 
        
        throw new org.osid.NotFoundException(syllabusId + " not found");
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  syllabi specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Syllabi</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  syllabusIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByIds(org.osid.id.IdList syllabusIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.SyllabusQuery query = getQuery();

        try (org.osid.id.IdList ids = syllabusIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getSyllabiByQuery(query));
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  syllabus genus <code>Type</code> which does not include
     *  syllabi of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  syllabusGenusType a syllabus genus type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByGenusType(org.osid.type.Type syllabusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.SyllabusQuery query = getQuery();
        query.matchGenusType(syllabusGenusType, true);
        return (this.session.getSyllabiByQuery(query));
    }


    /**
     *  Gets a <code>SyllabusList</code> corresponding to the given
     *  syllabus genus <code>Type</code> and include any additional
     *  syllabi with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  syllabusGenusType a syllabus genus type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByParentGenusType(org.osid.type.Type syllabusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.SyllabusQuery query = getQuery();
        query.matchParentGenusType(syllabusGenusType, true);
        return (this.session.getSyllabiByQuery(query));
    }


    /**
     *  Gets a <code>SyllabusList</code> containing the given
     *  syllabus record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  syllabusRecordType a syllabus record type 
     *  @return the returned <code>Syllabus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>syllabusRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiByRecordType(org.osid.type.Type syllabusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.SyllabusQuery query = getQuery();
        query.matchRecordType(syllabusRecordType, true);
        return (this.session.getSyllabiByQuery(query));
    }

    
    /**
     *  Gets a <code> SyllabusList </code> for the given course <code> . 
     *  </code> In plenary mode, the returned list contains all known syllabi 
     *  or an error results. Otherwise, the returned list may contain only 
     *  those syllabi that are accessible through this session. 
     *
     *  @param  courseId a course <code> Id </code> 
     *  @return the returned <code> Syllabus </code> list 
     *  @throws org.osid.NullArgumentException <code> courseId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabiForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.SyllabusQuery query = getQuery();
        query.matchCourseId(courseId, true);
        return (this.session.getSyllabiByQuery(query));
    }


    /**
     *  Gets all <code>Syllabi</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  syllabi or an error results. Otherwise, the returned list
     *  may contain only those syllabi that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Syllabi</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.syllabus.SyllabusList getSyllabi()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.syllabus.SyllabusQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getSyllabiByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.syllabus.SyllabusQuery getQuery() {
        org.osid.course.syllabus.SyllabusQuery query = this.session.getSyllabusQuery();
        return (query);
    }
}
