//
// AbstractResourceDemographicManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.demographic.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractResourceDemographicManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.resource.demographic.ResourceDemographicManager,
               org.osid.resource.demographic.ResourceDemographicProxyManager {

    private final Types demographicRecordTypes             = new TypeRefSet();
    private final Types demographicSearchRecordTypes       = new TypeRefSet();

    private final Types demographicEnablerRecordTypes      = new TypeRefSet();
    private final Types demographicEnablerSearchRecordTypes= new TypeRefSet();


    /**
     *  Constructs a new
     *  <code>AbstractResourceDemographicManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractResourceDemographicManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any broker federation is exposed. Federation is exposed when 
     *  a specific broker may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  brokers appears as a single broker. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if demographic is supported. 
     *
     *  @return <code> true </code> if demographic query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographics() {
        return (false);
    }


    /**
     *  Tests if looking up demographic is supported. 
     *
     *  @return <code> true </code> if demographic lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicLookup() {
        return (false);
    }


    /**
     *  Tests if querying demographic is supported. 
     *
     *  @return <code> true </code> if demographic query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicQuery() {
        return (false);
    }


    /**
     *  Tests if searching demographic is supported. 
     *
     *  @return <code> true </code> if demographic search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicSearch() {
        return (false);
    }


    /**
     *  Tests if a demographic administrative service is supported. 
     *
     *  @return <code> true </code> if demographic administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicAdmin() {
        return (false);
    }


    /**
     *  Tests if a demographic builder service is supported. 
     *
     *  @return <code> true </code> if demographic builder service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicBuilder() {
        return (false);
    }


    /**
     *  Tests if a demographic notification service is supported. 
     *
     *  @return <code> true </code> if demographic notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicNotification() {
        return (false);
    }


    /**
     *  Tests if a demographic bin lookup service is supported. 
     *
     *  @return <code> true </code> if a demographic bin lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicBin() {
        return (false);
    }


    /**
     *  Tests if a demographic bin service is supported. 
     *
     *  @return <code> true </code> if demographic bin assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicBinAssignment() {
        return (false);
    }


    /**
     *  Tests if a demographic bin lookup service is supported. 
     *
     *  @return <code> true </code> if a demographic bin service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicSmartBin() {
        return (false);
    }


    /**
     *  Tests if looking up demographic enablers is supported. 
     *
     *  @return <code> true </code> if demographic enabler lookup is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerLookup() {
        return (false);
    }


    /**
     *  Tests if querying demographic enablers is supported. 
     *
     *  @return <code> true </code> if demographic enabler query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerQuery() {
        return (false);
    }


    /**
     *  Tests if searching demographic enablers is supported. 
     *
     *  @return <code> true </code> if demographic enabler search is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerSearch() {
        return (false);
    }


    /**
     *  Tests if a demographic enabler administrative service is supported. 
     *
     *  @return <code> true </code> if demographic enabler administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerAdmin() {
        return (false);
    }


    /**
     *  Tests if a demographic enabler notification service is supported. 
     *
     *  @return <code> true </code> if demographic enabler notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerNotification() {
        return (false);
    }


    /**
     *  Tests if a demographic enabler bin lookup service is supported. 
     *
     *  @return <code> true </code> if a demographic enabler bin lookup 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerBin() {
        return (false);
    }


    /**
     *  Tests if a demographic enabler bin service is supported. 
     *
     *  @return <code> true </code> if demographic enabler bin assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerBinAssignment() {
        return (false);
    }


    /**
     *  Tests if a demographic enabler bin lookup service is supported. 
     *
     *  @return <code> true </code> if a demographic enabler bin service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerSmartBin() {
        return (false);
    }


    /**
     *  Tests if a demographic enabler rule lookup service is supported. 
     *
     *  @return <code> true </code> if a processor enabler rule lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerRuleLookup() {
        return (false);
    }


    /**
     *  Tests if a demographic enabler rule application service is supported. 
     *
     *  @return <code> true </code> if demographic enabler rule application 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerRuleApplication() {
        return (false);
    }


    /**
     *  Gets the supported <code> Demographic </code> record types. 
     *
     *  @return a list containing the supported <code> Demographic </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDemographicRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.demographicRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Demographic </code> record type is 
     *  supported. 
     *
     *  @param  demographicRecordType a <code> Type </code> indicating a 
     *          <code> Demographic </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> demographicRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDemographicRecordType(org.osid.type.Type demographicRecordType) {
        return (this.demographicRecordTypes.contains(demographicRecordType));
    }


    /**
     *  Adds support for a demographic record type.
     *
     *  @param demographicRecordType a demographic record type
     *  @throws org.osid.NullArgumentException
     *  <code>demographicRecordType</code> is <code>null</code>
     */

    protected void addDemographicRecordType(org.osid.type.Type demographicRecordType) {
        this.demographicRecordTypes.add(demographicRecordType);
        return;
    }


    /**
     *  Removes support for a demographic record type.
     *
     *  @param demographicRecordType a demographic record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>demographicRecordType</code> is <code>null</code>
     */

    protected void removeDemographicRecordType(org.osid.type.Type demographicRecordType) {
        this.demographicRecordTypes.remove(demographicRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Demographic </code> search record types. 
     *
     *  @return a list containing the supported <code> Demographic </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDemographicSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.demographicSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Demographic </code> search record type is 
     *  supported. 
     *
     *  @param  demographicSearchRecordType a <code> Type </code> indicating a 
     *          <code> Demographic </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDemographicSearchRecordType(org.osid.type.Type demographicSearchRecordType) {
        return (this.demographicSearchRecordTypes.contains(demographicSearchRecordType));
    }


    /**
     *  Adds support for a demographic search record type.
     *
     *  @param demographicSearchRecordType a demographic search record type
     *  @throws org.osid.NullArgumentException
     *  <code>demographicSearchRecordType</code> is <code>null</code>
     */

    protected void addDemographicSearchRecordType(org.osid.type.Type demographicSearchRecordType) {
        this.demographicSearchRecordTypes.add(demographicSearchRecordType);
        return;
    }


    /**
     *  Removes support for a demographic search record type.
     *
     *  @param demographicSearchRecordType a demographic search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>demographicSearchRecordType</code> is <code>null</code>
     */

    protected void removeDemographicSearchRecordType(org.osid.type.Type demographicSearchRecordType) {
        this.demographicSearchRecordTypes.remove(demographicSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> DemographicEnabler </code> record types. 
     *
     *  @return a list containing the supported <code> DemographicEnabler 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDemographicEnablerRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.demographicEnablerRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> DemographicEnabler </code> record type is 
     *  supported. 
     *
     *  @param  demographicEnablerRecordType a <code> Type </code> indicating 
     *          a <code> DemographicEnabler </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerRecordType(org.osid.type.Type demographicEnablerRecordType) {
        return (this.demographicEnablerRecordTypes.contains(demographicEnablerRecordType));
    }


    /**
     *  Adds support for a demographic enabler record type.
     *
     *  @param demographicEnablerRecordType a demographic enabler record type
     *  @throws org.osid.NullArgumentException
     *  <code>demographicEnablerRecordType</code> is <code>null</code>
     */

    protected void addDemographicEnablerRecordType(org.osid.type.Type demographicEnablerRecordType) {
        this.demographicEnablerRecordTypes.add(demographicEnablerRecordType);
        return;
    }


    /**
     *  Removes support for a demographic enabler record type.
     *
     *  @param demographicEnablerRecordType a demographic enabler record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>demographicEnablerRecordType</code> is <code>null</code>
     */

    protected void removeDemographicEnablerRecordType(org.osid.type.Type demographicEnablerRecordType) {
        this.demographicEnablerRecordTypes.remove(demographicEnablerRecordType);
        return;
    }


    /**
     *  Gets the supported <code> DemographicEnabler </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> DemographicEnabler 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDemographicEnablerSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.demographicEnablerSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> DemographicEnabler </code> search record 
     *  type is supported. 
     *
     *  @param  demographicEnablerSearchRecordType a <code> Type </code> 
     *          indicating a <code> DemographicEnabler </code> search record 
     *          type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsDemographicEnablerSearchRecordType(org.osid.type.Type demographicEnablerSearchRecordType) {
        return (this.demographicEnablerSearchRecordTypes.contains(demographicEnablerSearchRecordType));
    }


    /**
     *  Adds support for a demographic enabler search record type.
     *
     *  @param demographicEnablerSearchRecordType a demographic enabler search record type
     *  @throws org.osid.NullArgumentException
     *  <code>demographicEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void addDemographicEnablerSearchRecordType(org.osid.type.Type demographicEnablerSearchRecordType) {
        this.demographicEnablerSearchRecordTypes.add(demographicEnablerSearchRecordType);
        return;
    }


    /**
     *  Removes support for a demographic enabler search record type.
     *
     *  @param demographicEnablerSearchRecordType a demographic enabler search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>demographicEnablerSearchRecordType</code> is <code>null</code>
     */

    protected void removeDemographicEnablerSearchRecordType(org.osid.type.Type demographicEnablerSearchRecordType) {
        this.demographicEnablerSearchRecordTypes.remove(demographicEnablerSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  lookup service. 
     *
     *  @return a <code> DemographicLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicLookupSession getDemographicLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicLookupSession getDemographicLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicLookupSession getDemographicLookupSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicLookupSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicLookupSession getDemographicLookupSessionForBin(org.osid.id.Id binId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicLookupSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  query service. 
     *
     *  @return a <code> DemographicQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuerySession getDemographicQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuerySession getDemographicQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  query service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuerySession getDemographicQuerySessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicQuerySessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  query service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicQuerySession getDemographicQuerySessionForBin(org.osid.id.Id binId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicQuerySessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  search service. 
     *
     *  @return a <code> DemographicSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicSearchSession getDemographicSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicSearchSession getDemographicSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  earch service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicSearchSession getDemographicSearchSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicSearchSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  earch service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicSearchSession getDemographicSearchSessionForBin(org.osid.id.Id binId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicSearchSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  administration service. 
     *
     *  @return a <code> DemographicAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicAdminSession getDemographicAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicAdminSession getDemographicAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicAdminSession getDemographicAdminSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicAdminSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicAdminSession getDemographicAdminSessionForBin(org.osid.id.Id binId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicAdminSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  builder service. 
     *
     *  @return a <code> DemographicBuilderSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBuilder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBuilderSession getDemographicBuilderSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicBuilderSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  builder service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicBuilderSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBuilder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBuilderSession getDemographicBuilderSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicBuilderSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  builder service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicBuilderSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBuilder() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBuilderSession getDemographicBuilderSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicBuilderSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  builder service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicBuilderSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBuilder() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBuilderSession getDemographicBuilderSessionForBin(org.osid.id.Id binId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicBuilderSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  notification service. 
     *
     *  @param  demographicReceiver the notification callback 
     *  @return a <code> DemographicNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> demographicReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicNotificationSession getDemographicNotificationSession(org.osid.resource.demographic.DemographicReceiver demographicReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  notification service. 
     *
     *  @param  demographicReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> demographicReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicNotificationSession getDemographicNotificationSession(org.osid.resource.demographic.DemographicReceiver demographicReceiver, 
                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  notification service for the given bin. 
     *
     *  @param  demographicReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> demographicReceiver 
     *          </code> or <code> binId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicNotificationSession getDemographicNotificationSessionForBin(org.osid.resource.demographic.DemographicReceiver demographicReceiver, 
                                                                                                                org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicNotificationSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  notification service for the given bin. 
     *
     *  @param  demographicReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> demographicReceiver, 
     *          binId, </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicNotificationSession getDemographicNotificationSessionForBin(org.osid.resource.demographic.DemographicReceiver demographicReceiver, 
                                                                                                                org.osid.id.Id binId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicNotificationSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup demographic/bin mappings 
     *  for demographics. 
     *
     *  @return a <code> DemographicBinSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBinSession getDemographicBinSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicBinSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup demographic/bin mappings 
     *  for demographics. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicBinSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBinSession getDemographicBinSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicBinSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  demographic to bins. 
     *
     *  @return a <code> DemographicBinAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBinAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBinAssignmentSession getDemographicBinAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicBinAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  demographic to bins. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicBinAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicBinAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicBinAssignmentSession getDemographicBinAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicBinAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage demographic smart bins. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicSmartBinSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSmartBin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicSmartBinSession getDemographicSmartBinSession(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicSmartBinSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage demographic smart bins. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicSmartBinSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicSmartBin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicSmartBinSession getDemographicSmartBinSession(org.osid.id.Id binId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicSmartBinSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler lookup service. 
     *
     *  @return a <code> DemographicEnablerLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerLookupSession getDemographicEnablerLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerLookupSession getDemographicEnablerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerLookupSession getDemographicEnablerLookupSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerLookupSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerLookupSession getDemographicEnablerLookupSessionForBin(org.osid.id.Id binId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerLookupSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler query service. 
     *
     *  @return a <code> DemographicEnablerQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerQuerySession getDemographicEnablerQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerQuerySession getDemographicEnablerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler query service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerQuerySession getDemographicEnablerQuerySessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerQuerySessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler query service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerQuerySession getDemographicEnablerQuerySessionForBin(org.osid.id.Id binId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerQuerySessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler search service. 
     *
     *  @return a <code> DemographicEnablerSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerSearchSession getDemographicEnablerSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerSearchSession getDemographicEnablerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enablers earch service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerSearchSession getDemographicEnablerSearchSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerSearchSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enablers earch service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerSearchSession getDemographicEnablerSearchSessionForBin(org.osid.id.Id binId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerSearchSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler administration service. 
     *
     *  @return a <code> DemographicEnablerAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerAdminSession getDemographicEnablerAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerAdminSession getDemographicEnablerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerAdminSession getDemographicEnablerAdminSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerAdminSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler administration service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerAdminSession getDemographicEnablerAdminSessionForBin(org.osid.id.Id binId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerAdminSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler notification service. 
     *
     *  @param  demographicEnablerReceiver the notification callback 
     *  @return a <code> DemographicEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerReceiver </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerNotificationSession getDemographicEnablerNotificationSession(org.osid.resource.demographic.DemographicEnablerReceiver demographicEnablerReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler notification service. 
     *
     *  @param  demographicEnablerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerNotificationSession getDemographicEnablerNotificationSession(org.osid.resource.demographic.DemographicEnablerReceiver demographicEnablerReceiver, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler notification service for the given bin. 
     *
     *  @param  demographicEnablerReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerReceiver </code> or <code> binId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerNotificationSession getDemographicEnablerNotificationSessionForBin(org.osid.resource.demographic.DemographicEnablerReceiver demographicEnablerReceiver, 
                                                                                                                              org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerNotificationSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler notification service for the given bin. 
     *
     *  @param  demographicEnablerReceiver the notification callback 
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no bin found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          demographicEnablerReceiver, binId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerNotificationSession getDemographicEnablerNotificationSessionForBin(org.osid.resource.demographic.DemographicEnablerReceiver demographicEnablerReceiver, 
                                                                                                                              org.osid.id.Id binId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerNotificationSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup demographic enabler/bin 
     *  mappings for demographic enablers. 
     *
     *  @return a <code> DemographicEnablerBinSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerBinSession getDemographicEnablerBinSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerBinSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup demographic enabler/bin 
     *  mappings for demographic enablers. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerBinSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerBinSession getDemographicEnablerBinSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerBinSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  demographic enablers to bins. 
     *
     *  @return a <code> DemographicEnablerBinAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerBinAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerBinAssignmentSession getDemographicEnablerBinAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerBinAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  demographic enablers to bins. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerBinAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerBinAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerBinAssignmentSession getDemographicEnablerBinAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerBinAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage demographic enabler 
     *  smart bins. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerSmartBinSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerSmartBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerSmartBinSession getDemographicEnablerSmartBinSession(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerSmartBinSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage demographic enabler 
     *  smart bins. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerSmartBinSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerSmartBin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerSmartBinSession getDemographicEnablerSmartBinSession(org.osid.id.Id binId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerSmartBinSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler mapping lookup service. 
     *
     *  @return a <code> DemographicEnablerRuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleLookupSession getDemographicEnablerRuleLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler mapping lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerRuleLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleLookupSession getDemographicEnablerRuleLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerRuleLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler mapping lookup service. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleLookupSession getDemographicEnablerRuleLookupSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerRuleLookupSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler mapping lookup service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerRuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleLookupSession getDemographicEnablerRuleLookupSessionForBin(org.osid.id.Id binId, 
                                                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerRuleLookupSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler assignment service. 
     *
     *  @return a <code> DemographicEnablerRuleApplicationSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleApplicationSession getDemographicEnablerRuleApplicationSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleApplication() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleApplicationSession getDemographicEnablerRuleApplicationSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerRuleApplicationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler assignment service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @return a <code> DemographicEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleApplicationSession getDemographicEnablerRuleApplicationSessionForBin(org.osid.id.Id binId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicManager.getDemographicEnablerRuleApplicationSessionForBin not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the demographic 
     *  enabler assignment service for the given bin. 
     *
     *  @param  binId the <code> Id </code> of the <code> Bin </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DemographicEnablerRuleApplicationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Bin </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> binId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDemographicEnablerRuleApplication() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.demographic.DemographicEnablerRuleApplicationSession getDemographicEnablerRuleApplicationSessionForBin(org.osid.id.Id binId, 
                                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.resource.demographic.ResourceDemographicProxyManager.getDemographicEnablerRuleApplicationSessionForBin not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.demographicRecordTypes.clear();
        this.demographicRecordTypes.clear();

        this.demographicSearchRecordTypes.clear();
        this.demographicSearchRecordTypes.clear();

        this.demographicEnablerRecordTypes.clear();
        this.demographicEnablerRecordTypes.clear();

        this.demographicEnablerSearchRecordTypes.clear();
        this.demographicEnablerSearchRecordTypes.clear();

        return;
    }
}
