//
// AbstractAuctionProcessorEnablerLookupSession.java
//
//    A starter implementation framework for providing an AuctionProcessorEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an AuctionProcessorEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getAuctionProcessorEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractAuctionProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.bidding.rules.AuctionProcessorEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();
    

    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code>
     *          is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>AuctionProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionProcessorEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>AuctionProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionProcessorEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>AuctionProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionProcessorEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction processor enablers in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active auction processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionProcessorEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive auction processor enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionProcessorEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>AuctionProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionProcessorEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AuctionProcessorEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, auction processor enablers are returned that are currently
     *  active. In any status mode, active and inactive auction processor enablers
     *  are returned.
     *
     *  @param  auctionProcessorEnablerId <code>Id</code> of the
     *          <code>AuctionProcessorEnabler</code>
     *  @return the auction processor enabler
     *  @throws org.osid.NotFoundException <code>auctionProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnabler getAuctionProcessorEnabler(org.osid.id.Id auctionProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.bidding.rules.AuctionProcessorEnablerList auctionProcessorEnablers = getAuctionProcessorEnablers()) {
            while (auctionProcessorEnablers.hasNext()) {
                org.osid.bidding.rules.AuctionProcessorEnabler auctionProcessorEnabler = auctionProcessorEnablers.getNextAuctionProcessorEnabler();
                if (auctionProcessorEnabler.getId().equals(auctionProcessorEnablerId)) {
                    return (auctionProcessorEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(auctionProcessorEnablerId + " not found");
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionProcessorEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AuctionProcessorEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, auction processor enablers are returned that are currently
     *  active. In any status mode, active and inactive auction processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getAuctionProcessorEnablers()</code>.
     *
     *  @param  auctionProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByIds(org.osid.id.IdList auctionProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.bidding.rules.AuctionProcessorEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = auctionProcessorEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getAuctionProcessorEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("auction processor enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.LinkedAuctionProcessorEnablerList(ret));
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> corresponding to the given
     *  auction processor enabler genus <code>Type</code> which does not include
     *  auction processor enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  auction processor enablers or an error results. Otherwise, the returned list
     *  may contain only those auction processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processor enablers are returned that are currently
     *  active. In any status mode, active and inactive auction processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getAuctionProcessorEnablers()</code>.
     *
     *  @param  auctionProcessorEnablerGenusType an auctionProcessorEnabler genus type 
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByGenusType(org.osid.type.Type auctionProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionprocessorenabler.AuctionProcessorEnablerGenusFilterList(getAuctionProcessorEnablers(), auctionProcessorEnablerGenusType));
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> corresponding to the given
     *  auction processor enabler genus <code>Type</code> and include any additional
     *  auction processor enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  auction processor enablers or an error results. Otherwise, the returned list
     *  may contain only those auction processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processor enablers are returned that are currently
     *  active. In any status mode, active and inactive auction processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctionProcessorEnablers()</code>.
     *
     *  @param  auctionProcessorEnablerGenusType an auctionProcessorEnabler genus type 
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByParentGenusType(org.osid.type.Type auctionProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getAuctionProcessorEnablersByGenusType(auctionProcessorEnablerGenusType));
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> containing the given
     *  auction processor enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auction processor enablers or an error results. Otherwise, the returned list
     *  may contain only those auction processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, auction processor enablers are returned that are currently
     *  active. In any status mode, active and inactive auction processor enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getAuctionProcessorEnablers()</code>.
     *
     *  @param  auctionProcessorEnablerRecordType an auctionProcessorEnabler record type 
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByRecordType(org.osid.type.Type auctionProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionprocessorenabler.AuctionProcessorEnablerRecordFilterList(getAuctionProcessorEnablers(), auctionProcessorEnablerRecordType));
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  auction processor enablers or an error results. Otherwise, the returned list
     *  may contain only those auction processor enablers that are accessible
     *  through this session.
     *  
     *  In active mode, auction processor enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive auction processor enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AuctionProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionprocessorenabler.TemporalAuctionProcessorEnablerFilterList(getAuctionProcessorEnablers(), from, to));
    }
        

    /**
     *  Gets an <code>AuctionProcessorEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  auction processor enablers or an error results. Otherwise, the returned list
     *  may contain only those auction processor enablers that are accessible
     *  through this session.
     *
     *  In active mode, auction processor enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive auction processor enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getAuctionProcessorEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>AuctionProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction processor enablers or an error results. Otherwise, the returned list
     *  may contain only those auction processor enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processor enablers are returned that are currently
     *  active. In any status mode, active and inactive auction processor enablers
     *  are returned.
     *
     *  @return a list of <code>AuctionProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the auction processor enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of auction processor enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.bidding.rules.AuctionProcessorEnablerList filterAuctionProcessorEnablersOnViews(org.osid.bidding.rules.AuctionProcessorEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.bidding.rules.AuctionProcessorEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionprocessorenabler.ActiveAuctionProcessorEnablerFilterList(ret);
        }

        return (ret);
    }
}
