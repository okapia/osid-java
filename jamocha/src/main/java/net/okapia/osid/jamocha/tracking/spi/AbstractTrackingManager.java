//
// AbstractTrackingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTrackingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.tracking.TrackingManager,
               org.osid.tracking.TrackingProxyManager {

    private final Types issueRecordTypes                   = new TypeRefSet();
    private final Types issueSearchRecordTypes             = new TypeRefSet();

    private final Types logEntryRecordTypes                = new TypeRefSet();
    private final Types queueRecordTypes                   = new TypeRefSet();
    private final Types queueSearchRecordTypes             = new TypeRefSet();

    private final Types frontOfficeRecordTypes             = new TypeRefSet();
    private final Types frontOfficeSearchRecordTypes       = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractTrackingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTrackingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any front office federation is exposed. Federation is exposed 
     *  when a specific front office may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of front offices appears as a single front office. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if an issue service is supported for the current agent. 
     *
     *  @return <code> true </code> if my issue is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyIssue() {
        return (false);
    }


    /**
     *  Tests if an issue tracking service is supported. 
     *
     *  @return <code> true </code> if issue tracking is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueTracking() {
        return (false);
    }


    /**
     *  Tests if an issue resourcing service is supported. 
     *
     *  @return <code> true </code> if issue resourcing is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueResourcing() {
        return (false);
    }


    /**
     *  Tests if an issue triaging service is supported. 
     *
     *  @return <code> true </code> if issue triaging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueTriaging() {
        return (false);
    }


    /**
     *  Tests if looking up issues is supported. 
     *
     *  @return <code> true </code> if issue lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueLookup() {
        return (false);
    }


    /**
     *  Tests if querying issues is supported. 
     *
     *  @return <code> true </code> if issue query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Tests if searching issues is supported. 
     *
     *  @return <code> true </code> if issue search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSearch() {
        return (false);
    }


    /**
     *  Tests if issue <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if issue administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueAdmin() {
        return (false);
    }


    /**
     *  Tests if an issue <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if issue notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueNotification() {
        return (false);
    }


    /**
     *  Tests if an issue front office lookup service is supported. 
     *
     *  @return <code> true </code> if an issue front office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueFrontOffice() {
        return (false);
    }


    /**
     *  Tests if an issue front office assignment service is supported. 
     *
     *  @return <code> true </code> if an issue to front office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueFrontOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if an issue smart front office service is supported. 
     *
     *  @return <code> true </code> if an issue smart front office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSmartFrontOffice() {
        return (false);
    }


    /**
     *  Tests if looking up subtasks is supported. 
     *
     *  @return <code> true </code> if subtask lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubtaskIssueLookup() {
        return (false);
    }


    /**
     *  Tests if managing subtasks is supported. 
     *
     *  @return <code> true </code> if subtask admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubtaskIssueAdmin() {
        return (false);
    }


    /**
     *  Tests if looking up duplicate issues is supported. 
     *
     *  @return <code> true </code> if duplicate issue lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDuplicateIssueLookup() {
        return (false);
    }


    /**
     *  Tests if managing duplicate issues is supported. 
     *
     *  @return <code> true </code> if duplicate issue admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDuplicateIssueAdmin() {
        return (false);
    }


    /**
     *  Tests if looking up issue branches is supported. 
     *
     *  @return <code> true </code> if branched issue lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchedIssueLookup() {
        return (false);
    }


    /**
     *  Tests if managing issue branches is supported. 
     *
     *  @return <code> true </code> if branched issue admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchedIssueAdmin() {
        return (false);
    }


    /**
     *  Tests if looking up blocking issues is supported. 
     *
     *  @return <code> true </code> if blocking issue lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockingIssueLookup() {
        return (false);
    }


    /**
     *  Tests if managing issue blocks is supported. 
     *
     *  @return <code> true </code> if blocking issue admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockingIssueAdmin() {
        return (false);
    }


    /**
     *  Tests if looking up log entries is supported. 
     *
     *  @return <code> true </code> if log entry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryLookup() {
        return (false);
    }


    /**
     *  Tests if issue commenting and customer messaging is supported. 
     *
     *  @return <code> true </code> if issue messaging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueMessaging() {
        return (false);
    }


    /**
     *  Tests if log entry notification is supported. 
     *
     *  @return <code> true </code> if log entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryNotification() {
        return (false);
    }


    /**
     *  Tests if looking up queues is supported. 
     *
     *  @return <code> true </code> if queue lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueLookup() {
        return (false);
    }


    /**
     *  Tests if querying queues is supported. 
     *
     *  @return <code> true </code> if queue query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (false);
    }


    /**
     *  Tests if searching queues is supported. 
     *
     *  @return <code> true </code> if queue search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSearch() {
        return (false);
    }


    /**
     *  Tests if queue <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if queue administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueAdmin() {
        return (false);
    }


    /**
     *  Tests if a queue <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if queue notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueNotification() {
        return (false);
    }


    /**
     *  Tests if a queue front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue front office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue front office service is supported. 
     *
     *  @return <code> true </code> if queue to front office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueFrontOfficeAssignment() {
        return (false);
    }


    /**
     *  Tests if a queue smart front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue smart front office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSmartFrontOffice() {
        return (false);
    }


    /**
     *  Tests if a queue resourcing is supported. 
     *
     *  @return <code> true </code> if a queue resourcing service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueResourcing() {
        return (false);
    }


    /**
     *  Tests if looking up front offices is supported. 
     *
     *  @return <code> true </code> if front office lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeLookup() {
        return (false);
    }


    /**
     *  Tests if querying front offices is supported. 
     *
     *  @return <code> true </code> if a front office query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Tests if searching front offices is supported. 
     *
     *  @return <code> true </code> if front office search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeSearch() {
        return (false);
    }


    /**
     *  Tests if front office administrative service is supported. 
     *
     *  @return <code> true </code> if front office administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeAdmin() {
        return (false);
    }


    /**
     *  Tests if a front office <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if front office notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a front office hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if front office hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a front office hierarchy design service. 
     *
     *  @return <code> true </code> if front office hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a tracking rules service. 
     *
     *  @return <code> true </code> if a tracking rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTrackingRules() {
        return (false);
    }


    /**
     *  Gets the supported <code> Issue </code> record types. 
     *
     *  @return a list containing the supported <code> Issue </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIssueRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.issueRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Issue </code> record type is supported. 
     *
     *  @param  issueRecordType a <code> Type </code> indicating an <code> 
     *          Issue </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> issueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIssueRecordType(org.osid.type.Type issueRecordType) {
        return (this.issueRecordTypes.contains(issueRecordType));
    }


    /**
     *  Adds support for an issue record type.
     *
     *  @param issueRecordType an issue record type
     *  @throws org.osid.NullArgumentException
     *  <code>issueRecordType</code> is <code>null</code>
     */

    protected void addIssueRecordType(org.osid.type.Type issueRecordType) {
        this.issueRecordTypes.add(issueRecordType);
        return;
    }


    /**
     *  Removes support for an issue record type.
     *
     *  @param issueRecordType an issue record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>issueRecordType</code> is <code>null</code>
     */

    protected void removeIssueRecordType(org.osid.type.Type issueRecordType) {
        this.issueRecordTypes.remove(issueRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Issue </code> search types. 
     *
     *  @return a list containing the supported <code> Issue </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIssueSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.issueSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Issue </code> search type is supported. 
     *
     *  @param  issueSearchRecordType a <code> Type </code> indicating an 
     *          <code> Issue </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> issueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIssueSearchRecordType(org.osid.type.Type issueSearchRecordType) {
        return (this.issueSearchRecordTypes.contains(issueSearchRecordType));
    }


    /**
     *  Adds support for an issue search record type.
     *
     *  @param issueSearchRecordType an issue search record type
     *  @throws org.osid.NullArgumentException
     *  <code>issueSearchRecordType</code> is <code>null</code>
     */

    protected void addIssueSearchRecordType(org.osid.type.Type issueSearchRecordType) {
        this.issueSearchRecordTypes.add(issueSearchRecordType);
        return;
    }


    /**
     *  Removes support for an issue search record type.
     *
     *  @param issueSearchRecordType an issue search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>issueSearchRecordType</code> is <code>null</code>
     */

    protected void removeIssueSearchRecordType(org.osid.type.Type issueSearchRecordType) {
        this.issueSearchRecordTypes.remove(issueSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> LogEntry </code> record types. 
     *
     *  @return a list containing the supported <code> LogEntry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.logEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> LogEntry </code> record type is supported. 
     *
     *  @param  logEntryRecordType a <code> Type </code> indicating a <code> 
     *          LogEntry </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogEntryRecordType(org.osid.type.Type logEntryRecordType) {
        return (this.logEntryRecordTypes.contains(logEntryRecordType));
    }


    /**
     *  Adds support for a log entry record type.
     *
     *  @param logEntryRecordType a log entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>logEntryRecordType</code> is <code>null</code>
     */

    protected void addLogEntryRecordType(org.osid.type.Type logEntryRecordType) {
        this.logEntryRecordTypes.add(logEntryRecordType);
        return;
    }


    /**
     *  Removes support for a log entry record type.
     *
     *  @param logEntryRecordType a log entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>logEntryRecordType</code> is <code>null</code>
     */

    protected void removeLogEntryRecordType(org.osid.type.Type logEntryRecordType) {
        this.logEntryRecordTypes.remove(logEntryRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Queue </code> record types. 
     *
     *  @return a list containing the supported <code> Queue </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Queue </code> record type is supported. 
     *
     *  @param  queueRecordType a <code> Type </code> indicating a <code> 
     *          Queue </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueRecordType(org.osid.type.Type queueRecordType) {
        return (this.queueRecordTypes.contains(queueRecordType));
    }


    /**
     *  Adds support for a queue record type.
     *
     *  @param queueRecordType a queue record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueRecordType</code> is <code>null</code>
     */

    protected void addQueueRecordType(org.osid.type.Type queueRecordType) {
        this.queueRecordTypes.add(queueRecordType);
        return;
    }


    /**
     *  Removes support for a queue record type.
     *
     *  @param queueRecordType a queue record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueRecordType</code> is <code>null</code>
     */

    protected void removeQueueRecordType(org.osid.type.Type queueRecordType) {
        this.queueRecordTypes.remove(queueRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Queue </code> search record types. 
     *
     *  @return a list containing the supported <code> Queue </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.queueSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Queue </code> search record type is 
     *  supported. 
     *
     *  @param  queueSearchRecordType a <code> Type </code> indicating a 
     *          <code> Queue </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueSearchRecordType(org.osid.type.Type queueSearchRecordType) {
        return (this.queueSearchRecordTypes.contains(queueSearchRecordType));
    }


    /**
     *  Adds support for a queue search record type.
     *
     *  @param queueSearchRecordType a queue search record type
     *  @throws org.osid.NullArgumentException
     *  <code>queueSearchRecordType</code> is <code>null</code>
     */

    protected void addQueueSearchRecordType(org.osid.type.Type queueSearchRecordType) {
        this.queueSearchRecordTypes.add(queueSearchRecordType);
        return;
    }


    /**
     *  Removes support for a queue search record type.
     *
     *  @param queueSearchRecordType a queue search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>queueSearchRecordType</code> is <code>null</code>
     */

    protected void removeQueueSearchRecordType(org.osid.type.Type queueSearchRecordType) {
        this.queueSearchRecordTypes.remove(queueSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> FrontOffice </code> record types. 
     *
     *  @return a list containing the supported <code> FrontOffice </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFrontOfficeRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.frontOfficeRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> FrontOffice </code> record type is 
     *  supported. 
     *
     *  @param  frontOfficeRecordType a <code> Type </code> indicating a 
     *          <code> FrontOffice </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> frontOfficeRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFrontOfficeRecordType(org.osid.type.Type frontOfficeRecordType) {
        return (this.frontOfficeRecordTypes.contains(frontOfficeRecordType));
    }


    /**
     *  Adds support for a front office record type.
     *
     *  @param frontOfficeRecordType a front office record type
     *  @throws org.osid.NullArgumentException
     *  <code>frontOfficeRecordType</code> is <code>null</code>
     */

    protected void addFrontOfficeRecordType(org.osid.type.Type frontOfficeRecordType) {
        this.frontOfficeRecordTypes.add(frontOfficeRecordType);
        return;
    }


    /**
     *  Removes support for a front office record type.
     *
     *  @param frontOfficeRecordType a front office record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>frontOfficeRecordType</code> is <code>null</code>
     */

    protected void removeFrontOfficeRecordType(org.osid.type.Type frontOfficeRecordType) {
        this.frontOfficeRecordTypes.remove(frontOfficeRecordType);
        return;
    }


    /**
     *  Gets the supported <code> FrontOffice </code> search record types. 
     *
     *  @return a list containing the supported <code> FrontOffice </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFrontOfficeSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.frontOfficeSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> FrontOffice </code> search record type is 
     *  supported. 
     *
     *  @param  frontOfficeSearchRecordType a <code> Type </code> indicating a 
     *          <code> FrontOffice </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          frontOfficeSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFrontOfficeSearchRecordType(org.osid.type.Type frontOfficeSearchRecordType) {
        return (this.frontOfficeSearchRecordTypes.contains(frontOfficeSearchRecordType));
    }


    /**
     *  Adds support for a front office search record type.
     *
     *  @param frontOfficeSearchRecordType a front office search record type
     *  @throws org.osid.NullArgumentException
     *  <code>frontOfficeSearchRecordType</code> is <code>null</code>
     */

    protected void addFrontOfficeSearchRecordType(org.osid.type.Type frontOfficeSearchRecordType) {
        this.frontOfficeSearchRecordTypes.add(frontOfficeSearchRecordType);
        return;
    }


    /**
     *  Removes support for a front office search record type.
     *
     *  @param frontOfficeSearchRecordType a front office search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>frontOfficeSearchRecordType</code> is <code>null</code>
     */

    protected void removeFrontOfficeSearchRecordType(org.osid.type.Type frontOfficeSearchRecordType) {
        this.frontOfficeSearchRecordTypes.remove(frontOfficeSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my issue 
     *  service. 
     *
     *  @return a <code> MyIssueSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyIssue() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.MyIssueSession getMyIssueSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getMyIssueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my issue 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyIssueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyIssue() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.MyIssueSession getMyIssueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getMyIssueSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my issue 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return a <code> MyIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyIssue() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.MyIssueSession getMyIssueSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getMyIssueSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my issue 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> MyIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyIssue() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.MyIssueSession getMyIssueSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getMyIssueSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue tracking 
     *  service. 
     *
     *  @return an <code> IssueTrackingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueTracking() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTrackingSession getIssueTrackingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueTrackingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue tracking 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueTrackingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueTracking() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTrackingSession getIssueTrackingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueTrackingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue tracking 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return an <code> IssueTrackingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueTracking() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTrackingSession getIssueTrackingSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueTrackingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue tracking 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return an <code> IssueTrackingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueTracking() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTrackingSession getIssueTrackingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueTrackingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  resourcing service. 
     *
     *  @return an <code> IssueResourcingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueResourcing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueResourcingSession getIssueResourcingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueResourcingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  resourcing service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueResourcingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueResourcing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueResourcingSession getIssueResourcingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueResourcingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  resourcing service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return an <code> IssueResourcingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueResourcing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueResourcingSession getIssueResourcingSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueResourcingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  resourcing service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return an <code> IssueResourcingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueResourcing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueResourcingSession getIssueResourcingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueResourcingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue triaging 
     *  service. 
     *
     *  @return an <code> IssueTriagingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyIssueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTriagingSession getIssueTriagingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueTriagingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue triaging 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueTriagingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyIssueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTriagingSession getIssueTriagingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueTriagingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue triaging 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return an <code> IssueTriagingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueTriaging() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTriagingSession getIssueTriagingSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueTriagingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue triaging 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return an <code> IssueTriagingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueTriaging() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTriagingSession getIssueTriagingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueTriagingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service. 
     *
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueLookupSession getIssueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueLookupSession getIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueLookupSession getIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueLookupSession getIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service. 
     *
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuerySession getIssueQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuerySession getIssueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuerySession getIssueQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuerySession getIssueQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service. 
     *
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchSession getIssueSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchSession getIssueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchSession getIssueSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchSession getIssueSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service. 
     *
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueAdminSession getIssueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueAdminSession getIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueAdminSession getIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> Queue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueAdminSession getIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service. 
     *
     *  @param  issueReceiver the notification callback 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueNotificationSession getIssueNotificationSession(org.osid.tracking.IssueReceiver issueReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueNotificationSession getIssueNotificationSession(org.osid.tracking.IssueReceiver issueReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service for the given front office. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver </code> or 
     *          <code> frontOfficeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueNotificationSession getIssueNotificationSessionForFrontOffice(org.osid.tracking.IssueReceiver issueReceiver, 
                                                                                                org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service for the given front office. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver, 
     *          frontOfficeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueNotificationSession getIssueNotificationSessionForFrontOffice(org.osid.tracking.IssueReceiver issueReceiver, 
                                                                                                org.osid.id.Id frontOfficeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup issue/front office 
     *  trackings. 
     *
     *  @return an <code> IssueFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueFrontOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueFrontOfficeSession getIssueFrontOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup issue/front office 
     *  trackings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueFrontOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueFrontOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueFrontOfficeSession getIssueFrontOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning issues 
     *  to frontOffices. 
     *
     *  @return an <code> IssueFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueFrontOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueFrontOfficeAssignmentSession getIssueFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning issues 
     *  to frontOffices. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueFrontOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueFrontOfficeAssignmentSession getIssueFrontOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  frontOffices. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return an <code> IssueSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueSmartFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSmartFrontOfficeSession getIssueSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  frontOffices. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueSmartFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSmartFrontOfficeSession getIssueSmartFrontOfficeSession(org.osid.id.Id frontOfficeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  lookup service. 
     *
     *  @return a <code> SubtaskIssueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueLookupSession getSubtaskIssueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getSubtaskIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueLookupSession getSubtaskIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getSubtaskIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return a <code> SubtaskIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueLookupSession getSubtaskIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getSubtaskIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueLookupSession getSubtaskIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getSubtaskIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  administration service. 
     *
     *  @return a <code> SubtaskIssueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueAdminSession getSubtaskIssueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getSubtaskIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueAdminSession getSubtaskIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getSubtaskIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return a <code> SubtaskIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueAdminSession getSubtaskIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getSubtaskIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueAdminSession getSubtaskIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getSubtaskIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue lookup service. 
     *
     *  @return a <code> DuplicateIssueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueLookupSession getDuplicateIssueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getDuplicateIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DuplicateIssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueLookupSession getDuplicateIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getDuplicateIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return a <code> DuplicateIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueLookupSession getDuplicateIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getDuplicateIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> DuplicateIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueLookupSession getDuplicateIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getDuplicateIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue administration service. 
     *
     *  @return a <code> DuplicateIssueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueAdminSession getDuplicateIssueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getDuplicateIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DuplicateIssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueAdminSession getDuplicateIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getDuplicateIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return a <code> DuplicateIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueAdminSession getDuplicateIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getDuplicateIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> DuplicateIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueAdminSession getDuplicateIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getDuplicateIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  lookup service. 
     *
     *  @return a <code> BranchedIssueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueLookupSession getBranchedIssueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getBranchedIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BranchedIssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueLookupSession getBranchedIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getBranchedIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return a <code> BranchedIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueLookupSession getBranchedIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getBranchedIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> BranchedIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueLookupSession getBranchedIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getBranchedIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  administration service. 
     *
     *  @return a <code> BranchedIssueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueAdminSession getBranchedIssueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getBranchedIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BranchedIssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueAdminSession getBranchedIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getBranchedIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return a <code> BranchedIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueAdminSession getBranchedIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getBranchedIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> BranchedIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueAdminSession getBranchedIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getBranchedIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  lookup service. 
     *
     *  @return a <code> BlockingIssueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueLookupSession getBlockingIssueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getBlockingIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockingIssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueLookupSession getBlockingIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getBlockingIssueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return a <code> BlockingIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueLookupSession getBlockingIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getBlockingIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> BlockingIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueLookupSession getBlockingIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getBlockingIssueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  administration service. 
     *
     *  @return a <code> BlockingIssueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueAdminSession getBlockingIssueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getBlockingIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockingIssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueAdminSession getBlockingIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getBlockingIssueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @return a <code> BlockingIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueAdminSession getBlockingIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getBlockingIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> BlockingIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueAdminSession getBlockingIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getBlockingIssueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  lookup service. 
     *
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryLookupSession getLogEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getLogEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryLookupSession getLogEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getLogEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryLookupSession getLogEntryLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getLogEntryLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryLookupSession getLogEntryLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getLogEntryLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  messaging service. 
     *
     *  @return an <code> IssueMessagingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueMessaging() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueMessagingSession getIssueMessagingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueMessagingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  messaging service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueMessagingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueMessaging() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueMessagingSession getIssueMessagingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueMessagingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  messaging service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return an <code> IssueMessagingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueMessaging() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueMessagingSession getIssueMessagingSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getIssueMessagingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  messaging service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueMessagingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueMessaging() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueMessagingSession getIssueMessagingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getIssueMessagingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  notification service. 
     *
     *  @param  logEntryReceiver the notification callback 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryNotificationSession getLogEntryNotificationSession(org.osid.tracking.LogEntryReceiver logEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getLogEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  notification service. 
     *
     *  @param  logEntryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryNotificationSession getLogEntryNotificationSession(org.osid.tracking.LogEntryReceiver logEntryReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getLogEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  notification service for the given front office. 
     *
     *  @param  logEntryReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver </code> 
     *          or <code> frontOfficeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryNotificationSession getLogEntryNotificationSessionForFrontOffice(org.osid.tracking.LogEntryReceiver logEntryReceiver, 
                                                                                                      org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getLogEntryNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  notification service for the given front office. 
     *
     *  @param  logEntryReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver, 
     *          frontOfficeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryNotificationSession getLogEntryNotificationSessionForFrontOffice(org.osid.tracking.LogEntryReceiver logEntryReceiver, 
                                                                                                      org.osid.id.Id frontOfficeId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getLogEntryNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service. 
     *
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueLookupSession getQueueLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueLookupSession getQueueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueLookupSession getQueueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueLookupSession getQueueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueLookupSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service. 
     *
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuerySession getQueueQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuerySession getQueueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuerySession getQueueQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuerySession getQueueQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueQuerySessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service. 
     *
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSearchSession getQueueSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSearchSession getQueueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSearchSession getQueueSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSearchSession getQueueSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueSearchSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service. 
     *
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueAdminSession getQueueAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueAdminSession getQueueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueAdminSession getQueueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueAdminSession getQueueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueAdminSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service. 
     *
     *  @param  queueReceiver the notification callback 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueNotificationSession getQueueNotificationSession(org.osid.tracking.QueueReceiver queueReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service. 
     *
     *  @param  queueReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueNotificationSession getQueueNotificationSession(org.osid.tracking.QueueReceiver queueReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service for the given front office. 
     *
     *  @param  queueReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver </code> or 
     *          <code> frontOfficeId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueNotificationSession getQueueNotificationSessionForFrontOffice(org.osid.tracking.QueueReceiver queueReceiver, 
                                                                                                org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service for the given front office. 
     *
     *  @param  queueReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver, 
     *          frontOfficeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueNotificationSession getQueueNotificationSessionForFrontOffice(org.osid.tracking.QueueReceiver queueReceiver, 
                                                                                                org.osid.id.Id frontOfficeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueNotificationSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue/front office 
     *  issues. 
     *
     *  @return a <code> QueueFrontOfficeSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueFrontOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueFrontOfficeSession getQueueFrontOfficeSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue/front office 
     *  issues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueFrontOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueFrontOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueFrontOfficeSession getQueueFrontOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queues 
     *  to frontOffices. 
     *
     *  @return a <code> QueueFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueFrontOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueFrontOfficeAssignmentSession getQueueFrontOfficeAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queues 
     *  to frontOffices. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueFrontOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueFrontOfficeAssignmentSession getQueueFrontOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueFrontOfficeAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue smart 
     *  frontOffices. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueSmartFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSmartFrontOfficeSession getQueueSmartFrontOfficeSession(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue smart 
     *  frontOffices. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueSmartFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSmartFrontOfficeSession getQueueSmartFrontOfficeSession(org.osid.id.Id frontOfficeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueSmartFrontOfficeSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  resourcing service. 
     *
     *  @return a <code> QueueResourcingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueResourcing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueResourcingSession getQueueResourcingSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueResourcingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  resourcing service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueResourcingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueResourcing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueResourcingSession getQueueResourcingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueResourcingSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  resourcing service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @return a <code> QueueResourcingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueResourcing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueResourcingSession getQueueResourcingSessionForFrontOffice(org.osid.id.Id frontOfficeId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getQueueResourcingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  resourcing service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> Queue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueResourcingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueResourcing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueResourcingSession getQueueResourcingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getQueueResourcingSessionForFrontOffice not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  lookup service. 
     *
     *  @return a <code> FrontOfficeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeLookupSession getFrontOfficeLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getFrontOfficeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeLookupSession getFrontOfficeLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getFrontOfficeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  query service. 
     *
     *  @return a <code> FrontOfficeQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuerySession getFrontOfficeQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getFrontOfficeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuerySession getFrontOfficeQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getFrontOfficeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  search service. 
     *
     *  @return a <code> FrontOfficeSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeSearchSession getFrontOfficeSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getFrontOfficeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeSearchSession getFrontOfficeSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getFrontOfficeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  administrative service. 
     *
     *  @return a <code> FrontOfficeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeAdminSession getFrontOfficeAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getFrontOfficeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeAdminSession getFrontOfficeAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getFrontOfficeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  notification service. 
     *
     *  @param  frontOfficeReceiver the notification callback 
     *  @return a <code> FrontOfficeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeNotificationSession getFrontOfficeNotificationSession(org.osid.tracking.FrontOfficeReceiver frontOfficeReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getFrontOfficeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  notification service. 
     *
     *  @param  frontOfficeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeNotificationSession getFrontOfficeNotificationSession(org.osid.tracking.FrontOfficeReceiver frontOfficeReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getFrontOfficeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  hierarchy service. 
     *
     *  @return a hierarchy design session for front offices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeHierarchySession getFrontOfficeHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getFrontOfficeHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeHierarchySession </code> for frontOffices 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeHierarchySession getFrontOfficeHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getFrontOfficeHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  hierarchy design service. 
     *
     *  @return a hierarchy design session for front offices 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeHierarchyDesignSession getFrontOfficeHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getFrontOfficeHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for frontOffices 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeHierarchyDesignSession getFrontOfficeHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getFrontOfficeHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> TrackingRulesManager. </code> 
     *
     *  @return a <code> TrackingRulesManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTrackingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.TrackingRulesManager getTrackingRulesManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingManager.getTrackingRulesManager not implemented");
    }


    /**
     *  Gets a <code> TrackingRulesProxyManager. </code> 
     *
     *  @return a <code> TrackingRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTrackingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.TrackingRulesProxyManager getTrackingRulesProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.tracking.TrackingProxyManager.getTrackingRulesProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.issueRecordTypes.clear();
        this.issueRecordTypes.clear();

        this.issueSearchRecordTypes.clear();
        this.issueSearchRecordTypes.clear();

        this.logEntryRecordTypes.clear();
        this.logEntryRecordTypes.clear();

        this.queueRecordTypes.clear();
        this.queueRecordTypes.clear();

        this.queueSearchRecordTypes.clear();
        this.queueSearchRecordTypes.clear();

        this.frontOfficeRecordTypes.clear();
        this.frontOfficeRecordTypes.clear();

        this.frontOfficeSearchRecordTypes.clear();
        this.frontOfficeSearchRecordTypes.clear();

        return;
    }
}
