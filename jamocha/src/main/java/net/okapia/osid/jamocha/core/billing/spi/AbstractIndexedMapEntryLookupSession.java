//
// AbstractIndexedMapEntryLookupSession.java
//
//    A simple framework for providing an Entry lookup service
//    backed by a fixed collection of entries with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an Entry lookup service backed by a
 *  fixed collection of entries. The entries are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some entries may be compatible
 *  with more types than are indicated through these entry
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Entries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapEntryLookupSession
    extends AbstractMapEntryLookupSession
    implements org.osid.billing.EntryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.billing.Entry> entriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Entry>());
    private final MultiMap<org.osid.type.Type, org.osid.billing.Entry> entriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.billing.Entry>());


    /**
     *  Makes an <code>Entry</code> available in this session.
     *
     *  @param  entry an entry
     *  @throws org.osid.NullArgumentException <code>entry<code> is
     *          <code>null</code>
     */

    @Override
    protected void putEntry(org.osid.billing.Entry entry) {
        super.putEntry(entry);

        this.entriesByGenus.put(entry.getGenusType(), entry);
        
        try (org.osid.type.TypeList types = entry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.entriesByRecord.put(types.getNextType(), entry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes an entry from this session.
     *
     *  @param entryId the <code>Id</code> of the entry
     *  @throws org.osid.NullArgumentException <code>entryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeEntry(org.osid.id.Id entryId) {
        org.osid.billing.Entry entry;
        try {
            entry = getEntry(entryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.entriesByGenus.remove(entry.getGenusType());

        try (org.osid.type.TypeList types = entry.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.entriesByRecord.remove(types.getNextType(), entry);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeEntry(entryId);
        return;
    }


    /**
     *  Gets an <code>EntryList</code> corresponding to the given
     *  entry genus <code>Type</code> which does not include
     *  entries of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known entries or an error results. Otherwise,
     *  the returned list may contain only those entries that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  entryGenusType an entry genus type 
     *  @return the returned <code>Entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByGenusType(org.osid.type.Type entryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.entry.ArrayEntryList(this.entriesByGenus.get(entryGenusType)));
    }


    /**
     *  Gets an <code>EntryList</code> containing the given
     *  entry record <code>Type</code>. In plenary mode, the
     *  returned list contains all known entries or an error
     *  results. Otherwise, the returned list may contain only those
     *  entries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  entryRecordType an entry record type 
     *  @return the returned <code>entry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>entryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.EntryList getEntriesByRecordType(org.osid.type.Type entryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.billing.entry.ArrayEntryList(this.entriesByRecord.get(entryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.entriesByGenus.clear();
        this.entriesByRecord.clear();

        super.close();

        return;
    }
}
