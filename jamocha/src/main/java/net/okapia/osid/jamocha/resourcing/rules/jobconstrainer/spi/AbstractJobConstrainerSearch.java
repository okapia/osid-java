//
// AbstractJobConstrainerSearch.java
//
//     A template for making a JobConstrainer Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.rules.jobconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing job constrainer searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractJobConstrainerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resourcing.rules.JobConstrainerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.JobConstrainerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resourcing.rules.JobConstrainerSearchOrder jobConstrainerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of job constrainers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  jobConstrainerIds list of job constrainers
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongJobConstrainers(org.osid.id.IdList jobConstrainerIds) {
        while (jobConstrainerIds.hasNext()) {
            try {
                this.ids.add(jobConstrainerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongJobConstrainers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of job constrainer Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getJobConstrainerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  jobConstrainerSearchOrder job constrainer search order 
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>jobConstrainerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderJobConstrainerResults(org.osid.resourcing.rules.JobConstrainerSearchOrder jobConstrainerSearchOrder) {
	this.jobConstrainerSearchOrder = jobConstrainerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resourcing.rules.JobConstrainerSearchOrder getJobConstrainerSearchOrder() {
	return (this.jobConstrainerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given job constrainer search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a job constrainer implementing the requested record.
     *
     *  @param jobConstrainerSearchRecordType a job constrainer search record
     *         type
     *  @return the job constrainer search record
     *  @throws org.osid.NullArgumentException
     *          <code>jobConstrainerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(jobConstrainerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.JobConstrainerSearchRecord getJobConstrainerSearchRecord(org.osid.type.Type jobConstrainerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resourcing.rules.records.JobConstrainerSearchRecord record : this.records) {
            if (record.implementsRecordType(jobConstrainerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(jobConstrainerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this job constrainer search. 
     *
     *  @param jobConstrainerSearchRecord job constrainer search record
     *  @param jobConstrainerSearchRecordType jobConstrainer search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addJobConstrainerSearchRecord(org.osid.resourcing.rules.records.JobConstrainerSearchRecord jobConstrainerSearchRecord, 
                                           org.osid.type.Type jobConstrainerSearchRecordType) {

        addRecordType(jobConstrainerSearchRecordType);
        this.records.add(jobConstrainerSearchRecord);        
        return;
    }
}
