//
// BooleanMetadata.java
//
//     Defines a boolean Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;

/**
 *  Defines a boolean Metadata.
 */

public final class BooleanMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractBooleanMetadata {


    /**
     *  Constructs a new {@code BooleanMetadata} for single unlinked
     *  elements..
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public BooleanMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
        return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *          is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }

    
    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }

    
    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code
     *         false} if can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }
}
