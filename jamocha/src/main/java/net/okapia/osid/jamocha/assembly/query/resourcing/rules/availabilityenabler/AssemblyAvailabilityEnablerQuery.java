//
// AssemblyAvailabilityEnablerQuery.java
//
//     An AvailabilityEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.rules.availabilityenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An AvailabilityEnablerQuery that stores terms.
 */

public final class AssemblyAvailabilityEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.resourcing.rules.availabilityenabler.spi.AbstractAssemblyAvailabilityEnablerQuery
    implements org.osid.resourcing.rules.AvailabilityEnablerQuery,
               org.osid.resourcing.rules.AvailabilityEnablerQueryInspector,
               org.osid.resourcing.rules.AvailabilityEnablerSearchOrder {


    /** 
     *  Constructs a new <code>AssemblyAvailabilityEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    public AssemblyAvailabilityEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }


    /**
     *  Gets the query assembler.
     *
     *  @return the query assembler
     */

    public net.okapia.osid.jamocha.assembly.query.QueryAssembler getAssembler() {
        return (super.getAssembler());
    }
}
