//
// AbstractMapQuery.java
//
//     A template for making a Map Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.map.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for maps.
 */

public abstract class AbstractMapQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.mapping.MapQuery {

    private final java.util.Collection<org.osid.mapping.records.MapQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the location <code> Id </code> for this query to match maps that 
     *  have a related location. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches maps that have any location. 
     *
     *  @param  match <code> true </code> to match maps with any location, 
     *          <code> false </code> to match maps with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        return;
    }


    /**
     *  Clears the location query terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        return;
    }


    /**
     *  Sets the path <code> Id </code> for this query to match maps 
     *  containing paths. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Matches maps that have any path. 
     *
     *  @param  match <code> true </code> to match maps with any path, <code> 
     *          false </code> to match maps with no path 
     */

    @OSID @Override
    public void matchAnyPath(boolean match) {
        return;
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        return;
    }


    /**
     *  Sets the path <code> Id </code> for this query to match maps 
     *  containing paths. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRouteId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the route <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRouteIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RouteQuery </code> is available. 
     *
     *  @return <code> true </code> if a route query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (false);
    }


    /**
     *  Gets the query for a route. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the route query 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuery getRouteQuery() {
        throw new org.osid.UnimplementedException("supportsRouteQuery() is false");
    }


    /**
     *  Matches maps that have any route. 
     *
     *  @param  match <code> true </code> to match maps with any route, <code> 
     *          false </code> to match maps with no route 
     */

    @OSID @Override
    public void matchAnyRoute(boolean match) {
        return;
    }


    /**
     *  Clears the route query terms. 
     */

    @OSID @Override
    public void clearRouteTerms() {
        return;
    }


    /**
     *  Sets the map <code> Id </code> for this query to match maps that have 
     *  the specified map as an ancestor. 
     *
     *  @param  mapId a map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchAncestorMapId(org.osid.id.Id mapId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorMapIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorMapQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getAncestorMapQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorMapQuery() is false");
    }


    /**
     *  Matches maps with any ancestor. 
     *
     *  @param  match <code> true </code> to match maps with any ancestor, 
     *          <code> false </code> to match root maps 
     */

    @OSID @Override
    public void matchAnyAncestorMap(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor map query terms. 
     */

    @OSID @Override
    public void clearAncestorMapTerms() {
        return;
    }


    /**
     *  Sets the map <code> Id </code> for this query to match maps that have 
     *  the specified map as a descendant. 
     *
     *  @param  mapId a map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchDescendantMapId(org.osid.id.Id mapId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantMapIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantMapQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getDescendantMapQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantMapQuery() is false");
    }


    /**
     *  Matches maps with any descendant. 
     *
     *  @param  match <code> true </code> to match maps with any descendant, 
     *          <code> false </code> to match leaf maps 
     */

    @OSID @Override
    public void matchAnyDescendantMap(boolean match) {
        return;
    }


    /**
     *  Clears the descendant map query terms. 
     */

    @OSID @Override
    public void clearDescendantMapTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given map query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a map implementing the requested record.
     *
     *  @param mapRecordType a map record type
     *  @return the map query record
     *  @throws org.osid.NullArgumentException
     *          <code>mapRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(mapRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.records.MapQueryRecord getMapQueryRecord(org.osid.type.Type mapRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.records.MapQueryRecord record : this.records) {
            if (record.implementsRecordType(mapRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(mapRecordType + " is not supported");
    }


    /**
     *  Adds a record to this map query. 
     *
     *  @param mapQueryRecord map query record
     *  @param mapRecordType map record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addMapQueryRecord(org.osid.mapping.records.MapQueryRecord mapQueryRecord, 
                                          org.osid.type.Type mapRecordType) {

        addRecordType(mapRecordType);
        nullarg(mapQueryRecord, "map query record");
        this.records.add(mapQueryRecord);        
        return;
    }
}
