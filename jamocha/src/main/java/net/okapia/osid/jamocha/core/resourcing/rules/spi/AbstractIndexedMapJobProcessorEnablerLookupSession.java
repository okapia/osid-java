//
// AbstractIndexedMapJobProcessorEnablerLookupSession.java
//
//    A simple framework for providing a JobProcessorEnabler lookup service
//    backed by a fixed collection of job processor enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a JobProcessorEnabler lookup service backed by a
 *  fixed collection of job processor enablers. The job processor enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some job processor enablers may be compatible
 *  with more types than are indicated through these job processor enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>JobProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapJobProcessorEnablerLookupSession
    extends AbstractMapJobProcessorEnablerLookupSession
    implements org.osid.resourcing.rules.JobProcessorEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.JobProcessorEnabler> jobProcessorEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.JobProcessorEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.resourcing.rules.JobProcessorEnabler> jobProcessorEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.resourcing.rules.JobProcessorEnabler>());


    /**
     *  Makes a <code>JobProcessorEnabler</code> available in this session.
     *
     *  @param  jobProcessorEnabler a job processor enabler
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putJobProcessorEnabler(org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler) {
        super.putJobProcessorEnabler(jobProcessorEnabler);

        this.jobProcessorEnablersByGenus.put(jobProcessorEnabler.getGenusType(), jobProcessorEnabler);
        
        try (org.osid.type.TypeList types = jobProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobProcessorEnablersByRecord.put(types.getNextType(), jobProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a job processor enabler from this session.
     *
     *  @param jobProcessorEnablerId the <code>Id</code> of the job processor enabler
     *  @throws org.osid.NullArgumentException <code>jobProcessorEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeJobProcessorEnabler(org.osid.id.Id jobProcessorEnablerId) {
        org.osid.resourcing.rules.JobProcessorEnabler jobProcessorEnabler;
        try {
            jobProcessorEnabler = getJobProcessorEnabler(jobProcessorEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.jobProcessorEnablersByGenus.remove(jobProcessorEnabler.getGenusType());

        try (org.osid.type.TypeList types = jobProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.jobProcessorEnablersByRecord.remove(types.getNextType(), jobProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeJobProcessorEnabler(jobProcessorEnablerId);
        return;
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> corresponding to the given
     *  job processor enabler genus <code>Type</code> which does not include
     *  job processor enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known job processor enablers or an error results. Otherwise,
     *  the returned list may contain only those job processor enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  jobProcessorEnablerGenusType a job processor enabler genus type 
     *  @return the returned <code>JobProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByGenusType(org.osid.type.Type jobProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobprocessorenabler.ArrayJobProcessorEnablerList(this.jobProcessorEnablersByGenus.get(jobProcessorEnablerGenusType)));
    }


    /**
     *  Gets a <code>JobProcessorEnablerList</code> containing the given
     *  job processor enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known job processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  job processor enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  jobProcessorEnablerRecordType a job processor enabler record type 
     *  @return the returned <code>jobProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorEnablerList getJobProcessorEnablersByRecordType(org.osid.type.Type jobProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.resourcing.rules.jobprocessorenabler.ArrayJobProcessorEnablerList(this.jobProcessorEnablersByRecord.get(jobProcessorEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.jobProcessorEnablersByGenus.clear();
        this.jobProcessorEnablersByRecord.clear();

        super.close();

        return;
    }
}
