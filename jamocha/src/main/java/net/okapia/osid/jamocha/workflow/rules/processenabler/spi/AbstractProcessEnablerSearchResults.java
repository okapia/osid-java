//
// AbstractProcessEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.processenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProcessEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.workflow.rules.ProcessEnablerSearchResults {

    private org.osid.workflow.rules.ProcessEnablerList processEnablers;
    private final org.osid.workflow.rules.ProcessEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.workflow.rules.records.ProcessEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProcessEnablerSearchResults.
     *
     *  @param processEnablers the result set
     *  @param processEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>processEnablers</code>
     *          or <code>processEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProcessEnablerSearchResults(org.osid.workflow.rules.ProcessEnablerList processEnablers,
                                            org.osid.workflow.rules.ProcessEnablerQueryInspector processEnablerQueryInspector) {
        nullarg(processEnablers, "process enablers");
        nullarg(processEnablerQueryInspector, "process enabler query inspectpr");

        this.processEnablers = processEnablers;
        this.inspector = processEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the process enabler list resulting from a search.
     *
     *  @return a process enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablers() {
        if (this.processEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.workflow.rules.ProcessEnablerList processEnablers = this.processEnablers;
        this.processEnablers = null;
	return (processEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.workflow.rules.ProcessEnablerQueryInspector getProcessEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  process enabler search record <code> Type. </code> This method must
     *  be used to retrieve a processEnabler implementing the requested
     *  record.
     *
     *  @param processEnablerSearchRecordType a processEnabler search 
     *         record type 
     *  @return the process enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(processEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.ProcessEnablerSearchResultsRecord getProcessEnablerSearchResultsRecord(org.osid.type.Type processEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.workflow.rules.records.ProcessEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(processEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(processEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record process enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProcessEnablerRecord(org.osid.workflow.rules.records.ProcessEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "process enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
