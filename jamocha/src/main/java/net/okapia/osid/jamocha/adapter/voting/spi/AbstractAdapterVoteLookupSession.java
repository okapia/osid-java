//
// AbstractAdapterVoteLookupSession.java
//
//    A Vote lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Vote lookup session adapter.
 */

public abstract class AbstractAdapterVoteLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.voting.VoteLookupSession {

    private final org.osid.voting.VoteLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterVoteLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterVoteLookupSession(org.osid.voting.VoteLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Polls/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Polls Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.session.getPollsId());
    }


    /**
     *  Gets the {@code Polls} associated with this session.
     *
     *  @return the {@code Polls} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getPolls());
    }


    /**
     *  Tests if this user can perform {@code Vote} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupVotes() {
        return (this.session.canLookupVotes());
    }


    /**
     *  A complete view of the {@code Vote} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeVoteView() {
        this.session.useComparativeVoteView();
        return;
    }


    /**
     *  A complete view of the {@code Vote} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryVoteView() {
        this.session.usePlenaryVoteView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include votes in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        this.session.useFederatedPollsView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        this.session.useIsolatedPollsView();
        return;
    }
    

    /**
     *  Only votes whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveVoteView() {
        this.session.useEffectiveVoteView();
        return;
    }
    

    /**
     *  All votes of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveVoteView() {
        this.session.useAnyEffectiveVoteView();
        return;
    }

     
    /**
     *  Gets the {@code Vote} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Vote} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Vote} and
     *  retained for compatibility.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param voteId {@code Id} of the {@code Vote}
     *  @return the vote
     *  @throws org.osid.NotFoundException {@code voteId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code voteId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Vote getVote(org.osid.id.Id voteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVote(voteId));
    }


    /**
     *  Gets a {@code VoteList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  votes specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Votes} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  voteIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Vote} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code voteIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByIds(org.osid.id.IdList voteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesByIds(voteIds));
    }


    /**
     *  Gets a {@code VoteList} corresponding to the given
     *  vote genus {@code Type} which does not include
     *  votes of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  voteGenusType a vote genus type 
     *  @return the returned {@code Vote} list
     *  @throws org.osid.NullArgumentException
     *          {@code voteGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByGenusType(org.osid.type.Type voteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesByGenusType(voteGenusType));
    }


    /**
     *  Gets a {@code VoteList} corresponding to the given
     *  vote genus {@code Type} and include any additional
     *  votes with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  voteGenusType a vote genus type 
     *  @return the returned {@code Vote} list
     *  @throws org.osid.NullArgumentException
     *          {@code voteGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByParentGenusType(org.osid.type.Type voteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesByParentGenusType(voteGenusType));
    }


    /**
     *  Gets a {@code VoteList} containing the given
     *  vote record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @param  voteRecordType a vote record type 
     *  @return the returned {@code Vote} list
     *  @throws org.osid.NullArgumentException
     *          {@code voteRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesByRecordType(org.osid.type.Type voteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesByRecordType(voteRecordType));
    }


    /**
     *  Gets a {@code VoteList} effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *  
     *  In active mode, votes are returned that are currently
     *  active. In any status mode, active and inactive votes are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code Vote} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.VoteList getVotesOnDate(org.osid.calendaring.DateTime from, 
                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesOnDate(from, to));
    }
        

    /**
     *  Gets a list of votes corresponding to a voter
     *  {@code Id}.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the voter
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoter(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForVoter(resourceId));
    }


    /**
     *  Gets a list of votes corresponding to a voter {@code Id} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the voter
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterOnDate(org.osid.id.Id resourceId,
                                                           org.osid.calendaring.DateTime from,
                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForVoterOnDate(resourceId, from, to));
    }


    /**
     *  Gets a list of votes corresponding to a candidate {@code Id}.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  candidateId the {@code Id} of the candidate
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code candidateId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForCandidate(org.osid.id.Id candidateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForCandidate(candidateId));
    }


    /**
     *  Gets a list of votes corresponding to a candidate {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  candidateId the {@code Id} of the candidate
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code candidateId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForCandidateOnDate(org.osid.id.Id candidateId,
                                                               org.osid.calendaring.DateTime from,
                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForCandidateOnDate(candidateId, from, to));
    }


    /**
     *  Gets a list of votes corresponding to voter and candidate
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the voter
     *  @param  candidateId the {@code Id} of the candidate
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code candidateId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndCandidate(org.osid.id.Id resourceId,
                                                                 org.osid.id.Id candidateId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForVoterAndCandidate(resourceId, candidateId));
    }


    /**
     *  Gets a list of votes corresponding to voter and candidate
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective. In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  candidateId the {@code Id} of the candidate
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code candidateId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndCandidateOnDate(org.osid.id.Id resourceId,
                                                                       org.osid.id.Id candidateId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForVoterAndCandidateOnDate(resourceId, candidateId, from, to));
    }


    /**
     *  Gets a list of votes corresponding to a race {@code Id}.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  raceId the {@code Id} of the race
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code raceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForRace(org.osid.id.Id raceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForRace(raceId));
    }


    /**
     *  Gets a list of votes corresponding to a race {@code Id}
     *  and effective during the entire given date range inclusive but
     *  not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  raceId the {@code Id} of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code raceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForRaceOnDate(org.osid.id.Id raceId,
                                                          org.osid.calendaring.DateTime from,
                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForRaceOnDate(raceId, from, to));
    }


    /**
     *  Gets a list of votes corresponding to voter and race {@code
     *  Ids}.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the voter
     *  @param  raceId the {@code Id} of the race
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code raceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndRace(org.osid.id.Id resourceId,
                                                            org.osid.id.Id raceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForVoterAndRace(resourceId, raceId));
    }


    /**
     *  Gets a list of votes corresponding to voter and race {@code
     *  Ids} and effective during the entire given date range
     *  inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective. In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  raceId the {@code Id} of the race
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code raceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndRaceOnDate(org.osid.id.Id resourceId,
                                                                  org.osid.id.Id raceId,
                                                                  org.osid.calendaring.DateTime from,
                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForVoterAndRaceOnDate(resourceId, raceId, from, to));
    }


    /**
     *  Gets a list of votes corresponding to a ballot {@code Id}.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  ballotId the {@code Id} of the ballot
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code ballotId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForBallot(org.osid.id.Id ballotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForBallot(ballotId));
    }


    /**
     *  Gets a list of votes corresponding to a ballot {@code Id} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  ballotId the {@code Id} of the ballot
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code ballotId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForBallotOnDate(org.osid.id.Id ballotId,
                                                            org.osid.calendaring.DateTime from,
                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForBallotOnDate(ballotId, from, to));
    }


    /**
     *  Gets a list of votes corresponding to voter and ballot
     *  {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  resourceId the {@code Id} of the voter
     *  @param  ballotId the {@code Id} of the ballot
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code ballotId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndBallot(org.osid.id.Id resourceId,
                                                              org.osid.id.Id ballotId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForVoterAndBallot(resourceId, ballotId));
    }


    /**
     *  Gets a list of votes corresponding to voter and ballot
     *  {@code Ids} and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known votes or
     *  an error results. Otherwise, the returned list may contain
     *  only those votes that are accessible through this session.
     *
     *  In effective mode, votes are returned that are currently
     *  effective. In any effective mode, effective votes and those
     *  currently expired are returned.
     *
     *  @param  ballotId the {@code Id} of the ballot
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code VoteList}
     *  @throws org.osid.NullArgumentException {@code resourceId},
     *          {@code ballotId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotesForVoterAndBallotOnDate(org.osid.id.Id resourceId,
                                                                    org.osid.id.Id ballotId,
                                                                    org.osid.calendaring.DateTime from,
                                                                    org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotesForVoterAndBallotOnDate(resourceId, ballotId, from, to));
    }

    /**
     *  Gets all {@code Votes}. 
     *
     *  In plenary mode, the returned list contains all known
     *  votes or an error results. Otherwise, the returned list
     *  may contain only those votes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, votes are returned that are currently
     *  effective.  In any effective mode, effective votes and
     *  those currently expired are returned.
     *
     *  @return a list of {@code Votes} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.VoteList getVotes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getVotes());
    }
}
