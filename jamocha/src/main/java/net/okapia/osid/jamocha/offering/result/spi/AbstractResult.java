//
// AbstractResult.java
//
//     Defines a Result.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.result.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Result</code>.
 */

public abstract class AbstractResult
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.offering.Result {

    private org.osid.offering.Participant participant;
    private org.osid.grading.Grade grade;
    private java.math.BigDecimal value;

    private final java.util.Collection<org.osid.offering.records.ResultRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the participant in this result. 
     *
     *  @return the <code> Participant </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getParticipantId() {
        return (this.participant.getId());
    }


    /**
     *  Gets the participant in this result. 
     *
     *  @return the <code> Participant </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.offering.Participant getParticipant()
        throws org.osid.OperationFailedException {

        return (this.participant);
    }


    /**
     *  Sets the participant.
     *
     *  @param participant a participant
     *  @throws org.osid.NullArgumentException
     *          <code>participant</code> is <code>null</code>
     */

    protected void setParticipant(org.osid.offering.Participant participant) {
        nullarg(participant, "participant");
        this.participant = participant;
        return;
    }


    /**
     *  Tests if a grade or score has been assigned to this entry. Generally, 
     *  an entry is created with a grade or score. 
     *
     *  @return <code> true </code> if a grade has been assigned, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isGraded() {
        return (this.grade != null);
    }


    /**
     *  Gets the grade <code> Id </code> in this entry if the grading system 
     *  is based on grades. 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> GradeSystem.isBasedOnGrades() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradeId() {
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.grade.getId());
    }


    /**
     *  Gets the grade in this entry if the grading system is based on grades. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> GradeSystem.isBasedOnGrades() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getGrade()
        throws org.osid.OperationFailedException {

        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.grade);
    }


    /**
     *  Sets the grade.
     *
     *  @param grade a grade
     *  @throws org.osid.NullArgumentException
     *          <code>grade</code> is <code>null</code>
     */

    protected void setGrade(org.osid.grading.Grade grade) {
        nullarg(grade, "grade");
        this.grade = grade;
        return;
    }


    /**
     *  Gets the value in this entry if the grading system is not based on 
     *  grades. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> isGraded() </code> is 
     *          <code> false </code> or <code> GradeSystem.isBasedOnGrades() 
     *          </code> is <code> true </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getValue() {
        if (!isGraded()) {
            throw new org.osid.IllegalStateException("isGraded() is false");
        }

        return (this.value);
    }


    /**
     *  Sets the value.
     *
     *  @param value a value
     *  @throws org.osid.NullArgumentException
     *          <code>value</code> is <code>null</code>
     */

    protected void setValue(java.math.BigDecimal value) {
        this.value = value;
        return;
    }

    /**
     *  Tests if this result supports the given record
     *  <code>Type</code>.
     *
     *  @param  resultRecordType a result record type 
     *  @return <code>true</code> if the resultRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resultRecordType) {
        for (org.osid.offering.records.ResultRecord record : this.records) {
            if (record.implementsRecordType(resultRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  resultRecordType the result record type 
     *  @return the result record 
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resultRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.ResultRecord getResultRecord(org.osid.type.Type resultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.ResultRecord record : this.records) {
            if (record.implementsRecordType(resultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resultRecordType + " is not supported");
    }


    /**
     *  Adds a record to this result. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resultRecord the result record
     *  @param resultRecordType result record type
     *  @throws org.osid.NullArgumentException
     *          <code>resultRecord</code> or
     *          <code>resultRecordTyperesult</code> is
     *          <code>null</code>
     */
            
    protected void addResultRecord(org.osid.offering.records.ResultRecord resultRecord, 
                                     org.osid.type.Type resultRecordType) {

        nullarg(resultRecord, "result record");
        addRecordType(resultRecordType);
        this.records.add(resultRecord);
        
        return;
    }
}
