//
// AbstractProjectSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.construction.project.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractProjectSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.room.construction.ProjectSearchResults {

    private org.osid.room.construction.ProjectList projects;
    private final org.osid.room.construction.ProjectQueryInspector inspector;
    private final java.util.Collection<org.osid.room.construction.records.ProjectSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractProjectSearchResults.
     *
     *  @param projects the result set
     *  @param projectQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>projects</code>
     *          or <code>projectQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractProjectSearchResults(org.osid.room.construction.ProjectList projects,
                                            org.osid.room.construction.ProjectQueryInspector projectQueryInspector) {
        nullarg(projects, "projects");
        nullarg(projectQueryInspector, "project query inspectpr");

        this.projects = projects;
        this.inspector = projectQueryInspector;

        return;
    }


    /**
     *  Gets the project list resulting from a search.
     *
     *  @return a project list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.room.construction.ProjectList getProjects() {
        if (this.projects == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.room.construction.ProjectList projects = this.projects;
        this.projects = null;
	return (projects);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.room.construction.ProjectQueryInspector getProjectQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  project search record <code> Type. </code> This method must
     *  be used to retrieve a project implementing the requested
     *  record.
     *
     *  @param projectSearchRecordType a project search 
     *         record type 
     *  @return the project search
     *  @throws org.osid.NullArgumentException
     *          <code>projectSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(projectSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.construction.records.ProjectSearchResultsRecord getProjectSearchResultsRecord(org.osid.type.Type projectSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.room.construction.records.ProjectSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(projectSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(projectSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record project search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addProjectRecord(org.osid.room.construction.records.ProjectSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "project record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
