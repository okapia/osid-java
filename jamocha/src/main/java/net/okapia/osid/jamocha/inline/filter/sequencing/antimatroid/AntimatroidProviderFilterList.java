//
// AntimatroidProviderFilterList.java
//
//     Implements a filter for providers.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.sequencing.antimatroid;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for providers.
 */

public final class AntimatroidProviderFilterList
    extends net.okapia.osid.jamocha.inline.filter.sequencing.antimatroid.spi.AbstractAntimatroidFilterList
    implements org.osid.sequencing.AntimatroidList,
               AntimatroidFilter {

    private final org.osid.id.Id resourceId;


    /**
     *  Creates a new <code>AntimatroidProviderFilterList</code>.
     *
     *  @param list an <code>AntimatroidList</code>
     *  @param resourceId the provider Id
     *  @throws org.osid.NullArgumentException <code>list</code> or
     *          <code>resourceId</code> is <code>null</code>
     */

    public AntimatroidProviderFilterList(org.osid.sequencing.AntimatroidList list, org.osid.id.Id resourceId) {
        super(list);
        this.resourceId = resourceId;
        return;
    }    

    
    /**
     *  Filters Antimatroids.
     *
     *  @param antimatroid the antimatroid to filter
     *  @return <code>true</code> if the antimatroid passes the filter,
     *          <code>false</code> if the antimatroid should be filtered
     */

    @Override
    public boolean pass(org.osid.sequencing.Antimatroid antimatroid) {
        return (antimatroid.getProviderId().equals(this.resourceId));
    }
}
