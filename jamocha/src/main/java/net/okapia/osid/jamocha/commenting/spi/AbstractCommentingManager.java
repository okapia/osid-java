//
// AbstractCommentingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCommentingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.commenting.CommentingManager,
               org.osid.commenting.CommentingProxyManager {

    private final Types commentRecordTypes                 = new TypeRefSet();
    private final Types commentSearchRecordTypes           = new TypeRefSet();

    private final Types bookRecordTypes                    = new TypeRefSet();
    private final Types bookSearchRecordTypes              = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCommentingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCommentingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any book federation is exposed. Federation is exposed when a 
     *  specific book may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of books 
     *  appears as a single book. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a comment lookup service. 
     *
     *  @return <code> true </code> if comment lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentLookup() {
        return (false);
    }


    /**
     *  Tests for the availability of a rating lookup service. 
     *
     *  @return <code> true </code> if rating lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRatingLookup() {
        return (false);
    }


    /**
     *  Tests if querying comments is available. 
     *
     *  @return <code> true </code> if comment query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentQuery() {
        return (false);
    }


    /**
     *  Tests if searching for comments is available. 
     *
     *  @return <code> true </code> if comment search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentSearch() {
        return (false);
    }


    /**
     *  Tests if managing comments is available. 
     *
     *  @return <code> true </code> if comment admin is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentAdmin() {
        return (false);
    }


    /**
     *  Tests if comment notification is available. 
     *
     *  @return <code> true </code> if comment notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentNotification() {
        return (false);
    }


    /**
     *  Tests if a comment to book lookup session is available. 
     *
     *  @return <code> true </code> if comment book lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentBook() {
        return (false);
    }


    /**
     *  Tests if a comment to book assignment session is available. 
     *
     *  @return <code> true </code> if comment book assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentBookAssignment() {
        return (false);
    }


    /**
     *  Tests if a comment smart booking session is available. 
     *
     *  @return <code> true </code> if comment smart booking is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentSmartBook() {
        return (false);
    }


    /**
     *  Tests for the availability of an book lookup service. 
     *
     *  @return <code> true </code> if book lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookLookup() {
        return (false);
    }


    /**
     *  Tests if querying books is available. 
     *
     *  @return <code> true </code> if book query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookQuery() {
        return (false);
    }


    /**
     *  Tests if searching for books is available. 
     *
     *  @return <code> true </code> if book search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a book administrative service for 
     *  creating and deleting books. 
     *
     *  @return <code> true </code> if book administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a book notification service. 
     *
     *  @return <code> true </code> if book notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a book hierarchy traversal service. 
     *
     *  @return <code> true </code> if book hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a book hierarchy design service. 
     *
     *  @return <code> true </code> if book hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBookHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a commenting batch service. 
     *
     *  @return <code> true </code> if commenting batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCommentingBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Comment </code> record types. 
     *
     *  @return a list containing the supported comment record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commentRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Comment </code> record type is supported. 
     *
     *  @param  commentRecordType a <code> Type </code> indicating a <code> 
     *          Comment </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> commentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommentRecordType(org.osid.type.Type commentRecordType) {
        return (this.commentRecordTypes.contains(commentRecordType));
    }


    /**
     *  Adds support for a comment record type.
     *
     *  @param commentRecordType a comment record type
     *  @throws org.osid.NullArgumentException
     *  <code>commentRecordType</code> is <code>null</code>
     */

    protected void addCommentRecordType(org.osid.type.Type commentRecordType) {
        this.commentRecordTypes.add(commentRecordType);
        return;
    }


    /**
     *  Removes support for a comment record type.
     *
     *  @param commentRecordType a comment record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commentRecordType</code> is <code>null</code>
     */

    protected void removeCommentRecordType(org.osid.type.Type commentRecordType) {
        this.commentRecordTypes.remove(commentRecordType);
        return;
    }


    /**
     *  Gets the supported comment search record types. 
     *
     *  @return a list containing the supported comment search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCommentSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.commentSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given comment search record type is supported. 
     *
     *  @param  commentSearchRecordType a <code> Type </code> indicating a 
     *          comment record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> commentSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCommentSearchRecordType(org.osid.type.Type commentSearchRecordType) {
        return (this.commentSearchRecordTypes.contains(commentSearchRecordType));
    }


    /**
     *  Adds support for a comment search record type.
     *
     *  @param commentSearchRecordType a comment search record type
     *  @throws org.osid.NullArgumentException
     *  <code>commentSearchRecordType</code> is <code>null</code>
     */

    protected void addCommentSearchRecordType(org.osid.type.Type commentSearchRecordType) {
        this.commentSearchRecordTypes.add(commentSearchRecordType);
        return;
    }


    /**
     *  Removes support for a comment search record type.
     *
     *  @param commentSearchRecordType a comment search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>commentSearchRecordType</code> is <code>null</code>
     */

    protected void removeCommentSearchRecordType(org.osid.type.Type commentSearchRecordType) {
        this.commentSearchRecordTypes.remove(commentSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Book </code> record types. 
     *
     *  @return a list containing the supported book record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBookRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.bookRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Book </code> record type is supported. 
     *
     *  @param  bookRecordType a <code> Type </code> indicating a <code> Book 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bookRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBookRecordType(org.osid.type.Type bookRecordType) {
        return (this.bookRecordTypes.contains(bookRecordType));
    }


    /**
     *  Adds support for a book record type.
     *
     *  @param bookRecordType a book record type
     *  @throws org.osid.NullArgumentException
     *  <code>bookRecordType</code> is <code>null</code>
     */

    protected void addBookRecordType(org.osid.type.Type bookRecordType) {
        this.bookRecordTypes.add(bookRecordType);
        return;
    }


    /**
     *  Removes support for a book record type.
     *
     *  @param bookRecordType a book record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>bookRecordType</code> is <code>null</code>
     */

    protected void removeBookRecordType(org.osid.type.Type bookRecordType) {
        this.bookRecordTypes.remove(bookRecordType);
        return;
    }


    /**
     *  Gets the supported book search record types. 
     *
     *  @return a list containing the supported book search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBookSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.bookSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given book search record type is supported. 
     *
     *  @param  bookSearchRecordType a <code> Type </code> indicating a book 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bookSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBookSearchRecordType(org.osid.type.Type bookSearchRecordType) {
        return (this.bookSearchRecordTypes.contains(bookSearchRecordType));
    }


    /**
     *  Adds support for a book search record type.
     *
     *  @param bookSearchRecordType a book search record type
     *  @throws org.osid.NullArgumentException
     *  <code>bookSearchRecordType</code> is <code>null</code>
     */

    protected void addBookSearchRecordType(org.osid.type.Type bookSearchRecordType) {
        this.bookSearchRecordTypes.add(bookSearchRecordType);
        return;
    }


    /**
     *  Removes support for a book search record type.
     *
     *  @param bookSearchRecordType a book search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>bookSearchRecordType</code> is <code>null</code>
     */

    protected void removeBookSearchRecordType(org.osid.type.Type bookSearchRecordType) {
        this.bookSearchRecordTypes.remove(bookSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment lookup 
     *  service. 
     *
     *  @return a <code> CommentLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentLookupSession getCommentLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentLookupSession getCommentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment lookup 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @return a <code> CommentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentLookupSession getCommentLookupSessionForBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentLookupSessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment lookup 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentLookupSession getCommentLookupSessionForBook(org.osid.id.Id bookId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentLookupSessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rating lookup 
     *  service. 
     *
     *  @return a <code> RatingLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRatingLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.RatingLookupSession getRatingLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getRatingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rating lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RatingLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRatingLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.RatingLookupSession getRatingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getRatingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rating lookup 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @return a <code> RatingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRatingLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.RatingLookupSession getRatingLookupSessionForBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getRatingLookupSessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rating lookup 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RatingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRatingLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.RatingLookupSession getRatingLookupSessionForBook(org.osid.id.Id bookId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getRatingLookupSessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment query 
     *  service. 
     *
     *  @return a <code> CommentQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentQuerySession getCommentQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentQuerySession getCommentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment query 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @return a <code> CommentQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentQuerySession getCommentQuerySessionForBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentQuerySessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment query 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Comment </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentQuerySession getCommentQuerySessionForBook(org.osid.id.Id bookId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentQuerySessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment search 
     *  service. 
     *
     *  @return a <code> CommentSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentSearchSession getCommentSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentSearchSession getCommentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment search 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @return a <code> CommentSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentSearchSession getCommentSearchSessionForBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentSearchSessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment search 
     *  service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Comment </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentSearchSession getCommentSearchSessionForBook(org.osid.id.Id bookId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentSearchSessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  administration service. 
     *
     *  @return a <code> CommentAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentAdminSession getCommentAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentAdminSession getCommentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  administration service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @return a <code> CommentAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentAdminSession getCommentAdminSessionForBook(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentAdminSessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  administration service for the given book. 
     *
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Comment </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentAdminSession getCommentAdminSessionForBook(org.osid.id.Id bookId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentAdminSessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  notification service. 
     *
     *  @param  commentReceiver the receiver 
     *  @return a <code> CommentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> commentReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentNotificationSession getCommentNotificationSession(org.osid.commenting.CommentReceiver commentReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  notification service. 
     *
     *  @param  commentReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CommentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> commentReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentNotificationSession getCommentNotificationSession(org.osid.commenting.CommentReceiver commentReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  notification service for the given book. 
     *
     *  @param  commentReceiver the receiver 
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @return a <code> CommentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Book </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> commentReceiver </code> 
     *          or <code> bookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentNotificationSession getCommentNotificationSessionForBook(org.osid.commenting.CommentReceiver commentReceiver, 
                                                                                               org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentNotificationSessionForBook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the comment 
     *  notification service for the given book. 
     *
     *  @param  commentReceiver the receiver 
     *  @param  bookId the <code> Id </code> of the <code> Book </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CommentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Comment </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> commentReceiver, bookId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentNotificationSession getCommentNotificationSessionForBook(org.osid.commenting.CommentReceiver commentReceiver, 
                                                                                               org.osid.id.Id bookId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentNotificationSessionForBook not implemented");
    }


    /**
     *  Gets the session for retrieving comment to book mappings. 
     *
     *  @return a <code> CommentBookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentBook() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentBookSession getCommentBookSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentBookSession not implemented");
    }


    /**
     *  Gets the session for retrieving comment to book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentBookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCommentBook() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentBookSession getCommentBookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentBookSession not implemented");
    }


    /**
     *  Gets the session for assigning comment to book mappings. 
     *
     *  @return a <code> CommentBookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentBookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentBookAssignmentSession getCommentBookAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentBookAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning comment to book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CommentBookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentBookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentBookAssignmentSession getCommentBookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentBookAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the comment smart book for the given 
     *  book. 
     *
     *  @param  bookId the <code> Id </code> of the book 
     *  @return a <code> CommentSmartBookSession </code> 
     *  @throws org.osid.NotFoundException <code> bookId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bookId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentSmartBook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentSmartBookSession getCommentSmartBookSession(org.osid.id.Id bookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentSmartBookSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic comment books for the given 
     *  book. 
     *
     *  @param  bookId the <code> Id </code> of a book 
     *  @param  proxy a proxy 
     *  @return <code> bookId </code> not found 
     *  @throws org.osid.NotFoundException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.NullArgumentException <code> bookId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentSmartBook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.CommentSmartBookSession getCommentSmartBookSession(org.osid.id.Id bookId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentSmartBookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book lookup 
     *  service. 
     *
     *  @return a <code> BookLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookLookupSession getBookLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getBookLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookLookupSession getBookLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getBookLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book query 
     *  service. 
     *
     *  @return a <code> BookQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookQuerySession getBookQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getBookQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookQueryh() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookQuerySession getBookQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getBookQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book search 
     *  service. 
     *
     *  @return a <code> BookSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookSearchSession getBookSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getBookSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookSearchSession getBookSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getBookSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book 
     *  administrative service. 
     *
     *  @return a <code> BookAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookAdminSession getBookAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getBookAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookAdminSession getBookAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getBookAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book 
     *  notification service. 
     *
     *  @param  bookReceiver the receiver 
     *  @return a <code> BookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> bookReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBookNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookNotificationSession getBookNotificationSession(org.osid.commenting.BookReceiver bookReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getBookNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book 
     *  notification service. 
     *
     *  @param  bookReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> BookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> bookReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBookNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookNotificationSession getBookNotificationSession(org.osid.commenting.BookReceiver bookReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getBookNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book hierarchy 
     *  service. 
     *
     *  @return a <code> BookHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookHierarchySession getBookHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getBookHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book hierarchy 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBookHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookHierarchySession getBookHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getBookHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book hierarchy 
     *  design service. 
     *
     *  @return a <code> BookHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBookHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookHierarchyDesignSession getBookHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getBookHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the book hierarchy 
     *  design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BookHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBookHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.BookHierarchyDesignSession getBookHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getBookHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> CommentingBatchManager. </code> 
     *
     *  @return a <code> CommentingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.batch.CommentingBatchManager getCommentingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingManager.getCommentingBatchManager not implemented");
    }


    /**
     *  Gets a <code> CommentingBatchProxyManager. </code> 
     *
     *  @return a <code> CommentingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCommentingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.commenting.batch.CommentingBatchProxyManager getCommentingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.commenting.CommentingProxyManager.getCommentingBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.commentRecordTypes.clear();
        this.commentRecordTypes.clear();

        this.commentSearchRecordTypes.clear();
        this.commentSearchRecordTypes.clear();

        this.bookRecordTypes.clear();
        this.bookRecordTypes.clear();

        this.bookSearchRecordTypes.clear();
        this.bookSearchRecordTypes.clear();

        return;
    }
}
