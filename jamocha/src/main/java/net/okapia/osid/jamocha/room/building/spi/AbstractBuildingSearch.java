//
// AbstractBuildingSearch.java
//
//     A template for making a Building Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.building.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing building searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBuildingSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.room.BuildingSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.room.records.BuildingSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.room.BuildingSearchOrder buildingSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of buildings. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  buildingIds list of buildings
     *  @throws org.osid.NullArgumentException
     *          <code>buildingIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBuildings(org.osid.id.IdList buildingIds) {
        while (buildingIds.hasNext()) {
            try {
                this.ids.add(buildingIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBuildings</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of building Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBuildingIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  buildingSearchOrder building search order 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>buildingSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBuildingResults(org.osid.room.BuildingSearchOrder buildingSearchOrder) {
	this.buildingSearchOrder = buildingSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.room.BuildingSearchOrder getBuildingSearchOrder() {
	return (this.buildingSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given building search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a building implementing the requested record.
     *
     *  @param buildingSearchRecordType a building search record
     *         type
     *  @return the building search record
     *  @throws org.osid.NullArgumentException
     *          <code>buildingSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(buildingSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.BuildingSearchRecord getBuildingSearchRecord(org.osid.type.Type buildingSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.room.records.BuildingSearchRecord record : this.records) {
            if (record.implementsRecordType(buildingSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(buildingSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this building search. 
     *
     *  @param buildingSearchRecord building search record
     *  @param buildingSearchRecordType building search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBuildingSearchRecord(org.osid.room.records.BuildingSearchRecord buildingSearchRecord, 
                                           org.osid.type.Type buildingSearchRecordType) {

        addRecordType(buildingSearchRecordType);
        this.records.add(buildingSearchRecord);        
        return;
    }
}
