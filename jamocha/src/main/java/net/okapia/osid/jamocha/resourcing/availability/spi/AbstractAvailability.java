//
// AbstractAvailability.java
//
//     Defines an Availability.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.availability.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.percentarg;


/**
 *  Defines an <code>Availability</code>.
 */

public abstract class AbstractAvailability
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.resourcing.Availability {

    private org.osid.resource.Resource resource;
    private org.osid.resourcing.Job job;
    private org.osid.resourcing.Competency competency;
    private long percentage;

    private final java.util.Collection<org.osid.resourcing.records.AvailabilityRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the resource <code> Id. </code> 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException
     *          <code>resource</code> is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the job <code> Id. </code> 
     *
     *  @return the job <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getJobId() {
        return (this.job.getId());
    }


    /**
     *  Gets the job. 
     *
     *  @return the job 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Job getJob()
        throws org.osid.OperationFailedException {

        return (this.job);
    }


    /**
     *  Sets the job.
     *
     *  @param job a job
     *  @throws org.osid.NullArgumentException
     *          <code>job</code> is <code>null</code>
     */

    protected void setJob(org.osid.resourcing.Job job) {
        nullarg(job, "job");
        this.job = job;
        return;
    }


    /**
     *  Tests if a competency is specified for this availability. 
     *
     *  @return <code> true </code> if a competency is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isCompetent() {
        return (this.competency != null);
    }


    /**
     *  Gets the competency <code> Id. </code> 
     *
     *  @return the competency <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isCompetent() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCompetencyId() {
        if (!isCompetent()) {
            throw new org.osid.IllegalStateException("isCompetent() is false");
        }

        return (this.competency.getId());
    }


    /**
     *  Gets the competency. 
     *
     *  @return the competency 
     *  @throws org.osid.IllegalStateException <code> isCompetent() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resourcing.Competency getCompetency()
        throws org.osid.OperationFailedException {

        if (!isCompetent()) {
            throw new org.osid.IllegalStateException("isCompetent() is false");
        }

        return (this.competency);
    }


    /**
     *  Sets the competency.
     *
     *  @param competency a competency
     *  @throws org.osid.NullArgumentException
     *          <code>competency</code> is <code>null</code>
     */

    protected void setCompetency(org.osid.resourcing.Competency competency) {
        nullarg(competency, "competency");
        this.competency = competency;
        return;
    }


    /**
     *  Gets the percentage availability. 
     *
     *  @return the percentage availability 
     */

    @OSID @Override
    public long getPercentage() {
        return (this.percentage);
    }


    /**
     *  Sets the percentage.
     *
     *  @param percentage a percentage
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    protected void setPercentage(long percentage) {
        percentarg(percentage, "percentage");
        this.percentage = percentage;
        return;
    }


    /**
     *  Tests if this availability supports the given record
     *  <code>Type</code>.
     *
     *  @param  availabilityRecordType an availability record type 
     *  @return <code>true</code> if the availabilityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type availabilityRecordType) {
        for (org.osid.resourcing.records.AvailabilityRecord record : this.records) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Availability</code> record <code>Type</code>.
     *
     *  @param  availabilityRecordType the availability record type 
     *  @return the availability record 
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.AvailabilityRecord getAvailabilityRecord(org.osid.type.Type availabilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.records.AvailabilityRecord record : this.records) {
            if (record.implementsRecordType(availabilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this availability. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param availabilityRecord the availability record
     *  @param availabilityRecordType availability record type
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityRecord</code> or
     *          <code>availabilityRecordTypeavailability</code> is
     *          <code>null</code>
     */
            
    protected void addAvailabilityRecord(org.osid.resourcing.records.AvailabilityRecord availabilityRecord, 
                                         org.osid.type.Type availabilityRecordType) {
        
        nullarg(availabilityRecord, "availability record");
        addRecordType(availabilityRecordType);
        this.records.add(availabilityRecord);
        
        return;
    }
}
