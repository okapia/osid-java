//
// InvariantMapProxyResponseLookupSession
//
//    Implements a Response lookup service backed by a fixed
//    collection of responses. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.inquiry;


/**
 *  Implements a Response lookup service backed by a fixed
 *  collection of responses. The responses are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyResponseLookupSession
    extends net.okapia.osid.jamocha.core.inquiry.spi.AbstractMapResponseLookupSession
    implements org.osid.inquiry.ResponseLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyResponseLookupSession} with no
     *  responses.
     *
     *  @param inquest the inquest
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyResponseLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.proxy.Proxy proxy) {
        setInquest(inquest);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyResponseLookupSession} with a single
     *  response.
     *
     *  @param inquest the inquest
     *  @param response a single response
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code response} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyResponseLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.inquiry.Response response, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putResponse(response);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyResponseLookupSession} using
     *  an array of responses.
     *
     *  @param inquest the inquest
     *  @param responses an array of responses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code responses} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyResponseLookupSession(org.osid.inquiry.Inquest inquest,
                                                  org.osid.inquiry.Response[] responses, org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putResponses(responses);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyResponseLookupSession} using a
     *  collection of responses.
     *
     *  @param inquest the inquest
     *  @param responses a collection of responses
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code inquest},
     *          {@code responses} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyResponseLookupSession(org.osid.inquiry.Inquest inquest,
                                                  java.util.Collection<? extends org.osid.inquiry.Response> responses,
                                                  org.osid.proxy.Proxy proxy) {

        this(inquest, proxy);
        putResponses(responses);
        return;
    }
}
