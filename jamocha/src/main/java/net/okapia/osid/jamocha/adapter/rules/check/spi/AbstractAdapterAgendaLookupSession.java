//
// AbstractAdapterAgendaLookupSession.java
//
//    An Agenda lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.rules.check.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  An Agenda lookup session adapter.
 */

public abstract class AbstractAdapterAgendaLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.rules.check.AgendaLookupSession {

    private final org.osid.rules.check.AgendaLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterAgendaLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAgendaLookupSession(org.osid.rules.check.AgendaLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Engine/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Engine Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.session.getEngineId());
    }


    /**
     *  Gets the {@code Engine} associated with this session.
     *
     *  @return the {@code Engine} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getEngine());
    }


    /**
     *  Tests if this user can perform {@code Agenda} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupAgendas() {
        return (this.session.canLookupAgendas());
    }


    /**
     *  A complete view of the {@code Agenda} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAgendaView() {
        this.session.useComparativeAgendaView();
        return;
    }


    /**
     *  A complete view of the {@code Agenda} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAgendaView() {
        this.session.usePlenaryAgendaView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agendas in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.session.useFederatedEngineView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this engine only.
     */

    @OSID @Override
    public void useIsolatedEngineView() {
        this.session.useIsolatedEngineView();
        return;
    }
    
     
    /**
     *  Gets the {@code Agenda} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Agenda} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Agenda} and
     *  retained for compatibility.
     *
     *  @param agendaId {@code Id} of the {@code Agenda}
     *  @return the agenda
     *  @throws org.osid.NotFoundException {@code agendaId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code agendaId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Agenda getAgenda(org.osid.id.Id agendaId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgenda(agendaId));
    }


    /**
     *  Gets an {@code AgendaList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  agendas specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Agendas} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  agendaIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Agenda} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code agendaIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByIds(org.osid.id.IdList agendaIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgendasByIds(agendaIds));
    }


    /**
     *  Gets an {@code AgendaList} corresponding to the given
     *  agenda genus {@code Type} which does not include
     *  agendas of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  agendas or an error results. Otherwise, the returned list
     *  may contain only those agendas that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agendaGenusType an agenda genus type 
     *  @return the returned {@code Agenda} list
     *  @throws org.osid.NullArgumentException
     *          {@code agendaGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByGenusType(org.osid.type.Type agendaGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgendasByGenusType(agendaGenusType));
    }


    /**
     *  Gets an {@code AgendaList} corresponding to the given
     *  agenda genus {@code Type} and include any additional
     *  agendas with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  agendas or an error results. Otherwise, the returned list
     *  may contain only those agendas that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agendaGenusType an agenda genus type 
     *  @return the returned {@code Agenda} list
     *  @throws org.osid.NullArgumentException
     *          {@code agendaGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByParentGenusType(org.osid.type.Type agendaGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgendasByParentGenusType(agendaGenusType));
    }


    /**
     *  Gets an {@code AgendaList} containing the given
     *  agenda record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  agendas or an error results. Otherwise, the returned list
     *  may contain only those agendas that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agendaRecordType an agenda record type 
     *  @return the returned {@code Agenda} list
     *  @throws org.osid.NullArgumentException
     *          {@code agendaRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByRecordType(org.osid.type.Type agendaRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgendasByRecordType(agendaRecordType));
    }


    /**
     *  Gets all {@code Agendas}. 
     *
     *  In plenary mode, the returned list contains all known
     *  agendas or an error results. Otherwise, the returned list
     *  may contain only those agendas that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Agendas} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendas()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getAgendas());
    }
}
