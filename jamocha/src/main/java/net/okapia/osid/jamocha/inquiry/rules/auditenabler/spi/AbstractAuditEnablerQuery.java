//
// AbstractAuditEnablerQuery.java
//
//     A template for making an AuditEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inquiry.rules.auditenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for audit enablers.
 */

public abstract class AbstractAuditEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.inquiry.rules.AuditEnablerQuery {

    private final java.util.Collection<org.osid.inquiry.rules.records.AuditEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the audit. 
     *
     *  @param  auditId the audit <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auditId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuditId(org.osid.id.Id auditId, boolean match) {
        return;
    }


    /**
     *  Clears the audit <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuditIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuditQuery </code> is available. 
     *
     *  @return <code> true </code> if an audit query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuditQuery() {
        return (false);
    }


    /**
     *  Gets the query for an audit. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the audit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuditQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.AuditQuery getRuledAuditQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuditQuery() is false");
    }


    /**
     *  Matches enablers mapped to any audit. 
     *
     *  @param  match <code> true </code> for enablers mapped to any audit, 
     *          <code> false </code> to match enablers mapped to no audits 
     */

    @OSID @Override
    public void matchAnyRuledAudit(boolean match) {
        return;
    }


    /**
     *  Clears the audit query terms. 
     */

    @OSID @Override
    public void clearRuledAuditTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the inquest. 
     *
     *  @param  inquestId the inquest <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> inquestId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInquestId(org.osid.id.Id inquestId, boolean match) {
        return;
    }


    /**
     *  Clears the inquest <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearInquestIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> InquestQuery </code> is available. 
     *
     *  @return <code> true </code> if an inquest query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInquestQuery() {
        return (false);
    }


    /**
     *  Gets the query for an inquest. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the inquest query 
     *  @throws org.osid.UnimplementedException <code> supportsInquestQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inquiry.InquestQuery getInquestQuery() {
        throw new org.osid.UnimplementedException("supportsInquestQuery() is false");
    }


    /**
     *  Clears the inquest query terms. 
     */

    @OSID @Override
    public void clearInquestTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given audit enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an audit enabler implementing the requested record.
     *
     *  @param auditEnablerRecordType an audit enabler record type
     *  @return the audit enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>auditEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auditEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.inquiry.rules.records.AuditEnablerQueryRecord getAuditEnablerQueryRecord(org.osid.type.Type auditEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inquiry.rules.records.AuditEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(auditEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auditEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this audit enabler query. 
     *
     *  @param auditEnablerQueryRecord audit enabler query record
     *  @param auditEnablerRecordType auditEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuditEnablerQueryRecord(org.osid.inquiry.rules.records.AuditEnablerQueryRecord auditEnablerQueryRecord, 
                                          org.osid.type.Type auditEnablerRecordType) {

        addRecordType(auditEnablerRecordType);
        nullarg(auditEnablerQueryRecord, "audit enabler query record");
        this.records.add(auditEnablerQueryRecord);        
        return;
    }
}
