//
// AbstractImmutableTemporal
//
//     Defines an immutable wrapper for a Temporal OsidObject.
//
//
// Tom Coppeto
// Okapia
// 8 december 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an immutable wrapper for a Temporal OsidObject.
 */

public abstract class AbstractImmutableTemporal
    implements org.osid.Temporal {

    private final org.osid.Temporal temporal;


    /**
     *  Constructs a new <code>AbstractImmutableTemporal</code>.
     *
     *  @param temporal
     *  @throws org.osid.NullArgumentException <code>temporal</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableTemporal(org.osid.Temporal temporal) {
        nullarg(temporal, "temporal");
        this.temporal = temporal;
        return;
    }


    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        return (this.temporal.isEffective());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.temporal.getStartDate());
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.temporal.getEndDate());
    }


    /**
     *  Determines if the given <code> Id </code> is equal to this
     *  one. Two Ids are equal if the namespace, authority and
     *  identifier components are equal. The identifier is case
     *  sensitive while the namespace and authority strings are not
     *  case sensitive.
     *
     *  @param  obj an temporal to compare
     *  @return <code> true </code> if the given temporal is equal to
     *          this <code>Id</code>, <code> false </code> otherwise
     */

    @Override
    public boolean equals(Object obj) {
        return (this.temporal.equals(obj));
    }


    /**
     *  Returns a hash code value for this <code>Temporal</code>
     *  based on the <code>Id</code>.
     *
     *  @return a hash code value for this temporal
     */

    @Override
    public int hashCode() {
        return (this.temporal.hashCode());
    }


    /**
     *  Returns a string representation of this Temporal.
     *
     *  @return a string
     */

    @Override
    public String toString() {
        return (this.temporal.toString());
    }
}
