//
// AbstractCatalogEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.rules.catalogenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCatalogEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.cataloging.rules.CatalogEnablerSearchResults {

    private org.osid.cataloging.rules.CatalogEnablerList catalogEnablers;
    private final org.osid.cataloging.rules.CatalogEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.cataloging.rules.records.CatalogEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCatalogEnablerSearchResults.
     *
     *  @param catalogEnablers the result set
     *  @param catalogEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>catalogEnablers</code>
     *          or <code>catalogEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCatalogEnablerSearchResults(org.osid.cataloging.rules.CatalogEnablerList catalogEnablers,
                                            org.osid.cataloging.rules.CatalogEnablerQueryInspector catalogEnablerQueryInspector) {
        nullarg(catalogEnablers, "catalog enablers");
        nullarg(catalogEnablerQueryInspector, "catalog enabler query inspectpr");

        this.catalogEnablers = catalogEnablers;
        this.inspector = catalogEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the catalog enabler list resulting from a search.
     *
     *  @return a catalog enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.cataloging.rules.CatalogEnablerList getCatalogEnablers() {
        if (this.catalogEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.cataloging.rules.CatalogEnablerList catalogEnablers = this.catalogEnablers;
        this.catalogEnablers = null;
	return (catalogEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.cataloging.rules.CatalogEnablerQueryInspector getCatalogEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  catalog enabler search record <code> Type. </code> This method must
     *  be used to retrieve a catalogEnabler implementing the requested
     *  record.
     *
     *  @param catalogEnablerSearchRecordType a catalogEnabler search 
     *         record type 
     *  @return the catalog enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(catalogEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.rules.records.CatalogEnablerSearchResultsRecord getCatalogEnablerSearchResultsRecord(org.osid.type.Type catalogEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.cataloging.rules.records.CatalogEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(catalogEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(catalogEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record catalog enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCatalogEnablerRecord(org.osid.cataloging.rules.records.CatalogEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "catalog enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
