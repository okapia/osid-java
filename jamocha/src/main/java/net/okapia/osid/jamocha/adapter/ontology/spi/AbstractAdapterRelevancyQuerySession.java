//
// AbstractQueryRelevancyLookupSession.java
//
//    A RelevancyQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.ontology.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RelevancyQuerySession adapter.
 */

public abstract class AbstractAdapterRelevancyQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.ontology.RelevancyQuerySession {

    private final org.osid.ontology.RelevancyQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterRelevancyQuerySession.
     *
     *  @param session the underlying relevancy query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRelevancyQuerySession(org.osid.ontology.RelevancyQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeOntology</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeOntology Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOntologyId() {
        return (this.session.getOntologyId());
    }


    /**
     *  Gets the {@codeOntology</code> associated with this 
     *  session.
     *
     *  @return the {@codeOntology</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ontology.Ontology getOntology()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOntology());
    }


    /**
     *  Tests if this user can perform {@codeRelevancy</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchRelevancies() {
        return (this.session.canSearchRelevancies());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include relevancies in ontologies which are children
     *  of this ontology in the ontology hierarchy.
     */

    @OSID @Override
    public void useFederatedOntologyView() {
        this.session.useFederatedOntologyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this ontology only.
     */
    
    @OSID @Override
    public void useIsolatedOntologyView() {
        this.session.useIsolatedOntologyView();
        return;
    }
    
      
    /**
     *  Gets a relevancy query. The returned query will not have an
     *  extension query.
     *
     *  @return the relevancy query 
     */
      
    @OSID @Override
    public org.osid.ontology.RelevancyQuery getRelevancyQuery() {
        return (this.session.getRelevancyQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  relevancyQuery the relevancy query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code relevancyQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code relevancyQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.ontology.RelevancyList getRelevanciesByQuery(org.osid.ontology.RelevancyQuery relevancyQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getRelevanciesByQuery(relevancyQuery));
    }
}
