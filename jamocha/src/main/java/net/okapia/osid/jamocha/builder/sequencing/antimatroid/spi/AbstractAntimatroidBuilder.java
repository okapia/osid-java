//
// AbstractAntimatroid.java
//
//     Defines an Antimatroid builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.sequencing.antimatroid.spi;


/**
 *  Defines an <code>Antimatroid</code> builder.
 */

public abstract class AbstractAntimatroidBuilder<T extends AbstractAntimatroidBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidCatalogBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.sequencing.antimatroid.AntimatroidMiter antimatroid;


    /**
     *  Constructs a new <code>AbstractAntimatroidBuilder</code>.
     *
     *  @param antimatroid the antimatroid to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractAntimatroidBuilder(net.okapia.osid.jamocha.builder.sequencing.antimatroid.AntimatroidMiter antimatroid) {
        super(antimatroid);
        this.antimatroid = antimatroid;
        return;
    }


    /**
     *  Builds the antimatroid.
     *
     *  @return the new antimatroid
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>antimatroid</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.sequencing.Antimatroid build() {
        (new net.okapia.osid.jamocha.builder.validator.sequencing.antimatroid.AntimatroidValidator(getValidations())).validate(this.antimatroid);
        return (new net.okapia.osid.jamocha.builder.sequencing.antimatroid.ImmutableAntimatroid(this.antimatroid));
    }


    /**
     *  Gets the antimatroid. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new antimatroid
     */

    @Override
    public net.okapia.osid.jamocha.builder.sequencing.antimatroid.AntimatroidMiter getMiter() {
        return (this.antimatroid);
    }


    /**
     *  Adds an Antimatroid record.
     *
     *  @param record an antimatroid record
     *  @param recordType the type of antimatroid record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.sequencing.records.AntimatroidRecord record, org.osid.type.Type recordType) {
        getMiter().addAntimatroidRecord(record, recordType);
        return (self());
    }
}       


