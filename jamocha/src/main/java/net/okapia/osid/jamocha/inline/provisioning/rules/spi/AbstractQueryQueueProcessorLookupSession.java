//
// AbstractQueryQueueProcessorLookupSession.java
//
//    An inline adapter that maps a QueueProcessorLookupSession to
//    a QueueProcessorQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a QueueProcessorLookupSession to
 *  a QueueProcessorQuerySession.
 */

public abstract class AbstractQueryQueueProcessorLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractQueueProcessorLookupSession
    implements org.osid.provisioning.rules.QueueProcessorLookupSession {

    private boolean activeonly    = false;
    private final org.osid.provisioning.rules.QueueProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryQueueProcessorLookupSession.
     *
     *  @param querySession the underlying queue processor query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryQueueProcessorLookupSession(org.osid.provisioning.rules.QueueProcessorQuerySession querySession) {
        nullarg(querySession, "queue processor query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>QueueProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupQueueProcessors() {
        return (this.session.canSearchQueueProcessors());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include queue processors in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only active queue processors are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveQueueProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive queue processors are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusQueueProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>QueueProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>QueueProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>QueueProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, queue processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processors are returned.
     *
     *  @param  queueProcessorId <code>Id</code> of the
     *          <code>QueueProcessor</code>
     *  @return the queue processor
     *  @throws org.osid.NotFoundException <code>queueProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>queueProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessor getQueueProcessor(org.osid.id.Id queueProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueProcessorQuery query = getQuery();
        query.matchId(queueProcessorId, true);
        org.osid.provisioning.rules.QueueProcessorList queueProcessors = this.session.getQueueProcessorsByQuery(query);
        if (queueProcessors.hasNext()) {
            return (queueProcessors.getNextQueueProcessor());
        } 
        
        throw new org.osid.NotFoundException(queueProcessorId + " not found");
    }


    /**
     *  Gets a <code>QueueProcessorList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  queueProcessors specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>QueueProcessors</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, queue processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processors are returned.
     *
     *  @param  queueProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>QueueProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByIds(org.osid.id.IdList queueProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueProcessorQuery query = getQuery();

        try (org.osid.id.IdList ids = queueProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getQueueProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>QueueProcessorList</code> corresponding to the
     *  given queue processor genus <code>Type</code> which does not
     *  include queue processors of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those queue processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processors are returned.
     *
     *  @param  queueProcessorGenusType a queueProcessor genus type 
     *  @return the returned <code>QueueProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByGenusType(org.osid.type.Type queueProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueProcessorQuery query = getQuery();
        query.matchGenusType(queueProcessorGenusType, true);
        return (this.session.getQueueProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>QueueProcessorList</code> corresponding to the
     *  given queue processor genus <code>Type</code> and include any
     *  additional queue processors with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known queue
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those queue processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processors are returned.
     *
     *  @param  queueProcessorGenusType a queueProcessor genus type 
     *  @return the returned <code>QueueProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByParentGenusType(org.osid.type.Type queueProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueProcessorQuery query = getQuery();
        query.matchParentGenusType(queueProcessorGenusType, true);
        return (this.session.getQueueProcessorsByQuery(query));
    }


    /**
     *  Gets a <code>QueueProcessorList</code> containing the given
     *  queue processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known queue
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those queue processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processors are returned.
     *
     *  @param  queueProcessorRecordType a queueProcessor record type 
     *  @return the returned <code>QueueProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessorsByRecordType(org.osid.type.Type queueProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueProcessorQuery query = getQuery();
        query.matchRecordType(queueProcessorRecordType, true);
        return (this.session.getQueueProcessorsByQuery(query));
    }

    
    /**
     *  Gets all <code>QueueProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known queue
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those queue processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, queue processors are returned that are
     *  currently active. In any status mode, active and inactive
     *  queue processors are returned.
     *
     *  @return a list of <code>QueueProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.rules.QueueProcessorQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getQueueProcessorsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.rules.QueueProcessorQuery getQuery() {
        org.osid.provisioning.rules.QueueProcessorQuery query = this.session.getQueueProcessorQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
