//
// AbstractAuctionConstrainerEnablerQuery.java
//
//     A template for making an AuctionConstrainerEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for auction constrainer enablers.
 */

public abstract class AbstractAuctionConstrainerEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.bidding.rules.AuctionConstrainerEnablerQuery {

    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the auction constrainer. 
     *
     *  @param  auctionConstrainerId the auction constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAuctionConstrainerId(org.osid.id.Id auctionConstrainerId, 
                                               boolean match) {
        return;
    }


    /**
     *  Clears the auction constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionConstrainerIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AuctionConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if an auction constrainer query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAuctionConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for an auction constrainer. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the auction constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAuctionConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerQuery getRuledAuctionConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAuctionConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any auction constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any auction 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no auction constrainers 
     */

    @OSID @Override
    public void matchAnyRuledAuctionConstrainer(boolean match) {
        return;
    }


    /**
     *  Clears the auction constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledAuctionConstrainerTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the auction house. 
     *
     *  @param  auctionHouseId the auction house <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> auctionHouseId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchAuctionHouseId(org.osid.id.Id auctionHouseId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the auction house <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> AuctionHouseQuery </code> is available. 
     *
     *  @return <code> true </code> if a auction house query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAuctionHouseQuery() {
        return (false);
    }


    /**
     *  Gets the query for a auction house. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the auction house query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAuctionHouseQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouseQuery getAuctionHouseQuery() {
        throw new org.osid.UnimplementedException("supportsAuctionHouseQuery() is false");
    }


    /**
     *  Clears the auction house query terms. 
     */

    @OSID @Override
    public void clearAuctionHouseTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given auction constrainer enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction constrainer enabler implementing the requested record.
     *
     *  @param auctionConstrainerEnablerRecordType an auction constrainer enabler record type
     *  @return the auction constrainer enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryRecord getAuctionConstrainerEnablerQueryRecord(org.osid.type.Type auctionConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(auctionConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction constrainer enabler query. 
     *
     *  @param auctionConstrainerEnablerQueryRecord auction constrainer enabler query record
     *  @param auctionConstrainerEnablerRecordType auctionConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionConstrainerEnablerQueryRecord(org.osid.bidding.rules.records.AuctionConstrainerEnablerQueryRecord auctionConstrainerEnablerQueryRecord, 
                                          org.osid.type.Type auctionConstrainerEnablerRecordType) {

        addRecordType(auctionConstrainerEnablerRecordType);
        nullarg(auctionConstrainerEnablerQueryRecord, "auction constrainer enabler query record");
        this.records.add(auctionConstrainerEnablerQueryRecord);        
        return;
    }
}
