//
// AbstractQueryAgendaLookupSession.java
//
//    An AgendaQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AgendaQuerySession adapter.
 */

public abstract class AbstractAdapterAgendaQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.rules.check.AgendaQuerySession {

    private final org.osid.rules.check.AgendaQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterAgendaQuerySession.
     *
     *  @param session the underlying agenda query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterAgendaQuerySession(org.osid.rules.check.AgendaQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeEngine</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeEngine Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getEngineId() {
        return (this.session.getEngineId());
    }


    /**
     *  Gets the {@codeEngine</code> associated with this 
     *  session.
     *
     *  @return the {@codeEngine</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.Engine getEngine()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getEngine());
    }


    /**
     *  Tests if this user can perform {@codeAgenda</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchAgendas() {
        return (this.session.canSearchAgendas());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agendas in engines which are children
     *  of this engine in the engine hierarchy.
     */

    @OSID @Override
    public void useFederatedEngineView() {
        this.session.useFederatedEngineView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this engine only.
     */
    
    @OSID @Override
    public void useIsolatedEngineView() {
        this.session.useIsolatedEngineView();
        return;
    }
    
      
    /**
     *  Gets an agenda query. The returned query will not have an
     *  extension query.
     *
     *  @return the agenda query 
     */
      
    @OSID @Override
    public org.osid.rules.check.AgendaQuery getAgendaQuery() {
        return (this.session.getAgendaQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  agendaQuery the agenda query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code agendaQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code agendaQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendasByQuery(org.osid.rules.check.AgendaQuery agendaQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getAgendasByQuery(agendaQuery));
    }
}
