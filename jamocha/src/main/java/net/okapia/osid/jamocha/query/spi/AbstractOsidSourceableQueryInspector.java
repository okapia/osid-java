//
// AbstractOsidSourceableQueryInspector.java
//
//     Defines an OsidSourceableQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 1 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.query.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a simple OsidQueryInspector to extend. 
 */

public abstract class AbstractOsidSourceableQueryInspector
    extends AbstractOsidQueryInspector
    implements org.osid.OsidSourceableQueryInspector {
    
    private final java.util.Collection<org.osid.search.terms.StringTerm> licenseTerms = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.search.terms.IdTerm> providerIdTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.search.terms.IdTerm> brandingIdTerms = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.resource.ResourceQueryInspector> providerTerms = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.repository.AssetQueryInspector> brandingTerms = new java.util.LinkedHashSet<>();


    /**
     *  Gets the provider <code>Id</code> query terms.
     *
     *  @return the provider <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProviderIdTerms() {
        return (this.providerIdTerms.toArray(new org.osid.search.terms.IdTerm[this.providerIdTerms.size()]));
    }


    /**
     *  Adds a provider <code>Id</code> term.
     *
     *  @param term a provider <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addProviderIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "provider Id term");
        this.providerIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of provider <code>Id</code> terms.
     *
     *  @param terms a collection of provider <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addProviderIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "provider Id terms");
        this.providerIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a provider <code>Id</code> term.
     *
     *  @param resourceId the provider <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>resourceId</code>
     *          or is <code>null</code>
     */

    protected void addProviderIdTerm(org.osid.id.Id resourceId, boolean match) {
        this.providerIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(resourceId, match));
        return;
    }


    /**
     *  Gets the provider query terms.
     *
     *  @return the provider terms
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getProviderTerms() {
        return (this.providerTerms.toArray(new org.osid.resource.ResourceQueryInspector[this.providerTerms.size()]));
    }

    
    /**
     *  Adds a provider term.
     *
     *  @param term a provider term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addProviderTerm(org.osid.resource.ResourceQueryInspector term) {
        nullarg(term, "provider term");
        this.providerTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of provider terms.
     *
     *  @param terms a collection of provider terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addProviderTerms(java.util.Collection<org.osid.resource.ResourceQueryInspector> terms) {
        nullarg(terms, "provider terms");
        this.providerTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the asset <code>Id</code> query terms.
     *
     *  @return the asset <code>Id</code> terms
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getBrandingIdTerms() {
        return (this.brandingIdTerms.toArray(new org.osid.search.terms.IdTerm[this.brandingIdTerms.size()]));
    }

    
    /**
     *  Adds an asset <code>Id</code> term.
     *
     *  @param term an asset <code>Id</code> term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addBrandingIdTerm(org.osid.search.terms.IdTerm term) {
        nullarg(term, "asset Id term");
        this.brandingIdTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of asset <code>Id</code> terms.
     *
     *  @param terms a collection of asset <code>Id</code> terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addBrandingIdTerms(java.util.Collection<org.osid.search.terms.IdTerm> terms) {
        nullarg(terms, "asset Id terms");
        this.brandingIdTerms.addAll(terms);
        return;
    }


    /**
     *  Adds an asset <code>Id</code> term.
     *
     *  @param assetId the asset <code>Id</code>
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>assetId</code>
     *          or is <code>null</code>
     */

    protected void addBrandingIdTerm(org.osid.id.Id assetId, boolean match) {
        this.brandingIdTerms.add(new net.okapia.osid.primordium.terms.IdTerm(assetId, match));
        return;
    }


    /**
     *  Gets the asset query terms.
     *
     *  @return the asset terms
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getBrandingTerms() {
        return (this.brandingTerms.toArray(new org.osid.repository.AssetQueryInspector[this.brandingTerms.size()]));
    }

    
    /**
     *  Adds an asset term.
     *
     *  @param term an asset term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addBrandingTerm(org.osid.repository.AssetQueryInspector term) {
        nullarg(term, "asset term");
        this.brandingTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of asset terms.
     *
     *  @param terms a collection of asset terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addBrandingTerms(java.util.Collection<org.osid.repository.AssetQueryInspector> terms) {
        nullarg(terms, "asset terms");
        this.brandingTerms.addAll(terms);
        return;
    }


    /**
     *  Gets the license query terms.
     *
     *  @return the license terms
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getLicenseTerms() {
        return (this.licenseTerms.toArray(new org.osid.search.terms.StringTerm[this.licenseTerms.size()]));
    }

    
    /**
     *  Adds a license term.
     *
     *  @param term a license term
     *  @throws org.osid.NullArgumentException <code>term</code> is 
     *          <code>null</code>
     */

    protected void addLicenseTerm(org.osid.search.terms.StringTerm term) {
        nullarg(term, "license term");
        this.licenseTerms.add(term);
        return;
    }


    /**
     *  Adds a collection of license terms.
     *
     *  @param terms a collection of license terms
     *  @throws org.osid.NullArgumentException <code>terms</code> is 
     *          <code>null</code>
     */

    protected void addLicenseTerms(java.util.Collection<org.osid.search.terms.StringTerm> terms) {
        nullarg(terms, "license terms");
        this.licenseTerms.addAll(terms);
        return;
    }


    /**
     *  Adds a license term.
     *
     *  @param license the license
     *  @param stringMatchType a string match type
     *  @param match <code>true</code> for a positive match,
     *         <code>false</code> for a negative match
     *  @throws org.osid.NullArgumentException <code>id</code> or
     *          <code>stringMatchType</code> is <code>null</code>
     */

    protected void addLicenseTerm(String license, org.osid.type.Type stringMatchType, boolean match) {
        this.licenseTerms.add(new net.okapia.osid.primordium.terms.StringTerm(license, 
                                                                              stringMatchType, 
                                                                              match));
        return;
    }
}
