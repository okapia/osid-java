//
// AbstractBid.java
//
//     Defines a Bid.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.bid.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Bid</code>.
 */

public abstract class AbstractBid
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.bidding.Bid {

    private org.osid.bidding.Auction auction;
    private org.osid.resource.Resource bidder;
    private org.osid.authentication.Agent biddingAgent;
    private long quantity;
    private org.osid.financials.Currency currentBid;
    private org.osid.financials.Currency maximumBid;
    private boolean winner = false;
    private org.osid.financials.Currency settlementAmount;

    private final java.util.Collection<org.osid.bidding.records.BidRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the auction. 
     *
     *  @return the auction <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAuctionId() {
        return (this.auction.getId());
    }


    /**
     *  Gets the auction. 
     *
     *  @return the auction 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.bidding.Auction getAuction()
        throws org.osid.OperationFailedException {

        return (this.auction);
    }


    /**
     *  Sets the auction.
     *
     *  @param auction an auction
     *  @throws org.osid.NullArgumentException
     *          <code>auction</code> is <code>null</code>
     */

    protected void setAuction(org.osid.bidding.Auction auction) {
        nullarg(auction, "auction");
        this.auction = auction;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the bidder. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBidderId() {
        return (this.bidder.getId());
    }


    /**
     *  Gets the bidder. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getBidder()
        throws org.osid.OperationFailedException {

        return (this.bidder);
    }


    /**
     *  Sets the bidder.
     *
     *  @param bidder a bidder
     *  @throws org.osid.NullArgumentException <code>bidder</code> is
     *          <code>null</code>
     */

    protected void setBidder(org.osid.resource.Resource bidder) {
        nullarg(bidder, "bidder");
        this.bidder = bidder;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the bidding agent. 
     *
     *  @return the agent <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getBiddingAgentId() {
        return (this.biddingAgent.getId());
    }


    /**
     *  Gets the bidding agent. 
     *
     *  @return the agent 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getBiddingAgent()
        throws org.osid.OperationFailedException {

        return (this.biddingAgent);
    }


    /**
     *  Sets the bidding agent.
     *
     *  @param agent a bidding agent
     *  @throws org.osid.NullArgumentException
     *          <code>agent</code> is <code>null</code>
     */

    protected void setBiddingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "bidding agent");
        this.biddingAgent = agent;
        return;
    }


    /**
     *  Gets the quantity of items in this bid. 
     *
     *  @return the quantity of items 
     */

    @OSID @Override
    public long getQuantity() {
        return (this.quantity);
    }


    /**
     *  Sets the quantity.
     *
     *  @param quantity a quantity
     *  @throws org.osid.InvalidArgumentException
     *          <code>quantity</code> is negative
     */

    protected void setQuantity(long quantity) {
        cardinalarg(quantity, "quantity");
        this.quantity = quantity;
        return;
    }


    /**
     *  Gets the current bid amount. 
     *
     *  @return the current bid 
     */

    @OSID @Override
    public org.osid.financials.Currency getCurrentBid() {
        return (this.currentBid);
    }


    /**
     *  Sets the current bid.
     *
     *  @param bid a current bid
     *  @throws org.osid.NullArgumentException <code>bid</code> is
     *          <code>null</code>
     */

    protected void setCurrentBid(org.osid.financials.Currency bid) {
        nullarg(bid, "current bid");
        this.currentBid = bid;
        return;
    }


    /**
     *  Gets the maximum bid. For some auctions, the current bid is
     *  the maximum bid. For others, the current bid may be less then
     *  the maximum bid and automatically increment when greater bids
     *  are made up to the maximum bid.
     *
     *  @return the maximum bid 
     */

    @OSID @Override
    public org.osid.financials.Currency getMaximumBid() {
        return (this.maximumBid);
    }


    /**
     *  Sets the maximum bid.
     *
     *  @param bid a maximum bid
     *  @throws org.osid.NullArgumentException <code>bid</code> is
     *          <code>null</code>
     */

    protected void setMaximumBid(org.osid.financials.Currency bid) {
        nullarg(bid, "maximum bid");
        this.maximumBid = bid;
        return;
    }


    /**
     *  Tests if this was a winning bid.
     *
     *  @return <code> true </code> if this was a winnign bid, <code>
     *          false </code> otherwise
     */

    @OSID @Override
    public boolean isWinner() {
        return (this.winner);
    }


    /**
     *  Sets the winner flag.
     *
     *  @param winner <code> true </code> if this was a winnign bid,
     *         <code> false </code> otherwise
     */

    protected void setWinner(boolean winner) {
        this.winner = winner;
        return;
    }


    /**
     *  Gets the settlement amount owed from the bidder. 
     *
     *  @return the settlement amount 
     */

    @OSID @Override
    public org.osid.financials.Currency getSettlementAmount() {
        return (this.settlementAmount);
    }


    /**
     *  Sets the settlement amount.
     *
     *  @param amount a settlement amount
     *  @throws org.osid.NullArgumentException <code>amount</code> is
     *          <code>null</code>
     */

    protected void setSettlementAmount(org.osid.financials.Currency amount) {
        nullarg(amount, "settlement amount");
        this.settlementAmount = amount;
        return;
    }


    /**
     *  Tests if this bid supports the given record <code>Type</code>.
     *
     *  @param  bidRecordType a bid record type 
     *  @return <code>true</code> if the bidRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type bidRecordType) {
        for (org.osid.bidding.records.BidRecord record : this.records) {
            if (record.implementsRecordType(bidRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Bid</code> record <code>Type</code>.
     *
     *  @param  bidRecordType the bid record type 
     *  @return the bid record 
     *  @throws org.osid.NullArgumentException
     *          <code>bidRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(bidRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.records.BidRecord getBidRecord(org.osid.type.Type bidRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.bidding.records.BidRecord record : this.records) {
            if (record.implementsRecordType(bidRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(bidRecordType + " is not supported");
    }


    /**
     *  Adds a record to this bid. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param bidRecord the bid record
     *  @param bidRecordType bid record type
     *  @throws org.osid.NullArgumentException <code>bidRecord</code>
     *          or <code>bidRecordType</code> is <code>null</code>
     */
            
    protected void addBidRecord(org.osid.bidding.records.BidRecord bidRecord, 
                                org.osid.type.Type bidRecordType) {
        
        nullarg(bidRecord, "bid record");
        addRecordType(bidRecordType);
        this.records.add(bidRecord);
        
        return;
    }
}
