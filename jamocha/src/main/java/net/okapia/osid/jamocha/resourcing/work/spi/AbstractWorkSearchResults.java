//
// AbstractWorkSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.work.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractWorkSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.resourcing.WorkSearchResults {

    private org.osid.resourcing.WorkList works;
    private final org.osid.resourcing.WorkQueryInspector inspector;
    private final java.util.Collection<org.osid.resourcing.records.WorkSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractWorkSearchResults.
     *
     *  @param works the result set
     *  @param workQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>works</code>
     *          or <code>workQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractWorkSearchResults(org.osid.resourcing.WorkList works,
                                            org.osid.resourcing.WorkQueryInspector workQueryInspector) {
        nullarg(works, "works");
        nullarg(workQueryInspector, "work query inspectpr");

        this.works = works;
        this.inspector = workQueryInspector;

        return;
    }


    /**
     *  Gets the work list resulting from a search.
     *
     *  @return a work list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.resourcing.WorkList getWorks() {
        if (this.works == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.resourcing.WorkList works = this.works;
        this.works = null;
	return (works);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.resourcing.WorkQueryInspector getWorkQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  work search record <code> Type. </code> This method must
     *  be used to retrieve a work implementing the requested
     *  record.
     *
     *  @param workSearchRecordType a work search 
     *         record type 
     *  @return the work search
     *  @throws org.osid.NullArgumentException
     *          <code>workSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(workSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.WorkSearchResultsRecord getWorkSearchResultsRecord(org.osid.type.Type workSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.resourcing.records.WorkSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(workSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(workSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record work search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addWorkRecord(org.osid.resourcing.records.WorkSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "work record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
