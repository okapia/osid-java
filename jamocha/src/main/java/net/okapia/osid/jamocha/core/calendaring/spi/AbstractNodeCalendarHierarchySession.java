//
// AbstractNodeCalendarHierarchySession.java
//
//     Defines a Calendar hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a calendar hierarchy session for delivering a hierarchy
 *  of calendars using the CalendarNode interface.
 */

public abstract class AbstractNodeCalendarHierarchySession
    extends net.okapia.osid.jamocha.calendaring.spi.AbstractCalendarHierarchySession
    implements org.osid.calendaring.CalendarHierarchySession {

    private java.util.Collection<org.osid.calendaring.CalendarNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root calendar <code> Ids </code> in this hierarchy.
     *
     *  @return the root calendar <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootCalendarIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.calendaring.calendarnode.CalendarNodeToIdList(this.roots));
    }


    /**
     *  Gets the root calendars in the calendar hierarchy. A node
     *  with no parents is an orphan. While all calendar <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root calendars 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getRootCalendars()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.calendaring.calendarnode.CalendarNodeToCalendarList(new net.okapia.osid.jamocha.calendaring.calendarnode.ArrayCalendarNodeList(this.roots)));
    }


    /**
     *  Adds a root calendar node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootCalendar(org.osid.calendaring.CalendarNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root calendar nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootCalendars(java.util.Collection<org.osid.calendaring.CalendarNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root calendar node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootCalendar(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.calendaring.CalendarNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Calendar </code> has any parents. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @return <code> true </code> if the calendar has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentCalendars(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getCalendarNode(calendarId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  calendar.
     *
     *  @param  id an <code> Id </code> 
     *  @param  calendarId the <code> Id </code> of a calendar 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> calendarId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> calendarId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfCalendar(org.osid.id.Id id, org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.calendaring.CalendarNodeList parents = getCalendarNode(calendarId).getParentCalendarNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextCalendarNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given calendar. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @return the parent <code> Ids </code> of the calendar 
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentCalendarIds(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.calendaring.calendar.CalendarToIdList(getParentCalendars(calendarId)));
    }


    /**
     *  Gets the parents of the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> to query 
     *  @return the parents of the calendar 
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getParentCalendars(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.calendaring.calendarnode.CalendarNodeToCalendarList(getCalendarNode(calendarId).getParentCalendarNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  calendar.
     *
     *  @param  id an <code> Id </code> 
     *  @param  calendarId the Id of a calendar 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> calendarId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfCalendar(org.osid.id.Id id, org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCalendar(id, calendarId)) {
            return (true);
        }

        try (org.osid.calendaring.CalendarList parents = getParentCalendars(calendarId)) {
            while (parents.hasNext()) {
                if (isAncestorOfCalendar(id, parents.getNextCalendar().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a calendar has any children. 
     *
     *  @param  calendarId a calendar <code> Id </code> 
     *  @return <code> true </code> if the <code> calendarId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildCalendars(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCalendarNode(calendarId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  calendar.
     *
     *  @param  id an <code> Id </code> 
     *  @param calendarId the <code> Id </code> of a 
     *         calendar
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> calendarId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> calendarId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfCalendar(org.osid.id.Id id, org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfCalendar(calendarId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  calendar.
     *
     *  @param  calendarId the <code> Id </code> to query 
     *  @return the children of the calendar 
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildCalendarIds(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.calendaring.calendar.CalendarToIdList(getChildCalendars(calendarId)));
    }


    /**
     *  Gets the children of the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> to query 
     *  @return the children of the calendar 
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarList getChildCalendars(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.calendaring.calendarnode.CalendarNodeToCalendarList(getCalendarNode(calendarId).getChildCalendarNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  calendar.
     *
     *  @param  id an <code> Id </code> 
     *  @param calendarId the <code> Id </code> of a 
     *         calendar
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> calendarId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfCalendar(org.osid.id.Id id, org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfCalendar(calendarId, id)) {
            return (true);
        }

        try (org.osid.calendaring.CalendarList children = getChildCalendars(calendarId)) {
            while (children.hasNext()) {
                if (isDescendantOfCalendar(id, children.getNextCalendar().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  calendar.
     *
     *  @param  calendarId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified calendar node 
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getCalendarNodeIds(org.osid.id.Id calendarId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.calendaring.calendarnode.CalendarNodeToNode(getCalendarNode(calendarId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given calendar.
     *
     *  @param  calendarId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified calendar node 
     *  @throws org.osid.NotFoundException <code> calendarId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> calendarId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.CalendarNode getCalendarNodes(org.osid.id.Id calendarId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCalendarNode(calendarId));
    }


    /**
     *  Closes this <code>CalendarHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a calendar node.
     *
     *  @param calendarId the id of the calendar node
     *  @throws org.osid.NotFoundException <code>calendarId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>calendarId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.calendaring.CalendarNode getCalendarNode(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(calendarId, "calendar Id");
        for (org.osid.calendaring.CalendarNode calendar : this.roots) {
            if (calendar.getId().equals(calendarId)) {
                return (calendar);
            }

            org.osid.calendaring.CalendarNode r = findCalendar(calendar, calendarId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(calendarId + " is not found");
    }


    protected org.osid.calendaring.CalendarNode findCalendar(org.osid.calendaring.CalendarNode node, 
                                                            org.osid.id.Id calendarId)
	throws org.osid.OperationFailedException {

        try (org.osid.calendaring.CalendarNodeList children = node.getChildCalendarNodes()) {
            while (children.hasNext()) {
                org.osid.calendaring.CalendarNode calendar = children.getNextCalendarNode();
                if (calendar.getId().equals(calendarId)) {
                    return (calendar);
                }
                
                calendar = findCalendar(calendar, calendarId);
                if (calendar != null) {
                    return (calendar);
                }
            }
        }

        return (null);
    }
}
