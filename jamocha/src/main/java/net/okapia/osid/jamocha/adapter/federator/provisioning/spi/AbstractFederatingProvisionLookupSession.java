//
// AbstractFederatingProvisionLookupSession.java
//
//     An abstract federating adapter for a ProvisionLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ProvisionLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingProvisionLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.ProvisionLookupSession>
    implements org.osid.provisioning.ProvisionLookupSession {

    private boolean parallel = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Constructs a new <code>AbstractFederatingProvisionLookupSession</code>.
     */

    protected AbstractFederatingProvisionLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.ProvisionLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>Provision</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProvisions() {
        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            if (session.canLookupProvisions()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Provision</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProvisionView() {
        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            session.useComparativeProvisionView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Provision</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProvisionView() {
        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            session.usePlenaryProvisionView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include provisions in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            session.useFederatedDistributorView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            session.useIsolatedDistributorView();
        }

        return;
    }


    /**
     *  Only provisions whose effective dates are current are returned
     *  by methods in this session.
     */

    @OSID @Override
    public void useEffectiveProvisionView() {
        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            session.useEffectiveProvisionView();
        }

        return;
    }


    /**
     *  All provisions of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveProvisionView() {
        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            session.useAnyEffectiveProvisionView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Provision</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Provision</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Provision</code> and
     *  retained for compatibility.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionId <code>Id</code> of the
     *          <code>Provision</code>
     *  @return the provision
     *  @throws org.osid.NotFoundException <code>provisionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>provisionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Provision getProvision(org.osid.id.Id provisionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            try {
                return (session.getProvision(provisionId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(provisionId + " not found");
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  provisions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Provisions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>provisionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByIds(org.osid.id.IdList provisionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.provision.MutableProvisionList ret = new net.okapia.osid.jamocha.provisioning.provision.MutableProvisionList();

        try (org.osid.id.IdList ids = provisionIds) {
            while (ids.hasNext()) {
                ret.addProvision(getProvision(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  provision genus <code>Type</code> which does not include
     *  provisions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionGenusType a provision genus type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByGenusType(org.osid.type.Type provisionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsByGenusType(provisionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProvisionList</code> corresponding to the given
     *  provision genus <code>Type</code> and include any additional
     *  provisions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionGenusType a provision genus type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByParentGenusType(org.osid.type.Type provisionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsByParentGenusType(provisionGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProvisionList</code> containing the given
     *  provision record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @param  provisionRecordType a provision record type 
     *  @return the returned <code>Provision</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsByRecordType(org.osid.type.Type provisionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsByRecordType(provisionRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProvisionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *  
     *  In active mode, provisions are returned that are currently
     *  active. In any status mode, active and inactive provisions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Provision</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsOnDate(org.osid.calendaring.DateTime from, 
                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a list of provisions corresponding to a broker
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  brokerId the <code>Id</code> of the broker
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>brokerId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForBroker(org.osid.id.Id brokerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();
        
        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsForBroker(brokerId));
        }
        
        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of provisions corresponding to a broker
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  brokerId the <code>Id</code> of the broker
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>brokerId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
    
    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForBrokerOnDate(org.osid.id.Id brokerId,
                                                                            org.osid.calendaring.DateTime from,
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsForBrokerOnDate(brokerId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of provisions corresponding to a provisionable
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionList getProvisionsForProvisionable(org.osid.id.Id provisionableId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsForProvisionable(provisionableId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of provisions corresponding to a provisionable
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableOnDate(org.osid.id.Id provisionableId,
                                                                                   org.osid.calendaring.DateTime from,
                                                                                   org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsForProvisionableOnDate(provisionableId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of provisions corresponding to a recipient
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionList getProvisionsForRecipient(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsForRecipient(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of provisions corresponding to a recipient
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForRecipientOnDate(org.osid.id.Id resourceId,
                                                                               org.osid.calendaring.DateTime from,
                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsForRecipientOnDate(resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of provisions corresponding to provisionable and recipient
     *  <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  provisionableId the <code>Id</code> of the provisionable
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>,
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableAndRecipient(org.osid.id.Id provisionableId,
                                                                                         org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsForProvisionableAndRecipient(provisionableId, resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of provisions corresponding to provisionable and recipient
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the recipient
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>,
     *          <code>resourceId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisionsForProvisionableAndRecipientOnDate(org.osid.id.Id provisionableId,
                                                                                               org.osid.id.Id resourceId,
                                                                                               org.osid.calendaring.DateTime from,
                                                                                               org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsForProvisionableAndRecipientOnDate(provisionableId, resourceId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a list of provisions corresponding to a request
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible
     *  through this session.
     *
     *  In effective mode, provisions are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisions and those currently expired are returned.
     *
     *  @param  requestId the <code>Id</code> of the request
     *  @return the returned <code>ProvisionList</code>
     *  @throws org.osid.NullArgumentException <code>requestId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionList getProvisionsForRequest(org.osid.id.Id requestId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisionsForRequest(requestId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Provisions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  provisions or an error results. Otherwise, the returned list
     *  may contain only those provisions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisions are returned that are currently
     *  effective.  In any effective mode, effective provisions and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Provisions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionList getProvisions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList ret = getProvisionList();

        for (org.osid.provisioning.ProvisionLookupSession session : getSessions()) {
            ret.addProvisionList(session.getProvisions());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.provision.FederatingProvisionList getProvisionList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.provision.ParallelProvisionList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.provision.CompositeProvisionList());
        }
    }
}
