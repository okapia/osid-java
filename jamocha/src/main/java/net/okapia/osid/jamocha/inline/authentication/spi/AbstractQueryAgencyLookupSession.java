//
// AbstractQueryAgencyLookupSession.java
//
//    An inline adapter that maps an AgencyLookupSession to
//    an AgencyQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AgencyLookupSession to
 *  an AgencyQuerySession.
 */

public abstract class AbstractQueryAgencyLookupSession
    extends net.okapia.osid.jamocha.authentication.spi.AbstractAgencyLookupSession
    implements org.osid.authentication.AgencyLookupSession {

    private final org.osid.authentication.AgencyQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAgencyLookupSession.
     *
     *  @param querySession the underlying agency query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAgencyLookupSession(org.osid.authentication.AgencyQuerySession querySession) {
        nullarg(querySession, "agency query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Agency</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAgencies() {
        return (this.session.canSearchAgencies());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Agency</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Agency</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Agency</code> and
     *  retained for compatibility.
     *
     *  @param  agencyId <code>Id</code> of the
     *          <code>Agency</code>
     *  @return the agency
     *  @throws org.osid.NotFoundException <code>agencyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>agencyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency(org.osid.id.Id agencyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgencyQuery query = getQuery();
        query.matchId(agencyId, true);
        org.osid.authentication.AgencyList agencies = this.session.getAgenciesByQuery(query);
        if (agencies.hasNext()) {
            return (agencies.getNextAgency());
        } 
        
        throw new org.osid.NotFoundException(agencyId + " not found");
    }


    /**
     *  Gets an <code>AgencyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  agencies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Agencies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  agencyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Agency</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>agencyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByIds(org.osid.id.IdList agencyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgencyQuery query = getQuery();

        try (org.osid.id.IdList ids = agencyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAgenciesByQuery(query));
    }


    /**
     *  Gets an <code>AgencyList</code> corresponding to the given
     *  agency genus <code>Type</code> which does not include
     *  agencies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agencyGenusType an agency genus type 
     *  @return the returned <code>Agency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agencyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByGenusType(org.osid.type.Type agencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgencyQuery query = getQuery();
        query.matchGenusType(agencyGenusType, true);
        return (this.session.getAgenciesByQuery(query));
    }


    /**
     *  Gets an <code>AgencyList</code> corresponding to the given
     *  agency genus <code>Type</code> and include any additional
     *  agencies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agencyGenusType an agency genus type 
     *  @return the returned <code>Agency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agencyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByParentGenusType(org.osid.type.Type agencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgencyQuery query = getQuery();
        query.matchParentGenusType(agencyGenusType, true);
        return (this.session.getAgenciesByQuery(query));
    }


    /**
     *  Gets an <code>AgencyList</code> containing the given
     *  agency record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agencyRecordType an agency record type 
     *  @return the returned <code>Agency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agencyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByRecordType(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgencyQuery query = getQuery();
        query.matchRecordType(agencyRecordType, true);
        return (this.session.getAgenciesByQuery(query));
    }


    /**
     *  Gets an <code>AgencyList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known agencies or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  agencies that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Agency</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgenciesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgencyQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getAgenciesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Agencies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  agencies or an error results. Otherwise, the returned list
     *  may contain only those agencies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Agencies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgencyList getAgencies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.authentication.AgencyQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAgenciesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.authentication.AgencyQuery getQuery() {
        org.osid.authentication.AgencyQuery query = this.session.getAgencyQuery();
        return (query);
    }
}
