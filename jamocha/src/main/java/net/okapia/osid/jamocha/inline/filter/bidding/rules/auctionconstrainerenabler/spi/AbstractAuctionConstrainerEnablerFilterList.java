//
// AbstractAuctionConstrainerEnablerList
//
//     Implements a filter for an AuctionConstrainerEnablerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for an AuctionConstrainerEnablerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedAuctionConstrainerEnablerList
 *  to improve performance.
 */

public abstract class AbstractAuctionConstrainerEnablerFilterList
    extends net.okapia.osid.jamocha.bidding.rules.auctionconstrainerenabler.spi.AbstractAuctionConstrainerEnablerList
    implements org.osid.bidding.rules.AuctionConstrainerEnablerList,
               net.okapia.osid.jamocha.inline.filter.bidding.rules.auctionconstrainerenabler.AuctionConstrainerEnablerFilter {

    private org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler;
    private final org.osid.bidding.rules.AuctionConstrainerEnablerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractAuctionConstrainerEnablerFilterList</code>.
     *
     *  @param auctionConstrainerEnablerList an <code>AuctionConstrainerEnablerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerList</code> is <code>null</code>
     */

    protected AbstractAuctionConstrainerEnablerFilterList(org.osid.bidding.rules.AuctionConstrainerEnablerList auctionConstrainerEnablerList) {
        nullarg(auctionConstrainerEnablerList, "auction constrainer enabler list");
        this.list = auctionConstrainerEnablerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.auctionConstrainerEnabler == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> AuctionConstrainerEnabler </code> in this list. 
     *
     *  @return the next <code> AuctionConstrainerEnabler </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> AuctionConstrainerEnabler </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionConstrainerEnabler getNextAuctionConstrainerEnabler()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler = this.auctionConstrainerEnabler;
            this.auctionConstrainerEnabler = null;
            return (auctionConstrainerEnabler);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in auction constrainer enabler list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.auctionConstrainerEnabler = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters AuctionConstrainerEnablers.
     *
     *  @param auctionConstrainerEnabler the auction constrainer enabler to filter
     *  @return <code>true</code> if the auction constrainer enabler passes the filter,
     *          <code>false</code> if the auction constrainer enabler should be filtered
     */

    public abstract boolean pass(org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler);


    protected void prime() {
        if (this.auctionConstrainerEnabler != null) {
            return;
        }

        org.osid.bidding.rules.AuctionConstrainerEnabler auctionConstrainerEnabler = null;

        while (this.list.hasNext()) {
            try {
                auctionConstrainerEnabler = this.list.getNextAuctionConstrainerEnabler();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(auctionConstrainerEnabler)) {
                this.auctionConstrainerEnabler = auctionConstrainerEnabler;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
