//
// AbstractAssemblyLogEntryQuery.java
//
//     A LogEntryQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.tracking.logentry.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A LogEntryQuery that stores terms.
 */

public abstract class AbstractAssemblyLogEntryQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.tracking.LogEntryQuery,
               org.osid.tracking.LogEntryQueryInspector,
               org.osid.tracking.LogEntrySearchOrder {

    private final java.util.Collection<org.osid.tracking.records.LogEntryQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.records.LogEntryQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.tracking.records.LogEntrySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyLogEntryQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyLogEntryQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId the agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Orders the results by the agent. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getAgentColumn(), style);
        return;
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent.. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Clears the agent query terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Tests if an agent search order is available. 
     *
     *  @return <code> true </code> if an agent search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsCreatorSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Sets the issue <code> Id </code> for this query. 
     *
     *  @param  issueId the issue <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> issueId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIssueId(org.osid.id.Id issueId, boolean match) {
        getAssembler().addIdTerm(getIssueIdColumn(), issueId, match);
        return;
    }


    /**
     *  Clears the issue <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIssueIdTerms() {
        getAssembler().clearTerms(getIssueIdColumn());
        return;
    }


    /**
     *  Gets the issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIssueIdTerms() {
        return (getAssembler().getIdTerms(getIssueIdColumn()));
    }


    /**
     *  Orders the results by the issue. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByIssue(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getIssueColumn(), style);
        return;
    }


    /**
     *  Gets the IssueId column name.
     *
     * @return the column name
     */

    protected String getIssueIdColumn() {
        return ("issue_id");
    }


    /**
     *  Tests if an <code> IssueQuery </code> is available for an ending 
     *  issue. 
     *
     *  @return <code> true </code> if an issue query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (false);
    }


    /**
     *  Gets the query for an issue. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the issue query 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuery getIssueQuery() {
        throw new org.osid.UnimplementedException("supportsIssueQuery() is false");
    }


    /**
     *  Clears the issue query terms. 
     */

    @OSID @Override
    public void clearIssueTerms() {
        getAssembler().clearTerms(getIssueColumn());
        return;
    }


    /**
     *  Gets the issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getIssueTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Tests if an issue search order is available. 
     *
     *  @return <code> true </code> if an issue search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSearchOrder() {
        return (false);
    }


    /**
     *  Gets the issue search order. 
     *
     *  @return the issue search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsIssueSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchOrder getIssueSearchOrder() {
        throw new org.osid.UnimplementedException("supportsIssueSearchOrder() is false");
    }


    /**
     *  Gets the Issue column name.
     *
     * @return the column name
     */

    protected String getIssueColumn() {
        return ("issue");
    }


    /**
     *  Matches log entries that have the specified date inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchDate(org.osid.calendaring.DateTime from, 
                          org.osid.calendaring.DateTime to, boolean match) {
        getAssembler().addDateTimeRangeTerm(getDateColumn(), from, to, match);
        return;
    }


    /**
     *  Clears the date query terms. 
     */

    @OSID @Override
    public void clearDateTerms() {
        getAssembler().clearTerms(getDateColumn());
        return;
    }


    /**
     *  Gets the date query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDateTerms() {
        return (getAssembler().getDateTimeRangeTerms(getDateColumn()));
    }


    /**
     *  Orders the results by the date. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByDate(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getDateColumn(), style);
        return;
    }


    /**
     *  Gets the Date column name.
     *
     * @return the column name
     */

    protected String getDateColumn() {
        return ("date");
    }


    /**
     *  Matches a summary. 
     *
     *  @param  summary summary 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> summary </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> summary </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> stringMatchType </code> 
     *          not supported 
     */

    @OSID @Override
    public void matchSummary(String summary, 
                             org.osid.type.Type stringMatchType, boolean match) {
        getAssembler().addStringTerm(getSummaryColumn(), summary, stringMatchType, match);
        return;
    }


    /**
     *  Matches log entries that has any summary. 
     *
     *  @param  match <code> true </code> to match log entries with any 
     *          summary, <code> false </code> to match log entries with no 
     *          summary 
     */

    @OSID @Override
    public void matchAnySummary(boolean match) {
        getAssembler().addStringWildcardTerm(getSummaryColumn(), match);
        return;
    }


    /**
     *  Clears the summary query terms. 
     */

    @OSID @Override
    public void clearSummaryTerms() {
        getAssembler().clearTerms(getSummaryColumn());
        return;
    }


    /**
     *  Gets the summary query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getSummaryTerms() {
        return (getAssembler().getStringTerms(getSummaryColumn()));
    }


    /**
     *  Orders the results by the summary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderBySummary(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getSummaryColumn(), style);
        return;
    }


    /**
     *  Gets the Summary column name.
     *
     * @return the column name
     */

    protected String getSummaryColumn() {
        return ("summary");
    }


    /**
     *  Matches a message. 
     *
     *  @param  message a message 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> message </code> not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> message </code> or 
     *          <code> stringMatchType </code> is <code> null </code> 
     *  @throws org.osid.UnsupportedException <code> stringMatchType </code> 
     *          not supported 
     */

    @OSID @Override
    public void matchMessage(String message, 
                             org.osid.type.Type stringMatchType, boolean match) {
        getAssembler().addStringTerm(getMessageColumn(), message, stringMatchType, match);
        return;
    }


    /**
     *  Matches log entries that has any message. 
     *
     *  @param  match <code> true </code> to match log entries with any 
     *          message, <code> false </code> to match log entries with no 
     *          message 
     */

    @OSID @Override
    public void matchAnyMessage(boolean match) {
        getAssembler().addStringWildcardTerm(getMessageColumn(), match);
        return;
    }


    /**
     *  Clears the message query terms. 
     */

    @OSID @Override
    public void clearMessageTerms() {
        getAssembler().clearTerms(getMessageColumn());
        return;
    }


    /**
     *  Gets the message query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getMessageTerms() {
        return (getAssembler().getStringTerms(getMessageColumn()));
    }


    /**
     *  Orders the results by the summary. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByMessage(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getMessageColumn(), style);
        return;
    }


    /**
     *  Gets the Message column name.
     *
     * @return the column name
     */

    protected String getMessageColumn() {
        return ("message");
    }


    /**
     *  Sets the front office <code> Id </code> for this query to match log 
     *  entries assigned to front offices. 
     *
     *  @param  frontOfficeId the front office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFrontOfficeId(org.osid.id.Id frontOfficeId, boolean match) {
        getAssembler().addIdTerm(getFrontOfficeIdColumn(), frontOfficeId, match);
        return;
    }


    /**
     *  Clears the front office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeIdTerms() {
        getAssembler().clearTerms(getFrontOfficeIdColumn());
        return;
    }


    /**
     *  Gets the front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFrontOfficeIdTerms() {
        return (getAssembler().getIdTerms(getFrontOfficeIdColumn()));
    }


    /**
     *  Gets the FrontOfficeId column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeIdColumn() {
        return ("front_office_id");
    }


    /**
     *  Tests if a <code> FrontOfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if a front office query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a front office. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the front office query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuery getFrontOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsFrontOfficeQuery() is false");
    }


    /**
     *  Clears the front office query terms. 
     */

    @OSID @Override
    public void clearFrontOfficeTerms() {
        getAssembler().clearTerms(getFrontOfficeColumn());
        return;
    }


    /**
     *  Gets the front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }


    /**
     *  Gets the FrontOffice column name.
     *
     * @return the column name
     */

    protected String getFrontOfficeColumn() {
        return ("front_office");
    }


    /**
     *  Tests if this logEntry supports the given record
     *  <code>Type</code>.
     *
     *  @param  logEntryRecordType a log entry record type 
     *  @return <code>true</code> if the logEntryRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type logEntryRecordType) {
        for (org.osid.tracking.records.LogEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  logEntryRecordType the log entry record type 
     *  @return the log entry query record 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.LogEntryQueryRecord getLogEntryQueryRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.LogEntryQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  logEntryRecordType the log entry record type 
     *  @return the log entry query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.LogEntryQueryInspectorRecord getLogEntryQueryInspectorRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.LogEntryQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param logEntryRecordType the log entry record type
     *  @return the log entry search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(logEntryRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.LogEntrySearchOrderRecord getLogEntrySearchOrderRecord(org.osid.type.Type logEntryRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.LogEntrySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(logEntryRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(logEntryRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this log entry. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param logEntryQueryRecord the log entry query record
     *  @param logEntryQueryInspectorRecord the log entry query inspector
     *         record
     *  @param logEntrySearchOrderRecord the log entry search order record
     *  @param logEntryRecordType log entry record type
     *  @throws org.osid.NullArgumentException
     *          <code>logEntryQueryRecord</code>,
     *          <code>logEntryQueryInspectorRecord</code>,
     *          <code>logEntrySearchOrderRecord</code> or
     *          <code>logEntryRecordTypelogEntry</code> is
     *          <code>null</code>
     */
            
    protected void addLogEntryRecords(org.osid.tracking.records.LogEntryQueryRecord logEntryQueryRecord, 
                                      org.osid.tracking.records.LogEntryQueryInspectorRecord logEntryQueryInspectorRecord, 
                                      org.osid.tracking.records.LogEntrySearchOrderRecord logEntrySearchOrderRecord, 
                                      org.osid.type.Type logEntryRecordType) {

        addRecordType(logEntryRecordType);

        nullarg(logEntryQueryRecord, "log entry query record");
        nullarg(logEntryQueryInspectorRecord, "log entry query inspector record");
        nullarg(logEntrySearchOrderRecord, "log entry search odrer record");

        this.queryRecords.add(logEntryQueryRecord);
        this.queryInspectorRecords.add(logEntryQueryInspectorRecord);
        this.searchOrderRecords.add(logEntrySearchOrderRecord);
        
        return;
    }
}
