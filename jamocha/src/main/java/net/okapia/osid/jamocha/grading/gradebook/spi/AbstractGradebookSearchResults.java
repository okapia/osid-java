//
// AbstractGradebookSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebook.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractGradebookSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.grading.GradebookSearchResults {

    private org.osid.grading.GradebookList gradebooks;
    private final org.osid.grading.GradebookQueryInspector inspector;
    private final java.util.Collection<org.osid.grading.records.GradebookSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractGradebookSearchResults.
     *
     *  @param gradebooks the result set
     *  @param gradebookQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>gradebooks</code>
     *          or <code>gradebookQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractGradebookSearchResults(org.osid.grading.GradebookList gradebooks,
                                            org.osid.grading.GradebookQueryInspector gradebookQueryInspector) {
        nullarg(gradebooks, "gradebooks");
        nullarg(gradebookQueryInspector, "gradebook query inspectpr");

        this.gradebooks = gradebooks;
        this.inspector = gradebookQueryInspector;

        return;
    }


    /**
     *  Gets the gradebook list resulting from a search.
     *
     *  @return a gradebook list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.grading.GradebookList getGradebooks() {
        if (this.gradebooks == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.grading.GradebookList gradebooks = this.gradebooks;
        this.gradebooks = null;
	return (gradebooks);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.grading.GradebookQueryInspector getGradebookQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  gradebook search record <code> Type. </code> This method must
     *  be used to retrieve a gradebook implementing the requested
     *  record.
     *
     *  @param gradebookSearchRecordType a gradebook search 
     *         record type 
     *  @return the gradebook search
     *  @throws org.osid.NullArgumentException
     *          <code>gradebookSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(gradebookSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradebookSearchResultsRecord getGradebookSearchResultsRecord(org.osid.type.Type gradebookSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.grading.records.GradebookSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(gradebookSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(gradebookSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record gradebook search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addGradebookRecord(org.osid.grading.records.GradebookSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "gradebook record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
