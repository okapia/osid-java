//
// AbstractTermQueryInspector.java
//
//     A template for making a TermQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.term.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for terms.
 */

public abstract class AbstractTermQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.course.TermQueryInspector {

    private final java.util.Collection<org.osid.course.records.TermQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the display label query terms. 
     *
     *  @return the display labelquery terms 
     */

    @OSID @Override
    public org.osid.search.terms.StringTerm[] getDisplayLabelTerms() {
        return (new org.osid.search.terms.StringTerm[0]);
    }


    /**
     *  Gets the open date query terms. 
     *
     *  @return the open date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getOpenDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the registration start date query terms. 
     *
     *  @return the registration start date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getRegistrationStartTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the registration end date query terms. 
     *
     *  @return the registration end date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getRegistrationEndTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the registration period query terms. 
     *
     *  @return the registration period query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getRegistrationPeriodTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the registration duration query terms. 
     *
     *  @return the registration duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getRegistrationDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the class start date query terms. 
     *
     *  @return the class start date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClassesStartTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the class end date query terms. 
     *
     *  @return the class end date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClassesEndTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the class period query terms. 
     *
     *  @return the class period query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getClassesPeriodTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the classes duration query terms. 
     *
     *  @return the classes duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getClassesDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the add date query terms. 
     *
     *  @return the add date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getAddDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the drop date query terms. 
     *
     *  @return the drop date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getDropDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the final exam start date query terms. 
     *
     *  @return the final exam start date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getFinalExamStartTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the final exam end date query terms. 
     *
     *  @return the final exam end date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getFinalExamEndTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the final exam period query terms. 
     *
     *  @return the final exam period query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getFinalExamPeriodTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the final exam duration query terms. 
     *
     *  @return the final exam duration query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getFinalExamDurationTerms() {
        return (new org.osid.search.terms.DurationRangeTerm[0]);
    }


    /**
     *  Gets the close date query terms. 
     *
     *  @return the close date query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DateTimeRangeTerm[] getCloseDateTerms() {
        return (new org.osid.search.terms.DateTimeRangeTerm[0]);
    }


    /**
     *  Gets the ancestor term <code> Id </code> query terms. 
     *
     *  @return the ancestor term <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorTermIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor term query terms. 
     *
     *  @return the ancestor term terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getAncestorTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Gets the descendant term <code> Id </code> query terms. 
     *
     *  @return the descendant term <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantTermIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant term query terms. 
     *
     *  @return the descendant term terms 
     */

    @OSID @Override
    public org.osid.course.TermQueryInspector[] getDescendantTermTerms() {
        return (new org.osid.course.TermQueryInspector[0]);
    }


    /**
     *  Gets the course offering <code> Id </code> query terms. 
     *
     *  @return the course offering <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseOfferingIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course offering query terms. 
     *
     *  @return the course offering query terms 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQueryInspector[] getCourseOfferingTerms() {
        return (new org.osid.course.CourseOfferingQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given term query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a term implementing the requested record.
     *
     *  @param termRecordType a term record type
     *  @return the term query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(termRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.TermQueryInspectorRecord getTermQueryInspectorRecord(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.TermQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(termRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(termRecordType + " is not supported");
    }


    /**
     *  Adds a record to this term query. 
     *
     *  @param termQueryInspectorRecord term query inspector
     *         record
     *  @param termRecordType term record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addTermQueryInspectorRecord(org.osid.course.records.TermQueryInspectorRecord termQueryInspectorRecord, 
                                                   org.osid.type.Type termRecordType) {

        addRecordType(termRecordType);
        nullarg(termRecordType, "term record type");
        this.records.add(termQueryInspectorRecord);        
        return;
    }
}
