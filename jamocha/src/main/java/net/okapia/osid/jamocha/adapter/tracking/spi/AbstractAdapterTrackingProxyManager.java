//
// AbstractTrackingProxyManager.java
//
//     An adapter for a TrackingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.tracking.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a TrackingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterTrackingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.tracking.TrackingProxyManager>
    implements org.osid.tracking.TrackingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterTrackingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterTrackingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterTrackingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterTrackingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any front office federation is exposed. Federation is exposed 
     *  when a specific front office may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of front offices appears as a single front office. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if an issue service is supported for the current agent. 
     *
     *  @return <code> true </code> if my issue is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyIssue() {
        return (getAdapteeManager().supportsMyIssue());
    }


    /**
     *  Tests if an issue tracking service is supported. 
     *
     *  @return <code> true </code> if issue tracking is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueTracking() {
        return (getAdapteeManager().supportsIssueTracking());
    }


    /**
     *  Tests if an issue resourcing service is supported. 
     *
     *  @return <code> true </code> if issue resourcing is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueResourcing() {
        return (getAdapteeManager().supportsIssueResourcing());
    }


    /**
     *  Tests if an issue triaging service is supported. 
     *
     *  @return <code> true </code> if issue triaging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueTriaging() {
        return (getAdapteeManager().supportsIssueTriaging());
    }


    /**
     *  Tests if looking up issues is supported. 
     *
     *  @return <code> true </code> if issue lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueLookup() {
        return (getAdapteeManager().supportsIssueLookup());
    }


    /**
     *  Tests if querying issues is supported. 
     *
     *  @return <code> true </code> if issue query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueQuery() {
        return (getAdapteeManager().supportsIssueQuery());
    }


    /**
     *  Tests if searching issues is supported. 
     *
     *  @return <code> true </code> if issue search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSearch() {
        return (getAdapteeManager().supportsIssueSearch());
    }


    /**
     *  Tests if issue <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if issue administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueAdmin() {
        return (getAdapteeManager().supportsIssueAdmin());
    }


    /**
     *  Tests if an issue <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if issue notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueNotification() {
        return (getAdapteeManager().supportsIssueNotification());
    }


    /**
     *  Tests if an issue front office lookup service is supported. 
     *
     *  @return <code> true </code> if an issue front office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueFrontOffice() {
        return (getAdapteeManager().supportsIssueFrontOffice());
    }


    /**
     *  Tests if an issue front office assignment service is supported. 
     *
     *  @return <code> true </code> if an issue to front office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueFrontOfficeAssignment() {
        return (getAdapteeManager().supportsIssueFrontOfficeAssignment());
    }


    /**
     *  Tests if an issue smart front office service is supported. 
     *
     *  @return <code> true </code> if an issue smart front office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueSmartFrontOffice() {
        return (getAdapteeManager().supportsIssueSmartFrontOffice());
    }


    /**
     *  Tests if looking up subtasks is supported. 
     *
     *  @return <code> true </code> if subtask lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubtaskIssueLookup() {
        return (getAdapteeManager().supportsSubtaskIssueLookup());
    }


    /**
     *  Tests if managing subtasks is supported. 
     *
     *  @return <code> true </code> if subtask admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSubtaskIssueAdmin() {
        return (getAdapteeManager().supportsSubtaskIssueAdmin());
    }


    /**
     *  Tests if looking up duplicate issues is supported. 
     *
     *  @return <code> true </code> if duplicate issue lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDuplicateIssueLookup() {
        return (getAdapteeManager().supportsDuplicateIssueLookup());
    }


    /**
     *  Tests if managing duplicate issues is supported. 
     *
     *  @return <code> true </code> if duplicate issue admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDuplicateIssueAdmin() {
        return (getAdapteeManager().supportsDuplicateIssueAdmin());
    }


    /**
     *  Tests if looking up issue branches is supported. 
     *
     *  @return <code> true </code> if branched issue lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchedIssueLookup() {
        return (getAdapteeManager().supportsBranchedIssueLookup());
    }


    /**
     *  Tests if managing issue branches is supported. 
     *
     *  @return <code> true </code> if branched issue admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchedIssueAdmin() {
        return (getAdapteeManager().supportsBranchedIssueAdmin());
    }


    /**
     *  Tests if looking up blocking issues is supported. 
     *
     *  @return <code> true </code> if blocking issue lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockingIssueLookup() {
        return (getAdapteeManager().supportsBlockingIssueLookup());
    }


    /**
     *  Tests if managing issue blocks is supported. 
     *
     *  @return <code> true </code> if blocking issue admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBlockingIssueAdmin() {
        return (getAdapteeManager().supportsBlockingIssueAdmin());
    }


    /**
     *  Tests if looking up log entries is supported. 
     *
     *  @return <code> true </code> if log entry lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryLookup() {
        return (getAdapteeManager().supportsLogEntryLookup());
    }


    /**
     *  Tests if issue commenting and customer messaging is supported. 
     *
     *  @return <code> true </code> if issue messaging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIssueMessaging() {
        return (getAdapteeManager().supportsIssueMessaging());
    }


    /**
     *  Tests if log entry notification is supported. 
     *
     *  @return <code> true </code> if log entry notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryNotification() {
        return (getAdapteeManager().supportsLogEntryNotification());
    }


    /**
     *  Tests if looking up queues is supported. 
     *
     *  @return <code> true </code> if queue lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueLookup() {
        return (getAdapteeManager().supportsQueueLookup());
    }


    /**
     *  Tests if querying queues is supported. 
     *
     *  @return <code> true </code> if queue query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueQuery() {
        return (getAdapteeManager().supportsQueueQuery());
    }


    /**
     *  Tests if searching queues is supported. 
     *
     *  @return <code> true </code> if queue search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSearch() {
        return (getAdapteeManager().supportsQueueSearch());
    }


    /**
     *  Tests if queue <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if queue administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueAdmin() {
        return (getAdapteeManager().supportsQueueAdmin());
    }


    /**
     *  Tests if a queue <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if queue notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueNotification() {
        return (getAdapteeManager().supportsQueueNotification());
    }


    /**
     *  Tests if a queue front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue front office lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueFrontOffice() {
        return (getAdapteeManager().supportsQueueFrontOffice());
    }


    /**
     *  Tests if a queue front office service is supported. 
     *
     *  @return <code> true </code> if queue to front office assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueFrontOfficeAssignment() {
        return (getAdapteeManager().supportsQueueFrontOfficeAssignment());
    }


    /**
     *  Tests if a queue smart front office lookup service is supported. 
     *
     *  @return <code> true </code> if a queue smart front office service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueSmartFrontOffice() {
        return (getAdapteeManager().supportsQueueSmartFrontOffice());
    }


    /**
     *  Tests if a queue resourcing is supported. 
     *
     *  @return <code> true </code> if a queue resourcing service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQueueResourcing() {
        return (getAdapteeManager().supportsQueueResourcing());
    }


    /**
     *  Tests if looking up front offices is supported. 
     *
     *  @return <code> true </code> if front office lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeLookup() {
        return (getAdapteeManager().supportsFrontOfficeLookup());
    }


    /**
     *  Tests if querying front offices is supported. 
     *
     *  @return <code> true </code> if a front office query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeQuery() {
        return (getAdapteeManager().supportsFrontOfficeQuery());
    }


    /**
     *  Tests if searching front offices is supported. 
     *
     *  @return <code> true </code> if front office search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeSearch() {
        return (getAdapteeManager().supportsFrontOfficeSearch());
    }


    /**
     *  Tests if front office administrative service is supported. 
     *
     *  @return <code> true </code> if front office administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeAdmin() {
        return (getAdapteeManager().supportsFrontOfficeAdmin());
    }


    /**
     *  Tests if a front office <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if front office notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeNotification() {
        return (getAdapteeManager().supportsFrontOfficeNotification());
    }


    /**
     *  Tests for the availability of a front office hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if front office hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeHierarchy() {
        return (getAdapteeManager().supportsFrontOfficeHierarchy());
    }


    /**
     *  Tests for the availability of a front office hierarchy design service. 
     *
     *  @return <code> true </code> if front office hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFrontOfficeHierarchyDesign() {
        return (getAdapteeManager().supportsFrontOfficeHierarchyDesign());
    }


    /**
     *  Tests for the availability of a tracking rules service. 
     *
     *  @return <code> true </code> if a tracking rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTrackingRules() {
        return (getAdapteeManager().supportsTrackingRules());
    }


    /**
     *  Gets the supported <code> Issue </code> record types. 
     *
     *  @return a list containing the supported <code> Issue </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIssueRecordTypes() {
        return (getAdapteeManager().getIssueRecordTypes());
    }


    /**
     *  Tests if the given <code> Issue </code> record type is supported. 
     *
     *  @param  issueRecordType a <code> Type </code> indicating an <code> 
     *          Issue </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> issueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIssueRecordType(org.osid.type.Type issueRecordType) {
        return (getAdapteeManager().supportsIssueRecordType(issueRecordType));
    }


    /**
     *  Gets the supported <code> Issue </code> search types. 
     *
     *  @return a list containing the supported <code> Issue </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIssueSearchRecordTypes() {
        return (getAdapteeManager().getIssueSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Issue </code> search type is supported. 
     *
     *  @param  issueSearchRecordType a <code> Type </code> indicating an 
     *          <code> Issue </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> issueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIssueSearchRecordType(org.osid.type.Type issueSearchRecordType) {
        return (getAdapteeManager().supportsIssueSearchRecordType(issueSearchRecordType));
    }


    /**
     *  Gets the supported <code> LogEntry </code> record types. 
     *
     *  @return a list containing the supported <code> LogEntry </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogEntryRecordTypes() {
        return (getAdapteeManager().getLogEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> LogEntry </code> record type is supported. 
     *
     *  @param  logEntryRecordType a <code> Type </code> indicating a <code> 
     *          LogEntry </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogEntryRecordType(org.osid.type.Type logEntryRecordType) {
        return (getAdapteeManager().supportsLogEntryRecordType(logEntryRecordType));
    }


    /**
     *  Gets the supported <code> Queue </code> record types. 
     *
     *  @return a list containing the supported <code> Queue </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueRecordTypes() {
        return (getAdapteeManager().getQueueRecordTypes());
    }


    /**
     *  Tests if the given <code> Queue </code> record type is supported. 
     *
     *  @param  queueRecordType a <code> Type </code> indicating a <code> 
     *          Queue </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueRecordType(org.osid.type.Type queueRecordType) {
        return (getAdapteeManager().supportsQueueRecordType(queueRecordType));
    }


    /**
     *  Gets the supported <code> Queue </code> search record types. 
     *
     *  @return a list containing the supported <code> Queue </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getQueueSearchRecordTypes() {
        return (getAdapteeManager().getQueueSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Queue </code> search record type is 
     *  supported. 
     *
     *  @param  queueSearchRecordType a <code> Type </code> indicating a 
     *          <code> Queue </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> queueSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsQueueSearchRecordType(org.osid.type.Type queueSearchRecordType) {
        return (getAdapteeManager().supportsQueueSearchRecordType(queueSearchRecordType));
    }


    /**
     *  Gets the supported <code> FrontOffice </code> record types. 
     *
     *  @return a list containing the supported <code> FrontOffice </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFrontOfficeRecordTypes() {
        return (getAdapteeManager().getFrontOfficeRecordTypes());
    }


    /**
     *  Tests if the given <code> FrontOffice </code> record type is 
     *  supported. 
     *
     *  @param  frontOfficeRecordType a <code> Type </code> indicating a 
     *          <code> FrontOffice </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> frontOfficeRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFrontOfficeRecordType(org.osid.type.Type frontOfficeRecordType) {
        return (getAdapteeManager().supportsFrontOfficeRecordType(frontOfficeRecordType));
    }


    /**
     *  Gets the supported <code> FrontOffice </code> search record types. 
     *
     *  @return a list containing the supported <code> FrontOffice </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFrontOfficeSearchRecordTypes() {
        return (getAdapteeManager().getFrontOfficeSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> FrontOffice </code> search record type is 
     *  supported. 
     *
     *  @param  frontOfficeSearchRecordType a <code> Type </code> indicating a 
     *          <code> FrontOffice </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          frontOfficeSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsFrontOfficeSearchRecordType(org.osid.type.Type frontOfficeSearchRecordType) {
        return (getAdapteeManager().supportsFrontOfficeSearchRecordType(frontOfficeSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my issue 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyIssueSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyIssue() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.MyIssueSession getMyIssueSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyIssueSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my issue 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> MyIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyIssue() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.MyIssueSession getMyIssueSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyIssueSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue tracking 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueTrackingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueTracking() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTrackingSession getIssueTrackingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueTrackingSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue tracking 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return an <code> IssueTrackingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueTracking() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTrackingSession getIssueTrackingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueTrackingSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  resourcing service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueResourcingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueResourcing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueResourcingSession getIssueResourcingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueResourcingSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  resourcing service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return an <code> IssueResourcingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueResourcing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueResourcingSession getIssueResourcingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueResourcingSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue triaging 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueTriagingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyIssueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTriagingSession getIssueTriagingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueTriagingSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue triaging 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return an <code> IssueTriagingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueTriaging() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueTriagingSession getIssueTriagingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueTriagingSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueLookupSession getIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue lookup 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return an <code> IssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueLookupSession getIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueLookupSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuerySession getIssueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue query 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueQuerySession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueQuerySession getIssueQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueQuerySessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchSession getIssueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue search 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueSearchSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSearchSession getIssueSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueSearchSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueAdminSession getIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> Queue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsIssueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueAdminSession getIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueAdminSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueNotificationSession getIssueNotificationSession(org.osid.tracking.IssueReceiver issueReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueNotificationSession(issueReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  notification service for the given front office. 
     *
     *  @param  issueReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> issueReceiver, 
     *          frontOfficeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueNotificationSession getIssueNotificationSessionForFrontOffice(org.osid.tracking.IssueReceiver issueReceiver, 
                                                                                                org.osid.id.Id frontOfficeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueNotificationSessionForFrontOffice(issueReceiver, frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup issue/front office 
     *  trackings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueFrontOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueFrontOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueFrontOfficeSession getIssueFrontOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueFrontOfficeSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning issues 
     *  to frontOffices. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueFrontOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueFrontOfficeAssignmentSession getIssueFrontOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueFrontOfficeAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart 
     *  frontOffices. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueSmartFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueSmartFrontOfficeSession getIssueSmartFrontOfficeSession(org.osid.id.Id frontOfficeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueSmartFrontOfficeSession(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueLookupSession getSubtaskIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubtaskIssueLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueLookupSession getSubtaskIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubtaskIssueLookupSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueAdminSession getSubtaskIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSubtaskIssueAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the subtask issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> SubtaskIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSubtaskIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.SubtaskIssueAdminSession getSubtaskIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSubtaskIssueAdminSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DuplicateIssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueLookupSession getDuplicateIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDuplicateIssueLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> DuplicateIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueLookupSession getDuplicateIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDuplicateIssueLookupSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DuplicateIssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueAdminSession getDuplicateIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDuplicateIssueAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the duplicate 
     *  issue administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> DuplicateIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDuplicateIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.DuplicateIssueAdminSession getDuplicateIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDuplicateIssueAdminSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BranchedIssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueLookupSession getBranchedIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchedIssueLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> BranchedIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueLookupSession getBranchedIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchedIssueLookupSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BranchedIssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueAdminSession getBranchedIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchedIssueAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branched issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> BranchedIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchedIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BranchedIssueAdminSession getBranchedIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBranchedIssueAdminSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockingIssueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueLookupSession getBlockingIssueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockingIssueLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> BlockingIssueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueLookupSession getBlockingIssueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockingIssueLookupSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BlockingIssueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueAdminSession getBlockingIssueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockingIssueAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the blocking issue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the front office 
     *  @param  proxy a proxy 
     *  @return a <code> BlockingIssueAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBlockingIssueAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.BlockingIssueAdminSession getBlockingIssueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBlockingIssueAdminSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryLookupSession getLogEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  lookup service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryLookupSession getLogEntryLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryLookupSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  messaging service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> IssueMessagingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueMessaging() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueMessagingSession getIssueMessagingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueMessagingSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the issue 
     *  messaging service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return an <code> IssueMessagingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIssueMessaging() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.IssueMessagingSession getIssueMessagingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getIssueMessagingSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  notification service. 
     *
     *  @param  logEntryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryNotificationSession getLogEntryNotificationSession(org.osid.tracking.LogEntryReceiver logEntryReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryNotificationSession(logEntryReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  notification service for the given front office. 
     *
     *  @param  logEntryReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver, 
     *          frontOfficeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.LogEntryNotificationSession getLogEntryNotificationSessionForFrontOffice(org.osid.tracking.LogEntryReceiver logEntryReceiver, 
                                                                                                      org.osid.id.Id frontOfficeId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryNotificationSessionForFrontOffice(logEntryReceiver, frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueLookupSession getQueueLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue lookup 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueLookupSession getQueueLookupSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueLookupSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuerySession getQueueQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue query 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueQuerySession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueQuerySession getQueueQuerySessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueQuerySessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSearchSession getQueueSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue search 
     *  service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueSearchSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSearchSession getQueueSearchSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueSearchSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueAdminSession getQueueAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  administration service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueAdminSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId or proxy 
     *          is null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsQueueAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueAdminSession getQueueAdminSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueAdminSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service. 
     *
     *  @param  queueReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueNotificationSession getQueueNotificationSession(org.osid.tracking.QueueReceiver queueReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueNotificationSession(queueReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  notification service for the given front office. 
     *
     *  @param  queueReceiver the notification callback 
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueNotificationSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> queueReceiver, 
     *          frontOfficeId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueNotificationSession getQueueNotificationSessionForFrontOffice(org.osid.tracking.QueueReceiver queueReceiver, 
                                                                                                org.osid.id.Id frontOfficeId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueNotificationSessionForFrontOffice(queueReceiver, frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup queue/front office 
     *  issues. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueFrontOfficeSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueFrontOffice() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueFrontOfficeSession getQueueFrontOfficeSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueFrontOfficeSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning queues 
     *  to frontOffices. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueFrontOfficeAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueFrontOfficeAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueFrontOfficeAssignmentSession getQueueFrontOfficeAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueFrontOfficeAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage queue smart 
     *  frontOffices. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> FrontOffice 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueSmartFrontOfficeSession </code> 
     *  @throws org.osid.NotFoundException no front office found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueSmartFrontOffice() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueSmartFrontOfficeSession getQueueSmartFrontOfficeSession(org.osid.id.Id frontOfficeId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueSmartFrontOfficeSession(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  resourcing service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> QueueResourcingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueResourcing() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueResourcingSession getQueueResourcingSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueResourcingSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the queue 
     *  resourcing service for the given front office. 
     *
     *  @param  frontOfficeId the <code> Id </code> of the <code> Queue 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> QueueResourcingSession </code> 
     *  @throws org.osid.NotFoundException no <code> FrontOffice </code> found 
     *          by the given Id 
     *  @throws org.osid.NullArgumentException <code> frontOfficeId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQueueResourcing() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.QueueResourcingSession getQueueResourcingSessionForFrontOffice(org.osid.id.Id frontOfficeId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getQueueResourcingSessionForFrontOffice(frontOfficeId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeLookupSession getFrontOfficeLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFrontOfficeLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQuerySession getFrontOfficeQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFrontOfficeQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeSearchSession getFrontOfficeSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFrontOfficeSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeAdminSession getFrontOfficeAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFrontOfficeAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  notification service. 
     *
     *  @param  frontOfficeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> frontOfficeReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeNotificationSession getFrontOfficeNotificationSession(org.osid.tracking.FrontOfficeReceiver frontOfficeReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFrontOfficeNotificationSession(frontOfficeReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FrontOfficeHierarchySession </code> for frontOffices 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeHierarchySession getFrontOfficeHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFrontOfficeHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the front office 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for frontOffices 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFrontOfficeHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeHierarchyDesignSession getFrontOfficeHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFrontOfficeHierarchyDesignSession(proxy));
    }


    /**
     *  Gets a <code> TrackingRulesProxyManager. </code> 
     *
     *  @return a <code> TrackingRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTrackingRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.tracking.rules.TrackingRulesProxyManager getTrackingRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTrackingRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
