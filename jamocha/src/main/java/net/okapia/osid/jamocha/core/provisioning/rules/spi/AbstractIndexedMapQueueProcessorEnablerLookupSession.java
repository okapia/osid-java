//
// AbstractIndexedMapQueueProcessorEnablerLookupSession.java
//
//    A simple framework for providing a QueueProcessorEnabler lookup service
//    backed by a fixed collection of queue processor enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a QueueProcessorEnabler lookup service backed by a
 *  fixed collection of queue processor enablers. The queue processor enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some queue processor enablers may be compatible
 *  with more types than are indicated through these queue processor enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>QueueProcessorEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapQueueProcessorEnablerLookupSession
    extends AbstractMapQueueProcessorEnablerLookupSession
    implements org.osid.provisioning.rules.QueueProcessorEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.QueueProcessorEnabler> queueProcessorEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.QueueProcessorEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.QueueProcessorEnabler> queueProcessorEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.QueueProcessorEnabler>());


    /**
     *  Makes a <code>QueueProcessorEnabler</code> available in this session.
     *
     *  @param  queueProcessorEnabler a queue processor enabler
     *  @throws org.osid.NullArgumentException <code>queueProcessorEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putQueueProcessorEnabler(org.osid.provisioning.rules.QueueProcessorEnabler queueProcessorEnabler) {
        super.putQueueProcessorEnabler(queueProcessorEnabler);

        this.queueProcessorEnablersByGenus.put(queueProcessorEnabler.getGenusType(), queueProcessorEnabler);
        
        try (org.osid.type.TypeList types = queueProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queueProcessorEnablersByRecord.put(types.getNextType(), queueProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a queue processor enabler from this session.
     *
     *  @param queueProcessorEnablerId the <code>Id</code> of the queue processor enabler
     *  @throws org.osid.NullArgumentException <code>queueProcessorEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeQueueProcessorEnabler(org.osid.id.Id queueProcessorEnablerId) {
        org.osid.provisioning.rules.QueueProcessorEnabler queueProcessorEnabler;
        try {
            queueProcessorEnabler = getQueueProcessorEnabler(queueProcessorEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.queueProcessorEnablersByGenus.remove(queueProcessorEnabler.getGenusType());

        try (org.osid.type.TypeList types = queueProcessorEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.queueProcessorEnablersByRecord.remove(types.getNextType(), queueProcessorEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeQueueProcessorEnabler(queueProcessorEnablerId);
        return;
    }


    /**
     *  Gets a <code>QueueProcessorEnablerList</code> corresponding to the given
     *  queue processor enabler genus <code>Type</code> which does not include
     *  queue processor enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known queue processor enablers or an error results. Otherwise,
     *  the returned list may contain only those queue processor enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  queueProcessorEnablerGenusType a queue processor enabler genus type 
     *  @return the returned <code>QueueProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerList getQueueProcessorEnablersByGenusType(org.osid.type.Type queueProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueprocessorenabler.ArrayQueueProcessorEnablerList(this.queueProcessorEnablersByGenus.get(queueProcessorEnablerGenusType)));
    }


    /**
     *  Gets a <code>QueueProcessorEnablerList</code> containing the given
     *  queue processor enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known queue processor enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  queue processor enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  queueProcessorEnablerRecordType a queue processor enabler record type 
     *  @return the returned <code>queueProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnablerList getQueueProcessorEnablersByRecordType(org.osid.type.Type queueProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.queueprocessorenabler.ArrayQueueProcessorEnablerList(this.queueProcessorEnablersByRecord.get(queueProcessorEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.queueProcessorEnablersByGenus.clear();
        this.queueProcessorEnablersByRecord.clear();

        super.close();

        return;
    }
}
