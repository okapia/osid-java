//
// AbstractTerm.java
//
//     Defines a Term.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.term.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Term</code>.
 */

public abstract class AbstractTerm
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.course.Term {

    private org.osid.locale.DisplayText displayLabel;
    private org.osid.calendaring.DateTime openDate;
    private org.osid.calendaring.DateTime registrationStart;
    private org.osid.calendaring.DateTime registrationEnd;
    private org.osid.calendaring.DateTime classesStart;
    private org.osid.calendaring.DateTime classesEnd;
    private org.osid.calendaring.DateTime addDate;
    private org.osid.calendaring.DateTime dropDate;
    private org.osid.calendaring.DateTime finalExamStart;
    private org.osid.calendaring.DateTime finalExamEnd;
    private org.osid.calendaring.DateTime closeDate;

    private final java.util.Collection<org.osid.course.records.TermRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets a display label for this term which may be less formal than the 
     *  display name. 
     *
     *  @return the term label 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.displayLabel);
    }


    /**
     *  Sets the display label.
     *
     *  @param label a display label
     *  @throws org.osid.NullArgumentException
     *          <code>label</code> is <code>null</code>
     */

    protected void setDisplayLabel(org.osid.locale.DisplayText label) {
        nullarg(label, "display label");
        this.displayLabel = label;
        return;
    }


    /**
     *  Tests if this term has an open date when published offerings are 
     *  finalized. 
     *
     *  @return <code> true </code> if there is an open date associated with 
     *        q  this term, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasOpenDate() {
        return (this.openDate != null);
    }


    /**
     *  Gets the open date when published offerings are finalized. 
     *
     *  @return the open date 
     *  @throws org.osid.IllegalStateException <code> hasOpenDate()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getOpenDate() {
        if (!hasOpenDate()) {
            throw new org.osid.IllegalStateException("hasOpenDate() is false");
        }

        return (this.openDate);
    }


    /**
     *  Sets the open date.
     *
     *  @param date an open date
     *  @throws org.osid.NullArgumentException <code>date</code>
     *          is <code>null</code>
     */

    protected void setOpenDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "open date");
        this.openDate = date;
        return;
    }


    /**
     *  Tests if this term has a registration period. 
     *
     *  @return <code> true </code> if there is a registration period 
     *          associated with this term, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasRegistrationPeriod() {
        return ((this.registrationStart != null) && (this.registrationEnd != null));
    }


    /**
     *  Gets the start of the registration period. 
     *
     *  @return the start date 
     *  @throws org.osid.IllegalStateException <code> hasRegistrationPeriod() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getRegistrationStart() {
        if (!hasRegistrationPeriod()) {
            throw new org.osid.IllegalStateException("hasRegistrationPeriod() is false");
        }

        return (this.registrationStart);
    }


    /**
     *  Sets the registration start.
     *
     *  @param date a registration start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setRegistrationStart(org.osid.calendaring.DateTime date) {
        nullarg(date, "registration start date");
        this.registrationStart = date;
        return;
    }


    /**
     *  Gets the end of the registration period. 
     *
     *  @return the end date 
     *  @throws org.osid.IllegalStateException <code> hasRegistrationPeriod() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getRegistrationEnd() {
        if (!hasRegistrationPeriod()) {
            throw new org.osid.IllegalStateException("hasRegistrationPeriod() is false");
        }

        return (this.registrationEnd);
    }


    /**
     *  Sets the registration end.
     *
     *  @param date a registration end
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setRegistrationEnd(org.osid.calendaring.DateTime date) {
        nullarg(date, "registration end date");
        this.registrationEnd = date;
        return;
    }


    /**
     *  Gets the start of classes. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClassesStart() {
        return (this.classesStart);
    }


    /**
     *  Sets the classes start.
     *
     *  @param date a classes start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setClassesStart(org.osid.calendaring.DateTime date) {
        nullarg(date, "classes start date");
        this.classesStart = date;
        return;
    }


    /**
     *  Gets the end of classes. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getClassesEnd() {
        return (this.classesEnd);
    }


    /**
     *  Sets the classes end.
     *
     *  @param date a classes end
     *  @throws org.osid.NullArgumentException <code>date</code>
     *          is <code>null</code>
     */

    protected void setClassesEnd(org.osid.calendaring.DateTime date) {
        nullarg(date, "classes end date");
        this.classesEnd = date;
        return;
    }


    /**
     *  Tests if this term has an add/drop date. 
     *
     *  @return <code> true </code> if there is an add/drop date
     *          associated with this term, <code> false </code>
     *          otherwise
     */

    @OSID @Override
    public boolean hasAddDropDate() {
        return ((this.addDate != null) && (this.dropDate != null));
    }


    /**
     *  Gets the add date. 
     *
     *  @return the add date 
     *  @throws org.osid.IllegalStateException <code> hasAddDropDate()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getAddDate() {
        if (!hasAddDropDate()) {
            throw new org.osid.IllegalStateException("hasAddDropDate() is false");
        }

        return (this.addDate);
    }


    /**
     *  Sets the add date.
     *
     *  @param date the add date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setAddDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "add date");
        this.addDate = date;
        return;
    }


    /**
     *  Gets the drop date. 
     *
     *  @return the drop date 
     *  @throws org.osid.IllegalStateException <code> hasAddDropDate()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDropDate() {
        if (!hasAddDropDate()) {
            throw new org.osid.IllegalStateException("hasAddDropDate() is false");
        }

        return (this.dropDate);
    }


    /**
     *  Sets the drop date.
     *
     *  @param date the drop date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDropDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "drop date");
        this.dropDate = date;
        return;
    }


    /**
     *  Tests if this term has a final eam period. 
     *
     *  @return <code> true </code> if there is a final exam period
     *          associated with this term, <code> false </code>
     *          otherwise
     */

    @OSID @Override
    public boolean hasFinalExamPeriod() {
        return ((this.finalExamStart != null) && (this.finalExamEnd != null));
    }


    /**
     *  Gets the start of the final exam period. 
     *
     *  @return the start date 
     *  @throws org.osid.IllegalStateException <code>
     *          hasFinalExamPeriod() </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getFinalExamStart() {
        if (!hasFinalExamPeriod()) {
            throw new org.osid.IllegalStateException("hasFinalExamPeriod() is false");
        }

        return (this.finalExamStart);
    }


    /**
     *  Sets the final exam start.
     *
     *  @param date a final exam start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setFinalExamStart(org.osid.calendaring.DateTime date) {
        nullarg(date, "final exam start date");
        this.finalExamStart = date;
        return;
    }


    /**
     *  Gets the end of the final exam period. 
     *
     *  @return the end date 
     *  @throws org.osid.IllegalStateException <code> hasFinalExamPeriod() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getFinalExamEnd() {
        if (!hasFinalExamPeriod()) {
            throw new org.osid.IllegalStateException("hasFinalExamPeriod() is false");
        }

        return (this.finalExamEnd);
    }


    /**
     *  Sets the final exam end.
     *
     *  @param date a final exam end
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setFinalExamEnd(org.osid.calendaring.DateTime date) {
        nullarg(date, "final exam end date");
        this.finalExamEnd = date;
        return;
    }


    /**
     *  Tests if this term has a close date when results from course
     *  offerings are finalized.
     *
     *  @return <code> true </code> if there is a close date
     *          associated with this term, <code> false </code>
     *          otherwise
     */

    @OSID @Override
    public boolean hasCloseDate() {
        return (this.closeDate != null);
    }


    /**
     *  Gets the close date when results of course offerings are
     *  finalized.
     *
     *  @return the close date 
     *  @throws org.osid.IllegalStateException <code> hasCloseDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCloseDate() {
        if (!hasCloseDate()) {
            throw new org.osid.IllegalStateException("getCloseDate() is false");
        }

        return (this.closeDate);
    }


    /**
     *  Sets the close date.
     *
     *  @param date a close date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setCloseDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "close date");
        this.closeDate = date;
        return;
    }


    /**
     *  Tests if this term supports the given record
     *  <code>Type</code>.
     *
     *  @param  termRecordType a term record type 
     *  @return <code>true</code> if the termRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type termRecordType) {
        for (org.osid.course.records.TermRecord record : this.records) {
            if (record.implementsRecordType(termRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given <code>Term</code>
     *  record <code>Type</code>.
     *
     *  @param  termRecordType the term record type 
     *  @return the term record 
     *  @throws org.osid.NullArgumentException
     *          <code>termRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(termRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.TermRecord getTermRecord(org.osid.type.Type termRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.TermRecord record : this.records) {
            if (record.implementsRecordType(termRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(termRecordType + " is not supported");
    }


    /**
     *  Adds a record to this term. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param termRecord the term record
     *  @param termRecordType term record type
     *  @throws org.osid.NullArgumentException
     *          <code>termRecord</code> or
     *          <code>termRecordTypeterm</code> is
     *          <code>null</code>
     */
            
    protected void addTermRecord(org.osid.course.records.TermRecord termRecord, 
                                 org.osid.type.Type termRecordType) {
        
        nullarg(termRecord, "term record");
        addRecordType(termRecordType);
        this.records.add(termRecord);
        
        return;
    }
}
