//
// QueueConstrainerElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueconstrainer.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class QueueConstrainerElements
    extends net.okapia.osid.jamocha.spi.OsidConstrainerElements {


    /**
     *  Gets the QueueConstrainerElement Id.
     *
     *  @return the queue constrainer element Id
     */

    public static org.osid.id.Id getQueueConstrainerEntityId() {
        return (makeEntityId("osid.provisioning.rules.QueueConstrainer"));
    }


    /**
     *  Gets the SizeLimit element Id.
     *
     *  @return the SizeLimit element Id
     */

    public static org.osid.id.Id getSizeLimit() {
        return (makeElementId("osid.provisioning.rules.queueconstrainer.SizeLimit"));
    }


    /**
     *  Gets the RequiredProvisionPoolIds element Id.
     *
     *  @return the RequiredProvisionPoolIds element Id
     */

    public static org.osid.id.Id getRequiredProvisionPoolIds() {
        return (makeElementId("osid.provisioning.rules.queueconstrainer.RequiredProvisionPoolIds"));
    }


    /**
     *  Gets the RequiredProvisionPools element Id.
     *
     *  @return the RequiredProvisionPools element Id
     */

    public static org.osid.id.Id getRequiredProvisionPools() {
        return (makeElementId("osid.provisioning.rules.queueconstrainer.RequiredProvisionPools"));
    }


    /**
     *  Gets the RequiresProvisions element Id.
     *
     *  @return the RequiresProvisions element Id
     */

    public static org.osid.id.Id getRequiresProvisions() {
        return (makeQueryElementId("osid.provisioning.rules.queueconstrainer.RequiresProvisions"));
    }


    /**
     *  Gets the RuledQueueId element Id.
     *
     *  @return the RuledQueueId element Id
     */

    public static org.osid.id.Id getRuledQueueId() {
        return (makeQueryElementId("osid.provisioning.rules.queueconstrainer.RuledQueueId"));
    }


    /**
     *  Gets the RuledQueue element Id.
     *
     *  @return the RuledQueue element Id
     */

    public static org.osid.id.Id getRuledQueue() {
        return (makeQueryElementId("osid.provisioning.rules.queueconstrainer.RuledQueue"));
    }


    /**
     *  Gets the DistributorId element Id.
     *
     *  @return the DistributorId element Id
     */

    public static org.osid.id.Id getDistributorId() {
        return (makeQueryElementId("osid.provisioning.rules.queueconstrainer.DistributorId"));
    }


    /**
     *  Gets the Distributor element Id.
     *
     *  @return the Distributor element Id
     */

    public static org.osid.id.Id getDistributor() {
        return (makeQueryElementId("osid.provisioning.rules.queueconstrainer.Distributor"));
    }
}
