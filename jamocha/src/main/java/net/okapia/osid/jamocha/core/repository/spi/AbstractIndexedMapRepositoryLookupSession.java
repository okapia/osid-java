//
// AbstractIndexedMapRepositoryLookupSession.java
//
//    A simple framework for providing a Repository lookup service
//    backed by a fixed collection of repositories with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Repository lookup service backed by a
 *  fixed collection of repositories. The repositories are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some repositories may be compatible
 *  with more types than are indicated through these repository
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Repositories</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRepositoryLookupSession
    extends AbstractMapRepositoryLookupSession
    implements org.osid.repository.RepositoryLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.repository.Repository> repositoriesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.repository.Repository>());
    private final MultiMap<org.osid.type.Type, org.osid.repository.Repository> repositoriesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.repository.Repository>());


    /**
     *  Makes a <code>Repository</code> available in this session.
     *
     *  @param  repository a repository
     *  @throws org.osid.NullArgumentException <code>repository<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRepository(org.osid.repository.Repository repository) {
        super.putRepository(repository);

        this.repositoriesByGenus.put(repository.getGenusType(), repository);
        
        try (org.osid.type.TypeList types = repository.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.repositoriesByRecord.put(types.getNextType(), repository);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a repository from this session.
     *
     *  @param repositoryId the <code>Id</code> of the repository
     *  @throws org.osid.NullArgumentException <code>repositoryId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRepository(org.osid.id.Id repositoryId) {
        org.osid.repository.Repository repository;
        try {
            repository = getRepository(repositoryId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.repositoriesByGenus.remove(repository.getGenusType());

        try (org.osid.type.TypeList types = repository.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.repositoriesByRecord.remove(types.getNextType(), repository);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRepository(repositoryId);
        return;
    }


    /**
     *  Gets a <code>RepositoryList</code> corresponding to the given
     *  repository genus <code>Type</code> which does not include
     *  repositories of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known repositories or an error results. Otherwise,
     *  the returned list may contain only those repositories that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  repositoryGenusType a repository genus type 
     *  @return the returned <code>Repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByGenusType(org.osid.type.Type repositoryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.repository.ArrayRepositoryList(this.repositoriesByGenus.get(repositoryGenusType)));
    }


    /**
     *  Gets a <code>RepositoryList</code> containing the given
     *  repository record <code>Type</code>. In plenary mode, the
     *  returned list contains all known repositories or an error
     *  results. Otherwise, the returned list may contain only those
     *  repositories that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  repositoryRecordType a repository record type 
     *  @return the returned <code>repository</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>repositoryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.repository.RepositoryList getRepositoriesByRecordType(org.osid.type.Type repositoryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.repository.repository.ArrayRepositoryList(this.repositoriesByRecord.get(repositoryRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.repositoriesByGenus.clear();
        this.repositoriesByRecord.clear();

        super.close();

        return;
    }
}
