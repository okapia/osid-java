//
// AbstractCyclicEventSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.cyclicevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCyclicEventSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.cycle.CyclicEventSearchResults {

    private org.osid.calendaring.cycle.CyclicEventList cyclicEvents;
    private final org.osid.calendaring.cycle.CyclicEventQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.cycle.records.CyclicEventSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCyclicEventSearchResults.
     *
     *  @param cyclicEvents the result set
     *  @param cyclicEventQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>cyclicEvents</code>
     *          or <code>cyclicEventQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCyclicEventSearchResults(org.osid.calendaring.cycle.CyclicEventList cyclicEvents,
                                            org.osid.calendaring.cycle.CyclicEventQueryInspector cyclicEventQueryInspector) {
        nullarg(cyclicEvents, "cyclic events");
        nullarg(cyclicEventQueryInspector, "cyclic event query inspectpr");

        this.cyclicEvents = cyclicEvents;
        this.inspector = cyclicEventQueryInspector;

        return;
    }


    /**
     *  Gets the cyclic event list resulting from a search.
     *
     *  @return a cyclic event list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventList getCyclicEvents() {
        if (this.cyclicEvents == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.cycle.CyclicEventList cyclicEvents = this.cyclicEvents;
        this.cyclicEvents = null;
	return (cyclicEvents);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.cycle.CyclicEventQueryInspector getCyclicEventQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  cyclic event search record <code> Type. </code> This method must
     *  be used to retrieve a cyclicEvent implementing the requested
     *  record.
     *
     *  @param cyclicEventSearchRecordType a cyclicEvent search 
     *         record type 
     *  @return the cyclic event search
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicEventSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(cyclicEventSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.cycle.records.CyclicEventSearchResultsRecord getCyclicEventSearchResultsRecord(org.osid.type.Type cyclicEventSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.cycle.records.CyclicEventSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(cyclicEventSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(cyclicEventSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record cyclic event search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCyclicEventRecord(org.osid.calendaring.cycle.records.CyclicEventSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "cyclic event record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
