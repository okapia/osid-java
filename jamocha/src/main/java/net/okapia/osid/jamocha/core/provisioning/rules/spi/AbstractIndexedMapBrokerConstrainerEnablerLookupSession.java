//
// AbstractIndexedMapBrokerConstrainerEnablerLookupSession.java
//
//    A simple framework for providing a BrokerConstrainerEnabler lookup service
//    backed by a fixed collection of broker constrainer enablers with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a BrokerConstrainerEnabler lookup service backed by a
 *  fixed collection of broker constrainer enablers. The broker constrainer enablers are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some broker constrainer enablers may be compatible
 *  with more types than are indicated through these broker constrainer enabler
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BrokerConstrainerEnablers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapBrokerConstrainerEnablerLookupSession
    extends AbstractMapBrokerConstrainerEnablerLookupSession
    implements org.osid.provisioning.rules.BrokerConstrainerEnablerLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.BrokerConstrainerEnabler> brokerConstrainerEnablersByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.BrokerConstrainerEnabler>());
    private final MultiMap<org.osid.type.Type, org.osid.provisioning.rules.BrokerConstrainerEnabler> brokerConstrainerEnablersByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.provisioning.rules.BrokerConstrainerEnabler>());


    /**
     *  Makes a <code>BrokerConstrainerEnabler</code> available in this session.
     *
     *  @param  brokerConstrainerEnabler a broker constrainer enabler
     *  @throws org.osid.NullArgumentException <code>brokerConstrainerEnabler<code> is
     *          <code>null</code>
     */

    @Override
    protected void putBrokerConstrainerEnabler(org.osid.provisioning.rules.BrokerConstrainerEnabler brokerConstrainerEnabler) {
        super.putBrokerConstrainerEnabler(brokerConstrainerEnabler);

        this.brokerConstrainerEnablersByGenus.put(brokerConstrainerEnabler.getGenusType(), brokerConstrainerEnabler);
        
        try (org.osid.type.TypeList types = brokerConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokerConstrainerEnablersByRecord.put(types.getNextType(), brokerConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a broker constrainer enabler from this session.
     *
     *  @param brokerConstrainerEnablerId the <code>Id</code> of the broker constrainer enabler
     *  @throws org.osid.NullArgumentException <code>brokerConstrainerEnablerId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeBrokerConstrainerEnabler(org.osid.id.Id brokerConstrainerEnablerId) {
        org.osid.provisioning.rules.BrokerConstrainerEnabler brokerConstrainerEnabler;
        try {
            brokerConstrainerEnabler = getBrokerConstrainerEnabler(brokerConstrainerEnablerId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.brokerConstrainerEnablersByGenus.remove(brokerConstrainerEnabler.getGenusType());

        try (org.osid.type.TypeList types = brokerConstrainerEnabler.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.brokerConstrainerEnablersByRecord.remove(types.getNextType(), brokerConstrainerEnabler);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeBrokerConstrainerEnabler(brokerConstrainerEnablerId);
        return;
    }


    /**
     *  Gets a <code>BrokerConstrainerEnablerList</code> corresponding to the given
     *  broker constrainer enabler genus <code>Type</code> which does not include
     *  broker constrainer enablers of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known broker constrainer enablers or an error results. Otherwise,
     *  the returned list may contain only those broker constrainer enablers that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  brokerConstrainerEnablerGenusType a broker constrainer enabler genus type 
     *  @return the returned <code>BrokerConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByGenusType(org.osid.type.Type brokerConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerconstrainerenabler.ArrayBrokerConstrainerEnablerList(this.brokerConstrainerEnablersByGenus.get(brokerConstrainerEnablerGenusType)));
    }


    /**
     *  Gets a <code>BrokerConstrainerEnablerList</code> containing the given
     *  broker constrainer enabler record <code>Type</code>. In plenary mode, the
     *  returned list contains all known broker constrainer enablers or an error
     *  results. Otherwise, the returned list may contain only those
     *  broker constrainer enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  brokerConstrainerEnablerRecordType a broker constrainer enabler record type 
     *  @return the returned <code>brokerConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>brokerConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerEnablerList getBrokerConstrainerEnablersByRecordType(org.osid.type.Type brokerConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerconstrainerenabler.ArrayBrokerConstrainerEnablerList(this.brokerConstrainerEnablersByRecord.get(brokerConstrainerEnablerRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.brokerConstrainerEnablersByGenus.clear();
        this.brokerConstrainerEnablersByRecord.clear();

        super.close();

        return;
    }
}
