//
// AbstractStatisticQuery.java
//
//     A template for making a Statistic Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.statistic.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for statistics.
 */

public abstract class AbstractStatisticQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendiumQuery
    implements org.osid.metering.StatisticQuery {

    private final java.util.Collection<org.osid.metering.records.StatisticQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets a meter <code> Id. </code> 
     *
     *  @param  meterId a meter <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> meterId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMeterId(org.osid.id.Id meterId, boolean match) {
        return;
    }


    /**
     *  Clears the meter <code> Id </code> query term. 
     */

    @OSID @Override
    public void clearMeterIdTerms() {
        return;
    }


    /**
     *  Tests if a meter query is available. 
     *
     *  @return <code> true </code> if a meter query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeterQuery() {
        return (false);
    }


    /**
     *  Gets the query for a meter. 
     *
     *  @return the meter query 
     *  @throws org.osid.UnimplementedException <code> supportsMeterQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.metering.MeterQuery getMeterQuery() {
        throw new org.osid.UnimplementedException("supportsMeterQuery() is false");
    }


    /**
     *  Clears the meter query terms. 
     */

    @OSID @Override
    public void clearMeterTerms() {
        return;
    }


    /**
     *  Sets an object <code> Id. </code> 
     *
     *  @param  objectId an object <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> objectId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMeteredObjectId(org.osid.id.Id objectId, boolean match) {
        return;
    }


    /**
     *  Clears the object query terms. 
     */

    @OSID @Override
    public void clearMeteredObjectIdTerms() {
        return;
    }


    /**
     *  Sets the time for the statistics to generate. 
     *
     *  @param  start the start time 
     *  @param  end the end time 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void setTimeRange(org.osid.calendaring.DateTime start, 
                             org.osid.calendaring.DateTime end) {
        return;
    }


    /**
     *  Matches statistics with the sum between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchSum(java.math.BigDecimal low, java.math.BigDecimal high, 
                         boolean match) {
        return;
    }


    /**
     *  Clears the sum query terms. 
     */

    @OSID @Override
    public void clearSumTerms() {
        return;
    }


    /**
     *  Matches statistics with the minimum sum value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumSum(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum sum query terms. 
     */

    @OSID @Override
    public void clearMinimumSumTerms() {
        return;
    }


    /**
     *  Matches statistics with the mean between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchMean(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        return;
    }


    /**
     *  Clears the mean query terms. 
     */

    @OSID @Override
    public void clearMeanTerms() {
        return;
    }


    /**
     *  Matches statistics with the minimum mean value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMean(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum mean query terms. 
     */

    @OSID @Override
    public void clearMinimumMeanTerms() {
        return;
    }


    /**
     *  Matches statistics with the median between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchMedian(java.math.BigDecimal low, 
                            java.math.BigDecimal high, boolean match) {
        return;
    }


    /**
     *  Clears the median query terms. 
     */

    @OSID @Override
    public void clearMedianTerms() {
        return;
    }


    /**
     *  Matches statistics with the minimum median value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMedian(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum median query terms. 
     */

    @OSID @Override
    public void clearMinimumMedianTerms() {
        return;
    }


    /**
     *  Matches statistics with the mode between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchMode(java.math.BigDecimal low, java.math.BigDecimal high, 
                          boolean match) {
        return;
    }


    /**
     *  Clears the mode query terms. 
     */

    @OSID @Override
    public void clearModeTerms() {
        return;
    }


    /**
     *  Matches statistics with the minimum mode value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumMode(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum mode query terms. 
     */

    @OSID @Override
    public void clearMinimumModeTerms() {
        return;
    }


    /**
     *  Matches statistics with the standard deviation between the given range 
     *  inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchStandardDeviation(java.math.BigDecimal low, 
                                       java.math.BigDecimal high, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the standard deviation query terms. 
     */

    @OSID @Override
    public void clearStandardDeviationTerms() {
        return;
    }


    /**
     *  Matches statistics with the minimum standard deviation value 
     *  inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumStandardDeviation(java.math.BigDecimal value, 
                                              boolean match) {
        return;
    }


    /**
     *  Clears the minimum standard deviation query terms. 
     */

    @OSID @Override
    public void clearMinimumStandardDeviationTerms() {
        return;
    }


    /**
     *  Matches statistics with the root mean square between the given range 
     *  inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchRMS(java.math.BigDecimal low, java.math.BigDecimal high, 
                         boolean match) {
        return;
    }


    /**
     *  Clears the rms query terms. 
     */

    @OSID @Override
    public void clearRMSTerms() {
        return;
    }


    /**
     *  Matches statistics with the minimum rms value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumRMS(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum rms query terms. 
     */

    @OSID @Override
    public void clearMinimumRMSTerms() {
        return;
    }


    /**
     *  Matches statistics with the delta between the given range inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchDelta(java.math.BigDecimal low, java.math.BigDecimal high, 
                           boolean match) {
        return;
    }


    /**
     *  Clears the delta query terms. 
     */

    @OSID @Override
    public void clearDeltaTerms() {
        return;
    }


    /**
     *  Matches statistics with the minimum delta value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumDelta(java.math.BigDecimal value, boolean match) {
        return;
    }


    /**
     *  Clears the minimum delta query terms. 
     */

    @OSID @Override
    public void clearMinimumDeltaTerms() {
        return;
    }


    /**
     *  Matches statistics with the percentage change between the given range 
     *  inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchPercentChange(java.math.BigDecimal low, 
                                   java.math.BigDecimal high, boolean match) {
        return;
    }


    /**
     *  Clears the delta query terms. 
     */

    @OSID @Override
    public void clearPercentChangeTerms() {
        return;
    }


    /**
     *  Matches statistics with the minimum percentage change value inclusive. 
     *
     *  @param  value the low end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     */

    @OSID @Override
    public void matchMinimumPercentChange(java.math.BigDecimal value, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the minimum percentage change query terms. 
     */

    @OSID @Override
    public void clearMinimumPercentChangeTerms() {
        return;
    }


    /**
     *  Matches statistics with the average rate between the given range 
     *  inclusive. 
     *
     *  @param  low the low end of the range 
     *  @param  high the high end of the range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException the low is greater than the 
     *          high 
     */

    @OSID @Override
    public void matchAverageRate(java.math.BigDecimal low, 
                                 java.math.BigDecimal high, boolean match) {
        return;
    }


    /**
     *  Clears the average rate query terms. 
     */

    @OSID @Override
    public void clearAverageRateTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given statistic query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a statistic implementing the requested record.
     *
     *  @param statisticRecordType a statistic record type
     *  @return the statistic query record
     *  @throws org.osid.NullArgumentException
     *          <code>statisticRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(statisticRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.StatisticQueryRecord getStatisticQueryRecord(org.osid.type.Type statisticRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.StatisticQueryRecord record : this.records) {
            if (record.implementsRecordType(statisticRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(statisticRecordType + " is not supported");
    }


    /**
     *  Adds a record to this statistic query. 
     *
     *  @param statisticQueryRecord statistic query record
     *  @param statisticRecordType statistic record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addStatisticQueryRecord(org.osid.metering.records.StatisticQueryRecord statisticQueryRecord, 
                                          org.osid.type.Type statisticRecordType) {

        addRecordType(statisticRecordType);
        nullarg(statisticQueryRecord, "statistic query record");
        this.records.add(statisticQueryRecord);        
        return;
    }
}
