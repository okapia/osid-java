//
// AbstractMappingRouteManager.java
//
//     An adapter for a MappingRouteManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.mapping.route.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MappingRouteManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMappingRouteManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.mapping.route.MappingRouteManager>
    implements org.osid.mapping.route.MappingRouteManager {


    /**
     *  Constructs a new {@code AbstractAdapterMappingRouteManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMappingRouteManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMappingRouteManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMappingRouteManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any map federation is exposed. Federation is exposed when a 
     *  specific map may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of maps appears 
     *  as a single map. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a location routing service is supported. 
     *
     *  @return <code> true </code> if a location routing service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationRouting() {
        return (getAdapteeManager().supportsLocationRouting());
    }


    /**
     *  Tests if a location service is supported for the current agent. 
     *
     *  @return <code> true </code> if my location is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyLocation() {
        return (getAdapteeManager().supportsMyLocation());
    }


    /**
     *  Tests if looking up routes is supported. 
     *
     *  @return <code> true </code> if route lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteLookup() {
        return (getAdapteeManager().supportsRouteLookup());
    }


    /**
     *  Tests if querying routes is supported. 
     *
     *  @return <code> true </code> if route query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteQuery() {
        return (getAdapteeManager().supportsRouteQuery());
    }


    /**
     *  Tests if searching routes is supported. 
     *
     *  @return <code> true </code> if route search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteSearch() {
        return (getAdapteeManager().supportsRouteSearch());
    }


    /**
     *  Tests if a route <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if route administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteAdmin() {
        return (getAdapteeManager().supportsRouteAdmin());
    }


    /**
     *  Tests if a route <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if route notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteNotification() {
        return (getAdapteeManager().supportsRouteNotification());
    }


    /**
     *  Tests if a route map lookup service is supported. 
     *
     *  @return <code> true </code> if a route map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteMap() {
        return (getAdapteeManager().supportsRouteMap());
    }


    /**
     *  Tests if a route map service is supported. 
     *
     *  @return <code> true </code> if a route to map assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteMapAssignment() {
        return (getAdapteeManager().supportsRouteMapAssignment());
    }


    /**
     *  Tests if a route smart map service is supported. 
     *
     *  @return <code> true </code> if a route smart map lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRouteSmartMap() {
        return (getAdapteeManager().supportsRouteSmartMap());
    }


    /**
     *  Tests if a resource route service is supported. 
     *
     *  @return <code> true </code> if a resource route service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRoute() {
        return (getAdapteeManager().supportsResourceRoute());
    }


    /**
     *  Tests if a resource route update service is supported. 
     *
     *  @return <code> true </code> if a resource route update service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRouteAssignment() {
        return (getAdapteeManager().supportsResourceRouteAssignment());
    }


    /**
     *  Tests if a resource route notification service is supported. 
     *
     *  @return <code> true </code> if a resource route notification service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceRouteNotification() {
        return (getAdapteeManager().supportsResourceRouteNotification());
    }


    /**
     *  Tests if a route service is supported for the current agent. 
     *
     *  @return <code> true </code> if my route is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyRoute() {
        return (getAdapteeManager().supportsMyRoute());
    }


    /**
     *  Gets the supported <code> Route </code> record types. 
     *
     *  @return a list containing the supported <code> Route </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRouteRecordTypes() {
        return (getAdapteeManager().getRouteRecordTypes());
    }


    /**
     *  Tests if the given <code> Route </code> record type is supported. 
     *
     *  @param  routeRecordType a <code> Type </code> indicating a <code> 
     *          Route </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> routeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRouteRecordType(org.osid.type.Type routeRecordType) {
        return (getAdapteeManager().supportsRouteRecordType(routeRecordType));
    }


    /**
     *  Gets the supported <code> Route </code> search record types. 
     *
     *  @return a list containing the supported <code> Route </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRouteSearchRecordTypes() {
        return (getAdapteeManager().getRouteSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Route </code> search record type is 
     *  supported. 
     *
     *  @param  routeSearchRecordType a <code> Type </code> indicating a 
     *          <code> Route </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> routeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRouteSearchRecordType(org.osid.type.Type routeSearchRecordType) {
        return (getAdapteeManager().supportsRouteSearchRecordType(routeSearchRecordType));
    }


    /**
     *  Gets the supported <code> RouteSegment </code> record types. 
     *
     *  @return a list containing the supported <code> RouteSegment </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRouteSegmentRecordTypes() {
        return (getAdapteeManager().getRouteSegmentRecordTypes());
    }


    /**
     *  Tests if the given <code> RouteSegment </code> record type is 
     *  supported. 
     *
     *  @param  routeSegmentRecordType a <code> Type </code> indicating a 
     *          <code> RouteSegment </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supportsRouteSegmentRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRouteSegmentRecordType(org.osid.type.Type routeSegmentRecordType) {
        return (getAdapteeManager().supportsRouteSegmentRecordType(routeSegmentRecordType));
    }


    /**
     *  Gets the supported <code> RouteProgresst </code> record types. 
     *
     *  @return a list containing the supported <code> RouteProgress </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRouteProgressRecordTypes() {
        return (getAdapteeManager().getRouteProgressRecordTypes());
    }


    /**
     *  Tests if the given <code> RouteProgress </code> record type is 
     *  supported. 
     *
     *  @param  routeProgressRecordType a <code> Type </code> indicating a 
     *          <code> RouteProgress </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          supportsRouteProgressRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRouteProgressRecordType(org.osid.type.Type routeProgressRecordType) {
        return (getAdapteeManager().supportsRouteProgressRecordType(routeProgressRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  routing service. 
     *
     *  @return a <code> RoutingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouting() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RoutingSession getRoutingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRoutingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the location 
     *  routing service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RoutingSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouting() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RoutingSession getRoutingSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRoutingSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route lookup 
     *  service. 
     *
     *  @return a <code> RouteLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteLookupSession getRouteLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route lookup 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> RouteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteLookupSession getRouteLookupSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteLookupSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route query 
     *  service. 
     *
     *  @return a <code> RouteQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuerySession getRouteQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route query 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> RouteQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteQuerySession getRouteQuerySessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteQuerySessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route search 
     *  service. 
     *
     *  @return a <code> RouteSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSearchSession getRouteSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route search 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RouteSearchSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSearchSession getRouteSearchSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteSearchSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  administration service. 
     *
     *  @return a <code> RouteAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteAdminSession getRouteAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  administration service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RouteAdminSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteAdminSession getRouteAdminSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteAdminSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  notification service. 
     *
     *  @param  routeReceiver the notification callback 
     *  @return a <code> RouteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> routeReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteNotificationSession getRouteNotificationSession(org.osid.mapping.route.RouteReceiver routeReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteNotificationSession(routeReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the route 
     *  notification service for the given map. 
     *
     *  @param  routeReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RouteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> routeReceiver </code> or 
     *          <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteNotificationSession getRouteNotificationSessionForMap(org.osid.mapping.route.RouteReceiver routeReceiver, 
                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteNotificationSessionForMap(routeReceiver, mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup route/map mappings. 
     *
     *  @return a <code> RouteMapSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteMapSession getRouteMapSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteMapSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning routes 
     *  to maps. 
     *
     *  @return a <code> LocationMapAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRouteMapAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteMapAssignmentSession getRouteMapAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteMapAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage route smart maps. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> RouteSmartMapSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRouteSmartMap() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.RouteSmartMapSession getRouteSmartMapSession(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRouteSmartMapSession(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  service. 
     *
     *  @return a <code> ResourceRouteSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceRoute() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteSession getResourceRouteSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRouteSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceRouteSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsResourceRoute() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteSession getResourceRouteSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRouteSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  assignment service. 
     *
     *  @return a <code> ResourceRouteAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteAssignmentSession getResourceRouteAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRouteAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  assignment service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceRouteAssignmentSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteAssignmentSession getResourceRouteAssignmentSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRouteAssignmentSessionForMap(mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  notification service. 
     *
     *  @param  resourceRouteReceiver the notification callback 
     *  @return a <code> ResourceRouteNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> resourceRouteReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteNotificationSession getResourceRouteNotificationSession(org.osid.mapping.route.ResourceRouteReceiver resourceRouteReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRouteNotificationSession(resourceRouteReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the resource route 
     *  notification service for the given map. 
     *
     *  @param  resourceRouteReceiver the notification callback 
     *  @param  mapId the <code> Id </code> of the <code> Map </code> 
     *  @return a <code> ResourceRouteNotificationSession </code> 
     *  @throws org.osid.NotFoundException no map found by the given <code> Id 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> resourceRouteReceiver 
     *          </code> or <code> mapId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceRouteNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.ResourceRouteNotificationSession getResourceRouteNotificationSessionForMap(org.osid.mapping.route.ResourceRouteReceiver resourceRouteReceiver, 
                                                                                                             org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getResourceRouteNotificationSessionForMap(resourceRouteReceiver, mapId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my route 
     *  service. 
     *
     *  @return a <code> MyRouteLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyRouteLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.MyRouteSession getMyRouteSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyRouteSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the my route 
     *  service for the given map. 
     *
     *  @param  mapId the <code> Id </code> of the map 
     *  @return a <code> MyRouteLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Map </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMyRouteLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.route.MyRouteSession getMyRouteSessionForMap(org.osid.id.Id mapId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyRouteSessionForMap(mapId));
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
