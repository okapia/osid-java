//
// AbstractQueryCourseCatalogLookupSession.java
//
//    An inline adapter that maps a CourseCatalogLookupSession to
//    a CourseCatalogQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a CourseCatalogLookupSession to
 *  a CourseCatalogQuerySession.
 */

public abstract class AbstractQueryCourseCatalogLookupSession
    extends net.okapia.osid.jamocha.course.spi.AbstractCourseCatalogLookupSession
    implements org.osid.course.CourseCatalogLookupSession {

    private final org.osid.course.CourseCatalogQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryCourseCatalogLookupSession.
     *
     *  @param querySession the underlying course catalog query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryCourseCatalogLookupSession(org.osid.course.CourseCatalogQuerySession querySession) {
        nullarg(querySession, "course catalog query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>CourseCatalog</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCourseCatalogs() {
        return (this.session.canSearchCourseCatalogs());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>CourseCatalog</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>CourseCatalog</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>CourseCatalog</code> and
     *  retained for compatibility.
     *
     *  @param  courseCatalogId <code>Id</code> of the
     *          <code>CourseCatalog</code>
     *  @return the course catalog
     *  @throws org.osid.NotFoundException <code>courseCatalogId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>courseCatalogId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseCatalogQuery query = getQuery();
        query.matchId(courseCatalogId, true);
        org.osid.course.CourseCatalogList courseCatalogs = this.session.getCourseCatalogsByQuery(query);
        if (courseCatalogs.hasNext()) {
            return (courseCatalogs.getNextCourseCatalog());
        } 
        
        throw new org.osid.NotFoundException(courseCatalogId + " not found");
    }


    /**
     *  Gets a <code>CourseCatalogList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  courseCatalogs specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>CourseCatalogs</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  courseCatalogIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>CourseCatalog</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByIds(org.osid.id.IdList courseCatalogIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseCatalogQuery query = getQuery();

        try (org.osid.id.IdList ids = courseCatalogIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getCourseCatalogsByQuery(query));
    }


    /**
     *  Gets a <code>CourseCatalogList</code> corresponding to the given
     *  course catalog genus <code>Type</code> which does not include
     *  course catalogs of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  courseCatalogGenusType a courseCatalog genus type 
     *  @return the returned <code>CourseCatalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByGenusType(org.osid.type.Type courseCatalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseCatalogQuery query = getQuery();
        query.matchGenusType(courseCatalogGenusType, true);
        return (this.session.getCourseCatalogsByQuery(query));
    }


    /**
     *  Gets a <code>CourseCatalogList</code> corresponding to the given
     *  course catalog genus <code>Type</code> and include any additional
     *  course catalogs with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  courseCatalogGenusType a courseCatalog genus type 
     *  @return the returned <code>CourseCatalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByParentGenusType(org.osid.type.Type courseCatalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseCatalogQuery query = getQuery();
        query.matchParentGenusType(courseCatalogGenusType, true);
        return (this.session.getCourseCatalogsByQuery(query));
    }


    /**
     *  Gets a <code>CourseCatalogList</code> containing the given
     *  course catalog record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  courseCatalogRecordType a courseCatalog record type 
     *  @return the returned <code>CourseCatalog</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>courseCatalogRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByRecordType(org.osid.type.Type courseCatalogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseCatalogQuery query = getQuery();
        query.matchRecordType(courseCatalogRecordType, true);
        return (this.session.getCourseCatalogsByQuery(query));
    }


    /**
     *  Gets a <code>CourseCatalogList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known course catalogs or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  course catalogs that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>CourseCatalog</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.course.CourseCatalogQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getCourseCatalogsByQuery(query));        
    }

    
    /**
     *  Gets all <code>CourseCatalogs</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  course catalogs or an error results. Otherwise, the returned list
     *  may contain only those course catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>CourseCatalogs</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogList getCourseCatalogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.course.CourseCatalogQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getCourseCatalogsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.course.CourseCatalogQuery getQuery() {
        org.osid.course.CourseCatalogQuery query = this.session.getCourseCatalogQuery();
        
        return (query);
    }
}
