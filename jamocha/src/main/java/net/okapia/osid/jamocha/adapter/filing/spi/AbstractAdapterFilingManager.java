//
// AbstractFilingManager.java
//
//     An adapter for a FilingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.filing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a FilingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterFilingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.filing.FilingManager>
    implements org.osid.filing.FilingManager {


    /**
     *  Constructs a new {@code AbstractAdapterFilingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterFilingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterFilingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterFilingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if directory federation is exposed. Federation is exposed when a 
     *  specific directory may be identified, selected and used to access a 
     *  session. Federation is not exposed when a set of directories appears 
     *  as a single directory. 
     *
     *  @return <code> true </code> if federation is visible <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a <code> FileSystemSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> FileSystemSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSystem() {
        return (getAdapteeManager().supportsFileSystem());
    }


    /**
     *  Tests if a <code> FileSystemManagementSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> FileSystemManagementSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSystemManagement() {
        return (getAdapteeManager().supportsFileSystemManagement());
    }


    /**
     *  Tests if a <code> FileContentSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> FileContentSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileContent() {
        return (getAdapteeManager().supportsFileContent());
    }


    /**
     *  Tests if file lookup is supported. 
     *
     *  @return <code> true </code> if a <code> FileLookupSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileLookup() {
        return (getAdapteeManager().supportsFileLookup());
    }


    /**
     *  Tests if file querying is supported. 
     *
     *  @return <code> true </code> if a <code> FileQuerySession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileQuery() {
        return (getAdapteeManager().supportsFileQuery());
    }


    /**
     *  Tests if file searching is supported. 
     *
     *  @return <code> true </code> if a <code> FileSearchSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSearch() {
        return (getAdapteeManager().supportsFileSearch());
    }


    /**
     *  Tests if file notification is supported. 
     *
     *  @return <code> true </code> if a <code> FileNotificationSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileNotification() {
        return (getAdapteeManager().supportsFileNotification());
    }


    /**
     *  Tests if managing smart directories is supported. 
     *
     *  @return <code> true </code> if a <code> FileSmartDirectorySession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSmartDirectory() {
        return (getAdapteeManager().supportsFileSmartDirectory());
    }


    /**
     *  Tests if a <code> DirectoryLookupSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryLookupSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryLookup() {
        return (getAdapteeManager().supportsDirectoryLookup());
    }


    /**
     *  Tests if directory querying is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryQuerySession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryQuery() {
        return (getAdapteeManager().supportsDirectoryQuery());
    }


    /**
     *  Tests if directory searching is supported. 
     *
     *  @return <code> true </code> if a <code> DirectorySearchSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectorySearch() {
        return (getAdapteeManager().supportsDirectorySearch());
    }


    /**
     *  Tests if directory administration is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryAdminSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryAdmin() {
        return (getAdapteeManager().supportsDirectoryAdmin());
    }


    /**
     *  Tests if a directory <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryNotificationSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryNotification() {
        return (getAdapteeManager().supportsDirectoryNotification());
    }


    /**
     *  Tests if a file management service is supported. 
     *
     *  @return <code> true </code> if a <code> FileManagementSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingManagement() {
        return (getAdapteeManager().supportsFilingManagement());
    }


    /**
     *  Tests if a filing allocation service is supported. 
     *
     *  @return <code> true </code> if a <code> FilingAllocationManager 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingAllocation() {
        return (getAdapteeManager().supportsFilingAllocation());
    }


    /**
     *  Gets the supported file record types. 
     *
     *  @return a list containing the supported <code> File </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFileRecordTypes() {
        return (getAdapteeManager().getFileRecordTypes());
    }


    /**
     *  Tests if the given file record type is supported. 
     *
     *  @param  fileRecordType a <code> Type </code> indicating a file record 
     *          type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> fileRecordType </code> 
     *          is null 
     */

    @OSID @Override
    public boolean supportsFileRecordType(org.osid.type.Type fileRecordType) {
        return (getAdapteeManager().supportsFileRecordType(fileRecordType));
    }


    /**
     *  Gets the supported file search record types. 
     *
     *  @return a list containing the supported <code> File </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFileSearchRecordTypes() {
        return (getAdapteeManager().getFileSearchRecordTypes());
    }


    /**
     *  Tests if the given file search record type is supported. 
     *
     *  @param  fileSearchRecordType a <code> Type </code> indicating a file 
     *          search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> fileSearchRecordType 
     *          </code> is null 
     */

    @OSID @Override
    public boolean supportsFileSearchRecordType(org.osid.type.Type fileSearchRecordType) {
        return (getAdapteeManager().supportsFileSearchRecordType(fileSearchRecordType));
    }


    /**
     *  Gets the supported directory record types. 
     *
     *  @return a list containing the supported <code> Directory </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectoryRecordTypes() {
        return (getAdapteeManager().getDirectoryRecordTypes());
    }


    /**
     *  Tests if the given directory record type is supported. 
     *
     *  @param  directoryRecordType a <code> Type </code> indicating a 
     *          directory record type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> directoryRecordType 
     *          </code> is null 
     */

    @OSID @Override
    public boolean supportsDirectoryRecordType(org.osid.type.Type directoryRecordType) {
        return (getAdapteeManager().supportsDirectoryRecordType(directoryRecordType));
    }


    /**
     *  Gets the supported directory search record types. 
     *
     *  @return a list containing the supported <code> Directory </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectorySearchRecordTypes() {
        return (getAdapteeManager().getDirectorySearchRecordTypes());
    }


    /**
     *  Tests if the given directory search record type is supported. 
     *
     *  @param  directorySearchRecordType a <code> Type </code> indicating a 
     *          directory search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          directorySearchRecordType </code> is null 
     */

    @OSID @Override
    public boolean supportsDirectorySearchRecordType(org.osid.type.Type directorySearchRecordType) {
        return (getAdapteeManager().supportsDirectorySearchRecordType(directorySearchRecordType));
    }


    /**
     *  Gets the session for examining file systems. 
     *
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemSession());
    }


    /**
     *  Gets the session for exmaning file systems for the given path. 
     *
     *  @param  path the path to a directory 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not found or 
     *          is not a directory 
     *  @throws org.osid.NullArgumentException <code> path </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSessionForPath(String path)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemSessionForPath(path));
    }


    /**
     *  Gets the session for exmaning file systems for the given directory. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemSessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for manipulating file systems. 
     *
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemManagementSession());
    }


    /**
     *  Gets the session for manipulating files for the given path. 
     *
     *  @param  path the path to a directory 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not found or 
     *          is not a directory 
     *  @throws org.osid.NullArgumentException <code> path </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSessionForPath(String path)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemManagementSessionForPath(path));
    }


    /**
     *  Gets the session for manipulating files for the given path. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemManagementSessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for reading and writing files. 
     *
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileContentSession());
    }


    /**
     *  Gets the session for reading and writing files for the given path. 
     *
     *  @param  path the path to a directory 
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not found or 
     *          is not a directory 
     *  @throws org.osid.NullArgumentException <code> path </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSessionForPath(String path)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileContentSessionForPath(path));
    }


    /**
     *  Gets the session for reading and writing files for the given path. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileContentSessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for looking up files. 
     *
     *  @return the <code> FileLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileLookupSession getFileLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileLookupSession());
    }


    /**
     *  Gets the session for manipulating files for the given path. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileLookupSession getFileLookupSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileLookupSessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for querying files. 
     *
     *  @return the <code> FileQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuerySession getFileQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileQuerySession());
    }


    /**
     *  Gets a file query session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuerySession getFileQuerySessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileQuerySessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for searching for files. 
     *
     *  @return the <code> FileSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSearchSession getFileSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSearchSession());
    }


    /**
     *  Gets a file search session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSearchSession getFileSearchSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSearchSessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for receiving messages about changes to files. 
     *
     *  @param  fileReceiver the notification callback 
     *  @return <code> a FileNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> fileReceiver </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileNotificationSession getFileNotificationSession(org.osid.filing.FileReceiver fileReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileNotificationSession(fileReceiver));
    }


    /**
     *  Gets a file notification session for the specified directory. 
     *
     *  @param  fileReceiver the notification callback 
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> fileReceiver </code> or 
     *          <code> directoryId </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileNotificationSession getFileNotificationSessionForDirectory(org.osid.filing.FileReceiver fileReceiver, 
                                                                                          org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileNotificationSessionForDirectory(fileReceiver, directoryId));
    }


    /**
     *  Gets the session for managing dynamic directories. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> FileSmartDirectorySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSmartDirectory() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSmartDirectorySession getFileSmartDirectorySession(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSmartDirectorySession(directoryId));
    }


    /**
     *  Gets the session for examining directories. 
     *
     *  @return a <code> DirectoryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryLookupSession getDirectoryLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryLookupSession());
    }


    /**
     *  Gets the session for examining a given directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectoryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryLookupSession getDirectoryLookupSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryLookupSessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for querying directories. 
     *
     *  @return a <code> DirectoryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuerySession getDirectoryQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryQuerySession());
    }


    /**
     *  Gets the session for querying directories within a given directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectoryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuerySession getDirectoryQuerySessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryQuerySessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for searching for directories. 
     *
     *  @return a <code> DirectorySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectorySearchSession getDirectorySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectorySearchSession());
    }


    /**
     *  Gets the session for searching for directories within a given 
     *  directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectorySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectorySearchSession getDirectorySearchSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectorySearchSessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for creating and removing files. 
     *
     *  @return a <code> DirectoryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryAdminSession getDirectoryAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryAdminSession());
    }


    /**
     *  Gets the session for searching for creating and removing files in the 
     *  given directory. 
     *
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectoryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> is 
     *          null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryAdminSession getDirectoryAdminSessionForDirectory(org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryAdminSessionForDirectory(directoryId));
    }


    /**
     *  Gets the session for receiving messages about changes to directories. 
     *
     *  @param  directoryReceiver the notification callback 
     *  @return a <code> DirectoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> directoryReceiver 
     *          </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryNotificationSession getDirectoryNotificationSession(org.osid.filing.DirectoryReceiver directoryReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryNotificationSession(directoryReceiver));
    }


    /**
     *  Gets the session for receiving messages about changes to directories 
     *  in the given directory. 
     *
     *  @param  directoryReceiver the notification callback 
     *  @param  directoryId the <code> Id </code> of the directory 
     *  @return a <code> DirectoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryReceiver 
     *          </code> or <code> directoryId </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryNotificationSession getDirectoryNotificationSessionForDirectory(org.osid.filing.DirectoryReceiver directoryReceiver, 
                                                                                                    org.osid.id.Id directoryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryNotificationSessionForDirectory(directoryReceiver, directoryId));
    }


    /**
     *  Gets the <code> FilingAllocationManager. </code> 
     *
     *  @return a <code> FilingAllocationManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFilingAllocation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.FilingAllocationManager getFilingAllocationManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFilingAllocationManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
