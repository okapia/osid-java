//
// AbstractAdapterReceiptLookupSession.java
//
//    A Receipt lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.messaging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Receipt lookup session adapter.
 */

public abstract class AbstractAdapterReceiptLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.messaging.ReceiptLookupSession {

    private final org.osid.messaging.ReceiptLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterReceiptLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterReceiptLookupSession(org.osid.messaging.ReceiptLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Mailbox/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Mailbox Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getMailboxId() {
        return (this.session.getMailboxId());
    }


    /**
     *  Gets the {@code Mailbox} associated with this session.
     *
     *  @return the {@code Mailbox} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Mailbox getMailbox()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getMailbox());
    }


    /**
     *  Tests if this user can perform {@code Receipt} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupReceipts() {
        return (this.session.canLookupReceipts());
    }


    /**
     *  A complete view of the {@code Receipt} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeReceiptView() {
        this.session.useComparativeReceiptView();
        return;
    }


    /**
     *  A complete view of the {@code Receipt} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryReceiptView() {
        this.session.usePlenaryReceiptView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include receipts in mailboxes which are children
     *  of this mailbox in the mailbox hierarchy.
     */

    @OSID @Override
    public void useFederatedMailboxView() {
        this.session.useFederatedMailboxView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this mailbox only.
     */

    @OSID @Override
    public void useIsolatedMailboxView() {
        this.session.useIsolatedMailboxView();
        return;
    }
    
     
    /**
     *  Gets the {@code Receipt} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Receipt} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Receipt} and
     *  retained for compatibility.
     *
     *  @param receiptId {@code Id} of the {@code Receipt}
     *  @return the receipt
     *  @throws org.osid.NotFoundException {@code receiptId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code receiptId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.Receipt getReceipt(org.osid.id.Id receiptId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getReceipt(receiptId));
    }


    /**
     *  Gets a {@code ReceiptList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  receipts specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Receipts} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  receiptIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Receipt} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code receiptIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByIds(org.osid.id.IdList receiptIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getReceiptsByIds(receiptIds));
    }


    /**
     *  Gets a {@code ReceiptList} corresponding to the given
     *  receipt genus {@code Type} which does not include
     *  receipts of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  receipts or an error results. Otherwise, the returned list
     *  may contain only those receipts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  receiptGenusType a receipt genus type 
     *  @return the returned {@code Receipt} list
     *  @throws org.osid.NullArgumentException
     *          {@code receiptGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByGenusType(org.osid.type.Type receiptGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getReceiptsByGenusType(receiptGenusType));
    }


    /**
     *  Gets a {@code ReceiptList} corresponding to the given
     *  receipt genus {@code Type} and include any additional
     *  receipts with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  receipts or an error results. Otherwise, the returned list
     *  may contain only those receipts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  receiptGenusType a receipt genus type 
     *  @return the returned {@code Receipt} list
     *  @throws org.osid.NullArgumentException
     *          {@code receiptGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByParentGenusType(org.osid.type.Type receiptGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getReceiptsByParentGenusType(receiptGenusType));
    }


    /**
     *  Gets a {@code ReceiptList} containing the given
     *  receipt record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  receipts or an error results. Otherwise, the returned list
     *  may contain only those receipts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  receiptRecordType a receipt record type 
     *  @return the returned {@code Receipt} list
     *  @throws org.osid.NullArgumentException
     *          {@code receiptRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsByRecordType(org.osid.type.Type receiptRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getReceiptsByRecordType(receiptRecordType));
    }

    
    /**
     *  Gets a {@code ReceiptList} for the given message. In plenary
     *  mode, the returned list contains all known receipts or an
     *  error results. Otherwise, the returned list may contain only
     *  those receipts that are accessible through this session.
     *
     *  @param  messageId a message {@code Id} 
     *  @return the returned {@code Receipt} list 
     *  @throws org.osid.NullArgumentException {@code messageId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsForMessage(org.osid.id.Id messageId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getReceiptsForMessage(messageId));
    }


    /**
     *  Gets a {@code ReceiptList} for the given recipient. In plenary
     *  mode, the returned list contains all known receipts or an
     *  error results. Otherwise, the returned list may contain only
     *  those receipts that are accessible through this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Receipts} 
     *  @throws org.osid.NullArgumentException {@code resourceId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsForRecipient(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getReceiptsForRecipient(resourceId));
    }


    /**
     *  Gets a list of {@code Receipts} for the given message and
     *  recipient. In plenary mode, the returned list contains all
     *  known receipts or an error results. Otherwise, the returned
     *  list may contain only those receipts that are accessible
     *  through this session.
     *
     *  @param  messageId a message {@code Id} 
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Receipt} 
     *  @throws org.osid.NullArgumentException {@code messageId} or 
     *          {@code recipientId} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @compliance mandatory This method must be implemented. 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceiptsForMessageAndRecipient(org.osid.id.Id messageId, 
                                                                            org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getReceiptsForMessageAndRecipient(messageId, resourceId));
    }


    /**
     *  Gets all {@code Receipts}. 
     *
     *  In plenary mode, the returned list contains all known
     *  receipts or an error results. Otherwise, the returned list
     *  may contain only those receipts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Receipts} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptList getReceipts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getReceipts());
    }
}
