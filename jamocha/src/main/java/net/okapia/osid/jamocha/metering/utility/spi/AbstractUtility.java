//
// AbstractUtility.java
//
//     Defines an Utility.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metering.utility.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Utility</code>.
 */

public abstract class AbstractUtility
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalog
    implements org.osid.metering.Utility {

    private final java.util.Collection<org.osid.metering.records.UtilityRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Tests if this utility supports the given record
     *  <code>Type</code>.
     *
     *  @param  utilityRecordType an utility record type 
     *  @return <code>true</code> if the utilityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type utilityRecordType) {
        for (org.osid.metering.records.UtilityRecord record : this.records) {
            if (record.implementsRecordType(utilityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Utility</code> record <code>Type</code>.
     *
     *  @param  utilityRecordType the utility record type 
     *  @return the utility record 
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(utilityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.metering.records.UtilityRecord getUtilityRecord(org.osid.type.Type utilityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.metering.records.UtilityRecord record : this.records) {
            if (record.implementsRecordType(utilityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(utilityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this utility. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param utilityRecord the utility record
     *  @param utilityRecordType utility record type
     *  @throws org.osid.NullArgumentException
     *          <code>utilityRecord</code> or
     *          <code>utilityRecordTypeutility</code> is
     *          <code>null</code>
     */
            
    protected void addUtilityRecord(org.osid.metering.records.UtilityRecord utilityRecord, 
                                    org.osid.type.Type utilityRecordType) {
        
        nullarg(utilityRecord, "utility record");
        addRecordType(utilityRecordType);
        this.records.add(utilityRecord);
        
        return;
    }
}
