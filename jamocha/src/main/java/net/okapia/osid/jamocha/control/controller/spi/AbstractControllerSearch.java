//
// AbstractControllerSearch.java
//
//     A template for making a Controller Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.control.controller.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing controller searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractControllerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.control.ControllerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.control.records.ControllerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.control.ControllerSearchOrder controllerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of controllers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  controllerIds list of controllers
     *  @throws org.osid.NullArgumentException
     *          <code>controllerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongControllers(org.osid.id.IdList controllerIds) {
        while (controllerIds.hasNext()) {
            try {
                this.ids.add(controllerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongControllers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of controller Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getControllerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  controllerSearchOrder controller search order 
     *  @throws org.osid.NullArgumentException
     *          <code>controllerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>controllerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderControllerResults(org.osid.control.ControllerSearchOrder controllerSearchOrder) {
	this.controllerSearchOrder = controllerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.control.ControllerSearchOrder getControllerSearchOrder() {
	return (this.controllerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given controller search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a controller implementing the requested record.
     *
     *  @param controllerSearchRecordType a controller search record
     *         type
     *  @return the controller search record
     *  @throws org.osid.NullArgumentException
     *          <code>controllerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(controllerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.ControllerSearchRecord getControllerSearchRecord(org.osid.type.Type controllerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.control.records.ControllerSearchRecord record : this.records) {
            if (record.implementsRecordType(controllerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(controllerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this controller search. 
     *
     *  @param controllerSearchRecord controller search record
     *  @param controllerSearchRecordType controller search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addControllerSearchRecord(org.osid.control.records.ControllerSearchRecord controllerSearchRecord, 
                                           org.osid.type.Type controllerSearchRecordType) {

        addRecordType(controllerSearchRecordType);
        this.records.add(controllerSearchRecord);        
        return;
    }
}
