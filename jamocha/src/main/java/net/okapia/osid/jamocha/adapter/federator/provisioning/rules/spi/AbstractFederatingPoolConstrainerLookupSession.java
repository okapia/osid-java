//
// AbstractFederatingPoolConstrainerLookupSession.java
//
//     An abstract federating adapter for a PoolConstrainerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  PoolConstrainerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingPoolConstrainerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.provisioning.rules.PoolConstrainerLookupSession>
    implements org.osid.provisioning.rules.PoolConstrainerLookupSession {

    private boolean parallel = false;
    private org.osid.provisioning.Distributor distributor = new net.okapia.osid.jamocha.nil.provisioning.distributor.UnknownDistributor();


    /**
     *  Constructs a new
     *  <code>AbstractFederatingPoolConstrainerLookupSession</code>.
     */

    protected AbstractFederatingPoolConstrainerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.provisioning.rules.PoolConstrainerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Distributor/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.distributor.getId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.distributor);
    }


    /**
     *  Sets the <code>Distributor</code>.
     *
     *  @param  distributor the distributor for this session
     *  @throws org.osid.NullArgumentException <code>distributor</code>
     *          is <code>null</code>
     */

    protected void setDistributor(org.osid.provisioning.Distributor distributor) {
        nullarg(distributor, "distributor");
        this.distributor = distributor;
        return;
    }


    /**
     *  Tests if this user can perform <code>PoolConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPoolConstrainers() {
        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            if (session.canLookupPoolConstrainers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>PoolConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePoolConstrainerView() {
        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            session.useComparativePoolConstrainerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>PoolConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPoolConstrainerView() {
        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            session.usePlenaryPoolConstrainerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include pool constrainers in distributors which are
     *  children of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            session.useFederatedDistributorView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            session.useIsolatedDistributorView();
        }

        return;
    }


    /**
     *  Only active pool constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActivePoolConstrainerView() {
        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            session.useActivePoolConstrainerView();
        }

        return;
    }


    /**
     *  Active and inactive pool constrainers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusPoolConstrainerView() {
        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            session.useAnyStatusPoolConstrainerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>PoolConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>PoolConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>PoolConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @param  poolConstrainerId <code>Id</code> of the
     *          <code>PoolConstrainer</code>
     *  @return the pool constrainer
     *  @throws org.osid.NotFoundException <code>poolConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>poolConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainer getPoolConstrainer(org.osid.id.Id poolConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            try {
                return (session.getPoolConstrainer(poolConstrainerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(poolConstrainerId + " not found");
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  poolConstrainers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>PoolConstrainers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @param  poolConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByIds(org.osid.id.IdList poolConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.provisioning.rules.poolconstrainer.MutablePoolConstrainerList ret = new net.okapia.osid.jamocha.provisioning.rules.poolconstrainer.MutablePoolConstrainerList();

        try (org.osid.id.IdList ids = poolConstrainerIds) {
            while (ids.hasNext()) {
                ret.addPoolConstrainer(getPoolConstrainer(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the
     *  given pool constrainer genus <code>Type</code> which does not
     *  include pool constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @param  poolConstrainerGenusType a poolConstrainer genus type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.FederatingPoolConstrainerList ret = getPoolConstrainerList();

        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            ret.addPoolConstrainerList(session.getPoolConstrainersByGenusType(poolConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> corresponding to the
     *  given pool constrainer genus <code>Type</code> and include any
     *  additional pool constrainers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @param  poolConstrainerGenusType a poolConstrainer genus type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByParentGenusType(org.osid.type.Type poolConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.FederatingPoolConstrainerList ret = getPoolConstrainerList();

        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            ret.addPoolConstrainerList(session.getPoolConstrainersByParentGenusType(poolConstrainerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>PoolConstrainerList</code> containing the given
     *  pool constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known pool
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, pool constrainers are returned that are
     *  currently active. In any status mode, active and inactive pool
     *  constrainers are returned.
     *
     *  @param  poolConstrainerRecordType a poolConstrainer record type 
     *  @return the returned <code>PoolConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>poolConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainersByRecordType(org.osid.type.Type poolConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.FederatingPoolConstrainerList ret = getPoolConstrainerList();

        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            ret.addPoolConstrainerList(session.getPoolConstrainersByRecordType(poolConstrainerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>PoolConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  pool constrainers or an error results. Otherwise, the returned list
     *  may contain only those pool constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, pool constrainers are returned that are currently
     *  active. In any status mode, active and inactive pool constrainers
     *  are returned.
     *
     *  @return a list of <code>PoolConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolConstrainerList getPoolConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.FederatingPoolConstrainerList ret = getPoolConstrainerList();

        for (org.osid.provisioning.rules.PoolConstrainerLookupSession session : getSessions()) {
            ret.addPoolConstrainerList(session.getPoolConstrainers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.FederatingPoolConstrainerList getPoolConstrainerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.ParallelPoolConstrainerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.provisioning.rules.poolconstrainer.CompositePoolConstrainerList());
        }
    }
}
