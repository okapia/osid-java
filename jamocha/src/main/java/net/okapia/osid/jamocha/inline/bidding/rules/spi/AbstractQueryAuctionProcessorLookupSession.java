//
// AbstractQueryAuctionProcessorLookupSession.java
//
//    An inline adapter that maps an AuctionProcessorLookupSession to
//    an AuctionProcessorQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AuctionProcessorLookupSession to
 *  an AuctionProcessorQuerySession.
 */

public abstract class AbstractQueryAuctionProcessorLookupSession
    extends net.okapia.osid.jamocha.bidding.rules.spi.AbstractAuctionProcessorLookupSession
    implements org.osid.bidding.rules.AuctionProcessorLookupSession {

    private boolean activeonly    = false;
    private final org.osid.bidding.rules.AuctionProcessorQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAuctionProcessorLookupSession.
     *
     *  @param querySession the underlying auction processor query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAuctionProcessorLookupSession(org.osid.bidding.rules.AuctionProcessorQuerySession querySession) {
        nullarg(querySession, "auction processor query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform <code>AuctionProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionProcessors() {
        return (this.session.canSearchAuctionProcessors());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction processors in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only active auction processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionProcessorView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive auction processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionProcessorView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>AuctionProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>AuctionProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorId <code>Id</code> of the
     *          <code>AuctionProcessor</code>
     *  @return the auction processor
     *  @throws org.osid.NotFoundException <code>auctionProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessor getAuctionProcessor(org.osid.id.Id auctionProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionProcessorQuery query = getQuery();
        query.matchId(auctionProcessorId, true);
        org.osid.bidding.rules.AuctionProcessorList auctionProcessors = this.session.getAuctionProcessorsByQuery(query);
        if (auctionProcessors.hasNext()) {
            return (auctionProcessors.getNextAuctionProcessor());
        } 
        
        throw new org.osid.NotFoundException(auctionProcessorId + " not found");
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionProcessors specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>AuctionProcessors</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByIds(org.osid.id.IdList auctionProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionProcessorQuery query = getQuery();

        try (org.osid.id.IdList ids = auctionProcessorIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAuctionProcessorsByQuery(query));
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the given
     *  auction processor genus <code>Type</code> which does not include
     *  auction processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorGenusType an auctionProcessor genus type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByGenusType(org.osid.type.Type auctionProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionProcessorQuery query = getQuery();
        query.matchGenusType(auctionProcessorGenusType, true);
        return (this.session.getAuctionProcessorsByQuery(query));
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> corresponding to the given
     *  auction processor genus <code>Type</code> and include any additional
     *  auction processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorGenusType an auctionProcessor genus type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByParentGenusType(org.osid.type.Type auctionProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionProcessorQuery query = getQuery();
        query.matchParentGenusType(auctionProcessorGenusType, true);
        return (this.session.getAuctionProcessorsByQuery(query));
    }


    /**
     *  Gets an <code>AuctionProcessorList</code> containing the given
     *  auction processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @param  auctionProcessorRecordType an auctionProcessor record type 
     *  @return the returned <code>AuctionProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessorsByRecordType(org.osid.type.Type auctionProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionProcessorQuery query = getQuery();
        query.matchRecordType(auctionProcessorRecordType, true);
        return (this.session.getAuctionProcessorsByQuery(query));
    }

    
    /**
     *  Gets all <code>AuctionProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auction processors or an error results. Otherwise, the returned list
     *  may contain only those auction processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auction processors are returned that are currently
     *  active. In any status mode, active and inactive auction processors
     *  are returned.
     *
     *  @return a list of <code>AuctionProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorList getAuctionProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.rules.AuctionProcessorQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAuctionProcessorsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.bidding.rules.AuctionProcessorQuery getQuery() {
        org.osid.bidding.rules.AuctionProcessorQuery query = this.session.getAuctionProcessorQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
