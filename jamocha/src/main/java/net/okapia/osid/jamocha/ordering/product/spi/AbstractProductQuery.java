//
// AbstractProductQuery.java
//
//     A template for making a Product Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.product.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for products.
 */

public abstract class AbstractProductQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQuery
    implements org.osid.ordering.ProductQuery {

    private final java.util.Collection<org.osid.ordering.records.ProductQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches product codes. 
     *
     *  @param  code a product code 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> code </code> not of 
     *          <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> code </code> or <code> 
     *          stringMatchType </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchCode(String code, org.osid.type.Type stringMatchType, 
                          boolean match) {
        return;
    }


    /**
     *  Matches products with any code. 
     *
     *  @param  match <code> true </code> to match products with any code, 
     *          <code> false </code> to match products with no code 
     */

    @OSID @Override
    public void matchAnyCode(boolean match) {
        return;
    }


    /**
     *  Clears the code. 
     */

    @OSID @Override
    public void clearCodeTerms() {
        return;
    }


    /**
     *  Sets the price schedule <code> Id </code> for this query. 
     *
     *  @param  priceScheduleId a price schedule <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchPriceScheduleId(org.osid.id.Id priceScheduleId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the price schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearPriceScheduleIdTerms() {
        return;
    }


    /**
     *  Tests if a price schedule query is available. 
     *
     *  @return <code> true </code> if a price schedule query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPriceScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a price schedule. 
     *
     *  @return the price schedule query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPriceScheduleQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleQuery getPriceScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsPriceScheduleQuery() is false");
    }


    /**
     *  Matches products with any price schedule. 
     *
     *  @param  match <code> true </code> to match products with any price 
     *          schedule, <code> false </code> to match products with no price 
     *          schedule 
     */

    @OSID @Override
    public void matchAnyPriceSchedule(boolean match) {
        return;
    }


    /**
     *  Clears the price schedule terms. 
     */

    @OSID @Override
    public void clearPriceScheduleTerms() {
        return;
    }


    /**
     *  Matches product availability between the given range inclusive. 
     *
     *  @param  low a product code 
     *  @param  high a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchAvailability(long low, long high, boolean match) {
        return;
    }


    /**
     *  Matches products with any availability set. 
     *
     *  @param  match <code> true </code> to match products with 
     *          anyavailability value, <code> false </code> to match products 
     *          with no availability value 
     */

    @OSID @Override
    public void matchAnyAvailability(boolean match) {
        return;
    }


    /**
     *  Clears the availability terms. 
     */

    @OSID @Override
    public void clearAvailabilityTerms() {
        return;
    }


    /**
     *  Sets the item <code> Id </code> for this query. 
     *
     *  @param  itemId an item <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> priceScheduleId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchItemId(org.osid.id.Id itemId, boolean match) {
        return;
    }


    /**
     *  Clears the item <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearItemIdTerms() {
        return;
    }


    /**
     *  Tests if an item query is available. 
     *
     *  @return <code> true </code> if an item query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (false);
    }


    /**
     *  Gets the query for an item. 
     *
     *  @return the item query 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.ItemQuery getItemQuery() {
        throw new org.osid.UnimplementedException("supportsItemQuery() is false");
    }


    /**
     *  Matches prices used with any item. 
     *
     *  @param  match <code> true </code> to match prices with any order, 
     *          <code> false </code> to match prices with no orders 
     */

    @OSID @Override
    public void matchAnyItem(boolean match) {
        return;
    }


    /**
     *  Clears the item terms. 
     */

    @OSID @Override
    public void clearItemTerms() {
        return;
    }


    /**
     *  Sets the product <code> Id </code> for this query to match orders 
     *  assigned to stores. 
     *
     *  @param  storeId a store <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> storeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStoreId(org.osid.id.Id storeId, boolean match) {
        return;
    }


    /**
     *  Clears the store <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearStoreIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> StoreQuery </code> is available. 
     *
     *  @return <code> true </code> if a store query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStoreQuery() {
        return (false);
    }


    /**
     *  Gets the query for a store query. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the store query 
     *  @throws org.osid.UnimplementedException <code> supportsStoreQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.ordering.StoreQuery getStoreQuery() {
        throw new org.osid.UnimplementedException("supportsStoreQuery() is false");
    }


    /**
     *  Clears the store terms. 
     */

    @OSID @Override
    public void clearStoreTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given product query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a product implementing the requested record.
     *
     *  @param productRecordType a product record type
     *  @return the product query record
     *  @throws org.osid.NullArgumentException
     *          <code>productRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(productRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.ProductQueryRecord getProductQueryRecord(org.osid.type.Type productRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.ordering.records.ProductQueryRecord record : this.records) {
            if (record.implementsRecordType(productRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(productRecordType + " is not supported");
    }


    /**
     *  Adds a record to this product query. 
     *
     *  @param productQueryRecord product query record
     *  @param productRecordType product record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addProductQueryRecord(org.osid.ordering.records.ProductQueryRecord productQueryRecord, 
                                          org.osid.type.Type productRecordType) {

        addRecordType(productRecordType);
        nullarg(productQueryRecord, "product query record");
        this.records.add(productQueryRecord);        
        return;
    }
}
