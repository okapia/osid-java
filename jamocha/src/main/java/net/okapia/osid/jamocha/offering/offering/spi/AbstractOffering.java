//
// AbstractOffering.java
//
//     Defines an Offering.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.offering.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>Offering</code>.
 */

public abstract class AbstractOffering
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.offering.Offering {

    private org.osid.offering.CanonicalUnit canonicalUnit;
    private org.osid.calendaring.TimePeriod timePeriod;
    private org.osid.locale.DisplayText title;
    private String code;
    private final java.util.Collection<org.osid.grading.GradeSystem> resultOptions = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.Resource> sponsors = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.Schedule> schedules = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.offering.records.OfferingRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the canonical unit. 
     *
     *  @return the canonical unit <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCanonicalUnitId() {
        return (this.canonicalUnit.getId());
    }


    /**
     *  Gets the canonical of this offering. 
     *
     *  @return the canonical unit 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnit getCanonicalUnit()
        throws org.osid.OperationFailedException {

        return (this.canonicalUnit);
    }


    /**
     *  Sets the canonical unit.
     *
     *  @param canonicalUnit a canonical unit
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnit</code> is <code>null</code>
     */

    protected void setCanonicalUnit(org.osid.offering.CanonicalUnit canonicalUnit) {
        this.canonicalUnit = canonicalUnit;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the time period. 
     *
     *  @return the time period <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTimePeriodId() {
        return (this.timePeriod.getId());
    }


    /**
     *  Gets the time period of this offering 
     *
     *  @return the time period 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.TimePeriod getTimePeriod()
        throws org.osid.OperationFailedException {

        return (this.timePeriod);
    }


    /**
     *  Sets the time period.
     *
     *  @param timePeriod a time period
     *  @throws org.osid.NullArgumentException
     *          <code>timePeriod</code> is <code>null</code>
     */

    protected void setTimePeriod(org.osid.calendaring.TimePeriod timePeriod) {
        nullarg(timePeriod, "time period");
        this.timePeriod = timePeriod;
        return;
    }


    /**
     *  Gets the title for this offering. 
     *
     *  @return the title 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getTitle() {
        return (this.title);
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException
     *          <code>title</code> is <code>null</code>
     */

    protected void setTitle(org.osid.locale.DisplayText title) {
        nullarg(title, "title");
        this.title = title;
        return;
    }


    /**
     *  Gets the code for this offering. 
     *
     *  @return the code 
     */

    @OSID @Override
    public String getCode() {
        return (this.code);
    }


    /**
     *  Sets the code.
     *
     *  @param code a code
     *  @throws org.osid.NullArgumentException
     *          <code>code</code> is <code>null</code>
     */

    protected void setCode(String code) {
        nullarg(code, "code");
        this.code = code;
        return;
    }


    /**
     *  Tests if this offering has results when offered. 
     *
     *  @return <code> true </code> if this offering has results, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasResults() {
        return (this.resultOptions.size() > 0);
    }


    /**
     *  Gets the various result option <code> Ids </code> allowed for
     *  results.
     *
     *  @return the returned list of grading option <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasResults()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.IdList getResultOptionIds() {
        try {
            org.osid.grading.GradeSystemList resultOptions = getResultOptions();
            return (new net.okapia.osid.jamocha.adapter.converter.grading.gradesystem.GradeSystemToIdList(resultOptions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the various result options allowed for this offering. 
     *
     *  @return the returned list of grading options 
     *  @throws org.osid.IllegalStateException <code> hasResults() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemList getResultOptions()
        throws org.osid.OperationFailedException {

        if (!hasResults()) {
            throw new org.osid.IllegalStateException("hasResults() is false");
        }

        return (new net.okapia.osid.jamocha.grading.gradesystem.ArrayGradeSystemList(this.resultOptions));
    }


    /**
     *  Adds a result option.
     *
     *  @param resultOption a result option
     *  @throws org.osid.NullArgumentException
     *          <code>resultOption</code> is <code>null</code>
     */

    protected void addResultOption(org.osid.grading.GradeSystem resultOption) {
        nullarg(resultOption, "result option");
        this.resultOptions.add(resultOption);
        return;
    }


    /**
     *  Sets all the result options.
     *
     *  @param resultOptions a collection of result options
     *  @throws org.osid.NullArgumentException
     *          <code>resultOptions</code> is <code>null</code>
     */

    protected void setResultOptions(java.util.Collection<org.osid.grading.GradeSystem> resultOptions) {
        nullarg(resultOptions, "result options");
        this.resultOptions.clear();
        this.resultOptions.addAll(resultOptions);
        return;
    }


    /**
     *  Tests if this offering has sponsors. 
     *
     *  @return <code> true </code> if this offering has sponsors, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.sponsors.size() > 0);
    }


    /**
     *  Gets the sponsor <code> Ids. </code> 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        try {
            org.osid.resource.ResourceList sponsors = getSponsors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(sponsors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the sponsors. 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() ia false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.sponsors));
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException
     *          <code>sponsor</code> is <code>null</code>
     */

    protected void addSponsor(org.osid.resource.Resource sponsor) {
        nullarg(sponsor, "sponsor");
        this.sponsors.add(sponsor);
        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException
     *          <code>sponsors</code> is <code>null</code>
     */

    protected void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        nullarg(sponsors, "sponsors");
        this.sponsors.clear();
        this.sponsors.addAll(sponsors);
        return;
    }


    /**
     *  Gets the schedule <code> Ids </code> associated with this
     *  offering.
     *
     *  @return the schedule <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getScheduleIds() {
        try {
            org.osid.calendaring.ScheduleList schedules = getSchedules();
            return (new net.okapia.osid.jamocha.adapter.converter.calendaring.schedule.ScheduleToIdList(schedules));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the schedules associated with this offering. 
     *
     *  @return the schedules 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.calendaring.schedule.ArrayScheduleList(this.schedules));
    }


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @throws org.osid.NullArgumentException
     *          <code>schedule</code> is <code>null</code>
     */

    protected void addSchedule(org.osid.calendaring.Schedule schedule) {
        nullarg(schedule, "schedule");
        this.schedules.add(schedule);
        return;
    }


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @throws org.osid.NullArgumentException
     *          <code>schedules</code> is <code>null</code>
     */

    protected void setSchedules(java.util.Collection<org.osid.calendaring.Schedule> schedules) {
        nullarg(schedules, "schedules");
        this.schedules.clear();
        this.schedules.addAll(schedules);
        return;
    }

    /**
     *  Tests if this offering supports the given record
     *  <code>Type</code>.
     *
     *  @param  offeringRecordType an offering record type 
     *  @return <code>true</code> if the offeringRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>offeringRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type offeringRecordType) {
        for (org.osid.offering.records.OfferingRecord record : this.records) {
            if (record.implementsRecordType(offeringRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  offeringRecordType the offering record type 
     *  @return the offering record 
     *  @throws org.osid.NullArgumentException
     *          <code>offeringRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offeringRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.records.OfferingRecord getOfferingRecord(org.osid.type.Type offeringRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.records.OfferingRecord record : this.records) {
            if (record.implementsRecordType(offeringRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offeringRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offering. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param offeringRecord the offering record
     *  @param offeringRecordType offering record type
     *  @throws org.osid.NullArgumentException
     *          <code>offeringRecord</code> or
     *          <code>offeringRecordTypeoffering</code> is
     *          <code>null</code>
     */
            
    protected void addOfferingRecord(org.osid.offering.records.OfferingRecord offeringRecord, 
                                     org.osid.type.Type offeringRecordType) {

        nullarg(offeringRecord, "offering record");
        addRecordType(offeringRecordType);
        this.records.add(offeringRecord);
        
        return;
    }
}
