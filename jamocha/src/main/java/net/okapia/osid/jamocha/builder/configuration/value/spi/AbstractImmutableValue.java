//
// AbstractImmutableValue.java
//
//     Wraps a mutable Value to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.configuration.value.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Value</code> to hide modifiers. This
 *  wrapper provides an immutized Value from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying value whose state changes are visible.
 */

public abstract class AbstractImmutableValue
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOperableOsidObject
    implements org.osid.configuration.Value {

    private final org.osid.configuration.Value value;


    /**
     *  Constructs a new <code>AbstractImmutableValue</code>.
     *
     *  @param value the value to immutablize
     *  @throws org.osid.NullArgumentException <code>value</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableValue(org.osid.configuration.Value value) {
        super(value);
        this.value = value;
        return;
    }


    /**
     *  Gets the parameter <code> Id </code> of this value. 
     *
     *  @return the parameter <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getParameterId() {
        return (this.value.getParameterId());
    }


    /**
     *  Gets the parameter of this value. 
     *
     *  @return the parameter 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.configuration.Parameter getParameter()
        throws org.osid.OperationFailedException {

        return (this.value.getParameter());
    }


    /**
     *  Gets the priority of this value. If the values for this parameter are 
     *  not shuffled, the priority indicates a static ordering of the values 
     *  from lowest to highest. If the values for this parameter are shuffled, 
     *  the priority effects the weight with greater numbers having greater 
     *  weights. 
     *
     *  @return the index of this value 
     */

    @OSID @Override
    public long getPriority() {
        return (this.value.getPriority());
    }


    /**
     *  Gets the value if it is a boolean. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          BOOLEAN </code> 
     */

    @OSID @Override
    public boolean getBooleanValue() {
        return (this.value.getBooleanValue());
    }


    /**
     *  Gets the value if it is a byte. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          BYTE </code> 
     */

    @OSID @Override
    public byte[] getBytesValue() {
        return (this.value.getBytesValue());
    }


    /**
     *  Gets the value if it is a cardinal. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          CARDINAL </code> 
     */

    @OSID @Override
    public long getCardinalValue() {
        return (this.value.getCardinalValue());
    }


    /**
     *  Gets the value if it is a coordinate. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          COORDINATE </code> 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getCoordinateValue() {
        return (this.value.getCoordinateValue());
    }


    /**
     *  Gets the value if it is a currency. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          CURRENCY </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getCurrencyValue() {
        return (this.value.getCurrencyValue());
    }


    /**
     *  Gets the value if it is a <code> DateTime. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          DATETIME </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDateTimeValue() {
        return (this.value.getDateTimeValue());
    }


    /**
     *  Gets the value if it is a decimal. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          DECIMAL </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getDecimalValue() {
        return (this.value.getDecimalValue());
    }


    /**
     *  Gets the value if it is a <code> Distance. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          DISTANCE </code> 
     */

    @OSID @Override
    public org.osid.mapping.Distance getDistanceValue() {
        return (this.value.getDistanceValue());
    }


    /**
     *  Gets the value if it is a <code> Duration. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          DURATION </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Duration getDurationValue() {
        return (this.value.getDurationValue());
    }


    /**
     *  Gets the value if it is a <code> Heading. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          HEADING </code> 
     */

    @OSID @Override
    public org.osid.mapping.Heading getHeadingValue() {
        return (this.value.getHeadingValue());
    }


    /**
     *  Gets the value if it is an <code> Id. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          ID </code> 
     */

    @OSID @Override
    public org.osid.id.Id getIdValue() {
        return (this.value.getIdValue());
    }


    /**
     *  Gets the value if it is an integer. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          INTEGER </code> 
     */

    @OSID @Override
    public long getIntegerValue() {
        return (this.value.getIntegerValue());
    }


    /**
     *  Gets the value if it is an object. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          OBJECT </code> 
     */

    @OSID @Override
    public java.lang.Object getObjectValue() {
        return (this.value.getObjectValue());
    }


    /**
     *  Gets the value if it is a spatial unit. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          SPATIALUNIT </code> 
     */

    @OSID @Override
    public org.osid.mapping.SpatialUnit getSpatialUnitValue() {
        return (this.value.getSpatialUnitValue());
    }


    /**
     *  Gets the value if it is a speed. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          SPEED </code> 
     */

    @OSID @Override
    public org.osid.mapping.Speed getSpeedValue() {
        return (this.value.getSpeedValue());
    }


    /**
     *  Gets the value if it is a string. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          STRING </code> 
     */

    @OSID @Override
    public String getStringValue() {
        return (this.value.getStringValue());
    }


    /**
     *  Gets the value if it is a time. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          TIME </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Time getTimeValue() {
        return (this.value.getTimeValue());
    }


    /**
     *  Gets the value if it is a <code> Type. </code> 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          TYPE </code> 
     */

    @OSID @Override
    public org.osid.type.Type getTypeValue() {
        return (this.value.getTypeValue());
    }


    /**
     *  Gets the value if it is a version. 
     *
     *  @return the value 
     *  @throws org.osid.IllegalStateException <code> Parameter.getSyntax() != 
     *          VERSION </code> 
     */

    @OSID @Override
    public org.osid.installation.Version getVersionValue() {
        return (this.value.getVersionValue());
    }


    /**
     *  Gets the value record corresponding to the given <code> Value
     *  </code> record <code> Type. </code> This method is used to
     *  retrieve an object implementing the requested record. The
     *  <code> valueRecordType </code> may be the <code> Type </code>
     *  returned in <code> getRecordTypes() </code> or any of its
     *  parents in a <code> Type </code> hierarchy where <code>
     *  hasRecordType(valueRecordType) </code> is <code> true </code>.
     *
     *  @param  valueRecordType the type of value record to retrieve 
     *  @return the value record 
     *  @throws org.osid.NullArgumentException <code> valueRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(valueRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.configuration.records.ValueRecord getValueRecord(org.osid.type.Type valueRecordType)
        throws org.osid.OperationFailedException {

        return (this.value.getValueRecord(valueRecordType));
    }
}

