//
// AbstractRecurringEventEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.recurringeventenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractRecurringEventEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.calendaring.rules.RecurringEventEnablerSearchResults {

    private org.osid.calendaring.rules.RecurringEventEnablerList recurringEventEnablers;
    private final org.osid.calendaring.rules.RecurringEventEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.calendaring.rules.records.RecurringEventEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractRecurringEventEnablerSearchResults.
     *
     *  @param recurringEventEnablers the result set
     *  @param recurringEventEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>recurringEventEnablers</code>
     *          or <code>recurringEventEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractRecurringEventEnablerSearchResults(org.osid.calendaring.rules.RecurringEventEnablerList recurringEventEnablers,
                                            org.osid.calendaring.rules.RecurringEventEnablerQueryInspector recurringEventEnablerQueryInspector) {
        nullarg(recurringEventEnablers, "recurring event enablers");
        nullarg(recurringEventEnablerQueryInspector, "recurring event enabler query inspectpr");

        this.recurringEventEnablers = recurringEventEnablers;
        this.inspector = recurringEventEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the recurring event enabler list resulting from a search.
     *
     *  @return a recurring event enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.calendaring.rules.RecurringEventEnablerList getRecurringEventEnablers() {
        if (this.recurringEventEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.calendaring.rules.RecurringEventEnablerList recurringEventEnablers = this.recurringEventEnablers;
        this.recurringEventEnablers = null;
	return (recurringEventEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.calendaring.rules.RecurringEventEnablerQueryInspector getRecurringEventEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  recurring event enabler search record <code> Type. </code> This method must
     *  be used to retrieve a recurringEventEnabler implementing the requested
     *  record.
     *
     *  @param recurringEventEnablerSearchRecordType a recurringEventEnabler search 
     *         record type 
     *  @return the recurring event enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(recurringEventEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.RecurringEventEnablerSearchResultsRecord getRecurringEventEnablerSearchResultsRecord(org.osid.type.Type recurringEventEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.calendaring.rules.records.RecurringEventEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(recurringEventEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(recurringEventEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record recurring event enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addRecurringEventEnablerRecord(org.osid.calendaring.rules.records.RecurringEventEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "recurring event enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
