//
// AbstractCampusLookupSession.java
//
//    A starter implementation framework for providing a Campus
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Campus lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getCampuses(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractCampusLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.room.CampusLookupSession {

    private boolean pedantic = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();
    


    /**
     *  Tests if this user can perform <code>Campus</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupCampuses() {
        return (true);
    }


    /**
     *  A complete view of the <code>Campus</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCampusView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Campus</code> returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCampusView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Campus</code> specified by its <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Campus</code> may have a different <code>Id</code> than
     *  requested, such as the case where a duplicate <code>Id</code>
     *  was assigned to a <code>Campus</code> and retained for
     *  compatibility.
     *
     *  @param  campusId <code>Id</code> of the
     *          <code>Campus</code>
     *  @return the campus
     *  @throws org.osid.NotFoundException <code>campusId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>campusId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus(org.osid.id.Id campusId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.room.CampusList campuses = getCampuses()) {
            while (campuses.hasNext()) {
                org.osid.room.Campus campus = campuses.getNextCampus();
                if (campus.getId().equals(campusId)) {
                    return (campus);
                }
            }
        } 

        throw new org.osid.NotFoundException(campusId + " not found");
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  campuses specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Campuses</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getCampuses()</code>.
     *
     *  @param  campusIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>campusIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByIds(org.osid.id.IdList campusIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.Campus> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = campusIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getCampus(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("campus " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.campus.LinkedCampusList(ret));
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  campus genus <code>Type</code> which does not include campuses
     *  of types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known campuses
     *  or an error results. Otherwise, the returned list may contain
     *  only those campuses that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getCampuses()</code>.
     *
     *  @param  campusGenusType a campus genus type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByGenusType(org.osid.type.Type campusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.campus.CampusGenusFilterList(getCampuses(), campusGenusType));
    }


    /**
     *  Gets a <code>CampusList</code> corresponding to the given
     *  campus genus <code>Type</code> and include any additional
     *  campuses with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known campuses
     *  or an error results. Otherwise, the returned list may contain
     *  only those campuses that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCampuses()</code>.
     *
     *  @param  campusGenusType a campus genus type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByParentGenusType(org.osid.type.Type campusGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getCampusesByGenusType(campusGenusType));
    }


    /**
     *  Gets a <code>CampusList</code> containing the given campus
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known campuses
     *  or an error results. Otherwise, the returned list may contain
     *  only those campuses that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getCampuses()</code>.
     *
     *  @param  campusRecordType a campus record type 
     *  @return the returned <code>Campus</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>campusRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByRecordType(org.osid.type.Type campusRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.campus.CampusRecordFilterList(getCampuses(), campusRecordType));
    }


    /**
     *  Gets a <code>CampusList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known campuses
     *  or an error results. Otherwise, the returned list may contain
     *  only those campuses that are accessible through this session.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Campus</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.CampusList getCampusesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.room.campus.CampusProviderFilterList(getCampuses(), resourceId));
    }


    /**
     *  Gets all <code>Campuses</code>.
     *
     *  In plenary mode, the returned list contains all known campuses
     *  or an error results. Otherwise, the returned list may contain
     *  only those campuses that are accessible through this
     *  session. In both cases, the order of the set is not specified.
     *
     *  @return a list of <code>Campuses</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.room.CampusList getCampuses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the campus list for active and effective views. Should
     *  be called by <code>getObjects()</code> if no filtering is
     *  already performed.
     *
     *  @param list the list of campuses
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.room.CampusList filterCampusesOnViews(org.osid.room.CampusList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
