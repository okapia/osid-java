//
// RealmNode.java
//
//     Defines a Realm node within an in code hierarchy.
//
//
// Tom Coppeto
// Okapia
// 8 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  A class for managing a hierarchy of realm nodes in core.
 */

public final class RealmNode
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractRealmNode
    implements org.osid.personnel.RealmNode {


    /**
     *  Constructs a new <code>RealmNode</code> from a single
     *  realm.
     *
     *  @param realm the realm
     *  @throws org.osid.NullArgumentException <code>realm</code> is 
     *          <code>null</code>.
     */

    public RealmNode(org.osid.personnel.Realm realm) {
        super(realm);
        return;
    }


    /**
     *  Constructs a new <code>RealmNode</code>.
     *
     *  @param realm the realm
     *  @param root <code>true</code> if this node is a root, 
     *         <code>false</code> otherwise
     *  @param leaf <code>true</code> if this node is a leaf, 
     *         <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException <code>realm</code>
     *          is <code>null</code>.
     */

    public RealmNode(org.osid.personnel.Realm realm, boolean root, boolean leaf) {
        super(realm, root, leaf);
        return;
    }


    /**
     *  Adds a parent to this realm.
     *
     *  @param node the parent to add
     *  @throws org.osid.IllegalStateException this is a root
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addParent(org.osid.personnel.RealmNode node) {
        super.addParent(node);
        return;
    }


    /**
     *  Adds a parent to this realm.
     *
     *  @param realm the realm to add as a parent
     *  @throws org.osid.NullArgumentException <code>realm</code>
     *          is <code>null</code>
     */

    public void addParent(org.osid.personnel.Realm realm) {
        addParent(new RealmNode(realm));
        return;
    }


    /**
     *  Adds a child to this realm.
     *
     *  @param node the child node to add
     *  @throws org.osid.NullArgumentException <code>node</code>
     *          is <code>null</code>
     */

    @Override
    public void addChild(org.osid.personnel.RealmNode node) {
        super.addChild(node);
        return;
    }


    /**
     *  Adds a child to this realm.
     *
     *  @param realm the realm to add as a child
     *  @throws org.osid.NullArgumentException <code>realm</code>
     *          is <code>null</code>
     */

    public void addChild(org.osid.personnel.Realm realm) {
        addChild(new RealmNode(realm));
        return;
    }
}
