//
// AbstractShipment.java
//
//     Defines a Shipment.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inventory.shipment.shipment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Shipment</code>.
 */

public abstract class AbstractShipment
    extends net.okapia.osid.jamocha.spi.AbstractOsidObject
    implements org.osid.inventory.shipment.Shipment {

    private org.osid.resource.Resource source;
    private org.osid.ordering.Order order;
    private org.osid.calendaring.DateTime date;
    private final java.util.Collection<org.osid.inventory.shipment.Entry> entries = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.inventory.shipment.records.ShipmentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the resource <code> Id </code> representing the shipment. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceId() {
        return (this.source.getId());
    }


    /**
     *  Gets the resource. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSource()
        throws org.osid.OperationFailedException {

        return (this.source);
    }


    /**
     *  Sets the source.
     *
     *  @param source a source
     *  @throws org.osid.NullArgumentException
     *          <code>source</code> is <code>null</code>
     */

    protected void setSource(org.osid.resource.Resource source) {
        nullarg(source, "source");
        this.source = source;
        return;
    }


    /**
     *  Tests if this shipment has a related order. 
     *
     *  @return <code> true </code> if this shipment relates to an odrer, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasOrder() {
        return (this.order != null);
    }


    /**
     *  Gets the order <code> Id </code> associated with this shipment. 
     *
     *  @return the order <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasOrder() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getOrderId() {
        if (!hasOrder()) {
            throw new org.osid.IllegalStateException("hasOrder() is false");
        }
        
        return (this.order.getId());
    }


    /**
     *  Gets the order associated with this shipment. 
     *
     *  @return the order 
     *  @throws org.osid.IllegalStateException <code> hasOrder() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.ordering.Order getOrder()
        throws org.osid.OperationFailedException {

        if (!hasOrder()) {
            throw new org.osid.IllegalStateException("hasOrder() is false");
        }

        return (this.order);
    }


    /**
     *  Sets the order.
     *
     *  @param order an order
     *  @throws org.osid.NullArgumentException
     *          <code>order</code> is <code>null</code>
     */

    protected void setOrder(org.osid.ordering.Order order) {
        nullarg(order, "order");
        this.order = order;
        return;
    }


    /**
     *  Gets the date this shipment was received. 
     *
     *  @return the received date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDate() {
        return (this.date);
    }


    /**
     *  Sets the date.
     *
     *  @param date a date
     *  @throws org.osid.NullArgumentException
     *          <code>date</code> is <code>null</code>
     */

    protected void setDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.date = date;
        return;
    }


    /**
     *  Gets the entry <code> Ids </code> of this shipment. 
     *
     *  @return the entry <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getEntryIds() {
        try {
            org.osid.inventory.shipment.EntryList entries = getEntries();
            return (new net.okapia.osid.jamocha.adapter.converter.inventory.shipment.entry.EntryToIdList(entries));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the entries of this shipment. 
     *
     *  @return the entries 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.inventory.shipment.EntryList getEntries()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.inventory.shipment.entry.ArrayEntryList(this.entries));
    }


    /**
     *  Adds an entry.
     *
     *  @param entry an entry
     *  @throws org.osid.NullArgumentException
     *          <code>entry</code> is <code>null</code>
     */

    protected void addEntry(org.osid.inventory.shipment.Entry entry) {
        nullarg(entry, "entry");
        this.entries.add(entry);
        return;
    }


    /**
     *  Sets all the entries.
     *
     *  @param entries a collection of entries
     *  @throws org.osid.NullArgumentException
     *          <code>entries</code> is <code>null</code>
     */

    protected void setEntries(java.util.Collection<org.osid.inventory.shipment.Entry> entries) {
        nullarg(entries, "entries");
        this.entries.clear();
        this.entries.addAll(entries);
        return;
    }


    /**
     *  Tests if this shipment supports the given record
     *  <code>Type</code>.
     *
     *  @param  shipmentRecordType a shipment record type 
     *  @return <code>true</code> if the shipmentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type shipmentRecordType) {
        for (org.osid.inventory.shipment.records.ShipmentRecord record : this.records) {
            if (record.implementsRecordType(shipmentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Shipment</code> record <code>Type</code>.
     *
     *  @param  shipmentRecordType the shipment record type 
     *  @return the shipment record 
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(shipmentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.inventory.shipment.records.ShipmentRecord getShipmentRecord(org.osid.type.Type shipmentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.inventory.shipment.records.ShipmentRecord record : this.records) {
            if (record.implementsRecordType(shipmentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(shipmentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this shipment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param shipmentRecord the shipment record
     *  @param shipmentRecordType shipment record type
     *  @throws org.osid.NullArgumentException
     *          <code>shipmentRecord</code> or
     *          <code>shipmentRecordTypeshipment</code> is
     *          <code>null</code>
     */
            
    protected void addShipmentRecord(org.osid.inventory.shipment.records.ShipmentRecord shipmentRecord, 
                                     org.osid.type.Type shipmentRecordType) {

        nullarg(shipmentRecord, "shipment record");
        addRecordType(shipmentRecordType);
        this.records.add(shipmentRecord);
        
        return;
    }
}
