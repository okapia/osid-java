//
// MutableMapProxyActivityLookupSession
//
//    Implements an Activity lookup service backed by a collection of
//    activities that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning;


/**
 *  Implements an Activity lookup service backed by a collection of
 *  activities. The activities are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of activities can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyActivityLookupSession
    extends net.okapia.osid.jamocha.core.learning.spi.AbstractMapActivityLookupSession
    implements org.osid.learning.ActivityLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyActivityLookupSession}
     *  with no activities.
     *
     *  @param objectiveBank the objective bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                                  org.osid.proxy.Proxy proxy) {
        setObjectiveBank(objectiveBank);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyActivityLookupSession} with a
     *  single activity.
     *
     *  @param objectiveBank the objective bank
     *  @param activity an activity
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBank},
     *          {@code activity}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                                org.osid.learning.Activity activity, org.osid.proxy.Proxy proxy) {
        this(objectiveBank, proxy);
        putActivity(activity);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyActivityLookupSession} using an
     *  array of activities.
     *
     *  @param objectiveBank the objective bank
     *  @param activities an array of activities
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBank},
     *          {@code activities}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                                org.osid.learning.Activity[] activities, org.osid.proxy.Proxy proxy) {
        this(objectiveBank, proxy);
        putActivities(activities);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyActivityLookupSession} using a
     *  collection of activities.
     *
     *  @param objectiveBank the objective bank
     *  @param activities a collection of activities
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBank},
     *          {@code activities}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                                java.util.Collection<? extends org.osid.learning.Activity> activities,
                                                org.osid.proxy.Proxy proxy) {
   
        this(objectiveBank, proxy);
        setSessionProxy(proxy);
        putActivities(activities);
        return;
    }

    
    /**
     *  Makes a {@code Activity} available in this session.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException {@code activity{@code 
     *          is {@code null}
     */

    @Override
    public void putActivity(org.osid.learning.Activity activity) {
        super.putActivity(activity);
        return;
    }


    /**
     *  Makes an array of activities available in this session.
     *
     *  @param activities an array of activities
     *  @throws org.osid.NullArgumentException {@code activities{@code 
     *          is {@code null}
     */

    @Override
    public void putActivities(org.osid.learning.Activity[] activities) {
        super.putActivities(activities);
        return;
    }


    /**
     *  Makes collection of activities available in this session.
     *
     *  @param activities
     *  @throws org.osid.NullArgumentException {@code activity{@code 
     *          is {@code null}
     */

    @Override
    public void putActivities(java.util.Collection<? extends org.osid.learning.Activity> activities) {
        super.putActivities(activities);
        return;
    }


    /**
     *  Removes a Activity from this session.
     *
     *  @param activityId the {@code Id} of the activity
     *  @throws org.osid.NullArgumentException {@code activityId{@code  is
     *          {@code null}
     */

    @Override
    public void removeActivity(org.osid.id.Id activityId) {
        super.removeActivity(activityId);
        return;
    }    
}
