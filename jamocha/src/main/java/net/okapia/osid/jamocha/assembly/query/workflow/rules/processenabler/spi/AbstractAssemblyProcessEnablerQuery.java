//
// AbstractAssemblyProcessEnablerQuery.java
//
//     A ProcessEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.workflow.rules.processenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A ProcessEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyProcessEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.workflow.rules.ProcessEnablerQuery,
               org.osid.workflow.rules.ProcessEnablerQueryInspector,
               org.osid.workflow.rules.ProcessEnablerSearchOrder {

    private final java.util.Collection<org.osid.workflow.rules.records.ProcessEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.ProcessEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.workflow.rules.records.ProcessEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyProcessEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyProcessEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to a process. 
     *
     *  @param  processId the process <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> processId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledProcessId(org.osid.id.Id processId, boolean match) {
        getAssembler().addIdTerm(getRuledProcessIdColumn(), processId, match);
        return;
    }


    /**
     *  Clears the process <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledProcessIdTerms() {
        getAssembler().clearTerms(getRuledProcessIdColumn());
        return;
    }


    /**
     *  Gets the process <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledProcessIdTerms() {
        return (getAssembler().getIdTerms(getRuledProcessIdColumn()));
    }


    /**
     *  Gets the RuledProcessId column name.
     *
     * @return the column name
     */

    protected String getRuledProcessIdColumn() {
        return ("ruled_process_id");
    }


    /**
     *  Tests if a <code> ProcessQuery </code> is available. 
     *
     *  @return <code> true </code> if a process query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledProcessQuery() {
        return (false);
    }


    /**
     *  Gets the query for a process. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the process query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledProcessQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQuery getRuledProcessQuery() {
        throw new org.osid.UnimplementedException("supportsRuledProcessQuery() is false");
    }


    /**
     *  Matches rules mapped to any process. 
     *
     *  @param  match <code> true </code> for rules mapped to any process, 
     *          <code> false </code> to match rules mapped to no processs 
     */

    @OSID @Override
    public void matchAnyRuledProcess(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledProcessColumn(), match);
        return;
    }


    /**
     *  Clears the process query terms. 
     */

    @OSID @Override
    public void clearRuledProcessTerms() {
        getAssembler().clearTerms(getRuledProcessColumn());
        return;
    }


    /**
     *  Gets the process query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.ProcessQueryInspector[] getRuledProcessTerms() {
        return (new org.osid.workflow.ProcessQueryInspector[0]);
    }


    /**
     *  Gets the RuledProcess column name.
     *
     * @return the column name
     */

    protected String getRuledProcessColumn() {
        return ("ruled_process");
    }


    /**
     *  Matches enablers mapped to an office. 
     *
     *  @param  officeId the office <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> officeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchOfficeId(org.osid.id.Id officeId, boolean match) {
        getAssembler().addIdTerm(getOfficeIdColumn(), officeId, match);
        return;
    }


    /**
     *  Clears the office <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearOfficeIdTerms() {
        getAssembler().clearTerms(getOfficeIdColumn());
        return;
    }


    /**
     *  Gets the office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getOfficeIdTerms() {
        return (getAssembler().getIdTerms(getOfficeIdColumn()));
    }


    /**
     *  Gets the OfficeId column name.
     *
     * @return the column name
     */

    protected String getOfficeIdColumn() {
        return ("office_id");
    }


    /**
     *  Tests if an <code> OfficeQuery </code> is available. 
     *
     *  @return <code> true </code> if an office query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsOfficeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an office. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the office query 
     *  @throws org.osid.UnimplementedException <code> supportsOfficeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQuery getOfficeQuery() {
        throw new org.osid.UnimplementedException("supportsOfficeQuery() is false");
    }


    /**
     *  Clears the office query terms. 
     */

    @OSID @Override
    public void clearOfficeTerms() {
        getAssembler().clearTerms(getOfficeColumn());
        return;
    }


    /**
     *  Gets the office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.workflow.OfficeQueryInspector[] getOfficeTerms() {
        return (new org.osid.workflow.OfficeQueryInspector[0]);
    }


    /**
     *  Gets the Office column name.
     *
     * @return the column name
     */

    protected String getOfficeColumn() {
        return ("office");
    }


    /**
     *  Tests if this processEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  processEnablerRecordType a process enabler record type 
     *  @return <code>true</code> if the processEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type processEnablerRecordType) {
        for (org.osid.workflow.rules.records.ProcessEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(processEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  processEnablerRecordType the process enabler record type 
     *  @return the process enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.ProcessEnablerQueryRecord getProcessEnablerQueryRecord(org.osid.type.Type processEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.ProcessEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(processEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  processEnablerRecordType the process enabler record type 
     *  @return the process enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.ProcessEnablerQueryInspectorRecord getProcessEnablerQueryInspectorRecord(org.osid.type.Type processEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.ProcessEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(processEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param processEnablerRecordType the process enabler record type
     *  @return the process enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(processEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.workflow.rules.records.ProcessEnablerSearchOrderRecord getProcessEnablerSearchOrderRecord(org.osid.type.Type processEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.workflow.rules.records.ProcessEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(processEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(processEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this process enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param processEnablerQueryRecord the process enabler query record
     *  @param processEnablerQueryInspectorRecord the process enabler query inspector
     *         record
     *  @param processEnablerSearchOrderRecord the process enabler search order record
     *  @param processEnablerRecordType process enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>processEnablerQueryRecord</code>,
     *          <code>processEnablerQueryInspectorRecord</code>,
     *          <code>processEnablerSearchOrderRecord</code> or
     *          <code>processEnablerRecordTypeprocessEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addProcessEnablerRecords(org.osid.workflow.rules.records.ProcessEnablerQueryRecord processEnablerQueryRecord, 
                                      org.osid.workflow.rules.records.ProcessEnablerQueryInspectorRecord processEnablerQueryInspectorRecord, 
                                      org.osid.workflow.rules.records.ProcessEnablerSearchOrderRecord processEnablerSearchOrderRecord, 
                                      org.osid.type.Type processEnablerRecordType) {

        addRecordType(processEnablerRecordType);

        nullarg(processEnablerQueryRecord, "process enabler query record");
        nullarg(processEnablerQueryInspectorRecord, "process enabler query inspector record");
        nullarg(processEnablerSearchOrderRecord, "process enabler search odrer record");

        this.queryRecords.add(processEnablerQueryRecord);
        this.queryInspectorRecords.add(processEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(processEnablerSearchOrderRecord);
        
        return;
    }
}
