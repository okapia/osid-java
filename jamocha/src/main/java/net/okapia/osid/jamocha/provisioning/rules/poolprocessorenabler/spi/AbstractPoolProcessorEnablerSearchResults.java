//
// AbstractPoolProcessorEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractPoolProcessorEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.rules.PoolProcessorEnablerSearchResults {

    private org.osid.provisioning.rules.PoolProcessorEnablerList poolProcessorEnablers;
    private final org.osid.provisioning.rules.PoolProcessorEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractPoolProcessorEnablerSearchResults.
     *
     *  @param poolProcessorEnablers the result set
     *  @param poolProcessorEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>poolProcessorEnablers</code>
     *          or <code>poolProcessorEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractPoolProcessorEnablerSearchResults(org.osid.provisioning.rules.PoolProcessorEnablerList poolProcessorEnablers,
                                            org.osid.provisioning.rules.PoolProcessorEnablerQueryInspector poolProcessorEnablerQueryInspector) {
        nullarg(poolProcessorEnablers, "pool processor enablers");
        nullarg(poolProcessorEnablerQueryInspector, "pool processor enabler query inspectpr");

        this.poolProcessorEnablers = poolProcessorEnablers;
        this.inspector = poolProcessorEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the pool processor enabler list resulting from a search.
     *
     *  @return a pool processor enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.rules.PoolProcessorEnablerList getPoolProcessorEnablers() {
        if (this.poolProcessorEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.rules.PoolProcessorEnablerList poolProcessorEnablers = this.poolProcessorEnablers;
        this.poolProcessorEnablers = null;
	return (poolProcessorEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.rules.PoolProcessorEnablerQueryInspector getPoolProcessorEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  pool processor enabler search record <code> Type. </code> This method must
     *  be used to retrieve a poolProcessorEnabler implementing the requested
     *  record.
     *
     *  @param poolProcessorEnablerSearchRecordType a poolProcessorEnabler search 
     *         record type 
     *  @return the pool processor enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>poolProcessorEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(poolProcessorEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorEnablerSearchResultsRecord getPoolProcessorEnablerSearchResultsRecord(org.osid.type.Type poolProcessorEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.rules.records.PoolProcessorEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(poolProcessorEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record pool processor enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addPoolProcessorEnablerRecord(org.osid.provisioning.rules.records.PoolProcessorEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "pool processor enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
