//
// AbstractAssessmentProxyManager.java
//
//     An adapter for a AssessmentProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.assessment.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a AssessmentProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterAssessmentProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.assessment.AssessmentProxyManager>
    implements org.osid.assessment.AssessmentProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterAssessmentProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterAssessmentProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterAssessmentProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a session is available to lookup taken assessments for the 
     *  authenticated agent. 
     *
     *  @return <code> true </code> if my assessment taken session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMyAssessmentTaken() {
        return (getAdapteeManager().supportsMyAssessmentTaken());
    }


    /**
     *  Tests for the availability of a assessment service which is the 
     *  service for taking and examining assessments taken. 
     *
     *  @return <code> true </code> if assessment is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessment() {
        return (getAdapteeManager().supportsAssessment());
    }


    /**
     *  Tests for the availability of an assessment rsults service. 
     *
     *  @return <code> true </code> if assessment results is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentResults() {
        return (getAdapteeManager().supportsAssessmentResults());
    }


    /**
     *  Tests if an item lookup service is supported. 
     *
     *  @return true if item lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsItemLookup() {
        return (getAdapteeManager().supportsItemLookup());
    }


    /**
     *  Tests if an item query service is supported. 
     *
     *  @return <code> true </code> if item query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemQuery() {
        return (getAdapteeManager().supportsItemQuery());
    }


    /**
     *  Tests if an item search service is supported. 
     *
     *  @return <code> true </code> if item search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSearch() {
        return (getAdapteeManager().supportsItemSearch());
    }


    /**
     *  Tests if an item administrative service is supported. 
     *
     *  @return <code> true </code> if item admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemAdmin() {
        return (getAdapteeManager().supportsItemAdmin());
    }


    /**
     *  Tests if item notification is supported. Messages may be sent when 
     *  items are created, modified, or deleted. 
     *
     *  @return <code> true </code> if item notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemNotification() {
        return (getAdapteeManager().supportsItemNotification());
    }


    /**
     *  Tests if an item to bank lookup session is available. 
     *
     *  @return <code> true </code> if item bank lookup session is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBank() {
        return (getAdapteeManager().supportsItemBank());
    }


    /**
     *  Tests if an item to bank assignment session is available. 
     *
     *  @return <code> true </code> if item bank assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemBankAssignment() {
        return (getAdapteeManager().supportsItemBankAssignment());
    }


    /**
     *  Tests if an item smart bank session is available. 
     *
     *  @return <code> true </code> if item smart bank session is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsItemSmartBank() {
        return (getAdapteeManager().supportsItemSmartBank());
    }


    /**
     *  Tests if an assessment lookup service is supported. An assessment 
     *  lookup service defines methods to access assessments. 
     *
     *  @return true if assessment lookup is supported, false otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentLookup() {
        return (getAdapteeManager().supportsAssessmentLookup());
    }


    /**
     *  Tests if an assessment query service is supported. 
     *
     *  @return <code> true </code> if assessment query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentQuery() {
        return (getAdapteeManager().supportsAssessmentQuery());
    }


    /**
     *  Tests if an assessment search service is supported. 
     *
     *  @return <code> true </code> if assessment search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSearch() {
        return (getAdapteeManager().supportsAssessmentSearch());
    }


    /**
     *  Tests if an assessment administrative service is supported. 
     *
     *  @return <code> true </code> if assessment admin is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentAdmin() {
        return (getAdapteeManager().supportsAssessmentAdmin());
    }


    /**
     *  Tests if assessment notification is supported. Messages may be sent 
     *  when assessments are created, modified, or deleted. 
     *
     *  @return <code> true </code> if assessment notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentNotification() {
        return (getAdapteeManager().supportsAssessmentNotification());
    }


    /**
     *  Tests if an assessment to bank lookup session is available. 
     *
     *  @return <code> true </code> if assessment bank lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentBank() {
        return (getAdapteeManager().supportsAssessmentBank());
    }


    /**
     *  Tests if an assessment to bank assignment session is available. 
     *
     *  @return <code> true </code> if assessment bank assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentBankAssignment() {
        return (getAdapteeManager().supportsAssessmentBankAssignment());
    }


    /**
     *  Tests if an assessment smart bank session is available. 
     *
     *  @return <code> true </code> if assessment smart bank session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentSmartBank() {
        return (getAdapteeManager().supportsAssessmentSmartBank());
    }


    /**
     *  Tests if an assessment basic authoring session is available. 
     *
     *  @return <code> true </code> if assessment basic authoring is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentBasicAuthoring() {
        return (getAdapteeManager().supportsAssessmentBasicAuthoring());
    }


    /**
     *  Tests if an assessment offered lookup service is supported. 
     *
     *  @return true if assessment offered lookup is supported, false 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedLookup() {
        return (getAdapteeManager().supportsAssessmentOfferedLookup());
    }


    /**
     *  Tests if an assessment offered query service is supported. 
     *
     *  @return <code> true </code> if assessment offered query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedQuery() {
        return (getAdapteeManager().supportsAssessmentOfferedQuery());
    }


    /**
     *  Tests if an assessment offered search service is supported. 
     *
     *  @return <code> true </code> if assessment offered search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedSearch() {
        return (getAdapteeManager().supportsAssessmentOfferedSearch());
    }


    /**
     *  Tests if an assessment offered administrative service is supported. 
     *
     *  @return <code> true </code> if assessment offered admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedAdmin() {
        return (getAdapteeManager().supportsAssessmentOfferedAdmin());
    }


    /**
     *  Tests if assessment offered notification is supported. Messages may be 
     *  sent when offered assessments are created, modified, or deleted. 
     *
     *  @return <code> true </code> if assessment offered notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedNotification() {
        return (getAdapteeManager().supportsAssessmentOfferedNotification());
    }


    /**
     *  Tests if an assessment offered to bank lookup session is available. 
     *
     *  @return <code> true </code> if assessment offered bank lookup session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedBank() {
        return (getAdapteeManager().supportsAssessmentOfferedBank());
    }


    /**
     *  Tests if an assessment offered to bank assignment session is 
     *  available. 
     *
     *  @return <code> true </code> if assessment offered bank assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedBankAssignment() {
        return (getAdapteeManager().supportsAssessmentOfferedBankAssignment());
    }


    /**
     *  Tests if an assessment offered smart bank session is available. 
     *
     *  @return <code> true </code> if assessment offered smart bank session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedSmartBank() {
        return (getAdapteeManager().supportsAssessmentOfferedSmartBank());
    }


    /**
     *  Tests if an assessment taken lookup service is supported. 
     *
     *  @return <code> true </code> if assessment taken lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenLookup() {
        return (getAdapteeManager().supportsAssessmentTakenLookup());
    }


    /**
     *  Tests if an assessment taken query service is supported. 
     *
     *  @return <code> true </code> if assessment taken query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenQuery() {
        return (getAdapteeManager().supportsAssessmentTakenQuery());
    }


    /**
     *  Tests if an assessment taken search service is supported. 
     *
     *  @return <code> true </code> if assessment taken search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenSearch() {
        return (getAdapteeManager().supportsAssessmentTakenSearch());
    }


    /**
     *  Tests if an assessment taken administrative service is supported which 
     *  is used to instantiate an assessment offered. 
     *
     *  @return <code> true </code> if assessment taken admin is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenAdmin() {
        return (getAdapteeManager().supportsAssessmentTakenAdmin());
    }


    /**
     *  Tests if assessment taken notification is supported. Messages may be 
     *  sent when items are created, modified, or deleted. 
     *
     *  @return <code> true </code> if assessment taken notification is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenNotification() {
        return (getAdapteeManager().supportsAssessmentTakenNotification());
    }


    /**
     *  Tests if an assessment taken to bank lookup session is available. 
     *
     *  @return <code> true </code> if assessment taken bank lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenBank() {
        return (getAdapteeManager().supportsAssessmentTakenBank());
    }


    /**
     *  Tests if an assessment taken to bank assignment session is available. 
     *
     *  @return <code> true </code> if assessment taken bank assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenBankAssignment() {
        return (getAdapteeManager().supportsAssessmentTakenBankAssignment());
    }


    /**
     *  Tests if an assessment taken smart bank session is available. 
     *
     *  @return <code> true </code> if assessment taken smart bank session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenSmartBank() {
        return (getAdapteeManager().supportsAssessmentTakenSmartBank());
    }


    /**
     *  Tests if a bank lookup service is supported. A bank lookup service 
     *  defines methods to access assessment banks. 
     *
     *  @return <code> true </code> if bank lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankLookup() {
        return (getAdapteeManager().supportsBankLookup());
    }


    /**
     *  Tests if a bank query service is supported. 
     *
     *  @return <code> true </code> if bank query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankQuery() {
        return (getAdapteeManager().supportsBankQuery());
    }


    /**
     *  Tests if a bank search service is supported. 
     *
     *  @return <code> true </code> if bank search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankSearch() {
        return (getAdapteeManager().supportsBankSearch());
    }


    /**
     *  Tests if a banlk administrative service is supported. 
     *
     *  @return <code> true </code> if bank admin is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankAdmin() {
        return (getAdapteeManager().supportsBankAdmin());
    }


    /**
     *  Tests if bank notification is supported. Messages may be sent when 
     *  items are created, modified, or deleted. 
     *
     *  @return <code> true </code> if bank notification is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankNotification() {
        return (getAdapteeManager().supportsBankNotification());
    }


    /**
     *  Tests if a bank hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a bank hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankHierarchy() {
        return (getAdapteeManager().supportsBankHierarchy());
    }


    /**
     *  Tests if bank hierarchy design is supported. 
     *
     *  @return <code> true </code> if a bank hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBankHierarchyDesign() {
        return (getAdapteeManager().supportsBankHierarchyDesign());
    }


    /**
     *  Tests if an assessment authoring service is supported. 
     *
     *  @return <code> true </code> if an assessment authoring is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentAuthoring() {
        return (getAdapteeManager().supportsAssessmentAuthoring());
    }


    /**
     *  Tests if an assessment batch service is supported. 
     *
     *  @return <code> true </code> if an assessment batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssessmentBatch() {
        return (getAdapteeManager().supportsAssessmentBatch());
    }


    /**
     *  Gets the supported <code> Item </code> record types. 
     *
     *  @return a list containing the supported <code> Item </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemRecordTypes() {
        return (getAdapteeManager().getItemRecordTypes());
    }


    /**
     *  Tests if the given <code> Item </code> record type is supported. 
     *
     *  @param  itemRecordType a <code> Type </code> indicating a <code> Item 
     *          </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemRecordType(org.osid.type.Type itemRecordType) {
        return (getAdapteeManager().supportsItemRecordType(itemRecordType));
    }


    /**
     *  Gets the supported <code> Item </code> search record types. 
     *
     *  @return a list containing the supported <code> Item </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getItemSearchRecordTypes() {
        return (getAdapteeManager().getItemSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Item </code> search record type is 
     *  supported. 
     *
     *  @param  itemSearchRecordType a <code> Type </code> indicating an 
     *          <code> Item </code> search record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> itemSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsItemSearchRecordType(org.osid.type.Type itemSearchRecordType) {
        return (getAdapteeManager().supportsItemSearchRecordType(itemSearchRecordType));
    }


    /**
     *  Gets the supported <code> Assessment </code> record types. 
     *
     *  @return a list containing the supported <code> Assessment </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentRecordTypes() {
        return (getAdapteeManager().getAssessmentRecordTypes());
    }


    /**
     *  Tests if the given <code> Assessment </code> record type is supported. 
     *
     *  @param  assessmentRecordType a <code> Type </code> indicating an 
     *          <code> Assessment </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> assessmentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentRecordType(org.osid.type.Type assessmentRecordType) {
        return (getAdapteeManager().supportsAssessmentRecordType(assessmentRecordType));
    }


    /**
     *  Gets the supported <code> Assessment </code> search record types. 
     *
     *  @return a list containing the supported assessment search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentSearchRecordTypes() {
        return (getAdapteeManager().getAssessmentSearchRecordTypes());
    }


    /**
     *  Tests if the given assessment search record type is supported. 
     *
     *  @param  assessmentSearchRecordType a <code> Type </code> indicating an 
     *          assessment search record type 
     *  @return <code> true </code> if the given search record Type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentSearchRecordType(org.osid.type.Type assessmentSearchRecordType) {
        return (getAdapteeManager().supportsAssessmentSearchRecordType(assessmentSearchRecordType));
    }


    /**
     *  Gets the supported <code> AssessmentOffered </code> record types. 
     *
     *  @return a list containing the supported <code> AssessmentOffered 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentOfferedRecordTypes() {
        return (getAdapteeManager().getAssessmentOfferedRecordTypes());
    }


    /**
     *  Tests if the given <code> AssessmentOffered </code> record type is 
     *  supported. 
     *
     *  @param  assessmentOfferedRecordType a <code> Type </code> indicating 
     *          an <code> AssessmentOffered </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentOfferedRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedRecordType(org.osid.type.Type assessmentOfferedRecordType) {
        return (getAdapteeManager().supportsAssessmentOfferedRecordType(assessmentOfferedRecordType));
    }


    /**
     *  Gets the supported <code> AssessmentOffered </code> search record 
     *  types. 
     *
     *  @return a list containing the supported <code> AssessmentOffered 
     *          </code> search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentOfferedSearchRecordTypes() {
        return (getAdapteeManager().getAssessmentOfferedSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AssessmentOffered </code> search record type 
     *  is supported. 
     *
     *  @param  assessmentOfferedSearchRecordType a <code> Type </code> 
     *          indicating an <code> AssessmentOffered </code> search record 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentOfferedSearchRecordType </code> is <code> null 
     *          </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentOfferedSearchRecordType(org.osid.type.Type assessmentOfferedSearchRecordType) {
        return (getAdapteeManager().supportsAssessmentOfferedSearchRecordType(assessmentOfferedSearchRecordType));
    }


    /**
     *  Gets the supported <code> AssessmentTaken </code> record types. 
     *
     *  @return a list containing the supported <code> AssessmentTaken </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentTakenRecordTypes() {
        return (getAdapteeManager().getAssessmentTakenRecordTypes());
    }


    /**
     *  Tests if the given <code> AssessmentTaken </code> record type is 
     *  supported. 
     *
     *  @param  assessmentTakenRecordType a <code> Type </code> indicating an 
     *          <code> AssessmentTaken </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentTakenRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenRecordType(org.osid.type.Type assessmentTakenRecordType) {
        return (getAdapteeManager().supportsAssessmentTakenRecordType(assessmentTakenRecordType));
    }


    /**
     *  Gets the supported <code> AssessmentTaken </code> search record types. 
     *
     *  @return a list containing the supported <code> AssessmentTaken </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentTakenSearchRecordTypes() {
        return (getAdapteeManager().getAssessmentTakenSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> AssessmentTaken </code> search record type 
     *  is supported. 
     *
     *  @param  assessmentTakenSearchRecordType a <code> Type </code> 
     *          indicating an <code> AssessmentTaken </code> search record 
     *          type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentTakenSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentTakenSearchRecordType(org.osid.type.Type assessmentTakenSearchRecordType) {
        return (getAdapteeManager().supportsAssessmentTakenSearchRecordType(assessmentTakenSearchRecordType));
    }


    /**
     *  Gets the supported <code> AssessmentSection </code> record types. 
     *
     *  @return a list containing the supported <code> AssessmentSection 
     *          </code> record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAssessmentSectionRecordTypes() {
        return (getAdapteeManager().getAssessmentSectionRecordTypes());
    }


    /**
     *  Tests if the given <code> AssessmentSection </code> record type is 
     *  supported. 
     *
     *  @param  assessmentSectionRecordType a <code> Type </code> indicating 
     *          an <code> AssessmentSection </code> record type 
     *  @return <code> true </code> if the given Type is supported, <code> 
     *          false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentSectionRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAssessmentSectionRecordType(org.osid.type.Type assessmentSectionRecordType) {
        return (getAdapteeManager().supportsAssessmentSectionRecordType(assessmentSectionRecordType));
    }


    /**
     *  Gets the supported <code> Bank </code> record types. 
     *
     *  @return a list containing the supported <code> Bank </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBankRecordTypes() {
        return (getAdapteeManager().getBankRecordTypes());
    }


    /**
     *  Tests if the given <code> Bank </code> record type is supported. 
     *
     *  @param  bankRecordType a <code> Type </code> indicating a <code> Bank 
     *          </code> type 
     *  @return <code> true </code> if the given key record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bankRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBankRecordType(org.osid.type.Type bankRecordType) {
        return (getAdapteeManager().supportsBankRecordType(bankRecordType));
    }


    /**
     *  Gets the supported bank search record types. 
     *
     *  @return a list containing the supported <code> Bank </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBankSearchRecordTypes() {
        return (getAdapteeManager().getBankSearchRecordTypes());
    }


    /**
     *  Tests if the given bank search record type is supported. 
     *
     *  @param  bankSearchRecordType a <code> Type </code> indicating a <code> 
     *          Bank </code> search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> bankSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBankSearchRecordType(org.osid.type.Type bankSearchRecordType) {
        return (getAdapteeManager().supportsBankSearchRecordType(bankSearchRecordType));
    }


    /**
     *  Gets a <code> MyAssessmentTakenSession </code> to retrieve assessments 
     *  taken for the current agent. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MyAssessmentTakenSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyAssessmentTaken() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.MyAssessmentTakenSession getMyAssessmentTakenSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMyAssessmentTakenSession(proxy));
    }


    /**
     *  Gets a <code> MyAssessmentTakenSession </code> to retrieve assessments 
     *  taken for the current agent for the given bank <code> Id. </code> 
     *
     *  @param  bankId the <code> Id </code> of a bank 
     *  @param  proxy a proxy 
     *  @return a <code> MyAssessmentTakenSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMyAssessmentTaken() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.MyAssessmentTakenSession getMyAssessmentTakenSessionForBank(org.osid.id.Id bankId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMyAssessmentTakenSessionForBank(bankId, proxy));
    }


    /**
     *  Gets an <code> AssessmentSession </code> which is responsible for 
     *  taking assessments and examining responses from assessments taken. 
     *
     *  @param  proxy a proxy 
     *  @return an assessment session for this service 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssessment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSession getAssessmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentSession(proxy));
    }


    /**
     *  Gets an <code> AssessmentSession </code> which is responsible for 
     *  performing assessments for the given bank <code> Id. </code> 
     *
     *  @param  bankId the <code> Id </code> of a bank 
     *  @param  proxy a proxy 
     *  @return an assessment session for this service 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAssessment() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSession getAssessmentSessionForBank(org.osid.id.Id bankId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentSessionForBank(bankId, proxy));
    }


    /**
     *  Gets an <code> AssessmentResultsSession </code> to retrieve assessment 
     *  results. 
     *
     *  @param  proxy a proxy 
     *  @return an assessment results session for this service 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentResults() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentResultsSession getAssessmentResultsSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentResultsSession(proxy));
    }


    /**
     *  Gets an <code> AssessmentResultsSession </code> to retrieve assessment 
     *  results for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the assessment taken 
     *  @param  proxy a proxy 
     *  @return an assessment results session for this service 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentResults() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentResultsSession getAssessmentResultsSessionForBank(org.osid.id.Id bankId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentResultsSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemLookupSession getItemLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item lookup 
     *  service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an ItemLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsItemLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemLookupSession getItemLookupSessionForBank(org.osid.id.Id bankId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemLookupSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemQuerySession getItemQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item query 
     *  service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an ItemQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsItemQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemQuerySession getItemQuerySessionForBank(org.osid.id.Id bankId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemQuerySessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemSearchSession getItemSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item search 
     *  service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an ItemSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          porxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsItemSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemSearchSession getItemSearchSessionForBank(org.osid.id.Id bankId, 
                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemSearchSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemAdminSession getItemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item admin 
     *  service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an ItemAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsItemAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemAdminSession getItemAdminSessionForBank(org.osid.id.Id bankId, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemAdminSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to item 
     *  changes. 
     *
     *  @param  itemReceiver the item receiver interface 
     *  @param  proxy a proxy 
     *  @return an <code> ItemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> itemReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemNotificationSession getItemNotificationSession(org.osid.assessment.ItemReceiver itemReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemNotificationSession(itemReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item 
     *  notification service for the given bank. 
     *
     *  @param  itemReceiver the item receiver interface 
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an ItemNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> itemReceiver, bankId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentNotificationSession getItemNotificationSessionForBank(org.osid.assessment.ItemReceiver itemReceiver, 
                                                                                               org.osid.id.Id bankId, 
                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemNotificationSessionForBank(itemReceiver, bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item banking 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemBankSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemBank() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemBankSession getItemBankSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBankSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item bank 
     *  assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ItemBankAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsItemBankAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemBankAssignmentSession getItemBankAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getItemBankAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the item smart 
     *  banking service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> ItemSmartBankSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsItemSmartBank() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.ItemSmartBankSession getItemSmartBankSession(org.osid.id.Id bankId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getItemSmartBankSession(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentLookupSession getAssessmentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an AssessmentLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentLookupSession getAssessmentLookupSessionForBank(org.osid.id.Id bankId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentLookupSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuerySession getAssessmentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an AssessmentQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentQuerySession getAssessmentQuerySessionForBank(org.osid.id.Id bankId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentQuerySessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchSession getAssessmentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  search service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an AssessmentSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSearchSession getAssessmentSearchSessionForBank(org.osid.id.Id bankId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentSearchSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentAdminSession getAssessmentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  admin service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an AssessmentAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentAdminSession getAssessmentAdminSessionForBank(org.osid.id.Id bankId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentAdminSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to 
     *  assessment changes. 
     *
     *  @param  assessmentReceiver the assessment receiver interface 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentNotificationSession getAssessmentNotificationSession(org.osid.assessment.AssessmentReceiver assessmentReceiver, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentNotificationSession(assessmentReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  notification service for the given bank. 
     *
     *  @param  assessmentReceiver the assessment receiver interface 
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return <code> an AssessmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> assessmentReceiver, 
     *          bankId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentNotificationSession getAssessmentNotificationSessionForBank(org.osid.assessment.AssessmentReceiver assessmentReceiver, 
                                                                                                     org.osid.id.Id bankId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentNotificationSessionForBank(assessmentReceiver, bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  banking service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentBankSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentBank() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentBankSession getAssessmentBankSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentBankSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  bank assignment service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentBankAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentBankAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentBankAssignmentSession getAssessmentBankAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentBankAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  smart banking service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentSmartBankSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentSmartBank() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentSmartBankSession getAssessmentSmartBankSession(org.osid.id.Id bankId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentSmartBankSession(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  authoring service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentBasicAuthoringSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentBasicAuthoring() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentBasicAuthoringSession getAssessmentBasicAuthoringSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentBasicAuthoringSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  authoring service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of a bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentBasicAuthoringSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentBasicAuthoring() </code> or <code> 
     *          supportsVisibeFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentBasicAuthoringSession getAssessmentBasicAuthoringSessionForBank(org.osid.id.Id bankId, 
                                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentBasicAuthoringSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  offered lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedLookupSession getAssessmentOfferedLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  offered lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedLookupSession getAssessmentOfferedLookupSessionForBank(org.osid.id.Id bankId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedLookupSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  offered query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuerySession getAssessmentOfferedQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  offered query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedQuerySession getAssessmentOfferedQuerySessionForBank(org.osid.id.Id bankId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedQuerySessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  offered search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedSearchSession getAssessmentOfferedSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  offered search service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or proxy 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedSearchSession getAssessmentOfferedSearchSessionForBank(org.osid.id.Id bankId, 
                                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedSearchSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  offered administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedAdminSession getAssessmentOfferedAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  offered admin service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedAdminSession getAssessmentOfferedAdminSessionForBank(org.osid.id.Id bankId, 
                                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedAdminSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to offered 
     *  assessment changes. 
     *
     *  @param  assessmentOfferedReceiver the assessment offered receiver 
     *          interface 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentOfferedReceiver </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedNotification() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedNotificationSession getAssessmentOfferedNotificationSession(org.osid.assessment.AssessmentOfferedReceiver assessmentOfferedReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedNotificationSession(assessmentOfferedReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the offered 
     *  assessment notification service for the given bank. 
     *
     *  @param  assessmentOfferedReceiver the assessment offered receiver 
     *          interface 
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return a <code> AssessmentOfferedNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> or <code> 
     *          proxy </code> not found 
     *  @throws org.osid.NullArgumentException <code> 
     *          assessmentOfferedReceiver, bankId </code> or <code> proxy 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedNotificationSession getAssessmentOfferedNotificationSessionForBank(org.osid.assessment.AssessmentOfferedReceiver assessmentOfferedReceiver, 
                                                                                                                   org.osid.id.Id bankId, 
                                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedNotificationSessionForBank(assessmentOfferedReceiver, bankId, proxy));
    }


    /**
     *  Gets the session for retrieving offered assessments to bank mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedBankSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedBankSession getAssessmentOfferedBankSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedBankSession(proxy));
    }


    /**
     *  Gets the session for assigning offered assessments to bank mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedBankAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedBankAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedBankAssignmentSession getAssessmentOfferedBankAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedBankAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  offered smart banking service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentOfferedSmartBankSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentOfferedSmartBank() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentOfferedSmartBankSession getAssessmentOfferedSmartBankSession(org.osid.id.Id bankId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentOfferedSmartBankSession(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  taken lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenLookupSession getAssessmentTakenLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  taken lookup service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenLookupSession getAssessmentTakenLookupSessionForBank(org.osid.id.Id bankId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenLookupSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  taken query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQuerySession getAssessmentTakenQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  taken query service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenQuerySession getAssessmentTakenQuerySessionForBank(org.osid.id.Id bankId, 
                                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenQuerySessionForBank(bankId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  taken search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenSearchSession getAssessmentTakenSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the assessment 
     *  taken search service for the given bank. 
     *
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> bankId </code> or <code> 
     *          proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenSearchSession getAssessmentTakenSearchSessionForBank(org.osid.id.Id bankId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenSearchSessionForBank(bankId, proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to taken 
     *  assessment changes. 
     *
     *  @param  assessmentTakenReceiver the assessment taken receiver 
     *          interface 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> assessmentTakenReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenNotificationSession getAssessmentTakenNotificationSession(org.osid.assessment.AssessmentTakenReceiver assessmentTakenReceiver, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenNotificationSession(assessmentTakenReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the taken 
     *  assessment notification service for the given bank. 
     *
     *  @param  assessmentTakenReceiver the assessment taken receiver 
     *          interface 
     *  @param  bankId the <code> Id </code> of the bank 
     *  @param  proxy a proxy 
     *  @return an <code> AssessmentTakenNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> bankId </code> not found 
     *  @throws org.osid.NullArgumentException <code> assessmentTakenReceiver, 
     *          bankId </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentTakenNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.assessment.AssessmentTakenNotificationSession getAssessmentTakenNotificationSessionForBank(org.osid.assessment.AssessmentTakenReceiver assessmentTakenReceiver, 
                                                                                                               org.osid.id.Id bankId, 
                                                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentTakenNotificationSessionForBank(assessmentTakenReceiver, bankId, proxy));
    }


    /**
     *  Gets the OsidSession associated with the bank lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BankLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBankLookup() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankLookupSession getBankLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBankLookupSession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the bank query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BankQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBankQuery() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankQuerySession getBankQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBankQuerySession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the bank search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BankSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBankSearch() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankSearchSession getBankSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBankSearchSession(proxy));
    }


    /**
     *  Gets the OsidSession associated with the bank administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BankAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBankAdmin() is 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankAdminSession getBankAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBankAdminSession(proxy));
    }


    /**
     *  Gets the notification session for notifications pertaining to bank 
     *  service changes. 
     *
     *  @param  bankReceiver the bank receiver interface 
     *  @param  proxy a proxy 
     *  @return a <code> BankNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> bankReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBankNotification() is false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankNotificationSession getBankNotificationSession(org.osid.assessment.BankReceiver bankReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBankNotificationSession(bankReceiver, proxy));
    }


    /**
     *  Gets the session traversing bank hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BankHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBankHierarchy() 
     *          is false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankHierarchySession getBankHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBankHierarchySession(proxy));
    }


    /**
     *  Gets the session designing bank hierarchies. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> BankHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBankHierarchyDesign() is false </code> 
     */

    @OSID @Override
    public org.osid.assessment.BankHierarchyDesignSession getBankHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBankHierarchyDesignSession(proxy));
    }


    /**
     *  Gets an <code> AssessmentAuthoringProxyManager. </code> 
     *
     *  @return an <code> AssessmentAuthoringProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentAuthoring() is false </code> 
     */

    @OSID @Override
    public org.osid.assessment.authoring.AssessmentAuthoringProxyManager getAssessmentAuthoringProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentAuthoringProxyManager());
    }


    /**
     *  Gets an <code> AssessmentBatchProxyManager. </code> 
     *
     *  @return an <code> AssessmentBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAssessmentBatch() is false </code> 
     */

    @OSID @Override
    public org.osid.assessment.batch.AssessmentBatchProxyManager getAssessmentBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAssessmentBatchProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
