//
// AbstractQueryCookbookLookupSession.java
//
//    A CookbookQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A CookbookQuerySession adapter.
 */

public abstract class AbstractAdapterCookbookQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.recipe.CookbookQuerySession {

    private final org.osid.recipe.CookbookQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterCookbookQuerySession.
     *
     *  @param session the underlying cookbook query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCookbookQuerySession(org.osid.recipe.CookbookQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@codeCookbook</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchCookbooks() {
        return (this.session.canSearchCookbooks());
    }

      
    /**
     *  Gets a cookbook query. The returned query will not have an
     *  extension query.
     *
     *  @return the cookbook query 
     */
      
    @OSID @Override
    public org.osid.recipe.CookbookQuery getCookbookQuery() {
        return (this.session.getCookbookQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  cookbookQuery the cookbook query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code cookbookQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code cookbookQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.recipe.CookbookList getCookbooksByQuery(org.osid.recipe.CookbookQuery cookbookQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getCookbooksByQuery(cookbookQuery));
    }
}
