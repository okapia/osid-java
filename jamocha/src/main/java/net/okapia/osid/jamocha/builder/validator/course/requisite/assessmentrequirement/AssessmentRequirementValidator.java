//
// AssessmentRequirementValidator.java
//
//     Validates an AssessmentRequirement.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.course.requisite.assessmentrequirement;


/**
 *  Validates an AssessmentRequirement.
 */

public final class AssessmentRequirementValidator
    extends net.okapia.osid.jamocha.builder.validator.course.requisite.assessmentrequirement.spi.AbstractAssessmentRequirementValidator {


    /**
     *  Constructs a new <code>AssessmentRequirementValidator</code>.
     */

    public AssessmentRequirementValidator() {
        return;
    }


    /**
     *  Constructs a new <code>AssessmentRequirementValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public AssessmentRequirementValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates an AssessmentRequirement with a default validation.
     *
     *  @param assessmentRequirement an assessment requirement to validate
     *  @return the assessment requirement
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>assessmentRequirement</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.course.requisite.AssessmentRequirement validateAssessmentRequirement(org.osid.course.requisite.AssessmentRequirement assessmentRequirement) {
        AssessmentRequirementValidator validator = new AssessmentRequirementValidator();
        validator.validate(assessmentRequirement);
        return (assessmentRequirement);
    }


    /**
     *  Validates an AssessmentRequirement for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param assessmentRequirement an assessment requirement to validate
     *  @return the assessment requirement
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>assessmentRequirement</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.course.requisite.AssessmentRequirement validateAssessmentRequirement(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.course.requisite.AssessmentRequirement assessmentRequirement) {

        AssessmentRequirementValidator validator = new AssessmentRequirementValidator(validation);
        validator.validate(assessmentRequirement);
        return (assessmentRequirement);
    }
}
