//
// MutableMapProxyProcedureLookupSession
//
//    Implements a Procedure lookup service backed by a collection of
//    procedures that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.recipe;


/**
 *  Implements a Procedure lookup service backed by a collection of
 *  procedures. The procedures are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of procedures can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyProcedureLookupSession
    extends net.okapia.osid.jamocha.core.recipe.spi.AbstractMapProcedureLookupSession
    implements org.osid.recipe.ProcedureLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyProcedureLookupSession}
     *  with no procedures.
     *
     *  @param cookbook the cookbook
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyProcedureLookupSession(org.osid.recipe.Cookbook cookbook,
                                                  org.osid.proxy.Proxy proxy) {
        setCookbook(cookbook);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyProcedureLookupSession} with a
     *  single procedure.
     *
     *  @param cookbook the cookbook
     *  @param procedure a procedure
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code procedure}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyProcedureLookupSession(org.osid.recipe.Cookbook cookbook,
                                                org.osid.recipe.Procedure procedure, org.osid.proxy.Proxy proxy) {
        this(cookbook, proxy);
        putProcedure(procedure);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyProcedureLookupSession} using an
     *  array of procedures.
     *
     *  @param cookbook the cookbook
     *  @param procedures an array of procedures
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code procedures}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyProcedureLookupSession(org.osid.recipe.Cookbook cookbook,
                                                org.osid.recipe.Procedure[] procedures, org.osid.proxy.Proxy proxy) {
        this(cookbook, proxy);
        putProcedures(procedures);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyProcedureLookupSession} using a
     *  collection of procedures.
     *
     *  @param cookbook the cookbook
     *  @param procedures a collection of procedures
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code cookbook},
     *          {@code procedures}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyProcedureLookupSession(org.osid.recipe.Cookbook cookbook,
                                                java.util.Collection<? extends org.osid.recipe.Procedure> procedures,
                                                org.osid.proxy.Proxy proxy) {
   
        this(cookbook, proxy);
        setSessionProxy(proxy);
        putProcedures(procedures);
        return;
    }

    
    /**
     *  Makes a {@code Procedure} available in this session.
     *
     *  @param procedure an procedure
     *  @throws org.osid.NullArgumentException {@code procedure{@code 
     *          is {@code null}
     */

    @Override
    public void putProcedure(org.osid.recipe.Procedure procedure) {
        super.putProcedure(procedure);
        return;
    }


    /**
     *  Makes an array of procedures available in this session.
     *
     *  @param procedures an array of procedures
     *  @throws org.osid.NullArgumentException {@code procedures{@code 
     *          is {@code null}
     */

    @Override
    public void putProcedures(org.osid.recipe.Procedure[] procedures) {
        super.putProcedures(procedures);
        return;
    }


    /**
     *  Makes collection of procedures available in this session.
     *
     *  @param procedures
     *  @throws org.osid.NullArgumentException {@code procedure{@code 
     *          is {@code null}
     */

    @Override
    public void putProcedures(java.util.Collection<? extends org.osid.recipe.Procedure> procedures) {
        super.putProcedures(procedures);
        return;
    }


    /**
     *  Removes a Procedure from this session.
     *
     *  @param procedureId the {@code Id} of the procedure
     *  @throws org.osid.NullArgumentException {@code procedureId{@code  is
     *          {@code null}
     */

    @Override
    public void removeProcedure(org.osid.id.Id procedureId) {
        super.removeProcedure(procedureId);
        return;
    }    
}
