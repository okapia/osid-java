//
// AbstractLearningObjectiveRequirement.java
//
//     Defines a LearningObjectiveRequirement.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.requisite.learningobjectiverequirement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>LearningObjectiveRequirement</code>.
 */

public abstract class AbstractLearningObjectiveRequirement
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.course.requisite.LearningObjectiveRequirement {

    private org.osid.learning.Objective learningObjective;
    private org.osid.grading.Grade minimumProficiency;

    private final java.util.Collection<org.osid.course.requisite.Requisite> altRequisites = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.course.requisite.records.LearningObjectiveRequirementRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets any <code> Requisites </code> that may be substituted in
     *  place of this <code> LearningObjectiveRequirement. </code> All
     *  <code> Requisites </code> must be satisifed to be a substitute
     *  for this learning objective requirement. Inactive <code>
     *  Requisites </code> are not evaluated but if no applicable
     *  requisite exists, then the alternate requisite is not
     *  satisifed.
     *
     *  @return the alternate requisites 
     */

    @OSID @Override
    public org.osid.course.requisite.Requisite[] getAltRequisites() {
        return (this.altRequisites.toArray(new org.osid.course.requisite.Requisite[this.altRequisites.size()]));
    }


    /**
     *  Adds an alt requisite.
     *
     *  @param altRequisite an alternate requisite
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisite</code> is <code>null</code>
     */

    protected void addAltRequisite(org.osid.course.requisite.Requisite altRequisite) {
        nullarg(altRequisite, "alt requisite");
        this.altRequisites.add(altRequisite);
        return;
    }


    /**
     *  Sets all the alt requisites.
     *
     *  @param altRequisites a collection of alternate requisites
     *  @throws org.osid.NullArgumentException
     *          <code>altRequisites</code> is <code>null</code>
     */

    protected void setAltRequisites(java.util.Collection<org.osid.course.requisite.Requisite> altRequisites) {
        nullarg(altRequisites, "alt requisites");

        this.altRequisites.clear();
        this.altRequisites.addAll(altRequisites);

        return;
    }


    /**
     *  Gets the <code> Id </code> of the <code> LearningObjective. </code> 
     *
     *  @return the learning objective <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLearningObjectiveId() {
        return (this.learningObjective.getId());
    }


    /**
     *  Gets the <code> LearningObjective. </code> 
     *
     *  @return the learning objective 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.Objective getLearningObjective()
        throws org.osid.OperationFailedException {

        return (this.learningObjective);
    }


    /**
     *  Sets the learning objective.
     *
     *  @param objective a learning objective
     *  @throws org.osid.NullArgumentException
     *          <code>objective</code> is <code>null</code>
     */

    protected void setLearningObjective(org.osid.learning.Objective objective) {
        nullarg(objective, "learning objective");
        this.learningObjective = objective;
        return;
    }


    /**
     *  Tests if a minimum grade in proficency is required for this learning 
     *  objective. 
     *
     *  @return <code> true </code> if a minimum proficiency is required, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasMinimumProficiency() {
        return (this.minimumProficiency != null);
    }


    /**
     *  Gets the minimum proficiency expressed as a <code> Grade. </code> 
     *
     *  @return the minimum proficiency 
     *  @throws org.osid.IllegalStateException <code> hasMinimumProficiency() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getMinimumProficiencyId() {
        if (!hasMinimumProficiency()) {
            throw new org.osid.IllegalStateException("hasMinimumProficiency() is null");
        }

        return (this.minimumProficiency.getId());
    }


    /**
     *  Gets the minimum proficiency expressed as a <code> Grade. </code> 
     *
     *  @return the minimum proficiency 
     *  @throws org.osid.IllegalStateException <code> hasMinimumProficiency() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getMinimumProficiency()
        throws org.osid.OperationFailedException {

        if (!hasMinimumProficiency()) {
            throw new org.osid.IllegalStateException("hasMinimumProficiency() is null");
        }

        return (this.minimumProficiency);
    }


    /**
     *  Sets the minimum proficiency.
     *
     *  @param proficiency a minimum proficiency
     *  @throws org.osid.NullArgumentException
     *          <code>proficiency</code> is <code>null</code>
     */

    protected void setMinimumProficiency(org.osid.grading.Grade proficiency) {
        nullarg(proficiency, "minimum proficiency");
        this.minimumProficiency = proficiency;
        return;
    }


    /**
     *  Tests if this learningObjectiveRequirement supports the given record
     *  <code>Type</code>.
     *
     *  @param learningObjectiveRequirementRecordType a learning
     *         objective requirement record type
     *  @return <code>true</code> if the
     *          learningObjectiveRequirementRecordType is supported,
     *          <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type learningObjectiveRequirementRecordType) {
        for (org.osid.course.requisite.records.LearningObjectiveRequirementRecord record : this.records) {
            if (record.implementsRecordType(learningObjectiveRequirementRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>LearningObjectiveRequirement</code> record
     *  <code>Type</code>.
     *
     *  @param learningObjectiveRequirementRecordType the learning
     *         objective requirement record type
     *  @return the learning objective requirement record 
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(learningObjectiveRequirementRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.requisite.records.LearningObjectiveRequirementRecord getLearningObjectiveRequirementRecord(org.osid.type.Type learningObjectiveRequirementRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.requisite.records.LearningObjectiveRequirementRecord record : this.records) {
            if (record.implementsRecordType(learningObjectiveRequirementRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(learningObjectiveRequirementRecordType + " is not supported");
    }


    /**
     *  Adds a record to this learning objective requirement. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param learningObjectiveRequirementRecord the learning
     *         objective requirement record
     *  @param learningObjectiveRequirementRecordType learning
     *         objective requirement record type
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectiveRequirementRecord</code> or
     *          <code>learningObjectiveRequirementRecordTypelearningObjectiveRequirement</code>
     *          is <code>null</code>
     */
            
    protected void addLearningObjectiveRequirementRecord(org.osid.course.requisite.records.LearningObjectiveRequirementRecord learningObjectiveRequirementRecord, 
                                                         org.osid.type.Type learningObjectiveRequirementRecordType) {

        nullarg(learningObjectiveRequirementRecord, "learning objective requirement record");
        addRecordType(learningObjectiveRequirementRecordType);
        this.records.add(learningObjectiveRequirementRecord);
        
        return;
    }
}
