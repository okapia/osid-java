//
// AbstractRouteQuery.java
//
//     A template for making a Route Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.route.route.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for routes.
 */

public abstract class AbstractRouteQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.mapping.route.RouteQuery {

    private final java.util.Collection<org.osid.mapping.route.records.RouteQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the location <code> Id </code> for this query to match routes 
     *  with a starting location. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStartingLocationId(org.osid.id.Id locationId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the starting location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStartingLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for a starting 
     *  location. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStartingLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a starting location. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStartingLocationQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getStartingLocationQuery() {
        throw new org.osid.UnimplementedException("supportsStartingLocationQuery() is false");
    }


    /**
     *  Clears the starting location query terms. 
     */

    @OSID @Override
    public void clearStartingLocationTerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query to match routes 
     *  with an ending location. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEndingLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the ending location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearEndingLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available for an ending 
     *  location. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEndingLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ending location. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEndingLocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getEndingLocationQuery() {
        throw new org.osid.UnimplementedException("supportsEndingLocationQuery() is false");
    }


    /**
     *  Clears the ending location query terms. 
     */

    @OSID @Override
    public void clearEndingLocationTerms() {
        return;
    }


    /**
     *  Sets the location <code> Ids </code> for this query to match routes 
     *  along all the given locations. 
     *
     *  @param  locationIds the location <code> Ids </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationIds </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAlongLocationIds(org.osid.id.Id[] locationIds, boolean match) {
        return;
    }


    /**
     *  Clears the along location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAlongLocationIdsTerms() {
        return;
    }


    /**
     *  Matches routes that have distances within the specified range 
     *  inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     */

    @OSID @Override
    public void matchDistance(org.osid.mapping.Distance from, 
                              org.osid.mapping.Distance to, boolean match) {
        return;
    }


    /**
     *  Matches routes that has any distance assigned or calculated.. 
     *
     *  @param  match <code> true </code> to match routes with any distance, 
     *          <code> false </code> to match routes with no distance 
     */

    @OSID @Override
    public void matchAnyDistance(boolean match) {
        return;
    }


    /**
     *  Clears the distance query terms. 
     */

    @OSID @Override
    public void clearDistanceTerms() {
        return;
    }


    /**
     *  Matches routes that have the specified estimated travel time 
     *  inclusive. 
     *
     *  @param  from starting range 
     *  @param  to ending range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> to </code> is less 
     *          than <code> from </code> 
     *  @throws org.osid.NullArgumentException <code> from </code> or <code> 
     *          to </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchETA(org.osid.calendaring.Duration from, 
                         org.osid.calendaring.Duration to, boolean match) {
        return;
    }


    /**
     *  Matches routes that has any estimated time assigned or calculated. 
     *
     *  @param  match <code> true </code> to match routes with any eta, <code> 
     *          false </code> to match routes with no eta 
     */

    @OSID @Override
    public void matchAnyETA(boolean match) {
        return;
    }


    /**
     *  Clears the ETA query terms. 
     */

    @OSID @Override
    public void clearETATerms() {
        return;
    }


    /**
     *  Sets the location <code> Id </code> for this query to match routes 
     *  through a location. 
     *
     *  @param  locationId the location <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchLocationId(org.osid.id.Id locationId, boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> supportsLocationQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getLocationQuery() {
        throw new org.osid.UnimplementedException("supportsLocationQuery() is false");
    }


    /**
     *  Matches routes that has any location along the route, exclusive of the 
     *  endpoints. 
     *
     *  @param  match <code> true </code> to match routes with any location, 
     *          <code> false </code> to match routes with no location 
     */

    @OSID @Override
    public void matchAnyLocation(boolean match) {
        return;
    }


    /**
     *  Clears the location query terms. 
     */

    @OSID @Override
    public void clearLocationTerms() {
        return;
    }


    /**
     *  Sets the path <code> Id </code> for this query to match routes using a 
     *  designated path. 
     *
     *  @param  pathId the path <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pathId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchPathId(org.osid.id.Id pathId, boolean match) {
        return;
    }


    /**
     *  Clears the path <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPathIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PathQuery </code> is available. 
     *
     *  @return <code> true </code> if a path query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Gets the query for a path. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the path query 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.PathQuery getPathQuery() {
        throw new org.osid.UnimplementedException("supportsPathQuery() is false");
    }


    /**
     *  Matches routes using any designated path. 
     *
     *  @param  match <code> true </code> to match routes with any path <code> 
     *          false </code> to match routes with no path 
     */

    @OSID @Override
    public void matchAnyPath(boolean match) {
        return;
    }


    /**
     *  Clears the path query terms. 
     */

    @OSID @Override
    public void clearPathTerms() {
        return;
    }


    /**
     *  Sets the map <code> Id </code> for this query to match routes assigned 
     *  to maps. 
     *
     *  @param  mapId the map <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> mapId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMapId(org.osid.id.Id mapId, boolean match) {
        return;
    }


    /**
     *  Clears the map <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearMapIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> MapQuery </code> is available. 
     *
     *  @return <code> true </code> if a map query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMapQuery() {
        return (false);
    }


    /**
     *  Gets the query for a map. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the map query 
     *  @throws org.osid.UnimplementedException <code> supportsMapQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.MapQuery getMapQuery() {
        throw new org.osid.UnimplementedException("supportsMapQuery() is false");
    }


    /**
     *  Clears the map query terms. 
     */

    @OSID @Override
    public void clearMapTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given route query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a route implementing the requested record.
     *
     *  @param routeRecordType a route record type
     *  @return the route query record
     *  @throws org.osid.NullArgumentException
     *          <code>routeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(routeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.route.records.RouteQueryRecord getRouteQueryRecord(org.osid.type.Type routeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.route.records.RouteQueryRecord record : this.records) {
            if (record.implementsRecordType(routeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(routeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this route query. 
     *
     *  @param routeQueryRecord route query record
     *  @param routeRecordType route record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRouteQueryRecord(org.osid.mapping.route.records.RouteQueryRecord routeQueryRecord, 
                                          org.osid.type.Type routeRecordType) {

        addRecordType(routeRecordType);
        nullarg(routeQueryRecord, "route query record");
        this.records.add(routeQueryRecord);        
        return;
    }
}
