//
// AbstractFederatingActionEnablerLookupSession.java
//
//     An abstract federating adapter for an ActionEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.control.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  ActionEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingActionEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.control.rules.ActionEnablerLookupSession>
    implements org.osid.control.rules.ActionEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.control.System system = new net.okapia.osid.jamocha.nil.control.system.UnknownSystem();


    /**
     *  Constructs a new <code>AbstractFederatingActionEnablerLookupSession</code>.
     */

    protected AbstractFederatingActionEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.control.rules.ActionEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>System/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>System Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getSystemId() {
        return (this.system.getId());
    }


    /**
     *  Gets the <code>System</code> associated with this 
     *  session.
     *
     *  @return the <code>System</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.System getSystem()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.system);
    }


    /**
     *  Sets the <code>System</code>.
     *
     *  @param  system the system for this session
     *  @throws org.osid.NullArgumentException <code>system</code>
     *          is <code>null</code>
     */

    protected void setSystem(org.osid.control.System system) {
        nullarg(system, "system");
        this.system = system;
        return;
    }


    /**
     *  Tests if this user can perform <code>ActionEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActionEnablers() {
        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            if (session.canLookupActionEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>ActionEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActionEnablerView() {
        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            session.useComparativeActionEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>ActionEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActionEnablerView() {
        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            session.usePlenaryActionEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include action enablers in systems which are children
     *  of this system in the system hierarchy.
     */

    @OSID @Override
    public void useFederatedSystemView() {
        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            session.useFederatedSystemView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this system only.
     */

    @OSID @Override
    public void useIsolatedSystemView() {
        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            session.useIsolatedSystemView();
        }

        return;
    }


    /**
     *  Only active action enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveActionEnablerView() {
        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            session.useActiveActionEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive action enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusActionEnablerView() {
        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            session.useAnyStatusActionEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>ActionEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActionEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ActionEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param  actionEnablerId <code>Id</code> of the
     *          <code>ActionEnabler</code>
     *  @return the action enabler
     *  @throws org.osid.NotFoundException <code>actionEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>actionEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnabler getActionEnabler(org.osid.id.Id actionEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            try {
                return (session.getActionEnabler(actionEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(actionEnablerId + " not found");
    }


    /**
     *  Gets an <code>ActionEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  actionEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ActionEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getActionEnablers()</code>.
     *
     *  @param  actionEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActionEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByIds(org.osid.id.IdList actionEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.control.rules.actionenabler.MutableActionEnablerList ret = new net.okapia.osid.jamocha.control.rules.actionenabler.MutableActionEnablerList();

        try (org.osid.id.IdList ids = actionEnablerIds) {
            while (ids.hasNext()) {
                ret.addActionEnabler(getActionEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>ActionEnablerList</code> corresponding to the given
     *  action enabler genus <code>Type</code> which does not include
     *  action enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getActionEnablers()</code>.
     *
     *  @param  actionEnablerGenusType an actionEnabler genus type 
     *  @return the returned <code>ActionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByGenusType(org.osid.type.Type actionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.actionenabler.FederatingActionEnablerList ret = getActionEnablerList();

        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            ret.addActionEnablerList(session.getActionEnablersByGenusType(actionEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ActionEnablerList</code> corresponding to the given
     *  action enabler genus <code>Type</code> and include any additional
     *  action enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActionEnablers()</code>.
     *
     *  @param  actionEnablerGenusType an actionEnabler genus type 
     *  @return the returned <code>ActionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByParentGenusType(org.osid.type.Type actionEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.actionenabler.FederatingActionEnablerList ret = getActionEnablerList();

        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            ret.addActionEnablerList(session.getActionEnablersByParentGenusType(actionEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ActionEnablerList</code> containing the given
     *  action enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActionEnablers()</code>.
     *
     *  @param  actionEnablerRecordType an actionEnabler record type 
     *  @return the returned <code>ActionEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersByRecordType(org.osid.type.Type actionEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.actionenabler.FederatingActionEnablerList ret = getActionEnablerList();

        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            ret.addActionEnablerList(session.getActionEnablersByRecordType(actionEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ActionEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible
     *  through this session.
     *  
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>ActionEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.actionenabler.FederatingActionEnablerList ret = getActionEnablerList();

        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            ret.addActionEnablerList(session.getActionEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>ActionEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible
     *  through this session.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>ActionEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                     org.osid.calendaring.DateTime from,
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.actionenabler.FederatingActionEnablerList ret = getActionEnablerList();

        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            ret.addActionEnablerList(session.getActionEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>ActionEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  action enablers or an error results. Otherwise, the returned list
     *  may contain only those action enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, action enablers are returned that are currently
     *  active. In any status mode, active and inactive action enablers
     *  are returned.
     *
     *  @return a list of <code>ActionEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.rules.ActionEnablerList getActionEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.control.rules.actionenabler.FederatingActionEnablerList ret = getActionEnablerList();

        for (org.osid.control.rules.ActionEnablerLookupSession session : getSessions()) {
            ret.addActionEnablerList(session.getActionEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.control.rules.actionenabler.FederatingActionEnablerList getActionEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.control.rules.actionenabler.ParallelActionEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.control.rules.actionenabler.CompositeActionEnablerList());
        }
    }
}
