//
// AbstractAssemblyAgencyQuery.java
//
//     An AgencyQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.authentication.agency.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AgencyQuery that stores terms.
 */

public abstract class AbstractAssemblyAgencyQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.authentication.AgencyQuery,
               org.osid.authentication.AgencyQueryInspector,
               org.osid.authentication.AgencySearchOrder {

    private final java.util.Collection<org.osid.authentication.records.AgencyQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authentication.records.AgencyQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.authentication.records.AgencySearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAgencyQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAgencyQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the agent <code> Id </code> for this query. 
     *
     *  @param  agentId an agent <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agentId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAgentId(org.osid.id.Id agentId, boolean match) {
        getAssembler().addIdTerm(getAgentIdColumn(), agentId, match);
        return;
    }


    /**
     *  Clears the agent <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAgentIdTerms() {
        getAssembler().clearTerms(getAgentIdColumn());
        return;
    }


    /**
     *  Gets the agent <code> Id </code> terms. 
     *
     *  @return the agent <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAgentIdTerms() {
        return (getAssembler().getIdTerms(getAgentIdColumn()));
    }


    /**
     *  Gets the AgentId column name.
     *
     * @return the column name
     */

    protected String getAgentIdColumn() {
        return ("agent_id");
    }


    /**
     *  Tests if an <code> AgentQuery </code> is available. 
     *
     *  @return <code> true </code> if an agent query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agent. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agent query 
     *  @throws org.osid.UnimplementedException <code> supportsAgentQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAgentQuery() {
        throw new org.osid.UnimplementedException("supportsAgentQuery() is false");
    }


    /**
     *  Matches agencies with any agent. 
     *
     *  @param  match <code> true </code> to match agencies with any agent. 
     *          <code> false </code> to match agencies with no agents 
     */

    @OSID @Override
    public void matchAnyAgent(boolean match) {
        getAssembler().addIdWildcardTerm(getAgentColumn(), match);
        return;
    }


    /**
     *  Clears the agent terms. 
     */

    @OSID @Override
    public void clearAgentTerms() {
        getAssembler().clearTerms(getAgentColumn());
        return;
    }


    /**
     *  Gets the agent terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.authentication.AgentQueryInspector[] getAgentTerms() {
        return (new org.osid.authentication.AgentQueryInspector[0]);
    }


    /**
     *  Gets the Agent column name.
     *
     * @return the column name
     */

    protected String getAgentColumn() {
        return ("agent");
    }


    /**
     *  Sets the agency <code> Id </code> for this query to match agencies 
     *  that have the specified agency as an ancestor. 
     *
     *  @param  agencyId an agency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorAgencyId(org.osid.id.Id agencyId, boolean match) {
        getAssembler().addIdTerm(getAncestorAgencyIdColumn(), agencyId, match);
        return;
    }


    /**
     *  Clears the ancestor agency <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorAgencyIdTerms() {
        getAssembler().clearTerms(getAncestorAgencyIdColumn());
        return;
    }


    /**
     *  Gets the ancestor agency <code> Id </code> terms. 
     *
     *  @return the ancestor agency <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorAgencyIdTerms() {
        return (getAssembler().getIdTerms(getAncestorAgencyIdColumn()));
    }


    /**
     *  Gets the AncestorAgencyId column name.
     *
     * @return the column name
     */

    protected String getAncestorAgencyIdColumn() {
        return ("ancestor_agency_id");
    }


    /**
     *  Tests if an <code> AgencyQuery </code> is available. 
     *
     *  @return <code> true </code> if an agency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorAgencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorAgencyQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getAncestorAgencyQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorAgencyQuery() is false");
    }


    /**
     *  Matches agencies with any ancestor. 
     *
     *  @param  match <code> true </code> to match agencies with any ancestor, 
     *          <code> false </code> to match root agencies 
     */

    @OSID @Override
    public void matchAnyAncestorAgency(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorAgencyColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor agency terms. 
     */

    @OSID @Override
    public void clearAncestorAgencyTerms() {
        getAssembler().clearTerms(getAncestorAgencyColumn());
        return;
    }


    /**
     *  Gets the ancestor agency terms. 
     *
     *  @return the ancestor agency terms 
     */

    @OSID @Override
    public org.osid.authentication.AgencyQueryInspector[] getAncestorAgencyTerms() {
        return (new org.osid.authentication.AgencyQueryInspector[0]);
    }


    /**
     *  Gets the AncestorAgency column name.
     *
     * @return the column name
     */

    protected String getAncestorAgencyColumn() {
        return ("ancestor_agency");
    }


    /**
     *  Sets the agency <code> Id </code> for this query to match agencies 
     *  that have the specified agency as an descendant. 
     *
     *  @param  agencyId an agency <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for negative match 
     *  @throws org.osid.NullArgumentException <code> agencyId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantAgencyId(org.osid.id.Id agencyId, boolean match) {
        getAssembler().addIdTerm(getDescendantAgencyIdColumn(), agencyId, match);
        return;
    }


    /**
     *  Clears the descendant agency <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantAgencyIdTerms() {
        getAssembler().clearTerms(getDescendantAgencyIdColumn());
        return;
    }


    /**
     *  Gets the descendant agency <code> Id </code> terms. 
     *
     *  @return the descendant agency <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantAgencyIdTerms() {
        return (getAssembler().getIdTerms(getDescendantAgencyIdColumn()));
    }


    /**
     *  Gets the DescendantAgencyId column name.
     *
     * @return the column name
     */

    protected String getDescendantAgencyIdColumn() {
        return ("descendant_agency_id");
    }


    /**
     *  Tests if an <code> AgencyQuery </code> is available. 
     *
     *  @return <code> true </code> if an agency query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantAgencyQuery() {
        return (false);
    }


    /**
     *  Gets the query for an agency. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the agency query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantAgencyQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentQuery getDescendantAgencyQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantAgencyQuery() is false");
    }


    /**
     *  Matches agencies with any descendant. 
     *
     *  @param  match <code> true </code> to match agencies with any 
     *          descendant, <code> false </code> to match leaf agencies 
     */

    @OSID @Override
    public void matchAnyDescendantAgency(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantAgencyColumn(), match);
        return;
    }


    /**
     *  Clears the descendant agency terms. 
     */

    @OSID @Override
    public void clearDescendantAgencyTerms() {
        getAssembler().clearTerms(getDescendantAgencyColumn());
        return;
    }


    /**
     *  Gets the descendant agency terms. 
     *
     *  @return the descendant agency terms 
     */

    @OSID @Override
    public org.osid.authentication.AgencyQueryInspector[] getDescendantAgencyTerms() {
        return (new org.osid.authentication.AgencyQueryInspector[0]);
    }


    /**
     *  Gets the DescendantAgency column name.
     *
     * @return the column name
     */

    protected String getDescendantAgencyColumn() {
        return ("descendant_agency");
    }


    /**
     *  Tests if this agency supports the given record
     *  <code>Type</code>.
     *
     *  @param  agencyRecordType an agency record type 
     *  @return <code>true</code> if the agencyRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>agencyRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type agencyRecordType) {
        for (org.osid.authentication.records.AgencyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(agencyRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  agencyRecordType the agency record type 
     *  @return the agency query record 
     *  @throws org.osid.NullArgumentException
     *          <code>agencyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgencyQueryRecord getAgencyQueryRecord(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgencyQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(agencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agencyRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  agencyRecordType the agency record type 
     *  @return the agency query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>agencyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgencyQueryInspectorRecord getAgencyQueryInspectorRecord(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgencyQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(agencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agencyRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param agencyRecordType the agency record type
     *  @return the agency search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>agencyRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(agencyRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.authentication.records.AgencySearchOrderRecord getAgencySearchOrderRecord(org.osid.type.Type agencyRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authentication.records.AgencySearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(agencyRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(agencyRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this agency. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param agencyQueryRecord the agency query record
     *  @param agencyQueryInspectorRecord the agency query inspector
     *         record
     *  @param agencySearchOrderRecord the agency search order record
     *  @param agencyRecordType agency record type
     *  @throws org.osid.NullArgumentException
     *          <code>agencyQueryRecord</code>,
     *          <code>agencyQueryInspectorRecord</code>,
     *          <code>agencySearchOrderRecord</code> or
     *          <code>agencyRecordTypeagency</code> is
     *          <code>null</code>
     */
            
    protected void addAgencyRecords(org.osid.authentication.records.AgencyQueryRecord agencyQueryRecord, 
                                      org.osid.authentication.records.AgencyQueryInspectorRecord agencyQueryInspectorRecord, 
                                      org.osid.authentication.records.AgencySearchOrderRecord agencySearchOrderRecord, 
                                      org.osid.type.Type agencyRecordType) {

        addRecordType(agencyRecordType);

        nullarg(agencyQueryRecord, "agency query record");
        nullarg(agencyQueryInspectorRecord, "agency query inspector record");
        nullarg(agencySearchOrderRecord, "agency search odrer record");

        this.queryRecords.add(agencyQueryRecord);
        this.queryInspectorRecords.add(agencyQueryInspectorRecord);
        this.searchOrderRecords.add(agencySearchOrderRecord);
        
        return;
    }
}
