//
// TypeMetadata.java
//
//     Defines a type Metadata.
//
//
// Tom Coppeto
// Okapia
// 11 January 2023
//
//
// Copyright (c) 2023 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata;


/**
 *  Defines a type Metadata.
 */

public final class TypeMetadata
    extends net.okapia.osid.jamocha.metadata.spi.AbstractTypeMetadata
    implements org.osid.Metadata {


    /**
     *  Constructs a new single unlinked {@code TypeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public TypeMetadata(org.osid.id.Id elementId) {
        super(elementId);
        return;
    }


    /**
     *  Constructs a new unlinked {@code TypeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public TypeMetadata(org.osid.id.Id elementId, boolean isArray) {
        super(elementId, isArray, false);
        return;
    }


    /**
     *  Constructs a new {@code TypeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    public TypeMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(elementId, isArray, isLinked);
        return;
    }

    
    /**
     *  Sets the element label.
     *
     *  @param label the new element label
     *  @throws org.osid.NullArgumentException {@code label} is {@code
     *          null}
     */

    public void setLabel(org.osid.locale.DisplayText label) {
        super.setLabel(label);
        return;
    }


    /**
     *  Sets the instructions.
     *
     *  @param instructions the new instructions
     *  @throws org.osid.NullArgumentException {@code instructions}
     *          is {@code null}
     */

    public void setInstructions(org.osid.locale.DisplayText instructions) {
        super.setInstructions(instructions);
        return;
    }

    
    /**
     *  Sets the required flag.
     *
     *  @param required {@code true} if required, {@code false} if
     *         optional
     */

    public void setRequired(boolean required) {
        super.setRequired(required);
        return;
    }

    
    /**
     *  Sets the has value flag.
     *
     *  @param exists {@code true} if has existing value, {@code
     *         false} if no value exists
     */

    public void setValueExists(boolean exists) {
        super.setValueExists(exists);
        return;
    }


    /**
     *  Sets the read only flag.
     *
     *  @param readonly {@code true} if read only, {@code false} if
     *         can be updated
     */

    public void setReadOnly(boolean readonly) {
        super.setReadOnly(readonly);
        return;
    }


    /**
     *  Sets the units.
     *
     *	@param units the new units
     *  @throws org.osid.NullArgumentException {@code units}
     *          is {@code null}
     */

    public void setUnits(org.osid.locale.DisplayText units) {
        super.setUnits(units);
        return;
    }


    /**
     *  Sets the type set.
     *
     *  @param values a collection of accepted type values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setTypeSet(java.util.Collection<org.osid.type.Type> values) {
        super.setTypeSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the type set.
     *
     *  @param values a collection of accepted type values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addToTypeSet(java.util.Collection<org.osid.type.Type> values) {
        super.addToTypeSet(values);
        return;
    }


    /**
     *  Adds a value to the type set.
     *
     *  @param value a type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addToTypeSet(org.osid.type.Type value) {
        super.addToTypeSet(value);
        return;
    }


    /**
     *  Removes a value from the type set.
     *
     *  @param value a type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeFromTypeSet(org.osid.type.Type value) {
        super.removeFromTypeSet(value);
        return;
    }


    /**
     *  Clears the type set.
     */

    public void clearTypeSet() {
        super.clearTypeSet();
        return;
    }


    /**
     *  Sets the default Type set.
     *
     *  @param values a collection of default Type values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setDefaultTypeValues(java.util.Collection<org.osid.type.Type> values) {
        super.setDefaultTypeValues(values);
        return;
    }


    /**
     *  Adds a collection of default Type values.
     *
     *  @param values a collection of default Type values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addDefaultTypeValues(java.util.Collection<org.osid.type.Type> values) {
        super.addDefaultTypeValues(values);
        return;
    }


    /**
     *  Adds a default Type value.
     *
     *  @param value an Type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addDefaultTypeValue(org.osid.type.Type value) {
        super.addDefaultTypeValue(value);
        return;
    }


    /**
     *  Removes a default Type value.
     *
     *  @param value an Type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeDefaultTypeValue(org.osid.type.Type value) {
        super.removeDefaultTypeValue(value);
        return;
    }


    /**
     *  Clears the default Type values.
     */

    public void clearDefaultTypeValues() {
        super.clearDefaultTypeValues();
        return;
    }


    /**
     *  Sets the existing Type set.
     *
     *  @param values a collection of existing Type values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    public void setExistingTypeValues(java.util.Collection<org.osid.type.Type> values) {
        super.setExistingTypeValues(values);
        return;
    }


    /**
     *  Adds a collection of existing Type values.
     *
     *  @param values a collection of existing Type values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    public void addExistingTypeValues(java.util.Collection<org.osid.type.Type> values) {
        super.addExistingTypeValues(values);
        return;
    }


    /**
     *  Adds a existing Type value.
     *
     *  @param value an Type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void addExistingTypeValue(org.osid.type.Type value) {
        super.addExistingTypeValue(value);
        return;
    }


    /**
     *  Removes a existing Type value.
     *
     *  @param value an Type value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    public void removeExistingTypeValue(org.osid.type.Type value) {
        super.removeExistingTypeValue(value);
        return;
    }


    /**
     *  Clears the existing Type values.
     */

    public void clearExistingTypeValues() {
        super.clearExistingTypeValues();
        return;
    }            
}
