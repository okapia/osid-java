//
// AbstractFederatingRaceProcessorEnablerLookupSession.java
//
//     An abstract federating adapter for a RaceProcessorEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.voting.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  RaceProcessorEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingRaceProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.voting.rules.RaceProcessorEnablerLookupSession>
    implements org.osid.voting.rules.RaceProcessorEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.voting.Polls polls = new net.okapia.osid.jamocha.nil.voting.polls.UnknownPolls();


    /**
     *  Constructs a new <code>AbstractFederatingRaceProcessorEnablerLookupSession</code>.
     */

    protected AbstractFederatingRaceProcessorEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.voting.rules.RaceProcessorEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Polls/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Polls Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getPollsId() {
        return (this.polls.getId());
    }


    /**
     *  Gets the <code>Polls</code> associated with this 
     *  session.
     *
     *  @return the <code>Polls</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.Polls getPolls()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.polls);
    }


    /**
     *  Sets the <code>Polls</code>.
     *
     *  @param  polls the polls for this session
     *  @throws org.osid.NullArgumentException <code>polls</code>
     *          is <code>null</code>
     */

    protected void setPolls(org.osid.voting.Polls polls) {
        nullarg(polls, "polls");
        this.polls = polls;
        return;
    }


    /**
     *  Tests if this user can perform <code>RaceProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupRaceProcessorEnablers() {
        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            if (session.canLookupRaceProcessorEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>RaceProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeRaceProcessorEnablerView() {
        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            session.useComparativeRaceProcessorEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>RaceProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryRaceProcessorEnablerView() {
        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            session.usePlenaryRaceProcessorEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include race processor enablers in pollses which are children
     *  of this polls in the polls hierarchy.
     */

    @OSID @Override
    public void useFederatedPollsView() {
        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            session.useFederatedPollsView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this polls only.
     */

    @OSID @Override
    public void useIsolatedPollsView() {
        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            session.useIsolatedPollsView();
        }

        return;
    }


    /**
     *  Only active race processor enablers are returned by methods in
     *  this session.
     */
     
    @OSID @Override
    public void useActiveRaceProcessorEnablerView() {
        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            session.useActiveRaceProcessorEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive race processor enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusRaceProcessorEnablerView() {
        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            session.useAnyStatusRaceProcessorEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>RaceProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>RaceProcessorEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>RaceProcessorEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  raceProcessorEnablerId <code>Id</code> of the
     *          <code>RaceProcessorEnabler</code>
     *  @return the race processor enabler
     *  @throws org.osid.NotFoundException <code>raceProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>raceProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnabler getRaceProcessorEnabler(org.osid.id.Id raceProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            try {
                return (session.getRaceProcessorEnabler(raceProcessorEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(raceProcessorEnablerId + " not found");
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> corresponding to
     *  the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  raceProcessorEnablers specified in the <code>Id</code> list,
     *  in the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>RaceProcessorEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  raceProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>RaceProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByIds(org.osid.id.IdList raceProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.voting.rules.raceprocessorenabler.MutableRaceProcessorEnablerList ret = new net.okapia.osid.jamocha.voting.rules.raceprocessorenabler.MutableRaceProcessorEnablerList();

        try (org.osid.id.IdList ids = raceProcessorEnablerIds) {
            while (ids.hasNext()) {
                ret.addRaceProcessorEnabler(getRaceProcessorEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> corresponding to
     *  the given race processor enabler genus <code>Type</code> which
     *  does not include race processor enablers of types derived from
     *  the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  raceProcessorEnablerGenusType a raceProcessorEnabler genus type 
     *  @return the returned <code>RaceProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByGenusType(org.osid.type.Type raceProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessorenabler.FederatingRaceProcessorEnablerList ret = getRaceProcessorEnablerList();

        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            ret.addRaceProcessorEnablerList(session.getRaceProcessorEnablersByGenusType(raceProcessorEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> corresponding to
     *  the given race processor enabler genus <code>Type</code> and
     *  include any additional race processor enablers with genus
     *  types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  raceProcessorEnablerGenusType a raceProcessorEnabler genus type 
     *  @return the returned <code>RaceProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByParentGenusType(org.osid.type.Type raceProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessorenabler.FederatingRaceProcessorEnablerList ret = getRaceProcessorEnablerList();

        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            ret.addRaceProcessorEnablerList(session.getRaceProcessorEnablersByParentGenusType(raceProcessorEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> containing the
     *  given race processor enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  raceProcessorEnablerRecordType a raceProcessorEnabler record type 
     *  @return the returned <code>RaceProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>raceProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersByRecordType(org.osid.type.Type raceProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessorenabler.FederatingRaceProcessorEnablerList ret = getRaceProcessorEnablerList();

        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            ret.addRaceProcessorEnablerList(session.getRaceProcessorEnablersByRecordType(raceProcessorEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>RaceProcessorEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session.
     *  
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>RaceProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessorenabler.FederatingRaceProcessorEnablerList ret = getRaceProcessorEnablerList();

        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            ret.addRaceProcessorEnablerList(session.getRaceProcessorEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets a <code>RaceProcessorEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>RaceProcessorEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessorenabler.FederatingRaceProcessorEnablerList ret = getRaceProcessorEnablerList();

        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            ret.addRaceProcessorEnablerList(session.getRaceProcessorEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>RaceProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known race
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those race processor enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, race processor enablers are returned that are
     *  currently active. In any status mode, active and inactive race
     *  processor enablers are returned.
     *
     *  @return a list of <code>RaceProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceProcessorEnablerList getRaceProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessorenabler.FederatingRaceProcessorEnablerList ret = getRaceProcessorEnablerList();

        for (org.osid.voting.rules.RaceProcessorEnablerLookupSession session : getSessions()) {
            ret.addRaceProcessorEnablerList(session.getRaceProcessorEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessorenabler.FederatingRaceProcessorEnablerList getRaceProcessorEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessorenabler.ParallelRaceProcessorEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.voting.rules.raceprocessorenabler.CompositeRaceProcessorEnablerList());
        }
    }
}
