//
// AbstractOffsetEventEnablerSearch.java
//
//     A template for making an OffsetEventEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.rules.offseteventenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing offset event enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractOffsetEventEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.calendaring.rules.OffsetEventEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.calendaring.rules.records.OffsetEventEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.calendaring.rules.OffsetEventEnablerSearchOrder offsetEventEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of offset event enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  offsetEventEnablerIds list of offset event enablers
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongOffsetEventEnablers(org.osid.id.IdList offsetEventEnablerIds) {
        while (offsetEventEnablerIds.hasNext()) {
            try {
                this.ids.add(offsetEventEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongOffsetEventEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of offset event enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getOffsetEventEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  offsetEventEnablerSearchOrder offset event enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>offsetEventEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderOffsetEventEnablerResults(org.osid.calendaring.rules.OffsetEventEnablerSearchOrder offsetEventEnablerSearchOrder) {
	this.offsetEventEnablerSearchOrder = offsetEventEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.calendaring.rules.OffsetEventEnablerSearchOrder getOffsetEventEnablerSearchOrder() {
	return (this.offsetEventEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given offset event enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an offset event enabler implementing the requested record.
     *
     *  @param offsetEventEnablerSearchRecordType an offset event enabler search record
     *         type
     *  @return the offset event enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>offsetEventEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(offsetEventEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.rules.records.OffsetEventEnablerSearchRecord getOffsetEventEnablerSearchRecord(org.osid.type.Type offsetEventEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.calendaring.rules.records.OffsetEventEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(offsetEventEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(offsetEventEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this offset event enabler search. 
     *
     *  @param offsetEventEnablerSearchRecord offset event enabler search record
     *  @param offsetEventEnablerSearchRecordType offsetEventEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addOffsetEventEnablerSearchRecord(org.osid.calendaring.rules.records.OffsetEventEnablerSearchRecord offsetEventEnablerSearchRecord, 
                                           org.osid.type.Type offsetEventEnablerSearchRecordType) {

        addRecordType(offsetEventEnablerSearchRecordType);
        this.records.add(offsetEventEnablerSearchRecord);        
        return;
    }
}
