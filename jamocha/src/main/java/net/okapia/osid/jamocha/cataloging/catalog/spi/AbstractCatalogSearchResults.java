//
// AbstractCatalogSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.catalog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractCatalogSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.cataloging.CatalogSearchResults {

    private org.osid.cataloging.CatalogList catalogs;
    private final org.osid.cataloging.CatalogQueryInspector inspector;
    private final java.util.Collection<org.osid.cataloging.records.CatalogSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractCatalogSearchResults.
     *
     *  @param catalogs the result set
     *  @param catalogQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>catalogs</code>
     *          or <code>catalogQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractCatalogSearchResults(org.osid.cataloging.CatalogList catalogs,
                                           org.osid.cataloging.CatalogQueryInspector catalogQueryInspector) {
        nullarg(catalogs, "catalogs");
        nullarg(catalogQueryInspector, "catalog query inspectpr");

        this.catalogs = catalogs;
        this.inspector = catalogQueryInspector;

        return;
    }


    /**
     *  Gets the catalog list resulting from a search.
     *
     *  @return a catalog list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogs() {
        if (this.catalogs == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.cataloging.CatalogList catalogs = this.catalogs;
        this.catalogs = null;
	return (catalogs);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.cataloging.CatalogQueryInspector getCatalogQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  catalog search record <code> Type. </code> This method must
     *  be used to retrieve a catalog implementing the requested
     *  record.
     *
     *  @param catalogSearchRecordType a catalog search 
     *         record type 
     *  @return the catalog search
     *  @throws org.osid.NullArgumentException
     *          <code>catalogSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(catalogSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.records.CatalogSearchResultsRecord getCatalogSearchResultsRecord(org.osid.type.Type catalogSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.cataloging.records.CatalogSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(catalogSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(catalogSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record catalog search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addCatalogRecord(org.osid.cataloging.records.CatalogSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "catalog record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
