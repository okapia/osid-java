//
// AbstractFilingProxyManager.java
//
//     An adapter for a FilingProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.filing.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a FilingProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterFilingProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.filing.FilingProxyManager>
    implements org.osid.filing.FilingProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterFilingProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterFilingProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterFilingProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterFilingProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if directory federation is exposed. Federation is exposed when a 
     *  specific directory may be identified, selected and used to access a 
     *  session. Federation is not exposed when a set of directories appears 
     *  as a single directory. 
     *
     *  @return <code> true </code> if federation is visible <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if a <code> FileSystemSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> FileSystemSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSystem() {
        return (getAdapteeManager().supportsFileSystem());
    }


    /**
     *  Tests if a <code> FileSystemManagementSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> FileSystemManagementSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSystemManagement() {
        return (getAdapteeManager().supportsFileSystemManagement());
    }


    /**
     *  Tests if a <code> FileContentSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> FileContentSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileContent() {
        return (getAdapteeManager().supportsFileContent());
    }


    /**
     *  Tests if file lookup is supported. 
     *
     *  @return <code> true </code> if a <code> FileLookupSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileLookup() {
        return (getAdapteeManager().supportsFileLookup());
    }


    /**
     *  Tests if file querying is supported. 
     *
     *  @return <code> true </code> if a <code> FileQuerySession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileQuery() {
        return (getAdapteeManager().supportsFileQuery());
    }


    /**
     *  Tests if file searching is supported. 
     *
     *  @return <code> true </code> if a <code> FileSearchSession </code> is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSearch() {
        return (getAdapteeManager().supportsFileSearch());
    }


    /**
     *  Tests if file notification is supported. 
     *
     *  @return <code> true </code> if a <code> FileNotificationSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileNotification() {
        return (getAdapteeManager().supportsFileNotification());
    }


    /**
     *  Tests if managing smart directories is supported. 
     *
     *  @return <code> true </code> if a <code> FileSmartDirectorySession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFileSmartDirectory() {
        return (getAdapteeManager().supportsFileSmartDirectory());
    }


    /**
     *  Tests if a <code> DirectoryLookupSession </code> is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryLookupSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryLookup() {
        return (getAdapteeManager().supportsDirectoryLookup());
    }


    /**
     *  Tests if directory querying is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryQuerySession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryQuery() {
        return (getAdapteeManager().supportsDirectoryQuery());
    }


    /**
     *  Tests if directory searching is supported. 
     *
     *  @return <code> true </code> if a <code> DirectorySearchSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectorySearch() {
        return (getAdapteeManager().supportsDirectorySearch());
    }


    /**
     *  Tests if directory administration is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryAdminSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryAdmin() {
        return (getAdapteeManager().supportsDirectoryAdmin());
    }


    /**
     *  Tests if a directory <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if a <code> DirectoryNotificationSession 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectoryNotification() {
        return (getAdapteeManager().supportsDirectoryNotification());
    }


    /**
     *  Tests if a file management service is supported. 
     *
     *  @return <code> true </code> if a <code> FileManagementSession </code> 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingManagement() {
        return (getAdapteeManager().supportsFilingManagement());
    }


    /**
     *  Tests if a filing allocation service is supported. 
     *
     *  @return <code> true </code> if a <code> FilingAllocationManager 
     *          </code> is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFilingAllocation() {
        return (getAdapteeManager().supportsFilingAllocation());
    }


    /**
     *  Gets the supported file record types. 
     *
     *  @return a list containing the supported <code> File </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFileRecordTypes() {
        return (getAdapteeManager().getFileRecordTypes());
    }


    /**
     *  Tests if the given file record type is supported. 
     *
     *  @param  fileRecordType a <code> Type </code> indicating a file record 
     *          type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> fileRecordType </code> 
     *          is null 
     */

    @OSID @Override
    public boolean supportsFileRecordType(org.osid.type.Type fileRecordType) {
        return (getAdapteeManager().supportsFileRecordType(fileRecordType));
    }


    /**
     *  Gets the supported file search record types. 
     *
     *  @return a list containing the supported <code> File </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getFileSearchRecordTypes() {
        return (getAdapteeManager().getFileSearchRecordTypes());
    }


    /**
     *  Tests if the given file search record type is supported. 
     *
     *  @param  fileSearchRecordType a <code> Type </code> indicating a file 
     *          search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> fileSearchRecordType 
     *          </code> is null 
     */

    @OSID @Override
    public boolean supportsFileSearchRecordType(org.osid.type.Type fileSearchRecordType) {
        return (getAdapteeManager().supportsFileSearchRecordType(fileSearchRecordType));
    }


    /**
     *  Gets the supported directory record types. 
     *
     *  @return a list containing the supported <code> Directory </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectoryRecordTypes() {
        return (getAdapteeManager().getDirectoryRecordTypes());
    }


    /**
     *  Tests if the given directory record type is supported. 
     *
     *  @param  directoryRecordType a <code> Type </code> indicating a 
     *          directory record type 
     *  @return <code> true </code> if the given record <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> directoryRecordType 
     *          </code> is null 
     */

    @OSID @Override
    public boolean supportsDirectoryRecordType(org.osid.type.Type directoryRecordType) {
        return (getAdapteeManager().supportsDirectoryRecordType(directoryRecordType));
    }


    /**
     *  Gets the supported directory search record types. 
     *
     *  @return a list containing the supported <code> Directory </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectorySearchRecordTypes() {
        return (getAdapteeManager().getDirectorySearchRecordTypes());
    }


    /**
     *  Tests if the given directory search record type is supported. 
     *
     *  @param  directorySearchRecordType a <code> Type </code> indicating a 
     *          directory search record type 
     *  @return <code> true </code> if the given search record <code> Type 
     *          </code> is supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          directorySearchRecordType </code> is null 
     */

    @OSID @Override
    public boolean supportsDirectorySearchRecordType(org.osid.type.Type directorySearchRecordType) {
        return (getAdapteeManager().supportsDirectorySearchRecordType(directorySearchRecordType));
    }


    /**
     *  Gets the session for examining file systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemSession(proxy));
    }


    /**
     *  Gets the session for exmaning file systems for the given path. 
     *
     *  @param  path the path to a directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not found or 
     *          is not a directory 
     *  @throws org.osid.NullArgumentException <code> path </code> or <code> 
     *          proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSessionForPath(String path, 
                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemSessionForPath(path, proxy));
    }


    /**
     *  Gets the session for exmaning file systems for the given directory. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemSession getFileSystemSessionForDirectory(org.osid.id.Id directoryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemSessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for manipulating file systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemManagementSession(proxy));
    }


    /**
     *  Gets the session for manipulating files for the given path. 
     *
     *  @param  path the path to a directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not found or 
     *          is not a directory 
     *  @throws org.osid.NullArgumentException <code> path </code> or <code> 
     *          proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSessionForPath(String path, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemManagementSessionForPath(path, proxy));
    }


    /**
     *  Gets the session for manipulating files for the given path. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSystemManagementSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSystemManagement() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSystemManagementSession getFileSystemManagementSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSystemManagementSessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for reading and writing files. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileContentSession(proxy));
    }


    /**
     *  Gets the session for reading and writing files for the given path. 
     *
     *  @param  path the path to a directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NotFoundException <code> path </code> is not found or 
     *          is not a directory 
     *  @throws org.osid.NullArgumentException <code> path </code> or <code> 
     *          proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSessionForPath(String path, 
                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileContentSessionForPath(path, proxy));
    }


    /**
     *  Gets the session for reading and writing files for the given path. 
     *
     *  @param  directoryId the <code> Id </code> of a directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileContentSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileContent() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileContentSession getFileContentSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileContentSessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for looking up files. 
     *
     *  @param  proxy a proxy 
     *  @return the <code> FileLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileLookupSession getFileLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileLookupSession(proxy));
    }


    /**
     *  Gets a file lookup session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileLookupSession getFileLookupSessionForDirectory(org.osid.id.Id directoryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileLookupSessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for querying files. 
     *
     *  @param  proxy a proxy 
     *  @return the <code> FileQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuerySession getFileQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileQuerySession(proxy));
    }


    /**
     *  Gets a file query session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileQuerySession getFileQuerySessionForDirectory(org.osid.id.Id directoryId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileQuerySessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for searching for files. 
     *
     *  @param  proxy a proxy 
     *  @return the <code> FileSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSearchSession getFileSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSearchSession(proxy));
    }


    /**
     *  Gets a file search session for the specified directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryId </code> or 
     *          <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsFileSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSearchSession getFileSearchSessionForDirectory(org.osid.id.Id directoryId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSearchSessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for receiving messages about changes to files. 
     *
     *  @param  fileReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a FileNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> fileReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileNotificationSession getFileNotificationSession(org.osid.filing.FileReceiver fileReceiver, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFileNotificationSession(fileReceiver, proxy));
    }


    /**
     *  Gets a file notification session for the specified directory. 
     *
     *  @param  fileReceiver the notification callback 
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> fileReceiver, 
     *          directoryId, </code> or <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileNotificationSession getFileNotificationSessionForDirectory(org.osid.filing.FileReceiver fileReceiver, 
                                                                                          org.osid.id.Id directoryId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileNotificationSessionForDirectory(fileReceiver, directoryId, proxy));
    }


    /**
     *  Gets the session for managing dynamic diectories. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> FileSmartDirectorySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFileSmartDirectory() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.FileSmartDirectorySession getFileSmartDirectorySession(org.osid.id.Id directoryId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getFileSmartDirectorySession(directoryId, proxy));
    }


    /**
     *  Gets the session for examining directories. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryLookupSession getDirectoryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryLookupSession(proxy));
    }


    /**
     *  Gets the session for examining a given directory. If the path is an 
     *  alias, the target directory is used. The path indicates the file alias 
     *  and the real path indicates the target directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryLookupSession getDirectoryLookupSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryLookupSessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for querying directories. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuerySession getDirectoryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryQuerySession(proxy));
    }


    /**
     *  Gets the session for querying directories within a given directory. If 
     *  the path is an alias, the target directory is used. The path indicates 
     *  the file alias and the real path indicates the target directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryQuerySession getDirectoryQuerySessionForDirectory(org.osid.id.Id directoryId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryQuerySessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for searching for directories. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectorySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectorySearchSession getDirectorySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectorySearchSession(proxy));
    }


    /**
     *  Gets the session for searching for directories within a given 
     *  directory. If the path is an alias, the target directory is used. The 
     *  path indicates the file alias and the real path indicates the target 
     *  directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectorySearchSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectorySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectorySearchSession getDirectorySearchSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectorySearchSessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for creating and removing files. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryAdminSession getDirectoryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryAdminSession(proxy));
    }


    /**
     *  Gets the session for searching for creating and removing files in the 
     *  given directory. If the path is an alias, the target directory is 
     *  used. The path indicates the file alias and the real path indicates 
     *  the target directory. 
     *
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryId is not found 
     *          </code> 
     *  @throws org.osid.NullArgumentException <code> directoryId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryAdminSession getDirectoryAdminSessionForDirectory(org.osid.id.Id directoryId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryAdminSessionForDirectory(directoryId, proxy));
    }


    /**
     *  Gets the session for receiving messages about changes to directories. 
     *
     *  @param  directoryReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> directoryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryNotificationSession getDirectoryNotificationSession(org.osid.filing.DirectoryReceiver directoryReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryNotificationSession(directoryReceiver, proxy));
    }


    /**
     *  Gets the session for receiving messages about changes to directories 
     *  in the given directory. If the path is an alias, the target directory 
     *  is used. The path indicates the file alias and the real path indicates 
     *  the target directory. 
     *
     *  @param  directoryReceiver the notification callback 
     *  @param  directoryId the <code> Id </code> to the directory 
     *  @param  proxy a proxy 
     *  @return a <code> DirectoryNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> directoryPath </code> is not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> directoryReceiver, 
     *          directoryId, </code> or <code> proxy </code> is null 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectoryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.DirectoryNotificationSession getDirectoryNotificationSessionForDirectory(org.osid.filing.DirectoryReceiver directoryReceiver, 
                                                                                                    org.osid.id.Id directoryId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDirectoryNotificationSessionForDirectory(directoryReceiver, directoryId, proxy));
    }


    /**
     *  Gets the <code> FilingAllocationProxyManager. </code> 
     *
     *  @return a <code> FilingAllocationProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFilingAllocation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.FilingAllocationProxyManager getFilingAllocationProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getFilingAllocationProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
