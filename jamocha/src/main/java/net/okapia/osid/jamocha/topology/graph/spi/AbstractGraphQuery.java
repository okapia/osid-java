//
// AbstractGraphQuery.java
//
//     A template for making a Graph Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.graph.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for graphs.
 */

public abstract class AbstractGraphQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.topology.GraphQuery {

    private final java.util.Collection<org.osid.topology.records.GraphQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets the node <code> Id </code> for this query to match graphs that 
     *  have a related node. 
     *
     *  @param  nodeId a node <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> nodeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchNodeId(org.osid.id.Id nodeId, boolean match) {
        return;
    }


    /**
     *  Clears the node <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearNodeIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> NodeQuery </code> is available. 
     *
     *  @return <code> true </code> if a node query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsNodeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a node. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the node query 
     *  @throws org.osid.UnimplementedException <code> supportsNodeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.NodeQuery getNodeQuery() {
        throw new org.osid.UnimplementedException("supportsNodeQuery() is false");
    }


    /**
     *  Matches graphs that have any node. 
     *
     *  @param  match <code> true </code> to match graphs with any node, 
     *          <code> false </code> to match graphs with no node 
     */

    @OSID @Override
    public void matchAnyNode(boolean match) {
        return;
    }


    /**
     *  Clears the node terms. 
     */

    @OSID @Override
    public void clearNodeTerms() {
        return;
    }


    /**
     *  Sets the edge <code> Id </code> for this query to match graphs 
     *  containing edges. 
     *
     *  @param  edgeId the edge <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> edgeId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchEdgeId(org.osid.id.Id edgeId, boolean match) {
        return;
    }


    /**
     *  Clears the edge <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEdgeIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> EdgeQuery </code> is available. 
     *
     *  @return <code> true </code> if an edge query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEdgeQuery() {
        return (false);
    }


    /**
     *  Gets the query for an edge. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the edge query 
     *  @throws org.osid.UnimplementedException <code> supportsEdgeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.EdgeQuery getEdgeQuery() {
        throw new org.osid.UnimplementedException("supportsEdgeQuery() is false");
    }


    /**
     *  Matches graphs that have any edge. 
     *
     *  @param  match <code> true </code> to match graphs with any edge, 
     *          <code> false </code> to match graphs with no edge 
     */

    @OSID @Override
    public void matchAnyEdge(boolean match) {
        return;
    }


    /**
     *  Clears the edge terms. 
     */

    @OSID @Override
    public void clearEdgeTerms() {
        return;
    }


    /**
     *  Sets the graph <code> Id </code> for this query to match graphs that 
     *  have the specified graph as an ancestor. 
     *
     *  @param  graphId a graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorGraphId(org.osid.id.Id graphId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor graph <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorGraphIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorGraphQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getAncestorGraphQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorGraphQuery() is false");
    }


    /**
     *  Matches graphs with any ancestor. 
     *
     *  @param  match <code> true </code> to match graphs with any ancestor, 
     *          <code> false </code> to match root graphs 
     */

    @OSID @Override
    public void matchAnyAncestorGraph(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor graph terms. 
     */

    @OSID @Override
    public void clearAncestorGraphTerms() {
        return;
    }


    /**
     *  Sets the graph <code> Id </code> for this query to match graphs that 
     *  have the specified graph as a descendant. 
     *
     *  @param  graphId a graph <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantGraphId(org.osid.id.Id graphId, boolean match) {
        return;
    }


    /**
     *  Clears the descendant graph <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantGraphIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> GraphQuery </code> is available. 
     *
     *  @return <code> true </code> if a graph query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantGraphQuery() {
        return (false);
    }


    /**
     *  Gets the query for a graph. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the graph query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantGraphQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.GraphQuery getDescendantGraphQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantGraphQuery() is false");
    }


    /**
     *  Matches graphs with any descendant. 
     *
     *  @param  match <code> true </code> to match graphs with any descendant, 
     *          <code> false </code> to match leaf graphs 
     */

    @OSID @Override
    public void matchAnyDescendantGraph(boolean match) {
        return;
    }


    /**
     *  Clears the descendant graph terms. 
     */

    @OSID @Override
    public void clearDescendantGraphTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given graph query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a graph implementing the requested record.
     *
     *  @param graphRecordType a graph record type
     *  @return the graph query record
     *  @throws org.osid.NullArgumentException
     *          <code>graphRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(graphRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.topology.records.GraphQueryRecord getGraphQueryRecord(org.osid.type.Type graphRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.topology.records.GraphQueryRecord record : this.records) {
            if (record.implementsRecordType(graphRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(graphRecordType + " is not supported");
    }


    /**
     *  Adds a record to this graph query. 
     *
     *  @param graphQueryRecord graph query record
     *  @param graphRecordType graph record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGraphQueryRecord(org.osid.topology.records.GraphQueryRecord graphQueryRecord, 
                                          org.osid.type.Type graphRecordType) {

        addRecordType(graphRecordType);
        nullarg(graphQueryRecord, "graph query record");
        this.records.add(graphQueryRecord);        
        return;
    }
}
