//
// PeriodElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.billing.period.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class PeriodElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the PeriodElement Id.
     *
     *  @return the period element Id
     */

    public static org.osid.id.Id getPeriodEntityId() {
        return (makeEntityId("osid.billing.Period"));
    }


    /**
     *  Gets the DisplayLabel element Id.
     *
     *  @return the DisplayLabel element Id
     */

    public static org.osid.id.Id getDisplayLabel() {
        return (makeElementId("osid.billing.period.DisplayLabel"));
    }


    /**
     *  Gets the OpenDate element Id.
     *
     *  @return the OpenDate element Id
     */

    public static org.osid.id.Id getOpenDate() {
        return (makeElementId("osid.billing.period.OpenDate"));
    }


    /**
     *  Gets the CloseDate element Id.
     *
     *  @return the CloseDate element Id
     */

    public static org.osid.id.Id getCloseDate() {
        return (makeElementId("osid.billing.period.CloseDate"));
    }


    /**
     *  Gets the BillingDate element Id.
     *
     *  @return the BillingDate element Id
     */

    public static org.osid.id.Id getBillingDate() {
        return (makeElementId("osid.billing.period.BillingDate"));
    }


    /**
     *  Gets the DueDate element Id.
     *
     *  @return the DueDate element Id
     */

    public static org.osid.id.Id getDueDate() {
        return (makeElementId("osid.billing.period.DueDate"));
    }


    /**
     *  Gets the BusinessId element Id.
     *
     *  @return the BusinessId element Id
     */

    public static org.osid.id.Id getBusinessId() {
        return (makeQueryElementId("osid.billing.period.BusinessId"));
    }


    /**
     *  Gets the Business element Id.
     *
     *  @return the Business element Id
     */

    public static org.osid.id.Id getBusiness() {
        return (makeQueryElementId("osid.billing.period.Business"));
    }
}
