//
// InvariantMapProxyObjectiveLookupSession
//
//    Implements an Objective lookup service backed by a fixed
//    collection of objectives. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning;


/**
 *  Implements an Objective lookup service backed by a fixed
 *  collection of objectives. The objectives are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyObjectiveLookupSession
    extends net.okapia.osid.jamocha.core.learning.spi.AbstractMapObjectiveLookupSession
    implements org.osid.learning.ObjectiveLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyObjectiveLookupSession} with no
     *  objectives.
     *
     *  @param objectiveBank the objective bank
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyObjectiveLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                                  org.osid.proxy.Proxy proxy) {
        setObjectiveBank(objectiveBank);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyObjectiveLookupSession} with a single
     *  objective.
     *
     *  @param objectiveBank the objective bank
     *  @param objective an single objective
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBank},
     *          {@code objective} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyObjectiveLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                                  org.osid.learning.Objective objective, org.osid.proxy.Proxy proxy) {

        this(objectiveBank, proxy);
        putObjective(objective);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyObjectiveLookupSession} using
     *  an array of objectives.
     *
     *  @param objectiveBank the objective bank
     *  @param objectives an array of objectives
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBank},
     *          {@code objectives} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyObjectiveLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                                  org.osid.learning.Objective[] objectives, org.osid.proxy.Proxy proxy) {

        this(objectiveBank, proxy);
        putObjectives(objectives);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyObjectiveLookupSession} using a
     *  collection of objectives.
     *
     *  @param objectiveBank the objective bank
     *  @param objectives a collection of objectives
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code objectiveBank},
     *          {@code objectives} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyObjectiveLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                                  java.util.Collection<? extends org.osid.learning.Objective> objectives,
                                                  org.osid.proxy.Proxy proxy) {

        this(objectiveBank, proxy);
        putObjectives(objectives);
        return;
    }
}
