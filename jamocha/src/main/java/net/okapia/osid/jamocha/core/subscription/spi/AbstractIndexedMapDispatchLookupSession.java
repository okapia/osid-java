//
// AbstractIndexedMapDispatchLookupSession.java
//
//    A simple framework for providing a Dispatch lookup service
//    backed by a fixed collection of dispatches with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Dispatch lookup service backed by a
 *  fixed collection of dispatches. The dispatches are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some dispatches may be compatible
 *  with more types than are indicated through these dispatch
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Dispatches</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapDispatchLookupSession
    extends AbstractMapDispatchLookupSession
    implements org.osid.subscription.DispatchLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.subscription.Dispatch> dispatchesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.subscription.Dispatch>());
    private final MultiMap<org.osid.type.Type, org.osid.subscription.Dispatch> dispatchesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.subscription.Dispatch>());


    /**
     *  Makes a <code>Dispatch</code> available in this session.
     *
     *  @param  dispatch a dispatch
     *  @throws org.osid.NullArgumentException <code>dispatch<code> is
     *          <code>null</code>
     */

    @Override
    protected void putDispatch(org.osid.subscription.Dispatch dispatch) {
        super.putDispatch(dispatch);

        this.dispatchesByGenus.put(dispatch.getGenusType(), dispatch);
        
        try (org.osid.type.TypeList types = dispatch.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.dispatchesByRecord.put(types.getNextType(), dispatch);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a dispatch from this session.
     *
     *  @param dispatchId the <code>Id</code> of the dispatch
     *  @throws org.osid.NullArgumentException <code>dispatchId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeDispatch(org.osid.id.Id dispatchId) {
        org.osid.subscription.Dispatch dispatch;
        try {
            dispatch = getDispatch(dispatchId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.dispatchesByGenus.remove(dispatch.getGenusType());

        try (org.osid.type.TypeList types = dispatch.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.dispatchesByRecord.remove(types.getNextType(), dispatch);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeDispatch(dispatchId);
        return;
    }


    /**
     *  Gets a <code>DispatchList</code> corresponding to the given
     *  dispatch genus <code>Type</code> which does not include
     *  dispatches of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known dispatches or an error results. Otherwise,
     *  the returned list may contain only those dispatches that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  dispatchGenusType a dispatch genus type 
     *  @return the returned <code>Dispatch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByGenusType(org.osid.type.Type dispatchGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.dispatch.ArrayDispatchList(this.dispatchesByGenus.get(dispatchGenusType)));
    }


    /**
     *  Gets a <code>DispatchList</code> containing the given
     *  dispatch record <code>Type</code>. In plenary mode, the
     *  returned list contains all known dispatches or an error
     *  results. Otherwise, the returned list may contain only those
     *  dispatches that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  dispatchRecordType a dispatch record type 
     *  @return the returned <code>dispatch</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>dispatchRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.DispatchList getDispatchesByRecordType(org.osid.type.Type dispatchRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.dispatch.ArrayDispatchList(this.dispatchesByRecord.get(dispatchRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.dispatchesByGenus.clear();
        this.dispatchesByRecord.clear();

        super.close();

        return;
    }
}
