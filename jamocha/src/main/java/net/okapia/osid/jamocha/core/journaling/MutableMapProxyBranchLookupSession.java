//
// MutableMapProxyBranchLookupSession
//
//    Implements a Branch lookup service backed by a collection of
//    branches that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling;


/**
 *  Implements a Branch lookup service backed by a collection of
 *  branches. The branches are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of branches can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyBranchLookupSession
    extends net.okapia.osid.jamocha.core.journaling.spi.AbstractMapBranchLookupSession
    implements org.osid.journaling.BranchLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyBranchLookupSession}
     *  with no branches.
     *
     *  @param journal the journal
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyBranchLookupSession(org.osid.journaling.Journal journal,
                                                  org.osid.proxy.Proxy proxy) {
        setJournal(journal);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBranchLookupSession} with a
     *  single branch.
     *
     *  @param journal the journal
     *  @param branch a branch
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal},
     *          {@code branch}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBranchLookupSession(org.osid.journaling.Journal journal,
                                                org.osid.journaling.Branch branch, org.osid.proxy.Proxy proxy) {
        this(journal, proxy);
        putBranch(branch);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBranchLookupSession} using an
     *  array of branches.
     *
     *  @param journal the journal
     *  @param branches an array of branches
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal},
     *          {@code branches}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBranchLookupSession(org.osid.journaling.Journal journal,
                                                org.osid.journaling.Branch[] branches, org.osid.proxy.Proxy proxy) {
        this(journal, proxy);
        putBranches(branches);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBranchLookupSession} using a
     *  collection of branches.
     *
     *  @param journal the journal
     *  @param branches a collection of branches
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal},
     *          {@code branches}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyBranchLookupSession(org.osid.journaling.Journal journal,
                                                java.util.Collection<? extends org.osid.journaling.Branch> branches,
                                                org.osid.proxy.Proxy proxy) {
   
        this(journal, proxy);
        setSessionProxy(proxy);
        putBranches(branches);
        return;
    }

    
    /**
     *  Makes a {@code Branch} available in this session.
     *
     *  @param branch an branch
     *  @throws org.osid.NullArgumentException {@code branch{@code 
     *          is {@code null}
     */

    @Override
    public void putBranch(org.osid.journaling.Branch branch) {
        super.putBranch(branch);
        return;
    }


    /**
     *  Makes an array of branches available in this session.
     *
     *  @param branches an array of branches
     *  @throws org.osid.NullArgumentException {@code branches{@code 
     *          is {@code null}
     */

    @Override
    public void putBranches(org.osid.journaling.Branch[] branches) {
        super.putBranches(branches);
        return;
    }


    /**
     *  Makes collection of branches available in this session.
     *
     *  @param branches
     *  @throws org.osid.NullArgumentException {@code branch{@code 
     *          is {@code null}
     */

    @Override
    public void putBranches(java.util.Collection<? extends org.osid.journaling.Branch> branches) {
        super.putBranches(branches);
        return;
    }


    /**
     *  Removes a Branch from this session.
     *
     *  @param branchId the {@code Id} of the branch
     *  @throws org.osid.NullArgumentException {@code branchId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBranch(org.osid.id.Id branchId) {
        super.removeBranch(branchId);
        return;
    }    
}
