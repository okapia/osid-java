//
// AbstractImmutableEngine.java
//
//     Wraps a mutable Engine to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.search.engine.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Engine</code> to hide modifiers. This
 *  wrapper provides an immutized Engine from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying engine whose state changes are visible.
 */

public abstract class AbstractImmutableEngine
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidCatalog
    implements org.osid.search.Engine {

    private final org.osid.search.Engine engine;


    /**
     *  Constructs a new <code>AbstractImmutableEngine</code>.
     *
     *  @param engine the engine to immutablize
     *  @throws org.osid.NullArgumentException <code>engine</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableEngine(org.osid.search.Engine engine) {
        super(engine);
        this.engine = engine;
        return;
    }


    /**
     *  Gets the engine record corresponding to the given <code> Engine 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> engineRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(engineRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  engineRecordType the type of engine record to retrieve 
     *  @return the engine record 
     *  @throws org.osid.NullArgumentException <code> engineRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(engineRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.search.records.EngineRecord getEngineRecord(org.osid.type.Type engineRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.engine.getEngineRecord(engineRecordType));
    }
}

