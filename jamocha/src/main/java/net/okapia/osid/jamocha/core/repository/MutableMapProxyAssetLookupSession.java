//
// MutableMapProxyAssetLookupSession
//
//    Implements an Asset lookup service backed by a collection of
//    assets that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements an Asset lookup service backed by a collection of
 *  assets. The assets are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of assets can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyAssetLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractMapAssetLookupSession
    implements org.osid.repository.AssetLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyAssetLookupSession}
     *  with no assets.
     *
     *  @param repository the repository
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyAssetLookupSession(org.osid.repository.Repository repository,
                                                  org.osid.proxy.Proxy proxy) {
        setRepository(repository);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyAssetLookupSession} with a
     *  single asset.
     *
     *  @param repository the repository
     *  @param asset an asset
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code asset}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAssetLookupSession(org.osid.repository.Repository repository,
                                                org.osid.repository.Asset asset, org.osid.proxy.Proxy proxy) {
        this(repository, proxy);
        putAsset(asset);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAssetLookupSession} using an
     *  array of assets.
     *
     *  @param repository the repository
     *  @param assets an array of assets
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code assets}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAssetLookupSession(org.osid.repository.Repository repository,
                                                org.osid.repository.Asset[] assets, org.osid.proxy.Proxy proxy) {
        this(repository, proxy);
        putAssets(assets);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyAssetLookupSession} using a
     *  collection of assets.
     *
     *  @param repository the repository
     *  @param assets a collection of assets
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code repository},
     *          {@code assets}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyAssetLookupSession(org.osid.repository.Repository repository,
                                                java.util.Collection<? extends org.osid.repository.Asset> assets,
                                                org.osid.proxy.Proxy proxy) {
   
        this(repository, proxy);
        setSessionProxy(proxy);
        putAssets(assets);
        return;
    }

    
    /**
     *  Makes a {@code Asset} available in this session.
     *
     *  @param asset an asset
     *  @throws org.osid.NullArgumentException {@code asset{@code 
     *          is {@code null}
     */

    @Override
    public void putAsset(org.osid.repository.Asset asset) {
        super.putAsset(asset);
        return;
    }


    /**
     *  Makes an array of assets available in this session.
     *
     *  @param assets an array of assets
     *  @throws org.osid.NullArgumentException {@code assets{@code 
     *          is {@code null}
     */

    @Override
    public void putAssets(org.osid.repository.Asset[] assets) {
        super.putAssets(assets);
        return;
    }


    /**
     *  Makes collection of assets available in this session.
     *
     *  @param assets
     *  @throws org.osid.NullArgumentException {@code asset{@code 
     *          is {@code null}
     */

    @Override
    public void putAssets(java.util.Collection<? extends org.osid.repository.Asset> assets) {
        super.putAssets(assets);
        return;
    }


    /**
     *  Removes a Asset from this session.
     *
     *  @param assetId the {@code Id} of the asset
     *  @throws org.osid.NullArgumentException {@code assetId{@code  is
     *          {@code null}
     */

    @Override
    public void removeAsset(org.osid.id.Id assetId) {
        super.removeAsset(assetId);
        return;
    }    
}
