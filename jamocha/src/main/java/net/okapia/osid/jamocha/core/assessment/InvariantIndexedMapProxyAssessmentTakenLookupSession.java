//
// InvariantIndexedMapProxyAssessmentTakenLookupSession
//
//    Implements an AssessmentTaken lookup service backed by a fixed
//    collection of assessmentsTaken indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.assessment;


/**
 *  Implements a AssessmentTaken lookup service backed by a fixed
 *  collection of assessmentsTaken. The assessmentsTaken are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some assessmentsTaken may be compatible
 *  with more types than are indicated through these assessmentTaken
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyAssessmentTakenLookupSession
    extends net.okapia.osid.jamocha.core.assessment.spi.AbstractIndexedMapAssessmentTakenLookupSession
    implements org.osid.assessment.AssessmentTakenLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyAssessmentTakenLookupSession}
     *  using an array of assessments taken.
     *
     *  @param bank the bank
     *  @param assessmentsTaken an array of assessments taken
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentsTaken} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                                         org.osid.assessment.AssessmentTaken[] assessmentsTaken, 
                                                         org.osid.proxy.Proxy proxy) {

        setBank(bank);
        setSessionProxy(proxy);
        putAssessmentsTaken(assessmentsTaken);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyAssessmentTakenLookupSession}
     *  using a collection of assessments taken.
     *
     *  @param bank the bank
     *  @param assessmentsTaken a collection of assessments taken
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code bank},
     *          {@code assessmentsTaken} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyAssessmentTakenLookupSession(org.osid.assessment.Bank bank,
                                                         java.util.Collection<? extends org.osid.assessment.AssessmentTaken> assessmentsTaken,
                                                         org.osid.proxy.Proxy proxy) {

        setBank(bank);
        setSessionProxy(proxy);
        putAssessmentsTaken(assessmentsTaken);

        return;
    }
}
