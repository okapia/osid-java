//
// AbstractImmutableCredit.java
//
//     Wraps a mutable Credit to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.acknowledgement.credit.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Credit</code> to hide modifiers. This
 *  wrapper provides an immutized Credit from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying credit whose state changes are visible.
 */

public abstract class AbstractImmutableCredit
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.acknowledgement.Credit {

    private final org.osid.acknowledgement.Credit credit;


    /**
     *  Constructs a new <code>AbstractImmutableCredit</code>.
     *
     *  @param credit the credit to immutablize
     *  @throws org.osid.NullArgumentException <code>credit</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableCredit(org.osid.acknowledgement.Credit credit) {
        super(credit);
        this.credit = credit;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the referenced object. 
     *
     *  @return the reference <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        return (this.credit.getReferenceId());
    }


    /**
     *  Gets the <code> Id </code> of the resource in this credit. 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.credit.getResourceId());
    }


    /**
     *  Gets the resource in this credit. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.credit.getResource());
    }


    /**
     *  Gets the record corresponding to the given <code> Credit </code> 
     *  record <code> Type. </code> This method is used to retrieve an object 
     *  implementing the requested record. The <code> creditRecordType </code> 
     *  may be the <code> Type </code> returned in <code> getRecordTypes() 
     *  </code> or any of its parents in a <code> Type </code> hierarchy where 
     *  <code> hasRecordType(creditRecordType) </code> is <code> true </code> 
     *  . 
     *
     *  @param  creditRecordType the type of credit record to retrieve 
     *  @return the credit record 
     *  @throws org.osid.NullArgumentException <code> creditRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(creditRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.acknowledgement.records.CreditRecord getCreditRecord(org.osid.type.Type creditRecordType)
        throws org.osid.OperationFailedException {

        return (this.credit.getCreditRecord(creditRecordType));
    }
}

