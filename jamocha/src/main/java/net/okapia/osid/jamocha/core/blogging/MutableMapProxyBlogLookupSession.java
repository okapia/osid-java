//
// MutableMapProxyBlogLookupSession
//
//    Implements a Blog lookup service backed by a collection of
//    blogs that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.blogging;


/**
 *  Implements a Blog lookup service backed by a collection of
 *  blogs. The blogs are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of blogs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyBlogLookupSession
    extends net.okapia.osid.jamocha.core.blogging.spi.AbstractMapBlogLookupSession
    implements org.osid.blogging.BlogLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBlogLookupSession} with no
     *  blogs.
     *
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code proxy} is
     *          {@code null}
     */

    public MutableMapProxyBlogLookupSession(org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBlogLookupSession} with a
     *  single blog.
     *
     *  @param blog a blog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code blog} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyBlogLookupSession(org.osid.blogging.Blog blog, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBlog(blog);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyBlogLookupSession} using an
     *  array of blogs.
     *
     *  @param blogs an array of blogs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code blogs} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyBlogLookupSession(org.osid.blogging.Blog[] blogs, org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBlogs(blogs);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyBlogLookupSession} using
     *  a collection of blogs.
     *
     *  @param blogs a collection of blogs
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code blogs} or
     *          {@code proxy} is {@code null}
     */

    public MutableMapProxyBlogLookupSession(java.util.Collection<? extends org.osid.blogging.Blog> blogs,
                                                org.osid.proxy.Proxy proxy) {
        setSessionProxy(proxy);
        putBlogs(blogs);
        return;
    }

    
    /**
     *  Makes a {@code Blog} available in this session.
     *
     *  @param blog an blog
     *  @throws org.osid.NullArgumentException {@code blog{@code 
     *          is {@code null}
     */

    @Override
    public void putBlog(org.osid.blogging.Blog blog) {
        super.putBlog(blog);
        return;
    }


    /**
     *  Makes an array of blogs available in this session.
     *
     *  @param blogs an array of blogs
     *  @throws org.osid.NullArgumentException {@code blogs{@code 
     *          is {@code null}
     */

    @Override
    public void putBlogs(org.osid.blogging.Blog[] blogs) {
        super.putBlogs(blogs);
        return;
    }


    /**
     *  Makes collection of blogs available in this session.
     *
     *  @param blogs
     *  @throws org.osid.NullArgumentException {@code blog{@code 
     *          is {@code null}
     */

    @Override
    public void putBlogs(java.util.Collection<? extends org.osid.blogging.Blog> blogs) {
        super.putBlogs(blogs);
        return;
    }


    /**
     *  Removes a Blog from this session.
     *
     *  @param blogId the {@code Id} of the blog
     *  @throws org.osid.NullArgumentException {@code blogId{@code  is
     *          {@code null}
     */

    @Override
    public void removeBlog(org.osid.id.Id blogId) {
        super.removeBlog(blogId);
        return;
    }    
}
