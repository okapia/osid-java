//
// AbstractAdapterTrustLookupSession.java
//
//    A Trust lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.authentication.process.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Trust lookup session adapter.
 */

public abstract class AbstractAdapterTrustLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.authentication.process.TrustLookupSession {

    private final org.osid.authentication.process.TrustLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterTrustLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterTrustLookupSession(org.osid.authentication.process.TrustLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Agency/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Agency Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.session.getAgencyId());
    }


    /**
     *  Gets the {@code Agency} associated with this session.
     *
     *  @return the {@code Agency} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getAgency());
    }


    /**
     *  Tests if this user can perform {@code Trust} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupTrusts() {
        return (this.session.canLookupTrusts());
    }


    /**
     *  A complete view of the {@code Trust} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeTrustView() {
        this.session.useComparativeTrustView();
        return;
    }


    /**
     *  A complete view of the {@code Trust} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryTrustView() {
        this.session.usePlenaryTrustView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include trusts in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        this.session.useFederatedAgencyView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        this.session.useIsolatedAgencyView();
        return;
    }
    
     
    /**
     *  Gets the {@code Trust} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Trust} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Trust} and
     *  retained for compatibility.
     *
     *  @param trustId {@code Id} of the {@code Trust}
     *  @return the trust
     *  @throws org.osid.NotFoundException {@code trustId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code trustId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.Trust getTrust(org.osid.id.Id trustId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTrust(trustId));
    }


    /**
     *  Gets a {@code TrustList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the trusts
     *  specified in the {@code Id} list, in the order of the list,
     *  including duplicates, or an error results if an {@code Id} in
     *  the supplied list is not found or inaccessible. Otherwise,
     *  inaccessible {@code Trusts} may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  @param  trustIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Trust} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code trustIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByIds(org.osid.id.IdList trustIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTrustsByIds(trustIds));
    }


    /**
     *  Gets a {@code TrustList} corresponding to the given
     *  trust genus {@code Type} which does not include
     *  trusts of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  trusts or an error results. Otherwise, the returned list
     *  may contain only those trusts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  trustGenusType a trust genus type 
     *  @return the returned {@code Trust} list
     *  @throws org.osid.NullArgumentException
     *          {@code trustGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByGenusType(org.osid.type.Type trustGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTrustsByGenusType(trustGenusType));
    }


    /**
     *  Gets a {@code TrustList} corresponding to the given
     *  trust genus {@code Type} and include any additional
     *  trusts with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  trusts or an error results. Otherwise, the returned list
     *  may contain only those trusts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  trustGenusType a trust genus type 
     *  @return the returned {@code Trust} list
     *  @throws org.osid.NullArgumentException
     *          {@code trustGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByParentGenusType(org.osid.type.Type trustGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTrustsByParentGenusType(trustGenusType));
    }


    /**
     *  Gets a {@code TrustList} containing the given
     *  trust record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  trusts or an error results. Otherwise, the returned list
     *  may contain only those trusts that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  trustRecordType a trust record type 
     *  @return the returned {@code Trust} list
     *  @throws org.osid.NullArgumentException
     *          {@code trustRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrustsByRecordType(org.osid.type.Type trustRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTrustsByRecordType(trustRecordType));
    }


    /**
     *  Gets a {@code TrustList} in the same circle, or the same level
     *  of confidence, as the given trust. In plenary mode, the
     *  returned list contains all known trusts or an error
     *  results. Otherwise, the returned list may contain only those
     *  trusts that are accessible through this session.
     *
     *  @param  trustId a trust {@code Id} 
     *  @return the returned {@code Trust} list 
     *  @throws org.osid.NotFoundException {@code trustId} is not found 
     *  @throws org.osid.NullArgumentException {@code trustId} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getCircleOfTrust(org.osid.id.Id trustId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.getCircleOfTrust(trustId));
    }


    /**
     *  Gets all {@code Trusts}. 
     *
     *  In plenary mode, the returned list contains all known trusts
     *  or an error results. Otherwise, the returned list may contain
     *  only those trusts that are accessible through this session. In
     *  both cases, the order of the set is not specified.
     *
     *  @return a list of {@code Trusts} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.process.TrustList getTrusts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getTrusts());
    }
}
