//
// AbstractQueryOublietteLookupSession.java
//
//    An inline adapter that maps an OublietteLookupSession to
//    an OublietteQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.hold.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an OublietteLookupSession to
 *  an OublietteQuerySession.
 */

public abstract class AbstractQueryOublietteLookupSession
    extends net.okapia.osid.jamocha.hold.spi.AbstractOublietteLookupSession
    implements org.osid.hold.OublietteLookupSession {

    private final org.osid.hold.OublietteQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryOublietteLookupSession.
     *
     *  @param querySession the underlying oubliette query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryOublietteLookupSession(org.osid.hold.OublietteQuerySession querySession) {
        nullarg(querySession, "oubliette query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Oubliette</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupOubliettes() {
        return (this.session.canSearchOubliettes());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Oubliette</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Oubliette</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Oubliette</code> and
     *  retained for compatibility.
     *
     *  @param  oublietteId <code>Id</code> of the
     *          <code>Oubliette</code>
     *  @return the oubliette
     *  @throws org.osid.NotFoundException <code>oublietteId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>oublietteId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.Oubliette getOubliette(org.osid.id.Id oublietteId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.OublietteQuery query = getQuery();
        query.matchId(oublietteId, true);
        org.osid.hold.OublietteList oubliettes = this.session.getOubliettesByQuery(query);
        if (oubliettes.hasNext()) {
            return (oubliettes.getNextOubliette());
        } 
        
        throw new org.osid.NotFoundException(oublietteId + " not found");
    }


    /**
     *  Gets an <code>OublietteList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  oubliettes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Oubliettes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  oublietteIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Oubliette</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByIds(org.osid.id.IdList oublietteIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.OublietteQuery query = getQuery();

        try (org.osid.id.IdList ids = oublietteIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getOubliettesByQuery(query));
    }


    /**
     *  Gets an <code>OublietteList</code> corresponding to the given
     *  oubliette genus <code>Type</code> which does not include
     *  oubliettes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  oublietteGenusType an oubliette genus type 
     *  @return the returned <code>Oubliette</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByGenusType(org.osid.type.Type oublietteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.OublietteQuery query = getQuery();
        query.matchGenusType(oublietteGenusType, true);
        return (this.session.getOubliettesByQuery(query));
    }


    /**
     *  Gets an <code>OublietteList</code> corresponding to the given
     *  oubliette genus <code>Type</code> and include any additional
     *  oubliettes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  oublietteGenusType an oubliette genus type 
     *  @return the returned <code>Oubliette</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByParentGenusType(org.osid.type.Type oublietteGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.OublietteQuery query = getQuery();
        query.matchParentGenusType(oublietteGenusType, true);
        return (this.session.getOubliettesByQuery(query));
    }


    /**
     *  Gets an <code>OublietteList</code> containing the given
     *  oubliette record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  oublietteRecordType an oubliette record type 
     *  @return the returned <code>Oubliette</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>oublietteRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByRecordType(org.osid.type.Type oublietteRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.OublietteQuery query = getQuery();
        query.matchRecordType(oublietteRecordType, true);
        return (this.session.getOubliettesByQuery(query));
    }


    /**
     *  Gets an <code>OublietteList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known oubliettes or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  oubliettes that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Oubliette</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.OublietteQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getOubliettesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Oubliettes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  oubliettes or an error results. Otherwise, the returned list
     *  may contain only those oubliettes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Oubliettes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hold.OublietteList getOubliettes()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.hold.OublietteQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getOubliettesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.hold.OublietteQuery getQuery() {
        org.osid.hold.OublietteQuery query = this.session.getOublietteQuery();
        return (query);
    }
}
