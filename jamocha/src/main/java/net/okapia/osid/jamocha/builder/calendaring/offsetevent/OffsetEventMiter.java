//
// OffsetEventMiter.java
//
//     Defines an OffsetEvent miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.calendaring.offsetevent;


/**
 *  Defines an <code>OffsetEvent</code> miter for use with the builders.
 */

public interface OffsetEventMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.calendaring.OffsetEvent {


    /**
     *  Sets the fixed start time.
     *
     *  @param time a fixed start time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setFixedStartTime(org.osid.calendaring.DateTime time);


    /**
     *  Sets the start reference event.
     *
     *  @param event a start reference event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public void setStartReferenceEvent(org.osid.calendaring.Event event);


    /**
     *  Sets the fixed start offset.
     *
     *  @param offset a fixed start offset
     *  @throws org.osid.NullArgumentException <code>offset</code> is
     *          <code>null</code>
     */

    public void setFixedStartOffset(org.osid.calendaring.Duration offset);


    /**
     *  Sets the relative weekday start offset.
     *
     *  @param offset a relative weekday start offset
     */

    public void setRelativeWeekdayStartOffset(long offset);


    /**
     *  Sets the relative start weekday.
     *
     *  @param weekday a relative start weekday
     */

    public void setRelativeStartWeekday(long weekday);


    /**
     *  Sets the fixed duration.
     *
     *  @param duration a duration
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setFixedDuration(org.osid.calendaring.Duration duration);


    /**
     *  Sets the end reference event.
     *
     *  @param event an end reference event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public void setEndReferenceEvent(org.osid.calendaring.Event event);


    /**
     *  Sets the fixed end offset.
     *
     *  @param offset a fixed end offset
     *  @throws org.osid.NullArgumentException <code>offset</code> is
     *          <code>null</code>
     */

    public void setFixedEndOffset(org.osid.calendaring.Duration offset);


    /**
     *  Sets the relative weekday end offset.
     *
     *  @param offset a relative weekday end offset
     */

    public void setRelativeWeekdayEndOffset(long offset);


    /**
     *  Sets the relative end weekday.
     *
     *  @param weekday a relative end weekday
     */

    public void setRelativeEndWeekday(long weekday);


    /**
     *  Sets the location description.
     *
     *  @param description a location description
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    public void setLocationDescription(org.osid.locale.DisplayText description);


    /**
     *  Sets the location.
     *
     *  @param location a location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public void setLocation(org.osid.mapping.Location location);


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    public void addSponsor(org.osid.resource.Resource sponsor);


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    public void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors);


    /**
     *  Adds an OffsetEvent record.
     *
     *  @param record an offsetEvent record
     *  @param recordType the type of offsetEvent record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addOffsetEventRecord(org.osid.calendaring.records.OffsetEventRecord record, org.osid.type.Type recordType);
}       


