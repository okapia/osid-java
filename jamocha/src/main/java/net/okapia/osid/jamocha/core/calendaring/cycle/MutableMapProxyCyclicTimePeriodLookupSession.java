//
// MutableMapProxyCyclicTimePeriodLookupSession
//
//    Implements a CyclicTimePeriod lookup service backed by a collection of
//    cyclicTimePeriods that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.cycle;


/**
 *  Implements a CyclicTimePeriod lookup service backed by a collection of
 *  cyclicTimePeriods. The cyclicTimePeriods are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of cyclic time periods can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyCyclicTimePeriodLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.cycle.spi.AbstractMapCyclicTimePeriodLookupSession
    implements org.osid.calendaring.cycle.CyclicTimePeriodLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyCyclicTimePeriodLookupSession}
     *  with no cyclic time periods.
     *
     *  @param calendar the calendar
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                  org.osid.proxy.Proxy proxy) {
        setCalendar(calendar);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyCyclicTimePeriodLookupSession} with a
     *  single cyclic time period.
     *
     *  @param calendar the calendar
     *  @param cyclicTimePeriod a cyclic time period
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code cyclicTimePeriod}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putCyclicTimePeriod(cyclicTimePeriod);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCyclicTimePeriodLookupSession} using an
     *  array of cyclic time periods.
     *
     *  @param calendar the calendar
     *  @param cyclicTimePeriods an array of cyclic time periods
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code cyclicTimePeriods}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                org.osid.calendaring.cycle.CyclicTimePeriod[] cyclicTimePeriods, org.osid.proxy.Proxy proxy) {
        this(calendar, proxy);
        putCyclicTimePeriods(cyclicTimePeriods);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyCyclicTimePeriodLookupSession} using a
     *  collection of cyclic time periods.
     *
     *  @param calendar the calendar
     *  @param cyclicTimePeriods a collection of cyclic time periods
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code calendar},
     *          {@code cyclicTimePeriods}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyCyclicTimePeriodLookupSession(org.osid.calendaring.Calendar calendar,
                                                java.util.Collection<? extends org.osid.calendaring.cycle.CyclicTimePeriod> cyclicTimePeriods,
                                                org.osid.proxy.Proxy proxy) {
   
        this(calendar, proxy);
        setSessionProxy(proxy);
        putCyclicTimePeriods(cyclicTimePeriods);
        return;
    }

    
    /**
     *  Makes a {@code CyclicTimePeriod} available in this session.
     *
     *  @param cyclicTimePeriod an cyclic time period
     *  @throws org.osid.NullArgumentException {@code cyclicTimePeriod{@code 
     *          is {@code null}
     */

    @Override
    public void putCyclicTimePeriod(org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod) {
        super.putCyclicTimePeriod(cyclicTimePeriod);
        return;
    }


    /**
     *  Makes an array of cyclicTimePeriods available in this session.
     *
     *  @param cyclicTimePeriods an array of cyclic time periods
     *  @throws org.osid.NullArgumentException {@code cyclicTimePeriods{@code 
     *          is {@code null}
     */

    @Override
    public void putCyclicTimePeriods(org.osid.calendaring.cycle.CyclicTimePeriod[] cyclicTimePeriods) {
        super.putCyclicTimePeriods(cyclicTimePeriods);
        return;
    }


    /**
     *  Makes collection of cyclic time periods available in this session.
     *
     *  @param cyclicTimePeriods
     *  @throws org.osid.NullArgumentException {@code cyclicTimePeriod{@code 
     *          is {@code null}
     */

    @Override
    public void putCyclicTimePeriods(java.util.Collection<? extends org.osid.calendaring.cycle.CyclicTimePeriod> cyclicTimePeriods) {
        super.putCyclicTimePeriods(cyclicTimePeriods);
        return;
    }


    /**
     *  Removes a CyclicTimePeriod from this session.
     *
     *  @param cyclicTimePeriodId the {@code Id} of the cyclic time period
     *  @throws org.osid.NullArgumentException {@code cyclicTimePeriodId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCyclicTimePeriod(org.osid.id.Id cyclicTimePeriodId) {
        super.removeCyclicTimePeriod(cyclicTimePeriodId);
        return;
    }    
}
