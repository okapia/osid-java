//
// AbstractMutableItem.java
//
//     Defines a mutable Item.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.item.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Item</code>.
 */

public abstract class AbstractMutableItem
    extends net.okapia.osid.jamocha.assessment.item.spi.AbstractItem
    implements org.osid.assessment.Item,
               net.okapia.osid.jamocha.builder.assessment.item.ItemMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this item. 
     *
     *  @param record item record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addItemRecord(org.osid.assessment.records.ItemRecord record, org.osid.type.Type recordType) {
        super.addItemRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the display name for this item.
     *
     *  @param displayName the name for this item
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this item.
     *
     *  @param description the description of this item
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException
     *          <code>genusType</code> is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the question.
     *
     *  @param question the question
     *  @throws org.osid/NullArgumentException <code>question</code>
     *          is <code>null</code>
     */

    @Override
    public void setQuestion(org.osid.assessment.Question question) {
        super.setQuestion(question);
        return;
    }


    /**
     *  Adds an answer.
     *
     *  @param answer an answer
     *  @throws org.osid/NullArgumentException <code>answer</code>
     *          is <code>null</code>
     */

    @Override
    public void addAnswer(org.osid.assessment.Answer answer) {
        super.addAnswer(answer);
        return;
    }


    /**
     *  Sets all the answers.
     *
     *  @param answers a collection of answers
     *  @throws org.osid/NullArgumentException <code>answers</code>
     *          is <code>null</code>
     */

    @Override
    public void setAnswers(java.util.Collection<org.osid.assessment.Answer> answers) {
        super.setAnswers(answers);
        return;
    }


    /**
     *  Adds a learning objective.
     *
     *  @param learningObjective a learning objective
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjective</code> is <code>null</code>
     */

    @Override
    public void addLearningObjective(org.osid.learning.Objective learningObjective) {
        super.addLearningObjective(learningObjective);
        return;
    }


    /**
     *  Sets all the learning objectives.
     *
     *  @param learningObjectives a collection of learning objectives
     *  @throws org.osid.NullArgumentException
     *          <code>learningObjectives</code> is <code>null</code>
     */

    @Override
    public void setLearningObjectives(java.util.Collection<org.osid.learning.Objective> learningObjectives) {
        super.setLearningObjectives(learningObjectives);
        return;
    }
}

