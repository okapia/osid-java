//
// AbstractObjectiveBankNotificationSession.java
//
//     A template for making ObjectiveBankNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.learning.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code ObjectiveBank} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code ObjectiveBank} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for objective bank entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractObjectiveBankNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.learning.ObjectiveBankNotificationSession {


    /**
     *  Tests if this user can register for {@code ObjectiveBank}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForObjectiveBankNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeObjectiveBankNotification() </code>.
     */

    @OSID @Override
    public void reliableObjectiveBankNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableObjectiveBankNotifications() {
        return;
    }


    /**
     *  Acknowledge an objective bank notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeObjectiveBankNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new objective banks. {@code
     *  ObjectiveBankReceiver.newObjectiveBank()} is invoked when an
     *  new {@code ObjectiveBank} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewObjectiveBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of updated objective banks. {@code
     *  ObjectiveBankReceiver.changedObjectiveBank()} is invoked when
     *  an objective bank is changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedObjectiveBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated objective
     *  bank. {@code ObjectiveBankReceiver.changedObjectiveBank()} is
     *  invoked when the specified objective bank is changed.
     *
     *  @param objectiveBankId the {@code Id} of the {@code ObjectiveBank} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code objectiveBankId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted objective banks. {@code
     *  ObjectiveBankReceiver.deletedObjectiveBank()} is invoked when
     *  an objective bank is deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedObjectiveBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of a deleted objective bank. {@code
     *  ObjectiveBankReceiver.deletedObjectiveBank()} is invoked when
     *  the specified objective bank is deleted.
     *
     *  @param objectiveBankId the {@code Id} of the
     *          {@code ObjectiveBank} to monitor
     *  @throws org.osid.NullArgumentException {@code objectiveBankId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated objective bank
     *  hierarchy structure.  <code>
     *  ObjectiveBankReceiver.changedChildOfObjectiveBanks() </code>
     *  is invoked when a node experiences a change in its children.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedObjectiveBankHierarchy()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated objective bank
     *  hierarchy structure.  <code>
     *  ObjectiveBankReceiver.changedChildOfObjectiveBanks() </code>
     *  is invoked when the specified node or any of its ancestors
     *  experiences a change in its children.
     *
     *  @param objectiveBankId the <code> Id </code> of the <code>
     *          ObjectiveBank </code> node to monitor
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedObjectiveBankHierarchyForAncestors(org.osid.id.Id objectiveBankId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Registers for notification of an updated objective bank
     *  hierarchy structure.  <code>
     *  ObjectiveBankReceiver.changedChildOfObjectiveBanks() </code>
     *  is invoked when the specified node or any of its descendants
     *  experiences a change in its children.
     *
     *  @param objectiveBankId the <code> Id </code> of the <code> ObjectiveBank
     *          </code> node to monitor
     *  @throws org.osid.NullArgumentException <code> objectiveBankId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedObjectiveBankHierarchyForDescendants(org.osid.id.Id objectiveBankId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }
}
