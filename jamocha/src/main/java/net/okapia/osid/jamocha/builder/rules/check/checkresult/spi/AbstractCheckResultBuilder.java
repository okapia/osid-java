//
// AbstractCheckResult.java
//
//     Defines a CheckResult builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.rules.check.checkresult.spi;


/**
 *  Defines a <code>CheckResult</code> builder.
 */

public abstract class AbstractCheckResultBuilder<T extends AbstractCheckResultBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractBrowsableBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.rules.check.checkresult.CheckResultMiter checkResult;


    /**
     *  Constructs a new <code>AbstractCheckResultBuilder</code>.
     *
     *  @param checkResult the check result to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractCheckResultBuilder(net.okapia.osid.jamocha.builder.rules.check.checkresult.CheckResultMiter checkResult) {
        super(checkResult);
        this.checkResult = checkResult;
        return;
    }


    /**
     *  Builds the check result.
     *
     *  @return the new check result
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>checkResult</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.rules.check.CheckResult build() {
        (new net.okapia.osid.jamocha.builder.validator.rules.check.checkresult.CheckResultValidator(getValidations())).validate(this.checkResult);
        return (new net.okapia.osid.jamocha.builder.rules.check.checkresult.ImmutableCheckResult(this.checkResult));
    }


    /**
     *  Gets the check result. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new checkResult
     */

    @Override
    public net.okapia.osid.jamocha.builder.rules.check.checkresult.CheckResultMiter getMiter() {
        return (this.checkResult);
    }


    /**
     *  Sets the instruction.
     *
     *  @param instruction an instruction
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>instruction</code> is <code>null</code>
     */

    public T instruction(org.osid.rules.check.Instruction instruction) {
        getMiter().setInstruction(instruction);
        return (self());
    }


    /**
     *  Sets the failed flag.
     */

    public T failed() {
        getMiter().setFailed(true);
        return (self());
    }


    /**
     *  Unsets the failed flag for a success.
     */

    public T success() {
        getMiter().setFailed(false);
        return (self());
    }


    /**
     *  Sets the warning flag.
     */

    public T warning() {
        getMiter().setWarning(true);
        return (self());
    }


    /**
     *  Unsets the warning flag to indicate a hard failure.
     */

    public T error() {
        getMiter().setWarning(false);
        return (self());
    }


    /**
     *  Sets the message.
     *
     *  @param message a message
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>message</code> is <code>null</code>
     */

    public T message(org.osid.locale.DisplayText message) {
        getMiter().setMessage(message);
        return (self());
    }


    /**
     *  Adds a CheckResult record.
     *
     *  @param record a check result record
     *  @param recordType the type of check result record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.rules.check.records.CheckResultRecord record, org.osid.type.Type recordType) {
        getMiter().addCheckResultRecord(record, recordType);
        return (self());
    }
}       


