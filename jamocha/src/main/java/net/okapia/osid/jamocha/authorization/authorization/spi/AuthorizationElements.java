//
// AuthorizationElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.authorization.spi;

/**
 *  Ids for object elements for use in forms and queries.
 */

public class AuthorizationElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the AuthorizationElement Id.
     *
     *  @return the authorization element Id
     */

    public static org.osid.id.Id getAuthorizationEntityId() {
        return (makeEntityId("osid.authorization.Authorization"));
    }


    /**
     *  Gets the ResourceId element Id.
     *
     *  @return the ResourceId element Id
     */

    public static org.osid.id.Id getResourceId() {
        return (makeElementId("osid.authorization.authorization.ResourceId"));
    }


    /**
     *  Gets the Resource element Id.
     *
     *  @return the Resource element Id
     */

    public static org.osid.id.Id getResource() {
        return (makeElementId("osid.authorization.authorization.Resource"));
    }


    /**
     *  Gets the TrustId element Id.
     *
     *  @return the TrustId element Id
     */

    public static org.osid.id.Id getTrustId() {
        return (makeElementId("osid.authorization.authorization.TrustId"));
    }


    /**
     *  Gets the Trust element Id.
     *
     *  @return the Trust element Id
     */

    public static org.osid.id.Id getTrust() {
        return (makeElementId("osid.authorization.authorization.Trust"));
    }


    /**
     *  Gets the AgentId element Id.
     *
     *  @return the AgentId element Id
     */

    public static org.osid.id.Id getAgentId() {
        return (makeElementId("osid.authorization.authorization.AgentId"));
    }


    /**
     *  Gets the Agent element Id.
     *
     *  @return the Agent element Id
     */

    public static org.osid.id.Id getAgent() {
        return (makeElementId("osid.authorization.authorization.Agent"));
    }


    /**
     *  Gets the FunctionId element Id.
     *
     *  @return the FunctionId element Id
     */

    public static org.osid.id.Id getFunctionId() {
        return (makeElementId("osid.authorization.authorization.FunctionId"));
    }


    /**
     *  Gets the Function element Id.
     *
     *  @return the Function element Id
     */

    public static org.osid.id.Id getFunction() {
        return (makeElementId("osid.authorization.authorization.Function"));
    }


    /**
     *  Gets the QualifierId element Id.
     *
     *  @return the QualifierId element Id
     */

    public static org.osid.id.Id getQualifierId() {
        return (makeElementId("osid.authorization.authorization.QualifierId"));
    }


    /**
     *  Gets the Qualifier element Id.
     *
     *  @return the Qualifier element Id
     */

    public static org.osid.id.Id getQualifier() {
        return (makeElementId("osid.authorization.authorization.Qualifier"));
    }


    /**
     *  Gets the ExplicitAuthorizations element Id.
     *
     *  @return the ExplicitAuthorizations element Id
     */

    public static org.osid.id.Id getExplicitAuthorizations() {
        return (makeQueryElementId("osid.authorization.authorization.ExplicitAuthorizations"));
    }


    /**
     *  Gets the RelatedAuthorizationId element Id.
     *
     *  @return the RelatedAuthorizationId element Id
     */

    public static org.osid.id.Id getRelatedAuthorizationId() {
        return (makeQueryElementId("osid.authorization.authorization.RelatedAuthorizationId"));
    }


    /**
     *  Gets the RelatedAuthorization element Id.
     *
     *  @return the RelatedAuthorization element Id
     */

    public static org.osid.id.Id getRelatedAuthorization() {
        return (makeQueryElementId("osid.authorization.authorization.RelatedAuthorization"));
    }


    /**
     *  Gets the VaultId element Id.
     *
     *  @return the VaultId element Id
     */

    public static org.osid.id.Id getVaultId() {
        return (makeQueryElementId("osid.authorization.authorization.VaultId"));
    }


    /**
     *  Gets the Vault element Id.
     *
     *  @return the Vault element Id
     */

    public static org.osid.id.Id getVault() {
        return (makeQueryElementId("osid.authorization.authorization.Vault"));
    }
}
