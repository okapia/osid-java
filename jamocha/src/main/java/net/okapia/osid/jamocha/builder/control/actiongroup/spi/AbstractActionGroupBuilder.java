//
// AbstractActionGroup.java
//
//     Defines an ActionGroup builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.actiongroup.spi;


/**
 *  Defines an <code>ActionGroup</code> builder.
 */

public abstract class AbstractActionGroupBuilder<T extends AbstractActionGroupBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.control.actiongroup.ActionGroupMiter actionGroup;


    /**
     *  Constructs a new <code>AbstractActionGroupBuilder</code>.
     *
     *  @param actionGroup the action group to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractActionGroupBuilder(net.okapia.osid.jamocha.builder.control.actiongroup.ActionGroupMiter actionGroup) {
        super(actionGroup);
        this.actionGroup = actionGroup;
        return;
    }


    /**
     *  Builds the action group.
     *
     *  @return the new action group
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.NullArgumentException <code>actionGroup</code>
     *          is <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.control.ActionGroup build() {
        (new net.okapia.osid.jamocha.builder.validator.control.actiongroup.ActionGroupValidator(getValidations())).validate(this.actionGroup);
        return (new net.okapia.osid.jamocha.builder.control.actiongroup.ImmutableActionGroup(this.actionGroup));
    }


    /**
     *  Gets the action group. This method is used to get the miter
     *  interface for further updates. Use <code>build()</code> to
     *  finalize and validate construction.
     *
     *  @return the new actionGroup
     */

    @Override
    public net.okapia.osid.jamocha.builder.control.actiongroup.ActionGroupMiter getMiter() {
        return (this.actionGroup);
    }


    /**
     *  Adds an action.
     *
     *  @param action an action
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>action</code> is
     *          <code>null</code>
     */

    public T action(org.osid.control.Action action) {
        getMiter().addAction(action);
        return (self());
    }


    /**
     *  Sets all the actions.
     *
     *  @param actions a collection of actions
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>actions</code> is
     *          <code>null</code>
     */

    public T actions(java.util.Collection<org.osid.control.Action> actions) {
        getMiter().setActions(actions);
        return (self());
    }


    /**
     *  Adds an ActionGroup record.
     *
     *  @param record an action group record
     *  @param recordType the type of action group record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.control.records.ActionGroupRecord record, org.osid.type.Type recordType) {
        getMiter().addActionGroupRecord(record, recordType);
        return (self());
    }
}       


