//
// ActivityRegistrationElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activityregistration.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ActivityRegistrationElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the ActivityRegistrationElement Id.
     *
     *  @return the activity registration element Id
     */

    public static org.osid.id.Id getActivityRegistrationEntityId() {
        return (makeEntityId("osid.course.registration.ActivityRegistration"));
    }


    /**
     *  Gets the RegistrationId element Id.
     *
     *  @return the RegistrationId element Id
     */

    public static org.osid.id.Id getRegistrationId() {
        return (makeElementId("osid.course.registration.activityregistration.RegistrationId"));
    }


    /**
     *  Gets the Registration element Id.
     *
     *  @return the Registration element Id
     */

    public static org.osid.id.Id getRegistration() {
        return (makeElementId("osid.course.registration.activityregistration.Registration"));
    }


    /**
     *  Gets the ActivityId element Id.
     *
     *  @return the ActivityId element Id
     */

    public static org.osid.id.Id getActivityId() {
        return (makeElementId("osid.course.registration.activityregistration.ActivityId"));
    }


    /**
     *  Gets the Activity element Id.
     *
     *  @return the Activity element Id
     */

    public static org.osid.id.Id getActivity() {
        return (makeElementId("osid.course.registration.activityregistration.Activity"));
    }


    /**
     *  Gets the StudentId element Id.
     *
     *  @return the StudentId element Id
     */

    public static org.osid.id.Id getStudentId() {
        return (makeElementId("osid.course.registration.activityregistration.StudentId"));
    }


    /**
     *  Gets the Student element Id.
     *
     *  @return the Student element Id
     */

    public static org.osid.id.Id getStudent() {
        return (makeElementId("osid.course.registration.activityregistration.Student"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.registration.activityregistration.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.registration.activityregistration.CourseCatalog"));
    }
}
