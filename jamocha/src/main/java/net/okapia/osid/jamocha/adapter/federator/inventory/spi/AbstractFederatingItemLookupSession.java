//
// AbstractFederatingItemLookupSession.java
//
//     An abstract federating adapter for an ItemLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.inventory.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  ItemLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingItemLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.inventory.ItemLookupSession>
    implements org.osid.inventory.ItemLookupSession {

    private boolean parallel = false;
    private org.osid.inventory.Warehouse warehouse = new net.okapia.osid.jamocha.nil.inventory.warehouse.UnknownWarehouse();


    /**
     *  Constructs a new <code>AbstractFederatingItemLookupSession</code>.
     */

    protected AbstractFederatingItemLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.inventory.ItemLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Warehouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Warehouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getWarehouseId() {
        return (this.warehouse.getId());
    }


    /**
     *  Gets the <code>Warehouse</code> associated with this 
     *  session.
     *
     *  @return the <code>Warehouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Warehouse getWarehouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.warehouse);
    }


    /**
     *  Sets the <code>Warehouse</code>.
     *
     *  @param  warehouse the warehouse for this session
     *  @throws org.osid.NullArgumentException <code>warehouse</code>
     *          is <code>null</code>
     */

    protected void setWarehouse(org.osid.inventory.Warehouse warehouse) {
        nullarg(warehouse, "warehouse");
        this.warehouse = warehouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>Item</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupItems() {
        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            if (session.canLookupItems()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Item</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeItemView() {
        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            session.useComparativeItemView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Item</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryItemView() {
        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            session.usePlenaryItemView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include items in warehouses which are children
     *  of this warehouse in the warehouse hierarchy.
     */

    @OSID @Override
    public void useFederatedWarehouseView() {
        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            session.useFederatedWarehouseView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this warehouse only.
     */

    @OSID @Override
    public void useIsolatedWarehouseView() {
        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            session.useIsolatedWarehouseView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Item</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Item</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Item</code> and
     *  retained for compatibility.
     *
     *  @param  itemId <code>Id</code> of the
     *          <code>Item</code>
     *  @return the item
     *  @throws org.osid.NotFoundException <code>itemId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>itemId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.Item getItem(org.osid.id.Id itemId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            try {
                return (session.getItem(itemId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(itemId + " not found");
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  items specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Items</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  itemIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>itemIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByIds(org.osid.id.IdList itemIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.inventory.item.MutableItemList ret = new net.okapia.osid.jamocha.inventory.item.MutableItemList();

        try (org.osid.id.IdList ids = itemIds) {
            while (ids.hasNext()) {
                ret.addItem(getItem(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> which does not include
     *  items of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();

        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            ret.addItemList(session.getItemsByGenusType(itemGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ItemList</code> corresponding to the given
     *  item genus <code>Type</code> and include any additional
     *  items with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByParentGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();

        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            ret.addItemList(session.getItemsByParentGenusType(itemGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ItemList</code> containing the given
     *  item record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  itemRecordType an item record type 
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>itemRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByRecordType(org.osid.type.Type itemRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();

        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            ret.addItemList(session.getItemsByRecordType(itemRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ItemList</code> for the given stock. In plenary
     *  mode, the returned list contains all known items or an error
     *  results.  Otherwise, the returned list may contain only those
     *  items that are accessible through this session.
     *
     *  @param  stockId a stock <code>Id</code>
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException <code>stockId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByStock(org.osid.id.Id stockId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();

        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            ret.addItemList(session.getItemsByStock(stockId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ItemList</code> for the given property tag. In
     *  plenary mode, the returned list contains all known items or an
     *  error results.  Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  property a property number
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException <code>property</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsByPropertyTag(String property)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();

        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            ret.addItemList(session.getItemsByPropertyTag(property));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ItemList</code> for the given serial number. In
     *  plenary mode, the returned list contains all known items or an
     *  error results. Otherwise, the returned list may contain only
     *  those items that are accessible through this session.
     *
     *  @param  serialNumber a serial number
     *  @return the returned <code>Itemd </code> list
     *  @throws org.osid.NullArgumentException
     *          <code>serialNumber</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemsBySerialNumber(String serialNumber)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();

        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            ret.addItemList(session.getItemsBySerialNumber(serialNumber));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>ItemList</code> immediately contained within the
     *  given item. In plenary mode, the returned list contains all
     *  known items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through this
     *  session.
     *
     *  @param  itemId an item <code>Id</code>
     *  @return the returned <code>Item</code> list
     *  @throws org.osid.NullArgumentException <code>itemId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItemParts(org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();

        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            ret.addItemList(session.getItemParts(itemId));
        }

        ret.noMore();
        return (ret);
    }

        
    /**
     *  Gets all <code>Items</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  items or an error results. Otherwise, the returned list
     *  may contain only those items that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Items</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.inventory.ItemList getItems()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList ret = getItemList();

        for (org.osid.inventory.ItemLookupSession session : getSessions()) {
            ret.addItemList(session.getItems());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.inventory.item.FederatingItemList getItemList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.item.ParallelItemList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.inventory.item.CompositeItemList());
        }
    }
}
