//
// AbstractQueryAcademyLookupSession.java
//
//    An inline adapter that maps an AcademyLookupSession to
//    an AcademyQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AcademyLookupSession to
 *  an AcademyQuerySession.
 */

public abstract class AbstractQueryAcademyLookupSession
    extends net.okapia.osid.jamocha.recognition.spi.AbstractAcademyLookupSession
    implements org.osid.recognition.AcademyLookupSession {

    private final org.osid.recognition.AcademyQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAcademyLookupSession.
     *
     *  @param querySession the underlying academy query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAcademyLookupSession(org.osid.recognition.AcademyQuerySession querySession) {
        nullarg(querySession, "academy query session");
        this.session = querySession;
        return;
    }



    /**
     *  Tests if this user can perform <code>Academy</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAcademies() {
        return (this.session.canSearchAcademies());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }

     
    /**
     *  Gets the <code>Academy</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Academy</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Academy</code> and
     *  retained for compatibility.
     *
     *  @param  academyId <code>Id</code> of the
     *          <code>Academy</code>
     *  @return the academy
     *  @throws org.osid.NotFoundException <code>academyId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>academyId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.Academy getAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AcademyQuery query = getQuery();
        query.matchId(academyId, true);
        org.osid.recognition.AcademyList academies = this.session.getAcademiesByQuery(query);
        if (academies.hasNext()) {
            return (academies.getNextAcademy());
        } 
        
        throw new org.osid.NotFoundException(academyId + " not found");
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  academies specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Academies</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  academyIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>academyIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByIds(org.osid.id.IdList academyIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AcademyQuery query = getQuery();

        try (org.osid.id.IdList ids = academyIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAcademiesByQuery(query));
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  academy genus <code>Type</code> which does not include
     *  academies of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  academyGenusType an academy genus type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByGenusType(org.osid.type.Type academyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AcademyQuery query = getQuery();
        query.matchGenusType(academyGenusType, true);
        return (this.session.getAcademiesByQuery(query));
    }


    /**
     *  Gets an <code>AcademyList</code> corresponding to the given
     *  academy genus <code>Type</code> and include any additional
     *  academies with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  academyGenusType an academy genus type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByParentGenusType(org.osid.type.Type academyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AcademyQuery query = getQuery();
        query.matchParentGenusType(academyGenusType, true);
        return (this.session.getAcademiesByQuery(query));
    }


    /**
     *  Gets an <code>AcademyList</code> containing the given
     *  academy record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  academyRecordType an academy record type 
     *  @return the returned <code>Academy</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>academyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByRecordType(org.osid.type.Type academyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AcademyQuery query = getQuery();
        query.matchRecordType(academyRecordType, true);
        return (this.session.getAcademiesByQuery(query));
    }


    /**
     *  Gets an <code>AcademyList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known academies or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  academies that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Academy</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademiesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AcademyQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getAcademiesByQuery(query));        
    }

    
    /**
     *  Gets all <code>Academies</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  academies or an error results. Otherwise, the returned list
     *  may contain only those academies that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Academies</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.recognition.AcademyList getAcademies()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.recognition.AcademyQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAcademiesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.recognition.AcademyQuery getQuery() {
        org.osid.recognition.AcademyQuery query = this.session.getAcademyQuery();
        return (query);
    }
}
