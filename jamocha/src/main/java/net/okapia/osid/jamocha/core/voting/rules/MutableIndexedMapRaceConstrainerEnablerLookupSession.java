//
// MutableIndexedMapRaceConstrainerEnablerLookupSession
//
//    Implements a RaceConstrainerEnabler lookup service backed by a collection of
//    raceConstrainerEnablers indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.voting.rules;


/**
 *  Implements a RaceConstrainerEnabler lookup service backed by a collection of
 *  race constrainer enablers. The race constrainer enablers are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some race constrainer enablers may be compatible
 *  with more types than are indicated through these race constrainer enabler
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of race constrainer enablers can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRaceConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.core.voting.rules.spi.AbstractIndexedMapRaceConstrainerEnablerLookupSession
    implements org.osid.voting.rules.RaceConstrainerEnablerLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapRaceConstrainerEnablerLookupSession} with no race constrainer enablers.
     *
     *  @param polls the polls
     *  @throws org.osid.NullArgumentException {@code polls}
     *          is {@code null}
     */

      public MutableIndexedMapRaceConstrainerEnablerLookupSession(org.osid.voting.Polls polls) {
        setPolls(polls);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceConstrainerEnablerLookupSession} with a
     *  single race constrainer enabler.
     *  
     *  @param polls the polls
     *  @param  raceConstrainerEnabler a single raceConstrainerEnabler
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceConstrainerEnabler} is {@code null}
     */

    public MutableIndexedMapRaceConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler) {
        this(polls);
        putRaceConstrainerEnabler(raceConstrainerEnabler);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceConstrainerEnablerLookupSession} using an
     *  array of race constrainer enablers.
     *
     *  @param polls the polls
     *  @param  raceConstrainerEnablers an array of race constrainer enablers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceConstrainerEnablers} is {@code null}
     */

    public MutableIndexedMapRaceConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  org.osid.voting.rules.RaceConstrainerEnabler[] raceConstrainerEnablers) {
        this(polls);
        putRaceConstrainerEnablers(raceConstrainerEnablers);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRaceConstrainerEnablerLookupSession} using a
     *  collection of race constrainer enablers.
     *
     *  @param polls the polls
     *  @param  raceConstrainerEnablers a collection of race constrainer enablers
     *  @throws org.osid.NullArgumentException {@code polls} or
     *          {@code raceConstrainerEnablers} is {@code null}
     */

    public MutableIndexedMapRaceConstrainerEnablerLookupSession(org.osid.voting.Polls polls,
                                                  java.util.Collection<? extends org.osid.voting.rules.RaceConstrainerEnabler> raceConstrainerEnablers) {

        this(polls);
        putRaceConstrainerEnablers(raceConstrainerEnablers);
        return;
    }
    

    /**
     *  Makes a {@code RaceConstrainerEnabler} available in this session.
     *
     *  @param  raceConstrainerEnabler a race constrainer enabler
     *  @throws org.osid.NullArgumentException {@code raceConstrainerEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putRaceConstrainerEnabler(org.osid.voting.rules.RaceConstrainerEnabler raceConstrainerEnabler) {
        super.putRaceConstrainerEnabler(raceConstrainerEnabler);
        return;
    }


    /**
     *  Makes an array of race constrainer enablers available in this session.
     *
     *  @param  raceConstrainerEnablers an array of race constrainer enablers
     *  @throws org.osid.NullArgumentException {@code raceConstrainerEnablers{@code 
     *          is {@code null}
     */

    @Override
    public void putRaceConstrainerEnablers(org.osid.voting.rules.RaceConstrainerEnabler[] raceConstrainerEnablers) {
        super.putRaceConstrainerEnablers(raceConstrainerEnablers);
        return;
    }


    /**
     *  Makes collection of race constrainer enablers available in this session.
     *
     *  @param  raceConstrainerEnablers a collection of race constrainer enablers
     *  @throws org.osid.NullArgumentException {@code raceConstrainerEnabler{@code  is
     *          {@code null}
     */

    @Override
    public void putRaceConstrainerEnablers(java.util.Collection<? extends org.osid.voting.rules.RaceConstrainerEnabler> raceConstrainerEnablers) {
        super.putRaceConstrainerEnablers(raceConstrainerEnablers);
        return;
    }


    /**
     *  Removes a RaceConstrainerEnabler from this session.
     *
     *  @param raceConstrainerEnablerId the {@code Id} of the race constrainer enabler
     *  @throws org.osid.NullArgumentException {@code raceConstrainerEnablerId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRaceConstrainerEnabler(org.osid.id.Id raceConstrainerEnablerId) {
        super.removeRaceConstrainerEnabler(raceConstrainerEnablerId);
        return;
    }    
}
