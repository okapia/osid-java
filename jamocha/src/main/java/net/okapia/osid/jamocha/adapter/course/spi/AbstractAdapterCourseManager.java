//
// AbstractCourseManager.java
//
//     An adapter for a CourseManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a CourseManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterCourseManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.course.CourseManager>
    implements org.osid.course.CourseManager {


    /**
     *  Constructs a new {@code AbstractAdapterCourseManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterCourseManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterCourseManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterCourseManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up courses is supported. 
     *
     *  @return <code> true </code> if course lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseLookup() {
        return (getAdapteeManager().supportsCourseLookup());
    }


    /**
     *  Tests if querying courses is supported. 
     *
     *  @return <code> true </code> if course query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseQuery() {
        return (getAdapteeManager().supportsCourseQuery());
    }


    /**
     *  Tests if searching courses is supported. 
     *
     *  @return <code> true </code> if course search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSearch() {
        return (getAdapteeManager().supportsCourseSearch());
    }


    /**
     *  Tests if course <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if course administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseAdmin() {
        return (getAdapteeManager().supportsCourseAdmin());
    }


    /**
     *  Tests if a course <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if course notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseNotification() {
        return (getAdapteeManager().supportsCourseNotification());
    }


    /**
     *  Tests if a course cataloging service is supported. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCourseCatalog() {
        return (getAdapteeManager().supportsCourseCourseCatalog());
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps courses to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCourseCatalogAssignment() {
        return (getAdapteeManager().supportsCourseCourseCatalogAssignment());
    }


    /**
     *  Tests if a course smart course catalog session is available. 
     *
     *  @return <code> true </code> if a course smart course catalog session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSmartCourseCatalog() {
        return (getAdapteeManager().supportsCourseSmartCourseCatalog());
    }


    /**
     *  Tests if looking up activity units is supported. 
     *
     *  @return <code> true </code> if activity unit lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitLookup() {
        return (getAdapteeManager().supportsActivityUnitLookup());
    }


    /**
     *  Tests if querying activity units is supported. 
     *
     *  @return <code> true </code> if activity unit query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (getAdapteeManager().supportsActivityUnitQuery());
    }


    /**
     *  Tests if searching activity units is supported. 
     *
     *  @return <code> true </code> if activity unit search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitSearch() {
        return (getAdapteeManager().supportsActivityUnitSearch());
    }


    /**
     *  Tests if an activity unit <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity unit administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitAdmin() {
        return (getAdapteeManager().supportsActivityUnitAdmin());
    }


    /**
     *  Tests if an activity unit <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if activity unit notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitNotification() {
        return (getAdapteeManager().supportsActivityUnitNotification());
    }


    /**
     *  Tests if an activity unit cataloging service is supported. 
     *
     *  @return <code> true </code> if activity unit catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitCourseCatalog() {
        return (getAdapteeManager().supportsActivityUnitCourseCatalog());
    }


    /**
     *  Tests if an activity unit cataloging service is supported. A 
     *  cataloging service maps activity units to catalogs. 
     *
     *  @return <code> true </code> if activity unit cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitCourseCatalogAssignment() {
        return (getAdapteeManager().supportsActivityUnitCourseCatalogAssignment());
    }


    /**
     *  Tests if an activity unit smart course catalog session is available. 
     *
     *  @return <code> true </code> if an activity unit smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitSmartCourseCatalog() {
        return (getAdapteeManager().supportsActivityUnitSmartCourseCatalog());
    }


    /**
     *  Tests if looking up course offerings is supported. 
     *
     *  @return <code> true </code> if course offering lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingLookup() {
        return (getAdapteeManager().supportsCourseOfferingLookup());
    }


    /**
     *  Tests if querying course offerings is supported. 
     *
     *  @return <code> true </code> if course offering query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (getAdapteeManager().supportsCourseOfferingQuery());
    }


    /**
     *  Tests if searching course offerings is supported. 
     *
     *  @return <code> true </code> if course offering search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingSearch() {
        return (getAdapteeManager().supportsCourseOfferingSearch());
    }


    /**
     *  Tests if course <code> </code> offering <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if course offering administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingAdmin() {
        return (getAdapteeManager().supportsCourseOfferingAdmin());
    }


    /**
     *  Tests if a course offering <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if course offering notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingNotification() {
        return (getAdapteeManager().supportsCourseOfferingNotification());
    }


    /**
     *  Tests if a course offering cataloging service is supported. 
     *
     *  @return <code> true </code> if course offering catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingCourseCatalog() {
        return (getAdapteeManager().supportsCourseOfferingCourseCatalog());
    }


    /**
     *  Tests if a course offering cataloging service is supported. A 
     *  cataloging service maps course offerings to catalogs. 
     *
     *  @return <code> true </code> if course offering cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingCourseCatalogAssignment() {
        return (getAdapteeManager().supportsCourseOfferingCourseCatalogAssignment());
    }


    /**
     *  Tests if a course offering smart course catalog session is available. 
     *
     *  @return <code> true </code> if a course offering smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingSmartCourseCatalog() {
        return (getAdapteeManager().supportsCourseOfferingSmartCourseCatalog());
    }


    /**
     *  Tests if looking up activities is supported. 
     *
     *  @return <code> true </code> if activity lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityLookup() {
        return (getAdapteeManager().supportsActivityLookup());
    }


    /**
     *  Tests if querying activities is supported. 
     *
     *  @return <code> true </code> if activity query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityQuery() {
        return (getAdapteeManager().supportsActivityQuery());
    }


    /**
     *  Tests if searching activities is supported. 
     *
     *  @return <code> true </code> if activity search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySearch() {
        return (getAdapteeManager().supportsActivitySearch());
    }


    /**
     *  Tests if activity administrative service is supported. 
     *
     *  @return <code> true </code> if activity administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityAdmin() {
        return (getAdapteeManager().supportsActivityAdmin());
    }


    /**
     *  Tests if an activity <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if activity notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityNotification() {
        return (getAdapteeManager().supportsActivityNotification());
    }


    /**
     *  Tests if an activity cataloging service is supported. 
     *
     *  @return <code> true </code> if activity catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityCourseCatalog() {
        return (getAdapteeManager().supportsActivityCourseCatalog());
    }


    /**
     *  Tests if an activity cataloging service is supported. A cataloging 
     *  service maps activities to catalogs. 
     *
     *  @return <code> true </code> if activity cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityCourseCatalogAssignment() {
        return (getAdapteeManager().supportsActivityCourseCatalogAssignment());
    }


    /**
     *  Tests if an activity smart course catalog session is available. 
     *
     *  @return <code> true </code> if an activity smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivitySmartCourseCatalog() {
        return (getAdapteeManager().supportsActivitySmartCourseCatalog());
    }


    /**
     *  Tests if unravelling activities is supported. 
     *
     *  @return <code> true </code> if unravelling activities is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnravelling() {
        return (getAdapteeManager().supportsActivityUnravelling());
    }


    /**
     *  Tests if looking up terms is supported. 
     *
     *  @return <code> true </code> if term lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermLookup() {
        return (getAdapteeManager().supportsTermLookup());
    }


    /**
     *  Tests if querying terms is supported. 
     *
     *  @return <code> true </code> if term query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (getAdapteeManager().supportsTermQuery());
    }


    /**
     *  Tests if searching terms is supported. 
     *
     *  @return <code> true </code> if term search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSearch() {
        return (getAdapteeManager().supportsTermSearch());
    }


    /**
     *  Tests if term <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if term administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermAdmin() {
        return (getAdapteeManager().supportsTermAdmin());
    }


    /**
     *  Tests if a term <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if term notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermNotification() {
        return (getAdapteeManager().supportsTermNotification());
    }


    /**
     *  Tests if term <code> </code> hierarchy traversal service is supported. 
     *
     *  @return <code> true </code> if term hierarchy is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermHierarchy() {
        return (getAdapteeManager().supportsTermHierarchy());
    }


    /**
     *  Tests if a term <code> </code> hierarchy design service is supported. 
     *
     *  @return <code> true </code> if term hierarchy design is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermHierarchyDesign() {
        return (getAdapteeManager().supportsTermHierarchyDesign());
    }


    /**
     *  Tests if a term cataloging service is supported. 
     *
     *  @return <code> true </code> if term catalog is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermCourseCatalog() {
        return (getAdapteeManager().supportsTermCourseCatalog());
    }


    /**
     *  Tests if a term cataloging service is supported. A cataloging service 
     *  maps terms to catalogs. 
     *
     *  @return <code> true </code> if term cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermCourseCatalogAssignment() {
        return (getAdapteeManager().supportsTermCourseCatalogAssignment());
    }


    /**
     *  Tests if a term smart course catalog session is available. 
     *
     *  @return <code> true </code> if a term smart course catalog session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermSmartCourseCatalog() {
        return (getAdapteeManager().supportsTermSmartCourseCatalog());
    }


    /**
     *  Tests if looking up course catalogs is supported. 
     *
     *  @return <code> true </code> if course catalog lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogLookup() {
        return (getAdapteeManager().supportsCourseCatalogLookup());
    }


    /**
     *  Tests if searching course catalogs is supported. 
     *
     *  @return <code> true </code> if course catalog search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogSearch() {
        return (getAdapteeManager().supportsCourseCatalogSearch());
    }


    /**
     *  Tests if querying course catalogs is supported. 
     *
     *  @return <code> true </code> if course catalog query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (getAdapteeManager().supportsCourseCatalogQuery());
    }


    /**
     *  Tests if course catalog administrative service is supported. 
     *
     *  @return <code> true </code> if course catalog administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogAdmin() {
        return (getAdapteeManager().supportsCourseCatalogAdmin());
    }


    /**
     *  Tests if a course catalog <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if course catalog notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogNotification() {
        return (getAdapteeManager().supportsCourseCatalogNotification());
    }


    /**
     *  Tests for the availability of a course catalog hierarchy traversal 
     *  service. 
     *
     *  @return <code> true </code> if course catalog hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogHierarchy() {
        return (getAdapteeManager().supportsCourseCatalogHierarchy());
    }


    /**
     *  Tests for the availability of a course catalog hierarchy design 
     *  service. 
     *
     *  @return <code> true </code> if course catalog hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogHierarchyDesign() {
        return (getAdapteeManager().supportsCourseCatalogHierarchyDesign());
    }


    /**
     *  Tests for the availability of a course batch service. 
     *
     *  @return <code> true </code> if a course batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseBatch() {
        return (getAdapteeManager().supportsCourseBatch());
    }


    /**
     *  Tests for the availability of a course program service. 
     *
     *  @return <code> true </code> if a course program service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProgram() {
        return (getAdapteeManager().supportsCourseProgram());
    }


    /**
     *  Tests for the availability of a course registration service. 
     *
     *  @return <code> true </code> if a course registration service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseRegistration() {
        return (getAdapteeManager().supportsCourseRegistration());
    }


    /**
     *  Tests for the availability of a course requisite service. 
     *
     *  @return <code> true </code> if a course requisite service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseRequisite() {
        return (getAdapteeManager().supportsCourseRequisite());
    }


    /**
     *  Tests for the availability of a course syllabus service. 
     *
     *  @return <code> true </code> if a course syllabus service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseSyllabus() {
        return (getAdapteeManager().supportsCourseSyllabus());
    }


    /**
     *  Tests for the availability of a course plan service. 
     *
     *  @return <code> true </code> if a course plan service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCoursePlan() {
        return (getAdapteeManager().supportsCoursePlan());
    }


    /**
     *  Tests for the availability of a course chronicle service. 
     *
     *  @return <code> true </code> if a course chronicle service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseChronicle() {
        return (getAdapteeManager().supportsCourseChronicle());
    }


    /**
     *  Gets the supported <code> Course </code> record types. 
     *
     *  @return a list containing the supported <code> Course </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseRecordTypes() {
        return (getAdapteeManager().getCourseRecordTypes());
    }


    /**
     *  Tests if the given <code> Course </code> record type is supported. 
     *
     *  @param  courseRecordType a <code> Type </code> indicating a <code> 
     *          Course </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseRecordType(org.osid.type.Type courseRecordType) {
        return (getAdapteeManager().supportsCourseRecordType(courseRecordType));
    }


    /**
     *  Gets the supported <code> Course </code> search record types. 
     *
     *  @return a list containing the supported <code> Course </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseSearchRecordTypes() {
        return (getAdapteeManager().getCourseSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Course </code> search record type is 
     *  supported. 
     *
     *  @param  courseSearchRecordType a <code> Type </code> indicating a 
     *          <code> Course </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseSearchRecordType(org.osid.type.Type courseSearchRecordType) {
        return (getAdapteeManager().supportsCourseSearchRecordType(courseSearchRecordType));
    }


    /**
     *  Gets the supported <code> ActivityUnit </code> record types. 
     *
     *  @return a list containing the supported <code> ActivityUnit </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityUnitRecordTypes() {
        return (getAdapteeManager().getActivityUnitRecordTypes());
    }


    /**
     *  Tests if the given <code> ActivityUnit </code> record type is 
     *  supported. 
     *
     *  @param  activityUnitRecordType a <code> Type </code> indicating an 
     *          <code> ActivityUnit </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityUnitRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityUnitRecordType(org.osid.type.Type activityUnitRecordType) {
        return (getAdapteeManager().supportsActivityUnitRecordType(activityUnitRecordType));
    }


    /**
     *  Gets the supported <code> ActivityUnit </code> search record types. 
     *
     *  @return a list containing the supported <code> ActivityUnit </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityUnitSearchRecordTypes() {
        return (getAdapteeManager().getActivityUnitSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ActivityUnit </code> search record type is 
     *  supported. 
     *
     *  @param  activityUnitSearchRecordType a <code> Type </code> indicating 
     *          an <code> ActivityUnit </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          activityUnitSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityUnitSearchRecordType(org.osid.type.Type activityUnitSearchRecordType) {
        return (getAdapteeManager().supportsActivityUnitSearchRecordType(activityUnitSearchRecordType));
    }


    /**
     *  Gets the supported <code> CourseOffering </code> record types. 
     *
     *  @return a list containing the supported <code> CourseOffering </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseOfferingRecordTypes() {
        return (getAdapteeManager().getCourseOfferingRecordTypes());
    }


    /**
     *  Tests if the given <code> CourseOffering </code> record type is 
     *  supported. 
     *
     *  @param  courseOfferingRecordType a <code> Type </code> indicating an 
     *          <code> CourseOffering </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseOfferingRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseOfferingRecordType(org.osid.type.Type courseOfferingRecordType) {
        return (getAdapteeManager().supportsCourseOfferingRecordType(courseOfferingRecordType));
    }


    /**
     *  Gets the supported <code> CourseOffering </code> search record types. 
     *
     *  @return a list containing the supported <code> CourseOffering </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseOfferingSearchRecordTypes() {
        return (getAdapteeManager().getCourseOfferingSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CourseOffering </code> search record type is 
     *  supported. 
     *
     *  @param  courseOfferingSearchRecordType a <code> Type </code> 
     *          indicating an <code> CourseOffering </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          courseOfferingSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseOfferingSearchRecordType(org.osid.type.Type courseOfferingSearchRecordType) {
        return (getAdapteeManager().supportsCourseOfferingSearchRecordType(courseOfferingSearchRecordType));
    }


    /**
     *  Gets the supported <code> Activity </code> record types. 
     *
     *  @return a list containing the supported <code> Activity </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivityRecordTypes() {
        return (getAdapteeManager().getActivityRecordTypes());
    }


    /**
     *  Tests if the given <code> Activity </code> record type is supported. 
     *
     *  @param  activityRecordType a <code> Type </code> indicating an <code> 
     *          Activity </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activityRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivityRecordType(org.osid.type.Type activityRecordType) {
        return (getAdapteeManager().supportsActivityRecordType(activityRecordType));
    }


    /**
     *  Gets the supported <code> Activity </code> search record types. 
     *
     *  @return a list containing the supported <code> Activity </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActivitySearchRecordTypes() {
        return (getAdapteeManager().getActivitySearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Activity </code> search record type is 
     *  supported. 
     *
     *  @param  activitySearchRecordType a <code> Type </code> indicating an 
     *          <code> Activity </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> activitySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActivitySearchRecordType(org.osid.type.Type activitySearchRecordType) {
        return (getAdapteeManager().supportsActivitySearchRecordType(activitySearchRecordType));
    }


    /**
     *  Gets the supported <code> Term </code> record types. 
     *
     *  @return a list containing the supported <code> Term </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTermRecordTypes() {
        return (getAdapteeManager().getTermRecordTypes());
    }


    /**
     *  Tests if the given <code> Term </code> record type is supported. 
     *
     *  @param  termRecordType a <code> Type </code> indicating a <code> Term 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> termRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTermRecordType(org.osid.type.Type termRecordType) {
        return (getAdapteeManager().supportsTermRecordType(termRecordType));
    }


    /**
     *  Gets the supported <code> Term </code> search record types. 
     *
     *  @return a list containing the supported <code> Term </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTermSearchRecordTypes() {
        return (getAdapteeManager().getTermSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Term </code> search record type is 
     *  supported. 
     *
     *  @param  termSearchRecordType a <code> Type </code> indicating a <code> 
     *          Term </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> termSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTermSearchRecordType(org.osid.type.Type termSearchRecordType) {
        return (getAdapteeManager().supportsTermSearchRecordType(termSearchRecordType));
    }


    /**
     *  Gets the supported <code> CourseCatalog </code> record types. 
     *
     *  @return a list containing the supported <code> CourseCatalog </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseCatalogRecordTypes() {
        return (getAdapteeManager().getCourseCatalogRecordTypes());
    }


    /**
     *  Tests if the given <code> CourseCatalog </code> record type is 
     *  supported. 
     *
     *  @param  courseCatalogRecordType a <code> Type </code> indicating an 
     *          <code> CourseCatalog </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> courseCatalogRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseCatalogRecordType(org.osid.type.Type courseCatalogRecordType) {
        return (getAdapteeManager().supportsCourseCatalogRecordType(courseCatalogRecordType));
    }


    /**
     *  Gets the supported <code> CourseCatalog </code> search record types. 
     *
     *  @return a list containing the supported <code> CourseCatalog </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCourseCatalogSearchRecordTypes() {
        return (getAdapteeManager().getCourseCatalogSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> CourseCatalog </code> search record type is 
     *  supported. 
     *
     *  @param  courseCatalogSearchRecordType a <code> Type </code> indicating 
     *          an <code> CourseCatalog </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          courseCatalogSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCourseCatalogSearchRecordType(org.osid.type.Type courseCatalogSearchRecordType) {
        return (getAdapteeManager().supportsCourseCatalogSearchRecordType(courseCatalogSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course lookup 
     *  service. 
     *
     *  @return a <code> CourseLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseLookupSession getCourseLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CourseLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseLookupSession getCourseLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course query 
     *  service. 
     *
     *  @return a <code> CourseQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuerySession getCourseQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseQuerySession getCourseQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course search 
     *  service. 
     *
     *  @return a <code> CourseSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchSession getCourseSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSearchSession getCourseSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  administration service. 
     *
     *  @return a <code> CourseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseAdminSession getCourseAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseAdminSession getCourseAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service. 
     *
     *  @param  courseReceiver the notification callback 
     *  @return a <code> CourseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseNotificationSession getCourseNotificationSession(org.osid.course.CourseReceiver courseReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseNotificationSession(courseReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  courseReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseNotificationSession getCourseNotificationSessionForCourseCatalog(org.osid.course.CourseReceiver courseReceiver, 
                                                                                                  org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseNotificationSessionForCourseCatalog(courseReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup course/catalog mappings. 
     *
     *  @return a <code> CourseCourseCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCourseCatalogSession getCourseCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning courses 
     *  to course catalogs. 
     *
     *  @return a <code> CourseCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCourseCatalogAssignmentSession getCourseCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseSmartCourseCatalogSession getCourseSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  lookup service. 
     *
     *  @return an <code> ActivityUnitSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitLookupSession getActivityUnitLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> ActivityUnitLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitLookupSession getActivityUnitLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  query service. 
     *
     *  @return an <code> ActivityUnitQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuerySession getActivityUnitQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuerySession getActivityUnitQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  search service. 
     *
     *  @return an <code> ActivityUnitSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSearchSession getActivityUnitSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSearchSession getActivityUnitSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  administration service. 
     *
     *  @return an <code> ActivityUnitAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitAdminSession getActivityUnitAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitAdminSession getActivityUnitAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  notification service. 
     *
     *  @param  activityUnitReceiver the notification callback 
     *  @return an <code> ActivityUnitNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityUnitReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitNotificationSession getActivityUnitNotificationSession(org.osid.course.ActivityUnitReceiver activityUnitReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitNotificationSession(activityUnitReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  notification service for the given course catalog. 
     *
     *  @param  activityUnitReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityUnitReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitNotificationSession getActivityUnitNotificationSessionForCourseCatalog(org.osid.course.ActivityUnitReceiver activityUnitReceiver, 
                                                                                                              org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitNotificationSessionForCourseCatalog(activityUnitReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity unit/catalog 
     *  mappings. 
     *
     *  @return an <code> ActivityUnitCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitCourseCatalogSession getActivityUnitCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning activity 
     *  units to course catalogs. 
     *
     *  @return an <code> ActivityUnitCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitCourseCatalogAssignmentSession getActivityUnitCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity unit 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityUnitSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitSmartCourseCatalogSession getActivityUnitSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnitSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering lookup service. 
     *
     *  @return a <code> CourseOfferingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingLookupSession getCourseOfferingLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CourseOfferingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingLookupSession getCourseOfferingLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering query service. 
     *
     *  @return a <code> CourseOfferingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuerySession getCourseOfferingQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuerySession getCourseOfferingQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering search service. 
     *
     *  @return a <code> CourseOfferingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSearchSession getCourseOfferingSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSearchSession getCourseOfferingSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering administration service. 
     *
     *  @return a <code> CourseOfferingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingAdminSession getCourseOfferingAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingAdminSession getCourseOfferingAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering notification service. 
     *
     *  @param  courseOfferingReceiver the notification callback 
     *  @return a <code> CourseOfferingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseOfferingReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingNotificationSession getCourseOfferingNotificationSession(org.osid.course.CourseOfferingReceiver courseOfferingReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingNotificationSession(courseOfferingReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering notification service for the given course catalog. 
     *
     *  @param  courseOfferingReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseOfferingReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingNotificationSession getCourseOfferingNotificationSessionForCourseCatalog(org.osid.course.CourseOfferingReceiver courseOfferingReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingNotificationSessionForCourseCatalog(courseOfferingReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup course offering/catalog 
     *  mappings. 
     *
     *  @return a <code> CourseOfferingCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingCourseCatalogSession getCourseOfferingCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning course 
     *  offerings to course catalogs. 
     *
     *  @return a <code> CourseOfferingCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingCourseCatalogAssignmentSession getCourseOfferingCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  offering smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CourseOfferingSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingSmartCourseCatalogSession getCourseOfferingSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseOfferingSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service. 
     *
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityLookupSession getActivityLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityLookupSession getActivityLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service. 
     *
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuerySession getActivityQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuerySession getActivityQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service. 
     *
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchSession getActivitySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivitySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivitySearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySearchSession getActivitySearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivitySearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service. 
     *
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityAdminSession getActivityAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsActivityAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityAdminSession getActivityAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service. 
     *
     *  @param  activityReceiver the notification callback 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityNotificationSession getActivityNotificationSession(org.osid.course.ActivityReceiver activityReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityNotificationSession(activityReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity 
     *  notification service for the given course catalog. 
     *
     *  @param  activityReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivityNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> activityReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityNotificationSession getActivityNotificationSessionForCourseCatalog(org.osid.course.ActivityReceiver activityReceiver, 
                                                                                                      org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityNotificationSessionForCourseCatalog(activityReceiver, courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup activity/catalog 
     *  mappings. 
     *
     *  @return an <code> ActivityCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityCourseCatalogSession getActivityCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  activities to course catalogs. 
     *
     *  @return an <code> ActivityCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityCourseCatalogAssignmentSession getActivityCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the activity smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> ActivitySmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivitySmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivitySmartCourseCatalogSession getActivitySmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivitySmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with unravelling 
     *  activities into meeting times. 
     *
     *  @return an <code> ActivityUnravellingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnravelling() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnravellingSession getActivityUnravellingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnravellingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with unravelling 
     *  activities into meeting times for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> ActivityUnravellingSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnravelling() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnravellingSession getActivityUnravellingSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActivityUnravellingSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term lookup 
     *  service. 
     *
     *  @return a <code> TermLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermLookupSession getTermLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermLookupSession getTermLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTermLookupSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term query 
     *  service. 
     *
     *  @return a <code> TermQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuerySession getTermQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuerySession getTermQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTermQuerySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term search 
     *  service. 
     *
     *  @return a <code> TermSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchSession getTermSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSearchSession getTermSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTermSearchSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  administration service. 
     *
     *  @return a <code> TermAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermAdminSession getTermAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermAdminSession getTermAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTermAdminSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  notification service. 
     *
     *  @param  termReceiver the notification callback 
     *  @return a <code> TermNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> termReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermNotificationSession getTermNotificationSession(org.osid.course.TermReceiver termReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermNotificationSession(termReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term 
     *  notification service for the given course catalog. 
     *
     *  @param  termReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> termReceiver </code> or 
     *          <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermNotificationSession getTermNotificationSessionForCourseCatalog(org.osid.course.TermReceiver termReceiver, 
                                                                                              org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTermNotificationSessionForCourseCatalog(termReceiver, courseCatalogId));
    }


    /**
     *  Gets the term hierarchy traversal session. 
     *
     *  @return a <code> TermHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchySession getTermHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermHierarchySession());
    }


    /**
     *  Gets the term hierarchy traversal session for the given course 
     *  catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermHierarchySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTermHierarchy() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchySession getTermHierarchySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTermHierarchySessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the term hierarchy design session. 
     *
     *  @return a <code> TermHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchyDesignSession getTermHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermHierarchyDesignSession());
    }


    /**
     *  Gets the term hierarchy design session for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermHierarchyDesignSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermHierarchyDesign() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermHierarchyDesignSession getTermHierarchyDesignSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTermHierarchyDesignSessionForCourseCatalog(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup term/catalog mappings. 
     *
     *  @return a <code> TermCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermCourseCatalogSession getTermCourseCatalogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermCourseCatalogSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning terms to 
     *  course catalogs. 
     *
     *  @return a <code> TermCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermCourseCatalogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.TermCourseCatalogAssignmentSession getTermCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTermCourseCatalogAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the term smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> TermSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTermSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermSmartCourseCatalogSession getTermSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTermSmartCourseCatalogSession(courseCatalogId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  lookup service. 
     *
     *  @return a <code> CourseCatalogLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogLookupSession getCourseCatalogLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCatalogLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  query service. 
     *
     *  @return a <code> CourseCatalogQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuerySession getCourseCatalogQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCatalogQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  search service. 
     *
     *  @return a <code> CourseCatalogSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogSearchSession getCourseCatalogSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCatalogSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  administrative service. 
     *
     *  @return a <code> CourseCatalogAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogAdminSession getCourseCatalogAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCatalogAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  notification service. 
     *
     *  @param  courseCatalogReceiver the notification callback 
     *  @return a <code> CourseCatalogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogNotificationSession getCourseCatalogNotificationSession(org.osid.course.CourseCatalogReceiver courseCatalogReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCatalogNotificationSession(courseCatalogReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  hierarchy service. 
     *
     *  @return a <code> CourseCatalogHierarchySession </code> for course 
     *          catalogs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogHierarchy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogHierarchySession getCourseCatalogHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCatalogHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course catalog 
     *  hierarchy design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for course catalogs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogHierarchyDesignSession getCourseCatalogHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseCatalogHierarchyDesignSession());
    }


    /**
     *  Gets a <code> CourseBatchManager. </code> 
     *
     *  @return a <code> CourseBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.batch.CourseBatchManager getCourseBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseBatchManager());
    }


    /**
     *  Gets a <code> CourseProgramManager. </code> 
     *
     *  @return a <code> CourseProgramManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseProgram() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramManager getCourseProgramManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseProgramManager());
    }


    /**
     *  Gets a <code> CourseRegistrationManager. </code> 
     *
     *  @return a <code> CourseRegistrationManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseRegistration() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.registration.CourseRegistrationManager getCourseRegistrationManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseRegistrationManager());
    }


    /**
     *  Gets a <code> CourseRequisiteManager. </code> 
     *
     *  @return a <code> CourseRequisiteManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseRequisite() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.requisite.CourseRequisiteManager getCourseRequisiteManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseRequisiteManager());
    }


    /**
     *  Gets a <code> CourseSyllabusManager. </code> 
     *
     *  @return a <code> CourseSyllabusManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseSyllabus() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.syllabus.CourseSyllabusManager getCourseSyllabusManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseSyllabusManager());
    }


    /**
     *  Gets a <code> CoursePlanManager. </code> 
     *
     *  @return a <code> CoursePlanManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCoursePlan() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.plan.CoursePlanManager getCoursePlanManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCoursePlanManager());
    }


    /**
     *  Gets a <code> CourseChronicleManager. </code> 
     *
     *  @return a <code> CourseChronicleManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseChronicle() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseChronicleManager getCourseChronicleManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCourseChronicleManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
