//
// MutableMapKeyLookupSession
//
//    Implements a Key lookup service backed by a collection of
//    keys that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authentication.keys;


/**
 *  Implements a Key lookup service backed by a collection of
 *  keys. The keys are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of keys can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapKeyLookupSession
    extends net.okapia.osid.jamocha.core.authentication.keys.spi.AbstractMapKeyLookupSession
    implements org.osid.authentication.keys.KeyLookupSession {


    /**
     *  Constructs a new {@code MutableMapKeyLookupSession}
     *  with no keys.
     *
     *  @param agency the agency
     *  @throws org.osid.NullArgumentException {@code agency} is
     *          {@code null}
     */

      public MutableMapKeyLookupSession(org.osid.authentication.Agency agency) {
        setAgency(agency);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapKeyLookupSession} with a
     *  single key.
     *
     *  @param agency the agency  
     *  @param key a key
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code key} is {@code null}
     */

    public MutableMapKeyLookupSession(org.osid.authentication.Agency agency,
                                           org.osid.authentication.keys.Key key) {
        this(agency);
        putKey(key);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapKeyLookupSession}
     *  using an array of keys.
     *
     *  @param agency the agency
     *  @param keys an array of keys
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code keys} is {@code null}
     */

    public MutableMapKeyLookupSession(org.osid.authentication.Agency agency,
                                           org.osid.authentication.keys.Key[] keys) {
        this(agency);
        putKeys(keys);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapKeyLookupSession}
     *  using a collection of keys.
     *
     *  @param agency the agency
     *  @param keys a collection of keys
     *  @throws org.osid.NullArgumentException {@code agency} or
     *          {@code keys} is {@code null}
     */

    public MutableMapKeyLookupSession(org.osid.authentication.Agency agency,
                                           java.util.Collection<? extends org.osid.authentication.keys.Key> keys) {

        this(agency);
        putKeys(keys);
        return;
    }

    
    /**
     *  Makes a {@code Key} available in this session.
     *
     *  @param key a key
     *  @throws org.osid.NullArgumentException {@code key{@code  is
     *          {@code null}
     */

    @Override
    public void putKey(org.osid.authentication.keys.Key key) {
        super.putKey(key);
        return;
    }


    /**
     *  Makes an array of keys available in this session.
     *
     *  @param keys an array of keys
     *  @throws org.osid.NullArgumentException {@code keys{@code 
     *          is {@code null}
     */

    @Override
    public void putKeys(org.osid.authentication.keys.Key[] keys) {
        super.putKeys(keys);
        return;
    }


    /**
     *  Makes collection of keys available in this session.
     *
     *  @param keys a collection of keys
     *  @throws org.osid.NullArgumentException {@code keys{@code  is
     *          {@code null}
     */

    @Override
    public void putKeys(java.util.Collection<? extends org.osid.authentication.keys.Key> keys) {
        super.putKeys(keys);
        return;
    }


    /**
     *  Removes a Key from this session.
     *
     *  @param keyId the {@code Id} of the key
     *  @throws org.osid.NullArgumentException {@code keyId{@code 
     *          is {@code null}
     */

    @Override
    public void removeKey(org.osid.id.Id keyId) {
        super.removeKey(keyId);
        return;
    }    
}
