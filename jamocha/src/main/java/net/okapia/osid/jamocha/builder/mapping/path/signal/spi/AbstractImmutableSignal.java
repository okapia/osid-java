//
// AbstractImmutableSignal.java
//
//     Wraps a mutable Signal to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.path.signal.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Signal</code> to hide modifiers. This
 *  wrapper provides an immutized Signal from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying signal whose state changes are visible.
 */

public abstract class AbstractImmutableSignal
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.mapping.path.Signal {

    private final org.osid.mapping.path.Signal signal;


    /**
     *  Constructs a new <code>AbstractImmutableSignal</code>.
     *
     *  @param signal the signal to immutablize
     *  @throws org.osid.NullArgumentException <code>signal</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableSignal(org.osid.mapping.path.Signal signal) {
        super(signal);
        this.signal = signal;
        return;
    }


    /**
     *  Gets the path <code> Id. </code> 
     *
     *  @return the <code> Id </code> of the path 
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        return (this.signal.getPathId());
    }


    /**
     *  Gets the path. 
     *
     *  @return the path 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        return (this.signal.getPath());
    }


    /**
     *  Gets the coordinate of the signal on the path. 
     *
     *  @return the coordinate 
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getCoordinate() {
        return (this.signal.getCoordinate());
    }


    /**
     *  Gets the valid state <code> Ids </code> of this signal. 
     *
     *  @return the <code> Ids </code> of the states 
     */

    @OSID @Override
    public org.osid.id.IdList getStateIds() {
        return (this.signal.getStateIds());
    }


    /**
     *  Gets the states of this signal. 
     *
     *  @return the states 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.process.StateList getStates()
        throws org.osid.OperationFailedException {

        return (this.signal.getStates());
    }


    /**
     *  Gets the signal record corresponding to the given <code> Signal 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record.. The <code> signalRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(signalRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  signalRecordType the type of signal record to retrieve 
     *  @return the signal record 
     *  @throws org.osid.NullArgumentException <code> signalRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(signalRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.path.records.SignalRecord getSignalRecord(org.osid.type.Type signalRecordType)
        throws org.osid.OperationFailedException {

        return (this.signal.getSignalRecord(signalRecordType));
    }
}

