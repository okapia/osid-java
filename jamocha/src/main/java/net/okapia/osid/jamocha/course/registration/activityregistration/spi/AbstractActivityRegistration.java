//
// AbstractActivityRegistration.java
//
//     Defines an ActivityRegistration.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activityregistration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines an <code>ActivityRegistration</code>.
 */

public abstract class AbstractActivityRegistration
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.course.registration.ActivityRegistration {

    private org.osid.course.registration.Registration registration;
    private org.osid.course.Activity activity;
    private org.osid.resource.Resource student;

    private final java.util.Collection<org.osid.course.registration.records.ActivityRegistrationRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the registration <code> Id </code> associated with this
     *  registration.
     *
     *  @return the registration <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRegistrationId() {
        return (this.registration.getId());
    }


    /**
     *  Gets the registration associated with this registration. 
     *
     *  @return the registration 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.registration.Registration getRegistration()
        throws org.osid.OperationFailedException {

        return (this.registration);
    }


    /**
     *  Sets the registration.
     *
     *  @param registration the registration
     *  @throws org.osid.NullArgumentException <code>registration</code>
     *          is <code>null</code>
     */

    protected void setRegistration(org.osid.course.registration.Registration registration) {
        nullarg(registration, "registration");
        this.registration = registration;
        return;
    }


    /** 
     *  Gets the activity <code>Id</code>.
     *
     *  @return the activity <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getActivityId() {
        return (this.activity.getId());
    }


    /**
     *  Gets the activity associated with this registration. 
     *
     *  @return the activity 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.course.Activity getActivity()
        throws org.osid.OperationFailedException {

        return (this.activity);
    }


    /**
     *  Sets the activity.
     *
     *  @param activity an activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    protected void setActivity(org.osid.course.Activity activity) {
        nullarg(activity, "activity");
        this.activity = activity;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the student <code> Resource. </code> 
     *
     *  @return the <code> Student </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getStudentId() {
        return (this.student.getId());
    }


    /**
     *  Gets the student <code> Resource. </code> 
     *
     *  @return the student 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getStudent()
        throws org.osid.OperationFailedException {

        return (this.student);
    }


    /**
     *  Sets the student.
     *
     *  @param student a student
     *  @throws org.osid.NullArgumentException <code>student</code> is
     *          <code>null</code>
     */

    protected void setStudent(org.osid.resource.Resource student) {
        nullarg(student, "student");
        this.student = student;
        return;
    }


    /**
     *  Tests if this activityRegistration supports the given record
     *  <code>Type</code>.
     *
     *  @param  activityRegistrationRecordType an activity registration record type 
     *  @return <code>true</code> if the activityRegistrationRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type activityRegistrationRecordType) {
        for (org.osid.course.registration.records.ActivityRegistrationRecord record : this.records) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ActivityRegistration</code> record <code>Type</code>.
     *
     *  @param  activityRegistrationRecordType the activity registration record type 
     *  @return the activity registration record 
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRegistrationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationRecord getActivityRegistrationRecord(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityRegistrationRecord record : this.records) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRegistrationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity registration. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param activityRegistrationRecord the activity registration record
     *  @param activityRegistrationRecordType activity registration record type
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecord</code> or
     *          <code>activityRegistrationRecordTypeactivityRegistration</code>
     *          is <code>null</code>
     */
            
    protected void addActivityRegistrationRecord(org.osid.course.registration.records.ActivityRegistrationRecord activityRegistrationRecord, 
                                                 org.osid.type.Type activityRegistrationRecordType) {
        
        nullarg(activityRegistrationRecord, "activity registration record");
        addRecordType(activityRegistrationRecordType);
        this.records.add(activityRegistrationRecord);
        
        return;
    }
}
