//
// AbstractIndexedMapJournalLookupSession.java
//
//    A simple framework for providing a Journal lookup service
//    backed by a fixed collection of journals with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Journal lookup service backed by a
 *  fixed collection of journals. The journals are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some journals may be compatible
 *  with more types than are indicated through these journal
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Journals</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapJournalLookupSession
    extends AbstractMapJournalLookupSession
    implements org.osid.journaling.JournalLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.journaling.Journal> journalsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.journaling.Journal>());
    private final MultiMap<org.osid.type.Type, org.osid.journaling.Journal> journalsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.journaling.Journal>());


    /**
     *  Makes a <code>Journal</code> available in this session.
     *
     *  @param  journal a journal
     *  @throws org.osid.NullArgumentException <code>journal<code> is
     *          <code>null</code>
     */

    @Override
    protected void putJournal(org.osid.journaling.Journal journal) {
        super.putJournal(journal);

        this.journalsByGenus.put(journal.getGenusType(), journal);
        
        try (org.osid.type.TypeList types = journal.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.journalsByRecord.put(types.getNextType(), journal);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a journal from this session.
     *
     *  @param journalId the <code>Id</code> of the journal
     *  @throws org.osid.NullArgumentException <code>journalId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeJournal(org.osid.id.Id journalId) {
        org.osid.journaling.Journal journal;
        try {
            journal = getJournal(journalId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.journalsByGenus.remove(journal.getGenusType());

        try (org.osid.type.TypeList types = journal.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.journalsByRecord.remove(types.getNextType(), journal);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeJournal(journalId);
        return;
    }


    /**
     *  Gets a <code>JournalList</code> corresponding to the given
     *  journal genus <code>Type</code> which does not include
     *  journals of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known journals or an error results. Otherwise,
     *  the returned list may contain only those journals that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  journalGenusType a journal genus type 
     *  @return the returned <code>Journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByGenusType(org.osid.type.Type journalGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.journaling.journal.ArrayJournalList(this.journalsByGenus.get(journalGenusType)));
    }


    /**
     *  Gets a <code>JournalList</code> containing the given
     *  journal record <code>Type</code>. In plenary mode, the
     *  returned list contains all known journals or an error
     *  results. Otherwise, the returned list may contain only those
     *  journals that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  journalRecordType a journal record type 
     *  @return the returned <code>journal</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>journalRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.journaling.JournalList getJournalsByRecordType(org.osid.type.Type journalRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.journaling.journal.ArrayJournalList(this.journalsByRecord.get(journalRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.journalsByGenus.clear();
        this.journalsByRecord.clear();

        super.close();

        return;
    }
}
