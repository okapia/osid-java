//
// InvariantMapProxyJournalEntryLookupSession
//
//    Implements a JournalEntry lookup service backed by a fixed
//    collection of journalEntries. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.journaling;


/**
 *  Implements a JournalEntry lookup service backed by a fixed
 *  collection of journal entries. The journal entries are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyJournalEntryLookupSession
    extends net.okapia.osid.jamocha.core.journaling.spi.AbstractMapJournalEntryLookupSession
    implements org.osid.journaling.JournalEntryLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyJournalEntryLookupSession} with no
     *  journal entries.
     *
     *  @param journal the journal
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyJournalEntryLookupSession(org.osid.journaling.Journal journal,
                                                  org.osid.proxy.Proxy proxy) {
        setJournal(journal);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyJournalEntryLookupSession} with a single
     *  journal entry.
     *
     *  @param journal the journal
     *  @param journalEntry a single journal entry
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal},
     *          {@code journalEntry} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyJournalEntryLookupSession(org.osid.journaling.Journal journal,
                                                  org.osid.journaling.JournalEntry journalEntry, org.osid.proxy.Proxy proxy) {

        this(journal, proxy);
        putJournalEntry(journalEntry);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyJournalEntryLookupSession} using
     *  an array of journal entries.
     *
     *  @param journal the journal
     *  @param journalEntries an array of journal entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal},
     *          {@code journalEntries} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyJournalEntryLookupSession(org.osid.journaling.Journal journal,
                                                  org.osid.journaling.JournalEntry[] journalEntries, org.osid.proxy.Proxy proxy) {

        this(journal, proxy);
        putJournalEntries(journalEntries);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyJournalEntryLookupSession} using a
     *  collection of journal entries.
     *
     *  @param journal the journal
     *  @param journalEntries a collection of journal entries
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code journal},
     *          {@code journalEntries} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyJournalEntryLookupSession(org.osid.journaling.Journal journal,
                                                  java.util.Collection<? extends org.osid.journaling.JournalEntry> journalEntries,
                                                  org.osid.proxy.Proxy proxy) {

        this(journal, proxy);
        putJournalEntries(journalEntries);
        return;
    }
}
