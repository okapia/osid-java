//
// AbstractAdapterCatalogLookupSession.java
//
//    A Catalog lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.cataloging.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A Catalog lookup session adapter.
 */

public abstract class AbstractAdapterCatalogLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.cataloging.CatalogLookupSession {

    private final org.osid.cataloging.CatalogLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCatalogLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCatalogLookupSession(org.osid.cataloging.CatalogLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Tests if this user can perform {@code Catalog} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCatalogs() {
        return (this.session.canLookupCatalogs());
    }


    /**
     *  A complete view of the {@code Catalog} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCatalogView() {
        this.session.useComparativeCatalogView();
        return;
    }


    /**
     *  A complete view of the {@code Catalog} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCatalogView() {
        this.session.usePlenaryCatalogView();
        return;
    }

     
    /**
     *  Gets the {@code Catalog} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code Catalog} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code Catalog} and
     *  retained for compatibility.
     *
     *  @param catalogId {@code Id} of the {@code Catalog}
     *  @return the catalog
     *  @throws org.osid.NotFoundException {@code catalogId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code catalogId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.Catalog getCatalog(org.osid.id.Id catalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalog(catalogId));
    }


    /**
     *  Gets a {@code CatalogList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  catalogs specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code Catalogs} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  catalogIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code Catalog} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code catalogIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByIds(org.osid.id.IdList catalogIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogsByIds(catalogIds));
    }


    /**
     *  Gets a {@code CatalogList} corresponding to the given
     *  catalog genus {@code Type} which does not include
     *  catalogs of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  catalogs or an error results. Otherwise, the returned list
     *  may contain only those catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogGenusType a catalog genus type 
     *  @return the returned {@code Catalog} list
     *  @throws org.osid.NullArgumentException
     *          {@code catalogGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByGenusType(org.osid.type.Type catalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogsByGenusType(catalogGenusType));
    }


    /**
     *  Gets a {@code CatalogList} corresponding to the given
     *  catalog genus {@code Type} and include any additional
     *  catalogs with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  catalogs or an error results. Otherwise, the returned list
     *  may contain only those catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogGenusType a catalog genus type 
     *  @return the returned {@code Catalog} list
     *  @throws org.osid.NullArgumentException
     *          {@code catalogGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByParentGenusType(org.osid.type.Type catalogGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogsByParentGenusType(catalogGenusType));
    }


    /**
     *  Gets a {@code CatalogList} containing the given
     *  catalog record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  catalogs or an error results. Otherwise, the returned list
     *  may contain only those catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  catalogRecordType a catalog record type 
     *  @return the returned {@code Catalog} list
     *  @throws org.osid.NullArgumentException
     *          {@code catalogRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByRecordType(org.osid.type.Type catalogRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogsByRecordType(catalogRecordType));
    }


    /**
     *  Gets a {@code CatalogList} from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known
     *  catalogs or an error results. Otherwise, the returned list
     *  may contain only those catalogs that are accessible through
     *  this session.
     *
     *  @param  resourceId a resource {@code Id} 
     *  @return the returned {@code Catalog} list 
     *  @throws org.osid.NullArgumentException
     *          {@code resourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogsByProvider(resourceId));
    }


    /**
     *  Gets all {@code Catalogs}. 
     *
     *  In plenary mode, the returned list contains all known
     *  catalogs or an error results. Otherwise, the returned list
     *  may contain only those catalogs that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code Catalogs} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogList getCatalogs()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCatalogs());
    }
}
