//
// AbstractMapCredentialEntryLookupSession
//
//    A simple framework for providing a CredentialEntry lookup service
//    backed by a fixed collection of credential entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CredentialEntry lookup service backed by a
 *  fixed collection of credential entries. The credential entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CredentialEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCredentialEntryLookupSession
    extends net.okapia.osid.jamocha.course.chronicle.spi.AbstractCredentialEntryLookupSession
    implements org.osid.course.chronicle.CredentialEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.chronicle.CredentialEntry> credentialEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.chronicle.CredentialEntry>());


    /**
     *  Makes a <code>CredentialEntry</code> available in this session.
     *
     *  @param  credentialEntry a credential entry
     *  @throws org.osid.NullArgumentException <code>credentialEntry<code>
     *          is <code>null</code>
     */

    protected void putCredentialEntry(org.osid.course.chronicle.CredentialEntry credentialEntry) {
        this.credentialEntries.put(credentialEntry.getId(), credentialEntry);
        return;
    }


    /**
     *  Makes an array of credential entries available in this session.
     *
     *  @param  credentialEntries an array of credential entries
     *  @throws org.osid.NullArgumentException <code>credentialEntries<code>
     *          is <code>null</code>
     */

    protected void putCredentialEntries(org.osid.course.chronicle.CredentialEntry[] credentialEntries) {
        putCredentialEntries(java.util.Arrays.asList(credentialEntries));
        return;
    }


    /**
     *  Makes a collection of credential entries available in this session.
     *
     *  @param  credentialEntries a collection of credential entries
     *  @throws org.osid.NullArgumentException <code>credentialEntries<code>
     *          is <code>null</code>
     */

    protected void putCredentialEntries(java.util.Collection<? extends org.osid.course.chronicle.CredentialEntry> credentialEntries) {
        for (org.osid.course.chronicle.CredentialEntry credentialEntry : credentialEntries) {
            this.credentialEntries.put(credentialEntry.getId(), credentialEntry);
        }

        return;
    }


    /**
     *  Removes a CredentialEntry from this session.
     *
     *  @param  credentialEntryId the <code>Id</code> of the credential entry
     *  @throws org.osid.NullArgumentException <code>credentialEntryId<code> is
     *          <code>null</code>
     */

    protected void removeCredentialEntry(org.osid.id.Id credentialEntryId) {
        this.credentialEntries.remove(credentialEntryId);
        return;
    }


    /**
     *  Gets the <code>CredentialEntry</code> specified by its <code>Id</code>.
     *
     *  @param  credentialEntryId <code>Id</code> of the <code>CredentialEntry</code>
     *  @return the credentialEntry
     *  @throws org.osid.NotFoundException <code>credentialEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>credentialEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntry getCredentialEntry(org.osid.id.Id credentialEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.chronicle.CredentialEntry credentialEntry = this.credentialEntries.get(credentialEntryId);
        if (credentialEntry == null) {
            throw new org.osid.NotFoundException("credentialEntry not found: " + credentialEntryId);
        }

        return (credentialEntry);
    }


    /**
     *  Gets all <code>CredentialEntries</code>. In plenary mode, the returned
     *  list contains all known credentialEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  credentialEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CredentialEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CredentialEntryList getCredentialEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.credentialentry.ArrayCredentialEntryList(this.credentialEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.credentialEntries.clear();
        super.close();
        return;
    }
}
