//
// AbstractBankList
//
//     Implements a filter for a BankList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.assessment.bank.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a BankList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedBankList
 *  to improve performance.
 */

public abstract class AbstractBankFilterList
    extends net.okapia.osid.jamocha.assessment.bank.spi.AbstractBankList
    implements org.osid.assessment.BankList,
               net.okapia.osid.jamocha.inline.filter.assessment.bank.BankFilter {

    private org.osid.assessment.Bank bank;
    private final org.osid.assessment.BankList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractBankFilterList</code>.
     *
     *  @param bankList a <code>BankList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>bankList</code> is <code>null</code>
     */

    protected AbstractBankFilterList(org.osid.assessment.BankList bankList) {
        nullarg(bankList, "bank list");
        this.list = bankList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.bank == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Bank </code> in this list. 
     *
     *  @return the next <code> Bank </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Bank </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.assessment.Bank getNextBank()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.assessment.Bank bank = this.bank;
            this.bank = null;
            return (bank);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in bank list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.bank = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Banks.
     *
     *  @param bank the bank to filter
     *  @return <code>true</code> if the bank passes the filter,
     *          <code>false</code> if the bank should be filtered
     */

    public abstract boolean pass(org.osid.assessment.Bank bank);


    protected void prime() {
        if (this.bank != null) {
            return;
        }

        org.osid.assessment.Bank bank = null;

        while (this.list.hasNext()) {
            try {
                bank = this.list.getNextBank();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(bank)) {
                this.bank = bank;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
