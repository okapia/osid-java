//
// AbstractAssemblyAvailabilityEnablerQuery.java
//
//     An AvailabilityEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.resourcing.rules.availabilityenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An AvailabilityEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyAvailabilityEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.resourcing.rules.AvailabilityEnablerQuery,
               org.osid.resourcing.rules.AvailabilityEnablerQueryInspector,
               org.osid.resourcing.rules.AvailabilityEnablerSearchOrder {

    private final java.util.Collection<org.osid.resourcing.rules.records.AvailabilityEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.AvailabilityEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resourcing.rules.records.AvailabilityEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyAvailabilityEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyAvailabilityEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches mapped to an availability. 
     *
     *  @param  availabilityId the availability <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> availabilityId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledAvailabilityId(org.osid.id.Id availabilityId, 
                                         boolean match) {
        getAssembler().addIdTerm(getRuledAvailabilityIdColumn(), availabilityId, match);
        return;
    }


    /**
     *  Clears the availability <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledAvailabilityIdTerms() {
        getAssembler().clearTerms(getRuledAvailabilityIdColumn());
        return;
    }


    /**
     *  Gets the availability <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledAvailabilityIdTerms() {
        return (getAssembler().getIdTerms(getRuledAvailabilityIdColumn()));
    }


    /**
     *  Gets the RuledAvailabilityId column name.
     *
     * @return the column name
     */

    protected String getRuledAvailabilityIdColumn() {
        return ("ruled_availability_id");
    }


    /**
     *  Tests if an <code> AvailabilityQuery </code> is available. 
     *
     *  @return <code> true </code> if an availability query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledAvailabilityQuery() {
        return (false);
    }


    /**
     *  Gets the query for an availability. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the availability query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledAvailabilityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQuery getRuledAvailabilityQuery() {
        throw new org.osid.UnimplementedException("supportsRuledAvailabilityQuery() is false");
    }


    /**
     *  Matches mapped to any availability. 
     *
     *  @param  match <code> true </code> for mapped to any availability, 
     *          <code> false </code> to match mapped to no availabilities 
     */

    @OSID @Override
    public void matchAnyRuledAvailability(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledAvailabilityColumn(), match);
        return;
    }


    /**
     *  Clears the availability query terms. 
     */

    @OSID @Override
    public void clearRuledAvailabilityTerms() {
        getAssembler().clearTerms(getRuledAvailabilityColumn());
        return;
    }


    /**
     *  Gets the availability query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.AvailabilityQueryInspector[] getRuledAvailabilityTerms() {
        return (new org.osid.resourcing.AvailabilityQueryInspector[0]);
    }


    /**
     *  Gets the RuledAvailability column name.
     *
     * @return the column name
     */

    protected String getRuledAvailabilityColumn() {
        return ("ruled_availability");
    }


    /**
     *  Matches mapped to the foundry. 
     *
     *  @param  foundryId the foundry <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> foundryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchFoundryId(org.osid.id.Id foundryId, boolean match) {
        getAssembler().addIdTerm(getFoundryIdColumn(), foundryId, match);
        return;
    }


    /**
     *  Clears the foundry <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearFoundryIdTerms() {
        getAssembler().clearTerms(getFoundryIdColumn());
        return;
    }


    /**
     *  Gets the foundry <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getFoundryIdTerms() {
        return (getAssembler().getIdTerms(getFoundryIdColumn()));
    }


    /**
     *  Gets the FoundryId column name.
     *
     * @return the column name
     */

    protected String getFoundryIdColumn() {
        return ("foundry_id");
    }


    /**
     *  Tests if a <code> FoundryQuery </code> is available. 
     *
     *  @return <code> true </code> if a foundry query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFoundryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a foundry. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the foundry query 
     *  @throws org.osid.UnimplementedException <code> supportsFoundryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQuery getFoundryQuery() {
        throw new org.osid.UnimplementedException("supportsFoundryQuery() is false");
    }


    /**
     *  Clears the foundry query terms. 
     */

    @OSID @Override
    public void clearFoundryTerms() {
        getAssembler().clearTerms(getFoundryColumn());
        return;
    }


    /**
     *  Gets the foundry query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryQueryInspector[] getFoundryTerms() {
        return (new org.osid.resourcing.FoundryQueryInspector[0]);
    }


    /**
     *  Gets the Foundry column name.
     *
     * @return the column name
     */

    protected String getFoundryColumn() {
        return ("foundry");
    }


    /**
     *  Tests if this availabilityEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  availabilityEnablerRecordType an availability enabler record type 
     *  @return <code>true</code> if the availabilityEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type availabilityEnablerRecordType) {
        for (org.osid.resourcing.rules.records.AvailabilityEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(availabilityEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  availabilityEnablerRecordType the availability enabler record type 
     *  @return the availability enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.AvailabilityEnablerQueryRecord getAvailabilityEnablerQueryRecord(org.osid.type.Type availabilityEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.AvailabilityEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(availabilityEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  availabilityEnablerRecordType the availability enabler record type 
     *  @return the availability enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.AvailabilityEnablerQueryInspectorRecord getAvailabilityEnablerQueryInspectorRecord(org.osid.type.Type availabilityEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.AvailabilityEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(availabilityEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param availabilityEnablerRecordType the availability enabler record type
     *  @return the availability enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(availabilityEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.rules.records.AvailabilityEnablerSearchOrderRecord getAvailabilityEnablerSearchOrderRecord(org.osid.type.Type availabilityEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resourcing.rules.records.AvailabilityEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(availabilityEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(availabilityEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this availability enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param availabilityEnablerQueryRecord the availability enabler query record
     *  @param availabilityEnablerQueryInspectorRecord the availability enabler query inspector
     *         record
     *  @param availabilityEnablerSearchOrderRecord the availability enabler search order record
     *  @param availabilityEnablerRecordType availability enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerQueryRecord</code>,
     *          <code>availabilityEnablerQueryInspectorRecord</code>,
     *          <code>availabilityEnablerSearchOrderRecord</code> or
     *          <code>availabilityEnablerRecordTypeavailabilityEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addAvailabilityEnablerRecords(org.osid.resourcing.rules.records.AvailabilityEnablerQueryRecord availabilityEnablerQueryRecord, 
                                      org.osid.resourcing.rules.records.AvailabilityEnablerQueryInspectorRecord availabilityEnablerQueryInspectorRecord, 
                                      org.osid.resourcing.rules.records.AvailabilityEnablerSearchOrderRecord availabilityEnablerSearchOrderRecord, 
                                      org.osid.type.Type availabilityEnablerRecordType) {

        addRecordType(availabilityEnablerRecordType);

        nullarg(availabilityEnablerQueryRecord, "availability enabler query record");
        nullarg(availabilityEnablerQueryInspectorRecord, "availability enabler query inspector record");
        nullarg(availabilityEnablerSearchOrderRecord, "availability enabler search odrer record");

        this.queryRecords.add(availabilityEnablerQueryRecord);
        this.queryInspectorRecords.add(availabilityEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(availabilityEnablerSearchOrderRecord);
        
        return;
    }
}
