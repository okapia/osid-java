//
// AbstractIndexedMapRoomLookupSession.java
//
//    A simple framework for providing a Room lookup service
//    backed by a fixed collection of rooms with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Room lookup service backed by a
 *  fixed collection of rooms. The rooms are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some rooms may be compatible
 *  with more types than are indicated through these room
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Rooms</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapRoomLookupSession
    extends AbstractMapRoomLookupSession
    implements org.osid.room.RoomLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.room.Room> roomsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.Room>());
    private final MultiMap<org.osid.type.Type, org.osid.room.Room> roomsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.room.Room>());


    /**
     *  Makes a <code>Room</code> available in this session.
     *
     *  @param  room a room
     *  @throws org.osid.NullArgumentException <code>room<code> is
     *          <code>null</code>
     */

    @Override
    protected void putRoom(org.osid.room.Room room) {
        super.putRoom(room);

        this.roomsByGenus.put(room.getGenusType(), room);
        
        try (org.osid.type.TypeList types = room.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.roomsByRecord.put(types.getNextType(), room);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a room from this session.
     *
     *  @param roomId the <code>Id</code> of the room
     *  @throws org.osid.NullArgumentException <code>roomId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeRoom(org.osid.id.Id roomId) {
        org.osid.room.Room room;
        try {
            room = getRoom(roomId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.roomsByGenus.remove(room.getGenusType());

        try (org.osid.type.TypeList types = room.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.roomsByRecord.remove(types.getNextType(), room);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeRoom(roomId);
        return;
    }


    /**
     *  Gets a <code>RoomList</code> corresponding to the given
     *  room genus <code>Type</code> which does not include
     *  rooms of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known rooms or an error results. Otherwise,
     *  the returned list may contain only those rooms that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  roomGenusType a room genus type 
     *  @return the returned <code>Room</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>roomGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByGenusType(org.osid.type.Type roomGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.room.ArrayRoomList(this.roomsByGenus.get(roomGenusType)));
    }


    /**
     *  Gets a <code>RoomList</code> containing the given
     *  room record <code>Type</code>. In plenary mode, the
     *  returned list contains all known rooms or an error
     *  results. Otherwise, the returned list may contain only those
     *  rooms that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  roomRecordType a room record type 
     *  @return the returned <code>room</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>roomRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRoomsByRecordType(org.osid.type.Type roomRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.room.ArrayRoomList(this.roomsByRecord.get(roomRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roomsByGenus.clear();
        this.roomsByRecord.clear();

        super.close();

        return;
    }
}
