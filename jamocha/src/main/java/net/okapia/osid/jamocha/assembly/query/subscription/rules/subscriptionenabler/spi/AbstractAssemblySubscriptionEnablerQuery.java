//
// AbstractAssemblySubscriptionEnablerQuery.java
//
//     A SubscriptionEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.subscription.rules.subscriptionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SubscriptionEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblySubscriptionEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.subscription.rules.SubscriptionEnablerQuery,
               org.osid.subscription.rules.SubscriptionEnablerQueryInspector,
               org.osid.subscription.rules.SubscriptionEnablerSearchOrder {

    private final java.util.Collection<org.osid.subscription.rules.records.SubscriptionEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.subscription.rules.records.SubscriptionEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.subscription.rules.records.SubscriptionEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySubscriptionEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySubscriptionEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the subscription. 
     *
     *  @param  subscriptionId the subscription <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> subscriptionId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledSubscriptionId(org.osid.id.Id subscriptionId, 
                                         boolean match) {
        getAssembler().addIdTerm(getRuledSubscriptionIdColumn(), subscriptionId, match);
        return;
    }


    /**
     *  Clears the subscription <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledSubscriptionIdTerms() {
        getAssembler().clearTerms(getRuledSubscriptionIdColumn());
        return;
    }


    /**
     *  Gets the subscription <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledSubscriptionIdTerms() {
        return (getAssembler().getIdTerms(getRuledSubscriptionIdColumn()));
    }


    /**
     *  Gets the RuledSubscriptionId column name.
     *
     * @return the column name
     */

    protected String getRuledSubscriptionIdColumn() {
        return ("ruled_subscription_id");
    }


    /**
     *  Tests if a <code> SubscriptionQuery </code> is available. 
     *
     *  @return <code> true </code> if a subscription query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledSubscriptionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a subscription. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the subscription query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledSubscriptionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQuery getRuledSubscriptionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledSubscriptionQuery() is false");
    }


    /**
     *  Matches enablers mapped to any subscription. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          subscription, <code> false </code> to match enablers mapped to 
     *          no subscription 
     */

    @OSID @Override
    public void matchAnyRuledSubscription(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledSubscriptionColumn(), match);
        return;
    }


    /**
     *  Clears the subscription query terms. 
     */

    @OSID @Override
    public void clearRuledSubscriptionTerms() {
        getAssembler().clearTerms(getRuledSubscriptionColumn());
        return;
    }


    /**
     *  Gets the subscription query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionQueryInspector[] getRuledSubscriptionTerms() {
        return (new org.osid.subscription.SubscriptionQueryInspector[0]);
    }


    /**
     *  Gets the RuledSubscription column name.
     *
     * @return the column name
     */

    protected String getRuledSubscriptionColumn() {
        return ("ruled_subscription");
    }


    /**
     *  Matches enablers mapped to the publisher. 
     *
     *  @param  publisherId the publisher <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> publisherId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPublisherId(org.osid.id.Id publisherId, boolean match) {
        getAssembler().addIdTerm(getPublisherIdColumn(), publisherId, match);
        return;
    }


    /**
     *  Clears the publisher <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPublisherIdTerms() {
        getAssembler().clearTerms(getPublisherIdColumn());
        return;
    }


    /**
     *  Gets the publisher <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPublisherIdTerms() {
        return (getAssembler().getIdTerms(getPublisherIdColumn()));
    }


    /**
     *  Gets the PublisherId column name.
     *
     * @return the column name
     */

    protected String getPublisherIdColumn() {
        return ("publisher_id");
    }


    /**
     *  Tests if a <code> PublisherQuery </code> is available. 
     *
     *  @return <code> true </code> if a publisher query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPublisherQuery() {
        return (false);
    }


    /**
     *  Gets the query for a publisher. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the publisher query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPublisherQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQuery getPublisherQuery() {
        throw new org.osid.UnimplementedException("supportsPublisherQuery() is false");
    }


    /**
     *  Clears the publisher query terms. 
     */

    @OSID @Override
    public void clearPublisherTerms() {
        getAssembler().clearTerms(getPublisherColumn());
        return;
    }


    /**
     *  Gets the publisher query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.subscription.PublisherQueryInspector[] getPublisherTerms() {
        return (new org.osid.subscription.PublisherQueryInspector[0]);
    }


    /**
     *  Gets the Publisher column name.
     *
     * @return the column name
     */

    protected String getPublisherColumn() {
        return ("publisher");
    }


    /**
     *  Tests if this subscriptionEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  subscriptionEnablerRecordType a subscription enabler record type 
     *  @return <code>true</code> if the subscriptionEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type subscriptionEnablerRecordType) {
        for (org.osid.subscription.rules.records.SubscriptionEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(subscriptionEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  subscriptionEnablerRecordType the subscription enabler record type 
     *  @return the subscription enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.rules.records.SubscriptionEnablerQueryRecord getSubscriptionEnablerQueryRecord(org.osid.type.Type subscriptionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.rules.records.SubscriptionEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(subscriptionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  subscriptionEnablerRecordType the subscription enabler record type 
     *  @return the subscription enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.rules.records.SubscriptionEnablerQueryInspectorRecord getSubscriptionEnablerQueryInspectorRecord(org.osid.type.Type subscriptionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.rules.records.SubscriptionEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(subscriptionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param subscriptionEnablerRecordType the subscription enabler record type
     *  @return the subscription enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(subscriptionEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.subscription.rules.records.SubscriptionEnablerSearchOrderRecord getSubscriptionEnablerSearchOrderRecord(org.osid.type.Type subscriptionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.subscription.rules.records.SubscriptionEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(subscriptionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(subscriptionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this subscription enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param subscriptionEnablerQueryRecord the subscription enabler query record
     *  @param subscriptionEnablerQueryInspectorRecord the subscription enabler query inspector
     *         record
     *  @param subscriptionEnablerSearchOrderRecord the subscription enabler search order record
     *  @param subscriptionEnablerRecordType subscription enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>subscriptionEnablerQueryRecord</code>,
     *          <code>subscriptionEnablerQueryInspectorRecord</code>,
     *          <code>subscriptionEnablerSearchOrderRecord</code> or
     *          <code>subscriptionEnablerRecordTypesubscriptionEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addSubscriptionEnablerRecords(org.osid.subscription.rules.records.SubscriptionEnablerQueryRecord subscriptionEnablerQueryRecord, 
                                      org.osid.subscription.rules.records.SubscriptionEnablerQueryInspectorRecord subscriptionEnablerQueryInspectorRecord, 
                                      org.osid.subscription.rules.records.SubscriptionEnablerSearchOrderRecord subscriptionEnablerSearchOrderRecord, 
                                      org.osid.type.Type subscriptionEnablerRecordType) {

        addRecordType(subscriptionEnablerRecordType);

        nullarg(subscriptionEnablerQueryRecord, "subscription enabler query record");
        nullarg(subscriptionEnablerQueryInspectorRecord, "subscription enabler query inspector record");
        nullarg(subscriptionEnablerSearchOrderRecord, "subscription enabler search odrer record");

        this.queryRecords.add(subscriptionEnablerQueryRecord);
        this.queryInspectorRecords.add(subscriptionEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(subscriptionEnablerSearchOrderRecord);
        
        return;
    }
}
