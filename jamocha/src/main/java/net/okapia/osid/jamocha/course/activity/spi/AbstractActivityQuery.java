//
// AbstractActivityQuery.java
//
//     A template for making an Activity Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.activity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for activities.
 */

public abstract class AbstractActivityQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQuery
    implements org.osid.course.ActivityQuery {

    private final java.util.Collection<org.osid.course.records.ActivityQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches an activity that is implicitly generated. 
     *
     *  @param  match <code> true </code> to match activities implicitly 
     *          generated, <code> false </code> to match activities explicitly 
     *          defined 
     */

    @OSID @Override
    public void matchImplicit(boolean match) {
        return;
    }


    /**
     *  Clears the implcit terms. 
     */

    @OSID @Override
    public void clearImplicitTerms() {
        return;
    }


    /**
     *  Sets the activity unit <code> Id </code> for this query. 
     *
     *  @param  activityUnitId an activity unit <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityUnitId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchActivityUnitId(org.osid.id.Id activityUnitId, 
                                    boolean match) {
        return;
    }


    /**
     *  Clears the activity unit <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearActivityUnitIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityUnitQuery </code> is available. 
     *
     *  @return <code> true </code> if an activity unit query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActivityUnitQuery() {
        return (false);
    }


    /**
     *  Gets the query for an activity unit. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the activity unit query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActivityUnitQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitQuery getActivityUnitQuery() {
        throw new org.osid.UnimplementedException("supportsActivityUnitQuery() is false");
    }


    /**
     *  Clears the activity unit terms. 
     */

    @OSID @Override
    public void clearActivityUnitTerms() {
        return;
    }


    /**
     *  Sets the course offering <code> Id </code> for this query. 
     *
     *  @param  courseOfferingId a course offering <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseOfferingId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseOfferingId(org.osid.id.Id courseOfferingId, 
                                      boolean match) {
        return;
    }


    /**
     *  Clears the course offering <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseOfferingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseOfferingQuery </code> is available. 
     *
     *  @return <code> true </code> if a course offering query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseOfferingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a coure offering. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course offering query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseOfferingQuery getCourseOfferingQuery() {
        throw new org.osid.UnimplementedException("supportsCourseOfferingQuery() is false");
    }


    /**
     *  Clears the course offering terms. 
     */

    @OSID @Override
    public void clearCourseOfferingTerms() {
        return;
    }


    /**
     *  Sets the term <code> Id </code> for this query to match course 
     *  offerings that have a related term. 
     *
     *  @param  termId a term <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> termId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchTermId(org.osid.id.Id termId, boolean match) {
        return;
    }


    /**
     *  Clears the term <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearTermIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> TermQuery </code> is available. 
     *
     *  @return <code> true </code> if a term query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTermQuery() {
        return (false);
    }


    /**
     *  Gets the query for a reporting term. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the term query 
     *  @throws org.osid.UnimplementedException <code> supportsTermQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.TermQuery getTermQuery() {
        throw new org.osid.UnimplementedException("supportsTermQuery() is false");
    }


    /**
     *  Clears the term terms. 
     */

    @OSID @Override
    public void clearTermTerms() {
        return;
    }


    /**
     *  Matches activities where the given time falls within a denormalized 
     *  meeting time inclusive. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchMeetingTime(org.osid.calendaring.DateTime date, 
                                 boolean match) {
        return;
    }


    /**
     *  Clears the meeting time terms. 
     */

    @OSID @Override
    public void clearMeetingTimeTerms() {
        return;
    }


    /**
     *  Matches activities with any denormalized meeting time within the given 
     *  date range inclusive. 
     *
     *  @param  start a start date 
     *  @param  end an end date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchMeetingTimeInclusive(org.osid.calendaring.DateTime start, 
                                          org.osid.calendaring.DateTime end, 
                                          boolean match) {
        return;
    }


    /**
     *  Clears the meeting time inclusive terms. 
     */

    @OSID @Override
    public void clearMeetingTimeInclusiveTerms() {
        return;
    }


    /**
     *  Matches activities with any meeting location. 
     *
     *  @param  locationId a location <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> locationId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchMeetingLocationId(org.osid.id.Id locationId, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMeetingLocationIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> LocationQuery </code> is available. 
     *
     *  @return <code> true </code> if a location query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMeetingLocationQuery() {
        return (false);
    }


    /**
     *  Gets the query for a location. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the location query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMeetingLocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.mapping.LocationQuery getMeetingLocationQuery() {
        throw new org.osid.UnimplementedException("supportsMeetingLocationQuery() is false");
    }


    /**
     *  Clears the location <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearMeetingLocationTerms() {
        return;
    }


    /**
     *  Sets the schedule <code> Id </code> for this query. 
     *
     *  @param  scheduleId a schedule <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> scheduleId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchScheduleId(org.osid.id.Id scheduleId, boolean match) {
        return;
    }


    /**
     *  Clears the schedule <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearScheduleIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ScheduleQuery </code> is available. 
     *
     *  @return <code> true </code> if a schedule query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsScheduleQuery() {
        return (false);
    }


    /**
     *  Gets the query for a schedule. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the schedule query 
     *  @throws org.osid.UnimplementedException <code> supportsScheduleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleQuery getScheduleQuery() {
        throw new org.osid.UnimplementedException("supportsScheduleQuery() is false");
    }


    /**
     *  Matches an activity that has any schedule. 
     *
     *  @param  match <code> true </code> to match activitiies with any 
     *          schedule, <code> false </code> to match activitiies with no 
     *          schedule 
     */

    @OSID @Override
    public void matchAnySchedule(boolean match) {
        return;
    }


    /**
     *  Clears the schedule terms. 
     */

    @OSID @Override
    public void clearScheduleTerms() {
        return;
    }


    /**
     *  Sets the superseding activity <code> Id </code> for this query. 
     *
     *  @param  activityId a superseding activity <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> activityId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSupersedingActivityId(org.osid.id.Id activityId, 
                                           boolean match) {
        return;
    }


    /**
     *  Clears the superseding activity <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearSupersedingActivityIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> ActivityQuery </code> is available. 
     *
     *  @return <code> true </code> if a superseding activity query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSupersedingActivityQuery() {
        return (false);
    }


    /**
     *  Gets the query for a superseding activity. Multiple retrievals produce 
     *  a nested <code> OR </code> term. 
     *
     *  @return the superseding activity query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSupersedingActivityQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.ActivityQuery getSupersedingActivityQuery() {
        throw new org.osid.UnimplementedException("supportsSupersedingActivityQuery() is false");
    }


    /**
     *  Matches an activity that has any superseding activity. 
     *
     *  @param  match <code> true </code> to match activitiies with any 
     *          superseding activity, <code> false </code> to match 
     *          activitiies with no superseding activities 
     */

    @OSID @Override
    public void matchAnySupersedingActivity(boolean match) {
        return;
    }


    /**
     *  Clears the superseding activity terms. 
     */

    @OSID @Override
    public void clearSupersedingActivityTerms() {
        return;
    }





    /**
     *  Matches activities with specific dates between the given range 
     *  inclusive. 
     *
     *  @param  start start date 
     *  @param  end end date 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> end </code> is less 
     *          than <code> start </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> zero </code> 
     */

    @OSID @Override
    public void matchSpecificMeetingTime(org.osid.calendaring.DateTime start, 
                                         org.osid.calendaring.DateTime end, 
                                         boolean match) {

        return;
    }


    /**
     *  Matches an activity that has any specific date. 
     *
     *  @param  match <code> true </code> to match activitiies with any 
     *          specific date, <code> false </code> to match activitiies with 
     *          no specific dates 
     */

    @OSID @Override
    public void matchAnySpecificMeetingTime(boolean match) {
        return;
    }


    /**
     *  Clears the specific date terms. 
     */

    @OSID @Override
    public void clearSpecificMeetingTimeTerms() {
        return;
    }

    
    /**
     *  Matches activities containing the given blackout date. 
     *
     *  @param  date a date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> date </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchBlackout(org.osid.calendaring.DateTime date, 
                              boolean match) {
        return;
    }


    /**
     *  Matches an activity that has any blackout. 
     *
     *  @param  match <code> true </code> to match activitiies with any 
     *          blackout, <code> false </code> to match activitiies with no 
     *          blackouts 
     */

    @OSID @Override
    public void matchAnyBlackout(boolean match) {
        return;
    }


    /**
     *  Clears all blackout terms. 
     */

    @OSID @Override
    public void clearBlackoutTerms() {
        return;
    }


    /**
     *  Matches activities with blackouts within the given date range 
     *  inclousive. 
     *
     *  @param  start a start date 
     *  @param  end an end date 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> star </code> t or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchBlackoutInclusive(org.osid.calendaring.DateTime start, 
                                       org.osid.calendaring.DateTime end, 
                                       boolean match) {
        return;
    }


    /**
     *  Clears all blackout inclusive terms. 
     */

    @OSID @Override
    public void clearBlackoutInclusiveTerms() {
        return;
    }


    /**
     *  Sets the resource <code> Id </code> for this query to match course 
     *  offerings that have an instructor. 
     *
     *  @param  resourceId a resource <code> Id </code> 
     *  @param  match <code> true </code> if a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> resourceId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchInstructorId(org.osid.id.Id resourceId, boolean match) {
        return;
    }


    /**
     *  Clears the instructor <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearInstructorIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> ResourceQuery </code> is available. 
     *
     *  @return <code> true </code> if a resource query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInstructorQuery() {
        return (false);
    }


    /**
     *  Gets the query for an instructor. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return a resource query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInstructorQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceQuery getInstructorQuery() {
        throw new org.osid.UnimplementedException("supportsInstructorQuery() is false");
    }


    /**
     *  Matches course offerings that have any instructor. 
     *
     *  @param  match <code> true </code> to match course offerings with any 
     *          instructor, <code> false </code> to match course offerings 
     *          with no instructors 
     */

    @OSID @Override
    public void matchAnyInstructor(boolean match) {
        return;
    }


    /**
     *  Clears the instructor terms. 
     */

    @OSID @Override
    public void clearInstructorTerms() {
        return;
    }


    /**
     *  Matches activities with a minimum seating between the given numbers 
     *  inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMinimumSeats(long min, long max, boolean match) {
        return;
    }


    /**
     *  Matches activities with any minimum seating assigned. 
     *
     *  @param  match <code> true </code> to match activities with any minimum 
     *          seating, <code> false </code> to match activities with no 
     *          minimum seating 
     */

    @OSID @Override
    public void matchAnyMinimumSeats(boolean match) {
        return;
    }


    /**
     *  Clears the minimum seating terms. 
     */

    @OSID @Override
    public void clearMinimumSeatsTerms() {
        return;
    }


    /**
     *  Matches activities with a maximum seating between the given numbers 
     *  inclusive. 
     *
     *  @param  min low number 
     *  @param  max high number 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     */

    @OSID @Override
    public void matchMaximumSeats(long min, long max, boolean match) {
        return;
    }


    /**
     *  Matches activities with any maximum seating assigned. 
     *
     *  @param  match <code> true </code> to match activities with any maximum 
     *          seating, <code> false </code> to match activities with no 
     *          maximum seating 
     */

    @OSID @Override
    public void matchAnyMaximumSeats(boolean match) {
        return;
    }


    /**
     *  Clears the maximum seating terms. 
     */

    @OSID @Override
    public void clearMaximumSeatsTerms() {
        return;
    }


    /**
     *  Matches activities with a total effort between the given durations 
     *  inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetEffort(org.osid.calendaring.Duration min, 
                                       org.osid.calendaring.Duration max, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches an activity that has any total effort assigned. 
     *
     *  @param  match <code> true </code> to match activities with any total 
     *          effort, <code> false </code> to match activities with no total 
     *          effort 
     */

    @OSID @Override
    public void matchAnyTotalTargetEffort(boolean match) {
        return;
    }


    /**
     *  Clears the total effort terms. 
     */

    @OSID @Override
    public void clearTotalTargetEffortTerms() {
        return;
    }


    /**
     *  Matches activities that are contact activities. 
     *
     *  @param  match <code> true </code> to match activities that have 
     *          contact, <code> false </code> to match activities with no 
     *          contact 
     */

    @OSID @Override
    public void matchContact(boolean match) {
        return;
    }


    /**
     *  Clears the contact terms. 
     */

    @OSID @Override
    public void clearContactTerms() {
        return;
    }


    /**
     *  Matches activities with a total contact time between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetContactTime(org.osid.calendaring.Duration min, 
                                            org.osid.calendaring.Duration max, 
                                            boolean match) {
        return;
    }


    /**
     *  Matches an activity that has any total contact assigned. 
     *
     *  @param  match <code> true </code> to match activities with any total 
     *          contatc, <code> false </code> to match activities with no 
     *          total contact 
     */

    @OSID @Override
    public void matchAnyTotalTargetContactTime(boolean match) {
        return;
    }


    /**
     *  Clears the total contact terms. 
     */

    @OSID @Override
    public void clearTotalTargetContactTimeTerms() {
        return;
    }


    /**
     *  Matches activities with a individual effort between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchTotalTargetIndividualEffort(org.osid.calendaring.Duration min, 
                                                 org.osid.calendaring.Duration max, 
                                                 boolean match) {
        return;
    }


    /**
     *  Matches an activity that has any individual effort assigned. 
     *
     *  @param  match <code> true </code> to match activities with any 
     *          individual effort, <code> false </code> to match activities 
     *          with no individual effort 
     */

    @OSID @Override
    public void matchAnyTotalTargetIndividualEffort(boolean match) {
        return;
    }


    /**
     *  Clears the individual effort terms. 
     */

    @OSID @Override
    public void clearTotalTargetIndividualEffortTerms() {
        return;
    }


    /**
     *  Matches activities that recur weekly. 
     *
     *  @param  match <code> true </code> to match activities that recur 
     *          weekly, <code> false </code> to match activities with no 
     *          weekly recurrance 
     */

    @OSID @Override
    public void matchRecurringWeekly(boolean match) {
        return;
    }


    /**
     *  Clears the recurring weekly terms. 
     */

    @OSID @Override
    public void clearRecurringWeeklyTerms() {
        return;
    }


    /**
     *  Matches activities with a weekly effort between the given durations 
     *  inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyEffort(org.osid.calendaring.Duration min, 
                                  org.osid.calendaring.Duration max, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches an activity that has any weekly effort assigned. 
     *
     *  @param  match <code> true </code> to match activities with any weekly 
     *          effort, <code> false </code> to match activities with no 
     *          weekly effort 
     */

    @OSID @Override
    public void matchAnyWeeklyEffort(boolean match) {
        return;
    }


    /**
     *  Clears the weekly effort terms. 
     */

    @OSID @Override
    public void clearWeeklyEffortTerms() {
        return;
    }


    /**
     *  Matches activities with a weekly contact time between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyContactTime(org.osid.calendaring.Duration min, 
                                       org.osid.calendaring.Duration max, 
                                       boolean match) {
        return;
    }


    /**
     *  Matches an activity that has any weekly contact time assigned. 
     *
     *  @param  match <code> true </code> to match activities with any weekly 
     *          contact time, <code> false </code> to match activities with no 
     *          weekly contact time 
     */

    @OSID @Override
    public void matchAnyWeeklyContactTime(boolean match) {
        return;
    }


    /**
     *  Clears the weekly contact time terms. 
     */

    @OSID @Override
    public void clearWeeklyContactTimeTerms() {
        return;
    }


    /**
     *  Matches activities with a weekly individual effort between the given 
     *  durations inclusive. 
     *
     *  @param  min low range 
     *  @param  max high range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> max </code> is less 
     *          than <code> min </code> 
     *  @throws org.osid.NullArgumentException <code> min </code> or <code> 
     *          max </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchWeeklyIndividualEffort(org.osid.calendaring.Duration min, 
                                            org.osid.calendaring.Duration max, 
                                            boolean match) {
        return;
    }


    /**
     *  Matches an activity that has any weekly individual effort assigned. 
     *
     *  @param  match <code> true </code> to match activities with any weekly 
     *          individual effort, <code> false </code> to match activities 
     *          with no weekly individual effort 
     */

    @OSID @Override
    public void matchAnyWeeklyIndividualEffort(boolean match) {
        return;
    }


    /**
     *  Clears the weekly individual effort terms. 
     */

    @OSID @Override
    public void clearWeeklyIndividualEffortTerms() {
        return;
    }


    /**
     *  Sets the course catalog <code> Id </code> for this query to match 
     *  activities assigned to course catalogs. 
     *
     *  @param  courseCatalogId the course catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public void matchCourseCatalogId(org.osid.id.Id courseCatalogId, 
                                     boolean match) {
        return;
    }


    /**
     *  Clears the course catalog <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCourseCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CourseCatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a course catalog query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a course catalog. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the course catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQuery getCourseCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsCourseCatalogQuery() is false");
    }


    /**
     *  Clears the course catalog terms. 
     */

    @OSID @Override
    public void clearCourseCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given activity query
     *  record <code> Type. </code> This method must be used to
     *  retrieve an activity implementing the requested record.
     *
     *  @param activityRecordType an activity record type
     *  @return the activity query record
     *  @throws org.osid.NullArgumentException
     *          <code>activityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.records.ActivityQueryRecord getActivityQueryRecord(org.osid.type.Type activityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.records.ActivityQueryRecord record : this.records) {
            if (record.implementsRecordType(activityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity query. 
     *
     *  @param activityQueryRecord activity query record
     *  @param activityRecordType activity record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityQueryRecord(org.osid.course.records.ActivityQueryRecord activityQueryRecord, 
                                          org.osid.type.Type activityRecordType) {

        addRecordType(activityRecordType);
        nullarg(activityQueryRecord, "activity query record");
        this.records.add(activityQueryRecord);        
        return;
    }
}
