//
// AbstractFrontOfficeQueryInspector.java
//
//     A template for making a FrontOfficeQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.tracking.frontoffice.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for front offices.
 */

public abstract class AbstractFrontOfficeQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQueryInspector
    implements org.osid.tracking.FrontOfficeQueryInspector {

    private final java.util.Collection<org.osid.tracking.records.FrontOfficeQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the issue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIssueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the issue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.IssueQueryInspector[] getIssueTerms() {
        return (new org.osid.tracking.IssueQueryInspector[0]);
    }


    /**
     *  Gets the queue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getQueueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the queue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.QueueQueryInspector[] getQueueTerms() {
        return (new org.osid.tracking.QueueQueryInspector[0]);
    }


    /**
     *  Gets the ancestor front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorFrontOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the ancestor front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getAncestorFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }


    /**
     *  Gets the descendant front office <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantFrontOfficeIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the descendant front office query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.tracking.FrontOfficeQueryInspector[] getDescendantFrontOfficeTerms() {
        return (new org.osid.tracking.FrontOfficeQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given front office query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a front office implementing the requested record.
     *
     *  @param frontOfficeRecordType a front office record type
     *  @return the front office query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>frontOfficeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(frontOfficeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.tracking.records.FrontOfficeQueryInspectorRecord getFrontOfficeQueryInspectorRecord(org.osid.type.Type frontOfficeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.tracking.records.FrontOfficeQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(frontOfficeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(frontOfficeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this front office query. 
     *
     *  @param frontOfficeQueryInspectorRecord front office query inspector
     *         record
     *  @param frontOfficeRecordType frontOffice record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFrontOfficeQueryInspectorRecord(org.osid.tracking.records.FrontOfficeQueryInspectorRecord frontOfficeQueryInspectorRecord, 
                                                   org.osid.type.Type frontOfficeRecordType) {

        addRecordType(frontOfficeRecordType);
        nullarg(frontOfficeRecordType, "front office record type");
        this.records.add(frontOfficeQueryInspectorRecord);        
        return;
    }
}
