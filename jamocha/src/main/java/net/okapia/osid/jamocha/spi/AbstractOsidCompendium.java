//
// AbstractOsidCompendium.java
//
//     Defines a generic OsidCompendium.
//
//
// Tom Coppeto
// OnTapSolutions
// 5 October 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a generic <code>OsidCompendium</code>.
 */

public abstract class AbstractOsidCompendium
    extends AbstractOsidObject
    implements org.osid.OsidCompendium {

    private org.osid.calendaring.DateTime start;
    private org.osid.calendaring.DateTime end;
    private boolean interpolated;
    private boolean extrapolated;


    /**
     *  Gets the start date used in the evaluation of the
     *  transactional data on which this report is based.
     *
     *  @return the date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.start);
    }


    /**
     *  Sets the start date.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setStartDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.start = date;
        return;
    }



    /**
     *  Gets the end date used in the evaluation of the transactional
     *  data on which this report is based.
     *
     *  @return the date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.end);
    }


    /**
     *  Sets the end date.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    protected void setEndDate(org.osid.calendaring.DateTime date) {
        nullarg(date, "date");
        this.end = date;
        return;
    }


    /**
     *  Tests if this report is interpolated within measured data or
     *  known transactions. Interpolation may occur if the start or
     *  end date fall between two known facts or managed time period.
     *
     *  @return <code> true </code> if this report is interpolated,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isInterpolated() {
        return (this.interpolated);
    }


    /**
     *  Sets the interpolated flag.
     *
     *  @param interpolated {@code true} is interpolated, {@code
     *         false} otherwise
     */

    protected void setInterpolated(boolean interpolated) {
        this.interpolated = interpolated;
        return;
    }
        

    /**
     *  Tests if this report is extrapolated outside measured data or
     *  known transactions. Extrapolation may occur if the start or
     *  end date fall outside two known facts or managed time
     *  period. Extrapolation may occur within a managed time period
     *  in progress where the results of the entire time period are
     *  projected.
     *
     *  @return <code> true </code> if this report is extrapolated,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isExtrapolated() {
        return (this.extrapolated);
    }


    /**
     *  Sets the extrapolated flag.
     *
     *  @param extrapolated {@code true} is extrapolated, {@code
     *         false} otherwise
     */

    protected void setExtrapolated(boolean extrapolated) {
        this.extrapolated = extrapolated;
        return;
    }
}
