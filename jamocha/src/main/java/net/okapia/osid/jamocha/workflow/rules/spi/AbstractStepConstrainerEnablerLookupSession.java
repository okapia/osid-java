//
// AbstractStepConstrainerEnablerLookupSession.java
//
//    A starter implementation framework for providing a StepConstrainerEnabler
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a StepConstrainerEnabler
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getStepConstrainerEnablers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractStepConstrainerEnablerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.workflow.rules.StepConstrainerEnablerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();
    

    /**
     *  Gets the <code>Office/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>StepConstrainerEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStepConstrainerEnablers() {
        return (true);
    }


    /**
     *  A complete view of the <code>StepConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepConstrainerEnablerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>StepConstrainerEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepConstrainerEnablerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step constrainer enablers in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active step constrainer enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepConstrainerEnablerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive step constrainer enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepConstrainerEnablerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>StepConstrainerEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>StepConstrainerEnabler</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>StepConstrainerEnabler</code> and
     *  retained for compatibility.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @param  stepConstrainerEnablerId <code>Id</code> of the
     *          <code>StepConstrainerEnabler</code>
     *  @return the step constrainer enabler
     *  @throws org.osid.NotFoundException <code>stepConstrainerEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stepConstrainerEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnabler getStepConstrainerEnabler(org.osid.id.Id stepConstrainerEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.workflow.rules.StepConstrainerEnablerList stepConstrainerEnablers = getStepConstrainerEnablers()) {
            while (stepConstrainerEnablers.hasNext()) {
                org.osid.workflow.rules.StepConstrainerEnabler stepConstrainerEnabler = stepConstrainerEnablers.getNextStepConstrainerEnabler();
                if (stepConstrainerEnabler.getId().equals(stepConstrainerEnablerId)) {
                    return (stepConstrainerEnabler);
                }
            }
        } 

        throw new org.osid.NotFoundException(stepConstrainerEnablerId + " not found");
    }


    /**
     *  Gets a <code>StepConstrainerEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stepConstrainerEnablers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>StepConstrainerEnablers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getStepConstrainerEnablers()</code>.
     *
     *  @param  stepConstrainerEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>StepConstrainerEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
*               found
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByIds(org.osid.id.IdList stepConstrainerEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.workflow.rules.StepConstrainerEnabler> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = stepConstrainerEnablerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getStepConstrainerEnabler(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("step constrainer enabler " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.workflow.rules.stepconstrainerenabler.LinkedStepConstrainerEnablerList(ret));
    }


    /**
     *  Gets a <code>StepConstrainerEnablerList</code> corresponding to the given
     *  step constrainer enabler genus <code>Type</code> which does not include
     *  step constrainer enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getStepConstrainerEnablers()</code>.
     *
     *  @param  stepConstrainerEnablerGenusType a stepConstrainerEnabler genus type 
     *  @return the returned <code>StepConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByGenusType(org.osid.type.Type stepConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepconstrainerenabler.StepConstrainerEnablerGenusFilterList(getStepConstrainerEnablers(), stepConstrainerEnablerGenusType));
    }


    /**
     *  Gets a <code>StepConstrainerEnablerList</code> corresponding to the given
     *  step constrainer enabler genus <code>Type</code> and include any additional
     *  step constrainer enablers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStepConstrainerEnablers()</code>.
     *
     *  @param  stepConstrainerEnablerGenusType a stepConstrainerEnabler genus type 
     *  @return the returned <code>StepConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByParentGenusType(org.osid.type.Type stepConstrainerEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStepConstrainerEnablersByGenusType(stepConstrainerEnablerGenusType));
    }


    /**
     *  Gets a <code>StepConstrainerEnablerList</code> containing the given
     *  step constrainer enabler record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *

     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStepConstrainerEnablers()</code>.
     *
     *  @param  stepConstrainerEnablerRecordType a stepConstrainerEnabler record type 
     *  @return the returned <code>StepConstrainerEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersByRecordType(org.osid.type.Type stepConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepconstrainerenabler.StepConstrainerEnablerRecordFilterList(getStepConstrainerEnablers(), stepConstrainerEnablerRecordType));
    }


    /**
     *  Gets a <code>StepConstrainerEnablerList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible
     *  through this session.
     *  
     *  In active mode, step constrainer enablers are returned that are currently
     *  active in addition to being effective during the given date
     *  range. In any status mode, active and inactive step constrainer enablers are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>StepConstrainerEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepconstrainerenabler.TemporalStepConstrainerEnablerFilterList(getStepConstrainerEnablers(), from, to));
    }
        

    /**
     *  Gets a <code>StepConstrainerEnablerList </code> which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible
     *  through this session.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active in addition to being effective during the date
     *  range. In any status mode, active and inactive step constrainer enablers are
     *  returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>StepConstrainerEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (getStepConstrainerEnablersOnDate(from, to));
    }


    /**
     *  Gets all <code>StepConstrainerEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainer enablers or an error results. Otherwise, the returned list
     *  may contain only those step constrainer enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainer enablers are returned that are currently
     *  active. In any status mode, active and inactive step constrainer enablers
     *  are returned.
     *
     *  @return a list of <code>StepConstrainerEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.workflow.rules.StepConstrainerEnablerList getStepConstrainerEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the step constrainer enabler list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of step constrainer enablers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.workflow.rules.StepConstrainerEnablerList filterStepConstrainerEnablersOnViews(org.osid.workflow.rules.StepConstrainerEnablerList list)
        throws org.osid.OperationFailedException {

        org.osid.workflow.rules.StepConstrainerEnablerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepconstrainerenabler.ActiveStepConstrainerEnablerFilterList(ret);
        }

        return (ret);
    }
}
