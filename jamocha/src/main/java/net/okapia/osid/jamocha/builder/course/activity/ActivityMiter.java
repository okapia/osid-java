//
// ActivityMiter.java
//
//     Defines an Activity miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.activity;


/**
 *  Defines an <code>Activity</code> miter for use with the builders.
 */

public interface ActivityMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.course.Activity {


    /**
     *  Sets the activity unit.
     *
     *  @param activityUnit an activity unit
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnit</code> is <code>null</code>
     */

    public void setActivityUnit(org.osid.course.ActivityUnit activityUnit);


    /**
     *  Sets the course offering.
     *
     *  @param courseOffering a course offering
     *  @throws org.osid.NullArgumentException
     *          <code>courseOffering</code> is <code>null</code>
     */

    public void setCourseOffering(org.osid.course.CourseOffering courseOffering);


    /**
     *  Sets the term.
     *
     *  @param term a term
     *  @throws org.osid.NullArgumentException <code>term</code> is
     *          <code>null</code>
     */

    public void setTerm(org.osid.course.Term term);


    /**
     *  Sets this activity as implicit.
     *
     *  @param implicit <code> true </code> if this is an implicit
     *         activity, <code> false </code> if an explicit activity
     */

    public void setImplicit(boolean implicit);


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    public void addSchedule(org.osid.calendaring.Schedule schedule);


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @throws org.osid.NullArgumentException <code>schedules</code>
     *          is <code>null</code>
     */

    public void setSchedules(java.util.Collection<org.osid.calendaring.Schedule> schedules);


    /**
     *  Adds a superseding activity.
     *
     *  @param activity a superseding activity
     *  @throws org.osid.NullArgumentException <code>activity</code>
     *          is <code>null</code>
     */

    public void addSupersedingActivity(org.osid.course.Activity activity);


    /**
     *  Sets all the superseding activities.
     *
     *  @param activities a collection of superseding activities
     *  @throws org.osid.NullArgumentException <code>activities</code>
     *          is <code>null</code>
     */

    public void setSupersedingActivities(java.util.Collection<org.osid.course.Activity> activities);


    /**
     *  Adds a specific meeting time.
     *
     *  @param meetingTime a specific meeting time
     *  @throws org.osid.NullArgumentException
     *          <code>meetingTime</code> is <code>null</code>
     */

    public void addSpecificMeetingTime(org.osid.calendaring.MeetingTime meetingTime);


    /**
     *  Sets all the specific meeting times.
     *
     *  @param meetingTimes a collection of specific meeting times
     *  @throws org.osid.NullArgumentException
     *          <code>meetingTimes</code> is <code>null</code>
     */

    public void setSpecificMeetingTimes(java.util.Collection<org.osid.calendaring.MeetingTime> meetingTimes);


    /**
     *  Adds a blackout.
     *
     *  @param blackout a blackout
     *  @throws org.osid.NullArgumentException <code>blackout</code>
     *          is <code>null</code>
     */

    public void addBlackout(org.osid.calendaring.DateTimeInterval blackout);


    /**
     *  Sets all the blackouts.
     *
     *  @param blackouts a collection of blackouts
     *  @throws org.osid.NullArgumentException <code>blackouts</code>
     *          is <code>null</code>
     */

    public void setBlackouts(java.util.Collection<org.osid.calendaring.DateTimeInterval> blackouts);


    /**
     *  Adds an instructor.
     *
     *  @param instructor an instructor
     *  @throws org.osid.NullArgumentException <code>instructor</code>
     *          is <code>null</code>
     */

    public void addInstructor(org.osid.resource.Resource instructor);


    /**
     *  Sets all the instructors.
     *
     *  @param instructors a collection of instructors
     *  @throws org.osid.NullArgumentException
     *          <code>instructors</code> is <code>null</code>
     */

    public void setInstructors(java.util.Collection<org.osid.resource.Resource> instructors);


    /**
     *  Sets the minimum seats.
     *
     *  @param seats a minimum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    public void setMinimumSeats(long seats);


    /**
     *  Sets the maximum seats.
     *
     *  @param seats a maximum seats
     *  @throws org.osid.InvalidArgumentException <code>seats</code>
     *          is negative
     */

    public void setMaximumSeats(long seats);


    /**
     *  Sets the total target effort.
     *
     *  @param effort a total target effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public void setTotalTargetEffort(org.osid.calendaring.Duration effort);


    /**
     *  Sets the contact.
     *
     *  @param contact <code> true </code> if this is a contact
     *         activity, <code> false </code> if an independent
     *         activity
     */

    public void setContact(boolean contact);


    /**
     *  Sets the total target contact time.
     *
     *  @param duration a total target contact time
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public void setTotalTargetContactTime(org.osid.calendaring.Duration duration);


    /**
     *  Sets the total target individual effort.
     *
     *  @param effort a total target individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public void setTotalTargetIndividualEffort(org.osid.calendaring.Duration effort);


    /**
     *  Sets the weekly effort.
     *
     *  @param effort a weekly effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public void setWeeklyEffort(org.osid.calendaring.Duration effort);


    /**
     *  Sets the weekly contact time.
     *
     *  @param time a weekly contact time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    public void setWeeklyContactTime(org.osid.calendaring.Duration time);


    /**
     *  Sets the weekly individual effort.
     *
     *  @param effort a weekly individual effort
     *  @throws org.osid.NullArgumentException <code>effort</code> is
     *          <code>null</code>
     */

    public void setWeeklyIndividualEffort(org.osid.calendaring.Duration effort);


    /**
     *  Adds an Activity record.
     *
     *  @param record an activity record
     *  @param recordType the type of activity record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addActivityRecord(org.osid.course.records.ActivityRecord record, org.osid.type.Type recordType);
}       


