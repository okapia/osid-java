//
// MutableIndexedMapProxyCreditLookupSession
//
//    Implements a Credit lookup service backed by a collection of
//    credits indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.acknowledgement;


/**
 *  Implements a Credit lookup service backed by a collection of
 *  credits. The credits are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some credits may be compatible
 *  with more types than are indicated through these credit
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of credits can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyCreditLookupSession
    extends net.okapia.osid.jamocha.core.acknowledgement.spi.AbstractIndexedMapCreditLookupSession
    implements org.osid.acknowledgement.CreditLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCreditLookupSession} with
     *  no credit.
     *
     *  @param billing the billing
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billing} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCreditLookupSession(org.osid.acknowledgement.Billing billing,
                                                       org.osid.proxy.Proxy proxy) {
        setBilling(billing);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCreditLookupSession} with
     *  a single credit.
     *
     *  @param billing the billing
     *  @param  credit an credit
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billing},
     *          {@code credit}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCreditLookupSession(org.osid.acknowledgement.Billing billing,
                                                       org.osid.acknowledgement.Credit credit, org.osid.proxy.Proxy proxy) {

        this(billing, proxy);
        putCredit(credit);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCreditLookupSession} using
     *  an array of credits.
     *
     *  @param billing the billing
     *  @param  credits an array of credits
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billing},
     *          {@code credits}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCreditLookupSession(org.osid.acknowledgement.Billing billing,
                                                       org.osid.acknowledgement.Credit[] credits, org.osid.proxy.Proxy proxy) {

        this(billing, proxy);
        putCredits(credits);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyCreditLookupSession} using
     *  a collection of credits.
     *
     *  @param billing the billing
     *  @param  credits a collection of credits
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code billing},
     *          {@code credits}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyCreditLookupSession(org.osid.acknowledgement.Billing billing,
                                                       java.util.Collection<? extends org.osid.acknowledgement.Credit> credits,
                                                       org.osid.proxy.Proxy proxy) {
        this(billing, proxy);
        putCredits(credits);
        return;
    }

    
    /**
     *  Makes a {@code Credit} available in this session.
     *
     *  @param  credit a credit
     *  @throws org.osid.NullArgumentException {@code credit{@code 
     *          is {@code null}
     */

    @Override
    public void putCredit(org.osid.acknowledgement.Credit credit) {
        super.putCredit(credit);
        return;
    }


    /**
     *  Makes an array of credits available in this session.
     *
     *  @param  credits an array of credits
     *  @throws org.osid.NullArgumentException {@code credits{@code 
     *          is {@code null}
     */

    @Override
    public void putCredits(org.osid.acknowledgement.Credit[] credits) {
        super.putCredits(credits);
        return;
    }


    /**
     *  Makes collection of credits available in this session.
     *
     *  @param  credits a collection of credits
     *  @throws org.osid.NullArgumentException {@code credit{@code 
     *          is {@code null}
     */

    @Override
    public void putCredits(java.util.Collection<? extends org.osid.acknowledgement.Credit> credits) {
        super.putCredits(credits);
        return;
    }


    /**
     *  Removes a Credit from this session.
     *
     *  @param creditId the {@code Id} of the credit
     *  @throws org.osid.NullArgumentException {@code creditId{@code  is
     *          {@code null}
     */

    @Override
    public void removeCredit(org.osid.id.Id creditId) {
        super.removeCredit(creditId);
        return;
    }    
}
