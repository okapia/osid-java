//
// AbstractProperty
//
//     Implements a property.
//
//
// Tom Coppeto
// OnTapSolutions
// 20 September 2008
//
//
// Copyright (c) 2008 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.property.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a Property.
 */

public abstract class AbstractProperty
    implements org.osid.Property {

    private final org.osid.locale.DisplayText displayName;
    private final org.osid.locale.DisplayText displayLabel;
    private final org.osid.locale.DisplayText description;
    private String value = "";


    /**
     *  Constructs a new <code>AbstractProperty</code> with no value.
     *
     *  @param displayName the property display name
     *  @param displayLabel a property display label
     *  @param description the property description
     *  @throws org.osid.NullArgumentException
     *          <code>displayName</code>, <code>displayLabel</code> or
     *          <code>description</code> is <code>null</code>
     */

    protected AbstractProperty(org.osid.locale.DisplayText displayName, 
                               org.osid.locale.DisplayText displayLabel, 
                               org.osid.locale.DisplayText description) {

        nullarg(displayName, "display name");
        nullarg(displayLabel, "display label");
        nullarg(description, "description");
        
        this.displayName  = displayName;
        this.displayLabel = displayLabel;
        this.description  = description;

        return;
    }


    /**
     *  Constructs a new <code>AbstractProperty</code>.
     *
     *  @param displayName the property display name
     *  @param displayLabel a property display label
     *  @param description the property description
     *  @param value the property value
     *  @throws org.osid.NullArgumentException
     *          <code>displayName</code>, <code>displayLabel</code>,
     *          <code>description</code> or <code>value</code> is
     *          <code>null</code>
     */

    protected AbstractProperty(org.osid.locale.DisplayText displayName, 
                               org.osid.locale.DisplayText displayLabel, 
                               org.osid.locale.DisplayText description, 
                               String value) {

        this(displayName, displayLabel, description);

        nullarg(value, "value");
        this.value = value;

        return;
    }


    /**
     *  Constructs a new <code>AbstractProperty</code>.
     *
     *  @param property
     *  @param value the property value
     *  @throws org.osid.NullArgumentException <code>property</code>
     *          or <code>value</code> is
     *          <code>null</code>
     */

    protected AbstractProperty(org.osid.Property property, String value) {
        nullarg(property, "property");
        nullarg(value, "value");

        this.displayName  = property.getDisplayName();
        this.description  = property.getDescription();
        this.displayLabel = property.getDisplayLabel();
        this.value        = value;

        return;
    }


    /**
     *  The display name for this property. 
     *
     *  @return the display name 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayName() {
        return (this.displayName);
    }


    /**
     *  A short display label. 
     *
     *  @return the display label 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.displayLabel);
    }


    /**
     *  A description of this property. 
     *
     *  @return the description 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDescription() {
        return (this.description);
    }


    /**
     *  The values of this property, sorted by index.
     *
     *  @return the value 
     */

    @OSID @Override
    public String getValue() {
        return (this.value);
    }


    /**
     *  Set the value for this property.
     *
     *  @param value
     *  @throws org.osid.NullArgumentException <code>value</code>
     *          is <code>null</code>
     */

    protected void setValue(String value) {
        this.value = value;
        return;
    }


    @Override
    public String toString() {
        return ("Property: " + getDisplayLabel() + "=" + getValue());
    }
}
