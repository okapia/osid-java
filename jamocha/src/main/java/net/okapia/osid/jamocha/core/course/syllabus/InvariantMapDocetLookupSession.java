//
// InvariantMapDocetLookupSession
//
//    Implements a Docet lookup service backed by a fixed collection of
//    docets.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.syllabus;


/**
 *  Implements a Docet lookup service backed by a fixed
 *  collection of docets. The docets are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapDocetLookupSession
    extends net.okapia.osid.jamocha.core.course.syllabus.spi.AbstractMapDocetLookupSession
    implements org.osid.course.syllabus.DocetLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapDocetLookupSession</code> with no
     *  docets.
     *  
     *  @param courseCatalog the course catalog
     *  @throws org.osid.NullArgumnetException {@code courseCatalog} is
     *          {@code null}
     */

    public InvariantMapDocetLookupSession(org.osid.course.CourseCatalog courseCatalog) {
        setCourseCatalog(courseCatalog);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDocetLookupSession</code> with a single
     *  docet.
     *  
     *  @param courseCatalog the course catalog
     *  @param docet a single docet
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code docet} is <code>null</code>
     */

      public InvariantMapDocetLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.syllabus.Docet docet) {
        this(courseCatalog);
        putDocet(docet);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDocetLookupSession</code> using an array
     *  of docets.
     *  
     *  @param courseCatalog the course catalog
     *  @param docets an array of docets
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code docets} is <code>null</code>
     */

      public InvariantMapDocetLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               org.osid.course.syllabus.Docet[] docets) {
        this(courseCatalog);
        putDocets(docets);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapDocetLookupSession</code> using a
     *  collection of docets.
     *
     *  @param courseCatalog the course catalog
     *  @param docets a collection of docets
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code docets} is <code>null</code>
     */

      public InvariantMapDocetLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                               java.util.Collection<? extends org.osid.course.syllabus.Docet> docets) {
        this(courseCatalog);
        putDocets(docets);
        return;
    }
}
