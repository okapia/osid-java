//
// PackageMiter.java
//
//     Defines a Package miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.installation.pkg;


/**
 *  Defines a <code>Package</code> miter for use with the builders.
 */

public interface PackageMiter
    extends net.okapia.osid.jamocha.builder.spi.SourceableOsidObjectMiter,
            org.osid.installation.Package {


    /**
     *  Sets the version.
     *
     *  @param version a version
     *  @throws org.osid.NullArgumentException <code>version</code> is
     *          <code>null</code>
     */

    public void setVersion(org.osid.installation.Version version);


    /**
     *  Sets the copyright.
     *
     *  @param copyright a copyright
     *  @throws org.osid.NullArgumentException <code>copyright</code>
     *          is <code>null</code>
     */

    public void setCopyright(org.osid.locale.DisplayText copyright);


    /**
     *  Sets the creator.
     *
     *  @param creator a creator
     *  @throws org.osid.NullArgumentException <code>creator</code> is
     *          <code>null</code>
     */

    public void setCreator(org.osid.resource.Resource creator);


    /**
     *  Sets the release date.
     *
     *  @param date a release date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setReleaseDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds a dependency.
     *
     *  @param pkg a dependency
     *  @throws org.osid.NullArgumentException <code>pkg</code> is
     *          <code>null</code>
     */

    public void addDependency(org.osid.installation.Package pkg);


    /**
     *  Sets all the package dependencies.
     *
     *  @param packages a collection of packages
     *  @throws org.osid.NullArgumentException <code>packages</code>
     *          is <code>null</code>
     */

    public void setDependencies(java.util.Collection<org.osid.installation.Package> packages);


    /**
     *  Sets the URL.
     *
     *  @param url the URL
     *  @throws org.osid.NullArgumentException <code>url</code> is
     *          <code>null</code>
     */

    public void setURL(String url);


    /**
     *  Adds an installation content.
     *
     *  @param content an installation content
     *  @throws org.osid.NullArgumentException <code>content</code> is
     *          <code>null</code>
     */

    public void addInstallationContent(org.osid.installation.InstallationContent content); 


    /**
     *  Sets all the installation contents.
     *
     *  @param contents a collection of contents
     *  @throws org.osid.NullArgumentException <code>contents</code>
     *          is <code>null</code>
     */

    public void setInstallationContents(java.util.Collection<org.osid.installation.InstallationContent> contents);


    /**
     *  Adds a Package record.
     *
     *  @param record a pkg record
     *  @param recordType the type of pkg record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addPackageRecord(org.osid.installation.records.PackageRecord record, org.osid.type.Type recordType);
}       


