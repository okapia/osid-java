//
// AbstractVotingBatchManager.java
//
//     An adapter for a VotingBatchManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.voting.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a VotingBatchManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterVotingBatchManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.voting.batch.VotingBatchManager>
    implements org.osid.voting.batch.VotingBatchManager {


    /**
     *  Constructs a new {@code AbstractAdapterVotingBatchManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterVotingBatchManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterVotingBatchManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterVotingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if bulk voting is available. 
     *
     *  @return <code> true </code> if a voting bulk service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVotingBatchAdmin() {
        return (getAdapteeManager().supportsVotingBatchAdmin());
    }


    /**
     *  Tests if bulk administration of candidates is available. 
     *
     *  @return <code> true </code> if a candidate bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateBatchAdmin() {
        return (getAdapteeManager().supportsCandidateBatchAdmin());
    }


    /**
     *  Tests if bulk administration of races is available. 
     *
     *  @return <code> true </code> if a race bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRaceBatchAdmin() {
        return (getAdapteeManager().supportsRaceBatchAdmin());
    }


    /**
     *  Tests if bulk administration of ballots is available. 
     *
     *  @return <code> true </code> if a ballot bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBallotBatchAdmin() {
        return (getAdapteeManager().supportsBallotBatchAdmin());
    }


    /**
     *  Tests if bulk administration of polls is available. 
     *
     *  @return <code> true </code> if a polls bulk administrative service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsBatchAdmin() {
        return (getAdapteeManager().supportsPollsBatchAdmin());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk voting 
     *  service. 
     *
     *  @return a <code> VotingBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.VotingBatchSession getVotingBatchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getVotingBatchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk voting 
     *  service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> VotingBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsVotingBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.VotingBatchSession getVoteBatchSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getVoteBatchSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk candidate 
     *  administration service. 
     *
     *  @return a <code> CandidateBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.CandidateBatchAdminSession getCandidateBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk candidate 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> CandidateBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.CandidateBatchAdminSession getCandidateBatchAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getCandidateBatchAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk race 
     *  administration service. 
     *
     *  @return a <code> RaceBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.RaceBatchAdminSession getRaceBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk race 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> RaceBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRaceBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.RaceBatchAdminSession getRaceBatchAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRaceBatchAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ballot 
     *  administration service. 
     *
     *  @return a <code> BallotBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.BallotBatchAdminSession getBallotBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotBatchAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk ballot 
     *  administration service for the given polls. 
     *
     *  @param  pollsId the <code> Id </code> of the <code> Polls </code> 
     *  @return a <code> BallotBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Polls </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBallotBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.BallotBatchAdminSession getBallotBatchAdminSessionForPolls(org.osid.id.Id pollsId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getBallotBatchAdminSessionForPolls(pollsId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk polls 
     *  administration service. 
     *
     *  @return a <code> PollsBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPollsBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.batch.PollsBatchAdminSession getPollsBatchAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getPollsBatchAdminSession());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
