//
// AbstractImmutableProficiency.java
//
//     Wraps a mutable Proficiency to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.learning.proficiency.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Proficiency</code> to hide modifiers. This
 *  wrapper provides an immutized Proficiency from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying proficiency whose state changes are visible.
 */

public abstract class AbstractImmutableProficiency
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRelationship
    implements org.osid.learning.Proficiency {

    private final org.osid.learning.Proficiency proficiency;


    /**
     *  Constructs a new <code>AbstractImmutableProficiency</code>.
     *
     *  @param proficiency the proficiency to immutablize
     *  @throws org.osid.NullArgumentException <code>proficiency</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableProficiency(org.osid.learning.Proficiency proficiency) {
        super(proficiency);
        this.proficiency = proficiency;
        return;
    }


    /**
     *  Gets the resource <code> Id </code> to whom this proficiency applies. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.proficiency.getResourceId());
    }


    /**
     *  Gets the resource to whom this proficiency applies. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.proficiency.getResource());
    }


    /**
     *  Gets the objective <code> Id </code> to whom this proficiency applies. 
     *
     *  @return the objective <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getObjectiveId() {
        return (this.proficiency.getObjectiveId());
    }


    /**
     *  Gets the objective to whom this proficiency applies. 
     *
     *  @return the objective 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.learning.Objective getObjective()
        throws org.osid.OperationFailedException {

        return (this.proficiency.getObjective());
    }


    /**
     *  Gets the completion of this objective as a percentage 0-100. 
     *
     *  @return the completion 
     */

    @OSID @Override
    public java.math.BigDecimal getCompletion() {
        return (this.proficiency.getCompletion());
    }


    /**
     *  Tests if a proficiency level is available. 
     *
     *  @return <code> true </code> if a level is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasLevel() {
        return (this.proficiency.hasLevel());
    }


    /**
     *  Gets the proficiency level expressed as a grade. 
     *
     *  @return the grade <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasLevel() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getLevelId() {
        return (this.proficiency.getLevelId());
    }


    /**
     *  Gets the proficiency level expressed as a grade. 
     *
     *  @return the grade 
     *  @throws org.osid.IllegalStateException <code> hasLevel() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getLevel()
        throws org.osid.OperationFailedException {

        return (this.proficiency.getLevel());
    }


    /**
     *  Gets the proficiency record corresponding to the given <code> 
     *  Proficiency </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  proficiencyRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(proficiencyRecordType) </code> is <code> true </code> . 
     *
     *  @param  proficiencyRecordType the type of proficiency record to 
     *          retrieve 
     *  @return the proficiency record 
     *  @throws org.osid.NullArgumentException <code> proficiencyRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(proficiencyRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.learning.records.ProficiencyRecord getProficiencyRecord(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException {

        return (this.proficiency.getProficiencyRecord(proficiencyRecordType));
    }
}

