//
// AbstractAssemblyPollsQuery.java
//
//     A PollsQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.polls.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A PollsQuery that stores terms.
 */

public abstract class AbstractAssemblyPollsQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidCatalogQuery
    implements org.osid.voting.PollsQuery,
               org.osid.voting.PollsQueryInspector,
               org.osid.voting.PollsSearchOrder {

    private final java.util.Collection<org.osid.voting.records.PollsQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.records.PollsQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.records.PollsSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyPollsQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyPollsQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the candidate <code> Id </code> for this query. 
     *
     *  @param  candidateId a candidate <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> candidateId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCandidateId(org.osid.id.Id candidateId, boolean match) {
        getAssembler().addIdTerm(getCandidateIdColumn(), candidateId, match);
        return;
    }


    /**
     *  Clears the candidate <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCandidateIdTerms() {
        getAssembler().clearTerms(getCandidateIdColumn());
        return;
    }


    /**
     *  Gets the candidate <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCandidateIdTerms() {
        return (getAssembler().getIdTerms(getCandidateIdColumn()));
    }


    /**
     *  Gets the CandidateId column name.
     *
     * @return the column name
     */

    protected String getCandidateIdColumn() {
        return ("candidate_id");
    }


    /**
     *  Tests if a <code> CandidateQuery </code> is available. 
     *
     *  @return <code> true </code> if a candidate query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCandidateQuery() {
        return (false);
    }


    /**
     *  Gets the query for a candidate. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the candidate query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCandidateQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.CandidateQuery getCandidateQuery() {
        throw new org.osid.UnimplementedException("supportsCandidateQuery() is false");
    }


    /**
     *  Matches polls with any candidate. 
     *
     *  @param  match <code> true </code> to match polls with any candidate, 
     *          <code> false </code> to match polls with no candidates 
     */

    @OSID @Override
    public void matchAnyCandidate(boolean match) {
        getAssembler().addIdWildcardTerm(getCandidateColumn(), match);
        return;
    }


    /**
     *  Clears the candidate terms. 
     */

    @OSID @Override
    public void clearCandidateTerms() {
        getAssembler().clearTerms(getCandidateColumn());
        return;
    }


    /**
     *  Gets the candidate query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.CandidateQueryInspector[] getCandidateTerms() {
        return (new org.osid.voting.CandidateQueryInspector[0]);
    }


    /**
     *  Gets the Candidate column name.
     *
     * @return the column name
     */

    protected String getCandidateColumn() {
        return ("candidate");
    }


    /**
     *  Sets the polls <code> Id </code> for this query to match polls that 
     *  have the specified polls as an ancestor. 
     *
     *  @param  pollsid a polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorPollsId(org.osid.id.Id pollsid, boolean match) {
        getAssembler().addIdTerm(getAncestorPollsIdColumn(), pollsid, match);
        return;
    }


    /**
     *  Clears the ancestor polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAncestorPollsIdTerms() {
        getAssembler().clearTerms(getAncestorPollsIdColumn());
        return;
    }


    /**
     *  Gets the ancestor polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAncestorPollsIdTerms() {
        return (getAssembler().getIdTerms(getAncestorPollsIdColumn()));
    }


    /**
     *  Gets the AncestorPollsId column name.
     *
     * @return the column name
     */

    protected String getAncestorPollsIdColumn() {
        return ("ancestor_polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorPollsQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getAncestorPollsQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorPollsQuery() is false");
    }


    /**
     *  Matches polls with any ancestor. 
     *
     *  @param  match <code> true </code> to match polls with any ancestor, 
     *          <code> false </code> to match root polls 
     */

    @OSID @Override
    public void matchAnyAncestorPolls(boolean match) {
        getAssembler().addIdWildcardTerm(getAncestorPollsColumn(), match);
        return;
    }


    /**
     *  Clears the ancestor polls terms. 
     */

    @OSID @Override
    public void clearAncestorPollsTerms() {
        getAssembler().clearTerms(getAncestorPollsColumn());
        return;
    }


    /**
     *  Gets the ancestor polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getAncestorPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the AncestorPolls column name.
     *
     * @return the column name
     */

    protected String getAncestorPollsColumn() {
        return ("ancestor_polls");
    }


    /**
     *  Sets the polls <code> Id </code> for this query to match polls that 
     *  have the specified polls as a descendant. 
     *
     *  @param  pollsid a polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantPollsId(org.osid.id.Id pollsid, boolean match) {
        getAssembler().addIdTerm(getDescendantPollsIdColumn(), pollsid, match);
        return;
    }


    /**
     *  Clears the descendant polls <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearDescendantPollsIdTerms() {
        getAssembler().clearTerms(getDescendantPollsIdColumn());
        return;
    }


    /**
     *  Gets the descendant polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getDescendantPollsIdTerms() {
        return (getAssembler().getIdTerms(getDescendantPollsIdColumn()));
    }


    /**
     *  Gets the DescendantPollsId column name.
     *
     * @return the column name
     */

    protected String getDescendantPollsIdColumn() {
        return ("descendant_polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantPollsQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getDescendantPollsQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantPollsQuery() is false");
    }


    /**
     *  Matches polls with any descendant. 
     *
     *  @param  match <code> true </code> to match polls with any descendant, 
     *          <code> false </code> to match leaf polls 
     */

    @OSID @Override
    public void matchAnyDescendantPolls(boolean match) {
        getAssembler().addIdWildcardTerm(getDescendantPollsColumn(), match);
        return;
    }


    /**
     *  Clears the descendant polls terms. 
     */

    @OSID @Override
    public void clearDescendantPollsTerms() {
        getAssembler().clearTerms(getDescendantPollsColumn());
        return;
    }


    /**
     *  Gets the descendant polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getDescendantPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the DescendantPolls column name.
     *
     * @return the column name
     */

    protected String getDescendantPollsColumn() {
        return ("descendant_polls");
    }


    /**
     *  Tests if this polls supports the given record
     *  <code>Type</code>.
     *
     *  @param  pollsRecordType a polls record type 
     *  @return <code>true</code> if the pollsRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type pollsRecordType) {
        for (org.osid.voting.records.PollsQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(pollsRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  pollsRecordType the polls record type 
     *  @return the polls query record 
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pollsRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.PollsQueryRecord getPollsQueryRecord(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.PollsQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(pollsRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pollsRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  pollsRecordType the polls record type 
     *  @return the polls query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pollsRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.PollsQueryInspectorRecord getPollsQueryInspectorRecord(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.PollsQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(pollsRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pollsRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param pollsRecordType the polls record type
     *  @return the polls search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>pollsRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(pollsRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.PollsSearchOrderRecord getPollsSearchOrderRecord(org.osid.type.Type pollsRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.PollsSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(pollsRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(pollsRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this polls. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param pollsQueryRecord the polls query record
     *  @param pollsQueryInspectorRecord the polls query inspector
     *         record
     *  @param pollsSearchOrderRecord the polls search order record
     *  @param pollsRecordType polls record type
     *  @throws org.osid.NullArgumentException
     *          <code>pollsQueryRecord</code>,
     *          <code>pollsQueryInspectorRecord</code>,
     *          <code>pollsSearchOrderRecord</code> or
     *          <code>pollsRecordTypepolls</code> is
     *          <code>null</code>
     */
            
    protected void addPollsRecords(org.osid.voting.records.PollsQueryRecord pollsQueryRecord, 
                                      org.osid.voting.records.PollsQueryInspectorRecord pollsQueryInspectorRecord, 
                                      org.osid.voting.records.PollsSearchOrderRecord pollsSearchOrderRecord, 
                                      org.osid.type.Type pollsRecordType) {

        addRecordType(pollsRecordType);

        nullarg(pollsQueryRecord, "polls query record");
        nullarg(pollsQueryInspectorRecord, "polls query inspector record");
        nullarg(pollsSearchOrderRecord, "polls search odrer record");

        this.queryRecords.add(pollsQueryRecord);
        this.queryInspectorRecords.add(pollsQueryInspectorRecord);
        this.searchOrderRecords.add(pollsSearchOrderRecord);
        
        return;
    }
}
