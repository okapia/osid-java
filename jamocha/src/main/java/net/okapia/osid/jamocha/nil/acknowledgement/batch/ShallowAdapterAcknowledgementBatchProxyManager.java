//
// ShallowAcknowledgementBatchProxyManager.java
//
//     A nothing AcknowledgementBatchProxyManager adapter.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.acknowledgement.batch;


/**
 *  A nothing AcknowledgementBatchProxyManager adapter.
 */

public final class ShallowAdapterAcknowledgementBatchProxyManager
    extends net.okapia.osid.jamocha.adapter.acknowledgement.batch.spi.AbstractAdapterAcknowledgementBatchProxyManager
    implements org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager {


    /**
     *  Constructs a new {@code ShallowAdapterAcknowledgementBatchProxyManager} using
     *  the underlying provider.
     *
     *  @param manager the manager to adapt
     *  @throws org.osid.NullArgumentException {@code manager} is
     *          {@code null}
     */

    public ShallowAdapterAcknowledgementBatchProxyManager(org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager manager) {
        setAdapteeManager(manager);
        return;
    }


    /**
     *  Constructs a new {@code ShallowAdapterAcknowledgementBatchProxyManager}.
     *
     *  @param manager the manager to adapt
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code manager} or
     *          {@code provider} is {@code null}
     */

    public ShallowAdapterAcknowledgementBatchProxyManager(org.osid.acknowledgement.batch.AcknowledgementBatchProxyManager manager,
                                               net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        setAdapteeManager(manager);
        return;
    }
}
