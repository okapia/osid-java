//
// AbstractIndexedMapLogLookupSession.java
//
//    A simple framework for providing a Log lookup service
//    backed by a fixed collection of logs with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.logging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Log lookup service backed by a
 *  fixed collection of logs. The logs are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some logs may be compatible
 *  with more types than are indicated through these log
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Logs</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapLogLookupSession
    extends AbstractMapLogLookupSession
    implements org.osid.logging.LogLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.logging.Log> logsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.logging.Log>());
    private final MultiMap<org.osid.type.Type, org.osid.logging.Log> logsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.logging.Log>());


    /**
     *  Makes a <code>Log</code> available in this session.
     *
     *  @param  log a log
     *  @throws org.osid.NullArgumentException <code>log<code> is
     *          <code>null</code>
     */

    @Override
    protected void putLog(org.osid.logging.Log log) {
        super.putLog(log);

        this.logsByGenus.put(log.getGenusType(), log);
        
        try (org.osid.type.TypeList types = log.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.logsByRecord.put(types.getNextType(), log);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a log from this session.
     *
     *  @param logId the <code>Id</code> of the log
     *  @throws org.osid.NullArgumentException <code>logId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeLog(org.osid.id.Id logId) {
        org.osid.logging.Log log;
        try {
            log = getLog(logId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.logsByGenus.remove(log.getGenusType());

        try (org.osid.type.TypeList types = log.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.logsByRecord.remove(types.getNextType(), log);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeLog(logId);
        return;
    }


    /**
     *  Gets a <code>LogList</code> corresponding to the given
     *  log genus <code>Type</code> which does not include
     *  logs of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known logs or an error results. Otherwise,
     *  the returned list may contain only those logs that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  logGenusType a log genus type 
     *  @return the returned <code>Log</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByGenusType(org.osid.type.Type logGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.logging.log.ArrayLogList(this.logsByGenus.get(logGenusType)));
    }


    /**
     *  Gets a <code>LogList</code> containing the given
     *  log record <code>Type</code>. In plenary mode, the
     *  returned list contains all known logs or an error
     *  results. Otherwise, the returned list may contain only those
     *  logs that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  logRecordType a log record type 
     *  @return the returned <code>log</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>logRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.logging.LogList getLogsByRecordType(org.osid.type.Type logRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.logging.log.ArrayLogList(this.logsByRecord.get(logRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.logsByGenus.clear();
        this.logsByRecord.clear();

        super.close();

        return;
    }
}
