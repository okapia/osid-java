//
// AbstractRecognitionManager.java
//
//     An adapter for a RecognitionManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.recognition.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RecognitionManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRecognitionManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.recognition.RecognitionManager>
    implements org.osid.recognition.RecognitionManager {


    /**
     *  Constructs a new {@code AbstractAdapterRecognitionManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRecognitionManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRecognitionManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRecognitionManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any award federation is exposed. Federation is exposed when a 
     *  specific award may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of awards 
     *  appears as a single award. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests for the availability of a conferral lookup service. 
     *
     *  @return <code> true </code> if conferral lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralLookup() {
        return (getAdapteeManager().supportsConferralLookup());
    }


    /**
     *  Tests if querying conferrals is available. 
     *
     *  @return <code> true </code> if conferral query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralQuery() {
        return (getAdapteeManager().supportsConferralQuery());
    }


    /**
     *  Tests if searching for conferrals is available. 
     *
     *  @return <code> true </code> if conferral search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralSearch() {
        return (getAdapteeManager().supportsConferralSearch());
    }


    /**
     *  Tests if searching for conferrals is available. 
     *
     *  @return <code> true </code> if conferral search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralAdmin() {
        return (getAdapteeManager().supportsConferralAdmin());
    }


    /**
     *  Tests if conferral notification is available. 
     *
     *  @return <code> true </code> if conferral notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralNotification() {
        return (getAdapteeManager().supportsConferralNotification());
    }


    /**
     *  Tests if a conferral to academy lookup session is available. 
     *
     *  @return <code> true </code> if conferral academy lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralAcademy() {
        return (getAdapteeManager().supportsConferralAcademy());
    }


    /**
     *  Tests if a conferral to academy assignment session is available. 
     *
     *  @return <code> true </code> if conferral academy assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralAcademyAssignment() {
        return (getAdapteeManager().supportsConferralAcademyAssignment());
    }


    /**
     *  Tests if a conferral smart academy session is available. 
     *
     *  @return <code> true </code> if conferral smart academy is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConferralSmartAcademy() {
        return (getAdapteeManager().supportsConferralSmartAcademy());
    }


    /**
     *  Tests for the availability of an award lookup service. 
     *
     *  @return <code> true </code> if award lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardLookup() {
        return (getAdapteeManager().supportsAwardLookup());
    }


    /**
     *  Tests if querying awards is available. 
     *
     *  @return <code> true </code> if award query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardQuery() {
        return (getAdapteeManager().supportsAwardQuery());
    }


    /**
     *  Tests if searching for awards is available. 
     *
     *  @return <code> true </code> if award search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardSearch() {
        return (getAdapteeManager().supportsAwardSearch());
    }


    /**
     *  Tests for the availability of an award administrative service for 
     *  creating and deleting awards. 
     *
     *  @return <code> true </code> if award administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardAdmin() {
        return (getAdapteeManager().supportsAwardAdmin());
    }


    /**
     *  Tests for the availability of an award notification service. 
     *
     *  @return <code> true </code> if award notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardNotification() {
        return (getAdapteeManager().supportsAwardNotification());
    }


    /**
     *  Tests if an award to academy lookup session is available. 
     *
     *  @return <code> true </code> if award academy lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardAcademy() {
        return (getAdapteeManager().supportsAwardAcademy());
    }


    /**
     *  Tests if an award to academy assignment session is available. 
     *
     *  @return <code> true </code> if award academy assignment is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardAcademyAssignment() {
        return (getAdapteeManager().supportsAwardAcademyAssignment());
    }


    /**
     *  Tests if an award smart academy session is available. 
     *
     *  @return <code> true </code> if award smart academy is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAwardSmartAcademy() {
        return (getAdapteeManager().supportsAwardSmartAcademy());
    }


    /**
     *  Tests for the availability of a convocation service for getting 
     *  available convocations for a resource. 
     *
     *  @return <code> true </code> if convocation is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocation() {
        return (getAdapteeManager().supportsConvocation());
    }


    /**
     *  Tests for the availability of a convocation lookup service. 
     *
     *  @return <code> true </code> if convocation lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationLookup() {
        return (getAdapteeManager().supportsConvocationLookup());
    }


    /**
     *  Tests if querying convocations is available. 
     *
     *  @return <code> true </code> if convocation query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationQuery() {
        return (getAdapteeManager().supportsConvocationQuery());
    }


    /**
     *  Tests if searching for convocations is available. 
     *
     *  @return <code> true </code> if convocation search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationSearch() {
        return (getAdapteeManager().supportsConvocationSearch());
    }


    /**
     *  Tests if searching for convocations is available. 
     *
     *  @return <code> true </code> if convocation search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationAdmin() {
        return (getAdapteeManager().supportsConvocationAdmin());
    }


    /**
     *  Tests if convocation notification is available. 
     *
     *  @return <code> true </code> if convocation notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationNotification() {
        return (getAdapteeManager().supportsConvocationNotification());
    }


    /**
     *  Tests if a convocation to academy lookup session is available. 
     *
     *  @return <code> true </code> if convocation academy lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationAcademy() {
        return (getAdapteeManager().supportsConvocationAcademy());
    }


    /**
     *  Tests if a convocation to academy assignment session is available. 
     *
     *  @return <code> true </code> if convocation academy assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationAcademyAssignment() {
        return (getAdapteeManager().supportsConvocationAcademyAssignment());
    }


    /**
     *  Tests if a convocation smart academy session is available. 
     *
     *  @return <code> true </code> if convocation smart academy is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsConvocationSmartAcademy() {
        return (getAdapteeManager().supportsConvocationSmartAcademy());
    }


    /**
     *  Tests for the availability of an academy lookup service. 
     *
     *  @return <code> true </code> if academy lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyLookup() {
        return (getAdapteeManager().supportsAcademyLookup());
    }


    /**
     *  Tests if querying academies is available. 
     *
     *  @return <code> true </code> if academy query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyQuery() {
        return (getAdapteeManager().supportsAcademyQuery());
    }


    /**
     *  Tests if searching for academies is available. 
     *
     *  @return <code> true </code> if academy search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademySearch() {
        return (getAdapteeManager().supportsAcademySearch());
    }


    /**
     *  Tests for the availability of an academy administrative service for 
     *  creating and deleting academies. 
     *
     *  @return <code> true </code> if academy administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyAdmin() {
        return (getAdapteeManager().supportsAcademyAdmin());
    }


    /**
     *  Tests for the availability of an academy notification service. 
     *
     *  @return <code> true </code> if academy notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyNotification() {
        return (getAdapteeManager().supportsAcademyNotification());
    }


    /**
     *  Tests for the availability of an academy hierarchy traversal service. 
     *
     *  @return <code> true </code> if academy hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyHierarchy() {
        return (getAdapteeManager().supportsAcademyHierarchy());
    }


    /**
     *  Tests for the availability of an academy hierarchy design service. 
     *
     *  @return <code> true </code> if academy hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAcademyHierarchyDesign() {
        return (getAdapteeManager().supportsAcademyHierarchyDesign());
    }


    /**
     *  Tests for the availability of a recognition batch service. 
     *
     *  @return <code> true </code> if a recognition batch service is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecognitionBatch() {
        return (getAdapteeManager().supportsRecognitionBatch());
    }


    /**
     *  Gets the supported <code> Conferral </code> record types. 
     *
     *  @return a list containing the supported conferral record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConferralRecordTypes() {
        return (getAdapteeManager().getConferralRecordTypes());
    }


    /**
     *  Tests if the given <code> Conferral </code> record type is supported. 
     *
     *  @param  conferralRecordType a <code> Type </code> indicating a <code> 
     *          Conferral </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> conferralRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConferralRecordType(org.osid.type.Type conferralRecordType) {
        return (getAdapteeManager().supportsConferralRecordType(conferralRecordType));
    }


    /**
     *  Gets the supported conferral search record types. 
     *
     *  @return a list containing the supported conferral search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConferralSearchRecordTypes() {
        return (getAdapteeManager().getConferralSearchRecordTypes());
    }


    /**
     *  Tests if the given conferral search record type is supported. 
     *
     *  @param  conferralSearchRecordType a <code> Type </code> indicating a 
     *          conferral record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          conferralSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConferralSearchRecordType(org.osid.type.Type conferralSearchRecordType) {
        return (getAdapteeManager().supportsConferralSearchRecordType(conferralSearchRecordType));
    }


    /**
     *  Gets the supported <code> Award </code> record types. 
     *
     *  @return a list containing the supported award record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardRecordTypes() {
        return (getAdapteeManager().getAwardRecordTypes());
    }


    /**
     *  Tests if the given <code> Award </code> record type is supported. 
     *
     *  @param  awardRecordType a <code> Type </code> indicating a <code> 
     *          Award </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> awardRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardRecordType(org.osid.type.Type awardRecordType) {
        return (getAdapteeManager().supportsAwardRecordType(awardRecordType));
    }


    /**
     *  Gets the supported award search record types. 
     *
     *  @return a list containing the supported award search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAwardSearchRecordTypes() {
        return (getAdapteeManager().getAwardSearchRecordTypes());
    }


    /**
     *  Tests if the given award search record type is supported. 
     *
     *  @param  awardSearchRecordType a <code> Type </code> indicating an 
     *          award record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> awardSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAwardSearchRecordType(org.osid.type.Type awardSearchRecordType) {
        return (getAdapteeManager().supportsAwardSearchRecordType(awardSearchRecordType));
    }


    /**
     *  Gets the supported <code> Convocation </code> record types. 
     *
     *  @return a list containing the supported convocation record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConvocationRecordTypes() {
        return (getAdapteeManager().getConvocationRecordTypes());
    }


    /**
     *  Tests if the given <code> Convocation </code> record type is 
     *  supported. 
     *
     *  @param  convocationRecordType a <code> Type </code> indicating a 
     *          <code> Convocation </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> convocationRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConvocationRecordType(org.osid.type.Type convocationRecordType) {
        return (getAdapteeManager().supportsConvocationRecordType(convocationRecordType));
    }


    /**
     *  Gets the supported convocation search record types. 
     *
     *  @return a list containing the supported convocation search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getConvocationSearchRecordTypes() {
        return (getAdapteeManager().getConvocationSearchRecordTypes());
    }


    /**
     *  Tests if the given convocation search record type is supported. 
     *
     *  @param  convocationSearchRecordType a <code> Type </code> indicating a 
     *          convocation record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          convocationSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsConvocationSearchRecordType(org.osid.type.Type convocationSearchRecordType) {
        return (getAdapteeManager().supportsConvocationSearchRecordType(convocationSearchRecordType));
    }


    /**
     *  Gets the supported academy record types. 
     *
     *  @return a list containing the supported academy record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAcademyRecordTypes() {
        return (getAdapteeManager().getAcademyRecordTypes());
    }


    /**
     *  Tests if the given academy record type is supported. 
     *
     *  @param  academyRecordType a <code> Type </code> indicating an academy 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> academyRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAcademyRecordType(org.osid.type.Type academyRecordType) {
        return (getAdapteeManager().supportsAcademyRecordType(academyRecordType));
    }


    /**
     *  Gets the supported academy search record types. 
     *
     *  @return a list containing the supported academy search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getAcademySearchRecordTypes() {
        return (getAdapteeManager().getAcademySearchRecordTypes());
    }


    /**
     *  Tests if the given academy search record type is supported. 
     *
     *  @param  academySearchRecordType a <code> Type </code> indicating an 
     *          academy record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> academySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsAcademySearchRecordType(org.osid.type.Type academySearchRecordType) {
        return (getAdapteeManager().supportsAcademySearchRecordType(academySearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  lookup service. 
     *
     *  @return a <code> ConferralLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralLookupSession getConferralLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  lookup service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralLookupSession getConferralLookupSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralLookupSessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  query service. 
     *
     *  @return a <code> ConferralQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuerySession getConferralQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  query service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralQuerySession getConferralQuerySessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralQuerySessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  search service. 
     *
     *  @return a <code> ConferralSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralSearchSession getConferralSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  search service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralSearchSession getConferralSearchSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralSearchSessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  administration service. 
     *
     *  @return a <code> ConferralAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAdminSession getConferralAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAdminSession getConferralAdminSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralAdminSessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  notification service. 
     *
     *  @param  conferralReceiver the receiver 
     *  @return a <code> ConferralNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> conferralReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralNotificationSession getConferralNotificationSession(org.osid.recognition.ConferralReceiver conferralReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralNotificationSession(conferralReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the conferral 
     *  notification service for the given academy. 
     *
     *  @param  conferralReceiver the receiver 
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConferralNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> conferralReceiver 
     *          </code> or <code> academyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralNotificationSession getConferralNotificationSessionForAcademy(org.osid.recognition.ConferralReceiver conferralReceiver, 
                                                                                                       org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralNotificationSessionForAcademy(conferralReceiver, academyId));
    }


    /**
     *  Gets the session for retrieving conferral to academy mappings. 
     *
     *  @return a <code> ConferralAcademySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAcademy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAcademySession getConferralAcademySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralAcademySession());
    }


    /**
     *  Gets the session for assigning conferral to academy mappings. 
     *
     *  @return a <code> ConferralAcademyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralAcademyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralAcademyAssignmentSession getConferralAcademyAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralAcademyAssignmentSession());
    }


    /**
     *  Gets the session associated with the conferral smart academy for the 
     *  given academy. 
     *
     *  @param  academyId the <code> Id </code> of the academy 
     *  @return a <code> ConferralSmartAcademySession </code> 
     *  @throws org.osid.NotFoundException <code> conferralBookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> conferralBookId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConferralSmartAcademy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConferralSmartAcademySession getConferralSmartAcademySession(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConferralSmartAcademySession(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award lookup 
     *  service. 
     *
     *  @return an <code> AwardLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardLookupSession getAwardLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award lookup 
     *  service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardLookupSession getAwardLookupSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardLookupSessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award query 
     *  service. 
     *
     *  @return an <code> AwardQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuerySession getAwardQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award query 
     *  service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardQuerySession getAwardQuerySessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardQuerySessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award search 
     *  service. 
     *
     *  @return an <code> AwardSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSearchSession getAwardSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award search 
     *  service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSearchSession getAwardSearchSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardSearchSessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  administrative service. 
     *
     *  @return an <code> AwardAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAdminSession getAwardAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  administrative service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAdminSession getAwardAdminSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardAdminSessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  notification service. 
     *
     *  @param  awardReceiver the receiver 
     *  @return an <code> AwardNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> awardReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardNotificationSession getAwardNotificationSession(org.osid.recognition.AwardReceiver awardReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardNotificationSession(awardReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the award 
     *  notification service for the given academy. 
     *
     *  @param  awardReceiver the receiver 
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return an <code> AwardNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> awardReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardNotificationSession getAwardNotificationSessionForAcademy(org.osid.recognition.AwardReceiver awardReceiver, 
                                                                                               org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardNotificationSessionForAcademy(awardReceiver, academyId));
    }


    /**
     *  Gets the session for retrieving award to academy mappings. 
     *
     *  @return an <code> AwardAcademySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAwardAcademy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAcademySession getAwardAcademySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardAcademySession());
    }


    /**
     *  Gets the session for assigning award to academy mappings. 
     *
     *  @return an <code> AwardAcademyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardAcademyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardAcademyAssignmentSession getAwardAcademyAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardAcademyAssignmentSession());
    }


    /**
     *  Gets the session associated with the award smart academy for the given 
     *  academy. 
     *
     *  @param  academyId the <code> Id </code> of the academy 
     *  @return an <code> AwardSmartAcademySession </code> 
     *  @throws org.osid.NotFoundException <code> academyId </code> not found 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAwardSmartAcademy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AwardSmartAcademySession getAwardSmartAcademySession(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getAwardSmartAcademySession(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  lookup service. 
     *
     *  @return a <code> ConvocationLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationLookupSession getConvocationLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  lookup service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationLookupSession getConvocationLookupSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationLookupSessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  query service. 
     *
     *  @return a <code> ConvocationQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuerySession getConvocationQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  query service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationQuerySession getConvocationQuerySessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationQuerySessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  search service. 
     *
     *  @return a <code> ConvocationSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSearchSession getConvocationSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  search service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSearchSession getConvocationSearchSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationSearchSessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  administration service. 
     *
     *  @return a <code> ConvocationAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAdminSession getConvocationAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  administration service for the given academy. 
     *
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> academyId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAdminSession getConvocationAdminSessionForAcademy(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationAdminSessionForAcademy(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  notification service. 
     *
     *  @param  convocationReceiver the receiver 
     *  @return a <code> ConvocationNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> convocationReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationNotificationSession getConvocationNotificationSession(org.osid.recognition.ConvocationReceiver convocationReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationNotificationSession(convocationReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the convocation 
     *  notification service for the given academy. 
     *
     *  @param  convocationReceiver the receiver 
     *  @param  academyId the <code> Id </code> of the <code> Academy </code> 
     *  @return a <code> ConvocationNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Academy </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> convocationReceiver 
     *          </code> or <code> academyId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationNotificationSession getConvocationNotificationSessionForAcademy(org.osid.recognition.ConvocationReceiver convocationReceiver, 
                                                                                                           org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationNotificationSessionForAcademy(convocationReceiver, academyId));
    }


    /**
     *  Gets the session for retrieving convocation to academy mappings. 
     *
     *  @return a <code> ConvocationAcademySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAcademy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAcademySession getConvocationAcademySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationAcademySession());
    }


    /**
     *  Gets the session for assigning convocation to academy mappings. 
     *
     *  @return a <code> ConvocationAcademyAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationAcademyAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationAcademyAssignmentSession getConvocationAcademyAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationAcademyAssignmentSession());
    }


    /**
     *  Gets the session associated with the convocation smart academy for the 
     *  given academy. 
     *
     *  @param  academyId the <code> Id </code> of the convocation book 
     *  @return a <code> ConvocationSmartAcademySession </code> 
     *  @throws org.osid.NotFoundException <code> convocationBookId </code> 
     *          not found 
     *  @throws org.osid.NullArgumentException <code> convocationBookId 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsConvocationSmartAcademy() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.ConvocationSmartAcademySession getConvocationSmartAcademySession(org.osid.id.Id academyId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getConvocationSmartAcademySession(academyId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy lookup 
     *  service. 
     *
     *  @return an <code> AcademyLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyLookupSession getAcademyLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcademyLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy query 
     *  service. 
     *
     *  @return an <code> AcademyQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyQuerySession getAcademyQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcademyQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy search 
     *  service. 
     *
     *  @return an <code> AcademySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademySearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademySearchSession getAcademySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcademySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  administrative service. 
     *
     *  @return an <code> AcademyAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsAcademyAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyAdminSession getAcademyAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcademyAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  notification service. 
     *
     *  @param  academyReceiver the receiver 
     *  @return an <code> AcademyNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> academyReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyNotificationSession getAcademyNotificationSession(org.osid.recognition.AcademyReceiver academyReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcademyNotificationSession(academyReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  hierarchy service. 
     *
     *  @return an <code> AcademyHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyHierarchySession getAcademyHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcademyHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the academy 
     *  hierarchy design service. 
     *
     *  @return an <code> AcademyHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAcademyHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recognition.AcademyHierarchyDesignSession getAcademyHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getAcademyHierarchyDesignSession());
    }


    /**
     *  Gets a <code> RecognitionBatchManager. </code> 
     *
     *  @return a <code> RecognitionBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecognitionBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recognition.batch.RecognitionBatchManager getRecognitionBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRecognitionBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
