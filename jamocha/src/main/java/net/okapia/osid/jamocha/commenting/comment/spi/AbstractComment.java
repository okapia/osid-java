//
// AbstractComment.java
//
//     Defines a Comment.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.comment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Comment</code>.
 */

public abstract class AbstractComment
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.commenting.Comment {

    private org.osid.id.Id referenceId;
    private org.osid.resource.Resource commentor;
    private org.osid.authentication.Agent commentingAgent;
    private org.osid.locale.DisplayText text;
    private org.osid.grading.Grade rating;

    private final java.util.Collection<org.osid.commenting.records.CommentRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the referenced object to which this 
     *  comment pertains. 
     *
     *  @return the reference <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getReferenceId() {
        return (this.referenceId);
    }


    /**
     *  Sets the reference id.
     *
     *  @param referenceId a reference id
     *  @throws org.osid.NullArgumentException
     *          <code>referenceId</code> is <code>null</code>
     */

    protected void setReferenceId(org.osid.id.Id referenceId) {
        nullarg(referenceId, "reference Id");
        this.referenceId = referenceId;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the resource who created this comment. 
     *
     *  @return the <code> Resource </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCommentorId() {
        return (this.commentor.getId());
    }


    /**
     *  Gets the resource who created this comment. 
     *
     *  @return the <code> Resource </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getCommentor()
        throws org.osid.OperationFailedException {

        return (this.commentor);
    }


    /**
     *  Sets the commentor.
     *
     *  @param commentor a commentor
     *  @throws org.osid.NullArgumentException <code>commentor</code>
     *          is <code>null</code>
     */

    protected void setCommentor(org.osid.resource.Resource commentor) {
        nullarg(commentor, "commentor");
        this.commentor = commentor;
        return;
    }


    /**
     *  Gets the <code> Id </code> of the agent who created this comment. 
     *
     *  @return the <code> Agent </code> <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCommentingAgentId() {
        return (this.commentingAgent.getId());
    }


    /**
     *  Gets the agent who created this comment. 
     *
     *  @return the <code> Agent </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getCommentingAgent()
        throws org.osid.OperationFailedException {

        return (this.commentingAgent);
    }


    /**
     *  Sets the commenting agent.
     *
     *  @param agent a commenting agent
     *  @throws org.osid.NullArgumentException <code>agent</code> is
     *          <code>null</code>
     */

    protected void setCommentingAgent(org.osid.authentication.Agent agent) {
        nullarg(agent, "commenting agent");
        this.commentingAgent = agent;
        return;
    }


    /**
     *  Gets the comment text. 
     *
     *  @return the comment text 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getText() {
        return (this.text);
    }


    /**
     *  Sets the text.
     *
     *  @param text a text
     *  @throws org.osid.NullArgumentException
     *          <code>text</code> is <code>null</code>
     */

    protected void setText(org.osid.locale.DisplayText text) {
        nullarg(text, "text");
        this.text = text;
        return;
    }


    /**
     *  Tests if this comment includes a rating. 
     *
     *  @return <code> true </code> if this comment includes a rating, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasRating() {
        return (this.rating != null);
    }


    /**
     *  Gets the <code> Id </code> of the <code> Grade. </code> 
     *
     *  @return the <code> Agent </code> <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> hasRating()
     *          </code> is <code> false </code>
     */

    @OSID @Override
    public org.osid.id.Id getRatingId() {
        if (!hasRating()) {
            throw new org.osid.IllegalStateException("hasRating() is false");
        }

        return (this.rating.getId());
    }


    /**
     *  Gets the <code> Grade. </code> 
     *
     *  @return the <code> Grade </code> 
     *  @throws org.osid.IllegalStateException <code> hasRating() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.Grade getRating()
        throws org.osid.OperationFailedException {

        if (!hasRating()) {
            throw new org.osid.IllegalStateException("hasRating() is false");
        }

        return (this.rating);
    }


    /**
     *  Sets the rating.
     *
     *  @param rating a rating
     *  @throws org.osid.NullArgumentException <code>rating</code> is
     *          <code>null</code>
     */

    protected void setRating(org.osid.grading.Grade rating) {
        nullarg(rating, "rating");
        this.rating = rating;;
        return;
    }


    /**
     *  Tests if this comment supports the given record
     *  <code>Type</code>.
     *
     *  @param  commentRecordType a comment record type 
     *  @return <code>true</code> if the commentRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type commentRecordType) {
        for (org.osid.commenting.records.CommentRecord record : this.records) {
            if (record.implementsRecordType(commentRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Comment</code> record <code>Type</code>.
     *
     *  @param  commentRecordType the comment record type 
     *  @return the comment record 
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(commentRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.commenting.records.CommentRecord getCommentRecord(org.osid.type.Type commentRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.commenting.records.CommentRecord record : this.records) {
            if (record.implementsRecordType(commentRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(commentRecordType + " is not supported");
    }


    /**
     *  Adds a record to this comment. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param commentRecord the comment record
     *  @param commentRecordType comment record type
     *  @throws org.osid.NullArgumentException
     *          <code>commentRecord</code> or
     *          <code>commentRecordType</code> is <code>null</code>
     */
            
    protected void addCommentRecord(org.osid.commenting.records.CommentRecord commentRecord, 
                                    org.osid.type.Type commentRecordType) {

        nullarg(commentRecord, "comment record");
        addRecordType(commentRecordType);
        this.records.add(commentRecord);
        
        return;
    }
}
