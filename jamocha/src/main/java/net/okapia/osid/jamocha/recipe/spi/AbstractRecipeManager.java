//
// AbstractRecipeManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.recipe.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractRecipeManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.recipe.RecipeManager,
               org.osid.recipe.RecipeProxyManager {

    private final Types recipeRecordTypes                  = new TypeRefSet();
    private final Types recipeSearchRecordTypes            = new TypeRefSet();

    private final Types directionRecordTypes               = new TypeRefSet();
    private final Types directionSearchRecordTypes         = new TypeRefSet();

    private final Types ingredientRecordTypes              = new TypeRefSet();
    private final Types procedureRecordTypes               = new TypeRefSet();
    private final Types procedureSearchRecordTypes         = new TypeRefSet();

    private final Types cookbookRecordTypes                = new TypeRefSet();
    private final Types cookbookSearchRecordTypes          = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractRecipeManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractRecipeManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any cook book federation is exposed. Federation is exposed 
     *  when a specific cook book may be identified, selected and used to 
     *  create a lookup or admin session. Federation is not exposed when a set 
     *  of cook books appears as a single cookbook. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up recipes is supported. 
     *
     *  @return <code> true </code> if recipe lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeLookup() {
        return (false);
    }


    /**
     *  Tests if querying recipes is supported. 
     *
     *  @return <code> true </code> if recipe query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeQuery() {
        return (false);
    }


    /**
     *  Tests if searching recipes is supported. 
     *
     *  @return <code> true </code> if recipe search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeSearch() {
        return (false);
    }


    /**
     *  Tests if recipe administrative service is supported. 
     *
     *  @return <code> true </code> if recipe administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeAdmin() {
        return (false);
    }


    /**
     *  Tests if a recipe notification service is supported. 
     *
     *  @return <code> true </code> if recipe notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeNotification() {
        return (false);
    }


    /**
     *  Tests if a recipe cook book lookup service is supported. 
     *
     *  @return <code> true </code> if a recipe cook book lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeCookbook() {
        return (false);
    }


    /**
     *  Tests if a recipe cook book service is supported. 
     *
     *  @return <code> true </code> if recipe to cook book assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeCookbookAssignment() {
        return (false);
    }


    /**
     *  Tests if a recipe smart cook book lookup service is supported. 
     *
     *  @return <code> true </code> if a recipe smart cook book service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeSmartCookbook() {
        return (false);
    }


    /**
     *  Tests if looking up directions is supported. 
     *
     *  @return <code> true </code> if direction lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionLookup() {
        return (false);
    }


    /**
     *  Tests if querying directions is supported. 
     *
     *  @return <code> true </code> if direction query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionQuery() {
        return (false);
    }


    /**
     *  Tests if searching directions is supported. 
     *
     *  @return <code> true </code> if direction search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionSearch() {
        return (false);
    }


    /**
     *  Tests if direction <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if direction administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionAdmin() {
        return (false);
    }


    /**
     *  Tests if a direction <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if direction notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionNotification() {
        return (false);
    }


    /**
     *  Tests if a direction cook book lookup service is supported. 
     *
     *  @return <code> true </code> if a direction cook book lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionCookbook() {
        return (false);
    }


    /**
     *  Tests if a direction cook book assignment service is supported. 
     *
     *  @return <code> true </code> if a direction to cook book assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionCookbookAssignment() {
        return (false);
    }


    /**
     *  Tests if a direction smart cook book service is supported. 
     *
     *  @return <code> true </code> if a direction smart cook book service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDirectionSmartCookbook() {
        return (false);
    }


    /**
     *  Tests for the availability of a procedure lookup service. 
     *
     *  @return <code> true </code> if procedure lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureLookup() {
        return (false);
    }


    /**
     *  Tests if querying procedures is available. 
     *
     *  @return <code> true </code> if procedure query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureQuery() {
        return (false);
    }


    /**
     *  Tests if searching for procedures is available. 
     *
     *  @return <code> true </code> if procedure search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureSearch() {
        return (false);
    }


    /**
     *  Tests if searching for procedures is available. 
     *
     *  @return <code> true </code> if procedure search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureAdmin() {
        return (false);
    }


    /**
     *  Tests if procedure notification is available. 
     *
     *  @return <code> true </code> if procedure notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureNotification() {
        return (false);
    }


    /**
     *  Tests if a procedure to cook book lookup session is available. 
     *
     *  @return <code> true </code> if procedure cook book lookup session is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureCookbook() {
        return (false);
    }


    /**
     *  Tests if a procedure to cook book assignment session is available. 
     *
     *  @return <code> true </code> if procedure cook book assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureCookbookAssignment() {
        return (false);
    }


    /**
     *  Tests if a procedure smart cook book session is available. 
     *
     *  @return <code> true </code> if procedure smart cook book is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureSmartCookbook() {
        return (false);
    }


    /**
     *  Tests for the availability of a cook book lookup service. 
     *
     *  @return <code> true </code> if cook book lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookLookup() {
        return (false);
    }


    /**
     *  Tests if querying cook books is available. 
     *
     *  @return <code> true </code> if cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookQuery() {
        return (false);
    }


    /**
     *  Tests if searching for cook books is available. 
     *
     *  @return <code> true </code> if cook book search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a cook book administrative service for 
     *  creating and deleting cook books. 
     *
     *  @return <code> true </code> if cook book administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a cook book notification service. 
     *
     *  @return <code> true </code> if cook book notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a cook book hierarchy traversal service. 
     *
     *  @return <code> true </code> if cook book hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a cook book hierarchy design service. 
     *
     *  @return <code> true </code> if cook book hierarchy design is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a batch recipie service. 
     *
     *  @return <code> true </code> if a batch recipie service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipieBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> Recipe </code> record types. 
     *
     *  @return a list containing the supported <code> Recipe </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecipeRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.recipeRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Recipe </code> record type is supported. 
     *
     *  @param  recipeRecordType a <code> Type </code> indicating an <code> 
     *          Recipe </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> recipeRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecipeRecordType(org.osid.type.Type recipeRecordType) {
        return (this.recipeRecordTypes.contains(recipeRecordType));
    }


    /**
     *  Adds support for a recipe record type.
     *
     *  @param recipeRecordType a recipe record type
     *  @throws org.osid.NullArgumentException
     *  <code>recipeRecordType</code> is <code>null</code>
     */

    protected void addRecipeRecordType(org.osid.type.Type recipeRecordType) {
        this.recipeRecordTypes.add(recipeRecordType);
        return;
    }


    /**
     *  Removes support for a recipe record type.
     *
     *  @param recipeRecordType a recipe record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>recipeRecordType</code> is <code>null</code>
     */

    protected void removeRecipeRecordType(org.osid.type.Type recipeRecordType) {
        this.recipeRecordTypes.remove(recipeRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Recipe </code> search record types. 
     *
     *  @return a list containing the supported <code> Recipe </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRecipeSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.recipeSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Recipe </code> search record type is 
     *  supported. 
     *
     *  @param  recipeSearchRecordType a <code> Type </code> indicating an 
     *          <code> Recipe </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> recipeSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRecipeSearchRecordType(org.osid.type.Type recipeSearchRecordType) {
        return (this.recipeSearchRecordTypes.contains(recipeSearchRecordType));
    }


    /**
     *  Adds support for a recipe search record type.
     *
     *  @param recipeSearchRecordType a recipe search record type
     *  @throws org.osid.NullArgumentException
     *  <code>recipeSearchRecordType</code> is <code>null</code>
     */

    protected void addRecipeSearchRecordType(org.osid.type.Type recipeSearchRecordType) {
        this.recipeSearchRecordTypes.add(recipeSearchRecordType);
        return;
    }


    /**
     *  Removes support for a recipe search record type.
     *
     *  @param recipeSearchRecordType a recipe search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>recipeSearchRecordType</code> is <code>null</code>
     */

    protected void removeRecipeSearchRecordType(org.osid.type.Type recipeSearchRecordType) {
        this.recipeSearchRecordTypes.remove(recipeSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Direction </code> record types. 
     *
     *  @return a list containing the supported <code> Direction </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectionRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.directionRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Direction </code> record type is supported. 
     *
     *  @param  directionRecordType a <code> Type </code> indicating a <code> 
     *          Direction </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> directionRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDirectionRecordType(org.osid.type.Type directionRecordType) {
        return (this.directionRecordTypes.contains(directionRecordType));
    }


    /**
     *  Adds support for a direction record type.
     *
     *  @param directionRecordType a direction record type
     *  @throws org.osid.NullArgumentException
     *  <code>directionRecordType</code> is <code>null</code>
     */

    protected void addDirectionRecordType(org.osid.type.Type directionRecordType) {
        this.directionRecordTypes.add(directionRecordType);
        return;
    }


    /**
     *  Removes support for a direction record type.
     *
     *  @param directionRecordType a direction record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>directionRecordType</code> is <code>null</code>
     */

    protected void removeDirectionRecordType(org.osid.type.Type directionRecordType) {
        this.directionRecordTypes.remove(directionRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Direction </code> search types. 
     *
     *  @return a list containing the supported <code> Direction </code> 
     *          search types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDirectionSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.directionSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Direction </code> search type is supported. 
     *
     *  @param  directionSearchRecordType a <code> Type </code> indicating a 
     *          <code> Direction </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          directionSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDirectionSearchRecordType(org.osid.type.Type directionSearchRecordType) {
        return (this.directionSearchRecordTypes.contains(directionSearchRecordType));
    }


    /**
     *  Adds support for a direction search record type.
     *
     *  @param directionSearchRecordType a direction search record type
     *  @throws org.osid.NullArgumentException
     *  <code>directionSearchRecordType</code> is <code>null</code>
     */

    protected void addDirectionSearchRecordType(org.osid.type.Type directionSearchRecordType) {
        this.directionSearchRecordTypes.add(directionSearchRecordType);
        return;
    }


    /**
     *  Removes support for a direction search record type.
     *
     *  @param directionSearchRecordType a direction search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>directionSearchRecordType</code> is <code>null</code>
     */

    protected void removeDirectionSearchRecordType(org.osid.type.Type directionSearchRecordType) {
        this.directionSearchRecordTypes.remove(directionSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Ingredient </code> record types. 
     *
     *  @return a list containing the supported <code> Ingredient </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getIngredientRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.ingredientRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Ingredient </code> record type is supported. 
     *
     *  @param  ingredientRecordType a <code> Type </code> indicating an 
     *          <code> Ingredient </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ingredientRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsIngredientRecordType(org.osid.type.Type ingredientRecordType) {
        return (this.ingredientRecordTypes.contains(ingredientRecordType));
    }


    /**
     *  Adds support for an ingredient record type.
     *
     *  @param ingredientRecordType an ingredient record type
     *  @throws org.osid.NullArgumentException
     *  <code>ingredientRecordType</code> is <code>null</code>
     */

    protected void addIngredientRecordType(org.osid.type.Type ingredientRecordType) {
        this.ingredientRecordTypes.add(ingredientRecordType);
        return;
    }


    /**
     *  Removes support for an ingredient record type.
     *
     *  @param ingredientRecordType an ingredient record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>ingredientRecordType</code> is <code>null</code>
     */

    protected void removeIngredientRecordType(org.osid.type.Type ingredientRecordType) {
        this.ingredientRecordTypes.remove(ingredientRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Procedure </code> record types. 
     *
     *  @return a list containing the supported procedure record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcedureRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.procedureRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Procedure </code> record type is supported. 
     *
     *  @param  procedureRecordType a <code> Type </code> indicating a <code> 
     *          Procedure </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> procedureRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcedureRecordType(org.osid.type.Type procedureRecordType) {
        return (this.procedureRecordTypes.contains(procedureRecordType));
    }


    /**
     *  Adds support for a procedure record type.
     *
     *  @param procedureRecordType a procedure record type
     *  @throws org.osid.NullArgumentException
     *  <code>procedureRecordType</code> is <code>null</code>
     */

    protected void addProcedureRecordType(org.osid.type.Type procedureRecordType) {
        this.procedureRecordTypes.add(procedureRecordType);
        return;
    }


    /**
     *  Removes support for a procedure record type.
     *
     *  @param procedureRecordType a procedure record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>procedureRecordType</code> is <code>null</code>
     */

    protected void removeProcedureRecordType(org.osid.type.Type procedureRecordType) {
        this.procedureRecordTypes.remove(procedureRecordType);
        return;
    }


    /**
     *  Gets the supported procedure search record types. 
     *
     *  @return a list containing the supported procedure search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProcedureSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.procedureSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given procedure search record type is supported. 
     *
     *  @param  procedureSearchRecordType a <code> Type </code> indicating a 
     *          procedure record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          procedureSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProcedureSearchRecordType(org.osid.type.Type procedureSearchRecordType) {
        return (this.procedureSearchRecordTypes.contains(procedureSearchRecordType));
    }


    /**
     *  Adds support for a procedure search record type.
     *
     *  @param procedureSearchRecordType a procedure search record type
     *  @throws org.osid.NullArgumentException
     *  <code>procedureSearchRecordType</code> is <code>null</code>
     */

    protected void addProcedureSearchRecordType(org.osid.type.Type procedureSearchRecordType) {
        this.procedureSearchRecordTypes.add(procedureSearchRecordType);
        return;
    }


    /**
     *  Removes support for a procedure search record type.
     *
     *  @param procedureSearchRecordType a procedure search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>procedureSearchRecordType</code> is <code>null</code>
     */

    protected void removeProcedureSearchRecordType(org.osid.type.Type procedureSearchRecordType) {
        this.procedureSearchRecordTypes.remove(procedureSearchRecordType);
        return;
    }


    /**
     *  Gets the supported cook book record types. 
     *
     *  @return a list containing the supported cook book record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCookbookRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.cookbookRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given cook book record type is supported. 
     *
     *  @param  cookbookRecordType a <code> Type </code> indicating a cook 
     *          book record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> cookbookRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCookbookRecordType(org.osid.type.Type cookbookRecordType) {
        return (this.cookbookRecordTypes.contains(cookbookRecordType));
    }


    /**
     *  Adds support for a cookbook record type.
     *
     *  @param cookbookRecordType a cookbook record type
     *  @throws org.osid.NullArgumentException
     *  <code>cookbookRecordType</code> is <code>null</code>
     */

    protected void addCookbookRecordType(org.osid.type.Type cookbookRecordType) {
        this.cookbookRecordTypes.add(cookbookRecordType);
        return;
    }


    /**
     *  Removes support for a cookbook record type.
     *
     *  @param cookbookRecordType a cookbook record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>cookbookRecordType</code> is <code>null</code>
     */

    protected void removeCookbookRecordType(org.osid.type.Type cookbookRecordType) {
        this.cookbookRecordTypes.remove(cookbookRecordType);
        return;
    }


    /**
     *  Gets the supported cook book search record types. 
     *
     *  @return a list containing the supported cook book search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCookbookSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.cookbookSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given cook book search record type is supported. 
     *
     *  @param  cookbookSearchRecordType a <code> Type </code> indicating a 
     *          cook book record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> cookbookSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCookbookSearchRecordType(org.osid.type.Type cookbookSearchRecordType) {
        return (this.cookbookSearchRecordTypes.contains(cookbookSearchRecordType));
    }


    /**
     *  Adds support for a cookbook search record type.
     *
     *  @param cookbookSearchRecordType a cookbook search record type
     *  @throws org.osid.NullArgumentException
     *  <code>cookbookSearchRecordType</code> is <code>null</code>
     */

    protected void addCookbookSearchRecordType(org.osid.type.Type cookbookSearchRecordType) {
        this.cookbookSearchRecordTypes.add(cookbookSearchRecordType);
        return;
    }


    /**
     *  Removes support for a cookbook search record type.
     *
     *  @param cookbookSearchRecordType a cookbook search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>cookbookSearchRecordType</code> is <code>null</code>
     */

    protected void removeCookbookSearchRecordType(org.osid.type.Type cookbookSearchRecordType) {
        this.cookbookSearchRecordTypes.remove(cookbookSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe lookup 
     *  service. 
     *
     *  @return a <code> RecipeLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeLookupSession getRecipeLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecipeLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeLookupSession getRecipeLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe lookup 
     *  service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return q <code> RecipeLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeLookupSession getRecipeLookupSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeLookupSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe lookup 
     *  service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecipeLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeLookupSession getRecipeLookupSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeLookupSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe query 
     *  service. 
     *
     *  @return a <code> RecipeQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuerySession getRecipeQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecipeQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuerySession getRecipeQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe query 
     *  service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeQuerySession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuerySession getRecipeQuerySessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeQuerySessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe query 
     *  service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecipeQuerySession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuerySession getRecipeQuerySessionForCookbook(org.osid.id.Id cookbookId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeQuerySessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe search 
     *  service. 
     *
     *  @return a <code> RecipeSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSearchSession getRecipeSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecipeSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSearchSession getRecipeSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe search 
     *  service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeSearchSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSearchSession getRecipeSearchSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeSearchSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe search 
     *  service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecipeSearchSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSearchSession getRecipeSearchSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeSearchSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  administration service. 
     *
     *  @return a <code> RecipeAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeAdminSession getRecipeAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecipeAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeAdminSession getRecipeAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeAdminSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeAdminSession getRecipeAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecipeAdminSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeAdminSession getRecipeAdminSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  notification service. 
     *
     *  @param  recipeReceiver the notification callback 
     *  @return a <code> RecipeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> recipeReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeNotificationSession getRecipeNotificationSession(org.osid.recipe.RecipeReceiver recipeReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  notification service. 
     *
     *  @param  recipeReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> RecipeNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> recipeReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeNotificationSession getRecipeNotificationSession(org.osid.recipe.RecipeReceiver recipeReceiver, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  notification service for the given cookbook. 
     *
     *  @param  recipeReceiver the notification callback 
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeNotificationSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> recipeReceiver </code> 
     *          or <code> cookbookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeNotificationSession getRecipeNotificationSessionForCookbook(org.osid.recipe.RecipeReceiver recipeReceiver, 
                                                                                             org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeNotificationSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the recipe 
     *  notification service for the given cookbook. 
     *
     *  @param  recipeReceiver the notification callback 
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecipeNotificationSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> recipeReceiver, 
     *          cookbookId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeNotificationSession getRecipeNotificationSessionForCookbook(org.osid.recipe.RecipeReceiver recipeReceiver, 
                                                                                             org.osid.id.Id cookbookId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeNotificationSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup recipe/cook book 
     *  mappings. 
     *
     *  @return a <code> RecipeCookbookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeCookbookSession getRecipeCookbookSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeCookbookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup recipe/cook book 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecipeCookbookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeCookbookSession getRecipeCookbookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeCookbookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning recipes 
     *  to cook books. 
     *
     *  @return a <code> RecipeCookbookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeCookbookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeCookbookAssignmentSession getRecipeCookbookAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeCookbookAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning recipes 
     *  to cook books. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> RecipeCookbookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeCookbookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeCookbookAssignmentSession getRecipeCookbookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeCookbookAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage recipe smart cook books. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> RecipeSmartCookbookSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeSmartCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSmartCookbookSession getRecipeSmartCookbookSession(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeSmartCookbookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage recipe smart cook books. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> RecipeSmartCookbookSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRecipeSmartCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSmartCookbookSession getRecipeSmartCookbookSession(org.osid.id.Id cookbookId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeSmartCookbookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  lookup service. 
     *
     *  @return a <code> DirectionLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionLookupSession getDirectionLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectionLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionLookupSession getDirectionLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  lookup service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the cook book 
     *  @return a <code> DirectionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionLookupSession getDirectionLookupSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionLookupSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  lookup service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the cook book 
     *  @param  proxy a proxy 
     *  @return a <code> DirectionLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionLookupSession getDirectionLookupSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionLookupSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  query service. 
     *
     *  @return a <code> DirectionQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuerySession getDirectionQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectionQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuerySession getDirectionQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  query service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionQuerySession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuerySession getDirectionQuerySessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionQuerySessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  query service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DirectionQuerySession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionQuerySession getDirectionQuerySessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionQuerySessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  search service. 
     *
     *  @return a <code> DirectionSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionSearchSession getDirectionSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectionSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionSearchSession getDirectionSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  search service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionSearchSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionSearchSession getDirectionSearchSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionSearchSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  search service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DirectionSearchSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionSearchSession getDirectionSearchSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionSearchSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  administration service. 
     *
     *  @return a <code> DirectionAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionAdminSession getDirectionAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectionAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionAdminSession getDirectionAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionAdminSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionAdminSession getDirectionAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DirectionAdminSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionAdminSession getDirectionAdminSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  notification service. 
     *
     *  @param  directionReceiver the notification callback 
     *  @return a <code> DirectionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> directionReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionNotificationSession getDirectionNotificationSession(org.osid.recipe.DirectionReceiver directionReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  notification service. 
     *
     *  @param  directionReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DirectionNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> directionReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionNotificationSession getDirectionNotificationSession(org.osid.recipe.DirectionReceiver directionReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  notification service for the given cookbook. 
     *
     *  @param  directionReceiver the notification callback 
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> directionReceiver 
     *          </code> or <code> cookbookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionNotificationSession getDirectionNotificationSessionForCookbook(org.osid.recipe.DirectionReceiver directionReceiver, 
                                                                                                   org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionNotificationSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the direction 
     *  notification service for the given cookbook. 
     *
     *  @param  directionReceiver the notification callback 
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DirectionNotificationSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> directionReceiver, 
     *          </code> <code> cookbookId </code> or <code> proxy </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionNotificationSession getDirectionNotificationSessionForCookbook(org.osid.recipe.DirectionReceiver directionReceiver, 
                                                                                                   org.osid.id.Id cookbookId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionNotificationSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup direction/cook book 
     *  ingredients. 
     *
     *  @return a <code> DirectionCookbookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionCookbookSession getDirectionCookbookSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionCookbookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup direction/cook book 
     *  ingredients. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectionCookbookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionCookbookSession getDirectionCookbookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionCookbookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  directions to cook books. 
     *
     *  @return a <code> DirectionCookbookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionCookbookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionCookbookAssignmentSession getDirectionCookbookAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionCookbookAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning mappings 
     *  to cook books. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DirectionCookbookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionCookbookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionCookbookAssignmentSession getDirectionCookbookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionCookbookAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart cook 
     *  books. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> DirectionSmartCookbookSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionSmartCookbook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionSmartCookbookSession getDirectionSmartCookbookSession(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getDirectionSmartCookbookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart cook 
     *  books. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DirectionSmartCookbookSession </code> 
     *  @throws org.osid.NotFoundException no cook book found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDirectionSmartCookbook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.DirectionSmartCookbookSession getDirectionSmartCookbookSession(org.osid.id.Id cookbookId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getDirectionSmartCookbookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  lookup service. 
     *
     *  @return a <code> ProcedureLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureLookupSession getProcedureLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureLookupSession getProcedureLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  lookup service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureLookupSession getProcedureLookupSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureLookupSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  lookup service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureLookupSession getProcedureLookupSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureLookupSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  query service. 
     *
     *  @return a <code> ProcedureQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuerySession getProcedureQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuerySession getProcedureQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  query service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuerySession getProcedureQuerySessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureQuerySessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  query service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuerySession getProcedureQuerySessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureQuerySessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  search service. 
     *
     *  @return a <code> ProcedureSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureSearchSession getProcedureSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureSearchSession getProcedureSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  search service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureSearchSession getProcedureSearchSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureSearchSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  search service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureSearchSession getProcedureSearchSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                       org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureSearchSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  administration service. 
     *
     *  @return a <code> ProcedureAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureAdminSession getProcedureAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureAdminSession getProcedureAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureAdminSession getProcedureAdminSessionForCookbook(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  administration service for the given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureAdminSession getProcedureAdminSessionForCookbook(org.osid.id.Id cookbookId, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureAdminSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  notification service. 
     *
     *  @param  procedureReceiver the receiver 
     *  @return a <code> ProcedureNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> procedureReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureNotificationSession getProcedureNotificationSession(org.osid.recipe.ProcedureReceiver procedureReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  notification service. 
     *
     *  @param  procedureReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> procedureReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureNotificationSession getProcedureNotificationSession(org.osid.recipe.ProcedureReceiver procedureReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  notification service for the given cookbook. 
     *
     *  @param  procedureReceiver the receiver 
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @return a <code> ProcedureNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> procedureReceiver 
     *          </code> or <code> cookbookId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureNotificationSession getProcedureNotificationSessionForCookbook(org.osid.recipe.ProcedureReceiver procedureReceiver, 
                                                                                                   org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureNotificationSessionForCookbook not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the procedure 
     *  notification service for the given cookbook. 
     *
     *  @param  procedureReceiver the receiver 
     *  @param  cookbookId the <code> Id </code> of the <code> Cookbook 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Cookbook </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> procedureReceiver, 
     *          cookbookId, </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureNotificationSession getProcedureNotificationSessionForCookbook(org.osid.recipe.ProcedureReceiver procedureReceiver, 
                                                                                                   org.osid.id.Id cookbookId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureNotificationSessionForCookbook not implemented");
    }


    /**
     *  Gets the session for retrieving procedure to cook book mappings. 
     *
     *  @return a <code> ProcedureCookbookSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureCookbookSession getProcedureCookbookSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureCookbookSession not implemented");
    }


    /**
     *  Gets the session for retrieving procedure to cook book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureCookbookSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureCookbook() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureCookbookSession getProcedureCookbookSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureCookbookSession not implemented");
    }


    /**
     *  Gets the session for assigning procedure to cook book mappings. 
     *
     *  @return a <code> ProcedureCookbookAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureCookbookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureCookbookAssignmentSession getProcedureCookbookAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureCookbookAssignmentSession not implemented");
    }


    /**
     *  Gets the session for assigning procedure to cook book mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureCookbookAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureCookbookAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureCookbookAssignmentSession getProcedureCookbookAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureCookbookAssignmentSession not implemented");
    }


    /**
     *  Gets the session associated with the procedure smart cook book for the 
     *  given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the cookbook 
     *  @return a <code> ProcedureSmartCookbookSession </code> 
     *  @throws org.osid.NotFoundException <code> procedureBookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> procedureBookId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureSmartCookbook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureSmartCookbookSession getProcedureSmartCookbookSession(org.osid.id.Id cookbookId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getProcedureSmartCookbookSession not implemented");
    }


    /**
     *  Gets the session associated with the procedure smart cook book for the 
     *  given cookbook. 
     *
     *  @param  cookbookId the <code> Id </code> of the procedure book 
     *  @param  proxy a proxy 
     *  @return a <code> ProcedureSmartCookbookSession </code> 
     *  @throws org.osid.NotFoundException <code> procedureBookId </code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code> procedureBookId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureSmartCookbook() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureSmartCookbookSession getProcedureSmartCookbookSession(org.osid.id.Id cookbookId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getProcedureSmartCookbookSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  lookup service. 
     *
     *  @return a <code> CookbookLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookLookupSession getCookbookLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getCookbookLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CookbookLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookLookupSession getCookbookLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getCookbookLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  query service. 
     *
     *  @return a <code> CookbookQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuerySession getCookbookQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getCookbookQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CookbookQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuerySession getCookbookQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getCookbookQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  search service. 
     *
     *  @return a <code> CookbookSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookSearchSession getCookbookSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getCookbookSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CookbookSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookSearchSession getCookbookSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getCookbookSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  administrative service. 
     *
     *  @return a <code> CookbookAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookAdminSession getCookbookAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getCookbookAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CookbookAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookAdminSession getCookbookAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getCookbookAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  notification service. 
     *
     *  @param  cookbookReceiver the receiver 
     *  @return a <code> CookbookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookNotificationSession getCookbookNotificationSession(org.osid.recipe.CookbookReceiver cookbookReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getCookbookNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  notification service. 
     *
     *  @param  cookbookReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> CookbookNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> cookbookReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookNotificationSession getCookbookNotificationSession(org.osid.recipe.CookbookReceiver cookbookReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getCookbookNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  hierarchy service. 
     *
     *  @return a <code> CookbookHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookHierarchySession getCookbookHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getCookbookHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CookbookHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookHierarchySession getCookbookHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getCookbookHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  hierarchy design service. 
     *
     *  @return a <code> CookbookHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookHierarchyDesignSession getCookbookHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getCookbookHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the cook book 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CookbookHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCookbookHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookHierarchyDesignSession getCookbookHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getCookbookHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> RecipieBatchManager </code> . 
     *
     *  @return a <code> RecipeBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.RecipeBatchManager getRecipeBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeManager.getRecipeBatchManager not implemented");
    }


    /**
     *  Gets the <code> RecipieBatchProxyManager </code> . 
     *
     *  @return a <code> RecipeBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.batch.RecipeBatchProxyManager getRecipeBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.recipe.RecipeProxyManager.getRecipeBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.recipeRecordTypes.clear();
        this.recipeRecordTypes.clear();

        this.recipeSearchRecordTypes.clear();
        this.recipeSearchRecordTypes.clear();

        this.directionRecordTypes.clear();
        this.directionRecordTypes.clear();

        this.directionSearchRecordTypes.clear();
        this.directionSearchRecordTypes.clear();

        this.ingredientRecordTypes.clear();
        this.ingredientRecordTypes.clear();

        this.procedureRecordTypes.clear();
        this.procedureRecordTypes.clear();

        this.procedureSearchRecordTypes.clear();
        this.procedureSearchRecordTypes.clear();

        this.cookbookRecordTypes.clear();
        this.cookbookRecordTypes.clear();

        this.cookbookSearchRecordTypes.clear();
        this.cookbookSearchRecordTypes.clear();

        return;
    }
}
