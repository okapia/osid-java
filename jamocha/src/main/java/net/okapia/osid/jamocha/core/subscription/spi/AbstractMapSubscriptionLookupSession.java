//
// AbstractMapSubscriptionLookupSession
//
//    A simple framework for providing a Subscription lookup service
//    backed by a fixed collection of subscriptions.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Subscription lookup service backed by a
 *  fixed collection of subscriptions. The subscriptions are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Subscriptions</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapSubscriptionLookupSession
    extends net.okapia.osid.jamocha.subscription.spi.AbstractSubscriptionLookupSession
    implements org.osid.subscription.SubscriptionLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.subscription.Subscription> subscriptions = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.subscription.Subscription>());


    /**
     *  Makes a <code>Subscription</code> available in this session.
     *
     *  @param  subscription a subscription
     *  @throws org.osid.NullArgumentException <code>subscription<code>
     *          is <code>null</code>
     */

    protected void putSubscription(org.osid.subscription.Subscription subscription) {
        this.subscriptions.put(subscription.getId(), subscription);
        return;
    }


    /**
     *  Makes an array of subscriptions available in this session.
     *
     *  @param  subscriptions an array of subscriptions
     *  @throws org.osid.NullArgumentException <code>subscriptions<code>
     *          is <code>null</code>
     */

    protected void putSubscriptions(org.osid.subscription.Subscription[] subscriptions) {
        putSubscriptions(java.util.Arrays.asList(subscriptions));
        return;
    }


    /**
     *  Makes a collection of subscriptions available in this session.
     *
     *  @param  subscriptions a collection of subscriptions
     *  @throws org.osid.NullArgumentException <code>subscriptions<code>
     *          is <code>null</code>
     */

    protected void putSubscriptions(java.util.Collection<? extends org.osid.subscription.Subscription> subscriptions) {
        for (org.osid.subscription.Subscription subscription : subscriptions) {
            this.subscriptions.put(subscription.getId(), subscription);
        }

        return;
    }


    /**
     *  Removes a Subscription from this session.
     *
     *  @param  subscriptionId the <code>Id</code> of the subscription
     *  @throws org.osid.NullArgumentException <code>subscriptionId<code> is
     *          <code>null</code>
     */

    protected void removeSubscription(org.osid.id.Id subscriptionId) {
        this.subscriptions.remove(subscriptionId);
        return;
    }


    /**
     *  Gets the <code>Subscription</code> specified by its <code>Id</code>.
     *
     *  @param  subscriptionId <code>Id</code> of the <code>Subscription</code>
     *  @return the subscription
     *  @throws org.osid.NotFoundException <code>subscriptionId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>subscriptionId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Subscription getSubscription(org.osid.id.Id subscriptionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.subscription.Subscription subscription = this.subscriptions.get(subscriptionId);
        if (subscription == null) {
            throw new org.osid.NotFoundException("subscription not found: " + subscriptionId);
        }

        return (subscription);
    }


    /**
     *  Gets all <code>Subscriptions</code>. In plenary mode, the returned
     *  list contains all known subscriptions or an error
     *  results. Otherwise, the returned list may contain only those
     *  subscriptions that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Subscriptions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.SubscriptionList getSubscriptions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.subscription.subscription.ArraySubscriptionList(this.subscriptions.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.subscriptions.clear();
        super.close();
        return;
    }
}
