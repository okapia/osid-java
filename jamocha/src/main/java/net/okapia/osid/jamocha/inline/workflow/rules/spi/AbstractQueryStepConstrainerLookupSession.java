//
// AbstractQueryStepConstrainerLookupSession.java
//
//    An inline adapter that maps a StepConstrainerLookupSession to
//    a StepConstrainerQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a StepConstrainerLookupSession to
 *  a StepConstrainerQuerySession.
 */

public abstract class AbstractQueryStepConstrainerLookupSession
    extends net.okapia.osid.jamocha.workflow.rules.spi.AbstractStepConstrainerLookupSession
    implements org.osid.workflow.rules.StepConstrainerLookupSession {

    private boolean activeonly    = false;
    private final org.osid.workflow.rules.StepConstrainerQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryStepConstrainerLookupSession.
     *
     *  @param querySession the underlying step constrainer query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryStepConstrainerLookupSession(org.osid.workflow.rules.StepConstrainerQuerySession querySession) {
        nullarg(querySession, "step constrainer query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Office</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform <code>StepConstrainer</code>
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStepConstrainers() {
        return (this.session.canSearchStepConstrainers());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step constrainers in offices which are
     *  children of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    

    /**
     *  Only active step constrainers are returned by methods in this
     *  session.
     */
     
    @OSID @Override
    public void useActiveStepConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive step constrainers are returned by methods
     *  in this session.
     */
    
    @OSID @Override
    public void useAnyStatusStepConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>StepConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>StepConstrainer</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>StepConstrainer</code> and retained for compatibility.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @param stepConstrainerId <code>Id</code> of the
     *          <code>StepConstrainer</code>
     *  @return the step constrainer
     *  @throws org.osid.NotFoundException
     *          <code>stepConstrainerId</code> not found
     *  @throws org.osid.NullArgumentException <code>stepConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainer getStepConstrainer(org.osid.id.Id stepConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepConstrainerQuery query = getQuery();
        query.matchId(stepConstrainerId, true);
        org.osid.workflow.rules.StepConstrainerList stepConstrainers = this.session.getStepConstrainersByQuery(query);
        if (stepConstrainers.hasNext()) {
            return (stepConstrainers.getNextStepConstrainer());
        } 
        
        throw new org.osid.NotFoundException(stepConstrainerId + " not found");
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  stepConstrainers specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>StepConstrainers</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @param stepConstrainerIds the list of <code>Ids</code> to
     *         retrieve
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByIds(org.osid.id.IdList stepConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepConstrainerQuery query = getQuery();

        try (org.osid.id.IdList ids = stepConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getStepConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the
     *  given step constrainer genus <code>Type</code> which does not
     *  include step constrainers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known step
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @param  stepConstrainerGenusType a stepConstrainer genus type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerGenusType</code> is
     *          <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByGenusType(org.osid.type.Type stepConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepConstrainerQuery query = getQuery();
        query.matchGenusType(stepConstrainerGenusType, true);
        return (this.session.getStepConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the
     *  given step constrainer genus <code>Type</code> and include any
     *  additional step constrainers with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known step
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @param  stepConstrainerGenusType a stepConstrainer genus type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByParentGenusType(org.osid.type.Type stepConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepConstrainerQuery query = getQuery();
        query.matchParentGenusType(stepConstrainerGenusType, true);
        return (this.session.getStepConstrainersByQuery(query));
    }


    /**
     *  Gets a <code>StepConstrainerList</code> containing the given
     *  step constrainer record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known step
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @param  stepConstrainerRecordType a stepConstrainer record type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByRecordType(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.workflow.rules.StepConstrainerQuery query = getQuery();
        query.matchRecordType(stepConstrainerRecordType, true);
        return (this.session.getStepConstrainersByQuery(query));
    }

    
    /**
     *  Gets all <code>StepConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known step
     *  constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step constrainers are returned that are
     *  currently active. In any status mode, active and inactive step
     *  constrainers are returned.
     *
     *  @return a list of <code>StepConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.workflow.rules.StepConstrainerQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getStepConstrainersByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.workflow.rules.StepConstrainerQuery getQuery() {
        org.osid.workflow.rules.StepConstrainerQuery query = this.session.getStepConstrainerQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
