//
// AbstractControlProxyManager.java
//
//     An adapter for a ControlProxyManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a ControlProxyManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterControlProxyManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidProxyManager<org.osid.control.ControlProxyManager>
    implements org.osid.control.ControlProxyManager {


    /**
     *  Constructs a new {@code AbstractAdapterControlProxyManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterControlProxyManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterControlProxyManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterControlProxyManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any trigger federation is exposed. Federation is exposed when 
     *  a specific trigger may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  triggers appears as a single trigger. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if looking up devices is supported. 
     *
     *  @return <code> true </code> if device lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceLookup() {
        return (getAdapteeManager().supportsDeviceLookup());
    }


    /**
     *  Tests if querying devices is supported. 
     *
     *  @return <code> true </code> if device query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceQuery() {
        return (getAdapteeManager().supportsDeviceQuery());
    }


    /**
     *  Tests if searching devices is supported. 
     *
     *  @return <code> true </code> if device search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSearch() {
        return (getAdapteeManager().supportsDeviceSearch());
    }


    /**
     *  Tests if a device administrative service is supported. 
     *
     *  @return <code> true </code> if device administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceAdmin() {
        return (getAdapteeManager().supportsDeviceAdmin());
    }


    /**
     *  Tests if a device <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if device notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceNotification() {
        return (getAdapteeManager().supportsDeviceNotification());
    }


    /**
     *  Tests if a device system lookup service is supported. 
     *
     *  @return <code> true </code> if a device system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSystem() {
        return (getAdapteeManager().supportsDeviceSystem());
    }


    /**
     *  Tests if a device system assignment service is supported. 
     *
     *  @return <code> true </code> if a device to system assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSystemAssignment() {
        return (getAdapteeManager().supportsDeviceSystemAssignment());
    }


    /**
     *  Tests if a device smart system service is supported. 
     *
     *  @return <code> true </code> if an v smart system service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceSmartSystem() {
        return (getAdapteeManager().supportsDeviceSmartSystem());
    }


    /**
     *  Tests if returning devices is supported. 
     *
     *  @return <code> true </code> if returning devices is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDeviceReturn() {
        return (getAdapteeManager().supportsDeviceReturn());
    }


    /**
     *  Tests if looking up controllers is supported. 
     *
     *  @return <code> true </code> if controller lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerLookup() {
        return (getAdapteeManager().supportsControllerLookup());
    }


    /**
     *  Tests if querying controllers is supported. 
     *
     *  @return <code> true </code> if controller query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerQuery() {
        return (getAdapteeManager().supportsControllerQuery());
    }


    /**
     *  Tests if searching controllers is supported. 
     *
     *  @return <code> true </code> if controller search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSearch() {
        return (getAdapteeManager().supportsControllerSearch());
    }


    /**
     *  Tests if controller administrative service is supported. 
     *
     *  @return <code> true </code> if controller administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerAdmin() {
        return (getAdapteeManager().supportsControllerAdmin());
    }


    /**
     *  Tests if a controller notification service is supported. 
     *
     *  @return <code> true </code> if controller notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerNotification() {
        return (getAdapteeManager().supportsControllerNotification());
    }


    /**
     *  Tests if a controller trigger lookup service is supported. 
     *
     *  @return <code> true </code> if a controller trigger lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerTrigger() {
        return (getAdapteeManager().supportsControllerTrigger());
    }


    /**
     *  Tests if a controller trigger service is supported. 
     *
     *  @return <code> true </code> if controller to trigger assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerTriggerAssignment() {
        return (getAdapteeManager().supportsControllerTriggerAssignment());
    }


    /**
     *  Tests if a controller smart trigger lookup service is supported. 
     *
     *  @return <code> true </code> if a controller smart trigger service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControllerSmartTrigger() {
        return (getAdapteeManager().supportsControllerSmartTrigger());
    }


    /**
     *  Tests if looking up inputs is supported. 
     *
     *  @return <code> true </code> if input lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputLookup() {
        return (getAdapteeManager().supportsInputLookup());
    }


    /**
     *  Tests if querying inputs is supported. 
     *
     *  @return <code> true </code> if input query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputQuery() {
        return (getAdapteeManager().supportsInputQuery());
    }


    /**
     *  Tests if searching inputs is supported. 
     *
     *  @return <code> true </code> if input search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputSearch() {
        return (getAdapteeManager().supportsInputSearch());
    }


    /**
     *  Tests if an input administrative service is supported. 
     *
     *  @return <code> true </code> if input administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputAdmin() {
        return (getAdapteeManager().supportsInputAdmin());
    }


    /**
     *  Tests if an input <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if input notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputNotification() {
        return (getAdapteeManager().supportsInputNotification());
    }


    /**
     *  Tests if an input system lookup service is supported. 
     *
     *  @return <code> true </code> if an input system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputSystem() {
        return (getAdapteeManager().supportsInputSystem());
    }


    /**
     *  Tests if an input system assignment service is supported. 
     *
     *  @return <code> true </code> if an input to system assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputSystemAssignment() {
        return (getAdapteeManager().supportsInputSystemAssignment());
    }


    /**
     *  Tests if an input smart system service is supported. 
     *
     *  @return <code> true </code> if a smart system service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsInputSmartSystem() {
        return (getAdapteeManager().supportsInputSmartSystem());
    }


    /**
     *  Tests if looking up settings is supported. 
     *
     *  @return <code> true </code> if setting lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingLookup() {
        return (getAdapteeManager().supportsSettingLookup());
    }


    /**
     *  Tests if querying settings is supported. 
     *
     *  @return <code> true </code> if setting query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingQuery() {
        return (getAdapteeManager().supportsSettingQuery());
    }


    /**
     *  Tests if searching settings is supported. 
     *
     *  @return <code> true </code> if setting search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingSearch() {
        return (getAdapteeManager().supportsSettingSearch());
    }


    /**
     *  Tests if setting <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if setting administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingAdmin() {
        return (getAdapteeManager().supportsSettingAdmin());
    }


    /**
     *  Tests if a setting <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if setting notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingNotification() {
        return (getAdapteeManager().supportsSettingNotification());
    }


    /**
     *  Tests if a setting system lookup service is supported. 
     *
     *  @return <code> true </code> if a setting system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingSystem() {
        return (getAdapteeManager().supportsSettingSystem());
    }


    /**
     *  Tests if a setting system assignment service is supported. 
     *
     *  @return <code> true </code> if a setting to system assignment service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingSystemAssignment() {
        return (getAdapteeManager().supportsSettingSystemAssignment());
    }


    /**
     *  Tests if a setting smart system service is supported. 
     *
     *  @return <code> true </code> if a setting smart system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingSmartSystem() {
        return (getAdapteeManager().supportsSettingSmartSystem());
    }


    /**
     *  Tests if looking up scenes is supported. 
     *
     *  @return <code> true </code> if scene lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneLookup() {
        return (getAdapteeManager().supportsSceneLookup());
    }


    /**
     *  Tests if querying scenes is supported. 
     *
     *  @return <code> true </code> if scene query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneQuery() {
        return (getAdapteeManager().supportsSceneQuery());
    }


    /**
     *  Tests if searching scenes is supported. 
     *
     *  @return <code> true </code> if scene search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneSearch() {
        return (getAdapteeManager().supportsSceneSearch());
    }


    /**
     *  Tests if scene <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if scene administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneAdmin() {
        return (getAdapteeManager().supportsSceneAdmin());
    }


    /**
     *  Tests if a scene <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if scene notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneNotification() {
        return (getAdapteeManager().supportsSceneNotification());
    }


    /**
     *  Tests if a scene system lookup service is supported. 
     *
     *  @return <code> true </code> if a scene system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneSystem() {
        return (getAdapteeManager().supportsSceneSystem());
    }


    /**
     *  Tests if a scene system assignment service is supported. 
     *
     *  @return <code> true </code> if a scene to system assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneSystemAssignment() {
        return (getAdapteeManager().supportsSceneSystemAssignment());
    }


    /**
     *  Tests if a scene smart system service is supported. 
     *
     *  @return <code> true </code> if a scene smart system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSceneSmartSystem() {
        return (getAdapteeManager().supportsSceneSmartSystem());
    }


    /**
     *  Tests if looking up triggers is supported. 
     *
     *  @return <code> true </code> if trigger lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerLookup() {
        return (getAdapteeManager().supportsTriggerLookup());
    }


    /**
     *  Tests if querying triggers is supported. 
     *
     *  @return <code> true </code> if a trigger query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerQuery() {
        return (getAdapteeManager().supportsTriggerQuery());
    }


    /**
     *  Tests if searching triggers is supported. 
     *
     *  @return <code> true </code> if trigger search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerSearch() {
        return (getAdapteeManager().supportsTriggerSearch());
    }


    /**
     *  Tests if trigger administrative service is supported. 
     *
     *  @return <code> true </code> if trigger administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerAdmin() {
        return (getAdapteeManager().supportsTriggerAdmin());
    }


    /**
     *  Tests if a trigger <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if trigger notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerNotification() {
        return (getAdapteeManager().supportsTriggerNotification());
    }


    /**
     *  Tests if a trigger system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger system lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerSystem() {
        return (getAdapteeManager().supportsTriggerSystem());
    }


    /**
     *  Tests if a trigger system service is supported. 
     *
     *  @return <code> true </code> if trigger to system assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerSystemAssignment() {
        return (getAdapteeManager().supportsTriggerSystemAssignment());
    }


    /**
     *  Tests if a trigger smart system lookup service is supported. 
     *
     *  @return <code> true </code> if a trigger smart system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsTriggerSmartSystem() {
        return (getAdapteeManager().supportsTriggerSmartSystem());
    }


    /**
     *  Tests if looking up action groups is supported. 
     *
     *  @return <code> true </code> if action group lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupLookup() {
        return (getAdapteeManager().supportsActionGroupLookup());
    }


    /**
     *  Tests if querying action groups is supported. 
     *
     *  @return <code> true </code> if an action group query service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupQuery() {
        return (getAdapteeManager().supportsActionGroupQuery());
    }


    /**
     *  Tests if searching action groups is supported. 
     *
     *  @return <code> true </code> if action group search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupSearch() {
        return (getAdapteeManager().supportsActionGroupSearch());
    }


    /**
     *  Tests if action group administrative service is supported. 
     *
     *  @return <code> true </code> if action group administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupAdmin() {
        return (getAdapteeManager().supportsActionGroupAdmin());
    }


    /**
     *  Tests if an action group <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if action group notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupNotification() {
        return (getAdapteeManager().supportsActionGroupNotification());
    }


    /**
     *  Tests if an action group system lookup service is supported. 
     *
     *  @return <code> true </code> if an action group system lookup service 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupSystem() {
        return (getAdapteeManager().supportsActionGroupSystem());
    }


    /**
     *  Tests if an action group system service is supported. 
     *
     *  @return <code> true </code> if action group to system assignment 
     *          service is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupSystemAssignment() {
        return (getAdapteeManager().supportsActionGroupSystemAssignment());
    }


    /**
     *  Tests if an action group smart system lookup service is supported. 
     *
     *  @return <code> true </code> if an action group smart system service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsActionGroupSmartSystem() {
        return (getAdapteeManager().supportsActionGroupSmartSystem());
    }


    /**
     *  Tests if looking up systems is supported. 
     *
     *  @return <code> true </code> if system lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemLookup() {
        return (getAdapteeManager().supportsSystemLookup());
    }


    /**
     *  Tests if querying systems is supported. 
     *
     *  @return <code> true </code> if a system query service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (getAdapteeManager().supportsSystemQuery());
    }


    /**
     *  Tests if searching systems is supported. 
     *
     *  @return <code> true </code> if system search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemSearch() {
        return (getAdapteeManager().supportsSystemSearch());
    }


    /**
     *  Tests if system administrative service is supported. 
     *
     *  @return <code> true </code> if system administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemAdmin() {
        return (getAdapteeManager().supportsSystemAdmin());
    }


    /**
     *  Tests if a system <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if system notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemNotification() {
        return (getAdapteeManager().supportsSystemNotification());
    }


    /**
     *  Tests for the availability of a system hierarchy traversal service. 
     *
     *  @return <code> true </code> if system hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemHierarchy() {
        return (getAdapteeManager().supportsSystemHierarchy());
    }


    /**
     *  Tests for the availability of a system hierarchy design service. 
     *
     *  @return <code> true </code> if system hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemHierarchyDesign() {
        return (getAdapteeManager().supportsSystemHierarchyDesign());
    }


    /**
     *  Tests for the availability of a control batch service. 
     *
     *  @return <code> true </code> if control batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControlBatch() {
        return (getAdapteeManager().supportsControlBatch());
    }


    /**
     *  Tests for the availability of a control rules service. 
     *
     *  @return <code> true </code> if control rules service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsControlRules() {
        return (getAdapteeManager().supportsControlRules());
    }


    /**
     *  Gets the supported <code> Device </code> record types. 
     *
     *  @return a list containing the supported <code> Device </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceRecordTypes() {
        return (getAdapteeManager().getDeviceRecordTypes());
    }


    /**
     *  Tests if the given <code> Device </code> record type is supported. 
     *
     *  @param  deviceRecordType a <code> Type </code> indicating a <code> 
     *          Device </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deviceRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceRecordType(org.osid.type.Type deviceRecordType) {
        return (getAdapteeManager().supportsDeviceRecordType(deviceRecordType));
    }


    /**
     *  Gets the supported <code> Device </code> search types. 
     *
     *  @return a list containing the supported <code> Device </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceSearchRecordTypes() {
        return (getAdapteeManager().getDeviceSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Device </code> search type is supported. 
     *
     *  @param  deviceSearchRecordType a <code> Type </code> indicating a 
     *          <code> Device </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> effiortSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceSearchRecordType(org.osid.type.Type deviceSearchRecordType) {
        return (getAdapteeManager().supportsDeviceSearchRecordType(deviceSearchRecordType));
    }


    /**
     *  Gets the supported <code> DeviceReturn </code> record types. 
     *
     *  @return a list containing the supported <code> DeviceReturn </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getDeviceReturnRecordTypes() {
        return (getAdapteeManager().getDeviceReturnRecordTypes());
    }


    /**
     *  Tests if the given <code> DeviceReturn </code> record type is 
     *  supported. 
     *
     *  @param  deviceReturnRecordType a <code> Type </code> indicating a 
     *          <code> DeviceReturn </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> deviceReturnRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsDeviceReturnRecordType(org.osid.type.Type deviceReturnRecordType) {
        return (getAdapteeManager().supportsDeviceReturnRecordType(deviceReturnRecordType));
    }


    /**
     *  Gets the supported <code> Controller </code> record types. 
     *
     *  @return a list containing the supported <code> Controller </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getControllerRecordTypes() {
        return (getAdapteeManager().getControllerRecordTypes());
    }


    /**
     *  Tests if the given <code> Controller </code> record type is supported. 
     *
     *  @param  controllerRecordType a <code> Type </code> indicating a <code> 
     *          Controller </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> controllerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsControllerRecordType(org.osid.type.Type controllerRecordType) {
        return (getAdapteeManager().supportsControllerRecordType(controllerRecordType));
    }


    /**
     *  Gets the supported <code> Controller </code> search record types. 
     *
     *  @return a list containing the supported <code> Controller </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getControllerSearchRecordTypes() {
        return (getAdapteeManager().getControllerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Controller </code> search record type is 
     *  supported. 
     *
     *  @param  controllerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Controller </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          controllerSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsControllerSearchRecordType(org.osid.type.Type controllerSearchRecordType) {
        return (getAdapteeManager().supportsControllerSearchRecordType(controllerSearchRecordType));
    }


    /**
     *  Gets the supported <code> Input </code> record types. 
     *
     *  @return a list containing the supported <code> Input </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputRecordTypes() {
        return (getAdapteeManager().getInputRecordTypes());
    }


    /**
     *  Tests if the given <code> Input </code> record type is supported. 
     *
     *  @param  inputRecordType a <code> Type </code> indicating an <code> 
     *          Input </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inputRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputRecordType(org.osid.type.Type inputRecordType) {
        return (getAdapteeManager().supportsInputRecordType(inputRecordType));
    }


    /**
     *  Gets the supported <code> Input </code> search types. 
     *
     *  @return a list containing the supported <code> Input </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getInputSearchRecordTypes() {
        return (getAdapteeManager().getInputSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Input </code> search type is supported. 
     *
     *  @param  inputSearchRecordType a <code> Type </code> indicating an 
     *          <code> Input </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> inputSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsInputSearchRecordType(org.osid.type.Type inputSearchRecordType) {
        return (getAdapteeManager().supportsInputSearchRecordType(inputSearchRecordType));
    }


    /**
     *  Gets the supported <code> Setting </code> record types. 
     *
     *  @return a list containing the supported <code> Setting </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSettingRecordTypes() {
        return (getAdapteeManager().getSettingRecordTypes());
    }


    /**
     *  Tests if the given <code> Setting </code> record type is supported. 
     *
     *  @param  settingRecordType a <code> Type </code> indicating a <code> 
     *          Setting </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> settingRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSettingRecordType(org.osid.type.Type settingRecordType) {
        return (getAdapteeManager().supportsSettingRecordType(settingRecordType));
    }


    /**
     *  Gets the supported <code> Setting </code> search types. 
     *
     *  @return a list containing the supported <code> Setting </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSettingSearchRecordTypes() {
        return (getAdapteeManager().getSettingSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Setting </code> search type is supported. 
     *
     *  @param  settingSearchRecordType a <code> Type </code> indicating a 
     *          <code> Setting </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> settingSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSettingSearchRecordType(org.osid.type.Type settingSearchRecordType) {
        return (getAdapteeManager().supportsSettingSearchRecordType(settingSearchRecordType));
    }


    /**
     *  Gets the supported <code> Scene </code> record types. 
     *
     *  @return a list containing the supported <code> Scene </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSceneRecordTypes() {
        return (getAdapteeManager().getSceneRecordTypes());
    }


    /**
     *  Tests if the given <code> Scene </code> record type is supported. 
     *
     *  @param  sceneRecordType a <code> Type </code> indicating a <code> 
     *          Scene </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sceneRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSceneRecordType(org.osid.type.Type sceneRecordType) {
        return (getAdapteeManager().supportsSceneRecordType(sceneRecordType));
    }


    /**
     *  Gets the supported <code> Scene </code> search types. 
     *
     *  @return a list containing the supported <code> Scene </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSceneSearchRecordTypes() {
        return (getAdapteeManager().getSceneSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Scene </code> search type is supported. 
     *
     *  @param  sceneSearchRecordType a <code> Type </code> indicating a 
     *          <code> Scene </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> sceneSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSceneSearchRecordType(org.osid.type.Type sceneSearchRecordType) {
        return (getAdapteeManager().supportsSceneSearchRecordType(sceneSearchRecordType));
    }


    /**
     *  Gets the supported <code> Trigger </code> record types. 
     *
     *  @return a list containing the supported <code> Trigger </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerRecordTypes() {
        return (getAdapteeManager().getTriggerRecordTypes());
    }


    /**
     *  Tests if the given <code> Trigger </code> record type is supported. 
     *
     *  @param  triggerRecordType a <code> Type </code> indicating a <code> 
     *          Trigger </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> triggerRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerRecordType(org.osid.type.Type triggerRecordType) {
        return (getAdapteeManager().supportsTriggerRecordType(triggerRecordType));
    }


    /**
     *  Gets the supported <code> Trigger </code> search record types. 
     *
     *  @return a list containing the supported <code> Trigger </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getTriggerSearchRecordTypes() {
        return (getAdapteeManager().getTriggerSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> Trigger </code> search record type is 
     *  supported. 
     *
     *  @param  triggerSearchRecordType a <code> Type </code> indicating a 
     *          <code> Trigger </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> triggerSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTriggerSearchRecordType(org.osid.type.Type triggerSearchRecordType) {
        return (getAdapteeManager().supportsTriggerSearchRecordType(triggerSearchRecordType));
    }


    /**
     *  Gets the supported <code> ActionGroup </code> record types. 
     *
     *  @return a list containing the supported <code> ActionGroup </code> 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionGroupRecordTypes() {
        return (getAdapteeManager().getActionGroupRecordTypes());
    }


    /**
     *  Tests if the given <code> ActionGroup </code> record type is 
     *  supported. 
     *
     *  @param  actionGroupRecordType a <code> Type </code> indicating an 
     *          <code> ActionGroup </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> actionGroupRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionGroupRecordType(org.osid.type.Type actionGroupRecordType) {
        return (getAdapteeManager().supportsActionGroupRecordType(actionGroupRecordType));
    }


    /**
     *  Gets the supported <code> ActionGroup </code> search record types. 
     *
     *  @return a list containing the supported <code> ActionGroup </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionGroupSearchRecordTypes() {
        return (getAdapteeManager().getActionGroupSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> ActionGroup </code> search record type is 
     *  supported. 
     *
     *  @param  actionGroupSearchRecordType a <code> Type </code> indicating 
     *          an <code> ActionGroup </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          actionGroupSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionGroupSearchRecordType(org.osid.type.Type actionGroupSearchRecordType) {
        return (getAdapteeManager().supportsActionGroupSearchRecordType(actionGroupSearchRecordType));
    }


    /**
     *  Gets the supported <code> Action </code> record types. 
     *
     *  @return a list containing the supported <code> Action </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getActionRecordTypes() {
        return (getAdapteeManager().getActionRecordTypes());
    }


    /**
     *  Tests if the given <code> Action </code> record type is supported. 
     *
     *  @param  actionRecordType a <code> Type </code> indicating an <code> 
     *          Action </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> actionRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsActionRecordType(org.osid.type.Type actionRecordType) {
        return (getAdapteeManager().supportsActionRecordType(actionRecordType));
    }


    /**
     *  Gets the supported <code> System </code> record types. 
     *
     *  @return a list containing the supported <code> System </code> types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSystemRecordTypes() {
        return (getAdapteeManager().getSystemRecordTypes());
    }


    /**
     *  Tests if the given <code> System </code> record type is supported. 
     *
     *  @param  systemRecordType a <code> Type </code> indicating a <code> 
     *          System </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> systemRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSystemRecordType(org.osid.type.Type systemRecordType) {
        return (getAdapteeManager().supportsSystemRecordType(systemRecordType));
    }


    /**
     *  Gets the supported <code> System </code> search record types. 
     *
     *  @return a list containing the supported <code> System </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getSystemSearchRecordTypes() {
        return (getAdapteeManager().getSystemSearchRecordTypes());
    }


    /**
     *  Tests if the given <code> System </code> search record type is 
     *  supported. 
     *
     *  @param  systemSearchRecordType a <code> Type </code> indicating a 
     *          <code> System </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> systemSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsSystemSearchRecordType(org.osid.type.Type systemSearchRecordType) {
        return (getAdapteeManager().supportsSystemSearchRecordType(systemSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceLookupSession getDeviceLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceLookupSession getDeviceLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuerySession getDeviceQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceQuerySession getDeviceQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSearchSession getDeviceSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSearchSession getDeviceSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceAdminSession getDeviceAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceAdminSession getDeviceAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  notification service. 
     *
     *  @param  deviceReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> deviceReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceNotificationSession getDeviceNotificationSession(org.osid.control.DeviceReceiver deviceReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceNotificationSession(deviceReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the device 
     *  notification service for the given system. 
     *
     *  @param  deviceReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> deviceReceiver, systemId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceNotificationSession getDeviceNotificationSessionForSystem(org.osid.control.DeviceReceiver deviceReceiver, 
                                                                                            org.osid.id.Id systemId, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceNotificationSessionForSystem(deviceReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup device/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsDeviceSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSystemSession getDeviceSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning devices 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSystemAssignmentSession getDeviceSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage device smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> DeviceSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDeviceSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.DeviceSmartSystemSession getDeviceSmartSystemSession(org.osid.id.Id systemId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getDeviceSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerLookupSession getControllerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerLookupSession getControllerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuerySession getControllerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerQuerySession getControllerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchSession getControllerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  search service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSearchSession getControllerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerAdminSession getControllerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerAdminSession getControllerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  notification service. 
     *
     *  @param  controllerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> controllerReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerNotificationSession getControllerNotificationSession(org.osid.control.ControllerReceiver controllerReceiver, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerNotificationSession(controllerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the controller 
     *  notification service for the given system. 
     *
     *  @param  controllerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> controllerReceiver, 
     *          systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerNotificationSession getControllerNotificationSessionForSystem(org.osid.control.ControllerReceiver controllerReceiver, 
                                                                                                    org.osid.id.Id systemId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerNotificationSessionForSystem(controllerReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup controller/system 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSystemSession getControllerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  controllers to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSystemAssignmentSession getControllerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage controller smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> ControllerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsControllerSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ControllerSmartSystemSession getControllerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                         org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getControllerSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputLookupSession getInputLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputLookupSession getInputLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuerySession getInputQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputQuerySession getInputQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSearchSession getInputSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSearchSession getInputSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputAdminSession getInputAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return an <code> InputAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputAdminSession getInputAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  notification service. 
     *
     *  @param  inputReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> InputNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> inputReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputNotificationSession getInputNotificationSession(org.osid.control.InputReceiver inputReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputNotificationSession(inputReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the input 
     *  notification service for the given system. 
     *
     *  @param  inputReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> inputReceiver, systemId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputNotificationSession getInputNotificationSessionForSystem(org.osid.control.InputReceiver inputReceiver, 
                                                                                          org.osid.id.Id systemId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputNotificationSessionForSystem(inputReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup input/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsInputSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSystemSession getInputSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning inputs 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> InputSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.InputSystemAssignmentSession getInputSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getInputSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage input smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> InputSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsInputSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.InputSmartSystemSession getInputSmartSystemSession(org.osid.id.Id systemId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getInputSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingLookupSession getSettingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> SettingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingLookupSession getSettingLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuerySession getSettingQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuerySession getSettingQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSearchSession getSettingSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSearchSession getSettingSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingAdminSession getSettingAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  administration service for the given input. 
     *
     *  @param  inputId the <code> Id </code> of the <code> Input </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingAdminSession </code> 
     *  @throws org.osid.NotFoundException no input found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> inputId or proxy is null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingAdminSession getSettingAdminSessionForInput(org.osid.id.Id inputId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingAdminSessionForInput(inputId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  notification service. 
     *
     *  @param  settingReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SettingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> settingReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingNotificationSession getSettingNotificationSession(org.osid.control.SettingReceiver settingReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingNotificationSession(settingReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the setting 
     *  notification service for the given system. 
     *
     *  @param  settingReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> settingReceiver, </code> 
     *          <code> systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingNotificationSession getSettingNotificationSessionForSystem(org.osid.control.SettingReceiver settingReceiver, 
                                                                                              org.osid.id.Id systemId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingNotificationSessionForSystem(settingReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup setting/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSettingSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSystemSession getSettingSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning settings 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SettingSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSystemAssignmentSession getSettingSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SettingSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSettingSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingSmartSystemSession getSettingSmartSystemSession(org.osid.id.Id systemId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSettingSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneLookupSession getSceneLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the system 
     *  @param  proxy a proxy 
     *  @return a <code> SceneLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneLookupSession getSceneLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuerySession getSceneQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneQuerySession getSceneQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSearchSession getSceneSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSearchSession getSceneSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneAdminSession getSceneAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneAdminSession getSceneAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  notification service. 
     *
     *  @param  sceneReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SceneNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> sceneReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneNotificationSession getSceneNotificationSession(org.osid.control.SceneReceiver sceneReceiver, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneNotificationSession(sceneReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the scene 
     *  notification service for the given system. 
     *
     *  @param  sceneReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> sceneReceiver, </code> 
     *          <code> systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneNotificationSession getSceneNotificationSessionForSystem(org.osid.control.SceneReceiver sceneReceiver, 
                                                                                          org.osid.id.Id systemId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneNotificationSessionForSystem(sceneReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup scene/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSceneSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSystemSession getSceneSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning scenes 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SceneSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSystemAssignmentSession getSceneSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> SceneSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSceneSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SceneSmartSystemSession getSceneSmartSystemSession(org.osid.id.Id systemId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getSceneSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerLookupSession getTriggerLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger lookup 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerLookupSession getTriggerLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuerySession getTriggerQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger query 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerQuerySession getTriggerQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSearchSession getTriggerSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger search 
     *  service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSearchSession getTriggerSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerAdminSession getTriggerAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerAdminSession getTriggerAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  notification service. 
     *
     *  @param  triggerReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> triggerReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerNotificationSession getTriggerNotificationSession(org.osid.control.TriggerReceiver triggerReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerNotificationSession(triggerReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the trigger 
     *  notification service for the given system. 
     *
     *  @param  triggerReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> triggerReceiver, 
     *          systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerNotificationSession getTriggerNotificationSessionForSystem(org.osid.control.TriggerReceiver triggerReceiver, 
                                                                                              org.osid.id.Id systemId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerNotificationSessionForSystem(triggerReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup trigger/system mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsTriggerSystem() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSystemSession getTriggerSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning triggers 
     *  to systems. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSystemAssignmentSession getTriggerSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage trigger smart systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return a <code> TriggerSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsTriggerSmartSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.TriggerSmartSystemSession getTriggerSmartSystemSession(org.osid.id.Id systemId, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getTriggerSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupLookupSession getActionGroupLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  lookup service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> System </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupLookupSession getActionGroupLookupSessionForSystem(org.osid.id.Id systemId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupLookupSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuerySession getActionGroupQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  query service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupQuerySession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupQuerySession getActionGroupQuerySessionForSystem(org.osid.id.Id systemId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupQuerySessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSearchSession getActionGroupSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  search service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSearchSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSearchSession getActionGroupSearchSessionForSystem(org.osid.id.Id systemId, 
                                                                                          org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupSearchSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupAdminSession getActionGroupAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  administration service for the given system. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupAdminSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupAdminSession getActionGroupAdminSessionForSystem(org.osid.id.Id systemId, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupAdminSessionForSystem(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  notification service. 
     *
     *  @param  actionGroupReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> actionGroupReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupNotificationSession getActionGroupNotificationSession(org.osid.control.ActionGroupReceiver actionGroupReceiver, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupNotificationSession(actionGroupReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the action group 
     *  notification service for the given system. 
     *
     *  @param  actionGroupReceiver the notification callback 
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupNotificationSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> actionGroupReceiver, 
     *          systemId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupNotificationSession getActionGroupNotificationSessionForSystem(org.osid.control.ActionGroupReceiver actionGroupReceiver, 
                                                                                                      org.osid.id.Id systemId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupNotificationSessionForSystem(actionGroupReceiver, systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup action group/system 
     *  mappings. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSystemSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSystem() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSystemSession getActionGroupSystemSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupSystemSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning action 
     *  groups to systems. 
     *
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSystemAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSystemAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSystemAssignmentSession getActionGroupSystemAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupSystemAssignmentSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> to manage action group smart 
     *  systems. 
     *
     *  @param  systemId the <code> Id </code> of the <code> System </code> 
     *  @param  proxy a proxy 
     *  @return an <code> ActionGroupSmartSystemSession </code> 
     *  @throws org.osid.NotFoundException no system found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> systemId or proxy is 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsActionGroupSmartSystem() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.ActionGroupSmartSystemSession getActionGroupSmartSystemSession(org.osid.id.Id systemId, 
                                                                                           org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getActionGroupSmartSystemSession(systemId, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemLookupSession getSystemLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSystemLookupSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuerySession getSystemQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSystemQuerySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemSearchSession getSystemSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSystemSearchSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsSystemAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemAdminSession getSystemAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSystemAdminSession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  notification service. 
     *
     *  @param  systemReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> SystemNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> systemReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemNotificationSession getSystemNotificationSession(org.osid.control.SystemReceiver systemReceiver, 
                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSystemNotificationSession(systemReceiver, proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> SystemHierarchySession </code> for systems 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemHierarchySession getSystemHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSystemHierarchySession(proxy));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the system 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> HierarchyDesignSession </code> for systems 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsSystemHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.control.SystemHierarchyDesignSession getSystemHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getSystemHierarchyDesignSession(proxy));
    }


    /**
     *  Gets the <code> ControlBatchProxyManager. </code> 
     *
     *  @return a <code> ControlBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsControlBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.batch.ControlBatchProxyManager getControlBatchProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControlBatchProxyManager());
    }


    /**
     *  Gets the <code> ControlRulesProxyManager. </code> 
     *
     *  @return a <code> ControlRulesProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsControlRules() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.rules.ControlRulesProxyManager getControlRulesProxyManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getControlRulesProxyManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
