//
// MutableNodeConfigurationHierarchySession.java
//
//     Defines a Configuration hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration;


/**
 *  Defines a configuration hierarchy session for delivering a hierarchy
 *  of configurations using the ConfigurationNode interface.
 */

public final class MutableNodeConfigurationHierarchySession
    extends net.okapia.osid.jamocha.core.configuration.spi.AbstractNodeConfigurationHierarchySession
    implements org.osid.configuration.ConfigurationHierarchySession {


    /**
     *  Constructs a new
     *  <code>MutableNodeConfigurationHierarchySession</code> with no
     *  nodes.
     *
     *  @param hierarchy the hierarchy for this session
     *  @throws org.osid.NullArgumentException <code>hierarchy</code> 
     *          is <code>null</code>
     */

    public MutableNodeConfigurationHierarchySession(org.osid.hierarchy.Hierarchy hierarchy) {
        setHierarchy(hierarchy);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeConfigurationHierarchySession</code> using the
     *  root node for the hierarchy.
     *
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>node</code> 
     *          is <code>null</code>
     */

    public MutableNodeConfigurationHierarchySession(org.osid.configuration.ConfigurationNode root) {
        setHierarchy(new net.okapia.osid.jamocha.builder.hierarchy.hierarchy.HierarchyBuilder()
                     .id(root.getId())
                     .displayName(root.getConfiguration().getDisplayName())
                     .description(root.getConfiguration().getDescription())
                     .build());

        addRootConfiguration(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeConfigurationHierarchySession</code> using the
     *  given root as the root node.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param root a root node
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>root</code> is <code>null</code>
     */

    public MutableNodeConfigurationHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               org.osid.configuration.ConfigurationNode root) {
        setHierarchy(hierarchy);
        addRootConfiguration(root);
        return;
    }


    /**
     *  Constructs a new
     *  <code>MutableNodeConfigurationHierarchySession</code> using a
     *  collection of nodes as roots in the hierarchy.
     *
     *  @param hierarchy the hierarchy for this session
     *  @param roots a collection of root nodes
     *  @throws org.osid.NullArgumentException <code>hierarchy</code>
     *          or <code>roots</code> is <code>null</code>
     */

    public MutableNodeConfigurationHierarchySession(org.osid.hierarchy.Hierarchy hierarchy, 
                                               java.util.Collection<org.osid.configuration.ConfigurationNode> roots) {
        setHierarchy(hierarchy);
        addRootConfigurations(roots);
        return;
    }


    /**
     *  Adds a root configuration node to the hierarchy.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootConfiguration(org.osid.configuration.ConfigurationNode root) {
        super.addRootConfiguration(root);
        return;
    }


    /**
     *  Adds a collection of root configuration nodes.
     *
     *  @param roots hierarchy roots
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    @Override
    public void addRootConfigurations(java.util.Collection<org.osid.configuration.ConfigurationNode> roots) {
        super.addRootConfigurations(roots);
        return;
    }


    /**
     *  Removes a root configuration node from the hierarchy.
     *
     *  @param rootId a root node {@code Id}
     *  @throws org.osid.NullArgumentException <code>rootId</code> is
     *          <code>null</code>
     */

    @Override
    public void removeRootConfiguration(org.osid.id.Id rootId) {
        super.removeRootConfiguration(rootId);
        return;
    }
}
