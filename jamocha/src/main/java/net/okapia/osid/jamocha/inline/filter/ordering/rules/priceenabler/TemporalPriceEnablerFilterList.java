//
// TemporalPriceEnablerFilterList.java
//
//     Implements a filter for Temporal objects.
//
//
// Tom Coppeto
// Okapia
// 17 December 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.ordering.rules.priceenabler;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for Temporal objects.
 */

public final class TemporalPriceEnablerFilterList
    extends net.okapia.osid.jamocha.inline.filter.ordering.rules.priceenabler.spi.AbstractPriceEnablerFilterList
    implements org.osid.ordering.rules.PriceEnablerList,
               PriceEnablerFilter {

    private final org.osid.calendaring.DateTime start;
    private final org.osid.calendaring.DateTime end;


    /**
     *  Creates a new
     *  <code>TemporalPriceEnablerFilterList</code>. Temporals pass the
     *  filter if the given date range falls inside the start and end
     *  dates inclusive.
     *
     *  @param list a <code>PriceEnablerList</code>
     *  @param from start of date range
     *  @param to end of date range
     *  @throws org.osid.NullArgumentException <code>list</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     */

    public TemporalPriceEnablerFilterList(org.osid.ordering.rules.PriceEnablerList list, 
                                      org.osid.calendaring.DateTime from,
                                      org.osid.calendaring.DateTime to) {
        super(list);

        nullarg(from, "start date");
        nullarg(to, "end date");

        this.start = from;
        this.end = to;

        return;
    }    

    
    /**
     *  Filters PriceEnablers.
     *
     *  @param priceEnabler the price enabler to filter
     *  @return <code>true</code> if the price enabler passes the filter,
     *          <code>false</code> if the price enabler should be filtered
     */

    @Override
    public boolean pass(org.osid.ordering.rules.PriceEnabler priceEnabler) {
        if (priceEnabler.getStartDate().isGreater(this.start)) {
            return (false);
        }

        if (priceEnabler.getEndDate().isLess(this.end)) {
            return (false);
        }

        return (true);
    }
}
