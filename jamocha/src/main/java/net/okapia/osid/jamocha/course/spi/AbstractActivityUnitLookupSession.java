//
// AbstractActivityUnitLookupSession.java
//
//    A starter implementation framework for providing an ActivityUnit
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing an ActivityUnit
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getActivityUnits(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractActivityUnitLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.course.ActivityUnitLookupSession {

    private boolean pedantic      = false;
    private boolean federated     = false;
    private boolean activeonly    = false;
    private org.osid.course.CourseCatalog courseCatalog = new net.okapia.osid.jamocha.nil.course.coursecatalog.UnknownCourseCatalog();
    

    /**
     *  Gets the <code>CourseCatalog/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>CourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.courseCatalog.getId());
    }


    /**
     *  Gets the <code>CourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the <code>CourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.courseCatalog);
    }


    /**
     *  Sets the <code>CourseCatalog</code>.
     *
     *  @param  courseCatalog the course catalog for this session
     *  @throws org.osid.NullArgumentException <code>courseCatalog</code>
     *          is <code>null</code>
     */

    protected void setCourseCatalog(org.osid.course.CourseCatalog courseCatalog) {
        nullarg(courseCatalog, "course catalog");
        this.courseCatalog = courseCatalog;
        return;
    }


    /**
     *  Tests if this user can perform <code>ActivityUnit</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupActivityUnits() {
        return (true);
    }


    /**
     *  A complete view of the <code>ActivityUnit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeActivityUnitView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>ActivityUnit</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryActivityUnitView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include activity units in course catalogs which are
     *  children of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this course catalog only.
     */

    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active activity units whose are returned by methods in
     *  this session.
     */

    @OSID @Override
    public void useActiveActivityUnitView() {
       this.activeonly = true;         
       return;
    }


    /**
     *  All activity units of any active and inactive status are
     *  returned by all methods in this session.
     */

    @OSID @Override
    public void useAnyStatusActivityUnitView() {
        this.activeonly = false;
        return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */

    protected boolean isActiveOnly() {
        return (this.activeonly);
    }

     
    /**
     *  Gets the <code>ActivityUnit</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>ActivityUnit</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>ActivityUnit</code> and
     *  retained for compatibility.
     *
     *  In active mode, activity units are returned that are currently
     *  active.  In any status mode, active and inactive activity
     *  units expired are returned.
     *
     *  @param  activityUnitId <code>Id</code> of the
     *          <code>ActivityUnit</code>
     *  @return the activity unit
     *  @throws org.osid.NotFoundException <code>activityUnitId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>activityUnitId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnit getActivityUnit(org.osid.id.Id activityUnitId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.course.ActivityUnitList activityUnits = getActivityUnits()) {
            while (activityUnits.hasNext()) {
                org.osid.course.ActivityUnit activityUnit = activityUnits.getNextActivityUnit();
                if (activityUnit.getId().equals(activityUnitId)) {
                    return (activityUnit);
                }
            }
        } 

        throw new org.osid.NotFoundException(activityUnitId + " not found");
    }


    /**
     *  Gets an <code>ActivityUnitList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  activityUnits specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>ActivityUnits</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, activity units are returned that are currently
     *  active.  In any status mode, active and inactive activity
     *  units expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getActivityUnits()</code>.
     *
     *  @param  activityUnitIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>ActivityUnit</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByIds(org.osid.id.IdList activityUnitIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.course.ActivityUnit> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = activityUnitIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getActivityUnit(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("activity unit " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.course.activityunit.LinkedActivityUnitList(ret));
    }


    /**
     *  Gets an <code>ActivityUnitList</code> corresponding to the
     *  given activity unit genus <code>Type</code> which does not
     *  include activity units of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  units or an error results. Otherwise, the returned list may
     *  contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active.  In any status mode, active and inactive activity
     *  units expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getActivityUnits()</code>.
     *
     *  @param  activityUnitGenusType an activityUnit genus type 
     *  @return the returned <code>ActivityUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByGenusType(org.osid.type.Type activityUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activityunit.ActivityUnitGenusFilterList(getActivityUnits(), activityUnitGenusType));
    }


    /**
     *  Gets an <code>ActivityUnitList</code> corresponding to the given
     *  activity unit genus <code>Type</code> and include any additional
     *  activity units with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known activity
     *  units or an error results. Otherwise, the returned list may
     *  contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active.  In any status mode, active and inactive activity
     *  units expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivityUnits()</code>.
     *
     *  @param  activityUnitGenusType an activityUnit genus type 
     *  @return the returned <code>ActivityUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByParentGenusType(org.osid.type.Type activityUnitGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getActivityUnitsByGenusType(activityUnitGenusType));
    }


    /**
     *  Gets an <code>ActivityUnitList</code> containing the given
     *  activity unit record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known activity
     *  units or an error results. Otherwise, the returned list may
     *  contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active.  In any status mode, active and inactive activity
     *  units expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getActivityUnits()</code>.
     *
     *  @param  activityUnitRecordType an activityUnit record type 
     *  @return the returned <code>ActivityUnit</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>activityUnitRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsByRecordType(org.osid.type.Type activityUnitRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activityunit.ActivityUnitRecordFilterList(getActivityUnits(), activityUnitRecordType));
    }


    /**
     *  Gets an <code>ActivityUnitList</code> for the given course. In
     *  plenary mode, the returned list contains all known activity
     *  units or an error results. Otherwise, the returned list may
     *  contain only those activity units that are accessible through
     *  this session.
     *
     *  @param  courseId a course <code>Id</code> 
     *  @return the returned <code>ActivityUnit</code> list 
     *  @throws org.osid.NullArgumentException <code>courseId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.course.ActivityUnitList getActivityUnitsForCourse(org.osid.id.Id courseId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.course.activityunit.ActivityUnitFilterList(new CourseFilter(courseId), getActivityUnits()));
    }

    
    /**
     *  Gets all <code>ActivityUnits</code>. 
     *
     *  In plenary mode, the returned list contains all known activity
     *  units or an error results. Otherwise, the returned list may
     *  contain only those activity units that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, activity units are returned that are currently
     *  active.  In any status mode, active and inactive activity
     *  units expired are returned.
     *
     *  @return a list of <code>ActivityUnits</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.course.ActivityUnitList getActivityUnits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the activity unit list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of activity units
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.course.ActivityUnitList filterActivityUnitsOnViews(org.osid.course.ActivityUnitList list)
        throws org.osid.OperationFailedException {

        org.osid.course.ActivityUnitList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.course.activityunit.ActiveActivityUnitFilterList(ret);
        }

        return (ret);
    }


    public static class CourseFilter
        implements net.okapia.osid.jamocha.inline.filter.course.activityunit.ActivityUnitFilter {

        private final org.osid.id.Id courseId;

        
        /**
         *  Constructs a new <code>CourseFilter</code>.
         *
         *  @param courseId the course to filter
         *  @throws org.osid.NullArgumentException
         *          <code>courseId</code> is <code>null</code>
         */

        public CourseFilter(org.osid.id.Id courseId) {
            nullarg(courseId, "course Id");
            this.courseId = courseId;
            return;
        }


        /**
         *  Used by the ActivityUnitFilterList to filter the activity unit
         *  list based on course.
         *
         *  @param activityUnit the activity unit
         *  @return <code>true</code> to pass the activity unit,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.course.ActivityUnit activityUnit) {
            return (activityUnit.getCourseId().equals(this.courseId));
        }
    }        
}
