//
// AbstractAuthorizationSearchOdrer.java
//
//     Defines an AuthorizationSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.authorization.authorization.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines an {@code AuthorizationSearchOrder}.
 */

public abstract class AbstractAuthorizationSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipSearchOrder
    implements org.osid.authorization.AuthorizationSearchOrder {

    private final java.util.Collection<org.osid.authorization.records.AuthorizationSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Specifies a preference for ordering the result set by the resource. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByResource(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> Resource </code> is available. 
     *
     *  @return <code> true </code> if a resource search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsResourceSearchOrder() {
        return (false);
    }


    /**
     *  Gets the resource search order. 
     *
     *  @return the resource search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsResourceSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.resource.ResourceSearchOrder getResourceSearchOrder() {
        throw new org.osid.UnimplementedException("supportsResourceSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the trust. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByTrust(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Specifies a preference for ordering the result set by the agent. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAgent(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if an <code> Agent </code> is available. 
     *
     *  @return <code> true </code> if an agent search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAgentSearchOrder() {
        return (false);
    }


    /**
     *  Gets the agent search order. 
     *
     *  @return the agent search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAgentSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authentication.AgentSearchOrder getAgentSearchOrder() {
        throw new org.osid.UnimplementedException("supportsAgentSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the active 
     *  status. 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByFunction(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> Function </code> is available. 
     *
     *  @return <code> true </code> if a function search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsFunctionSearchOrder() {
        return (false);
    }


    /**
     *  Gets the function search order. 
     *
     *  @return the function search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsFunctionSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.FunctionSearchOrder getFunctionSearchOrder() {
        throw new org.osid.UnimplementedException("supportsFunctionSearchOrder() is false");
    }


    /**
     *  Specifies a preference for ordering the result set by the qualifier, 
     *
     *  @param  style the search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByQualifier(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Tests if a <code> Qualifier </code> is available. 
     *
     *  @return <code> true </code> if a qualifier search order interface is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsQualifierSearchOrder() {
        return (false);
    }


    /**
     *  Gets the qualifier search order. 
     *
     *  @return the qualifier search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsQualifierSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.authorization.QualifierSearchOrder getQualifierSearchOrder() {
        throw new org.osid.UnimplementedException("supportsQualifierSearchOrder() is false");
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  authorizationRecordType an authorization record type 
     *  @return {@code true} if the authorizationRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code authorizationRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type authorizationRecordType) {
        for (org.osid.authorization.records.AuthorizationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  authorizationRecordType the authorization record type 
     *  @return the authorization search order record
     *  @throws org.osid.NullArgumentException
     *          {@code authorizationRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(authorizationRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.authorization.records.AuthorizationSearchOrderRecord getAuthorizationSearchOrderRecord(org.osid.type.Type authorizationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.authorization.records.AuthorizationSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(authorizationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(authorizationRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this authorization. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param authorizationRecord the authorization search odrer record
     *  @param authorizationRecordType authorization record type
     *  @throws org.osid.NullArgumentException
     *          {@code authorizationRecord} or
     *          {@code authorizationRecordTypeauthorization} is
     *          {@code null}
     */
            
    protected void addAuthorizationRecord(org.osid.authorization.records.AuthorizationSearchOrderRecord authorizationSearchOrderRecord, 
                                     org.osid.type.Type authorizationRecordType) {

        addRecordType(authorizationRecordType);
        this.records.add(authorizationSearchOrderRecord);
        
        return;
    }
}
