//
// AbstractNodeDistributorHierarchySession.java
//
//     Defines a Distributor hierarchy session based on nodes.
//
//
// Tom Coppeto
// Okapia
// 17 September 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a distributor hierarchy session for delivering a hierarchy
 *  of distributors using the DistributorNode interface.
 */

public abstract class AbstractNodeDistributorHierarchySession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractDistributorHierarchySession
    implements org.osid.provisioning.DistributorHierarchySession {

    private java.util.Collection<org.osid.provisioning.DistributorNode> roots = new java.util.ArrayList<>();


    /**
     *  Gets the root distributor <code> Ids </code> in this hierarchy.
     *
     *  @return the root distributor <code> Ids </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getRootDistributorIds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.provisioning.distributornode.DistributorNodeToIdList(this.roots));
    }


    /**
     *  Gets the root distributors in the distributor hierarchy. A node
     *  with no parents is an orphan. While all distributor <code> Ids
     *  </code> are known to the hierarchy, an orphan does not appear
     *  in the hierarchy unless explicitly added as a root node or
     *  child of another node.
     *
     *  @return the root distributors 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getRootDistributors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.provisioning.distributornode.DistributorNodeToDistributorList(new net.okapia.osid.jamocha.provisioning.distributornode.ArrayDistributorNodeList(this.roots)));
    }


    /**
     *  Adds a root distributor node.
     *
     *  @param root the hierarchy root
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void addRootDistributor(org.osid.provisioning.DistributorNode root) {
        nullarg(root, "root");
        this.roots.add(root);
        return;
    }


    /**
     *  Adds root distributor nodes.
     *
     *  @param roots the roots of the hierarchy
     *  @throws org.osid.NullArgumentException <code>roots</code> is
     *          <code>null</code>
     */

    protected void addRootDistributors(java.util.Collection<org.osid.provisioning.DistributorNode> roots) {
        nullarg(roots, "roots");
        this.roots.addAll(roots);
        return;
    }


    /**
     *  Removes a root distributor node.
     *
     *  @param rootId the hierarchy root Id
     *  @throws org.osid.NullArgumentException <code>root</code> is
     *          <code>null</code>
     */

    protected void removeRootDistributor(org.osid.id.Id rootId) {
        nullarg(rootId, "root Id");

        for (org.osid.provisioning.DistributorNode node : this.roots) {
            if (node.getId().equals(rootId)) {
                this.roots.remove(node);
            }
        }

        return;
    }


    /**
     *  Tests if the <code> Distributor </code> has any parents. 
     *
     *  @param  distributorId a distributor <code> Id </code> 
     *  @return <code> true </code> if the distributor has parents,
     *          <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasParentDistributors(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (getDistributorNode(distributorId).hasParents());
    }
        

    /**
     *  Tests if an <code> Id </code> is a direct parent of a
     *  distributor.
     *
     *  @param  id an <code> Id </code> 
     *  @param  distributorId the <code> Id </code> of a distributor 
     *  @return <code> true </code> if this <code> id </code> is a
     *          parent of <code> distributorId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> distributorId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isParentOfDistributor(org.osid.id.Id id, org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.provisioning.DistributorNodeList parents = getDistributorNode(distributorId).getParentDistributorNodes()) {
            while (parents.hasNext()) {
                if (id.equals(parents.getNextDistributorNode().getId())) {
                    return (true);
                }
            }
        }

        return (false); 
    }


    /**
     *  Gets the parent <code> Ids </code> of the given distributor. 
     *
     *  @param  distributorId a distributor <code> Id </code> 
     *  @return the parent <code> Ids </code> of the distributor 
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getParentDistributorIds(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.provisioning.distributor.DistributorToIdList(getParentDistributors(distributorId)));
    }


    /**
     *  Gets the parents of the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> to query 
     *  @return the parents of the distributor 
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getParentDistributors(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.provisioning.distributornode.DistributorNodeToDistributorList(getDistributorNode(distributorId).getParentDistributorNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is an ancestor of a
     *  distributor.
     *
     *  @param  id an <code> Id </code> 
     *  @param  distributorId the Id of a distributor 
     *  @return <code> true </code> if this <code> id </code> is an
     *          ancestor of <code> distributorId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isAncestorOfDistributor(org.osid.id.Id id, org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfDistributor(id, distributorId)) {
            return (true);
        }

        try (org.osid.provisioning.DistributorList parents = getParentDistributors(distributorId)) {
            while (parents.hasNext()) {
                if (isAncestorOfDistributor(id, parents.getNextDistributor().getId())) {
                    return (true);
                }
            }
        }
        
        return (false);
    }


    /**
     *  Tests if a distributor has any children. 
     *
     *  @param  distributorId a distributor <code> Id </code> 
     *  @return <code> true </code> if the <code> distributorId </code>
     *          has children, <code> false </code> otherwise
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean hasChildDistributors(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDistributorNode(distributorId).hasChildren());
    }


    /**
     *  Tests if an <code> Id </code> is a direct child of a
     *  distributor.
     *
     *  @param  id an <code> Id </code> 
     *  @param distributorId the <code> Id </code> of a 
     *         distributor
     *  @return <code> true </code> if this <code> id </code> is a
     *          child of <code> distributorId, </code> <code> false
     *          </code> otherwise
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code> id </code> or
     *          <code> distributorId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isChildOfDistributor(org.osid.id.Id id, org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (isParentOfDistributor(distributorId, id));
    }


    /**
     *  Gets the <code> Ids </code> of the children of the given
     *  distributor.
     *
     *  @param  distributorId the <code> Id </code> to query 
     *  @return the children of the distributor 
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.id.IdList getChildDistributorIds(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.provisioning.distributor.DistributorToIdList(getChildDistributors(distributorId)));
    }


    /**
     *  Gets the children of the given distributor. 
     *
     *  @param  distributorId the <code> Id </code> to query 
     *  @return the children of the distributor 
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorList getChildDistributors(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.adapter.converter.provisioning.distributornode.DistributorNodeToDistributorList(getDistributorNode(distributorId).getChildDistributorNodes()));
    }


    /**
     *  Tests if an <code> Id </code> is a descendant of a
     *  distributor.
     *
     *  @param  id an <code> Id </code> 
     *  @param distributorId the <code> Id </code> of a 
     *         distributor
     *  @return <code> true </code> if the <code> id </code> is a
     *          descendant of the <code> distributorId, </code> <code>
     *          false </code> otherwise
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> or <code> id </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean isDescendantOfDistributor(org.osid.id.Id id, org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        if (isParentOfDistributor(distributorId, id)) {
            return (true);
        }

        try (org.osid.provisioning.DistributorList children = getChildDistributors(distributorId)) {
            while (children.hasNext()) {
                if (isDescendantOfDistributor(id, children.getNextDistributor().getId())) {
                    return (true);
                }
            }
        }

        return (false);
    }


    /**
     *  Gets a portion of the hierarchy for the given 
     *  distributor.
     *
     *  @param  distributorId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified distributor node 
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.hierarchy.Node getDistributorNodeIds(org.osid.id.Id distributorId, 
                                                      long ancestorLevels, 
                                                      long descendantLevels, 
                                                      boolean includeSiblings)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.adapter.converter.provisioning.distributornode.DistributorNodeToNode(getDistributorNode(distributorId)));
    }


    /**
     *  Gets a portion of the hierarchy for the given distributor.
     *
     *  @param  distributorId the <code> Id </code> to query 
     *  @param ancestorLevels the maximum number of ancestor levels to
     *          include. A value of 0 returns no parents in the node.
     *  @param descendantLevels the maximum number of descendant
     *          levels to include. A value of 0 returns no children in
     *          the node.
     *  @param includeSiblings <code> true </code> to include the
     *          siblings of the given node, <code> false </code> to
     *          omit the siblings
     *  @return the specified distributor node 
     *  @throws org.osid.NotFoundException <code> distributorId </code>
     *          not found
     *  @throws org.osid.NullArgumentException <code> distributorId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.InvalidArgumentException cardinal value is negative 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.DistributorNode getDistributorNodes(org.osid.id.Id distributorId, 
                                                             long ancestorLevels, 
                                                             long descendantLevels, 
                                                             boolean includeSiblings)
            throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getDistributorNode(distributorId));
    }


    /**
     *  Closes this <code>DistributorHierarchySession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.roots.clear();
        super.close();
        return;
    }


    /**
     *  Gets a distributor node.
     *
     *  @param distributorId the id of the distributor node
     *  @throws org.osid.NotFoundException <code>distributorId</code>
     *          is not found
     *  @throws org.osid.NullArgumentException <code>distributorId</code>
     *          is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    protected org.osid.provisioning.DistributorNode getDistributorNode(org.osid.id.Id distributorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        nullarg(distributorId, "distributor Id");
        for (org.osid.provisioning.DistributorNode distributor : this.roots) {
            if (distributor.getId().equals(distributorId)) {
                return (distributor);
            }

            org.osid.provisioning.DistributorNode r = findDistributor(distributor, distributorId);
            if (r != null) {
                return (r);
            }
        }
            
        throw new org.osid.NotFoundException(distributorId + " is not found");
    }


    protected org.osid.provisioning.DistributorNode findDistributor(org.osid.provisioning.DistributorNode node, 
                                                                    org.osid.id.Id distributorId)
	throws org.osid.OperationFailedException {

        try (org.osid.provisioning.DistributorNodeList children = node.getChildDistributorNodes()) {
            while (children.hasNext()) {
                org.osid.provisioning.DistributorNode distributor = children.getNextDistributorNode();
                if (distributor.getId().equals(distributorId)) {
                    return (distributor);
                }
                
                distributor = findDistributor(distributor, distributorId);
                if (distributor != null) {
                    return (distributor);
                }
            }
        }

        return (null);
    }
}
