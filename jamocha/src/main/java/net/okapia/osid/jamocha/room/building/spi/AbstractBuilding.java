//
// AbstractBuilding.java
//
//     Defines a Building.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.building.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.cardinalarg;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>Building</code>.
 */

public abstract class AbstractBuilding
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObject
    implements org.osid.room.Building {

    private org.osid.contact.Address address;
    private org.osid.locale.DisplayText officialName;
    private String number;
    private org.osid.room.Building enclosingBuilding;
    private final java.util.Collection<org.osid.room.Building> subdivisions = new java.util.LinkedHashSet<>();
    private java.math.BigDecimal area;

    private final java.util.Collection<org.osid.room.records.BuildingRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the address of this building. 
     *
     *  @return the building address <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAddressId() {
        return (this.address.getId());
    }


    /**
     *  Gets the building address. 
     *
     *  @return the building address. 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.contact.Address getAddress()
        throws org.osid.OperationFailedException {

        return (this.address);
    }


    /**
     *  Sets the address.
     *
     *  @param address an address
     *  @throws org.osid.NullArgumentException
     *          <code>address</code> is <code>null</code>
     */

    protected void setAddress(org.osid.contact.Address address) {
        nullarg(address, "address");
        this.address = address;
        return;
    }


    /**
     *  Gets the formal name for this building (e.g. Guggenheim Laboratory). 
     *  The display name may or may not differ from this name but may be based 
     *  on the code (e.g. Building 35). 
     *
     *  @return the name 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getOfficialName() {
        return (this.officialName);
    }


    /**
     *  Sets the official name.
     *
     *  @param officialName an official name
     *  @throws org.osid.NullArgumentException
     *          <code>officialName</code> is <code>null</code>
     */

    protected void setOfficialName(org.osid.locale.DisplayText officialName) {
        this.officialName = officialName;
        return;
    }


    /**
     *  Gets the building number or code (e.g. 35). 
     *
     *  @return the building number 
     */

    @OSID @Override
    public String getNumber() {
        return (this.number);
    }


    /**
     *  Sets the number.
     *
     *  @param number a number
     *  @throws org.osid.NullArgumentException
     *          <code>number</code> is <code>null</code>
     */

    protected void setNumber(String number) {
        nullarg(number, "number");
        this.number = number;
        return;
    }


    /**
     *  Tests if this building is a subdivision of another building. 
     *
     *  @return <code> true </code> if this building is a subdivisiaion, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean isSubdivision() {
        return (this.enclosingBuilding != null);
    }


    /**
     *  Gets the enclosing building <code> Id </code> if this building is a 
     *  subdivision. 
     *
     *  @return the enclosing building <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isSubdivision() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getEnclosingBuildingId() {
        if (!isSubdivision()) {
            throw new org.osid.IllegalStateException("isSubdivision() is false");
        }

        return (this.enclosingBuilding.getId());
    }


    /**
     *  Gets the enclosing building if this building is a subdivision. 
     *
     *  @return the enlcosing building 
     *  @throws org.osid.IllegalStateException <code> isSubdivision() </code> 
     *          is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.Building getEnclosingBuilding()
        throws org.osid.OperationFailedException {

        if (!isSubdivision()) {
            throw new org.osid.IllegalStateException("isSubdivision() is false");
        }

        return (this.enclosingBuilding);
    }


    /**
     *  Sets the enclosing building.
     *
     *  @param building an enclosing building
     *  @throws org.osid.NullArgumentException <code>building</code>
     *          is <code>null</code>
     */

    protected void setEnclosingBuilding(org.osid.room.Building building) {
        nullarg(building, "enclosing building");
        this.enclosingBuilding = building;
        return;
    }


    /**
     *  Gets the subdivision building <code> Ids </code> if this building is 
     *  subdivided. 
     *
     *  @return the subdivision building <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSubdivisionIds() {
        try {
            org.osid.room.BuildingList subdivisions = getSubdivisions();
            return (new net.okapia.osid.jamocha.adapter.converter.room.building.BuildingToIdList(subdivisions));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the subdivision buildings if this building is subdivided. 
     *
     *  @return the subdivision buildings 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.BuildingList getSubdivisions()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.room.building.ArrayBuildingList(this.subdivisions));
    }


    /**
     *  Adds a subdivision.
     *
     *  @param subdivision a subdivision
     *  @throws org.osid.NullArgumentException
     *          <code>subdivision</code> is <code>null</code>
     */

    protected void addSubdivision(org.osid.room.Building subdivision) {
        nullarg(subdivision, "subdivision");
        this.subdivisions.add(subdivision);
        return;
    }


    /**
     *  Sets all the subdivisions.
     *
     *  @param subdivisions a collection of subdivisions
     *  @throws org.osid.NullArgumentException
     *          <code>subdivisions</code> is <code>null</code>
     */

    protected void setSubdivisions(java.util.Collection<org.osid.room.Building> subdivisions) {
        nullarg(subdivisions, "subdivisions");
        this.subdivisions.clear();
        this.subdivisions.addAll(subdivisions);
        return;
    }


    /**
     *  Tests if an area is available. 
     *
     *  @return <code> true </code> if an area is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean hasArea() {
        return (this.area != null);
    }


    /**
     *  Gets the gross square footage of this building. 
     *
     *  @return the gross area 
     *  @throws org.osid.IllegalStateException <code> hasArea() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getGrossArea() {
        if (!hasArea()) {
            throw new org.osid.IllegalStateException("hasArea() is false");
        }

        return (this.area);
    }


    /**
     *  Sets the gross area.
     *
     *  @param area a gross area
     *  @throws org.osid.InvalidArgumentException <code>area</code> is
     *          negative
     *  @throws org.osid.NullArgumentException <code>area</code> is
     *          <code>null</code>
     */

    protected void setGrossArea(java.math.BigDecimal area) {
        cardinalarg(area, "area");
        this.area = area;
        return;
    }


    /**
     *  Tests if this building supports the given record
     *  <code>Type</code>.
     *
     *  @param  buildingRecordType a building record type 
     *  @return <code>true</code> if the buildingRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type buildingRecordType) {
        for (org.osid.room.records.BuildingRecord record : this.records) {
            if (record.implementsRecordType(buildingRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Building</code> record <code>Type</code>.
     *
     *  @param  buildingRecordType the building record type 
     *  @return the building record 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(buildingRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.BuildingRecord getBuildingRecord(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.BuildingRecord record : this.records) {
            if (record.implementsRecordType(buildingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(buildingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this building. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param buildingRecord the building record
     *  @param buildingRecordType building record type
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecord</code> or
     *          <code>buildingRecordTypebuilding</code> is
     *          <code>null</code>
     */
            
    protected void addBuildingRecord(org.osid.room.records.BuildingRecord buildingRecord, 
                                     org.osid.type.Type buildingRecordType) {

        nullarg(buildingRecord, "building record");
        addRecordType(buildingRecordType);
        this.records.add(buildingRecord);
        
        return;
    }
}
