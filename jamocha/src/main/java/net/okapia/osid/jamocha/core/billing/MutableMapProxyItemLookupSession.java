//
// MutableMapProxyItemLookupSession
//
//    Implements an Item lookup service backed by a collection of
//    items that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.billing;


/**
 *  Implements an Item lookup service backed by a collection of
 *  items. The items are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of items can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyItemLookupSession
    extends net.okapia.osid.jamocha.core.billing.spi.AbstractMapItemLookupSession
    implements org.osid.billing.ItemLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyItemLookupSession}
     *  with no items.
     *
     *  @param business the business
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyItemLookupSession(org.osid.billing.Business business,
                                                  org.osid.proxy.Proxy proxy) {
        setBusiness(business);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyItemLookupSession} with a
     *  single item.
     *
     *  @param business the business
     *  @param item an item
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code item}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyItemLookupSession(org.osid.billing.Business business,
                                                org.osid.billing.Item item, org.osid.proxy.Proxy proxy) {
        this(business, proxy);
        putItem(item);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyItemLookupSession} using an
     *  array of items.
     *
     *  @param business the business
     *  @param items an array of items
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code items}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyItemLookupSession(org.osid.billing.Business business,
                                                org.osid.billing.Item[] items, org.osid.proxy.Proxy proxy) {
        this(business, proxy);
        putItems(items);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyItemLookupSession} using a
     *  collection of items.
     *
     *  @param business the business
     *  @param items a collection of items
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code business},
     *          {@code items}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyItemLookupSession(org.osid.billing.Business business,
                                                java.util.Collection<? extends org.osid.billing.Item> items,
                                                org.osid.proxy.Proxy proxy) {
   
        this(business, proxy);
        setSessionProxy(proxy);
        putItems(items);
        return;
    }

    
    /**
     *  Makes a {@code Item} available in this session.
     *
     *  @param item an item
     *  @throws org.osid.NullArgumentException {@code item{@code 
     *          is {@code null}
     */

    @Override
    public void putItem(org.osid.billing.Item item) {
        super.putItem(item);
        return;
    }


    /**
     *  Makes an array of items available in this session.
     *
     *  @param items an array of items
     *  @throws org.osid.NullArgumentException {@code items{@code 
     *          is {@code null}
     */

    @Override
    public void putItems(org.osid.billing.Item[] items) {
        super.putItems(items);
        return;
    }


    /**
     *  Makes collection of items available in this session.
     *
     *  @param items
     *  @throws org.osid.NullArgumentException {@code item{@code 
     *          is {@code null}
     */

    @Override
    public void putItems(java.util.Collection<? extends org.osid.billing.Item> items) {
        super.putItems(items);
        return;
    }


    /**
     *  Removes a Item from this session.
     *
     *  @param itemId the {@code Id} of the item
     *  @throws org.osid.NullArgumentException {@code itemId{@code  is
     *          {@code null}
     */

    @Override
    public void removeItem(org.osid.id.Id itemId) {
        super.removeItem(itemId);
        return;
    }    
}
