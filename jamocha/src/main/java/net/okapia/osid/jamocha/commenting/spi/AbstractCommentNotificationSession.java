//
// AbstractCommentNotificationSession.java
//
//     A template for making CommentNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.commenting.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Comment} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Comment} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for comment entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractCommentNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.commenting.CommentNotificationSession {

    private boolean federated = false;
    private org.osid.commenting.Book book = new net.okapia.osid.jamocha.nil.commenting.book.UnknownBook();


    /**
     *  Gets the {@code Book/code> {@code Id} associated with this
     *  session.
     *
     *  @return the {@code Book Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getBookId() {
        return (this.book.getId());
    }

    
    /**
     *  Gets the {@code Book} associated with this session.
     *
     *  @return the {@code Book} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.commenting.Book getBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.book);
    }


    /**
     *  Sets the {@code Book}.
     *
     *  @param book the book for this session
     *  @throws org.osid.NullArgumentException {@code book}
     *          is {@code null}
     */

    protected void setBook(org.osid.commenting.Book book) {
        nullarg(book, "book");
        this.book = book;
        return;
    }


    /**
     *  Tests if this user can register for {@code Comment}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForCommentNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeCommentNotification() </code>.
     */

    @OSID @Override
    public void reliableCommentNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableCommentNotifications() {
        return;
    }


    /**
     *  Acknowledge a comment notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeCommentNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for comments in books which
     *  are children of this book in the book hierarchy.
     */

    @OSID @Override
    public void useFederatedBookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this book only.
     */

    @OSID @Override
    public void useIsolatedBookView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new comments. {@code
     *  CommentReceiver.newComment()} is invoked when a new {@code
     *  Comment} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewComments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new comments for the given
     *  commentor {@code Id}. {@code CommentReceiver.newComment()} is
     *  invoked when a new {@code Comment} is created.
     *
     *  @param  resourceId the {@code Id} of the commentor to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewCommentsForCommentor(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new comments for the given
     *  reference {@code Id}. {@code CommentReceiver.newComment()} is
     *  invoked when a new {@code Comment} is created.
     *
     *  @param  referenceId the {@code Id} of the reference to monitor
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewCommentsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated comments. {@code
     *  CommentReceiver.changedComment()} is invoked when a comment is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedComments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated comments for the given
     *  commentor {@code Id}. {@code CommentReceiver.changedComment()}
     *  is invoked when a {@code Comment} in this book is changed.
     *
     *  @param  resourceId the {@code Id} of the commentor to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedCommentsForCommentor(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated comments for the given
     *  reference {@code Id}. {@code CommentReceiver.changedComment()}
     *  is invoked when a {@code Comment} in this book is changed.
     *
     *  @param  referenceId the {@code Id} of the reference to monitor
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedCommentsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated comment. {@code
     *  CommentReceiver.changedComment()} is invoked when the
     *  specified comment is changed.
     *
     *  @param commentId the {@code Id} of the {@code Comment} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code commentId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedComment(org.osid.id.Id commentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted comments. {@code
     *  CommentReceiver.deletedComment()} is invoked when a comment is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedComments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted comments for the given
     *  commentor {@code Id}. {@code CommentReceiver.deletedComment()}
     *  is invoked when a {@code Comment} is deleted or removed from
     *  this book.
     *
     *  @param  resourceId the {@code Id} of the commentor to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedCommentsForCommentor(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted comments for the given
     *  reference {@code Id}. {@code CommentReceiver.deletedComment()}
     *  is invoked when a {@code Comment} is deleted or removed from
     *  this book.
     *
     *  @param  referenceId the {@code Id} of the reference to monitor
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedCommentsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted comment. {@code
     *  CommentReceiver.deletedComment()} is invoked when the
     *  specified comment is deleted.
     *
     *  @param commentId the {@code Id} of the
     *          {@code Comment} to monitor
     *  @throws org.osid.NullArgumentException {@code commentId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedComment(org.osid.id.Id commentId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
