//
// AbstractQueryPaymentLookupSession.java
//
//    An inline adapter that maps a PaymentLookupSession to
//    a PaymentQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.billing.payment.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a PaymentLookupSession to
 *  a PaymentQuerySession.
 */

public abstract class AbstractQueryPaymentLookupSession
    extends net.okapia.osid.jamocha.billing.payment.spi.AbstractPaymentLookupSession
    implements org.osid.billing.payment.PaymentLookupSession {

    private final org.osid.billing.payment.PaymentQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryPaymentLookupSession.
     *
     *  @param querySession the underlying payment query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryPaymentLookupSession(org.osid.billing.payment.PaymentQuerySession querySession) {
        nullarg(querySession, "payment query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Business</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Business Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBusinessId() {
        return (this.session.getBusinessId());
    }


    /**
     *  Gets the <code>Business</code> associated with this 
     *  session.
     *
     *  @return the <code>Business</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.Business getBusiness()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getBusiness());
    }


    /**
     *  Tests if this user can perform <code>Payment</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPayments() {
        return (this.session.canSearchPayments());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include payments in businesses which are children
     *  of this business in the business hierarchy.
     */

    @OSID @Override
    public void useFederatedBusinessView() {
        this.session.useFederatedBusinessView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this business only.
     */

    @OSID @Override
    public void useIsolatedBusinessView() {
        this.session.useIsolatedBusinessView();
        return;
    }
    
     
    /**
     *  Gets the <code>Payment</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Payment</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Payment</code> and
     *  retained for compatibility.
     *
     *  @param  paymentId <code>Id</code> of the
     *          <code>Payment</code>
     *  @return the payment
     *  @throws org.osid.NotFoundException <code>paymentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>paymentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.Payment getPayment(org.osid.id.Id paymentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchId(paymentId, true);
        org.osid.billing.payment.PaymentList payments = this.session.getPaymentsByQuery(query);
        if (payments.hasNext()) {
            return (payments.getNextPayment());
        } 
        
        throw new org.osid.NotFoundException(paymentId + " not found");
    }


    /**
     *  Gets a <code>PaymentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  payments specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Payments</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  paymentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Payment</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>paymentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByIds(org.osid.id.IdList paymentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();

        try (org.osid.id.IdList ids = paymentIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code>PaymentList</code> corresponding to the given
     *  payment genus <code>Type</code> which does not include
     *  payments of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  paymentGenusType a payment genus type 
     *  @return the returned <code>Payment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>paymentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByGenusType(org.osid.type.Type paymentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchGenusType(paymentGenusType, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code>PaymentList</code> corresponding to the given
     *  payment genus <code>Type</code> and include any additional
     *  payments with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  paymentGenusType a payment genus type 
     *  @return the returned <code>Payment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>paymentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByParentGenusType(org.osid.type.Type paymentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchParentGenusType(paymentGenusType, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code>PaymentList</code> containing the given
     *  payment record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  paymentRecordType a payment record type 
     *  @return the returned <code>Payment</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>paymentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByRecordType(org.osid.type.Type paymentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchRecordType(paymentRecordType, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code> PaymentList </code> in the given period. In
     *  plenary mode, the returned list contains all known payments or
     *  an error results. Otherwise, the returned list may contain
     *  only those payments that are accessible through this session.
     *
     *  @param  periodId a period <code> Id </code> 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.NullArgumentException <code> periodId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriod(org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchPeriodId(periodId, true);
        return (this.session.getPaymentsByQuery(query));
    }

    
    /**
     *  Gets a <code> PaymentList </code> for the given payer. In
     *  plenary mode, the returned list contains all known payments or
     *  an error results. Otherwise, the returned list may contain
     *  only those payments that are accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code> 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.NullArgumentException <code> payerId </code>
     *          is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayer(org.osid.id.Id payerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchPayerId(payerId, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given payer in a
     *  billing period. In plenary mode, the returned list contains
     *  all known payments or an error results. Otherwise, the
     *  returned list may contain only those payments that are
     *  accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code> 
     *  @param  periodId a period <code> Id </code> 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.NullArgumentException <code> payerId </code>
     *          or <code> periodId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriodForPayer(org.osid.id.Id payerId, 
                                                                            org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchPeriodId(periodId, true);
        query.matchPayerId(payerId, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given payer made
     *  within the given date range inclusive. In plenary mode, the
     *  returned list contains all known payments or an error
     *  results. Otherwise, the returned list may contain only those
     *  payments that are accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> payerId </code>
     *          or <code> periodId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayerOnDate(org.osid.id.Id payerId, 
                                                                          org.osid.calendaring.DateTime from, 
                                                                          org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchPayerId(payerId, true);
        query.matchPaymentDate(from, to, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given customer. In
     *  plenary mode, the returned list contains all known payments or
     *  an error results. Otherwise, the returned list may contain
     *  only those payments that are accessible through this session.
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.NullArgumentException <code> customerId
     *          </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForCustomer(org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchCustomerId(customerId, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given customer and
     *  billing period. In plenary mode, the returned list contains
     *  all known payments or an error results. Otherwise, the
     *  returned list may contain only those payments that are
     *  accessible through this session.
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  periodId a period <code> Id </code> 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.NullArgumentException <code> customerId
     *          </code> or <code> periodId </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriodForCustomer(org.osid.id.Id customerId, 
                                                                               org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchCustomerId(customerId, true);
        query.matchPeriodId(periodId, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given customer made
     *  within the given date range inclusive. In plenary mode, the
     *  returned list contains all known payments or an error
     *  results. Otherwise, the returned list may contain only those
     *  payments that are accessible through this session.
     *
     *  @param  customerId a customer <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> customerId
     *          </code> or <code> periodId </code> is <code> null
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForCustomerOnDate(org.osid.id.Id customerId, 
                                                                             org.osid.calendaring.DateTime from, 
                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchCustomerId(customerId, true);
        query.matchPaymentDate(from, to, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given payer and
     *  customer. In plenary mode, the returned list contains all
     *  known payments or an error results. Otherwise, the returned
     *  list may contain only those payments that are accessible
     *  through this session.
     *
     *  @param  payerId a payer <code> Id </code> 
     *  @param  customerId a customerId <code> Id </code> 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.NullArgumentException <code> payerId </code>
     *          or <code> customerId </code> is <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayerAndCustomer(org.osid.id.Id payerId, 
                                                                               org.osid.id.Id customerId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchPayerId(payerId, true);
        query.matchCustomerId(customerId, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets a <code> PaymentList </code> for the given payer and
     *  customer in a billing period. In plenary mode, the returned
     *  list contains all known payments or an error
     *  results. Otherwise, the returned list may contain only those
     *  payments that are accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code> 
     *  @param  customerId a customerId <code> Id </code> 
     *  @param  periodId a period <code> Id </code> 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.NullArgumentException <code> payerId,
     *          customerId, </code> or <code> periodId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsByPeriodForPayerAndCustomer(org.osid.id.Id payerId, 
                                                                                       org.osid.id.Id customerId, 
                                                                                       org.osid.id.Id periodId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchPayerId(payerId, true);
        query.matchCustomerId(customerId, true);
        query.matchPeriodId(periodId, true);
        return (this.session.getPaymentsByQuery(query));
    }

    
    /**
     *  Gets a <code> PaymentList </code> for the given customer and
     *  payer made within the given date range inclusive. In plenary
     *  mode, the returned list contains all known payments or an
     *  error results.  Otherwise, the returned list may contain only
     *  those payments that are accessible through this session.
     *
     *  @param  payerId a payer <code> Id </code> 
     *  @param  customerId a customerId <code> Id </code> 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Payment </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code>
     *          is greater than <code> to </code>
     *  @throws org.osid.NullArgumentException <code> payerId,
     *          customerId, </code> or <code> periodId </code> is
     *          <code> null </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPaymentsForPayerAndCustomerOnDate(org.osid.id.Id payerId, 
                                                                                     org.osid.id.Id customerId, 
                                                                                     org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchPayerId(payerId, true);
        query.matchCustomerId(customerId, true);
        query.matchPaymentDate(from, to, true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets all <code>Payments</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  payments or an error results. Otherwise, the returned list
     *  may contain only those payments that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Payments</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.billing.payment.PaymentList getPayments()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.billing.payment.PaymentQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getPaymentsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.billing.payment.PaymentQuery getQuery() {
        org.osid.billing.payment.PaymentQuery query = this.session.getPaymentQuery();
        return (query);
    }
}
