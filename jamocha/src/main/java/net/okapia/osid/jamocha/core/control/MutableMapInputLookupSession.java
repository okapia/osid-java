//
// MutableMapInputLookupSession
//
//    Implements an Input lookup service backed by a collection of
//    inputs that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control;


/**
 *  Implements an Input lookup service backed by a collection of
 *  inputs. The inputs are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of inputs can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapInputLookupSession
    extends net.okapia.osid.jamocha.core.control.spi.AbstractMapInputLookupSession
    implements org.osid.control.InputLookupSession {


    /**
     *  Constructs a new {@code MutableMapInputLookupSession}
     *  with no inputs.
     *
     *  @param system the system
     *  @throws org.osid.NullArgumentException {@code system} is
     *          {@code null}
     */

      public MutableMapInputLookupSession(org.osid.control.System system) {
        setSystem(system);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInputLookupSession} with a
     *  single input.
     *
     *  @param system the system  
     *  @param input an input
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code input} is {@code null}
     */

    public MutableMapInputLookupSession(org.osid.control.System system,
                                           org.osid.control.Input input) {
        this(system);
        putInput(input);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInputLookupSession}
     *  using an array of inputs.
     *
     *  @param system the system
     *  @param inputs an array of inputs
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code inputs} is {@code null}
     */

    public MutableMapInputLookupSession(org.osid.control.System system,
                                           org.osid.control.Input[] inputs) {
        this(system);
        putInputs(inputs);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapInputLookupSession}
     *  using a collection of inputs.
     *
     *  @param system the system
     *  @param inputs a collection of inputs
     *  @throws org.osid.NullArgumentException {@code system} or
     *          {@code inputs} is {@code null}
     */

    public MutableMapInputLookupSession(org.osid.control.System system,
                                           java.util.Collection<? extends org.osid.control.Input> inputs) {

        this(system);
        putInputs(inputs);
        return;
    }

    
    /**
     *  Makes an {@code Input} available in this session.
     *
     *  @param input an input
     *  @throws org.osid.NullArgumentException {@code input{@code  is
     *          {@code null}
     */

    @Override
    public void putInput(org.osid.control.Input input) {
        super.putInput(input);
        return;
    }


    /**
     *  Makes an array of inputs available in this session.
     *
     *  @param inputs an array of inputs
     *  @throws org.osid.NullArgumentException {@code inputs{@code 
     *          is {@code null}
     */

    @Override
    public void putInputs(org.osid.control.Input[] inputs) {
        super.putInputs(inputs);
        return;
    }


    /**
     *  Makes collection of inputs available in this session.
     *
     *  @param inputs a collection of inputs
     *  @throws org.osid.NullArgumentException {@code inputs{@code  is
     *          {@code null}
     */

    @Override
    public void putInputs(java.util.Collection<? extends org.osid.control.Input> inputs) {
        super.putInputs(inputs);
        return;
    }


    /**
     *  Removes an Input from this session.
     *
     *  @param inputId the {@code Id} of the input
     *  @throws org.osid.NullArgumentException {@code inputId{@code 
     *          is {@code null}
     */

    @Override
    public void removeInput(org.osid.id.Id inputId) {
        super.removeInput(inputId);
        return;
    }    
}
