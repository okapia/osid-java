//
// AbstractQueueProcessorEnablerList
//
//     Implements a filter for a QueueProcessorEnablerList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.provisioning.rules.queueprocessorenabler.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a QueueProcessorEnablerList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedQueueProcessorEnablerList
 *  to improve performance.
 */

public abstract class AbstractQueueProcessorEnablerFilterList
    extends net.okapia.osid.jamocha.provisioning.rules.queueprocessorenabler.spi.AbstractQueueProcessorEnablerList
    implements org.osid.provisioning.rules.QueueProcessorEnablerList,
               net.okapia.osid.jamocha.inline.filter.provisioning.rules.queueprocessorenabler.QueueProcessorEnablerFilter {

    private org.osid.provisioning.rules.QueueProcessorEnabler queueProcessorEnabler;
    private final org.osid.provisioning.rules.QueueProcessorEnablerList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractQueueProcessorEnablerFilterList</code>.
     *
     *  @param queueProcessorEnablerList a <code>QueueProcessorEnablerList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorEnablerList</code> is <code>null</code>
     */

    protected AbstractQueueProcessorEnablerFilterList(org.osid.provisioning.rules.QueueProcessorEnablerList queueProcessorEnablerList) {
        nullarg(queueProcessorEnablerList, "queue processor enabler list");
        this.list = queueProcessorEnablerList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.queueProcessorEnabler == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> QueueProcessorEnabler </code> in this list. 
     *
     *  @return the next <code> QueueProcessorEnabler </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> QueueProcessorEnabler </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorEnabler getNextQueueProcessorEnabler()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.provisioning.rules.QueueProcessorEnabler queueProcessorEnabler = this.queueProcessorEnabler;
            this.queueProcessorEnabler = null;
            return (queueProcessorEnabler);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in queue processor enabler list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.queueProcessorEnabler = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters QueueProcessorEnablers.
     *
     *  @param queueProcessorEnabler the queue processor enabler to filter
     *  @return <code>true</code> if the queue processor enabler passes the filter,
     *          <code>false</code> if the queue processor enabler should be filtered
     */

    public abstract boolean pass(org.osid.provisioning.rules.QueueProcessorEnabler queueProcessorEnabler);


    protected void prime() {
        if (this.queueProcessorEnabler != null) {
            return;
        }

        org.osid.provisioning.rules.QueueProcessorEnabler queueProcessorEnabler = null;

        while (this.list.hasNext()) {
            try {
                queueProcessorEnabler = this.list.getNextQueueProcessorEnabler();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(queueProcessorEnabler)) {
                this.queueProcessorEnabler = queueProcessorEnabler;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
