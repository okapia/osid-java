//
// AbstractBuildingQuery.java
//
//     A template for making a Building Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.building.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for buildings.
 */

public abstract class AbstractBuildingQuery    
    extends net.okapia.osid.jamocha.spi.AbstractTemporalOsidObjectQuery
    implements org.osid.room.BuildingQuery {

    private final java.util.Collection<org.osid.room.records.BuildingQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Sets an address <code> Id. </code> 
     *
     *  @param  addressId an address <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAddressId(org.osid.id.Id addressId, boolean match) {
        return;
    }


    /**
     *  Clears the address <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearAddressIdTerms() {
        return;
    }


    /**
     *  Tests if an <code> AddressQuery </code> is available. 
     *
     *  @return <code> true </code> if an address query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAddressQuery() {
        return (false);
    }


    /**
     *  Gets the query for an address query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the address query 
     *  @throws org.osid.UnimplementedException <code> supportsAddressQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.contact.AddressQuery getAddressQuery() {
        throw new org.osid.UnimplementedException("supportsAddressQuery() is false");
    }


    /**
     *  Matches any address. 
     *
     *  @param  match <code> true </code> to match buildings with any address, 
     *          <code> false </code> to match buildings with no address 
     */

    @OSID @Override
    public void matchAnyAddress(boolean match) {
        return;
    }


    /**
     *  Clears the address terms. 
     */

    @OSID @Override
    public void clearAddressTerms() {
        return;
    }


    /**
     *  Sets a name. 
     *
     *  @param  name an official name 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> name </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> name </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchOfficialName(String name, 
                                  org.osid.type.Type stringMatchType, 
                                  boolean match) {
        return;
    }


    /**
     *  Matches any official name. 
     *
     *  @param  match <code> true </code> to match buildings with any official 
     *          name, <code> false </code> to match buildings with no official 
     *          name 
     */

    @OSID @Override
    public void matchAnyOfficialName(boolean match) {
        return;
    }


    /**
     *  Clears the official name terms. 
     */

    @OSID @Override
    public void clearOfficialNameTerms() {
        return;
    }


    /**
     *  Sets a bulding number. 
     *
     *  @param  number a number 
     *  @param  stringMatchType a string match type 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> number </code> is not 
     *          of <code> stringMatchType </code> 
     *  @throws org.osid.NullArgumentException <code> number </code> is <code> 
     *          null </code> 
     *  @throws org.osid.UnsupportedException <code> 
     *          supportsStringMatchType(stringMatchType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public void matchNumber(String number, org.osid.type.Type stringMatchType, 
                            boolean match) {
        return;
    }


    /**
     *  Matches any building number. 
     *
     *  @param  match <code> true </code> to match buildings with any number, 
     *          <code> false </code> to match buildings with no number 
     */

    @OSID @Override
    public void matchAnyNumber(boolean match) {
        return;
    }


    /**
     *  Clears the building number terms. 
     */

    @OSID @Override
    public void clearNumberTerms() {
        return;
    }


    /**
     *  Sets a building <code> Id. </code> 
     *
     *  @param  buildingId a building <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> addressId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchEnclosingBuildingId(org.osid.id.Id buildingId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the building <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearEnclosingBuildingIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> BuildingQuery </code> is available. 
     *
     *  @return <code> true </code> if a building query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnclosingBuildingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the building query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnclosingBuildingQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.room.BuildingQuery getEnclosingBuildingQuery() {
        throw new org.osid.UnimplementedException("supportsEnclosingBuildingQuery() is false");
    }


    /**
     *  Matches any building. 
     *
     *  @param  match <code> true </code> to match buildings with any 
     *          enclosing building, <code> false </code> to match buildings 
     *          with no enclosing building 
     */

    @OSID @Override
    public void matchAnyEnclosingBuilding(boolean match) {
        return;
    }


    /**
     *  Clears the enclosing building terms. 
     */

    @OSID @Override
    public void clearEnclosingBuildingTerms() {
        return;
    }


    /**
     *  Matches an area within the given range inclusive. 
     *
     *  @param  low start of range 
     *  @param  high end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> low </code> is 
     *          greater than <code> high </code> 
     */

    @OSID @Override
    public void matchGrossArea(java.math.BigDecimal low, 
                               java.math.BigDecimal high, boolean match) {
        return;
    }


    /**
     *  Matches any area. 
     *
     *  @param  match <code> true </code> to match buildings with any area, 
     *          <code> false </code> to match buildings with no area assigned 
     */

    @OSID @Override
    public void matchAnyGrossArea(boolean match) {
        return;
    }


    /**
     *  Clears the area terms. 
     */

    @OSID @Override
    public void clearGrossAreaTerms() {
        return;
    }


    /**
     *  Sets the room <code> Id </code> for this query to match rooms assigned 
     *  to buildings. 
     *
     *  @param  roomId a room <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> roomId </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchRoomId(org.osid.id.Id roomId, boolean match) {
        return;
    }


    /**
     *  Clears the room <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearRoomIdTerms() {
        return;
    }


    /**
     *  Tests if a room query is available. 
     *
     *  @return <code> true </code> if a room query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRoomQuery() {
        return (false);
    }


    /**
     *  Gets the query for a building. 
     *
     *  @return the room query 
     *  @throws org.osid.UnimplementedException <code> supportsRoomQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.RoomQuery getRoomQuery() {
        throw new org.osid.UnimplementedException("supportsRoomQuery() is false");
    }


    /**
     *  Matches buildings with any room. 
     *
     *  @param  match <code> true </code> to match buildings with any room, 
     *          <code> false </code> to match buildings with no rooms 
     */

    @OSID @Override
    public void matchAnyRoom(boolean match) {
        return;
    }


    /**
     *  Clears the room terms. 
     */

    @OSID @Override
    public void clearRoomTerms() {
        return;
    }


    /**
     *  Sets the building <code> Id </code> for this query to match rooms 
     *  assigned to campuses. 
     *
     *  @param  campusId a campus <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> campusId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCampusId(org.osid.id.Id campusId, boolean match) {
        return;
    }


    /**
     *  Clears the campus <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCampusIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CampusQuery </code> is available. 
     *
     *  @return <code> true </code> if a campus query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCampusQuery() {
        return (false);
    }


    /**
     *  Gets the query for a campus query. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the campus query 
     *  @throws org.osid.UnimplementedException <code> supportsCampusQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.room.CampusQuery getCampusQuery() {
        throw new org.osid.UnimplementedException("supportsCampusQuery() is false");
    }


    /**
     *  Clears the campus terms. 
     */

    @OSID @Override
    public void clearCampusTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given building query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a building implementing the requested record.
     *
     *  @param buildingRecordType a building record type
     *  @return the building query record
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(buildingRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.room.records.BuildingQueryRecord getBuildingQueryRecord(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.room.records.BuildingQueryRecord record : this.records) {
            if (record.implementsRecordType(buildingRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(buildingRecordType + " is not supported");
    }


    /**
     *  Adds a record to this building query. 
     *
     *  @param buildingQueryRecord building query record
     *  @param buildingRecordType building record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBuildingQueryRecord(org.osid.room.records.BuildingQueryRecord buildingQueryRecord, 
                                          org.osid.type.Type buildingRecordType) {

        addRecordType(buildingRecordType);
        nullarg(buildingQueryRecord, "building query record");
        this.records.add(buildingQueryRecord);        
        return;
    }
}
