//
// AbstractActivityRegistrationQueryInspector.java
//
//     A template for making an ActivityRegistrationQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.registration.activityregistration.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for activity registrations.
 */

public abstract class AbstractActivityRegistrationQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationshipQueryInspector
    implements org.osid.course.registration.ActivityRegistrationQueryInspector {

    private final java.util.Collection<org.osid.course.registration.records.ActivityRegistrationQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the registration <code> Id </code> query terms. 
     *
     *  @return the registration <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRegistrationIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the registration query terms. 
     *
     *  @return the registration query terms 
     */

    @OSID @Override
    public org.osid.course.registration.RegistrationQueryInspector[] getRegistrationTerms() {
        return (new org.osid.course.registration.RegistrationQueryInspector[0]);
    }


    /**
     *  Gets the activity <code> Id </code> query terms. 
     *
     *  @return the activity <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getActivityIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the activity query terms. 
     *
     *  @return the activity query terms 
     */

    @OSID @Override
    public org.osid.course.ActivityQueryInspector[] getActivityTerms() {
        return (new org.osid.course.ActivityQueryInspector[0]);
    }


    /**
     *  Gets the student <code> Id </code> query terms. 
     *
     *  @return the student <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStudentIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the student query terms. 
     *
     *  @return the student query terms 
     */

    @OSID @Override
    public org.osid.resource.ResourceQueryInspector[] getStudentTerms() {
        return (new org.osid.resource.ResourceQueryInspector[0]);
    }


    /**
     *  Gets the course catalog <code> Id </code> query terms. 
     *
     *  @return the course catalog <code> Id </code> query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCourseCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the course catalog query terms. 
     *
     *  @return the course catalog query terms 
     */

    @OSID @Override
    public org.osid.course.CourseCatalogQueryInspector[] getCourseCatalogTerms() {
        return (new org.osid.course.CourseCatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given activity registration query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve an activity registration implementing the requested record.
     *
     *  @param activityRegistrationRecordType an activity registration record type
     *  @return the activity registration query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>activityRegistrationRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(activityRegistrationRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.course.registration.records.ActivityRegistrationQueryInspectorRecord getActivityRegistrationQueryInspectorRecord(org.osid.type.Type activityRegistrationRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.course.registration.records.ActivityRegistrationQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(activityRegistrationRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(activityRegistrationRecordType + " is not supported");
    }


    /**
     *  Adds a record to this activity registration query. 
     *
     *  @param activityRegistrationQueryInspectorRecord activity registration query inspector
     *         record
     *  @param activityRegistrationRecordType activityRegistration record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addActivityRegistrationQueryInspectorRecord(org.osid.course.registration.records.ActivityRegistrationQueryInspectorRecord activityRegistrationQueryInspectorRecord, 
                                                   org.osid.type.Type activityRegistrationRecordType) {

        addRecordType(activityRegistrationRecordType);
        nullarg(activityRegistrationRecordType, "activity registration record type");
        this.records.add(activityRegistrationQueryInspectorRecord);        
        return;
    }
}
