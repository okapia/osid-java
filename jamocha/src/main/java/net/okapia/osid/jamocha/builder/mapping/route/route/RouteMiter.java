//
// RouteMiter.java
//
//     Defines a Route miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.mapping.route.route;


/**
 *  Defines a <code>Route</code> miter for use with the builders.
 */

public interface RouteMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRelationshipMiter,
            org.osid.mapping.route.Route {


    /**
     *  Sets the starting location.
     *
     *  @param location a starting location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public void setStartingLocation(org.osid.mapping.Location location);


    /**
     *  Sets the ending location.
     *
     *  @param location an ending location
     *  @throws org.osid.NullArgumentException <code>location</code>
     *          is <code>null</code>
     */

    public void setEndingLocation(org.osid.mapping.Location location);


    /**
     *  Sets the distance.
     *
     *  @param distance a distance
     *  @throws org.osid.NullArgumentException <code>distance</code>
     *          is <code>null</code>
     */

    public void setDistance(org.osid.mapping.Distance distance);


    /**
     *  Sets the ETA.
     *
     *  @param eta the estimated time of arrival
     *  @throws org.osid.NullArgumentException <code>eta</code> is
     *          <code>null</code>
     */

    public void setETA(org.osid.calendaring.Duration eta);


    /**
     *  Adds a segment.
     *
     *  @param segment a segment
     *  @throws org.osid.NullArgumentException <code>segment</code> is
     *          <code>null</code>
     */

    public void addSegment(org.osid.mapping.route.RouteSegment segment);


    /**
     *  Sets all the segments.
     *
     *  @param segments a collection of segments
     *  @throws org.osid.NullArgumentException <code>segments</code>
     *          is <code>null</code>
     */

    public void setSegments(java.util.Collection<org.osid.mapping.route.RouteSegment> segments);


    /**
     *  Adds a Route record.
     *
     *  @param record a route record
     *  @param recordType the type of route record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addRouteRecord(org.osid.mapping.route.records.RouteRecord record, org.osid.type.Type recordType);
}       


