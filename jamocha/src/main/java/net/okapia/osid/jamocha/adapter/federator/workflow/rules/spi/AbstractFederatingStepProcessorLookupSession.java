//
// AbstractFederatingStepProcessorLookupSession.java
//
//     An abstract federating adapter for a StepProcessorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  StepProcessorLookupSession. Sessions are added to this session
 *  through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingStepProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.workflow.rules.StepProcessorLookupSession>
    implements org.osid.workflow.rules.StepProcessorLookupSession {

    private boolean parallel = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();


    /**
     *  Constructs a new
     *  <code>AbstractFederatingStepProcessorLookupSession</code>.
     */

    protected AbstractFederatingStepProcessorLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.workflow.rules.StepProcessorLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Office/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>StepProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStepProcessors() {
        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            if (session.canLookupStepProcessors()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>StepProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepProcessorView() {
        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            session.useComparativeStepProcessorView();
        }

        return;
    }


    /**
     *  A complete view of the <code>StepProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepProcessorView() {
        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            session.usePlenaryStepProcessorView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step processors in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            session.useFederatedOfficeView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            session.useIsolatedOfficeView();
        }

        return;
    }


    /**
     *  Only active step processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepProcessorView() {
        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            session.useActiveStepProcessorView();
        }

        return;
    }


    /**
     *  Active and inactive step processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepProcessorView() {
        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            session.useAnyStatusStepProcessorView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>StepProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>StepProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>StepProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @param  stepProcessorId <code>Id</code> of the
     *          <code>StepProcessor</code>
     *  @return the step processor
     *  @throws org.osid.NotFoundException <code>stepProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stepProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessor getStepProcessor(org.osid.id.Id stepProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            try {
                return (session.getStepProcessor(stepProcessorId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(stepProcessorId + " not found");
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the
     *  given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  stepProcessors specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>StepProcessors</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, step processors are returned that are
     *  currently active. In any status mode, active and inactive step
     *  processors are returned.
     *
     *  @param  stepProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByIds(org.osid.id.IdList stepProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.workflow.rules.stepprocessor.MutableStepProcessorList ret = new net.okapia.osid.jamocha.workflow.rules.stepprocessor.MutableStepProcessorList();

        try (org.osid.id.IdList ids = stepProcessorIds) {
            while (ids.hasNext()) {
                ret.addStepProcessor(getStepProcessor(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the
     *  given step processor genus <code>Type</code> which does not
     *  include step processors of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known step
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step processors are returned that are
     *  currently active. In any status mode, active and inactive step
     *  processors are returned.
     *
     *  @param  stepProcessorGenusType a stepProcessor genus type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByGenusType(org.osid.type.Type stepProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepprocessor.FederatingStepProcessorList ret = getStepProcessorList();

        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            ret.addStepProcessorList(session.getStepProcessorsByGenusType(stepProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>StepProcessorList</code> corresponding to the
     *  given step processor genus <code>Type</code> and include any
     *  additional step processors with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known step
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step processors are returned that are
     *  currently active. In any status mode, active and inactive step
     *  processors are returned.
     *
     *  @param  stepProcessorGenusType a stepProcessor genus type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByParentGenusType(org.osid.type.Type stepProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepprocessor.FederatingStepProcessorList ret = getStepProcessorList();

        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            ret.addStepProcessorList(session.getStepProcessorsByParentGenusType(stepProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>StepProcessorList</code> containing the given
     *  step processor record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known step
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, step processors are returned that are
     *  currently active. In any status mode, active and inactive step
     *  processors are returned.
     *
     *  @param  stepProcessorRecordType a stepProcessor record type 
     *  @return the returned <code>StepProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessorsByRecordType(org.osid.type.Type stepProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepprocessor.FederatingStepProcessorList ret = getStepProcessorList();

        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            ret.addStepProcessorList(session.getStepProcessorsByRecordType(stepProcessorRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>StepProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  step processors or an error results. Otherwise, the returned list
     *  may contain only those step processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step processors are returned that are currently
     *  active. In any status mode, active and inactive step processors
     *  are returned.
     *
     *  @return a list of <code>StepProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepProcessorList getStepProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepprocessor.FederatingStepProcessorList ret = getStepProcessorList();

        for (org.osid.workflow.rules.StepProcessorLookupSession session : getSessions()) {
            ret.addStepProcessorList(session.getStepProcessors());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepprocessor.FederatingStepProcessorList getStepProcessorList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepprocessor.ParallelStepProcessorList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.workflow.rules.stepprocessor.CompositeStepProcessorList());
        }
    }
}
