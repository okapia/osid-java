//
// TermMiter.java
//
//     Defines a Term miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.course.term;


/**
 *  Defines a <code>Term</code> miter for use with the builders.
 */

public interface TermMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidObjectMiter,
            org.osid.course.Term {


    /**
     *  Sets the display label.
     *
     *  @param label a display label
     *  @throws org.osid.NullArgumentException <code>label</code> is
     *          <code>null</code>
     */

    public void setDisplayLabel(org.osid.locale.DisplayText label);


    /**
     *  Sets the open date.
     *
     *  @param date an open date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setOpenDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the registration start.
     *
     *  @param date a registration start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setRegistrationStart(org.osid.calendaring.DateTime date);


    /**
     *  Sets the registration end.
     *
     *  @param date a registration end
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setRegistrationEnd(org.osid.calendaring.DateTime date);


    /**
     *  Sets the classes start.
     *
     *  @param date a classes start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setClassesStart(org.osid.calendaring.DateTime date);


    /**
     *  Sets the classes end.
     *
     *  @param date a classes end
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setClassesEnd(org.osid.calendaring.DateTime date);


    /**
     *  Sets the add date.
     *
     *  @param date the add date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setAddDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the drop date.
     *
     *  @param date the drop date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setDropDate(org.osid.calendaring.DateTime date);


    /**
     *  Sets the final exam start.
     *
     *  @param date a final exam start
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setFinalExamStart(org.osid.calendaring.DateTime date);


    /**
     *  Sets the final exam end.
     *
     *  @param date a final exam end
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setFinalExamEnd(org.osid.calendaring.DateTime date);


    /**
     *  Sets the close date.
     *
     *  @param date a close date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */

    public void setCloseDate(org.osid.calendaring.DateTime date);


    /**
     *  Adds a Term record.
     *
     *  @param record a term record
     *  @param recordType the type of term record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addTermRecord(org.osid.course.records.TermRecord record, org.osid.type.Type recordType);
}       


