//
// AbstractBrokerProcessorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.brokerprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBrokerProcessorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.rules.BrokerProcessorSearchResults {

    private org.osid.provisioning.rules.BrokerProcessorList brokerProcessors;
    private final org.osid.provisioning.rules.BrokerProcessorQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.rules.records.BrokerProcessorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBrokerProcessorSearchResults.
     *
     *  @param brokerProcessors the result set
     *  @param brokerProcessorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>brokerProcessors</code>
     *          or <code>brokerProcessorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBrokerProcessorSearchResults(org.osid.provisioning.rules.BrokerProcessorList brokerProcessors,
                                            org.osid.provisioning.rules.BrokerProcessorQueryInspector brokerProcessorQueryInspector) {
        nullarg(brokerProcessors, "broker processors");
        nullarg(brokerProcessorQueryInspector, "broker processor query inspectpr");

        this.brokerProcessors = brokerProcessors;
        this.inspector = brokerProcessorQueryInspector;

        return;
    }


    /**
     *  Gets the broker processor list resulting from a search.
     *
     *  @return a broker processor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerProcessorList getBrokerProcessors() {
        if (this.brokerProcessors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.rules.BrokerProcessorList brokerProcessors = this.brokerProcessors;
        this.brokerProcessors = null;
	return (brokerProcessors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.rules.BrokerProcessorQueryInspector getBrokerProcessorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  broker processor search record <code> Type. </code> This method must
     *  be used to retrieve a brokerProcessor implementing the requested
     *  record.
     *
     *  @param brokerProcessorSearchRecordType a brokerProcessor search 
     *         record type 
     *  @return the broker processor search
     *  @throws org.osid.NullArgumentException
     *          <code>brokerProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(brokerProcessorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.BrokerProcessorSearchResultsRecord getBrokerProcessorSearchResultsRecord(org.osid.type.Type brokerProcessorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.rules.records.BrokerProcessorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(brokerProcessorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(brokerProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record broker processor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBrokerProcessorRecord(org.osid.provisioning.rules.records.BrokerProcessorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "broker processor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
