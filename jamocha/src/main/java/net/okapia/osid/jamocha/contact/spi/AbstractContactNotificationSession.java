//
// AbstractContactNotificationSession.java
//
//     A template for making ContactNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.contact.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Contact} objects. This session is intended
 *  for consumers needing to synchronize their state with this service
 *  without the use of polling. Notifications are cancelled when this
 *  session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Contact} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for contact entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractContactNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.contact.ContactNotificationSession {

    private boolean federated = false;
    private org.osid.contact.AddressBook addressBook = new net.okapia.osid.jamocha.nil.contact.addressbook.UnknownAddressBook();


    /**
     *  Gets the {@code AddressBook/code> {@code Id} associated with
     *  this session.
     *
     *  @return the {@code AddressBook Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getAddressBookId() {
        return (this.addressBook.getId());
    }

    
    /**
     *  Gets the {@code AddressBook} associated with this session.
     *
     *  @return the {@code AddressBook} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.contact.AddressBook getAddressBook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.addressBook);
    }


    /**
     *  Sets the {@code AddressBook}.
     *
     *  @param addressBook the address book for this session
     *  @throws org.osid.NullArgumentException {@code addressBook}
     *          is {@code null}
     */

    protected void setAddressBook(org.osid.contact.AddressBook addressBook) {
        nullarg(addressBook, "address book");
        this.addressBook = addressBook;
        return;
    }


    /**
     *  Tests if this user can register for {@code Contact}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a {@code
     *  PERMISSION_DENIED}. This is intended as a hint to an
     *  application that may opt not to offer notification operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForContactNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeContactNotification() </code>.
     */

    @OSID @Override
    public void reliableContactNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableContactNotifications() {
        return;
    }


    /**
     *  Acknowledge a contact notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeContactNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for contacts in address books
     *  which are children of this address book in the address book
     *  hierarchy.
     */

    @OSID @Override
    public void useFederatedAddressBookView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this address book only.
     */

    @OSID @Override
    public void useIsolatedAddressBookView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new contacts. {@code
     *  ContactReceiver.newContact()} is invoked when a new {@code
     *  Contact} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewContacts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new contacts for the given genus
     *  {@code Type}. {@code ContactReceiver.newContact()} is invoked
     *  when a new {@code Contact} is created.
     *
     *  @param  contactGenusType the contact genus type to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *          contactGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewContactsByGenusType(org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new contacts for the given
     *  reference {@code Id}. {@code ContactReceiver.newContact()} is
     *  invoked when a new {@code Contact} is created.
     *
     *  @param  referenceId the {@code Id} of the reference to monitor
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewContactsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new contacts for the given
     *  address {@code Id}. {@code ContactReceiver.newContact()} is
     *  invoked when a new {@code Contact} is created.
     *
     *  @param  addressId the {@code Id} of the address to monitor
     *  @throws org.osid.NullArgumentException {@code addressId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewContactsForAddress(org.osid.id.Id addressId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated contacts. {@code
     *  ContactReceiver.changedContact()} is invoked when a contact is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedContacts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of changed contacts for the given
     *  contact genus {@code Type.} {@code
     *  ContactReceiver.changedContact()} is invoked when a {@code
     *  Contact} for the genus type is changed.
     *
     *  @param  contactGenusType the contact genus type to monitor 
     *  @throws org.osid.NullArgumentException {@code contactGenusType} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedContactsByGenusType(org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated contacts for the given
     *  reference {@code Id}. {@code ContactReceiver.changedContact()}
     *  is invoked when a {@code Contact} in this addressBook is
     *  changed.
     *
     *  @param  referenceId the {@code Id} of the reference to monitor
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedContactsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated contacts for the given
     *  address {@code Id}. {@code ContactReceiver.changedContact()}
     *  is invoked when a {@code Contact} in this addressBook is
     *  changed.
     *
     *  @param  addressId the {@code Id} of the address to monitor
     *  @throws org.osid.NullArgumentException {@code addressId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedContactsForAddress(org.osid.id.Id addressId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated contact. {@code
     *  ContactReceiver.changedContact()} is invoked when the
     *  specified contact is changed.
     *
     *  @param contactId the {@code Id} of the {@code Contact} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code contactId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedContact(org.osid.id.Id contactId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted contacts. {@code
     *  ContactReceiver.deletedContact()} is invoked when a contact is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedContacts()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted contacts for the given
     *  contact genus {@code Type.} {@code
     *  ContactReceiver.deletedContact()} is invoked when a {@code
     *  Contact} for the genus type is deleted.
     *
     *  @param  contactGenusType the contact genus type to monitor 
     *  @throws org.osid.NullArgumentException {@code
     *          contactGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedContactsByGenusType(org.osid.type.Type contactGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted contacts for the given
     *  reference {@code Id}. {@code ContactReceiver.deletedContact()}
     *  is invoked when a {@code Contact} is deleted or removed from
     *  this addressBook.
     *
     *  @param  referenceId the {@code Id} of the reference to monitor
     *  @throws org.osid.NullArgumentException {@code referenceId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedContactsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted contacts for the given
     *  address {@code Id}. {@code ContactReceiver.deletedContact()}
     *  is invoked when a {@code Contact} is deleted or removed from
     *  this addressBook.
     *
     *  @param  addressId the {@code Id} of the address to monitor
     *  @throws org.osid.NullArgumentException {@code addressId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedContactsForAddress(org.osid.id.Id addressId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted contact. {@code
     *  ContactReceiver.deletedContact()} is invoked when the
     *  specified contact is deleted.
     *
     *  @param contactId the {@code Id} of the
     *          {@code Contact} to monitor
     *  @throws org.osid.NullArgumentException {@code contactId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedContact(org.osid.id.Id contactId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
