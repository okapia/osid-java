//
// AbstractCompositionEnablerQuery.java
//
//     A template for making a CompositionEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.repository.rules.compositionenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for composition enablers.
 */

public abstract class AbstractCompositionEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.repository.rules.CompositionEnablerQuery {

    private final java.util.Collection<org.osid.repository.rules.records.CompositionEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the composition. 
     *
     *  @param  compositionId the composition <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> compositionId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRuledCompositionId(org.osid.id.Id compositionId, 
                                        boolean match) {
        return;
    }


    /**
     *  Clears the composition <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledCompositionIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CompositionQuery </code> is available. 
     *
     *  @return <code> true </code> if a composition query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledCompositionQuery() {
        return (false);
    }


    /**
     *  Gets the query for a composition. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the composition query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledCompositionQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.repository.CompositionQuery getRuledCompositionQuery() {
        throw new org.osid.UnimplementedException("supportsRuledCompositionQuery() is false");
    }


    /**
     *  Matches enablers mapped to any composition. 
     *
     *  @param  match <code> true </code> for enablers mapped to any 
     *          composition, <code> false </code> to match enablers mapped to 
     *          no composition 
     */

    @OSID @Override
    public void matchAnyRuledComposition(boolean match) {
        return;
    }


    /**
     *  Clears the composition query terms. 
     */

    @OSID @Override
    public void clearRuledCompositionTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the composition. 
     *
     *  @param  repositoryId the repository <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> repositoryId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRepositoryId(org.osid.id.Id repositoryId, boolean match) {
        return;
    }


    /**
     *  Clears the repository <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRepositoryIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RepositoryQuery </code> is available. 
     *
     *  @return <code> true </code> if a repository query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRepositoryQuery() {
        return (false);
    }


    /**
     *  Gets the query for a repository. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the repository query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRepositoryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.RepositoryQuery getRepositoryQuery() {
        throw new org.osid.UnimplementedException("supportsRepositoryQuery() is false");
    }


    /**
     *  Clears the repository query terms. 
     */

    @OSID @Override
    public void clearRepositoryTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given composition enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a composition enabler implementing the requested record.
     *
     *  @param compositionEnablerRecordType a composition enabler record type
     *  @return the composition enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>compositionEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(compositionEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.repository.rules.records.CompositionEnablerQueryRecord getCompositionEnablerQueryRecord(org.osid.type.Type compositionEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.repository.rules.records.CompositionEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(compositionEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(compositionEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this composition enabler query. 
     *
     *  @param compositionEnablerQueryRecord composition enabler query record
     *  @param compositionEnablerRecordType compositionEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCompositionEnablerQueryRecord(org.osid.repository.rules.records.CompositionEnablerQueryRecord compositionEnablerQueryRecord, 
                                          org.osid.type.Type compositionEnablerRecordType) {

        addRecordType(compositionEnablerRecordType);
        nullarg(compositionEnablerQueryRecord, "composition enabler query record");
        this.records.add(compositionEnablerQueryRecord);        
        return;
    }
}
