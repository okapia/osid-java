//
// LessonElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.plan.lesson.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class LessonElements
    extends net.okapia.osid.jamocha.spi.OsidRelationshipElements {


    /**
     *  Gets the LessonElement Id.
     *
     *  @return the lesson element Id
     */

    public static org.osid.id.Id getLessonEntityId() {
        return (makeEntityId("osid.course.plan.Lesson"));
    }


    /**
     *  Gets the PlanId element Id.
     *
     *  @return the PlanId element Id
     */

    public static org.osid.id.Id getPlanId() {
        return (makeElementId("osid.course.plan.lesson.PlanId"));
    }


    /**
     *  Gets the Plan element Id.
     *
     *  @return the Plan element Id
     */

    public static org.osid.id.Id getPlan() {
        return (makeElementId("osid.course.plan.lesson.Plan"));
    }


    /**
     *  Gets the DocetId element Id.
     *
     *  @return the DocetId element Id
     */

    public static org.osid.id.Id getDocetId() {
        return (makeElementId("osid.course.plan.lesson.DocetId"));
    }


    /**
     *  Gets the Docet element Id.
     *
     *  @return the Docet element Id
     */

    public static org.osid.id.Id getDocet() {
        return (makeElementId("osid.course.plan.lesson.Docet"));
    }


    /**
     *  Gets the ActivityIds element Id.
     *
     *  @return the ActivityIds element Id
     */

    public static org.osid.id.Id getActivityIds() {
        return (makeElementId("osid.course.plan.lesson.ActivityIds"));
    }


    /**
     *  Gets the Activities element Id.
     *
     *  @return the Activities element Id
     */

    public static org.osid.id.Id getActivities() {
        return (makeElementId("osid.course.plan.lesson.Activities"));
    }


    /**
     *  Gets the PlannedStartTime element Id.
     *
     *  @return the PlannedStartTime element Id
     */

    public static org.osid.id.Id getPlannedStartTime() {
        return (makeElementId("osid.course.plan.lesson.PlannedStartTime"));
    }


    /**
     *  Gets the ActualStartTime element Id.
     *
     *  @return the ActualStartTime element Id
     */

    public static org.osid.id.Id getActualStartTime() {
        return (makeElementId("osid.course.plan.lesson.ActualStartTime"));
    }


    /**
     *  Gets the ActualStartingActivityId element Id.
     *
     *  @return the ActualStartingActivityId element Id
     */

    public static org.osid.id.Id getActualStartingActivityId() {
        return (makeElementId("osid.course.plan.lesson.ActualStartingActivityId"));
    }


    /**
     *  Gets the ActualStartingActivity element Id.
     *
     *  @return the ActualStartingActivity element Id
     */

    public static org.osid.id.Id getActualStartingActivity() {
        return (makeElementId("osid.course.plan.lesson.ActualStartingActivity"));
    }


    /**
     *  Gets the ActualEndTime element Id.
     *
     *  @return the ActualEndTime element Id
     */

    public static org.osid.id.Id getActualEndTime() {
        return (makeElementId("osid.course.plan.lesson.ActualEndTime"));
    }


    /**
     *  Gets the ActualEndingActivityId element Id.
     *
     *  @return the ActualEndingActivityId element Id
     */

    public static org.osid.id.Id getActualEndingActivityId() {
        return (makeElementId("osid.course.plan.lesson.ActualEndingActivityId"));
    }


    /**
     *  Gets the ActualEndingActivity element Id.
     *
     *  @return the ActualEndingActivity element Id
     */

    public static org.osid.id.Id getActualEndingActivity() {
        return (makeElementId("osid.course.plan.lesson.ActualEndingActivity"));
    }


    /**
     *  Gets the ActualTimeSpent element Id.
     *
     *  @return the ActualTimeSpent element Id
     */

    public static org.osid.id.Id getActualTimeSpent() {
        return (makeElementId("osid.course.plan.lesson.ActualTimeSpent"));
    }


    /**
     *  Gets the Complete element Id.
     *
     *  @return the Complete element Id
     */

    public static org.osid.id.Id getComplete() {
        return (makeElementId("osid.course.plan.lesson.Complete"));
    }


    /**
     *  Gets the Skipped element Id.
     *
     *  @return the Skipped element Id
     */

    public static org.osid.id.Id getSkipped() {
        return (makeElementId("osid.course.plan.lesson.Skipped"));
    }


    /**
     *  Gets the CourseCatalogId element Id.
     *
     *  @return the CourseCatalogId element Id
     */

    public static org.osid.id.Id getCourseCatalogId() {
        return (makeQueryElementId("osid.course.plan.lesson.CourseCatalogId"));
    }


    /**
     *  Gets the CourseCatalog element Id.
     *
     *  @return the CourseCatalog element Id
     */

    public static org.osid.id.Id getCourseCatalog() {
        return (makeQueryElementId("osid.course.plan.lesson.CourseCatalog"));
    }
}
