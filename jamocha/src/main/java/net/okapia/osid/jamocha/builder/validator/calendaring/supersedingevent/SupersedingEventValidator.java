//
// SupersedingEventValidator.java
//
//     Validates a SupersedingEvent.
//
//
// Tom Coppeto
// Okapia
// 20 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.validator.calendaring.supersedingevent;


/**
 *  Validates a SupersedingEvent.
 */

public final class SupersedingEventValidator
    extends net.okapia.osid.jamocha.builder.validator.calendaring.supersedingevent.spi.AbstractSupersedingEventValidator {


    /**
     *  Constructs a new <code>SupersedingEventValidator</code>.
     */

    public SupersedingEventValidator() {
        return;
    }


    /**
     *  Constructs a new <code>SupersedingEventValidator</code>.
     *
     *  @param validation an EnumSet of validations
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          is <code>null</code>
     */

    public SupersedingEventValidator(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation) {
        super(validation);
        return;
    }

    
    /**
     *  Validates a SupersedingEvent with a default validation.
     *
     *  @param supersedingEvent a superseding event to validate
     *  @return the superseding event
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>supersedingEvent</code>
     *          is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.calendaring.SupersedingEvent validateSupersedingEvent(org.osid.calendaring.SupersedingEvent supersedingEvent) {
        SupersedingEventValidator validator = new SupersedingEventValidator();
        validator.validate(supersedingEvent);
        return (supersedingEvent);
    }


    /**
     *  Validates a SupersedingEvent for the given validations.
     *
     *  @param validation an EnumSet of validations
     *  @param supersedingEvent a superseding event to validate
     *  @return the superseding event
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not
     *          valid
     *  @throws org.osid.NullArgumentException <code>validation</code>
     *          or <code>supersedingEvent</code> is <code>null</code>
     *  @throws org.osid.NullReturnException a method returned
     *          <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred
     */

    public static org.osid.calendaring.SupersedingEvent validateSupersedingEvent(java.util.EnumSet<net.okapia.osid.jamocha.builder.validator.Validation> validation,
                                                       org.osid.calendaring.SupersedingEvent supersedingEvent) {

        SupersedingEventValidator validator = new SupersedingEventValidator(validation);
        validator.validate(supersedingEvent);
        return (supersedingEvent);
    }
}
