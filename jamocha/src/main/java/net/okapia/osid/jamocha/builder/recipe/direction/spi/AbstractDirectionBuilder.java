//
// AbstractDirection.java
//
//     Defines a Direction builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.recipe.direction.spi;


/**
 *  Defines a <code>Direction</code> builder.
 */

public abstract class AbstractDirectionBuilder<T extends AbstractDirectionBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.recipe.direction.DirectionMiter direction;


    /**
     *  Constructs a new <code>AbstractDirectionBuilder</code>.
     *
     *  @param direction the direction to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractDirectionBuilder(net.okapia.osid.jamocha.builder.recipe.direction.DirectionMiter direction) {
        super(direction);
        this.direction = direction;
        return;
    }


    /**
     *  Builds the direction.
     *
     *  @return the new direction
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.recipe.Direction build() {
        (new net.okapia.osid.jamocha.builder.validator.recipe.direction.DirectionValidator(getValidations())).validate(this.direction);
        return (new net.okapia.osid.jamocha.builder.recipe.direction.ImmutableDirection(this.direction));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the direction miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.recipe.direction.DirectionMiter getMiter() {
        return (this.direction);
    }


    /**
     *  Sets the recipe.
     *
     *  @param recipe a recipe
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>recipe</code> is
     *          <code>null</code>
     */

    public T recipe(org.osid.recipe.Recipe recipe) {
        getMiter().setRecipe(recipe);
        return (self());
    }


    /**
     *  Adds a procedure.
     *
     *  @param procedure a procedure
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>procedure</code>
     *          is <code>null</code>
     */

    public T procedure(org.osid.recipe.Procedure procedure) {
        getMiter().addProcedure(procedure);
        return (self());
    }


    /**
     *  Sets all the procedures.
     *
     *  @param procedures a collection of procedures
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>procedures</code>
     *          is <code>null</code>
     */

    public T procedures(java.util.Collection<org.osid.recipe.Procedure> procedures) {
        getMiter().setProcedures(procedures);
        return (self());
    }


    /**
     *  Adds an ingredient.
     *
     *  @param ingredient an ingredient
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>ingredient</code>
     *          is <code>null</code>
     */

    public T ingredient(org.osid.recipe.Ingredient ingredient) {
        getMiter().addIngredient(ingredient);
        return (self());
    }


    /**
     *  Sets all the ingredients.
     *
     *  @param ingredients a collection of ingredients
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>ingredients</code> is <code>null</code>
     */

    public T ingredients(java.util.Collection<org.osid.recipe.Ingredient> ingredients) {
        getMiter().setIngredients(ingredients);
        return (self());
    }


    /**
     *  Sets the estimated duration.
     *
     *  @param duration an estimated duration
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>duration</code>
     *          is <code>null</code>
     */

    public T estimatedDuration(org.osid.calendaring.Duration duration) {
        getMiter().setEstimatedDuration(duration);
        return (self());
    }


    /**
     *  Adds an asset.
     *
     *  @param asset an asset
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>asset</code> is
     *          <code>null</code>
     */

    public T asset(org.osid.repository.Asset asset) {
        getMiter().addAsset(asset);
        return (self());
    }


    /**
     *  Sets all the assets.
     *
     *  @param assets a collection of assets
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>assets</code> is
     *          <code>null</code>
     */

    public T assets(java.util.Collection<org.osid.repository.Asset> assets) {
        getMiter().setAssets(assets);
        return (self());
    }


    /**
     *  Adds a Direction record.
     *
     *  @param record a direction record
     *  @param recordType the type of direction record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.recipe.records.DirectionRecord record, org.osid.type.Type recordType) {
        getMiter().addDirectionRecord(record, recordType);
        return (self());
    }
}       


