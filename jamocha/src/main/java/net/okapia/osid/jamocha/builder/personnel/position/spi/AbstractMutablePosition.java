//
// AbstractMutablePosition.java
//
//     Defines a mutable Position.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.personnel.position.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a mutable <code>Position</code>.
 */

public abstract class AbstractMutablePosition
    extends net.okapia.osid.jamocha.personnel.position.spi.AbstractPosition
    implements org.osid.personnel.Position,
               net.okapia.osid.jamocha.builder.personnel.position.PositionMiter {


    /**
     *  Gets the <code> Id </code> associated with this instance of
     *  this OSID object.
     *
     *  @param id
     *  @throws org.osid.NullArgumentException <code>is</code> is
     *          <code>null</code>
     */

    @Override
    public void setId(org.osid.id.Id id) {
        super.setId(id);
        return;
    }


    /**
     *  Sets the current flag to <code>true</code>.
     */
    
    @Override
    public void current() {
        super.current();
        return;
    }


    /**
     *  Sets the current flag to <code>false</code>.
     */

    @Override
    public void stale() {
        super.stale();
        return;
    }


    /**
     *  Adds a record type.
     *
     *  @param recordType
     *  @throws org.osid.NullArgumentException <code>recordType</code>
     *          is <code>null</code>
     */

    @Override
    public void addRecordType(org.osid.type.Type recordType) {
        super.addRecordType(recordType);
        return;
    }


    /**
     *  Adds a record to this position. 
     *
     *  @param record position record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
        
    @Override    
    public void addPositionRecord(org.osid.personnel.records.PositionRecord record, org.osid.type.Type recordType) {
        super.addPositionRecord(record, recordType);     
        return;
    }


    /**
     *  Adds a property set.
     *
     *  @param set set of properties
     *  @param recordType associated type
     *  @throws org.osid.NullArgumentException <code>set</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    @Override
    public void addProperties(java.util.Collection<org.osid.Property> set, org.osid.type.Type recordType) {
        super.addProperties(set, recordType);
        return;
    }


    /**
     *  Sets the start date for when this position is effective.
     *
     *  @param date the start date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setStartDate(org.osid.calendaring.DateTime date) {
        super.setStartDate(date);
        return;
    }


    /**
     *  Sets the end date for when this position ceases to be
     *  effective.
     *
     *  @param date the end date
     *  @throws org.osid.NullArgumentException <code>date</code> is
     *          <code>null</code>
     */
    
    @Override
    public void setEndDate(org.osid.calendaring.DateTime date) {
        super.setEndDate(date);
        return;
    }


    /**
     *  Sets the display name for this position.
     *
     *  @param displayName the name for this position
     *  @throws org.osid.NullArgumentException <code>displayName</code> is
     *          <code>null</code>
     */

    @Override
    public void setDisplayName(org.osid.locale.DisplayText displayName) {
        super.setDisplayName(displayName);
        return;
    }


    /**
     *  Sets the description of this position.
     *
     *  @param description the description of this position
     *  @throws org.osid.NullArgumentException
     *          <code>description</code> is <code>null</code>
     */

    @Override
    public void setDescription(org.osid.locale.DisplayText description) {
        super.setDescription(description);
        return;
    }


    /**
     *  Sets a genus type.
     *
     *  @param genusType a genus type
     *  @throws org.osid.NullArgumentException <code>genusType</code>
     *          is <code>null</code>
     */

    @Override
    public void setGenusType(org.osid.type.Type genusType) {
        super.setGenusType(genusType);
        return;
    }


    /**
     *  Sets the organization.
     *
     *  @param organization an organization
     *  @throws org.osid.NullArgumentException
     *          <code>organization</code> is <code>null</code>
     */

    @Override
    public void setOrganization(org.osid.personnel.Organization organization) {
        super.setOrganization(organization);
        return;
    }


    /**
     *  Sets the title.
     *
     *  @param title a title
     *  @throws org.osid.NullArgumentException <code>title</code> is
     *          <code>null</code>
     */

    @Override
    public void setTitle(org.osid.locale.DisplayText title) {
        super.setTitle(title);
        return;
    }


    /**
     *  Sets the level.
     *
     *  @param level a level
     *  @throws org.osid.NullArgumentException <code>level</code> is
     *          <code>null</code>
     */

    @Override
    public void setLevel(org.osid.grading.Grade level) {
        super.setLevel(level);
        return;
    }


    /**
     *  Adds a qualification.
     *
     *  @param qualification a qualification
     *  @throws org.osid.NullArgumentException
     *          <code>qualification</code> is <code>null</code>
     */

    @Override
    public void addQualification(org.osid.learning.Objective qualification) {
        super.addQualification(qualification);
        return;
    }


    /**
     *  Sets all the qualifications.
     *
     *  @param qualifications a collection of qualifications
     *  @throws org.osid.NullArgumentException
     *          <code>qualifications</code> is <code>null</code>
     */

    @Override
    public void setQualifications(java.util.Collection<org.osid.learning.Objective> qualifications) {
        super.setQualifications(qualifications);
        return;
    }


    /**
     *  Sets the target appointments.
     *
     *  @param appointment a target appointments
     *  @throws org.osid.InvalidArgumentException
     *          <code>appointments</code> is negative
     */

    @Override
    public void setTargetAppointments(long appointment) {
        super.setTargetAppointments(appointment);
        return;
    }


    /**
     *  Sets the required commitment.
     *
     *  @param percentage a required commitment
     *  @throws org.osid.InvalidArgumentException
     *          <code>percentage</code> is out of range
     */

    @Override
    public void setRequiredCommitment(long percentage) {
        super.setRequiredCommitment(percentage);
        return;
    }


    /**
     *  Sets the low salary range.
     *
     *  @param salary a low salary range
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    @Override
    public void setLowSalaryRange(org.osid.financials.Currency salary) {
        super.setLowSalaryRange(salary);
        return;
    }


    /**
     *  Sets the midpoint salary range.
     *
     *  @param salary a midpoint salary range
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    @Override
    public void setMidpointSalaryRange(org.osid.financials.Currency salary) {
        super.setMidpointSalaryRange(salary);
        return;
    }


    /**
     *  Sets the high salary range.
     *
     *  @param salary a high salary range
     *  @throws org.osid.NullArgumentException <code>salary</code> is
     *          <code>null</code>
     */

    @Override
    public void setHighSalaryRange(org.osid.financials.Currency salary) {
        super.setHighSalaryRange(salary);
        return;
    }


    /**
     *  Sets the compensation frequency.
     *
     *  @param frequency a compensation frequency
     *  @throws org.osid.NullArgumentException <code>frequency</code>
     *          is <code>null</code>
     */

    @Override
    public void setCompensationFrequency(org.osid.calendaring.Duration frequency) {
        super.setCompensationFrequency(frequency);
        return;
    }


    /**
     *  Sets the exempt flag.
     *
     *  @param exempt <code> true </code> if this position is exempt,
     *         <code> false </code> is non-exempt
     */

    @Override
    public void setExempt(boolean exempt) {
        super.setExempt(exempt);
        return;
    }


    /**
     *  Sets the benefits type.
     *
     *  @param benefitsType a benefits type
     *  @throws org.osid.NullArgumentException
     *          <code>benefitsType</code> is <code>null</code>
     */

    @Override
    public void setBenefitsType(org.osid.type.Type benefitsType) {
        super.setBenefitsType(benefitsType);
        return;
    }
}

