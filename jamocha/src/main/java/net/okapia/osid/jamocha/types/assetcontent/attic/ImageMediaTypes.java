    CGM ("cgm", "Computer Graphics Metafile"),
    FITC ("fits", "Flexible Image Transport System"),
    G3FAX ("g3fax", "G3 Facsimile"),
    GIF ("gif", Graphic Interhange Format"),
    IEF ("ief", "Image Exchange Format"),
    JP2 ("jp2", "Joint Photographic Experts Group 2000"),
    JPEG ("jpeg", "Joint Photographic Experts Group"),
    JPM ("jpm", "Joint Photographic Experts Group 2000 Compound Image"),
    JPX ("jpx", "Joint Photographic Experts Group 2000 Extended"),
    KTX ("ktx", "OpenGL KTX"),
    NAPLPS ("naplps", "North American Presentation Level Protocol Syntax"),
    PNG ("png", "Portable Network Graphics"),
    PRS_BTIF ("prs.btif", "Personal Tree 16-bit"),
    PRS_PTI ("prs.pti", "Personal Tree"),
    PWG-RASTER ("pwg-raster, "Printer Working Group"),
    SVG ("svg+xml", "Scalable Vector Groups"),
    T38 ("t38", "Real-time Facsimile T.38"),
    TIFF ("tiff", "Tag Image File Format"),
    TIFF-FX ("tiff-fx", "Tag Image File Format Fax eXtended"),
    PHOTOSHOP ("vnd.adobe.photoshop", "Adobe Photoshop (PSD)"),
    AIRZIP ("vnd.airzip.accelerator.azv", "AirZip"),
    vnd.cns.inf2 [McLaughlin]
    vnd.dece.graphic [Dolan]
    vnd.djvu [Bottou]
    DWG ("vnd.dwg", "CAD DraWinG"),
    vnd.dxf [Moline]
    vnd.dvb.subtitle [Siebert][Lagally]
    vnd.fastbidsheet [Becker]
    vnd.fpx [Spencer]
    vnd.fst [Fuldseth]
    vnd.fujixerox.edmics-mmr [Onda]
    vnd.fujixerox.edmics-rlc [Onda]
    vnd.globalgraphics.pgb [Bailey]
    MSICON, ("vnd.microsoft.icon", "Microsoft Icon"),
    vnd.mix [Reddy]
    MODI("vnd.ms-modi", "Microsoft Office Document Imaging"),
    vnd.net-fpx [Spencer]
    vnd.radiance [Fritz][GWard]
    vnd.sealed.png [Petersen]
    vnd.sealedmedia.softseal.gif [Petersen]
    vnd.sealedmedia.softseal.jpg [Petersen]
    SVF ("vnd.svf", ""),
    WBMP ("vnd.wap.wbmp", "Wireless Application Protocol Bitmap Format"),
    XIFF ("vnd.xiff", "Xerox Image File");
