//
// InvariantMapNodeLookupSession
//
//    Implements a Node lookup service backed by a fixed collection of
//    nodes.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.topology;


/**
 *  Implements a Node lookup service backed by a fixed
 *  collection of nodes. The nodes are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapNodeLookupSession
    extends net.okapia.osid.jamocha.core.topology.spi.AbstractMapNodeLookupSession
    implements org.osid.topology.NodeLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapNodeLookupSession</code> with no
     *  nodes.
     *  
     *  @param graph the graph
     *  @throws org.osid.NullArgumnetException {@code graph} is
     *          {@code null}
     */

    public InvariantMapNodeLookupSession(org.osid.topology.Graph graph) {
        setGraph(graph);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapNodeLookupSession</code> with a single
     *  node.
     *  
     *  @param graph the graph
     *  @param node a single node
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code node} is <code>null</code>
     */

      public InvariantMapNodeLookupSession(org.osid.topology.Graph graph,
                                               org.osid.topology.Node node) {
        this(graph);
        putNode(node);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapNodeLookupSession</code> using an array
     *  of nodes.
     *  
     *  @param graph the graph
     *  @param nodes an array of nodes
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code nodes} is <code>null</code>
     */

      public InvariantMapNodeLookupSession(org.osid.topology.Graph graph,
                                               org.osid.topology.Node[] nodes) {
        this(graph);
        putNodes(nodes);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapNodeLookupSession</code> using a
     *  collection of nodes.
     *
     *  @param graph the graph
     *  @param nodes a collection of nodes
     *  @throws org.osid.NullArgumentException {@code graph} or
     *          {@code nodes} is <code>null</code>
     */

      public InvariantMapNodeLookupSession(org.osid.topology.Graph graph,
                                               java.util.Collection<? extends org.osid.topology.Node> nodes) {
        this(graph);
        putNodes(nodes);
        return;
    }
}
