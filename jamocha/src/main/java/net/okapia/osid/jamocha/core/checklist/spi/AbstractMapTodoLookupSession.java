//
// AbstractMapTodoLookupSession
//
//    A simple framework for providing a Todo lookup service
//    backed by a fixed collection of todos.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.checklist.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Todo lookup service backed by a
 *  fixed collection of todos. The todos are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Todos</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapTodoLookupSession
    extends net.okapia.osid.jamocha.checklist.spi.AbstractTodoLookupSession
    implements org.osid.checklist.TodoLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.checklist.Todo> todos = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.checklist.Todo>());


    /**
     *  Makes a <code>Todo</code> available in this session.
     *
     *  @param  todo a todo
     *  @throws org.osid.NullArgumentException <code>todo<code>
     *          is <code>null</code>
     */

    protected void putTodo(org.osid.checklist.Todo todo) {
        this.todos.put(todo.getId(), todo);
        return;
    }


    /**
     *  Makes an array of todos available in this session.
     *
     *  @param  todos an array of todos
     *  @throws org.osid.NullArgumentException <code>todos<code>
     *          is <code>null</code>
     */

    protected void putTodos(org.osid.checklist.Todo[] todos) {
        putTodos(java.util.Arrays.asList(todos));
        return;
    }


    /**
     *  Makes a collection of todos available in this session.
     *
     *  @param  todos a collection of todos
     *  @throws org.osid.NullArgumentException <code>todos<code>
     *          is <code>null</code>
     */

    protected void putTodos(java.util.Collection<? extends org.osid.checklist.Todo> todos) {
        for (org.osid.checklist.Todo todo : todos) {
            this.todos.put(todo.getId(), todo);
        }

        return;
    }


    /**
     *  Removes a Todo from this session.
     *
     *  @param  todoId the <code>Id</code> of the todo
     *  @throws org.osid.NullArgumentException <code>todoId<code> is
     *          <code>null</code>
     */

    protected void removeTodo(org.osid.id.Id todoId) {
        this.todos.remove(todoId);
        return;
    }


    /**
     *  Gets the <code>Todo</code> specified by its <code>Id</code>.
     *
     *  @param  todoId <code>Id</code> of the <code>Todo</code>
     *  @return the todo
     *  @throws org.osid.NotFoundException <code>todoId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>todoId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.Todo getTodo(org.osid.id.Id todoId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.checklist.Todo todo = this.todos.get(todoId);
        if (todo == null) {
            throw new org.osid.NotFoundException("todo not found: " + todoId);
        }

        return (todo);
    }


    /**
     *  Gets all <code>Todos</code>. In plenary mode, the returned
     *  list contains all known todos or an error
     *  results. Otherwise, the returned list may contain only those
     *  todos that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Todos</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.checklist.TodoList getTodos()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.checklist.todo.ArrayTodoList(this.todos.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.todos.clear();
        super.close();
        return;
    }
}
