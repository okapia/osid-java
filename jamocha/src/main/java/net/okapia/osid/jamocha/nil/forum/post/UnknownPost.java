//
// UnknownPost.java
//
//     Defines an unknown Post.
//
//
// Tom Coppeto
// Okapia
// 8 December 2009
//
//
// Copyright (c) 2009 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.nil.forum.post;


/**
 *  Defines an unknown <code>Post</code>.
 */

public final class UnknownPost
    extends net.okapia.osid.jamocha.nil.forum.post.spi.AbstractUnknownPost
    implements org.osid.forum.Post {


    /**
     *  Constructs a new <code>UnknownPost</code>.
     */

    public UnknownPost() {
        return;
    }


    /**
     *  Constructs a new <code>UnknownPost</code> with all
     *  the optional methods enabled.
     *
     *  @param optional <code>true</code> to enable the optional
     *         methods
     */

    public UnknownPost(boolean optional) {
        super(optional);
        addPostRecord(new PostRecord(), net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
        return;
    }


    /**
     *  Gets an unknown Post.
     *
     *  @return an unknown Post
     */

    public static org.osid.forum.Post create() {
        return (net.okapia.osid.jamocha.builder.validator.forum.post.PostValidator.validatePost(new UnknownPost()));
    }


    public class PostRecord 
        extends net.okapia.osid.jamocha.spi.AbstractOsidRecord
        implements org.osid.forum.records.PostRecord {

        
        protected PostRecord() {
            addRecordType(net.okapia.osid.jamocha.nil.privateutil.UnknownRecordType.valueOf(OBJECT));
            return;
        }
    }        
}
