//
// AbstractSpeedMetadata.java
//
//     Defines a speed Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a speed Metadata.
 */

public abstract class AbstractSpeedMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private org.osid.mapping.Speed minimum = net.okapia.osid.primordium.mapping.Speed.valueOf(0);
    private org.osid.mapping.Speed maximum = net.okapia.osid.primordium.mapping.Speed.valueOf(299792458);

    private final java.util.Collection<org.osid.mapping.Speed> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.Speed> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.mapping.Speed> existing = new java.util.LinkedHashSet<>();


    /**
     *  Constructs a new {@code AbstractSpeedMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractSpeedMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.SPEED, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractSpeedMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractSpeedMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.SPEED, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the minimum speed value. 
     *
     *  @return the minimum speed 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          SPEED </code>
     */

    @OSID @Override
    public org.osid.mapping.Speed getMinimumSpeed() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum speed value. 
     *
     *  @return the maximum speed 
     *  @throws org.osid.IllegalStateException syntax is not a <code> SPEED 
     *          </code> 
     */

    @OSID @Override
    public org.osid.mapping.Speed getMaximumSpeed() {
        return (this.maximum);
    }

    
    /**
     *  Sets the speed range.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max} or, {@code min} or {@code
     *          max} is negative
     */

    protected void setSpeedRange(org.osid.mapping.Speed min, org.osid.mapping.Speed max) {
        nullarg(min, "min");
        nullarg(max, "max");

        if (min.isGreater(max)) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.minimum = min;
        this.maximum = max;

        return;
    }


    /**
     *  Gets the set of acceptable speed values. 
     *
     *  @return a set of speeds or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          SPEED </code>
     */

    @OSID @Override
    public org.osid.mapping.Speed[] getSpeedSet() {
        return (this.set.toArray(new org.osid.mapping.Speed[this.set.size()]));
    }

    
    /**
     *  Sets the speed set.
     *
     *  @param values a collection of accepted speed values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setSpeedSet(java.util.Collection<org.osid.mapping.Speed> values) {
        this.set.clear();
        addToSpeedSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the speed set.
     *
     *  @param values a collection of accepted speed values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToSpeedSet(java.util.Collection<org.osid.mapping.Speed> values) {
        nullarg(values, "speed set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the speed set.
     *
     *  @param value a speed value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void addToSpeedSet(org.osid.mapping.Speed value) {
        nullarg(value, "speed value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the speed set.
     *
     *  @param value a speed value
     *  @throws org.osid.InvalidArgumentException value is negative
     */

    protected void removeFromSpeedSet(org.osid.mapping.Speed value) {
        nullarg(value, "speed value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the speed set.
     */

    protected void clearSpeedSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default speed values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default speed values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          SPEED </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.mapping.Speed[] getDefaultSpeedValues() {
        return (this.defvals.toArray(new org.osid.mapping.Speed[this.defvals.size()]));
    }


    /**
     *  Sets the default speed set.
     *
     *  @param values a collection of default speed values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultSpeedValues(java.util.Collection<org.osid.mapping.Speed> values) {
        clearDefaultSpeedValues();
        addDefaultSpeedValues(values);
        return;
    }


    /**
     *  Adds a collection of default speed values.
     *
     *  @param values a collection of default speed values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultSpeedValues(java.util.Collection<org.osid.mapping.Speed> values) {
        nullarg(values, "default speed values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default speed value.
     *
     *  @param value a speed value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultSpeedValue(org.osid.mapping.Speed value) {
        nullarg(value, "default speed value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default speed value.
     *
     *  @param value a speed value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultSpeedValue(org.osid.mapping.Speed value) {
        nullarg(value, "default speed value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default speed values.
     */

    protected void clearDefaultSpeedValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing speed values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing speed values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          SPEED </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.mapping.Speed[] getExistingSpeedValues() {
        return (this.existing.toArray(new org.osid.mapping.Speed[this.existing.size()]));
    }


    /**
     *  Sets the existing speed set.
     *
     *  @param values a collection of existing speed values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingSpeedValues(java.util.Collection<org.osid.mapping.Speed> values) {
        clearExistingSpeedValues();
        addExistingSpeedValues(values);
        return;
    }


    /**
     *  Adds a collection of existing speed values.
     *
     *  @param values a collection of existing speed values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingSpeedValues(java.util.Collection<org.osid.mapping.Speed> values) {
        nullarg(values, "existing speed values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing speed value.
     *
     *  @param value a speed value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingSpeedValue(org.osid.mapping.Speed value) {
        nullarg(value, "existing speed value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing speed value.
     *
     *  @param value a speed value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingSpeedValue(org.osid.mapping.Speed value) {
        nullarg(value, "existing speed value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing speed values.
     */

    protected void clearExistingSpeedValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }    
}