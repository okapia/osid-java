//
// AbstractMapBrokerConstrainerLookupSession
//
//    A simple framework for providing a BrokerConstrainer lookup service
//    backed by a fixed collection of broker constrainers.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.provisioning.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a BrokerConstrainer lookup service backed by a
 *  fixed collection of broker constrainers. The broker constrainers are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>BrokerConstrainers</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapBrokerConstrainerLookupSession
    extends net.okapia.osid.jamocha.provisioning.rules.spi.AbstractBrokerConstrainerLookupSession
    implements org.osid.provisioning.rules.BrokerConstrainerLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.provisioning.rules.BrokerConstrainer> brokerConstrainers = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.provisioning.rules.BrokerConstrainer>());


    /**
     *  Makes a <code>BrokerConstrainer</code> available in this session.
     *
     *  @param  brokerConstrainer a broker constrainer
     *  @throws org.osid.NullArgumentException <code>brokerConstrainer<code>
     *          is <code>null</code>
     */

    protected void putBrokerConstrainer(org.osid.provisioning.rules.BrokerConstrainer brokerConstrainer) {
        this.brokerConstrainers.put(brokerConstrainer.getId(), brokerConstrainer);
        return;
    }


    /**
     *  Makes an array of broker constrainers available in this session.
     *
     *  @param  brokerConstrainers an array of broker constrainers
     *  @throws org.osid.NullArgumentException <code>brokerConstrainers<code>
     *          is <code>null</code>
     */

    protected void putBrokerConstrainers(org.osid.provisioning.rules.BrokerConstrainer[] brokerConstrainers) {
        putBrokerConstrainers(java.util.Arrays.asList(brokerConstrainers));
        return;
    }


    /**
     *  Makes a collection of broker constrainers available in this session.
     *
     *  @param  brokerConstrainers a collection of broker constrainers
     *  @throws org.osid.NullArgumentException <code>brokerConstrainers<code>
     *          is <code>null</code>
     */

    protected void putBrokerConstrainers(java.util.Collection<? extends org.osid.provisioning.rules.BrokerConstrainer> brokerConstrainers) {
        for (org.osid.provisioning.rules.BrokerConstrainer brokerConstrainer : brokerConstrainers) {
            this.brokerConstrainers.put(brokerConstrainer.getId(), brokerConstrainer);
        }

        return;
    }


    /**
     *  Removes a BrokerConstrainer from this session.
     *
     *  @param  brokerConstrainerId the <code>Id</code> of the broker constrainer
     *  @throws org.osid.NullArgumentException <code>brokerConstrainerId<code> is
     *          <code>null</code>
     */

    protected void removeBrokerConstrainer(org.osid.id.Id brokerConstrainerId) {
        this.brokerConstrainers.remove(brokerConstrainerId);
        return;
    }


    /**
     *  Gets the <code>BrokerConstrainer</code> specified by its <code>Id</code>.
     *
     *  @param  brokerConstrainerId <code>Id</code> of the <code>BrokerConstrainer</code>
     *  @return the brokerConstrainer
     *  @throws org.osid.NotFoundException <code>brokerConstrainerId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>brokerConstrainerId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainer getBrokerConstrainer(org.osid.id.Id brokerConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.rules.BrokerConstrainer brokerConstrainer = this.brokerConstrainers.get(brokerConstrainerId);
        if (brokerConstrainer == null) {
            throw new org.osid.NotFoundException("brokerConstrainer not found: " + brokerConstrainerId);
        }

        return (brokerConstrainer);
    }


    /**
     *  Gets all <code>BrokerConstrainers</code>. In plenary mode, the returned
     *  list contains all known brokerConstrainers or an error
     *  results. Otherwise, the returned list may contain only those
     *  brokerConstrainers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>BrokerConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.rules.BrokerConstrainerList getBrokerConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.provisioning.rules.brokerconstrainer.ArrayBrokerConstrainerList(this.brokerConstrainers.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.brokerConstrainers.clear();
        super.close();
        return;
    }
}
