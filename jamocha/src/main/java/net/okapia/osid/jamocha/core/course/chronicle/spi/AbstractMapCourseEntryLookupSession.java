//
// AbstractMapCourseEntryLookupSession
//
//    A simple framework for providing a CourseEntry lookup service
//    backed by a fixed collection of course entries.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.chronicle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a CourseEntry lookup service backed by a
 *  fixed collection of course entries. The course entries are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CourseEntries</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapCourseEntryLookupSession
    extends net.okapia.osid.jamocha.course.chronicle.spi.AbstractCourseEntryLookupSession
    implements org.osid.course.chronicle.CourseEntryLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.course.chronicle.CourseEntry> courseEntries = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.course.chronicle.CourseEntry>());


    /**
     *  Makes a <code>CourseEntry</code> available in this session.
     *
     *  @param  courseEntry a course entry
     *  @throws org.osid.NullArgumentException <code>courseEntry<code>
     *          is <code>null</code>
     */

    protected void putCourseEntry(org.osid.course.chronicle.CourseEntry courseEntry) {
        this.courseEntries.put(courseEntry.getId(), courseEntry);
        return;
    }


    /**
     *  Makes an array of course entries available in this session.
     *
     *  @param  courseEntries an array of course entries
     *  @throws org.osid.NullArgumentException <code>courseEntries<code>
     *          is <code>null</code>
     */

    protected void putCourseEntries(org.osid.course.chronicle.CourseEntry[] courseEntries) {
        putCourseEntries(java.util.Arrays.asList(courseEntries));
        return;
    }


    /**
     *  Makes a collection of course entries available in this session.
     *
     *  @param  courseEntries a collection of course entries
     *  @throws org.osid.NullArgumentException <code>courseEntries<code>
     *          is <code>null</code>
     */

    protected void putCourseEntries(java.util.Collection<? extends org.osid.course.chronicle.CourseEntry> courseEntries) {
        for (org.osid.course.chronicle.CourseEntry courseEntry : courseEntries) {
            this.courseEntries.put(courseEntry.getId(), courseEntry);
        }

        return;
    }


    /**
     *  Removes a CourseEntry from this session.
     *
     *  @param  courseEntryId the <code>Id</code> of the course entry
     *  @throws org.osid.NullArgumentException <code>courseEntryId<code> is
     *          <code>null</code>
     */

    protected void removeCourseEntry(org.osid.id.Id courseEntryId) {
        this.courseEntries.remove(courseEntryId);
        return;
    }


    /**
     *  Gets the <code>CourseEntry</code> specified by its <code>Id</code>.
     *
     *  @param  courseEntryId <code>Id</code> of the <code>CourseEntry</code>
     *  @return the courseEntry
     *  @throws org.osid.NotFoundException <code>courseEntryId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>courseEntryId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntry getCourseEntry(org.osid.id.Id courseEntryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.course.chronicle.CourseEntry courseEntry = this.courseEntries.get(courseEntryId);
        if (courseEntry == null) {
            throw new org.osid.NotFoundException("courseEntry not found: " + courseEntryId);
        }

        return (courseEntry);
    }


    /**
     *  Gets all <code>CourseEntries</code>. In plenary mode, the returned
     *  list contains all known courseEntries or an error
     *  results. Otherwise, the returned list may contain only those
     *  courseEntries that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>CourseEntries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.chronicle.CourseEntryList getCourseEntries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.course.chronicle.courseentry.ArrayCourseEntryList(this.courseEntries.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.courseEntries.clear();
        super.close();
        return;
    }
}
