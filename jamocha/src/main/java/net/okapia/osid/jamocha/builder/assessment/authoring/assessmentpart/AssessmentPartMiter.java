//
// AssessmentPartMiter.java
//
//     Defines an AssessmentPart miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.authoring.assessmentpart;


/**
 *  Defines an <code>AssessmentPart</code> miter for use with the builders.
 */

public interface AssessmentPartMiter
    extends net.okapia.osid.jamocha.builder.spi.OperableOsidObjectMiter,
            net.okapia.osid.jamocha.builder.spi.ContainableMiter,
            org.osid.assessment.authoring.AssessmentPart {


    /**
     *  Sets the assessment.
     *
     *  @param assessment an assessment
     *  @throws org.osid.NullArgumentException
     *          <code>assessment</code> is <code>null</code>
     */

    public void setAssessment(org.osid.assessment.Assessment assessment);


    /**
     *  Sets the parent assessment part.
     *
     *  @param assessmentPart an assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPart</code> is <code>null</code>
     */

    public void setParentAssessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart);


    /**
     *  Sets the section.
     *
     *  @param section <code>true</code> if a section,
     *         <code>false</code> otherwise
     */

    public void setSection(boolean section);


    /**
     *  Sets the weight.
     *
     *  @param weight a weight
     */

    public void setWeight(long weight);


    /**
     *  Sets the allocated time.
     *
     *  @param allocatedTime an allocated time
     *  @throws org.osid.NullArgumentException
     *          <code>allocatedTime</code> is <code>null</code>
     */

    public void setAllocatedTime(org.osid.calendaring.Duration allocatedTime);


    /**
     *  Adds a child assessment part.
     *
     *  @param childAssessmentPart a child assessment part
     *  @throws org.osid.NullArgumentException
     *          <code>childAssessmentPart</code> is <code>null</code>
     */

    public void addChildAssessmentPart(org.osid.assessment.authoring.AssessmentPart childAssessmentPart);


    /**
     *  Sets all the child assessment parts.
     *
     *  @param childAssessmentParts a collection of child assessment parts
     *  @throws org.osid.NullArgumentException
     *          <code>childAssessmentParts</code> is <code>null</code>
     */

    public void setChildAssessmentParts(java.util.Collection<org.osid.assessment.authoring.AssessmentPart> childAssessmentParts);


    /**
     *  Adds an AssessmentPart record.
     *
     *  @param record an assessmentPart record
     *  @param recordType the type of assessmentPart record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addAssessmentPartRecord(org.osid.assessment.authoring.records.AssessmentPartRecord record, org.osid.type.Type recordType);
}       


