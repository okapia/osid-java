//
// MutableReplyList.java
//
//     Implements a ReplyList. This list allows Replies to be
//     added after this list has been created.
//
//
// Tom Coppeto
// OnTapSolutions
// 29 June 2008
//
//
// Copyright (c) 2008, 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.reply;


/**
 *  <p>Implements a ReplyList. This list allows Replies to be
 *  added after this reply has been created. One this list has been
 *  returned to the consumer, all subsequent additions occur in a
 *  separate processing thread.  The creator of this reply must
 *  invoke <code>eol()</code> when there are no more replies to be
 *  added.</p>
 *
 *  <p> If the consumer of the <code>ReplyList</code> interface
 *  reaches the end of the internal buffer before <code>eol()</code>,
 *  then methods will block until more replies are added or
 *  <code>eol()</code> is invoked.</p>
 *
 *  <p><code>available()</code> never blocks but may return
 *  <code>0</code> if waiting for more replies to be added.</p>
 */

public final class MutableReplyList
    extends net.okapia.osid.jamocha.forum.reply.spi.AbstractMutableReplyList
    implements org.osid.forum.ReplyList {


    /**
     *  Creates a new empty <code>MutableReplyList</code>.
     */

    public MutableReplyList() {
        super();
    }


    /**
     *  Creates a new <code>MutableReplyList</code>.
     *
     *  @param reply a <code>Reply</code>
     *  @throws org.osid.NullArgumentException <code>reply</code>
     *          is <code>null</code>
     */

    public MutableReplyList(org.osid.forum.Reply reply) {
        super(reply);
        return;
    }


    /**
     *  Creates a new <code>MutableReplyList</code>.
     *
     *  @param array an array of replies
     *  @throws org.osid.NullArgumentException <code>array</code>
     *          is <code>null</code>
     */

    public MutableReplyList(org.osid.forum.Reply[] array) {
        super(array);
        return;
    }


    /**
     *  Creates a new <code>MutableReplyList</code>.
     *
     *  @param collection a java.util.Collection of replies
     *  @throws org.osid.NullArgumentException <code>collection</code>
     *          is <code>null</code>
     */

    public MutableReplyList(java.util.Collection<org.osid.forum.Reply> collection) {
        super(collection);
        return;
    }
}
