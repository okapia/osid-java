//
// AbstractPublisherLookupSession.java
//
//    A starter implementation framework for providing a Publisher
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.subscription.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A starter implementation framework for providing a Publisher
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getPublishers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractPublisherLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.subscription.PublisherLookupSession {

    private boolean pedantic = false;
    private org.osid.subscription.Publisher publisher = new net.okapia.osid.jamocha.nil.subscription.publisher.UnknownPublisher();
    

    /**
     *  Tests if this user can perform <code>Publisher</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupPublishers() {
        return (true);
    }


    /**
     *  A complete view of the <code>Publisher</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativePublisherView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Publisher</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryPublisherView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }

     
    /**
     *  Gets the <code>Publisher</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Publisher</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Publisher</code> and
     *  retained for compatibility.
     *
     *  @param  publisherId <code>Id</code> of the
     *          <code>Publisher</code>
     *  @return the publisher
     *  @throws org.osid.NotFoundException <code>publisherId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>publisherId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.Publisher getPublisher(org.osid.id.Id publisherId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.subscription.PublisherList publishers = getPublishers()) {
            while (publishers.hasNext()) {
                org.osid.subscription.Publisher publisher = publishers.getNextPublisher();
                if (publisher.getId().equals(publisherId)) {
                    return (publisher);
                }
            }
        } 

        throw new org.osid.NotFoundException(publisherId + " not found");
    }


    /**
     *  Gets a <code>PublisherList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  publishers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Publishers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getPublishers()</code>.
     *
     *  @param  publisherIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Publisher</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>publisherIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByIds(org.osid.id.IdList publisherIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.subscription.Publisher> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = publisherIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getPublisher(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("publisher " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.subscription.publisher.LinkedPublisherList(ret));
    }


    /**
     *  Gets a <code>PublisherList</code> corresponding to the given
     *  publisher genus <code>Type</code> which does not include
     *  publishers of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getPublishers()</code>.
     *
     *  @param  publisherGenusType a publisher genus type 
     *  @return the returned <code>Publisher</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>publisherGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByGenusType(org.osid.type.Type publisherGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.publisher.PublisherGenusFilterList(getPublishers(), publisherGenusType));
    }


    /**
     *  Gets a <code>PublisherList</code> corresponding to the given
     *  publisher genus <code>Type</code> and include any additional
     *  publishers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPublishers()</code>.
     *
     *  @param  publisherGenusType a publisher genus type 
     *  @return the returned <code>Publisher</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>publisherGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByParentGenusType(org.osid.type.Type publisherGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getPublishersByGenusType(publisherGenusType));
    }


    /**
     *  Gets a <code>PublisherList</code> containing the given
     *  publisher record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getPublishers()</code>.
     *
     *  @param  publisherRecordType a publisher record type 
     *  @return the returned <code>Publisher</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>publisherRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByRecordType(org.osid.type.Type publisherRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.subscription.publisher.PublisherRecordFilterList(getPublishers(), publisherRecordType));
    }


    /**
     *  Gets a <code>PublisherList</code> from the given provider.
     *  
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session.
     *
     *  @param resourceId a resource <code>Id</code>
     *  @return the returned <code>Publisher</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.subscription.PublisherList getPublishersByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (new net.okapia.osid.jamocha.inline.filter.subscription.publisher.PublisherProviderFilterList(getPublishers(), resourceId));
    }


    /**
     *  Gets all <code>Publishers</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  publishers or an error results. Otherwise, the returned list
     *  may contain only those publishers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Publishers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.subscription.PublisherList getPublishers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the publisher list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of publishers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.subscription.PublisherList filterPublishersOnViews(org.osid.subscription.PublisherList list)
        throws org.osid.OperationFailedException {

        return (list);
    }
}
