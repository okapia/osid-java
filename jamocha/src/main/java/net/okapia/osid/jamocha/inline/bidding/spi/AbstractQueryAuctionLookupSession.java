//
// AbstractQueryAuctionLookupSession.java
//
//    An inline adapter that maps an AuctionLookupSession to
//    an AuctionQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.bidding.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps an AuctionLookupSession to
 *  an AuctionQuerySession.
 */

public abstract class AbstractQueryAuctionLookupSession
    extends net.okapia.osid.jamocha.bidding.spi.AbstractAuctionLookupSession
    implements org.osid.bidding.AuctionLookupSession {

    private boolean activeonly    = false;
    private boolean effectiveonly = false;    
    private final org.osid.bidding.AuctionQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryAuctionLookupSession.
     *
     *  @param querySession the underlying auction query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryAuctionLookupSession(org.osid.bidding.AuctionQuerySession querySession) {
        nullarg(querySession, "auction query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.session.getAuctionHouseId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getAuctionHouse());
    }


    /**
     *  Tests if this user can perform <code>Auction</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctions() {
        return (this.session.canSearchAuctions());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auctions in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        this.session.useFederatedAuctionHouseView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        this.session.useIsolatedAuctionHouseView();
        return;
    }
    

    /**
     *  Only active auctions are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive auctions are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>Auction</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Auction</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Auction</code> and
     *  retained for compatibility.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  auctionId <code>Id</code> of the
     *          <code>Auction</code>
     *  @return the auction
     *  @throws org.osid.NotFoundException <code>auctionId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.Auction getAuction(org.osid.id.Id auctionId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        query.matchId(auctionId, true);
        org.osid.bidding.AuctionList auctions = this.session.getAuctionsByQuery(query);
        if (auctions.hasNext()) {
            return (auctions.getNextAuction());
        } 
        
        throw new org.osid.NotFoundException(auctionId + " not found");
    }


    /**
     *  Gets an <code>AuctionList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  auctions specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Auctions</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  auctionIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByIds(org.osid.id.IdList auctionIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();

        try (org.osid.id.IdList ids = auctionIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getAuctionsByQuery(query));
    }


    /**
     *  Gets an <code>AuctionList</code> corresponding to the given
     *  auction genus <code>Type</code> which does not include
     *  auctions of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  auctionGenusType an auction genus type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByGenusType(org.osid.type.Type auctionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        query.matchGenusType(auctionGenusType, true);
        return (this.session.getAuctionsByQuery(query));
    }


    /**
     *  Gets an <code>AuctionList</code> corresponding to the given
     *  auction genus <code>Type</code> and include any additional
     *  auctions with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  auctionGenusType an auction genus type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByParentGenusType(org.osid.type.Type auctionGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        query.matchParentGenusType(auctionGenusType, true);
        return (this.session.getAuctionsByQuery(query));
    }


    /**
     *  Gets an <code>AuctionList</code> containing the given
     *  auction record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  auctionRecordType an auction record type 
     *  @return the returned <code>Auction</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByRecordType(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        query.matchRecordType(auctionRecordType, true);
        return (this.session.getAuctionsByQuery(query));
    }


    /**
     *  Gets an <code>AuctionList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known auctions or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  auctions that are accessible through this session. 
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Auction</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        query.matchProviderId(resourceId, true);
        return (this.session.getAuctionsByQuery(query));        
    }


    /**
     *  Gets an <code>AuctionList</code> effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible
     *  through this session.
     *  
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Auction</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getAuctionsByQuery(query));
    }
        

    /**
     *  Gets a list of auctions for an item. <code> </code> 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *  
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  itemId a resource <code> Id </code> 
     *  @return the returned <code> Auction </code> list 
     *  @throws org.osid.NullArgumentException <code> itemId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItem(org.osid.id.Id itemId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        query.matchItemId(itemId, true);
        return (this.session.getAuctionsByQuery(query));        
    }


    /**
     *  Gets a list of auctions for an item genus type. 
     *  
     *  In plenary mode, the returned list contains all known auctions
     *  or an error results. Otherwise, the returned list may contain
     *  only those auctions that are accessible through this session.
     *  
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions are
     *  returned.
     *
     *  @param  itemGenusType an item genus type 
     *  @return the returned <code> Auction </code> list 
     *  @throws org.osid.NullArgumentException <code> itemGenusType </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItemGenusType(org.osid.type.Type itemGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        if (query.supportsItemQuery()) {
            query.getItemQuery().matchGenusType(itemGenusType, true);
            return (this.session.getAuctionsByQuery(query));
        }

        return (super.getAuctionsByItemGenusType(itemGenusType));
    }


    /**
     *  Gets an <code> AuctionList </code> for an item genus type and 
     *  effective during the entire given date range inclusive but not 
     *  confined to the date range. 
     *  
     *  In plenary mode, the returned list contains all known auctions or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  auctions that are accessible through this session. 
     *  
     *  In active mode, auctions are returned that are currently active. In 
     *  any status mode, active and inactive auctions are returned. 
     *
     *  @param  itemGenusType an item genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code> Auction </code> list 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     *  @throws org.osid.NullArgumentException <code> itemGenusType, from, 
     *          </code> or <code> to </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctionsByItemGenusTypeOnDate(org.osid.type.Type itemGenusType, 
                                                                         org.osid.calendaring.DateTime from, 
                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        if (query.supportsItemQuery()) {
            query.getItemQuery().matchGenusType(itemGenusType, true);
            query.matchDate(from, to, true);
            return (this.session.getAuctionsByQuery(query));
        }

        return (super.getAuctionsByItemGenusTypeOnDate(itemGenusType, from, to));
    }


    /**
     *  Gets all <code>Auctions</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  auctions or an error results. Otherwise, the returned list
     *  may contain only those auctions that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, auctions are returned that are currently
     *  active. In any status mode, active and inactive auctions
     *  are returned.
     *
     *  @return a list of <code>Auctions</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionList getAuctions()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.bidding.AuctionQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getAuctionsByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.bidding.AuctionQuery getQuery() {
        org.osid.bidding.AuctionQuery query = this.session.getAuctionQuery();
        
        if (isActiveOnly()) {
            query.matchActive(true);
        }

        return (query);
    }
}
