//
// AbstractIndexedMapActionGroupLookupSession.java
//
//    A simple framework for providing an ActionGroup lookup service
//    backed by a fixed collection of action groups with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of an ActionGroup lookup service backed by a
 *  fixed collection of action groups. The action groups are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some action groups may be compatible
 *  with more types than are indicated through these action group
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ActionGroups</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapActionGroupLookupSession
    extends AbstractMapActionGroupLookupSession
    implements org.osid.control.ActionGroupLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.ActionGroup> actionGroupsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.ActionGroup>());
    private final MultiMap<org.osid.type.Type, org.osid.control.ActionGroup> actionGroupsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.ActionGroup>());


    /**
     *  Makes an <code>ActionGroup</code> available in this session.
     *
     *  @param  actionGroup an action group
     *  @throws org.osid.NullArgumentException <code>actionGroup<code> is
     *          <code>null</code>
     */

    @Override
    protected void putActionGroup(org.osid.control.ActionGroup actionGroup) {
        super.putActionGroup(actionGroup);

        this.actionGroupsByGenus.put(actionGroup.getGenusType(), actionGroup);
        
        try (org.osid.type.TypeList types = actionGroup.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.actionGroupsByRecord.put(types.getNextType(), actionGroup);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of action groups available in this session.
     *
     *  @param  actionGroups an array of action groups
     *  @throws org.osid.NullArgumentException <code>actionGroups<code>
     *          is <code>null</code>
     */

    @Override
    protected void putActionGroups(org.osid.control.ActionGroup[] actionGroups) {
        for (org.osid.control.ActionGroup actionGroup : actionGroups) {
            putActionGroup(actionGroup);
        }

        return;
    }


    /**
     *  Makes a collection of action groups available in this session.
     *
     *  @param  actionGroups a collection of action groups
     *  @throws org.osid.NullArgumentException <code>actionGroups<code>
     *          is <code>null</code>
     */

    @Override
    protected void putActionGroups(java.util.Collection<? extends org.osid.control.ActionGroup> actionGroups) {
        for (org.osid.control.ActionGroup actionGroup : actionGroups) {
            putActionGroup(actionGroup);
        }

        return;
    }


    /**
     *  Removes an action group from this session.
     *
     *  @param actionGroupId the <code>Id</code> of the action group
     *  @throws org.osid.NullArgumentException <code>actionGroupId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeActionGroup(org.osid.id.Id actionGroupId) {
        org.osid.control.ActionGroup actionGroup;
        try {
            actionGroup = getActionGroup(actionGroupId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.actionGroupsByGenus.remove(actionGroup.getGenusType());

        try (org.osid.type.TypeList types = actionGroup.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.actionGroupsByRecord.remove(types.getNextType(), actionGroup);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeActionGroup(actionGroupId);
        return;
    }


    /**
     *  Gets an <code>ActionGroupList</code> corresponding to the given
     *  action group genus <code>Type</code> which does not include
     *  action groups of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known action groups or an error results. Otherwise,
     *  the returned list may contain only those action groups that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  actionGroupGenusType an action group genus type 
     *  @return the returned <code>ActionGroup</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByGenusType(org.osid.type.Type actionGroupGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.actiongroup.ArrayActionGroupList(this.actionGroupsByGenus.get(actionGroupGenusType)));
    }


    /**
     *  Gets an <code>ActionGroupList</code> containing the given
     *  action group record <code>Type</code>. In plenary mode, the
     *  returned list contains all known action groups or an error
     *  results. Otherwise, the returned list may contain only those
     *  action groups that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  actionGroupRecordType an action group record type 
     *  @return the returned <code>actionGroup</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroupRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.ActionGroupList getActionGroupsByRecordType(org.osid.type.Type actionGroupRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.actiongroup.ArrayActionGroupList(this.actionGroupsByRecord.get(actionGroupRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.actionGroupsByGenus.clear();
        this.actionGroupsByRecord.clear();

        super.close();

        return;
    }
}
