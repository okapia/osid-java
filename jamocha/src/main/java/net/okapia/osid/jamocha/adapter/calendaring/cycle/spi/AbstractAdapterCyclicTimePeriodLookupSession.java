//
// AbstractAdapterCyclicTimePeriodLookupSession.java
//
//    A CyclicTimePeriod lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A CyclicTimePeriod lookup session adapter.
 */

public abstract class AbstractAdapterCyclicTimePeriodLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.calendaring.cycle.CyclicTimePeriodLookupSession {

    private final org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterCyclicTimePeriodLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterCyclicTimePeriodLookupSession(org.osid.calendaring.cycle.CyclicTimePeriodLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Calendar/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Calendar Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCalendarId() {
        return (this.session.getCalendarId());
    }


    /**
     *  Gets the {@code Calendar} associated with this session.
     *
     *  @return the {@code Calendar} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.Calendar getCalendar()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getCalendar());
    }


    /**
     *  Tests if this user can perform {@code CyclicTimePeriod} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupCyclicTimePeriods() {
        return (this.session.canLookupCyclicTimePeriods());
    }


    /**
     *  A complete view of the {@code CyclicTimePeriod} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeCyclicTimePeriodView() {
        this.session.useComparativeCyclicTimePeriodView();
        return;
    }


    /**
     *  A complete view of the {@code CyclicTimePeriod} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryCyclicTimePeriodView() {
        this.session.usePlenaryCyclicTimePeriodView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include cyclic time periods in calendars which are children
     *  of this calendar in the calendar hierarchy.
     */

    @OSID @Override
    public void useFederatedCalendarView() {
        this.session.useFederatedCalendarView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this calendar only.
     */

    @OSID @Override
    public void useIsolatedCalendarView() {
        this.session.useIsolatedCalendarView();
        return;
    }
    
     
    /**
     *  Gets the {@code CyclicTimePeriod} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code CyclicTimePeriod} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code CyclicTimePeriod} and
     *  retained for compatibility.
     *
     *  @param cyclicTimePeriodId {@code Id} of the {@code CyclicTimePeriod}
     *  @return the cyclic time period
     *  @throws org.osid.NotFoundException {@code cyclicTimePeriodId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code cyclicTimePeriodId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriod getCyclicTimePeriod(org.osid.id.Id cyclicTimePeriodId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicTimePeriod(cyclicTimePeriodId));
    }


    /**
     *  Gets a {@code CyclicTimePeriodList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  cyclicTimePeriods specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code CyclicTimePeriods} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  @param  cyclicTimePeriodIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code CyclicTimePeriod} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code cyclicTimePeriodIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByIds(org.osid.id.IdList cyclicTimePeriodIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicTimePeriodsByIds(cyclicTimePeriodIds));
    }


    /**
     *  Gets a {@code CyclicTimePeriodList} corresponding to the given
     *  cyclic time period genus {@code Type} which does not include
     *  cyclic time periods of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicTimePeriodGenusType a cyclicTimePeriod genus type 
     *  @return the returned {@code CyclicTimePeriod} list
     *  @throws org.osid.NullArgumentException
     *          {@code cyclicTimePeriodGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByGenusType(org.osid.type.Type cyclicTimePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicTimePeriodsByGenusType(cyclicTimePeriodGenusType));
    }


    /**
     *  Gets a {@code CyclicTimePeriodList} corresponding to the given
     *  cyclic time period genus {@code Type} and include any additional
     *  cyclic time periods with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicTimePeriodGenusType a cyclicTimePeriod genus type 
     *  @return the returned {@code CyclicTimePeriod} list
     *  @throws org.osid.NullArgumentException
     *          {@code cyclicTimePeriodGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByParentGenusType(org.osid.type.Type cyclicTimePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicTimePeriodsByParentGenusType(cyclicTimePeriodGenusType));
    }


    /**
     *  Gets a {@code CyclicTimePeriodList} containing the given
     *  cyclic time period record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  cyclicTimePeriodRecordType a cyclicTimePeriod record type 
     *  @return the returned {@code CyclicTimePeriod} list
     *  @throws org.osid.NullArgumentException
     *          {@code cyclicTimePeriodRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByRecordType(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicTimePeriodsByRecordType(cyclicTimePeriodRecordType));
    }


    /**
     *  Gets all {@code CyclicTimePeriods}. 
     *
     *  In plenary mode, the returned list contains all known
     *  cyclic time periods or an error results. Otherwise, the returned list
     *  may contain only those cyclic time periods that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of {@code CyclicTimePeriods} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriods()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getCyclicTimePeriods());
    }
}
