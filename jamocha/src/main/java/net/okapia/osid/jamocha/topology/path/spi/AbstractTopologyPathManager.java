//
// AbstractTopologyPathManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.topology.path.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractTopologyPathManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.topology.path.TopologyPathManager,
               org.osid.topology.path.TopologyPathProxyManager {

    private final Types pathRecordTypes                    = new TypeRefSet();
    private final Types pathSearchRecordTypes              = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractTopologyPathManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractTopologyPathManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any graph federation is exposed. Federation is exposed when a 
     *  specific graph may be identified, selected and used to create a lookup 
     *  or admin session. Federation is not exposed when a set of graphs 
     *  appears as a single graph. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up paths is supported. 
     *
     *  @return <code> true </code> if path lookup is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathLookup() {
        return (false);
    }


    /**
     *  Tests if querying paths is supported. 
     *
     *  @return <code> true </code> if path query is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathQuery() {
        return (false);
    }


    /**
     *  Tests if searching paths is supported. 
     *
     *  @return <code> true </code> if path search is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSearch() {
        return (false);
    }


    /**
     *  Tests if path <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if path administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathAdmin() {
        return (false);
    }


    /**
     *  Tests if a path <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if path notification is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathNotification() {
        return (false);
    }


    /**
     *  Tests if a path graph lookup service is supported. 
     *
     *  @return <code> true </code> if a path graph lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathGraph() {
        return (false);
    }


    /**
     *  Tests if a path graph service is supported. 
     *
     *  @return <code> true </code> if path to graph assignment service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathGraphAssignment() {
        return (false);
    }


    /**
     *  Tests if a path smart graph lookup service is supported. 
     *
     *  @return <code> true </code> if a path smart graph service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPathSmartGraph() {
        return (false);
    }


    /**
     *  Gets the supported <code> Path </code> record types. 
     *
     *  @return a list containing the supported <code> Path </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPathRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.pathRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Path </code> record type is supported. 
     *
     *  @param  pathRecordType a <code> Type </code> indicating a <code> Path 
     *          </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pathRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPathRecordType(org.osid.type.Type pathRecordType) {
        return (this.pathRecordTypes.contains(pathRecordType));
    }


    /**
     *  Adds support for a path record type.
     *
     *  @param pathRecordType a path record type
     *  @throws org.osid.NullArgumentException
     *  <code>pathRecordType</code> is <code>null</code>
     */

    protected void addPathRecordType(org.osid.type.Type pathRecordType) {
        this.pathRecordTypes.add(pathRecordType);
        return;
    }


    /**
     *  Removes support for a path record type.
     *
     *  @param pathRecordType a path record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>pathRecordType</code> is <code>null</code>
     */

    protected void removePathRecordType(org.osid.type.Type pathRecordType) {
        this.pathRecordTypes.remove(pathRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Path </code> search types. 
     *
     *  @return a list containing the supported <code> Path </code> search 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPathSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.pathSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Path </code> search type is supported. 
     *
     *  @param  pathSearchRecordType a <code> Type </code> indicating a <code> 
     *          Path </code> search type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> pathSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPathSearchRecordType(org.osid.type.Type pathSearchRecordType) {
        return (this.pathSearchRecordTypes.contains(pathSearchRecordType));
    }


    /**
     *  Adds support for a path search record type.
     *
     *  @param pathSearchRecordType a path search record type
     *  @throws org.osid.NullArgumentException
     *  <code>pathSearchRecordType</code> is <code>null</code>
     */

    protected void addPathSearchRecordType(org.osid.type.Type pathSearchRecordType) {
        this.pathSearchRecordTypes.add(pathSearchRecordType);
        return;
    }


    /**
     *  Removes support for a path search record type.
     *
     *  @param pathSearchRecordType a path search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>pathSearchRecordType</code> is <code>null</code>
     */

    protected void removePathSearchRecordType(org.osid.type.Type pathSearchRecordType) {
        this.pathSearchRecordTypes.remove(pathSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service. 
     *
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathLookupSession getPathLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathLookupSession getPathLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Graph </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathLookupSession getPathLookupSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path lookup 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathLookupSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given Id 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathLookupSession getPathLookupSessionForGraph(org.osid.id.Id graphId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathLookupSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service. 
     *
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathQuerySession getPathQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathQuerySession getPathQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathQuerySession getPathQuerySessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathQuerySessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path query 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathQuerySession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given Id 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathQuerySession getPathQuerySessionForGraph(org.osid.id.Id graphId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathQuerySessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service. 
     *
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathSearchSession getPathSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathSearchSession getPathSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathSearchSession getPathSearchSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathSearchSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path search 
     *  service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSearchSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given Id 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathSearchSession getPathSearchSessionForGraph(org.osid.id.Id graphId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathSearchSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service. 
     *
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathAdminSession getPathAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathAdminSession getPathAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> graph </code> 
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathAdminSession getPathAdminSessionForGraph(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  administration service for the given graph. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathAdminSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> graphId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathAdminSession getPathAdminSessionForGraph(org.osid.id.Id graphId, 
                                                                               org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathAdminSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service. 
     *
     *  @param  pathReceiver the notification callback 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathNotificationSession getPathNotificationSession(org.osid.topology.path.PathReceiver pathReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service. 
     *
     *  @param  pathReceiver the notification callback 
     *  @param  proxy a proxy 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathNotificationSession getPathNotificationSession(org.osid.topology.path.PathReceiver pathReceiver, 
                                                                                     org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service for the given graph. 
     *
     *  @param  pathReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> or 
     *          <code> graphId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathNotificationSession getPathNotificationSessionForGraph(org.osid.topology.path.PathReceiver pathReceiver, 
                                                                                             org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathNotificationSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the path 
     *  notification service for the given graph. 
     *
     *  @param  pathReceiver the notification callback 
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathNotificationSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver, graphId 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathNotificationSession getPathNotificationSessionForGraph(org.osid.topology.path.PathReceiver pathReceiver, 
                                                                                             org.osid.id.Id graphId, 
                                                                                             org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathNotificationSessionForGraph not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup path/graph locations. 
     *
     *  @return a <code> PathGraphSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathGraph() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathGraphSession getPathGraphSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup path/graph mappings. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathGraphSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsPathGraph() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathGraphSession getPathGraphSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning paths to 
     *  graphs. 
     *
     *  @return a <code> PathGraphAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathGraphAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathGraphAssignmentSession getPathGraphAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathGraphAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning paths to 
     *  maps. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> PathGraphAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathGraphAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathGraphAssignmentSession getPathGraphAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathGraphAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage path smart graphs. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @return a <code> PathSmartGraphSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> pathReceiver </code> or 
     *          <code> graphId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSmartGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathSmartGraphSession getPathSmartGraphSession(org.osid.id.Id graphId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathManager.getPathSmartGraphSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to manage path smart graphs. 
     *
     *  @param  graphId the <code> Id </code> of the <code> Graph </code> 
     *  @param  proxy a proxy 
     *  @return a <code> PathSmartMGraphSession </code> 
     *  @throws org.osid.NotFoundException no graph found by the given <code> 
     *          Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsPathSmartGraph() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.topology.path.PathSmartGraphSession getPathSmartGraphSession(org.osid.id.Id graphId, 
                                                                                 org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.topology.path.TopologyPathProxyManager.getPathSmartGraphSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.pathRecordTypes.clear();
        this.pathRecordTypes.clear();

        this.pathSearchRecordTypes.clear();
        this.pathSearchRecordTypes.clear();

        return;
    }
}
