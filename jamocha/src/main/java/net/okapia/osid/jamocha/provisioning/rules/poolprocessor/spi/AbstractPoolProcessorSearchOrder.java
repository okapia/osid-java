//
// AbstractPoolProcessorSearchOdrer.java
//
//     Defines a PoolProcessorSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.poolprocessor.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code PoolProcessorSearchOrder}.
 */

public abstract class AbstractPoolProcessorSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorSearchOrder
    implements org.osid.provisioning.rules.PoolProcessorSearchOrder {

    private final java.util.Collection<org.osid.provisioning.rules.records.PoolProcessorSearchOrderRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Orders the results by allocates by least use. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatesByLeastUse(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by allocates by most use. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatesByMostUse(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by allocates by least cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatesByLeastCost(org.osid.SearchOrderStyle style) {
        return;
    }


    /**
     *  Orders the results by allocates by most cost. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByAllocatesByMostCost(org.osid.SearchOrderStyle style) {
        return;
    }



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  poolProcessorRecordType a pool processor record type 
     *  @return {@code true} if the poolProcessorRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type poolProcessorRecordType) {
        for (org.osid.provisioning.rules.records.PoolProcessorSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  poolProcessorRecordType the pool processor record type 
     *  @return the pool processor search order record
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(poolProcessorRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.PoolProcessorSearchOrderRecord getPoolProcessorSearchOrderRecord(org.osid.type.Type poolProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.provisioning.rules.records.PoolProcessorSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(poolProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(poolProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this pool processor. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param poolProcessorRecord the pool processor search odrer record
     *  @param poolProcessorRecordType pool processor record type
     *  @throws org.osid.NullArgumentException
     *          {@code poolProcessorRecord} or
     *          {@code poolProcessorRecordTypepoolProcessor} is
     *          {@code null}
     */
            
    protected void addPoolProcessorRecord(org.osid.provisioning.rules.records.PoolProcessorSearchOrderRecord poolProcessorSearchOrderRecord, 
                                     org.osid.type.Type poolProcessorRecordType) {

        addRecordType(poolProcessorRecordType);
        this.records.add(poolProcessorSearchOrderRecord);
        
        return;
    }
}
