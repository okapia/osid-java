//
// AbstractMessagingBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.messaging.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractMessagingBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.messaging.batch.MessagingBatchManager,
               org.osid.messaging.batch.MessagingBatchProxyManager {


    /**
     *  Constructs a new <code>AbstractMessagingBatchManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractMessagingBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of messages is available. 
     *
     *  @return <code> true </code> if a message bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of receipts is available. 
     *
     *  @return <code> true </code> if a receipt bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of mailboxes is available. 
     *
     *  @return <code> true </code> if a mailbox bulk administrative service 
     *          is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk message 
     *  administration service. 
     *
     *  @return a <code> MessageBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessageBatchAdminSession getMessageBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchManager.getMessageBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk message 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MessageBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessageBatchAdminSession getMessageBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchProxyManager.getMessageBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk message 
     *  administration service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the <code> Mailbox </code> 
     *  @return a <code> MessageBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Mailbox </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessageBatchAdminSession getMessageBatchAdminSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchManager.getMessageBatchAdminSessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk message 
     *  administration service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the <code> Mailbox </code> 
     *  @param  proxy a proxy 
     *  @return a <code> MessageBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Mailbox </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessageBatchAdminSession getMessageBatchAdminSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchProxyManager.getMessageBatchAdminSessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk receipt 
     *  administration service. 
     *
     *  @return a <code> ReceiptBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.ReceiptBatchAdminSession getReceiptBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchManager.getReceiptBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk receipt 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> ReceiptBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.ReceiptBatchAdminSession getReceiptBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchProxyManager.getReceiptBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk receipt 
     *  administration service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the <code> Mailbox </code> 
     *  @return a <code> ReceiptBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Mailbox </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.ReceiptBatchAdminSession getReceiptBatchAdminSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchManager.getReceiptBatchAdminSessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk receipt 
     *  administration service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the <code> Mailbox </code> 
     *  @param  proxy a proxy 
     *  @return a <code> ReceiptBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Mailbox </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.ReceiptBatchAdminSession getReceiptBatchAdminSessionForMailbox(org.osid.id.Id mailboxId, 
                                                                                                   org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchProxyManager.getReceiptBatchAdminSessionForMailbox not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk mailbox 
     *  administration service. 
     *
     *  @return a <code> MailboxBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MailboxBatchAdminSession getMailboxBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchManager.getMailboxBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk mailbox 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> MailboxBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxBatchAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MailboxBatchAdminSession getMailboxBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.messaging.batch.MessagingBatchProxyManager.getMailboxBatchAdminSession not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
