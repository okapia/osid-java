//
// AbstractBuildingLookupSession.java
//
//    A starter implementation framework for providing a Building
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a Building lookup
 *  service.
 *
 *  Although this abstract class requires only the implementation of
 *  getBuildings(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractBuildingLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.room.BuildingLookupSession {

    private boolean pedantic      = false;
    private boolean effectiveonly = false;
    private boolean federated     = false;
    private org.osid.room.Campus campus = new net.okapia.osid.jamocha.nil.room.campus.UnknownCampus();
    

    /**
     *  Gets the <code>Campus/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Campus Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCampusId() {
        return (this.campus.getId());
    }


    /**
     *  Gets the <code>Campus</code> associated with this session.
     *
     *  @return the <code>Campus</code> associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Campus getCampus()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.campus);
    }


    /**
     *  Sets the <code>Campus</code>.
     *
     *  @param  campus the campus for this session
     *  @throws org.osid.NullArgumentException <code>campus</code>
     *          is <code>null</code>
     */

    protected void setCampus(org.osid.room.Campus campus) {
        nullarg(campus, "campus");
        this.campus = campus;
        return;
    }


    /**
     *  Tests if this user can perform <code>Building</code> lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupBuildings() {
        return (true);
    }


    /**
     *  A complete view of the <code>Building</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeBuildingView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>Building</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryBuildingView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include buildings in campuses which are children of
     *  this campus in the campus hierarchy.
     */

    @OSID @Override
    public void useFederatedCampusView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this campus only.
     */

    @OSID @Override
    public void useIsolatedCampusView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only buildings whose effective dates are current are returned
     *  by methods in this session.
     */

    @OSID @Override
    public void useEffectiveBuildingView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All buildings of any effective dates are returned by all
     *  methods in this session.
     */

    @OSID @Override
    public void useAnyEffectiveBuildingView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Building</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Building</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Building</code> and
     *  retained for compatibility.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @param  buildingId <code>Id</code> of the
     *          <code>Building</code>
     *  @return the building
     *  @throws org.osid.NotFoundException <code>buildingId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>buildingId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Building getBuilding(org.osid.id.Id buildingId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.room.BuildingList buildings = getBuildings()) {
            while (buildings.hasNext()) {
                org.osid.room.Building building = buildings.getNextBuilding();
                if (building.getId().equals(buildingId)) {
                    return (building);
                }
            }
        } 

        throw new org.osid.NotFoundException(buildingId + " not found");
    }


    /**
     *  Gets a <code>BuildingList</code> corresponding to the given
     *  <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  buildings specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Buildings</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getBuildings()</code>.
     *
     *  @param buildingIds the list of <code>Ids</code> to retrieve
     *  @return the returned <code>Building</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>buildingIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByIds(org.osid.id.IdList buildingIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.room.Building> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = buildingIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getBuilding(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("building " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.room.building.LinkedBuildingList(ret));
    }


    /**
     *  Gets a <code>BuildingList</code> corresponding to the given
     *  building genus <code>Type</code> which does not include
     *  buildings of types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getBuildings()</code>.
     *
     *  @param buildingGenusType a building genus type
     *  @return the returned <code>Building</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>buildingGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByGenusType(org.osid.type.Type buildingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.building.BuildingGenusFilterList(getBuildings(), buildingGenusType));
    }


    /**
     *  Gets a <code>BuildingList</code> corresponding to the given
     *  building genus <code>Type</code> and include any additional
     *  buildings with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBuildings()</code>.
     *
     *  @param buildingGenusType a building genus type
     *  @return the returned <code>Building</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>buildingGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByParentGenusType(org.osid.type.Type buildingGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getBuildingsByGenusType(buildingGenusType));
    }


    /**
     *  Gets a <code>BuildingList</code> containing the given building
     *  record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getBuildings()</code>.
     *
     *  @param buildingRecordType a building record type
     *  @return the returned <code>Building</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>buildingRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByRecordType(org.osid.type.Type buildingRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.building.BuildingRecordFilterList(getBuildings(), buildingRecordType));
    }


    /**
     *  Gets a <code>BuildingList</code> effective during the entire
     *  given date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session.
     *  
     *  In active mode, buildings are returned that are currently
     *  active. In any status mode, active and inactive buildings are
     *  returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Building</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.room.BuildingList getBuildingsOnDate(org.osid.calendaring.DateTime from, 
                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.building.TemporalBuildingFilterList(getBuildings(), from, to));
    }
        

    /**
     *  Gets a <code>BuildingList</code> containing of the given
     *  building number.
     *  
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session.
     *  
     *  In effective mode, buildings are returned that are currently
     *  effective. In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @param  number a building number 
     *  @return the returned <code>Building</code> list 
     *  @throws org.osid.NullArgumentException <code>number</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.room.BuildingList getBuildingsByNumber(String number)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.room.building.BuildingFilterList(new NumberFilter(number), getBuildings()));
    }


    /**
     *  Gets all <code>Buildings</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  buildings or an error results. Otherwise, the returned list
     *  may contain only those buildings that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, buildings are returned that are currently
     *  effective.  In any effective mode, effective buildings and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Buildings</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.room.BuildingList getBuildings()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the building list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of buildings
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.room.BuildingList filterBuildingsOnViews(org.osid.room.BuildingList list)
        throws org.osid.OperationFailedException {

        org.osid.room.BuildingList ret = list;

        if (isEffectiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.room.building.EffectiveBuildingFilterList(ret);
        }

        return (ret);
    }


    public static class NumberFilter
        implements net.okapia.osid.jamocha.inline.filter.room.building.BuildingFilter {
         
        private final String number;
         
         
        /**
         *  Constructs a new <code>NumberFilter</code>.
         *
         *  @param number the number to filter
         *  @throws org.osid.NullArgumentException
         *          <code>number</code> is <code>null</code>
         */
        
        public NumberFilter(String number) {
            nullarg(number, "number");
            this.number = number;
            return;
        }

         
        /**
         *  Used by the BuildingFilterList to filter the 
         *  building list based on number.
         *
         *  @param building the building
         *  @return <code>true</code> to pass the building,
         *          <code>false</code> to filter it
         */
        
        @Override
        public boolean pass(org.osid.room.Building building) {
            return (building.getNumber().equals(this.number));
        }
    }    
}
