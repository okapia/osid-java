//
// AbstractForumSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.forum.forum.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractForumSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.forum.ForumSearchResults {

    private org.osid.forum.ForumList forums;
    private final org.osid.forum.ForumQueryInspector inspector;
    private final java.util.Collection<org.osid.forum.records.ForumSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractForumSearchResults.
     *
     *  @param forums the result set
     *  @param forumQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>forums</code>
     *          or <code>forumQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractForumSearchResults(org.osid.forum.ForumList forums,
                                            org.osid.forum.ForumQueryInspector forumQueryInspector) {
        nullarg(forums, "forums");
        nullarg(forumQueryInspector, "forum query inspectpr");

        this.forums = forums;
        this.inspector = forumQueryInspector;

        return;
    }


    /**
     *  Gets the forum list resulting from a search.
     *
     *  @return a forum list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.forum.ForumList getForums() {
        if (this.forums == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.forum.ForumList forums = this.forums;
        this.forums = null;
	return (forums);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.forum.ForumQueryInspector getForumQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  forum search record <code> Type. </code> This method must
     *  be used to retrieve a forum implementing the requested
     *  record.
     *
     *  @param forumSearchRecordType a forum search 
     *         record type 
     *  @return the forum search
     *  @throws org.osid.NullArgumentException
     *          <code>forumSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(forumSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.forum.records.ForumSearchResultsRecord getForumSearchResultsRecord(org.osid.type.Type forumSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.forum.records.ForumSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(forumSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(forumSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record forum search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addForumRecord(org.osid.forum.records.ForumSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "forum record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
