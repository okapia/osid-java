//
// AbstractCourseProgramManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.course.program.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCourseProgramManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.course.program.CourseProgramManager,
               org.osid.course.program.CourseProgramProxyManager {

    private final Types programRecordTypes                 = new TypeRefSet();
    private final Types programSearchRecordTypes           = new TypeRefSet();

    private final Types programOfferingRecordTypes         = new TypeRefSet();
    private final Types programOfferingSearchRecordTypes   = new TypeRefSet();

    private final Types credentialRecordTypes              = new TypeRefSet();
    private final Types credentialSearchRecordTypes        = new TypeRefSet();

    private final Types enrollmentRecordTypes              = new TypeRefSet();
    private final Types enrollmentSearchRecordTypes        = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractCourseProgramManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCourseProgramManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any course catalog federation is exposed. Federation is 
     *  exposed when a specific course catalog may be identified, selected and 
     *  used to create a lookup or admin session. Federation is not exposed 
     *  when a set of catalogs appears as a single catalog. 
     *
     *  @return <code> true </code> if visible federation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if looking up programs is supported. 
     *
     *  @return <code> true </code> if program lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramLookup() {
        return (false);
    }


    /**
     *  Tests if querying programs is supported. 
     *
     *  @return <code> true </code> if program query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramQuery() {
        return (false);
    }


    /**
     *  Tests if searching programs is supported. 
     *
     *  @return <code> true </code> if program search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSearch() {
        return (false);
    }


    /**
     *  Tests if program administrative service is supported. 
     *
     *  @return <code> true </code> if program administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramAdmin() {
        return (false);
    }


    /**
     *  Tests if a program <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if program notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramNotification() {
        return (false);
    }


    /**
     *  Tests if a program cataloging service is supported. 
     *
     *  @return <code> true </code> if program cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps programs to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a program smart course catalog session is available. 
     *
     *  @return <code> true </code> if a program smart course catalog session 
     *          is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course/program lookup service is supported. 
     *
     *  @return <code> true </code> if course/program lookup service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProgramLookup() {
        return (false);
    }


    /**
     *  Tests if a course/program mapping service is supported. 
     *
     *  @return <code> true </code> if course/program mapping service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProgramAssignment() {
        return (false);
    }


    /**
     *  Tests if looking up program offerings is supported. 
     *
     *  @return <code> true </code> if program offering lookup is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingLookup() {
        return (false);
    }


    /**
     *  Tests if querying program offerings is supported. 
     *
     *  @return <code> true </code> if program offering query is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingQuery() {
        return (false);
    }


    /**
     *  Tests if searching program offerings is supported. 
     *
     *  @return <code> true </code> if program offering search is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingSearch() {
        return (false);
    }


    /**
     *  Tests if course <code> </code> offering <code> </code> administrative 
     *  service is supported. 
     *
     *  @return <code> true </code> if program offering administration is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingAdmin() {
        return (false);
    }


    /**
     *  Tests if a program offering <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if program offering notification is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingNotification() {
        return (false);
    }


    /**
     *  Tests if a program offering cataloging service is supported. 
     *
     *  @return <code> true </code> if program offering catalog is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a program offering cataloging service is supported. A 
     *  cataloging service maps program offerings to catalogs. 
     *
     *  @return <code> true </code> if program offering cataloging is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a program offering smart course catalog session is available. 
     *
     *  @return <code> true </code> if a program offering smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProgramOfferingSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up courses is supported. 
     *
     *  @return <code> true </code> if course lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialLookup() {
        return (false);
    }


    /**
     *  Tests if querying courses is supported. 
     *
     *  @return <code> true </code> if credential query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialQuery() {
        return (false);
    }


    /**
     *  Tests if searching courses is supported. 
     *
     *  @return <code> true </code> if credential search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialSearch() {
        return (false);
    }


    /**
     *  Tests if course <code> </code> administrative service is supported. 
     *
     *  @return <code> true </code> if course administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialAdmin() {
        return (false);
    }


    /**
     *  Tests if a course <code> </code> notification service is supported. 
     *
     *  @return <code> true </code> if course notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialNotification() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course cataloging service is supported. A course cataloging 
     *  service maps courses to catalogs. 
     *
     *  @return <code> true </code> if course cataloging is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if a credential smart course catalog session is available. 
     *
     *  @return <code> true </code> if a credential smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCredentialSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if looking up enrollments is supported. 
     *
     *  @return <code> true </code> if enrollment lookup is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentLookup() {
        return (false);
    }


    /**
     *  Tests if querying enrollments is supported. 
     *
     *  @return <code> true </code> if enrollment query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentQuery() {
        return (false);
    }


    /**
     *  Tests if searching enrollments is supported. 
     *
     *  @return <code> true </code> if enrollment search is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentSearch() {
        return (false);
    }


    /**
     *  Tests if an enrollment <code> </code> administrative service is 
     *  supported. 
     *
     *  @return <code> true </code> if enrollment administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentAdmin() {
        return (false);
    }


    /**
     *  Tests if an enrollment <code> </code> notification service is 
     *  supported. 
     *
     *  @return <code> true </code> if enrollment notification is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentNotification() {
        return (false);
    }


    /**
     *  Tests if an enrollment cataloging service is supported. 
     *
     *  @return <code> true </code> if enrollment catalog is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if an enrollment cataloging service is supported. A cataloging 
     *  service maps enrollments to catalogs. 
     *
     *  @return <code> true </code> if enrollment cataloging is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentCourseCatalogAssignment() {
        return (false);
    }


    /**
     *  Tests if an enrollment smart course catalog session is available. 
     *
     *  @return <code> true </code> if an enrollment smart course catalog 
     *          session is supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEnrollmentSmartCourseCatalog() {
        return (false);
    }


    /**
     *  Tests if a course program batch service is available. 
     *
     *  @return <code> true </code> if a course program batch service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCourseProgramBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> program </code> record types. 
     *
     *  @return a list containing the supported <code> Program </code> record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.programRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> program </code> record type is supported. 
     *
     *  @param  programRecordType a <code> Type </code> indicating a <code> 
     *          program </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> programRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramRecordType(org.osid.type.Type programRecordType) {
        return (this.programRecordTypes.contains(programRecordType));
    }


    /**
     *  Adds support for a program record type.
     *
     *  @param programRecordType a program record type
     *  @throws org.osid.NullArgumentException
     *  <code>programRecordType</code> is <code>null</code>
     */

    protected void addProgramRecordType(org.osid.type.Type programRecordType) {
        this.programRecordTypes.add(programRecordType);
        return;
    }


    /**
     *  Removes support for a program record type.
     *
     *  @param programRecordType a program record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>programRecordType</code> is <code>null</code>
     */

    protected void removeProgramRecordType(org.osid.type.Type programRecordType) {
        this.programRecordTypes.remove(programRecordType);
        return;
    }


    /**
     *  Gets the supported <code> program </code> search record types. 
     *
     *  @return a list containing the supported <code> program </code> search 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.programSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> program </code> search record type is 
     *  supported. 
     *
     *  @param  programSearchRecordType a <code> Type </code> indicating a 
     *          <code> Program </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> programSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramSearchRecordType(org.osid.type.Type programSearchRecordType) {
        return (this.programSearchRecordTypes.contains(programSearchRecordType));
    }


    /**
     *  Adds support for a program search record type.
     *
     *  @param programSearchRecordType a program search record type
     *  @throws org.osid.NullArgumentException
     *  <code>programSearchRecordType</code> is <code>null</code>
     */

    protected void addProgramSearchRecordType(org.osid.type.Type programSearchRecordType) {
        this.programSearchRecordTypes.add(programSearchRecordType);
        return;
    }


    /**
     *  Removes support for a program search record type.
     *
     *  @param programSearchRecordType a program search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>programSearchRecordType</code> is <code>null</code>
     */

    protected void removeProgramSearchRecordType(org.osid.type.Type programSearchRecordType) {
        this.programSearchRecordTypes.remove(programSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProgramOffering </code> record types. 
     *
     *  @return a list containing the supported <code> ProgramOffering </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramOfferingRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.programOfferingRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProgramOffering </code> record type is 
     *  supported. 
     *
     *  @param  programOfferingRecordType a <code> Type </code> indicating an 
     *          <code> ProgramOffering </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          programOfferingRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramOfferingRecordType(org.osid.type.Type programOfferingRecordType) {
        return (this.programOfferingRecordTypes.contains(programOfferingRecordType));
    }


    /**
     *  Adds support for a program offering record type.
     *
     *  @param programOfferingRecordType a program offering record type
     *  @throws org.osid.NullArgumentException
     *  <code>programOfferingRecordType</code> is <code>null</code>
     */

    protected void addProgramOfferingRecordType(org.osid.type.Type programOfferingRecordType) {
        this.programOfferingRecordTypes.add(programOfferingRecordType);
        return;
    }


    /**
     *  Removes support for a program offering record type.
     *
     *  @param programOfferingRecordType a program offering record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>programOfferingRecordType</code> is <code>null</code>
     */

    protected void removeProgramOfferingRecordType(org.osid.type.Type programOfferingRecordType) {
        this.programOfferingRecordTypes.remove(programOfferingRecordType);
        return;
    }


    /**
     *  Gets the supported <code> ProgramOffering </code> search record types. 
     *
     *  @return a list containing the supported <code> ProgramOffering </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getProgramOfferingSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.programOfferingSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> ProgramOffering </code> search record type 
     *  is supported. 
     *
     *  @param  programOfferingSearchRecordType a <code> Type </code> 
     *          indicating an <code> ProgramOffering </code> search record 
     *          type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          programOfferingSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsProgramOfferingSearchRecordType(org.osid.type.Type programOfferingSearchRecordType) {
        return (this.programOfferingSearchRecordTypes.contains(programOfferingSearchRecordType));
    }


    /**
     *  Adds support for a program offering search record type.
     *
     *  @param programOfferingSearchRecordType a program offering search record type
     *  @throws org.osid.NullArgumentException
     *  <code>programOfferingSearchRecordType</code> is <code>null</code>
     */

    protected void addProgramOfferingSearchRecordType(org.osid.type.Type programOfferingSearchRecordType) {
        this.programOfferingSearchRecordTypes.add(programOfferingSearchRecordType);
        return;
    }


    /**
     *  Removes support for a program offering search record type.
     *
     *  @param programOfferingSearchRecordType a program offering search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>programOfferingSearchRecordType</code> is <code>null</code>
     */

    protected void removeProgramOfferingSearchRecordType(org.osid.type.Type programOfferingSearchRecordType) {
        this.programOfferingSearchRecordTypes.remove(programOfferingSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> credential </code> record types. 
     *
     *  @return a list containing the supported <code> credential </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.credentialRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> credential </code> record type is supported. 
     *
     *  @param  credentialRecordType a <code> Type </code> indicating a <code> 
     *          credential </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> credentialRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialRecordType(org.osid.type.Type credentialRecordType) {
        return (this.credentialRecordTypes.contains(credentialRecordType));
    }


    /**
     *  Adds support for a credential record type.
     *
     *  @param credentialRecordType a credential record type
     *  @throws org.osid.NullArgumentException
     *  <code>credentialRecordType</code> is <code>null</code>
     */

    protected void addCredentialRecordType(org.osid.type.Type credentialRecordType) {
        this.credentialRecordTypes.add(credentialRecordType);
        return;
    }


    /**
     *  Removes support for a credential record type.
     *
     *  @param credentialRecordType a credential record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>credentialRecordType</code> is <code>null</code>
     */

    protected void removeCredentialRecordType(org.osid.type.Type credentialRecordType) {
        this.credentialRecordTypes.remove(credentialRecordType);
        return;
    }


    /**
     *  Gets the supported <code> credential </code> search record types. 
     *
     *  @return a list containing the supported <code> credential </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getCredentialSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.credentialSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> credential </code> search record type is 
     *  supported. 
     *
     *  @param  credentialSearchRecordType a <code> Type </code> indicating a 
     *          <code> credential </code> search record type 
     *  @return <code> true </code> if the given search record type is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          credentialSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsCredentialSearchRecordType(org.osid.type.Type credentialSearchRecordType) {
        return (this.credentialSearchRecordTypes.contains(credentialSearchRecordType));
    }


    /**
     *  Adds support for a credential search record type.
     *
     *  @param credentialSearchRecordType a credential search record type
     *  @throws org.osid.NullArgumentException
     *  <code>credentialSearchRecordType</code> is <code>null</code>
     */

    protected void addCredentialSearchRecordType(org.osid.type.Type credentialSearchRecordType) {
        this.credentialSearchRecordTypes.add(credentialSearchRecordType);
        return;
    }


    /**
     *  Removes support for a credential search record type.
     *
     *  @param credentialSearchRecordType a credential search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>credentialSearchRecordType</code> is <code>null</code>
     */

    protected void removeCredentialSearchRecordType(org.osid.type.Type credentialSearchRecordType) {
        this.credentialSearchRecordTypes.remove(credentialSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Enrollment </code> record types. 
     *
     *  @return a list containing the supported <code> Enrollment </code> 
     *          record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEnrollmentRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.enrollmentRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Enrollment </code> record type is supported. 
     *
     *  @param  enrollmentRecordType a <code> Type </code> indicating an 
     *          <code> Enrollment </code> record type 
     *  @return <code> true </code> if the given record type is supported, 
     *          <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> enrollmentRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEnrollmentRecordType(org.osid.type.Type enrollmentRecordType) {
        return (this.enrollmentRecordTypes.contains(enrollmentRecordType));
    }


    /**
     *  Adds support for an enrollment record type.
     *
     *  @param enrollmentRecordType an enrollment record type
     *  @throws org.osid.NullArgumentException
     *  <code>enrollmentRecordType</code> is <code>null</code>
     */

    protected void addEnrollmentRecordType(org.osid.type.Type enrollmentRecordType) {
        this.enrollmentRecordTypes.add(enrollmentRecordType);
        return;
    }


    /**
     *  Removes support for an enrollment record type.
     *
     *  @param enrollmentRecordType an enrollment record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>enrollmentRecordType</code> is <code>null</code>
     */

    protected void removeEnrollmentRecordType(org.osid.type.Type enrollmentRecordType) {
        this.enrollmentRecordTypes.remove(enrollmentRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Enrollment </code> search record types. 
     *
     *  @return a list containing the supported <code> Enrollment </code> 
     *          search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEnrollmentSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.enrollmentSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Enrollment </code> search record type is 
     *  supported. 
     *
     *  @param  enrollmentSearchRecordType a <code> Type </code> indicating an 
     *          <code> Enrollment </code> search record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          enrollmentSearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEnrollmentSearchRecordType(org.osid.type.Type enrollmentSearchRecordType) {
        return (this.enrollmentSearchRecordTypes.contains(enrollmentSearchRecordType));
    }


    /**
     *  Adds support for an enrollment search record type.
     *
     *  @param enrollmentSearchRecordType an enrollment search record type
     *  @throws org.osid.NullArgumentException
     *  <code>enrollmentSearchRecordType</code> is <code>null</code>
     */

    protected void addEnrollmentSearchRecordType(org.osid.type.Type enrollmentSearchRecordType) {
        this.enrollmentSearchRecordTypes.add(enrollmentSearchRecordType);
        return;
    }


    /**
     *  Removes support for an enrollment search record type.
     *
     *  @param enrollmentSearchRecordType an enrollment search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>enrollmentSearchRecordType</code> is <code>null</code>
     */

    protected void removeEnrollmentSearchRecordType(org.osid.type.Type enrollmentSearchRecordType) {
        this.enrollmentSearchRecordTypes.remove(enrollmentSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program lookup 
     *  service. 
     *
     *  @return a <code> ProgramLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramLookupSession getProgramLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program lookup 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramLookupSession getProgramLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> ProgramLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramLookupSession getProgramLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program lookup 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> ProgramLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramLookupSession getProgramLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program query 
     *  service. 
     *
     *  @return a <code> ProgramQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program query 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program query 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program search 
     *  service. 
     *
     *  @return a <code> ProgramSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchSession getProgramSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program search 
     *  service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchSession getProgramSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchSession getProgramSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program search 
     *  service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSearchSession getProgramSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  administration service. 
     *
     *  @return a <code> ProgramAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramAdminSession getProgramAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramAdminSession getProgramAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramAdminSession getProgramAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsProgramAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramAdminSession getProgramAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  notification service. 
     *
     *  @param  programReceiver the notification callback 
     *  @return a <code> ProgramNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> programReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramNotificationSession getProgramNotificationSession(org.osid.course.program.ProgramReceiver programReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  notification service. 
     *
     *  @param  programReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> ProgramNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> programReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramNotificationSession getProgramNotificationSession(org.osid.course.program.ProgramReceiver programReceiver, 
                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  programReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> programReceiver </code> 
     *          or <code> courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramNotificationSession getProgramNotificationSessionForCourseCatalog(org.osid.course.program.ProgramReceiver programReceiver, 
                                                                                                            org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  notification service for the given course catalog. 
     *
     *  @param  programReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> programReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramNotificationSession getProgramNotificationSessionForCourseCatalog(org.osid.course.program.ProgramReceiver programReceiver, 
                                                                                                            org.osid.id.Id courseCatalogId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup program/catalog 
     *  mappings. 
     *
     *  @return a <code> ProgramCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramCourseCatalogSession getProgramCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup program/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramCourseCatalog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramCourseCatalogSession getProgramCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning programs 
     *  to course catalogs. 
     *
     *  @return a <code> ProgramCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramCourseCatalogAssignmentSession getProgramCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning programs 
     *  to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramCourseCatalogAssignmentSession getProgramCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSmartCourseCatalogSession getProgramSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program smart 
     *  course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramSmartCourseCatalogSession getProgramSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with looking up course 
     *  to program mappings. 
     *
     *  @return a <code> CourseProgramLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramLookupSession getCourseProgramLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCourseProgramLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with looking up course 
     *  to program mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseProgramLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramLookupSession getCourseProgramLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCourseProgramLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with looking up course 
     *  to program mappings for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CourseProgramLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramLookupSession getCourseProgramLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCourseProgramLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with looking up course 
     *  to program mappings for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> CourseProgramLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramLookupSession getCourseProgramLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCourseProgramLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with managing course to 
     *  program mappings. 
     *
     *  @return a <code> CourseProgramAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramAssignmentSession getCourseProgramAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCourseProgramAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with managing course to 
     *  program mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseProgramAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramAssignmentSession getCourseProgramAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCourseProgramAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with managing course to 
     *  program mappings for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CourseProgramAssignmentSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramAssignmentSession getCourseProgramAssignmentSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCourseProgramAssignmentSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with managing course to 
     *  program mappings for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> CourseProgramAssignmentSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramAssignment() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CourseProgramAssignmentSession getCourseProgramAssignmentSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCourseProgramAssignmentSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering lookup service. 
     *
     *  @return a <code> ProgramOfferingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingLookupSession getProgramOfferingLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingLookup() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingLookupSession getProgramOfferingLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> ProgramOfferingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingLookupSession getProgramOfferingLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingLookupSession getProgramOfferingLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering query service. 
     *
     *  @return a <code> ProgramOfferingQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramOfferingQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQuerySession getProgramOfferingQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramQuerySession getProgramOfferingQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingQuerySession getProgramOfferingQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering search service. 
     *
     *  @return a <code> ProgramOfferingSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSearchSession getProgramOfferingSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSearch() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSearchSession getProgramOfferingSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSearchSession getProgramOfferingSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSearchSession getProgramOfferingSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering administration service. 
     *
     *  @return a <code> ProgramOfferingAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingAdminSession getProgramOfferingAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingAdminSession getProgramOfferingAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingAdminSession getProgramOfferingAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingAdminSession getProgramOfferingAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering notification service. 
     *
     *  @param  programOfferingReceiver the notification callback 
     *  @return a <code> ProgramOfferingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> programOfferingReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingNotificationSession getProgramOfferingNotificationSession(org.osid.course.program.ProgramOfferingReceiver programOfferingReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering notification service. 
     *
     *  @param  programOfferingReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> programOfferingReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingNotificationSession getProgramOfferingNotificationSession(org.osid.course.program.ProgramOfferingReceiver programOfferingReceiver, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering notification service for the given course catalog. 
     *
     *  @param  programOfferingReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> programOfferingReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingNotificationSession getProgramOfferingNotificationSessionForCourseCatalog(org.osid.course.program.ProgramOfferingReceiver programOfferingReceiver, 
                                                                                                                            org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering notification service for the given course catalog. 
     *
     *  @param  programOfferingReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> programOfferingReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingNotificationSession getProgramOfferingNotificationSessionForCourseCatalog(org.osid.course.program.ProgramOfferingReceiver programOfferingReceiver, 
                                                                                                                            org.osid.id.Id courseCatalogId, 
                                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup program offering/catalog 
     *  mappings. 
     *
     *  @return a <code> ProgramOfferingCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingCourseCatalogSession getProgramOfferingCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup program offering/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingCourseCatalogSession getProgramOfferingCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning program 
     *  offerings to course catalogs. 
     *
     *  @return a <code> ProgramOfferingCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingCourseCatalogAssignmentSession getProgramOfferingCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning program 
     *  offerings to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingCourseCatalogAssignment() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingCourseCatalogAssignmentSession getProgramOfferingCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> ProgramOfferingSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSmartCourseCatalogSession getProgramOfferingSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getProgramOfferingSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the program 
     *  offering smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> ProgramOfferingSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProgramOfferingSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.ProgramOfferingSmartCourseCatalogSession getProgramOfferingSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getProgramOfferingSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  lookup service. 
     *
     *  @return a <code> CredentialLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialLookupSession getCredentialLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialLookupSession getCredentialLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return a <code> CredentialLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialLookupSession getCredentialLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return a <code> CredentialLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialLookupSession getCredentialLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  query service. 
     *
     *  @return a <code> CredentialQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuerySession getCredentialQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuerySession getCredentialQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuerySession getCredentialQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialQuerySession getCredentialQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  search service. 
     *
     *  @return a <code> CredentialSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSearchSession getCredentialSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSearchSession getCredentialSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSearchSession getCredentialSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSearchSession getCredentialSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  administration service. 
     *
     *  @return a <code> CourseAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialAdminSession getCredentialAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CourseAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsCourseAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialAdminSession getCredentialAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialAdminSession getCredentialAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialAdminSession getCredentialAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service. 
     *
     *  @param  credentialReceiver the notification callback 
     *  @return a <code> CredentialNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> credentialReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialNotificationSession getCredentialNotificationSession(org.osid.course.program.CredentialReceiver credentialReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  notification service. 
     *
     *  @param  credentialReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return a <code> CourseNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> credentialReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialNotificationSession getCredentialNotificationSession(org.osid.course.program.CredentialReceiver credentialReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the course 
     *  notification service for the given course catalog. 
     *
     *  @param  credentialReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> credentialReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialNotificationSession getCredentialNotificationSessionForCourseCatalog(org.osid.course.program.CredentialReceiver credentialReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  notification service for the given course catalog. 
     *
     *  @param  credentialReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> credentialReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialNotificationSession getCredentialNotificationSessionForCourseCatalog(org.osid.course.program.CredentialReceiver credentialReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup credential/catalog 
     *  mappings. 
     *
     *  @return a <code> CredentialCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialCourseCatalogSession getCredentialCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup credential/catalog 
     *  mappings. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialCourseCatalogSession getCredentialCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  credentials to course catalogs. 
     *
     *  @return a <code> CredentialCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialCourseCatalogAssignmentSession getCredentialCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  credentials to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return a <code> CredentialCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialCourseCatalogAssignmentSession getCredentialCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return a <code> CredentialSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSmartCourseCatalogSession getCredentialSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCredentialSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the credential 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return a <code> CredentialSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCredentialSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.CredentialSmartCourseCatalogSession getCredentialSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCredentialSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  lookup service. 
     *
     *  @return an <code> EnrollmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentLookupSession getEnrollmentLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  lookup service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentLookupSession getEnrollmentLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @return an <code> EnrollmentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentLookupSession getEnrollmentLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  lookup service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the course catalog 
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> CourseCatalog </code> 
     *          found by the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentLookupSession getEnrollmentLookupSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentLookupSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  query service. 
     *
     *  @return an <code> EnrollmentQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentQuerySession getEnrollmentQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  query service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentQuerySession getEnrollmentQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentQuerySession getEnrollmentQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  query service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentQuerySession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentQuerySession getEnrollmentQuerySessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentQuerySessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  search service. 
     *
     *  @return an <code> EnrollmentSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentSearchSession getEnrollmentSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  search service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentSearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentSearchSession getEnrollmentSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentSearchSession getEnrollmentSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  search service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentSearchSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentSearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentSearchSession getEnrollmentSearchSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentSearchSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  administration service. 
     *
     *  @return an <code> EnrollmentAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentAdminSession getEnrollmentAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  administration service. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentAdminSession getEnrollmentAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentAdminSession getEnrollmentAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  administration service for the given course catalog. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentAdminSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> or <code> 
     *          courseCatalogId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentAdminSession getEnrollmentAdminSessionForCourseCatalog(org.osid.id.Id courseCatalogId, 
                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentAdminSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  notification service. 
     *
     *  @param  enrollmentReceiver the notification callback 
     *  @return an <code> EnrollmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> enrollmentReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentNotificationSession getEnrollmentNotificationSession(org.osid.course.program.EnrollmentReceiver enrollmentReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  notification service. 
     *
     *  @param  enrollmentReceiver the notification callback 
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> enrollmentReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentNotificationSession getEnrollmentNotificationSession(org.osid.course.program.EnrollmentReceiver enrollmentReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  notification service for the given course catalog. 
     *
     *  @param  enrollmentReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> enrollmentReceiver 
     *          </code> or <code> courseCatalogId </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentNotificationSession getEnrollmentNotificationSessionForCourseCatalog(org.osid.course.program.EnrollmentReceiver enrollmentReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  notification service for the given course catalog. 
     *
     *  @param  enrollmentReceiver the notification callback 
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentNotificationSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> enrollmentReceiver, 
     *          courseCatalogId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentNotificationSession getEnrollmentNotificationSessionForCourseCatalog(org.osid.course.program.EnrollmentReceiver enrollmentReceiver, 
                                                                                                                  org.osid.id.Id courseCatalogId, 
                                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentNotificationSessionForCourseCatalog not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup enrollment/catalog 
     *  mappings. 
     *
     *  @return an <code> EnrollmentCourseCatalogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentCourseCatalogSession getEnrollmentCourseCatalogSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup enrollment/course 
     *  catalog mappings. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentCourseCatalogSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentCourseCatalog() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentCourseCatalogSession getEnrollmentCourseCatalogSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  enrollments to course catalogs. 
     *
     *  @return an <code> EnrollmentCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentCourseCatalogAssignmentSession getEnrollmentCourseCatalogAssignmentSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning 
     *  enrollments to course catalogs. 
     *
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentCourseCatalogAssignmentSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentCourseCatalogAssignment() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentCourseCatalogAssignmentSession getEnrollmentCourseCatalogAssignmentSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentCourseCatalogAssignmentSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @return an <code> EnrollmentSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentSmartCourseCatalogSession getEnrollmentSmartCourseCatalogSession(org.osid.id.Id courseCatalogId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getEnrollmentSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the enrollment 
     *  smart course catalog service. 
     *
     *  @param  courseCatalogId the <code> Id </code> of the <code> 
     *          CourseCatalog </code> 
     *  @param  proxy proxy 
     *  @return an <code> EnrollmentSmartCourseCatalogSession </code> 
     *  @throws org.osid.NotFoundException no course catalog found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> courseCatalogId </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEnrollmentSmartCourseCatalog() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.EnrollmentSmartCourseCatalogSession getEnrollmentSmartCourseCatalogSession(org.osid.id.Id courseCatalogId, 
                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getEnrollmentSmartCourseCatalogSession not implemented");
    }


    /**
     *  Gets a <code> CourseProgramBatchManager. </code> 
     *
     *  @return a <code> CourseProgramBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.CourseProgramBatchManager getCourseProgramBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramManager.getCourseProgramBatchManager not implemented");
    }


    /**
     *  Gets a <code> CourseProgramBatchProxyManager. </code> 
     *
     *  @return a <code> CourseProgramBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCourseProgramBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.course.program.batch.CourseProgramBatchProxyManager getCourseProgramBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.course.program.CourseProgramProxyManager.getCourseProgramBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * Wthrows org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.programRecordTypes.clear();
        this.programRecordTypes.clear();

        this.programSearchRecordTypes.clear();
        this.programSearchRecordTypes.clear();

        this.programOfferingRecordTypes.clear();
        this.programOfferingRecordTypes.clear();

        this.programOfferingSearchRecordTypes.clear();
        this.programOfferingSearchRecordTypes.clear();

        this.credentialRecordTypes.clear();
        this.credentialRecordTypes.clear();

        this.credentialSearchRecordTypes.clear();
        this.credentialSearchRecordTypes.clear();

        this.enrollmentRecordTypes.clear();
        this.enrollmentRecordTypes.clear();

        this.enrollmentSearchRecordTypes.clear();
        this.enrollmentSearchRecordTypes.clear();

        return;
    }
}
