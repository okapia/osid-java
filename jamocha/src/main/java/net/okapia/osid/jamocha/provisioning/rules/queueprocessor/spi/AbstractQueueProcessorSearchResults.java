//
// AbstractQueueProcessorSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.provisioning.rules.queueprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractQueueProcessorSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.provisioning.rules.QueueProcessorSearchResults {

    private org.osid.provisioning.rules.QueueProcessorList queueProcessors;
    private final org.osid.provisioning.rules.QueueProcessorQueryInspector inspector;
    private final java.util.Collection<org.osid.provisioning.rules.records.QueueProcessorSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractQueueProcessorSearchResults.
     *
     *  @param queueProcessors the result set
     *  @param queueProcessorQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>queueProcessors</code>
     *          or <code>queueProcessorQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractQueueProcessorSearchResults(org.osid.provisioning.rules.QueueProcessorList queueProcessors,
                                            org.osid.provisioning.rules.QueueProcessorQueryInspector queueProcessorQueryInspector) {
        nullarg(queueProcessors, "queue processors");
        nullarg(queueProcessorQueryInspector, "queue processor query inspectpr");

        this.queueProcessors = queueProcessors;
        this.inspector = queueProcessorQueryInspector;

        return;
    }


    /**
     *  Gets the queue processor list resulting from a search.
     *
     *  @return a queue processor list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.provisioning.rules.QueueProcessorList getQueueProcessors() {
        if (this.queueProcessors == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.provisioning.rules.QueueProcessorList queueProcessors = this.queueProcessors;
        this.queueProcessors = null;
	return (queueProcessors);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.provisioning.rules.QueueProcessorQueryInspector getQueueProcessorQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  queue processor search record <code> Type. </code> This method must
     *  be used to retrieve a queueProcessor implementing the requested
     *  record.
     *
     *  @param queueProcessorSearchRecordType a queueProcessor search 
     *         record type 
     *  @return the queue processor search
     *  @throws org.osid.NullArgumentException
     *          <code>queueProcessorSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(queueProcessorSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueProcessorSearchResultsRecord getQueueProcessorSearchResultsRecord(org.osid.type.Type queueProcessorSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.provisioning.rules.records.QueueProcessorSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(queueProcessorSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(queueProcessorSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record queue processor search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addQueueProcessorRecord(org.osid.provisioning.rules.records.QueueProcessorSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "queue processor record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
