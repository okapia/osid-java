//
// AbstractAssemblyBallotConstrainerEnablerQuery.java
//
//     A BallotConstrainerEnablerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.voting.rules.ballotconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A BallotConstrainerEnablerQuery that stores terms.
 */

public abstract class AbstractAssemblyBallotConstrainerEnablerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidEnablerQuery
    implements org.osid.voting.rules.BallotConstrainerEnablerQuery,
               org.osid.voting.rules.BallotConstrainerEnablerQueryInspector,
               org.osid.voting.rules.BallotConstrainerEnablerSearchOrder {

    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerEnablerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerEnablerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.voting.rules.records.BallotConstrainerEnablerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyBallotConstrainerEnablerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyBallotConstrainerEnablerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches enablers mapped to the ballot constrainer. 
     *
     *  @param  ballotConstrainerId the ballot constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ballotConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledBallotConstrainerId(org.osid.id.Id ballotConstrainerId, 
                                              boolean match) {
        getAssembler().addIdTerm(getRuledBallotConstrainerIdColumn(), ballotConstrainerId, match);
        return;
    }


    /**
     *  Clears the ballot constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledBallotConstrainerIdTerms() {
        getAssembler().clearTerms(getRuledBallotConstrainerIdColumn());
        return;
    }


    /**
     *  Gets the ballot constrainer <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledBallotConstrainerIdTerms() {
        return (getAssembler().getIdTerms(getRuledBallotConstrainerIdColumn()));
    }


    /**
     *  Gets the RuledBallotConstrainerId column name.
     *
     * @return the column name
     */

    protected String getRuledBallotConstrainerIdColumn() {
        return ("ruled_ballot_constrainer_id");
    }


    /**
     *  Tests if a <code> BallotConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a ballot constrainer query is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledBallotConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a ballot constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the ballot constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledBallotConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerQuery getRuledBallotConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledBallotConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any ballot constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any ballot 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no ballot constrainers 
     */

    @OSID @Override
    public void matchAnyRuledBallotConstrainer(boolean match) {
        getAssembler().addIdWildcardTerm(getRuledBallotConstrainerColumn(), match);
        return;
    }


    /**
     *  Clears the ballot constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledBallotConstrainerTerms() {
        getAssembler().clearTerms(getRuledBallotConstrainerColumn());
        return;
    }


    /**
     *  Gets the ballot constrainer query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.rules.BallotConstrainerQueryInspector[] getRuledBallotConstrainerTerms() {
        return (new org.osid.voting.rules.BallotConstrainerQueryInspector[0]);
    }


    /**
     *  Gets the RuledBallotConstrainer column name.
     *
     * @return the column name
     */

    protected String getRuledBallotConstrainerColumn() {
        return ("ruled_ballot_constrainer");
    }


    /**
     *  Matches enablers mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        getAssembler().addIdTerm(getPollsIdColumn(), pollsId, match);
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        getAssembler().clearTerms(getPollsIdColumn());
        return;
    }


    /**
     *  Gets the polls <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getPollsIdTerms() {
        return (getAssembler().getIdTerms(getPollsIdColumn()));
    }


    /**
     *  Gets the PollsId column name.
     *
     * @return the column name
     */

    protected String getPollsIdColumn() {
        return ("polls_id");
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        getAssembler().clearTerms(getPollsColumn());
        return;
    }


    /**
     *  Gets the polls query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.voting.PollsQueryInspector[] getPollsTerms() {
        return (new org.osid.voting.PollsQueryInspector[0]);
    }


    /**
     *  Gets the Polls column name.
     *
     * @return the column name
     */

    protected String getPollsColumn() {
        return ("polls");
    }


    /**
     *  Tests if this ballotConstrainerEnabler supports the given record
     *  <code>Type</code>.
     *
     *  @param  ballotConstrainerEnablerRecordType a ballot constrainer enabler record type 
     *  @return <code>true</code> if the ballotConstrainerEnablerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type ballotConstrainerEnablerRecordType) {
        for (org.osid.voting.rules.records.BallotConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ballotConstrainerEnablerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  ballotConstrainerEnablerRecordType the ballot constrainer enabler record type 
     *  @return the ballot constrainer enabler query record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerEnablerQueryRecord getBallotConstrainerEnablerQueryRecord(org.osid.type.Type ballotConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.BallotConstrainerEnablerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(ballotConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  ballotConstrainerEnablerRecordType the ballot constrainer enabler record type 
     *  @return the ballot constrainer enabler query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerEnablerQueryInspectorRecord getBallotConstrainerEnablerQueryInspectorRecord(org.osid.type.Type ballotConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.BallotConstrainerEnablerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(ballotConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param ballotConstrainerEnablerRecordType the ballot constrainer enabler record type
     *  @return the ballot constrainer enabler search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(ballotConstrainerEnablerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.BallotConstrainerEnablerSearchOrderRecord getBallotConstrainerEnablerSearchOrderRecord(org.osid.type.Type ballotConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.BallotConstrainerEnablerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(ballotConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(ballotConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this ballot constrainer enabler. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param ballotConstrainerEnablerQueryRecord the ballot constrainer enabler query record
     *  @param ballotConstrainerEnablerQueryInspectorRecord the ballot constrainer enabler query inspector
     *         record
     *  @param ballotConstrainerEnablerSearchOrderRecord the ballot constrainer enabler search order record
     *  @param ballotConstrainerEnablerRecordType ballot constrainer enabler record type
     *  @throws org.osid.NullArgumentException
     *          <code>ballotConstrainerEnablerQueryRecord</code>,
     *          <code>ballotConstrainerEnablerQueryInspectorRecord</code>,
     *          <code>ballotConstrainerEnablerSearchOrderRecord</code> or
     *          <code>ballotConstrainerEnablerRecordTypeballotConstrainerEnabler</code> is
     *          <code>null</code>
     */
            
    protected void addBallotConstrainerEnablerRecords(org.osid.voting.rules.records.BallotConstrainerEnablerQueryRecord ballotConstrainerEnablerQueryRecord, 
                                      org.osid.voting.rules.records.BallotConstrainerEnablerQueryInspectorRecord ballotConstrainerEnablerQueryInspectorRecord, 
                                      org.osid.voting.rules.records.BallotConstrainerEnablerSearchOrderRecord ballotConstrainerEnablerSearchOrderRecord, 
                                      org.osid.type.Type ballotConstrainerEnablerRecordType) {

        addRecordType(ballotConstrainerEnablerRecordType);

        nullarg(ballotConstrainerEnablerQueryRecord, "ballot constrainer enabler query record");
        nullarg(ballotConstrainerEnablerQueryInspectorRecord, "ballot constrainer enabler query inspector record");
        nullarg(ballotConstrainerEnablerSearchOrderRecord, "ballot constrainer enabler search odrer record");

        this.queryRecords.add(ballotConstrainerEnablerQueryRecord);
        this.queryInspectorRecords.add(ballotConstrainerEnablerQueryInspectorRecord);
        this.searchOrderRecords.add(ballotConstrainerEnablerSearchOrderRecord);
        
        return;
    }
}
