//
// AbstractRaceResult.java
//
//     Defines a RaceResult.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.raceresult.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>RaceResult</code>.
 */

public abstract class AbstractRaceResult
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendium
    implements org.osid.voting.RaceResult {

    private org.osid.voting.Race race;
    private org.osid.voting.Candidate candidate;
    private org.osid.resource.Resource resource;
    private long votes;
    private java.math.BigDecimal mean;
    private long median;
    private java.math.BigDecimal standardDeviation;

    private boolean resultsFinal = false;

    private final java.util.Collection<org.osid.voting.records.RaceResultRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the race <code> Id </code> of the vote. 
     *
     *  @return the ballot <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getRaceId() {
        return (this.race.getId());
    }


    /**
     *  Gets the race of the vote. 
     *
     *  @return the race 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.voting.Race getRace()
        throws org.osid.OperationFailedException {

        return (this.race);
    }


    /**
     *  Sets the race.
     *
     *  @param race a race
     *  @throws org.osid.NullArgumentException <code>race</code> is
     *          <code>null</code>
     */

    protected void setRace(org.osid.voting.Race race) {
        nullarg(race, "race");
        this.race = race;
        return;
    }


    /**
     *  Gets the candidate <code> Id. </code> 
     *
     *  @return a candidate <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getCandidateId() {
        return (this.candidate.getId());
    }


    /**
     *  Gets the <code> Candidate. </code> 
     *
     *  @return the candidate 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.voting.Candidate getCandidate()
        throws org.osid.OperationFailedException {

        return (this.candidate);
    }


    /**
     *  Sets the candidate.
     *
     *  @param candidate a candidate
     *  @throws org.osid.NullArgumentException <code>candidate</code>
     *          is <code>null</code>
     */

    protected void setCandidate(org.osid.voting.Candidate candidate) {
        nullarg(candidate, "candidate");
        this.candidate = candidate;
        return;
    }


    /**
     *  Gets the resource <code> Id </code>. A resource represents a
     *  voter or group of voters across a demographic.
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the <code> Resource. </code> A resource represents a voter or 
     *  group of voters across a demographic. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {

        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource a resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Tests if these results are final. 
     *
     *  @return <code> true </code> if these results are final, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isFinal() {
        return (this.resultsFinal);
    }


    /**
     *  Sets the final.
     *
     *  @param resultsFinal <code> true </code> if these results are final,
     *         <code> false </code> otherwise
     */

    protected void setFinal(boolean resultsFinal) {
        this.resultsFinal = resultsFinal;
        return;
    }


    /**
     *  Gets the total number of votes cast for this candidate. 
     *
     *  @return the total number of votes cast 
     */

    @OSID @Override
    public long getTotalVotes() {
        return (this.votes);
    }


    /**
     *  Sets the total votes.
     *
     *  @param votes the total votes
     */

    protected void setTotalVotes(long votes) {
        this.votes = votes;
        return;
    }


    /**
     *  Gets the mean number of votes cast for this candidate. 
     *
     *  @return the mean number of votes cast 
     */

    @OSID @Override
    public java.math.BigDecimal getMeanVotes() {
        return (this.mean);
    }


    /**
     *  Sets the mean votes.
     *
     *  @param mean the mean votes
     *  @throws org.osid.NullArgumentException <code>mean</code> is
     *          <code>null</code>
     */

    protected void setMeanVotes(java.math.BigDecimal mean) {
        nullarg(mean, "mean votes");
        this.mean = mean;
        return;
    }


    /**
     *  Gets the median number of votes cast for this candidate.
     *
     *  @return the median number of votes cast 
     */

    @OSID @Override
    public long getMedianVotes() {
        return (this.median);
    }


    /**
     *  Sets the median votes.
     *
     *  @param median the median votes
     */

    protected void setMedianVotes(long median) {
        this.median = median;
        return;
    }


    /**
     *  Gets the standard deviation of the votes. 
     *
     *  @return the standard deviation 
     */

    @OSID @Override
    public java.math.BigDecimal getVoteStandardDeviation() {
        return (this.standardDeviation);
    }


    /**
     *  Sets the vote standard deviation.
     *
     *  @param standardDeviation a vote standard deviation
     *  @throws org.osid.NullArgumentException
     *          <code>standardDeviation</code> is <code>null</code>
     */

    protected void setVoteStandardDeviation(java.math.BigDecimal standardDeviation) {
        nullarg(standardDeviation, "standard deviation");
        this.standardDeviation = standardDeviation;
        return;
    }


    /**
     *  Tests if this race result supports the given record
     *  <code>Type</code>.
     *
     *  @param  raceResultRecordType a race result record type 
     *  @return <code>true</code> if the raceResultRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>raceResultRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type raceResultRecordType) {
        for (org.osid.voting.records.RaceResultRecord record : this.records) {
            if (record.implementsRecordType(raceResultRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>RaceResult</code> record <code>Type</code>.
     *
     *  @param raceResultRecordType the race result record type
     *  @return the race result record 
     *  @throws org.osid.NullArgumentException
     *          <code>raceResultRecordType</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceResultRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.RaceResultRecord getRaceResultRecord(org.osid.type.Type raceResultRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.records.RaceResultRecord record : this.records) {
            if (record.implementsRecordType(raceResultRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceResultRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race result. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param raceResultRecord the race result record
     *  @param raceResultRecordType race result record type
     *  @throws org.osid.NullArgumentException
     *          <code>raceResultRecord</code> or
     *          <code>raceResultRecordTyperaceResult</code> is
     *          <code>null</code>
     */
            
    protected void addRaceResultRecord(org.osid.voting.records.RaceResultRecord raceResultRecord, 
                                     org.osid.type.Type raceResultRecordType) {

        addRecordType(raceResultRecordType);
        this.records.add(raceResultRecord);
        
        return;
    }
}
