//
// InvariantMapRecurringEventLookupSession
//
//    Implements a RecurringEvent lookup service backed by a fixed collection of
//    recurringEvents.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring;


/**
 *  Implements a RecurringEvent lookup service backed by a fixed
 *  collection of recurring events. The recurring events are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapRecurringEventLookupSession
    extends net.okapia.osid.jamocha.core.calendaring.spi.AbstractMapRecurringEventLookupSession
    implements org.osid.calendaring.RecurringEventLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapRecurringEventLookupSession</code> with no
     *  recurring events.
     *  
     *  @param calendar the calendar
     *  @throws org.osid.NullArgumnetException {@code calendar} is
     *          {@code null}
     */

    public InvariantMapRecurringEventLookupSession(org.osid.calendaring.Calendar calendar) {
        setCalendar(calendar);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRecurringEventLookupSession</code> with a single
     *  recurring event.
     *  
     *  @param calendar the calendar
     *  @param recurringEvent a single recurring event
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEvent} is <code>null</code>
     */

      public InvariantMapRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.RecurringEvent recurringEvent) {
        this(calendar);
        putRecurringEvent(recurringEvent);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRecurringEventLookupSession</code> using an array
     *  of recurring events.
     *  
     *  @param calendar the calendar
     *  @param recurringEvents an array of recurring events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEvents} is <code>null</code>
     */

      public InvariantMapRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                               org.osid.calendaring.RecurringEvent[] recurringEvents) {
        this(calendar);
        putRecurringEvents(recurringEvents);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapRecurringEventLookupSession</code> using a
     *  collection of recurring events.
     *
     *  @param calendar the calendar
     *  @param recurringEvents a collection of recurring events
     *  @throws org.osid.NullArgumentException {@code calendar} or
     *          {@code recurringEvents} is <code>null</code>
     */

      public InvariantMapRecurringEventLookupSession(org.osid.calendaring.Calendar calendar,
                                               java.util.Collection<? extends org.osid.calendaring.RecurringEvent> recurringEvents) {
        this(calendar);
        putRecurringEvents(recurringEvents);
        return;
    }
}
