//
// AbstractImmutableGradebookColumnCalculation.java
//
//     Wraps a mutable GradebookColumnCalculation to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.grading.calculation.gradebookcolumncalculation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>GradebookColumnCalculation</code> to hide modifiers. This
 *  wrapper provides an immutized GradebookColumnCalculation from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying gradebookColumnCalculation whose state changes are visible.
 */

public abstract class AbstractImmutableGradebookColumnCalculation
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidRule
    implements org.osid.grading.calculation.GradebookColumnCalculation {

    private final org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation;


    /**
     *  Constructs a new <code>AbstractImmutableGradebookColumnCalculation</code>.
     *
     *  @param gradebookColumnCalculation the gradebook column calculation to immutablize
     *  @throws org.osid.NullArgumentException <code>gradebookColumnCalculation</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableGradebookColumnCalculation(org.osid.grading.calculation.GradebookColumnCalculation gradebookColumnCalculation) {
        super(gradebookColumnCalculation);
        this.gradebookColumnCalculation = gradebookColumnCalculation;
        return;
    }


    /**
     *  Gets the <code> GradebookColumn Ids </code> to which this column 
     *  applies. 
     *
     *  @return the gradebook column <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getGradebookColumnId() {
        return (this.gradebookColumnCalculation.getGradebookColumnId());
    }


    /**
     *  Gets the <code> GradebookColumn </code> to which this calculation 
     *  applies. 
     *
     *  @return the gradebook column 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumn getGradebookColumn()
        throws org.osid.OperationFailedException {

        return (this.gradebookColumnCalculation.getGradebookColumn());
    }


    /**
     *  Gets the <code> GradebookColumn Ids </code> from which this column is 
     *  derived. 
     *
     *  @return the derived column <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getInputGradebookColumnIds() {
        return (this.gradebookColumnCalculation.getInputGradebookColumnIds());
    }


    /**
     *  Gets the <code> GradebookColumns </code> from which this column is 
     *  derived. 
     *
     *  @return the derived columns 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradebookColumnList getInputGradebookColumns()
        throws org.osid.OperationFailedException {

        return (this.gradebookColumnCalculation.getInputGradebookColumns());
    }


    /**
     *  Gets the operation to perform for deriving this column. The output of 
     *  the operation determines the score or grade for the column entries. 
     *
     *  @return the calculation operation 
     */

    @OSID @Override
    public org.osid.grading.calculation.CalculationOperation getOperation() {
        return (this.gradebookColumnCalculation.getOperation());
    }


    /**
     *  Gets the tweaked midpoint input value of the grading system for 
     *  determining how to center calculated enries in this column. This tweak 
     *  is applied before calculating standard deviation offsets for entries 
     *  in this derived column. 
     *
     *  @return the tweaked center 
     *  @throws org.osid.IllegalStateException <code> getCalculation() </code> 
     *          is not <code> CalculationOperation.STD_DEVIATION_OFFSET 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getTweakedCenter() {
        return (this.gradebookColumnCalculation.getTweakedCenter());
    }


    /**
     *  Gets the tweaked standard deviation. This tweak is applied before 
     *  calculating standard deviation offsets for entries in this derived 
     *  column. 
     *
     *  @return the tweaked standard deviation 
     *  @throws org.osid.IllegalStateException <code> getCalculation() </code> 
     *          is not <code> CalculationOperation.STD_DEVIATION_OFFSET 
     *          </code> 
     */

    @OSID @Override
    public java.math.BigDecimal getTweakedStandardDeviation() {
        return (this.gradebookColumnCalculation.getTweakedStandardDeviation());
    }


    /**
     *  Gets the gradebook column calculation record corresponding to the 
     *  given <code> GradeBookColumnCalculation </code> record <code> Type. 
     *  </code> This method is used to retrieve an object implementing the 
     *  requested record. The <code> gradebookColumnCalculationRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> 
     *  hasRecordType(gradebookColumnCalculationRecordType) </code> is <code> 
     *  true </code> . 
     *
     *  @param  gradebookColumnCalculationRecordType the type of the record to 
     *          retrieve 
     *  @return the gradebook column calculation record 
     *  @throws org.osid.NullArgumentException <code> 
     *          gradebookColumnCalculationRecordType </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *          occurred 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(gradebookColumnCalculationRecordType) </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.grading.calculation.records.GradebookColumnCalculationRecord getGradebookColumnCalculationRecord(org.osid.type.Type gradebookColumnCalculationRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.gradebookColumnCalculation.getGradebookColumnCalculationRecord(gradebookColumnCalculationRecordType));
    }
}

