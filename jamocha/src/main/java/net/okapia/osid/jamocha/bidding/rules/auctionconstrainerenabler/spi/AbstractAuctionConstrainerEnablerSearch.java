//
// AbstractAuctionConstrainerEnablerSearch.java
//
//     A template for making an AuctionConstrainerEnabler Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.bidding.rules.auctionconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing auction constrainer enabler searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAuctionConstrainerEnablerSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.bidding.rules.AuctionConstrainerEnablerSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.bidding.rules.AuctionConstrainerEnablerSearchOrder auctionConstrainerEnablerSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of auction constrainer enablers. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  auctionConstrainerEnablerIds list of auction constrainer enablers
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAuctionConstrainerEnablers(org.osid.id.IdList auctionConstrainerEnablerIds) {
        while (auctionConstrainerEnablerIds.hasNext()) {
            try {
                this.ids.add(auctionConstrainerEnablerIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAuctionConstrainerEnablers</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of auction constrainer enabler Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAuctionConstrainerEnablerIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  auctionConstrainerEnablerSearchOrder auction constrainer enabler search order 
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>auctionConstrainerEnablerSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAuctionConstrainerEnablerResults(org.osid.bidding.rules.AuctionConstrainerEnablerSearchOrder auctionConstrainerEnablerSearchOrder) {
	this.auctionConstrainerEnablerSearchOrder = auctionConstrainerEnablerSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.bidding.rules.AuctionConstrainerEnablerSearchOrder getAuctionConstrainerEnablerSearchOrder() {
	return (this.auctionConstrainerEnablerSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given auction constrainer enabler search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an auction constrainer enabler implementing the requested record.
     *
     *  @param auctionConstrainerEnablerSearchRecordType an auction constrainer enabler search record
     *         type
     *  @return the auction constrainer enabler search record
     *  @throws org.osid.NullArgumentException
     *          <code>auctionConstrainerEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(auctionConstrainerEnablerSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchRecord getAuctionConstrainerEnablerSearchRecord(org.osid.type.Type auctionConstrainerEnablerSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchRecord record : this.records) {
            if (record.implementsRecordType(auctionConstrainerEnablerSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(auctionConstrainerEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this auction constrainer enabler search. 
     *
     *  @param auctionConstrainerEnablerSearchRecord auction constrainer enabler search record
     *  @param auctionConstrainerEnablerSearchRecordType auctionConstrainerEnabler search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAuctionConstrainerEnablerSearchRecord(org.osid.bidding.rules.records.AuctionConstrainerEnablerSearchRecord auctionConstrainerEnablerSearchRecord, 
                                           org.osid.type.Type auctionConstrainerEnablerSearchRecordType) {

        addRecordType(auctionConstrainerEnablerSearchRecordType);
        this.records.add(auctionConstrainerEnablerSearchRecord);        
        return;
    }
}
