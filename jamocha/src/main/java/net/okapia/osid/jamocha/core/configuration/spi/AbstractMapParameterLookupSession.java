//
// AbstractMapParameterLookupSession
//
//    A simple framework for providing a Parameter lookup service
//    backed by a fixed collection of parameters.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Parameter lookup service backed by a
 *  fixed collection of parameters. The parameters are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Parameters</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapParameterLookupSession
    extends net.okapia.osid.jamocha.configuration.spi.AbstractParameterLookupSession
    implements org.osid.configuration.ParameterLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.configuration.Parameter> parameters = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.configuration.Parameter>());


    /**
     *  Makes a <code>Parameter</code> available in this session.
     *
     *  @param  parameter a parameter
     *  @throws org.osid.NullArgumentException <code>parameter<code>
     *          is <code>null</code>
     */

    protected void putParameter(org.osid.configuration.Parameter parameter) {
        this.parameters.put(parameter.getId(), parameter);
        return;
    }


    /**
     *  Makes an array of parameters available in this session.
     *
     *  @param  parameters an array of parameters
     *  @throws org.osid.NullArgumentException <code>parameters<code>
     *          is <code>null</code>
     */

    protected void putParameters(org.osid.configuration.Parameter[] parameters) {
        putParameters(java.util.Arrays.asList(parameters));
        return;
    }


    /**
     *  Makes a collection of parameters available in this session.
     *
     *  @param  parameters a collection of parameters
     *  @throws org.osid.NullArgumentException <code>parameters<code>
     *          is <code>null</code>
     */

    protected void putParameters(java.util.Collection<? extends org.osid.configuration.Parameter> parameters) {
        for (org.osid.configuration.Parameter parameter : parameters) {
            this.parameters.put(parameter.getId(), parameter);
        }

        return;
    }


    /**
     *  Removes a Parameter from this session.
     *
     *  @param  parameterId the <code>Id</code> of the parameter
     *  @throws org.osid.NullArgumentException <code>parameterId<code> is
     *          <code>null</code>
     */

    protected void removeParameter(org.osid.id.Id parameterId) {
        this.parameters.remove(parameterId);
        return;
    }


    /**
     *  Gets the <code>Parameter</code> specified by its <code>Id</code>.
     *
     *  @param  parameterId <code>Id</code> of the <code>Parameter</code>
     *  @return the parameter
     *  @throws org.osid.NotFoundException <code>parameterId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>parameterId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.Parameter getParameter(org.osid.id.Id parameterId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.configuration.Parameter parameter = this.parameters.get(parameterId);
        if (parameter == null) {
            throw new org.osid.NotFoundException("parameter not found: " + parameterId);
        }

        return (parameter);
    }


    /**
     *  Gets all <code>Parameters</code>. In plenary mode, the returned
     *  list contains all known parameters or an error
     *  results. Otherwise, the returned list may contain only those
     *  parameters that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Parameters</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.ParameterList getParameters()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.parameter.ArrayParameterList(this.parameters.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.parameters.clear();
        super.close();
        return;
    }
}
