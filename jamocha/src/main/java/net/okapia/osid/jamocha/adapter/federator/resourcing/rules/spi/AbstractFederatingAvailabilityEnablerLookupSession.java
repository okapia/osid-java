//
// AbstractFederatingAvailabilityEnablerLookupSession.java
//
//     An abstract federating adapter for an AvailabilityEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AvailabilityEnablerLookupSession. Sessions are added to this
 *  session through <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAvailabilityEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.resourcing.rules.AvailabilityEnablerLookupSession>
    implements org.osid.resourcing.rules.AvailabilityEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();


    /**
     *  Constructs a new <code>AbstractFederatingAvailabilityEnablerLookupSession</code>.
     */

    protected AbstractFederatingAvailabilityEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.resourcing.rules.AvailabilityEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>AvailabilityEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAvailabilityEnablers() {
        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            if (session.canLookupAvailabilityEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AvailabilityEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAvailabilityEnablerView() {
        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            session.useComparativeAvailabilityEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AvailabilityEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAvailabilityEnablerView() {
        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            session.usePlenaryAvailabilityEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include availability enablers in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            session.useFederatedFoundryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            session.useIsolatedFoundryView();
        }

        return;
    }


    /**
     *  Only active availability enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAvailabilityEnablerView() {
        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            session.useActiveAvailabilityEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive availability enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusAvailabilityEnablerView() {
        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            session.useAnyStatusAvailabilityEnablerView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>AvailabilityEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AvailabilityEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AvailabilityEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, availability enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  availability enablers are returned.
     *
     *  @param  availabilityEnablerId <code>Id</code> of the
     *          <code>AvailabilityEnabler</code>
     *  @return the availability enabler
     *  @throws org.osid.NotFoundException <code>availabilityEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>availabilityEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnabler getAvailabilityEnabler(org.osid.id.Id availabilityEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            try {
                return (session.getAvailabilityEnabler(availabilityEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(availabilityEnablerId + " not found");
    }


    /**
     *  Gets an <code>AvailabilityEnablerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  availabilityEnablers specified in the <code>Id</code> list, in
     *  the order of the list, including duplicates, or an error
     *  results if an <code>Id</code> in the supplied list is not
     *  found or inaccessible. Otherwise, inaccessible
     *  <code>AvailabilityEnablers</code> may be omitted from the list
     *  and may present the elements in any order including returning
     *  a unique set.
     *
     *  In active mode, availability enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  availability enablers are returned.
     *
     *  @param  availabilityEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AvailabilityEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByIds(org.osid.id.IdList availabilityEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.resourcing.rules.availabilityenabler.MutableAvailabilityEnablerList ret = new net.okapia.osid.jamocha.resourcing.rules.availabilityenabler.MutableAvailabilityEnablerList();

        try (org.osid.id.IdList ids = availabilityEnablerIds) {
            while (ids.hasNext()) {
                ret.addAvailabilityEnabler(getAvailabilityEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AvailabilityEnablerList</code> corresponding to the given
     *  availability enabler genus <code>Type</code> which does not include
     *  availability enablers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the
     *  returned list may contain only those availability enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param  availabilityEnablerGenusType an availabilityEnabler genus type 
     *  @return the returned <code>AvailabilityEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByGenusType(org.osid.type.Type availabilityEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.availabilityenabler.FederatingAvailabilityEnablerList ret = getAvailabilityEnablerList();

        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            ret.addAvailabilityEnablerList(session.getAvailabilityEnablersByGenusType(availabilityEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AvailabilityEnablerList</code> corresponding to
     *  the given availability enabler genus <code>Type</code> and
     *  include any additional availability enablers with genus types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the
     *  returned list may contain only those availability enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, availability enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  availability enablers are returned.
     *
     *  @param  availabilityEnablerGenusType an availabilityEnabler genus type 
     *  @return the returned <code>AvailabilityEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByParentGenusType(org.osid.type.Type availabilityEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.availabilityenabler.FederatingAvailabilityEnablerList ret = getAvailabilityEnablerList();

        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            ret.addAvailabilityEnablerList(session.getAvailabilityEnablersByParentGenusType(availabilityEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AvailabilityEnablerList</code> containing the
     *  given availability enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the
     *  returned list may contain only those availability enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, availability enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  availability enablers are returned.
     *
     *  @param  availabilityEnablerRecordType an availabilityEnabler record type 
     *  @return the returned <code>AvailabilityEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>availabilityEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersByRecordType(org.osid.type.Type availabilityEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.availabilityenabler.FederatingAvailabilityEnablerList ret = getAvailabilityEnablerList();

        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            ret.addAvailabilityEnablerList(session.getAvailabilityEnablersByRecordType(availabilityEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AvailabilityEnablerList</code> effective during
     *  the entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the returned list
     *  may contain only those availability enablers that are accessible
     *  through this session.
     *  
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AvailabilityEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.availabilityenabler.FederatingAvailabilityEnablerList ret = getAvailabilityEnablerList();

        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            ret.addAvailabilityEnablerList(session.getAvailabilityEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>AvailabilityEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the returned list
     *  may contain only those availability enablers that are accessible
     *  through this session.
     *
     *  In active mode, availability enablers are returned that are currently
     *  active. In any status mode, active and inactive availability enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>AvailabilityEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.availabilityenabler.FederatingAvailabilityEnablerList ret = getAvailabilityEnablerList();

        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            ret.addAvailabilityEnablerList(session.getAvailabilityEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AvailabilityEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  availability enablers or an error results. Otherwise, the
     *  returned list may contain only those availability enablers
     *  that are accessible through this session. In both cases, the
     *  order of the set is not specified.
     *
     *  In active mode, availability enablers are returned that are
     *  currently active. In any status mode, active and inactive
     *  availability enablers are returned.
     *
     *  @return a list of <code>AvailabilityEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.AvailabilityEnablerList getAvailabilityEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.availabilityenabler.FederatingAvailabilityEnablerList ret = getAvailabilityEnablerList();

        for (org.osid.resourcing.rules.AvailabilityEnablerLookupSession session : getSessions()) {
            ret.addAvailabilityEnablerList(session.getAvailabilityEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.resourcing.rules.availabilityenabler.FederatingAvailabilityEnablerList getAvailabilityEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.rules.availabilityenabler.ParallelAvailabilityEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.rules.availabilityenabler.CompositeAvailabilityEnablerList());
        }
    }
}
