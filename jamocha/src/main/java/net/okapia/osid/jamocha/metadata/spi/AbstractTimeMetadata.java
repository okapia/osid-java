//
// AbstractTimeMetadata.java
//
//     Defines a time Metadata.
//
//
// Tom Coppeto
// Okapia
// 15 March 2013
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.metadata.spi;

import org.osid.binding.java.annotation.OSID;

import net.okapia.osid.primordium.calendaring.UTCTime;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeSet;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a time Metadata.
 */

public abstract class AbstractTimeMetadata
    extends AbstractMetadata
    implements org.osid.Metadata {

    private final Types types = new TypeSet();
    private org.osid.calendaring.DateTimeResolution resolution = org.osid.calendaring.DateTimeResolution.SECOND;

    private org.osid.calendaring.Time minimum = UTCTime.valueOf("00:00:00");
    private org.osid.calendaring.Time maximum = UTCTime.valueOf("23:59:59");

    private final java.util.Collection<org.osid.calendaring.Time> set = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.Time> defvals  = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.Time> existing = new java.util.LinkedHashSet<>();
    

    /**
     *  Constructs a new {@code AbstractTimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractTimeMetadata(org.osid.id.Id elementId) {
        super(org.osid.Syntax.TIME, elementId);
        return;
    }


    /**
     *  Constructs a new {@code AbstractTimeMetadata}.
     *
     *  @param elementId the Id of the element
     *  @param isArray {@code true} if the element is an array another
     *         element, {@code false} if a single element
     *  @param isLinked {@code true} if the element is linked to
     *         another element, {@code false} otherwise
     *  @throws org.osid.NullArgumentException {@code elementId} is
     *          {@code null}
     */

    protected AbstractTimeMetadata(org.osid.id.Id elementId, boolean isArray, boolean isLinked) {
        super(org.osid.Syntax.TIME, elementId, isArray, isLinked);
        return;
    }


    /**
     *  Gets the smallest resolution of the time value. 
     *
     *  @return the resolution 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>TIME</code>, <code>DURATION</code>, or
     *          <code>TIME</code>
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeResolution getDateTimeResolution() {
        return (this.resolution);
    }


    /**
     *  Sets the resolution.
     *
     *  @param resolution the smallest resolution allowed
     *  @throws org.osid.NullARgumentException {@code resolution} is
     *          {@code null}
     */

    protected void setResolution(org.osid.calendaring.DateTimeResolution resolution) {
        nullarg(resolution, "resolution");
        this.resolution = resolution;
        return;
    }


    /**
     *  Gets the set of acceptable time types. 
     *
     *  @return a set of time types or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>TIME</code> or <code>DURATION</code>
     */

    @OSID @Override
    public org.osid.type.Type[] getTimeTypes() {
        return (this.types.toArray());
    }


    /**
     *  Tests if the given time type is supported. 
     *
     *  @param  timeType a time Type 
     *  @return <code> true </code> if the type is supported, <code> false 
     *          </code> otherwise 
     *  @throws org.osid.IllegalStateException syntax is not a
     *          <code>TIME</code> or <code>DURATION</code>
     *  @throws org.osid.NullArgumentException <code> timeType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsTimeType(org.osid.type.Type timeType) {
        return (this.types.contains(timeType));
    }


    /**
     *  Add support for a time type.
     *
     *  @param timeType the type of time
     *  @throws org.osid.NullArgumentException {@code timeType} is
     *          {@code null}
     */

    protected void addTimeType(org.osid.type.Type timeType) {
        this.types.add(timeType);
        return;
    }


    /**
     *  Gets the minimum time value. 
     *
     *  @return the minimum time 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          TIME </code>
     */

    @OSID @Override
    public org.osid.calendaring.Time getMinimumTime() {
        return (this.minimum);
    }


    /**
     *  Gets the maximum time value. 
     *
     *  @return the maximum time 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          TIME </code>
     */

    @OSID @Override
    public org.osid.calendaring.Time getMaximumTime() {
        return (this.maximum);
    }


    /**
     *  Sets the min and max times.
     *
     *  @param min the minimum value
     *  @param max the maximum value
     *  @throws org.osid.InvalidArgumentException {@code min} is
     *          greater than {@code max}
     *  @throws org.osid.NullArgumentException {@code min} or
     *          {@code max} is {@code null}
     */

    protected void setTimeRange(org.osid.calendaring.Time min, org.osid.calendaring.Time max) {
        nullarg(min, "min time");
        nullarg(max, "max time");

        if (min.isGreater(max)) {
            throw new org.osid.InvalidArgumentException("min is greater than max");
        }

        this.maximum = min;
        this.maximum = min;

        return;
    }


    /**
     *  Gets the set of acceptable time values. 
     *
     *  @return a set of times or an empty array if not restricted 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          TIME </code>
     */

    @OSID @Override
    public org.osid.calendaring.Time[] getTimeSet() {
        return (this.set.toArray(new org.osid.calendaring.Time[this.set.size()]));
    }

    
    /**
     *  Sets the time set.
     *
     *  @param values a collection of accepted time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setTimeSet(java.util.Collection<org.osid.calendaring.Time> values) {
        this.set.clear();
        addToTimeSet(values);
        return;
    }


    /**
     *  Adds a collection of values to the time set.
     *
     *  @param values a collection of accepted time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addToTimeSet(java.util.Collection<org.osid.calendaring.Time> values) {
        nullarg(values, "time set");
        this.set.addAll(values);
        return;
    }


    /**
     *  Adds a value to the time set.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addToTimeSet(org.osid.calendaring.Time value) {
        nullarg(value, "time value");
        this.set.add(value);
        return;
    }


    /**
     *  Removes a value from the time set.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeFromTimeSet(org.osid.calendaring.Time value) {
        nullarg(value, "time value");
        this.set.remove(value);
        return;
    }


    /**
     *  Clears the time set.
     */

    protected void clearTimeSet() {
        this.set.clear();
        return;
    }


    /**
     *  Gets the default time values. These are the values used
     *  if the element value is not provided or is cleared. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the default time values 
     *  @throws org.osid.IllegalStateException syntax is not a <code>
     *          TIME </code> or <code> isRequired() </code> is
     *          <code> true </code>
     */

    @OSID @Override
    public org.osid.calendaring.Time[] getDefaultTimeValues() {
        return (this.defvals.toArray(new org.osid.calendaring.Time[this.defvals.size()]));
    }


    /**
     *  Sets the default time set.
     *
     *  @param values a collection of default time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setDefaultTimeValues(java.util.Collection<org.osid.calendaring.Time> values) {
        clearDefaultTimeValues();
        addDefaultTimeValues(values);
        return;
    }


    /**
     *  Adds a collection of default time values.
     *
     *  @param values a collection of default time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addDefaultTimeValues(java.util.Collection<org.osid.calendaring.Time> values) {
        nullarg(values, "default time values");
        this.defvals.addAll(values);
        return;
    }


    /**
     *  Adds a default time value.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addDefaultTimeValue(org.osid.calendaring.Time value) {
        nullarg(value, "default time value");
        this.defvals.add(value);
        return;
    }


    /**
     *  Removes a default time value.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeDefaultTimeValue(org.osid.calendaring.Time value) {
        nullarg(value, "default time value");
        this.defvals.remove(value);
        return;
    }


    /**
     *  Clears the default time values.
     */

    protected void clearDefaultTimeValues() {
        this.defvals.clear();
        return;
    }


    /**
     *  Gets the existing time values. If <code> hasValue()
     *  </code> and <code> isRequired() </code> are <code> false,
     *  </code> then these values are the default values. If <code>
     *  isArray() </code> is false, then this method returns at most a
     *  single value.
     *
     *  @return the existing time values 
     *  @throws org.osid.IllegalStateException syntax is not a <code> 
     *          TIME </code> or <code> isValueKnown() </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.Time[] getExistingTimeValues() {
        return (this.existing.toArray(new org.osid.calendaring.Time[this.existing.size()]));
    }


    /**
     *  Sets the existing time set.
     *
     *  @param values a collection of existing time values
     *  @throws org.osid.InvalidArgumentException a value is negative
     *  @throws org.osid.NullArgumentException {@code values} is
     *         {@code null}
     */

    protected void setExistingTimeValues(java.util.Collection<org.osid.calendaring.Time> values) {
        clearExistingTimeValues();
        addExistingTimeValues(values);
        return;
    }


    /**
     *  Adds a collection of existing time values.
     *
     *  @param values a collection of existing time values
     *  @throws org.osid.NullArgumentException {@code values} is
     *          {@code null}
     */

    protected void addExistingTimeValues(java.util.Collection<org.osid.calendaring.Time> values) {
        nullarg(values, "existing time values");

        this.existing.addAll(values);
        setValueKnown(true);

        return;
    }


    /**
     *  Adds a existing time value.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void addExistingTimeValue(org.osid.calendaring.Time value) {
        nullarg(value, "existing time value");

        this.existing.add(value);
        setValueKnown(true);

        return;
    }


    /**
     *  Removes a existing time value.
     *
     *  @param value a time value
     *  @throws org.osid.NullArgumentException {@code value} is {@code
     *          null}
     */

    protected void removeExistingTimeValue(org.osid.calendaring.Time value) {
        nullarg(value, "existing time value");
        this.existing.remove(value);
        return;
    }


    /**
     *  Clears the existing time values.
     */

    protected void clearExistingTimeValues() {
        this.existing.clear();
        setValueKnown(false);
        return;
    }        
}