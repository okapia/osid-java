//
// AbstractAssemblySceneQuery.java
//
//     A SceneQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.control.scene.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A SceneQuery that stores terms.
 */

public abstract class AbstractAssemblySceneQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.control.SceneQuery,
               org.osid.control.SceneQueryInspector,
               org.osid.control.SceneSearchOrder {

    private final java.util.Collection<org.osid.control.records.SceneQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.SceneQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.control.records.SceneSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblySceneQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblySceneQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the setting <code> Id </code> for this query. 
     *
     *  @param  settingId the setting <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> settingId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSettingId(org.osid.id.Id settingId, boolean match) {
        getAssembler().addIdTerm(getSettingIdColumn(), settingId, match);
        return;
    }


    /**
     *  Clears the setting <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSettingIdTerms() {
        getAssembler().clearTerms(getSettingIdColumn());
        return;
    }


    /**
     *  Gets the setting <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSettingIdTerms() {
        return (getAssembler().getIdTerms(getSettingIdColumn()));
    }


    /**
     *  Gets the SettingId column name.
     *
     * @return the column name
     */

    protected String getSettingIdColumn() {
        return ("setting_id");
    }


    /**
     *  Tests if a <code> SettingQuery </code> is available. 
     *
     *  @return <code> true </code> if a setting query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSettingQuery() {
        return (false);
    }


    /**
     *  Gets the query for a setting. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the setting query 
     *  @throws org.osid.UnimplementedException <code> supportsSettingQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SettingQuery getSettingQuery() {
        throw new org.osid.UnimplementedException("supportsSettingQuery() is false");
    }


    /**
     *  Matches scenes with any setting. 
     *
     *  @param  match <code> true </code> to match scenes with any setting, 
     *          <code> false </code> to match scenes with no settings 
     */

    @OSID @Override
    public void matchAnySetting(boolean match) {
        getAssembler().addIdWildcardTerm(getSettingColumn(), match);
        return;
    }


    /**
     *  Clears the setting query terms. 
     */

    @OSID @Override
    public void clearSettingTerms() {
        getAssembler().clearTerms(getSettingColumn());
        return;
    }


    /**
     *  Gets the setting query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SettingQueryInspector[] getSettingTerms() {
        return (new org.osid.control.SettingQueryInspector[0]);
    }


    /**
     *  Gets the Setting column name.
     *
     * @return the column name
     */

    protected String getSettingColumn() {
        return ("setting");
    }


    /**
     *  Sets the system <code> Id </code> for this query to match scenes 
     *  assigned to systems. 
     *
     *  @param  systemId the scene <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> sceneId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchSystemId(org.osid.id.Id systemId, boolean match) {
        getAssembler().addIdTerm(getSystemIdColumn(), systemId, match);
        return;
    }


    /**
     *  Clears the system <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearSystemIdTerms() {
        getAssembler().clearTerms(getSystemIdColumn());
        return;
    }


    /**
     *  Gets the system <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getSystemIdTerms() {
        return (getAssembler().getIdTerms(getSystemIdColumn()));
    }


    /**
     *  Gets the SystemId column name.
     *
     * @return the column name
     */

    protected String getSystemIdColumn() {
        return ("system_id");
    }


    /**
     *  Tests if a <code> SystemQuery </code> is available. 
     *
     *  @return <code> true </code> if a system query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsSystemQuery() {
        return (false);
    }


    /**
     *  Gets the query for a system. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the system query 
     *  @throws org.osid.UnimplementedException <code> supportsSystemQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.control.SystemQuery getSystemQuery() {
        throw new org.osid.UnimplementedException("supportsSystemQuery() is false");
    }


    /**
     *  Clears the system query terms. 
     */

    @OSID @Override
    public void clearSystemTerms() {
        getAssembler().clearTerms(getSystemColumn());
        return;
    }


    /**
     *  Gets the system query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.control.SystemQueryInspector[] getSystemTerms() {
        return (new org.osid.control.SystemQueryInspector[0]);
    }


    /**
     *  Gets the System column name.
     *
     * @return the column name
     */

    protected String getSystemColumn() {
        return ("system");
    }


    /**
     *  Tests if this scene supports the given record
     *  <code>Type</code>.
     *
     *  @param  sceneRecordType a scene record type 
     *  @return <code>true</code> if the sceneRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type sceneRecordType) {
        for (org.osid.control.records.SceneQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(sceneRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  sceneRecordType the scene record type 
     *  @return the scene query record 
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sceneRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SceneQueryRecord getSceneQueryRecord(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SceneQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(sceneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sceneRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  sceneRecordType the scene record type 
     *  @return the scene query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sceneRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SceneQueryInspectorRecord getSceneQueryInspectorRecord(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SceneQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(sceneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sceneRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param sceneRecordType the scene record type
     *  @return the scene search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(sceneRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.control.records.SceneSearchOrderRecord getSceneSearchOrderRecord(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.control.records.SceneSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(sceneRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(sceneRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this scene. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param sceneQueryRecord the scene query record
     *  @param sceneQueryInspectorRecord the scene query inspector
     *         record
     *  @param sceneSearchOrderRecord the scene search order record
     *  @param sceneRecordType scene record type
     *  @throws org.osid.NullArgumentException
     *          <code>sceneQueryRecord</code>,
     *          <code>sceneQueryInspectorRecord</code>,
     *          <code>sceneSearchOrderRecord</code> or
     *          <code>sceneRecordTypescene</code> is
     *          <code>null</code>
     */
            
    protected void addSceneRecords(org.osid.control.records.SceneQueryRecord sceneQueryRecord, 
                                      org.osid.control.records.SceneQueryInspectorRecord sceneQueryInspectorRecord, 
                                      org.osid.control.records.SceneSearchOrderRecord sceneSearchOrderRecord, 
                                      org.osid.type.Type sceneRecordType) {

        addRecordType(sceneRecordType);

        nullarg(sceneQueryRecord, "scene query record");
        nullarg(sceneQueryInspectorRecord, "scene query inspector record");
        nullarg(sceneSearchOrderRecord, "scene search odrer record");

        this.queryRecords.add(sceneQueryRecord);
        this.queryInspectorRecords.add(sceneQueryInspectorRecord);
        this.searchOrderRecords.add(sceneSearchOrderRecord);
        
        return;
    }
}
