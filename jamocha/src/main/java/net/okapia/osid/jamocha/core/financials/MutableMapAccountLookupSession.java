//
// MutableMapAccountLookupSession
//
//    Implements an Account lookup service backed by a collection of
//    accounts that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.financials;


/**
 *  Implements an Account lookup service backed by a collection of
 *  accounts. The accounts are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of accounts can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapAccountLookupSession
    extends net.okapia.osid.jamocha.core.financials.spi.AbstractMapAccountLookupSession
    implements org.osid.financials.AccountLookupSession {


    /**
     *  Constructs a new {@code MutableMapAccountLookupSession}
     *  with no accounts.
     *
     *  @param business the business
     *  @throws org.osid.NullArgumentException {@code business} is
     *          {@code null}
     */

      public MutableMapAccountLookupSession(org.osid.financials.Business business) {
        setBusiness(business);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAccountLookupSession} with a
     *  single account.
     *
     *  @param business the business  
     *  @param account an account
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code account} is {@code null}
     */

    public MutableMapAccountLookupSession(org.osid.financials.Business business,
                                           org.osid.financials.Account account) {
        this(business);
        putAccount(account);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAccountLookupSession}
     *  using an array of accounts.
     *
     *  @param business the business
     *  @param accounts an array of accounts
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code accounts} is {@code null}
     */

    public MutableMapAccountLookupSession(org.osid.financials.Business business,
                                           org.osid.financials.Account[] accounts) {
        this(business);
        putAccounts(accounts);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapAccountLookupSession}
     *  using a collection of accounts.
     *
     *  @param business the business
     *  @param accounts a collection of accounts
     *  @throws org.osid.NullArgumentException {@code business} or
     *          {@code accounts} is {@code null}
     */

    public MutableMapAccountLookupSession(org.osid.financials.Business business,
                                           java.util.Collection<? extends org.osid.financials.Account> accounts) {

        this(business);
        putAccounts(accounts);
        return;
    }

    
    /**
     *  Makes an {@code Account} available in this session.
     *
     *  @param account an account
     *  @throws org.osid.NullArgumentException {@code account{@code  is
     *          {@code null}
     */

    @Override
    public void putAccount(org.osid.financials.Account account) {
        super.putAccount(account);
        return;
    }


    /**
     *  Makes an array of accounts available in this session.
     *
     *  @param accounts an array of accounts
     *  @throws org.osid.NullArgumentException {@code accounts{@code 
     *          is {@code null}
     */

    @Override
    public void putAccounts(org.osid.financials.Account[] accounts) {
        super.putAccounts(accounts);
        return;
    }


    /**
     *  Makes collection of accounts available in this session.
     *
     *  @param accounts a collection of accounts
     *  @throws org.osid.NullArgumentException {@code accounts{@code  is
     *          {@code null}
     */

    @Override
    public void putAccounts(java.util.Collection<? extends org.osid.financials.Account> accounts) {
        super.putAccounts(accounts);
        return;
    }


    /**
     *  Removes an Account from this session.
     *
     *  @param accountId the {@code Id} of the account
     *  @throws org.osid.NullArgumentException {@code accountId{@code 
     *          is {@code null}
     */

    @Override
    public void removeAccount(org.osid.id.Id accountId) {
        super.removeAccount(accountId);
        return;
    }    
}
