//
// AbstractQueryProvisionableLookupSession.java
//
//    An inline adapter that maps a ProvisionableLookupSession to
//    a ProvisionableQuerySession.
//
//
// Tom Coppeto 
// Okapia 
// 5 March 2012
//
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.provisioning.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An inline adapter that maps a ProvisionableLookupSession to
 *  a ProvisionableQuerySession.
 */

public abstract class AbstractQueryProvisionableLookupSession
    extends net.okapia.osid.jamocha.provisioning.spi.AbstractProvisionableLookupSession
    implements org.osid.provisioning.ProvisionableLookupSession {

      private boolean effectiveonly = false;

    private final org.osid.provisioning.ProvisionableQuerySession session;
    

    /**
     *  Constructs a new AbstractQueryProvisionableLookupSession.
     *
     *  @param querySession the underlying provisionable query session
     *  @throws org.osid.NullArgumentException {@code querySession} is
     *          {@code null}
     */

    protected AbstractQueryProvisionableLookupSession(org.osid.provisioning.ProvisionableQuerySession querySession) {
        nullarg(querySession, "provisionable query session");
        this.session = querySession;
        return;
    }


    /**
     *  Gets the <code>Distributor</code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Distributor Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getDistributorId() {
        return (this.session.getDistributorId());
    }


    /**
     *  Gets the <code>Distributor</code> associated with this 
     *  session.
     *
     *  @return the <code>Distributor</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Distributor getDistributor()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getDistributor());
    }


    /**
     *  Tests if this user can perform <code>Provisionable</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProvisionables() {
        return (this.session.canSearchProvisionables());
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (true);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include provisionables in distributors which are children
     *  of this distributor in the distributor hierarchy.
     */

    @OSID @Override
    public void useFederatedDistributorView() {
        this.session.useFederatedDistributorView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this distributor only.
     */

    @OSID @Override
    public void useIsolatedDistributorView() {
        this.session.useIsolatedDistributorView();
        return;
    }
    

    /**
     *  Only provisionables whose effective dates are current are returned by
     *  methods in this session.
     */

    public void useEffectiveProvisionableView() {
       this.effectiveonly = true;         
       return;
    }


    /**
     *  All provisionables of any effective dates are returned by all
     *  methods in this session.
     */

    public void useAnyEffectiveProvisionableView() {
        this.effectiveonly = false;
        return;
    }


    /**
     *  Tests if an effective or any effective status view is set.
     *
     *  @return <code>true</code> if effective only</code>,
     *          <code>false</code> if both effective and ineffective
     */

    protected boolean isEffectiveOnly() {
        return (this.effectiveonly);
    }

     
    /**
     *  Gets the <code>Provisionable</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Provisionable</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Provisionable</code> and
     *  retained for compatibility.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  @param  provisionableId <code>Id</code> of the
     *          <code>Provisionable</code>
     *  @return the provisionable
     *  @throws org.osid.NotFoundException <code>provisionableId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>provisionableId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.Provisionable getProvisionable(org.osid.id.Id provisionableId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchId(provisionableId, true);
        org.osid.provisioning.ProvisionableList provisionables = this.session.getProvisionablesByQuery(query);
        if (provisionables.hasNext()) {
            return (provisionables.getNextProvisionable());
        } 
        
        throw new org.osid.NotFoundException(provisionableId + " not found");
    }


    /**
     *  Gets a <code>ProvisionableList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  provisionables specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Provisionables</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In effective mode, provisionables are returned that are currently effective.
     *  In any effective mode, effective provisionables and those currently expired
     *  are returned.
     *
     *  @param  provisionableIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Provisionable</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByIds(org.osid.id.IdList provisionableIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();

        try (org.osid.id.IdList ids = provisionableIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                query.matchId(id, true);
            }
        }

        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets a <code>ProvisionableList</code> corresponding to the given
     *  provisionable genus <code>Type</code> which does not include
     *  provisionables of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisionables are returned that are currently effective.
     *  In any effective mode, effective provisionables and those currently expired
     *  are returned.
     *
     *  @param  provisionableGenusType a provisionable genus type 
     *  @return the returned <code>Provisionable</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByGenusType(org.osid.type.Type provisionableGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchGenusType(provisionableGenusType, true);
        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets a <code>ProvisionableList</code> corresponding to the
     *  given provisionable genus <code>Type</code> and include any
     *  additional provisionables with genus types derived from the
     *  specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  provisionableGenusType a provisionable genus type 
     *  @return the returned <code>Provisionable</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByParentGenusType(org.osid.type.Type provisionableGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchParentGenusType(provisionableGenusType, true);
        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets a <code>ProvisionableList</code> containing the given
     *  provisionable record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  provisionableRecordType a provisionable record type 
     *  @return the returned <code>Provisionable</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>provisionableRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesByRecordType(org.osid.type.Type provisionableRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchRecordType(provisionableRecordType, true);
        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets a <code>ProvisionableList</code> effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *  
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>Provisionable</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesOnDate(org.osid.calendaring.DateTime from, 
                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchDate(from, to, true);
        return (this.session.getProvisionablesByQuery(query));
    }
        

    /**
     *  Gets a list of provisionables corresponding to a resource
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionableList getProvisionablesForResource(org.osid.id.Id resourceId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets a list of provisionables corresponding to a resource
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResourceOnDate(org.osid.id.Id resourceId,
                                                                                      org.osid.calendaring.DateTime from,
                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchDate(from, to, true);
        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets a list of provisionables corresponding to a pool
     *  <code>Id</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  poolId the <code>Id</code> of the pool
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>poolId</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

     @OSID @Override
     public org.osid.provisioning.ProvisionableList getProvisionablesForPool(org.osid.id.Id poolId)
         throws org.osid.OperationFailedException,
                org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchPoolId(poolId, true);
        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets a list of provisionables corresponding to a pool
     *  <code>Id</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  poolId the <code>Id</code> of the pool
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>poolId</code>,
     *          <code>from</code> or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForPoolOnDate(org.osid.id.Id poolId,
                                                                                  org.osid.calendaring.DateTime from,
                                                                                  org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchPoolId(poolId, true);
        query.matchDate(from, to, true);
        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets a list of provisionables corresponding to resource and
     *  pool <code>Ids</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned
     *  list may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  poolId the <code>Id</code> of the pool
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>poolId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResourceAndPool(org.osid.id.Id resourceId,
                                                                                       org.osid.id.Id poolId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchPoolId(poolId, true);
        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets a list of provisionables corresponding to resource and pool
     *  <code>Ids</code> and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible
     *  through this session.
     *
     *  In effective mode, provisionables are returned that are
     *  currently effective.  In any effective mode, effective
     *  provisionables and those currently expired are returned.
     *
     *  @param  resourceId the <code>Id</code> of the resource
     *  @param  poolId the <code>Id</code> of the pool
     *  @param  from from date
     *  @param  to to date
     *  @return the returned <code>ProvisionableList</code>
     *  @throws org.osid.NullArgumentException <code>resourceId</code>,
     *          <code>poolId</code>, <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionablesForResourceAndPoolOnDate(org.osid.id.Id resourceId,
                                                                                             org.osid.id.Id poolId,
                                                                                             org.osid.calendaring.DateTime from,
                                                                                             org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchResourceId(resourceId, true);
        query.matchPoolId(poolId, true);
        query.matchDate(from, to, true);
        return (this.session.getProvisionablesByQuery(query));
    }

    
    /**
     *  Gets all <code>Provisionables</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  provisionables or an error results. Otherwise, the returned list
     *  may contain only those provisionables that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In effective mode, provisionables are returned that are currently
     *  effective.  In any effective mode, effective provisionables and
     *  those currently expired are returned.
     *
     *  @return a list of <code>Provisionables</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.provisioning.ProvisionableList getProvisionables()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {


        org.osid.provisioning.ProvisionableQuery query = getQuery();
        query.matchAny(true);
        return (this.session.getProvisionablesByQuery(query));
    }


    /**
     *  Gets the query interface.
     *
     *  @return the query interface
     */

    protected org.osid.provisioning.ProvisionableQuery getQuery() {
        org.osid.provisioning.ProvisionableQuery query = this.session.getProvisionableQuery();
        
        if (isEffectiveOnly()) {
            query.matchEffective(true);
        }

        return (query);
    }
}
