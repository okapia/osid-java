//
// AbstractBallotSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.ballot.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractBallotSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.voting.BallotSearchResults {

    private org.osid.voting.BallotList ballots;
    private final org.osid.voting.BallotQueryInspector inspector;
    private final java.util.Collection<org.osid.voting.records.BallotSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractBallotSearchResults.
     *
     *  @param ballots the result set
     *  @param ballotQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>ballots</code>
     *          or <code>ballotQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractBallotSearchResults(org.osid.voting.BallotList ballots,
                                            org.osid.voting.BallotQueryInspector ballotQueryInspector) {
        nullarg(ballots, "ballots");
        nullarg(ballotQueryInspector, "ballot query inspectpr");

        this.ballots = ballots;
        this.inspector = ballotQueryInspector;

        return;
    }


    /**
     *  Gets the ballot list resulting from a search.
     *
     *  @return a ballot list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.voting.BallotList getBallots() {
        if (this.ballots == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.voting.BallotList ballots = this.ballots;
        this.ballots = null;
	return (ballots);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.voting.BallotQueryInspector getBallotQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  ballot search record <code> Type. </code> This method must
     *  be used to retrieve a ballot implementing the requested
     *  record.
     *
     *  @param ballotSearchRecordType a ballot search 
     *         record type 
     *  @return the ballot search
     *  @throws org.osid.NullArgumentException
     *          <code>ballotSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(ballotSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.records.BallotSearchResultsRecord getBallotSearchResultsRecord(org.osid.type.Type ballotSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.voting.records.BallotSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(ballotSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(ballotSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record ballot search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addBallotRecord(org.osid.voting.records.BallotSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "ballot record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
