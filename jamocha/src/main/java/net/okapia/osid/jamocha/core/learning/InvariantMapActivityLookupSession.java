//
// InvariantMapActivityLookupSession
//
//    Implements an Activity lookup service backed by a fixed collection of
//    activities.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning;


/**
 *  Implements an Activity lookup service backed by a fixed
 *  collection of activities. The activities are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapActivityLookupSession
    extends net.okapia.osid.jamocha.core.learning.spi.AbstractMapActivityLookupSession
    implements org.osid.learning.ActivityLookupSession {


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityLookupSession</code> with no
     *  activities.
     *  
     *  @param objectiveBank the objective bank
     *  @throws org.osid.NullArgumnetException {@code objectiveBank} is
     *          {@code null}
     */

    public InvariantMapActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank) {
        setObjectiveBank(objectiveBank);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityLookupSession</code> with a single
     *  activity.
     *  
     *  @param objectiveBank the objective bank
     *  @param activity an single activity
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code activity} is <code>null</code>
     */

      public InvariantMapActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                               org.osid.learning.Activity activity) {
        this(objectiveBank);
        putActivity(activity);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityLookupSession</code> using an array
     *  of activities.
     *  
     *  @param objectiveBank the objective bank
     *  @param activities an array of activities
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code activities} is <code>null</code>
     */

      public InvariantMapActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                               org.osid.learning.Activity[] activities) {
        this(objectiveBank);
        putActivities(activities);
        return;
    }


    /**
     *  Constructs a new
     *  <code>InvariantMapActivityLookupSession</code> using a
     *  collection of activities.
     *
     *  @param objectiveBank the objective bank
     *  @param activities a collection of activities
     *  @throws org.osid.NullArgumentException {@code objectiveBank} or
     *          {@code activities} is <code>null</code>
     */

      public InvariantMapActivityLookupSession(org.osid.learning.ObjectiveBank objectiveBank,
                                               java.util.Collection<? extends org.osid.learning.Activity> activities) {
        this(objectiveBank);
        putActivities(activities);
        return;
    }
}
