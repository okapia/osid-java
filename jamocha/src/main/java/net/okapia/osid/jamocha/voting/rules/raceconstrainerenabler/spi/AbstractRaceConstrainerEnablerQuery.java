//
// AbstractRaceConstrainerEnablerQuery.java
//
//     A template for making a RaceConstrainerEnabler Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.voting.rules.raceconstrainerenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for race constrainer enablers.
 */

public abstract class AbstractRaceConstrainerEnablerQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQuery
    implements org.osid.voting.rules.RaceConstrainerEnablerQuery {

    private final java.util.Collection<org.osid.voting.rules.records.RaceConstrainerEnablerQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches enablers mapped to the race constrainer. 
     *
     *  @param  raceConstrainerId the race constrainer <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> raceConstrainerId 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchRuledRaceConstrainerId(org.osid.id.Id raceConstrainerId, 
                                            boolean match) {
        return;
    }


    /**
     *  Clears the race constrainer <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRuledRaceConstrainerIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> RaceConstrainerQuery </code> is available. 
     *
     *  @return <code> true </code> if a race constrainer query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuledRaceConstrainerQuery() {
        return (false);
    }


    /**
     *  Gets the query for a race constrainer. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the race constrainer query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuledRaceConstrainerQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.voting.rules.RaceConstrainerQuery getRuledRaceConstrainerQuery() {
        throw new org.osid.UnimplementedException("supportsRuledRaceConstrainerQuery() is false");
    }


    /**
     *  Matches enablers mapped to any race constrainer. 
     *
     *  @param  match <code> true </code> for enablers mapped to any race 
     *          constrainer, <code> false </code> to match enablers mapped to 
     *          no race constrainers 
     */

    @OSID @Override
    public void matchAnyRuledRaceConstrainer(boolean match) {
        return;
    }


    /**
     *  Clears the race constrainer query terms. 
     */

    @OSID @Override
    public void clearRuledRaceConstrainerTerms() {
        return;
    }


    /**
     *  Matches enablers mapped to the polls. 
     *
     *  @param  pollsId the polls <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> pollsId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchPollsId(org.osid.id.Id pollsId, boolean match) {
        return;
    }


    /**
     *  Clears the polls <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearPollsIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> PollsQuery </code> is available. 
     *
     *  @return <code> true </code> if a polls query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsPollsQuery() {
        return (false);
    }


    /**
     *  Gets the query for a polls. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the polls query 
     *  @throws org.osid.UnimplementedException <code> supportsPollsQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.voting.PollsQuery getPollsQuery() {
        throw new org.osid.UnimplementedException("supportsPollsQuery() is false");
    }


    /**
     *  Clears the polls query terms. 
     */

    @OSID @Override
    public void clearPollsTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given race constrainer enabler query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a race constrainer enabler implementing the requested record.
     *
     *  @param raceConstrainerEnablerRecordType a race constrainer enabler record type
     *  @return the race constrainer enabler query record
     *  @throws org.osid.NullArgumentException
     *          <code>raceConstrainerEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(raceConstrainerEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.voting.rules.records.RaceConstrainerEnablerQueryRecord getRaceConstrainerEnablerQueryRecord(org.osid.type.Type raceConstrainerEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.voting.rules.records.RaceConstrainerEnablerQueryRecord record : this.records) {
            if (record.implementsRecordType(raceConstrainerEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(raceConstrainerEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this race constrainer enabler query. 
     *
     *  @param raceConstrainerEnablerQueryRecord race constrainer enabler query record
     *  @param raceConstrainerEnablerRecordType raceConstrainerEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addRaceConstrainerEnablerQueryRecord(org.osid.voting.rules.records.RaceConstrainerEnablerQueryRecord raceConstrainerEnablerQueryRecord, 
                                          org.osid.type.Type raceConstrainerEnablerRecordType) {

        addRecordType(raceConstrainerEnablerRecordType);
        nullarg(raceConstrainerEnablerQueryRecord, "race constrainer enabler query record");
        this.records.add(raceConstrainerEnablerQueryRecord);        
        return;
    }
}
