//
// AbstractSequenceRule.java
//
//     Defines a SequenceRule builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.assessment.authoring.sequencerule.spi;


/**
 *  Defines a <code>SequenceRule</code> builder.
 */

public abstract class AbstractSequenceRuleBuilder<T extends AbstractSequenceRuleBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.assessment.authoring.sequencerule.SequenceRuleMiter sequenceRule;


    /**
     *  Constructs a new <code>AbstractSequenceRuleBuilder</code>.
     *
     *  @param sequenceRule the sequence rule to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractSequenceRuleBuilder(net.okapia.osid.jamocha.builder.assessment.authoring.sequencerule.SequenceRuleMiter sequenceRule) {
        super(sequenceRule);
        this.sequenceRule = sequenceRule;
        return;
    }


    /**
     *  Builds the sequence rule.
     *
     *  @return the new sequence rule
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.assessment.authoring.SequenceRule build() {
        (new net.okapia.osid.jamocha.builder.validator.assessment.authoring.sequencerule.SequenceRuleValidator(getValidations())).validate(this.sequenceRule);
        return (new net.okapia.osid.jamocha.builder.assessment.authoring.sequencerule.ImmutableSequenceRule(this.sequenceRule));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the sequence rule miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.assessment.authoring.sequencerule.SequenceRuleMiter getMiter() {
        return (this.sequenceRule);
    }


    /**
     *  Sets the assessment part.
     *
     *  @param assessmentPart an assessment part
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentPart</code> is <code>null</code>
     */

    public T assessmentPart(org.osid.assessment.authoring.AssessmentPart assessmentPart) {
        getMiter().setAssessmentPart(assessmentPart);
        return (self());
    }


    /**
     *  Sets the next assessment part.
     *
     *  @param nextAssessmentPart a next assessment part
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>nextAssessmentPart</code> is <code>null</code>
     */

    public T nextAssessmentPart(org.osid.assessment.authoring.AssessmentPart nextAssessmentPart) {
        getMiter().setNextAssessmentPart(nextAssessmentPart);
        return (self());
    }


    /**
     *  Sets the minimum score.
     *
     *  @param score a minimum score
     *  @return the builder
     */

    public T minimumScore(long score) {
        getMiter().setMinimumScore(score);
        return (self());
    }


    /**
     *  Sets the maximum score.
     *
     *  @param score a maximum score
     *  @return the builder
     */

    public T maximumScore(long score) {
        getMiter().setMaximumScore(score);
        return (self());
    }


    /**
     *  Sets the cumulative flag.
     *
     *  @return the builder
     */

    public T cumulative() {
        getMiter().setCumulative(true);
        return (self());
    }


    /**
     *  Unsets the cumulative flag.
     *
     *  @return the builder
     */

    public T notcumulative() {
        getMiter().setCumulative(false);
        return (self());
    }


    /**
     *  Adds an applied assessment part.
     *
     *  @param appliedAssessmentPart an applied assessment part
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>appliedAssessmentPart</code> is <code>null</code>
     */

    public T appliedAssessmentPart(org.osid.assessment.authoring.AssessmentPart appliedAssessmentPart) {
        getMiter().addAppliedAssessmentPart(appliedAssessmentPart);
        return (self());
    }


    /**
     *  Sets all the applied assessment parts.
     *
     *  @param appliedAssessmentParts a collection of applied assessment parts
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>appliedAssessmentParts</code> is <code>null</code>
     */

    public T appliedAssessmentParts(java.util.Collection<org.osid.assessment.authoring.AssessmentPart> appliedAssessmentParts) {
        getMiter().setAppliedAssessmentParts(appliedAssessmentParts);
        return (self());
    }


    /**
     *  Adds a SequenceRule record.
     *
     *  @param record a sequence rule record
     *  @param recordType the type of sequence rule record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.assessment.authoring.records.SequenceRuleRecord record, org.osid.type.Type recordType) {
        getMiter().addSequenceRuleRecord(record, recordType);
        return (self());
    }
}       


