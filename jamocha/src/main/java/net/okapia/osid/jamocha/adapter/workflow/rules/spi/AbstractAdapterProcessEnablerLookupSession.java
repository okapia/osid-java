//
// AbstractAdapterProcessEnablerLookupSession.java
//
//    A ProcessEnabler lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A ProcessEnabler lookup session adapter.
 */

public abstract class AbstractAdapterProcessEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.workflow.rules.ProcessEnablerLookupSession {

    private final org.osid.workflow.rules.ProcessEnablerLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterProcessEnablerLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterProcessEnablerLookupSession(org.osid.workflow.rules.ProcessEnablerLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Office/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Office Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.session.getOfficeId());
    }


    /**
     *  Gets the {@code Office} associated with this session.
     *
     *  @return the {@code Office} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getOffice());
    }


    /**
     *  Tests if this user can perform {@code ProcessEnabler} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupProcessEnablers() {
        return (this.session.canLookupProcessEnablers());
    }


    /**
     *  A complete view of the {@code ProcessEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProcessEnablerView() {
        this.session.useComparativeProcessEnablerView();
        return;
    }


    /**
     *  A complete view of the {@code ProcessEnabler} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProcessEnablerView() {
        this.session.usePlenaryProcessEnablerView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include process enablers in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.session.useFederatedOfficeView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.session.useIsolatedOfficeView();
        return;
    }
    

    /**
     *  Only active process enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveProcessEnablerView() {
        this.session.useActiveProcessEnablerView();
        return;
    }


    /**
     *  Active and inactive process enablers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusProcessEnablerView() {
        this.session.useAnyStatusProcessEnablerView();
        return;
    }
    
     
    /**
     *  Gets the {@code ProcessEnabler} specified by its {@code Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a
     *  {@code NOT_FOUND} results. Otherwise, the returned
     *  {@code ProcessEnabler} may have a different {@code Id}
     *  than requested, such as the case where a duplicate
     *  {@code Id} was assigned to a {@code ProcessEnabler} and
     *  retained for compatibility.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @param processEnablerId {@code Id} of the {@code ProcessEnabler}
     *  @return the process enabler
     *  @throws org.osid.NotFoundException {@code processEnablerId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code processEnablerId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnabler getProcessEnabler(org.osid.id.Id processEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProcessEnabler(processEnablerId));
    }


    /**
     *  Gets a {@code ProcessEnablerList} corresponding to the given
     *  {@code IdList}. 
     *
     *  In plenary mode, the returned list contains all of the
     *  processEnablers specified in the {@code Id} list, in the order of
     *  the list, including duplicates, or an error results if an
     *  {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code ProcessEnablers} may be
     *  omitted from the list and may present the elements in any
     *  order including returning a unique set.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @param  processEnablerIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ProcessEnabler} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code processEnablerIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByIds(org.osid.id.IdList processEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProcessEnablersByIds(processEnablerIds));
    }


    /**
     *  Gets a {@code ProcessEnablerList} corresponding to the given
     *  process enabler genus {@code Type} which does not include
     *  process enablers of types derived from the specified
     *  {@code Type}.  
     *
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @param  processEnablerGenusType a processEnabler genus type 
     *  @return the returned {@code ProcessEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code processEnablerGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByGenusType(org.osid.type.Type processEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProcessEnablersByGenusType(processEnablerGenusType));
    }


    /**
     *  Gets a {@code ProcessEnablerList} corresponding to the given
     *  process enabler genus {@code Type} and include any additional
     *  process enablers with genus types derived from the specified
     *  {@code Type}.
     *
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @param  processEnablerGenusType a processEnabler genus type 
     *  @return the returned {@code ProcessEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code processEnablerGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByParentGenusType(org.osid.type.Type processEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProcessEnablersByParentGenusType(processEnablerGenusType));
    }


    /**
     *  Gets a {@code ProcessEnablerList} containing the given
     *  process enabler record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @param  processEnablerRecordType a processEnabler record type 
     *  @return the returned {@code ProcessEnabler} list
     *  @throws org.osid.NullArgumentException
     *          {@code processEnablerRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersByRecordType(org.osid.type.Type processEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProcessEnablersByRecordType(processEnablerRecordType));
    }


    /**
     *  Gets a {@code ProcessEnablerList} effective during the
     *  entire given date range inclusive but not confined to the
     *  date range.
     *  
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible
     *  through this session.
     *  
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ProcessEnabler} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                              org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProcessEnablersOnDate(from, to));
    }
        

    /**
     *  Gets a {@code ProcessEnablerList } which are effective
     *  for the entire given date range inclusive but not confined
     *  to the date range and evaluated against the given agent.
     *
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible
     *  through this session.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned {@code ProcessEnabler} list
     *  @throws org.osid.InvalidArgumentException {@code from} is
     *          greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code agent},
     *          {@code from}, or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                       org.osid.calendaring.DateTime from,
                                                                       org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        
        return (this.session.getProcessEnablersOnDateWithAgent(agentId, from, to));
    }


    /**
     *  Gets all {@code ProcessEnablers}. 
     *
     *  In plenary mode, the returned list contains all known
     *  process enablers or an error results. Otherwise, the returned list
     *  may contain only those process enablers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, process enablers are returned that are currently
     *  active. In any status mode, active and inactive process enablers
     *  are returned.
     *
     *  @return a list of {@code ProcessEnablers} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.ProcessEnablerList getProcessEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getProcessEnablers());
    }
}
