//
// AbstractImmutableQueueConstrainer.java
//
//     Wraps a mutable QueueConstrainer to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.provisioning.rules.queueconstrainer.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>QueueConstrainer</code> to hide modifiers. This
 *  wrapper provides an immutized QueueConstrainer from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying queueConstrainer whose state changes are visible.
 */

public abstract class AbstractImmutableQueueConstrainer
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidConstrainer
    implements org.osid.provisioning.rules.QueueConstrainer {

    private final org.osid.provisioning.rules.QueueConstrainer queueConstrainer;


    /**
     *  Constructs a new <code>AbstractImmutableQueueConstrainer</code>.
     *
     *  @param queueConstrainer the queue constrainer to immutablize
     *  @throws org.osid.NullArgumentException <code>queueConstrainer</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableQueueConstrainer(org.osid.provisioning.rules.QueueConstrainer queueConstrainer) {
        super(queueConstrainer);
        this.queueConstrainer = queueConstrainer;
        return;
    }


    /**
     *  Tests if this queue limits the number of requests. 
     *
     *  @return <code> true </code> if a queue size limit is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSizeLimit() {
        return (this.queueConstrainer.hasSizeLimit());
    }


    /**
     *  Gets the size limit of the queue. 
     *
     *  @return the size limit 
     *  @throws org.osid.IllegalStateException <code> hasSizeLimit() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public long getSizeLimit() {
        return (this.queueConstrainer.getSizeLimit());
    }


    /**
     *  Tests if a provision must exist before entering this queue. 
     *
     *  @return <code> true </code> if a provision is required, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean requiresProvisions() {
        return (this.queueConstrainer.requiresProvisions());
    }


    /**
     *  Gets the pool <code> Ids </code> for the required provisions. 
     *
     *  @return the <code> Ids </code> of the pools 
     *  @throws org.osid.IllegalStateException
     *          <code>requiresProvisions()</code> is <code> false
     *          </code>
     */

    @OSID @Override
    public org.osid.id.IdList getRequiredProvisionPoolIds() {
        return (this.queueConstrainer.getRequiredProvisionPoolIds());
    }


    /**
     *  Gets the pools for the required provisions. 
     *
     *  @return the pools 
     *  @throws org.osid.IllegalStateException
     *          <code>requiresProvisions()</code> is <code> false
     *          </code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.provisioning.PoolList getRequiredProvisionPools()
        throws org.osid.OperationFailedException {

        return (this.queueConstrainer.getRequiredProvisionPools());
    }


    /**
     *  Gets the queue constrainer record corresponding to the given <code> 
     *  QueueConstrainer </code> record <code> Type. </code> This method is 
     *  used to retrieve an object implementing the requested record. The 
     *  <code> queueConstrainerRecordType </code> may be the <code> Type 
     *  </code> returned in <code> getRecordTypes() </code> or any of its 
     *  parents in a <code> Type </code> hierarchy where <code> 
     *  hasRecordType(queueConstrainerRecordType) </code> is <code> true 
     *  </code> . 
     *
     *  @param  queueConstrainerRecordType the type of queue constrainer 
     *          record to retrieve 
     *  @return the queue constrainer record 
     *  @throws org.osid.NullArgumentException <code> 
     *          queueConstrainerRecordType </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(queueConstrainerRecordType) </code> is <code> 
     *          false </code> 
     */

    @OSID @Override
    public org.osid.provisioning.rules.records.QueueConstrainerRecord getQueueConstrainerRecord(org.osid.type.Type queueConstrainerRecordType)
        throws org.osid.OperationFailedException {

        return (this.queueConstrainer.getQueueConstrainerRecord(queueConstrainerRecordType));
    }
}

