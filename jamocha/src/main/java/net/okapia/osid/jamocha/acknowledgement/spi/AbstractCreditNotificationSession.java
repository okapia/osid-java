//
// AbstractCreditNotificationSession.java
//
//     A template for making CreditNotificationSessions.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.acknowledgement.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  This session defines methods to receive notifications on
 *  adds/changes to {@code Credit} objects. This session is
 *  intended for consumers needing to synchronize their state with
 *  this service without the use of polling. Notifications are
 *  cancelled when this session is closed.
 *  
 *  Notifications are triggered with changes to the
 *  {@code Credit} object itself. Adding and removing entries
 *  result in notifications available from the notification session
 *  for credit entries.
 *
 *  The methods in this abstract class do nothing.
 */

public abstract class AbstractCreditNotificationSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.acknowledgement.CreditNotificationSession {

    private boolean federated = false;
    private org.osid.acknowledgement.Billing billing = new net.okapia.osid.jamocha.nil.acknowledgement.billing.UnknownBilling();


    /**
     *  Gets the {@code Billing/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Billing Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */
    
    @OSID @Override
    public org.osid.id.Id getBillingId() {
        return (this.billing.getId());
    }

    
    /**
     *  Gets the {@code Billing} associated with this 
     *  session.
     *
     *  @return the {@code Billing} associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.acknowledgement.Billing getBilling()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.billing);
    }


    /**
     *  Sets the {@code Billing}.
     *
     *  @param  billing the billing for this session
     *  @throws org.osid.NullArgumentException {@code billing}
     *          is {@code null}
     */

    protected void setBilling(org.osid.acknowledgement.Billing billing) {
        nullarg(billing, "billing");
        this.billing = billing;
        return;
    }


    /**
     *  Tests if this user can register for {@code Credit}
     *  notifications.  A return of true does not guarantee successful
     *  authorization. A return of false indicates that it is known
     *  all methods in this session will result in a
     *  {@code PERMISSION_DENIED}. This is intended as a hint to
     *  an application that may opt not to offer notification
     *  operations.
     *
     *  @return {@code false} if notification methods are not
     *          authorized, {@code true} otherwise
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public boolean canRegisterForCreditNotifications() {
        return (true);
    }


    /**
     *  Reliable notifications are desired. In reliable mode,
     *  notifications are to be acknowledged using <code>
     *  acknowledgeCreditNotification() </code>.
     */

    @OSID @Override
    public void reliableCreditNotifications() {
        return;
    }


    /**
     *  Unreliable notifications are desired. In unreliable mode,
     *  notifications do not need to be acknowledged.
     */

    @OSID @Override
    public void unreliableCreditNotifications() {
        return;
    }


    /**
     *  Acknowledge a credit notification.
     *
     *  @param  notificationId the <code> Id </code> of the notification
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void acknowledgeCreditNotification(org.osid.id.Id notificationId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include notifications for credits in billings which
     *  are children of this billing in the billing hierarchy.
     */

    @OSID @Override
    public void useFederatedBillingView() {
        this.federated = false;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts notifications to this billing only.
     */

    @OSID @Override
    public void useIsolatedBillingView() {
        this.federated = false;
        return;
    }


    /**
     *  Tests if a federated view is set.
     *
     *  @return {@codetrue</code> if federated view,
     *          {@codefalse</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Register for notifications of new credits. {@code
     *  CreditReceiver.newCredit()} is invoked when a new {@code
     *  Credit} is created.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForNewCredits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of new credits by the given credit
     *  genus {@code Type.} {@code CreditReceiver.newCredit()
     * } is invoked when a new {@code Credit} appears in
     *  this billing.
     *
     *  @param  creditGenusType the {@code Id} of the reference to 
     *          monitor 
     *  @throws org.osid.NullArgumentException {@code creditGenusType} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForNewCreditsByGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of new credits for the given
     *  resource {@code Id}. {@code CreditReceiver.newCredit()} is
     *  invoked when a new {@code Credit} is created.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewCreditsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /** 
     *  Register for notifications of new credits for the given
     *  reference {@code Id}. {@code CreditReceiver.newCredit()} is
     *  invoked when a new {@code Credit} is created.
     *
     *  @param  referenceId the {@code Id} of the reference to monitor
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForNewCreditsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of updated credits. {@code
     *  CreditReceiver.changedCredit()} is invoked when a credit is
     *  changed.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCredits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of changed credits by the given credit 
     *  gfenus {@code Type.} {@code CreditReceiver.changedCredit() 
     * } is invoked when a {@code Credit} for the reference in 
     *  this billing is changed. 
     *
     *  @param  creditGenusType the genus type of the credit to monitor 
     *  @throws org.osid.NullArgumentException {@code creditGenusType} 
     *          is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForChangedCreditsByGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of updated credits for the given
     *  resource {@code Id}. {@code CreditReceiver.changedCredit()} is
     *  invoked when a {@code Credit} in this billing is changed.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedCreditsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of updated credits for the given
     *  reference {@code Id}. {@code CreditReceiver.changedCredit()}
     *  is invoked when a {@code Credit} in this billing is changed.
     *
     *  @param  referenceId the {@code Id} of the reference to monitor
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForChangedCreditsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of an updated credit. {@code
     *  CreditReceiver.changedCredit()} is invoked when the specified
     *  credit is changed.
     *
     *  @param creditId the {@code Id} of the {@code Credit} 
     *         to monitor
     *  @throws org.osid.NullArgumentException {@code creditId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForChangedCredit(org.osid.id.Id creditId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of deleted credits. {@code
     *  CreditReceiver.deletedCredit()} is invoked when a credit is
     *  deleted.
     *
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCredits()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted credits by the credit
     *  genus {@code Type}. {@code CreditReceiver.deletedCredit()} is
     *  invoked when a {@code Credit} for the reference is removed
     *  from this billing.
     *
     *  @param  creditGenusType the genus type of the credit to monitor 
     *  @throws org.osid.NullArgumentException {@code creditGenusType}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public void registerForDeletedCreditsByGenusType(org.osid.type.Type creditGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return;
    }


    /**
     *  Register for notifications of deleted credits for the given
     *  resource {@code Id}. {@code CreditReceiver.deletedCredit()} is
     *  invoked when a {@code Credit} is deleted or removed from this
     *  billing.
     *
     *  @param  resourceId the {@code Id} of the resource to monitor
     *  @throws org.osid.NullArgumentException {@code resourceId} is
     *         {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */
      
    @OSID @Override
    public void registerForDeletedCreditsForResource(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Register for notifications of deleted credits for the given
     *  reference {@code Id}. {@code CreditReceiver.deletedCredit()}
     *  is invoked when a {@code Credit} is deleted or removed from
     *  this billing.
     *
     *  @param  referenceId the {@code Id} of the reference to monitor
     *  @throws org.osid.NullArgumentException {@code referenceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public void registerForDeletedCreditsForReference(org.osid.id.Id referenceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }


    /**
     *  Registers for notification of a deleted credit. {@code
     *  CreditReceiver.deletedCredit()} is invoked when the specified
     *  credit is deleted.
     *
     *  @param creditId the {@code Id} of the
     *          {@code Credit} to monitor
     *  @throws org.osid.NullArgumentException {@code creditId}
     *          is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public void registerForDeletedCredit(org.osid.id.Id creditId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return;
    }
}
