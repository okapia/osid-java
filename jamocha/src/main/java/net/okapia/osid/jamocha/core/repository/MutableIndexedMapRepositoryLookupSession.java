//
// MutableIndexedMapRepositoryLookupSession
//
//    Implements a Repository lookup service backed by a collection of
//    repositories indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.repository;


/**
 *  Implements a Repository lookup service backed by a collection of
 *  repositories. The repositories are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some repositories may be compatible
 *  with more types than are indicated through these repository
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of repositories can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapRepositoryLookupSession
    extends net.okapia.osid.jamocha.core.repository.spi.AbstractIndexedMapRepositoryLookupSession
    implements org.osid.repository.RepositoryLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRepositoryLookupSession} with no
     *  repositories.
     */

    public MutableIndexedMapRepositoryLookupSession() {
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRepositoryLookupSession} with a
     *  single repository.
     *  
     *  @param  repository a single repository
     *  @throws org.osid.NullArgumentException {@code repository}
     *          is {@code null}
     */

    public MutableIndexedMapRepositoryLookupSession(org.osid.repository.Repository repository) {
        putRepository(repository);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRepositoryLookupSession} using an
     *  array of repositories.
     *
     *  @param  repositories an array of repositories
     *  @throws org.osid.NullArgumentException {@code repositories}
     *          is {@code null}
     */

    public MutableIndexedMapRepositoryLookupSession(org.osid.repository.Repository[] repositories) {
        putRepositories(repositories);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapRepositoryLookupSession} using a
     *  collection of repositories.
     *
     *  @param  repositories a collection of repositories
     *  @throws org.osid.NullArgumentException {@code repositories} is
     *          {@code null}
     */

    public MutableIndexedMapRepositoryLookupSession(java.util.Collection<? extends org.osid.repository.Repository> repositories) {
        putRepositories(repositories);
        return;
    }
    

    /**
     *  Makes a {@code Repository} available in this session.
     *
     *  @param  repository a repository
     *  @throws org.osid.NullArgumentException {@code repository{@code  is
     *          {@code null}
     */

    @Override
    public void putRepository(org.osid.repository.Repository repository) {
        super.putRepository(repository);
        return;
    }


    /**
     *  Makes an array of repositories available in this session.
     *
     *  @param  repositories an array of repositories
     *  @throws org.osid.NullArgumentException {@code repositories{@code 
     *          is {@code null}
     */

    @Override
    public void putRepositories(org.osid.repository.Repository[] repositories) {
        super.putRepositories(repositories);
        return;
    }


    /**
     *  Makes collection of repositories available in this session.
     *
     *  @param  repositories a collection of repositories
     *  @throws org.osid.NullArgumentException {@code repository{@code  is
     *          {@code null}
     */

    @Override
    public void putRepositories(java.util.Collection<? extends org.osid.repository.Repository> repositories) {
        super.putRepositories(repositories);
        return;
    }


    /**
     *  Removes a Repository from this session.
     *
     *  @param repositoryId the {@code Id} of the repository
     *  @throws org.osid.NullArgumentException {@code repositoryId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRepository(org.osid.id.Id repositoryId) {
        super.removeRepository(repositoryId);
        return;
    }    
}
