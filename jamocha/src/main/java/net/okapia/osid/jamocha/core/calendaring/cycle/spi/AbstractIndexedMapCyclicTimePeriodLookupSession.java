//
// AbstractIndexedMapCyclicTimePeriodLookupSession.java
//
//    A simple framework for providing a CyclicTimePeriod lookup service
//    backed by a fixed collection of cyclic time periods with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.calendaring.cycle.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a CyclicTimePeriod lookup service backed by a
 *  fixed collection of cyclic time periods. The cyclic time periods are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some cyclic time periods may be compatible
 *  with more types than are indicated through these cyclic time period
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>CyclicTimePeriods</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapCyclicTimePeriodLookupSession
    extends AbstractMapCyclicTimePeriodLookupSession
    implements org.osid.calendaring.cycle.CyclicTimePeriodLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.calendaring.cycle.CyclicTimePeriod> cyclicTimePeriodsByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.cycle.CyclicTimePeriod>());
    private final MultiMap<org.osid.type.Type, org.osid.calendaring.cycle.CyclicTimePeriod> cyclicTimePeriodsByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.calendaring.cycle.CyclicTimePeriod>());


    /**
     *  Makes a <code>CyclicTimePeriod</code> available in this session.
     *
     *  @param  cyclicTimePeriod a cyclic time period
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriod<code> is
     *          <code>null</code>
     */

    @Override
    protected void putCyclicTimePeriod(org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod) {
        super.putCyclicTimePeriod(cyclicTimePeriod);

        this.cyclicTimePeriodsByGenus.put(cyclicTimePeriod.getGenusType(), cyclicTimePeriod);
        
        try (org.osid.type.TypeList types = cyclicTimePeriod.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.cyclicTimePeriodsByRecord.put(types.getNextType(), cyclicTimePeriod);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a cyclic time period from this session.
     *
     *  @param cyclicTimePeriodId the <code>Id</code> of the cyclic time period
     *  @throws org.osid.NullArgumentException <code>cyclicTimePeriodId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeCyclicTimePeriod(org.osid.id.Id cyclicTimePeriodId) {
        org.osid.calendaring.cycle.CyclicTimePeriod cyclicTimePeriod;
        try {
            cyclicTimePeriod = getCyclicTimePeriod(cyclicTimePeriodId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.cyclicTimePeriodsByGenus.remove(cyclicTimePeriod.getGenusType());

        try (org.osid.type.TypeList types = cyclicTimePeriod.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.cyclicTimePeriodsByRecord.remove(types.getNextType(), cyclicTimePeriod);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeCyclicTimePeriod(cyclicTimePeriodId);
        return;
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> corresponding to the given
     *  cyclic time period genus <code>Type</code> which does not include
     *  cyclic time periods of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known cyclic time periods or an error results. Otherwise,
     *  the returned list may contain only those cyclic time periods that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  cyclicTimePeriodGenusType a cyclic time period genus type 
     *  @return the returned <code>CyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByGenusType(org.osid.type.Type cyclicTimePeriodGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.ArrayCyclicTimePeriodList(this.cyclicTimePeriodsByGenus.get(cyclicTimePeriodGenusType)));
    }


    /**
     *  Gets a <code>CyclicTimePeriodList</code> containing the given
     *  cyclic time period record <code>Type</code>. In plenary mode, the
     *  returned list contains all known cyclic time periods or an error
     *  results. Otherwise, the returned list may contain only those
     *  cyclic time periods that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  cyclicTimePeriodRecordType a cyclic time period record type 
     *  @return the returned <code>cyclicTimePeriod</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>cyclicTimePeriodRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicTimePeriodList getCyclicTimePeriodsByRecordType(org.osid.type.Type cyclicTimePeriodRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.calendaring.cycle.cyclictimeperiod.ArrayCyclicTimePeriodList(this.cyclicTimePeriodsByRecord.get(cyclicTimePeriodRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.cyclicTimePeriodsByGenus.clear();
        this.cyclicTimePeriodsByRecord.clear();

        super.close();

        return;
    }
}
