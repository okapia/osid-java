//
// AbstractImmutableAllocation.java
//
//     Wraps a mutable Allocation to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.filing.allocation.allocation.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Allocation</code> to hide modifiers. This
 *  wrapper provides an immutized Allocation from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying allocation whose state changes are visible.
 */

public abstract class AbstractImmutableAllocation
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableBrowsable
    implements org.osid.filing.allocation.Allocation {

    private final org.osid.filing.allocation.Allocation allocation;


    /**
     *  Constructs a new <code>AbstractImmutableAllocation</code>.
     *
     *  @param allocation the allocation to immutablize
     *  @throws org.osid.NullArgumentException <code>allocation</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAllocation(org.osid.filing.allocation.Allocation allocation) {
        super(allocation);
        this.allocation = allocation;
        return;
    }


    /**
     *  Gets the absolute path to the directory responsible for these usage 
     *  stats. The return may be a partition or volume containing the 
     *  requested directory. 
     *
     *  @return path name 
     */

    @OSID @Override
    public String getDirectoryPath() {
        return (this.allocation.getDirectoryPath());
    }


    /**
     *  Gets the directory responsible for these usage stats. The return may 
     *  be a partition, quota controlled directory, or volume containing the 
     *  requested directory. 
     *
     *  @return directory 
     */

    @OSID @Override
    public org.osid.filing.Directory getDirectory() {
        return (this.allocation.getDirectory());
    }


    /**
     *  Tests if this allocation is assigned to a specific user. 
     *
     *  @return <code> true </code> if this allocation is assigned to a 
     *          specific user, <code> false </code> if this allocation applied 
     *          to all users 
     */

    @OSID @Override
    public boolean isAssignedToUser() {
        return (this.allocation.isAssignedToUser());
    }


    /**
     *  Gets the agent <code> Id </code> of the user. 
     *
     *  @return the agent <code> Id </code> 
     *  @throws org.osid.IllegalStateException <code> isAssignedToUser() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.Id getAgentId() {
        return (this.allocation.getAgentId());
    }


    /**
     *  Gets the agent of the user. 
     *
     *  @return the agent 
     *  @throws org.osid.IllegalStateException <code> isAssignedToUser() 
     *          </code> is <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent()
        throws org.osid.OperationFailedException {

        return (this.allocation.getAgent());
    }


    /**
     *  Gets the total space in bytes of this allocation. 
     *
     *  @return number of bytes in this allocation 
     */

    @OSID @Override
    public long getTotalSpace() {
        return (this.allocation.getTotalSpace());
    }


    /**
     *  Gets the used space in bytes of this allocation. 
     *
     *  @return number of used bytes in this allocation 
     */

    @OSID @Override
    public long getUsedSpace() {
        return (this.allocation.getUsedSpace());
    }


    /**
     *  Gets the available space in bytes of this allocation. 
     *
     *  @return number of available bytes in this allocation 
     */

    @OSID @Override
    public long getAvailableSpace() {
        return (this.allocation.getAvailableSpace());
    }


    /**
     *  Gets the total number of files of this allocation. 
     *
     *  @return number of files in this allocation 
     */

    @OSID @Override
    public long getTotalFiles() {
        return (this.allocation.getTotalFiles());
    }


    /**
     *  Gets the used number of files of this allocation. 
     *
     *  @return number of used files in this allocation 
     */

    @OSID @Override
    public long getUsedFiles() {
        return (this.allocation.getUsedFiles());
    }


    /**
     *  Gets the available number o files of this allocation. 
     *
     *  @return number of available files in this allocation 
     */

    @OSID @Override
    public long getAvailableFiles() {
        return (this.allocation.getAvailableFiles());
    }


    /**
     *  Gets the allocation record corresponding to the given <code> 
     *  Allocation </code> record <code> Type. </code> This method is used to 
     *  retrieve an object implementing the requested record. The <code> 
     *  allocationRecordType </code> may be the <code> Type </code> returned 
     *  in <code> getRecordTypes() </code> or any of its parents in a <code> 
     *  Type </code> hierarchy where <code> 
     *  hasRecordType(allocationRecordType) </code> is <code> true </code> . 
     *
     *  @param  allocationRecordType the allocation record type 
     *  @return the allocation record 
     *  @throws org.osid.NullArgumentException <code> allocationRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(allocationRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.filing.allocation.records.AllocationRecord getAllocationRecord(org.osid.type.Type allocationRecordType)
        throws org.osid.OperationFailedException {

        return (this.allocation.getAllocationRecord(allocationRecordType));
    }
}

