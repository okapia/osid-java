//
// AbstractIndexedMapProficiencyLookupSession.java
//
//    A simple framework for providing a Proficiency lookup service
//    backed by a fixed collection of proficiencies with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Proficiency lookup service backed by a
 *  fixed collection of proficiencies. The proficiencies are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some proficiencies may be compatible
 *  with more types than are indicated through these proficiency
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Proficiencies</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapProficiencyLookupSession
    extends AbstractMapProficiencyLookupSession
    implements org.osid.learning.ProficiencyLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.learning.Proficiency> proficienciesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.learning.Proficiency>());
    private final MultiMap<org.osid.type.Type, org.osid.learning.Proficiency> proficienciesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.learning.Proficiency>());


    /**
     *  Makes a <code>Proficiency</code> available in this session.
     *
     *  @param  proficiency a proficiency
     *  @throws org.osid.NullArgumentException <code>proficiency<code> is
     *          <code>null</code>
     */

    @Override
    protected void putProficiency(org.osid.learning.Proficiency proficiency) {
        super.putProficiency(proficiency);

        this.proficienciesByGenus.put(proficiency.getGenusType(), proficiency);
        
        try (org.osid.type.TypeList types = proficiency.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.proficienciesByRecord.put(types.getNextType(), proficiency);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }


    /**
     *  Removes a proficiency from this session.
     *
     *  @param proficiencyId the <code>Id</code> of the proficiency
     *  @throws org.osid.NullArgumentException <code>proficiencyId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeProficiency(org.osid.id.Id proficiencyId) {
        org.osid.learning.Proficiency proficiency;
        try {
            proficiency = getProficiency(proficiencyId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.proficienciesByGenus.remove(proficiency.getGenusType());

        try (org.osid.type.TypeList types = proficiency.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.proficienciesByRecord.remove(types.getNextType(), proficiency);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeProficiency(proficiencyId);
        return;
    }


    /**
     *  Gets a <code>ProficiencyList</code> corresponding to the given
     *  proficiency genus <code>Type</code> which does not include
     *  proficiencies of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known proficiencies or an error results. Otherwise,
     *  the returned list may contain only those proficiencies that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  proficiencyGenusType a proficiency genus type 
     *  @return the returned <code>Proficiency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByGenusType(org.osid.type.Type proficiencyGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.proficiency.ArrayProficiencyList(this.proficienciesByGenus.get(proficiencyGenusType)));
    }


    /**
     *  Gets a <code>ProficiencyList</code> containing the given
     *  proficiency record <code>Type</code>. In plenary mode, the
     *  returned list contains all known proficiencies or an error
     *  results. Otherwise, the returned list may contain only those
     *  proficiencies that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  proficiencyRecordType a proficiency record type 
     *  @return the returned <code>proficiency</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>proficiencyRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ProficiencyList getProficienciesByRecordType(org.osid.type.Type proficiencyRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.proficiency.ArrayProficiencyList(this.proficienciesByRecord.get(proficiencyRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.proficienciesByGenus.clear();
        this.proficienciesByRecord.clear();

        super.close();

        return;
    }
}
