//
// AbstractGradebookSearchOdrer.java
//
//     Defines a GradebookSearchOrder.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.gradebook.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Defines a {@code GradebookSearchOrder}.
 */

public abstract class AbstractGradebookSearchOrder
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogSearchOrder
    implements org.osid.grading.GradebookSearchOrder {

    private final java.util.Collection<org.osid.grading.records.GradebookSearchOrderRecord> records = new java.util.LinkedHashSet<>();



    /**
     *  Tests if the given record {@code Type} is supported.
     *
     *  @param  gradebookRecordType a gradebook record type 
     *  @return {@code true} if the gradebookRecordType is
     *          supported, {@code false} otherwise
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookRecordType} is 
     *          {@code null}
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradebookRecordType) {
        for (org.osid.grading.records.GradebookSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradebookRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the search odrer record corresponding to the given
     *  {@code Object]} record {@code Type}.
     *
     *  @param  gradebookRecordType the gradebook record type 
     *  @return the gradebook search order record
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookRecordType} is 
     *          {@code null}
     *  @throws org.osid.UnsupportedException
     *          {@code hasRecordType(gradebookRecordType)} is
     *          {@code false}
     */

    @OSID @Override
    public org.osid.grading.records.GradebookSearchOrderRecord getGradebookSearchOrderRecord(org.osid.type.Type gradebookRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradebookSearchOrderRecord record : this.records) {
            if (record.implementsRecordType(gradebookRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradebookRecordType + " is not supported");
    }


    /**
     *  Adds a search order record to this gradebook. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  {@code getRecordTypes}. Additional types may be
     *  registered with this object using
     *  {@code addRecordType()}.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  {@code hasRecordType()}. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  {@code OsidRecord.implememtsRecordType()}. Some types may
     *  be supported in {@code OsidRecords} that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradebookRecord the gradebook search odrer record
     *  @param gradebookRecordType gradebook record type
     *  @throws org.osid.NullArgumentException
     *          {@code gradebookRecord} or
     *          {@code gradebookRecordTypegradebook} is
     *          {@code null}
     */
            
    protected void addGradebookRecord(org.osid.grading.records.GradebookSearchOrderRecord gradebookSearchOrderRecord, 
                                     org.osid.type.Type gradebookRecordType) {

        addRecordType(gradebookRecordType);
        this.records.add(gradebookSearchOrderRecord);
        
        return;
    }
}
