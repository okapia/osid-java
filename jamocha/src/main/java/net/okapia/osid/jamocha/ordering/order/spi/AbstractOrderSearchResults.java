//
// AbstractOrderSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.ordering.order.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractOrderSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.ordering.OrderSearchResults {

    private org.osid.ordering.OrderList orders;
    private final org.osid.ordering.OrderQueryInspector inspector;
    private final java.util.Collection<org.osid.ordering.records.OrderSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractOrderSearchResults.
     *
     *  @param orders the result set
     *  @param orderQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>orders</code>
     *          or <code>orderQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractOrderSearchResults(org.osid.ordering.OrderList orders,
                                            org.osid.ordering.OrderQueryInspector orderQueryInspector) {
        nullarg(orders, "orders");
        nullarg(orderQueryInspector, "order query inspectpr");

        this.orders = orders;
        this.inspector = orderQueryInspector;

        return;
    }


    /**
     *  Gets the order list resulting from a search.
     *
     *  @return an order list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.ordering.OrderList getOrders() {
        if (this.orders == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.ordering.OrderList orders = this.orders;
        this.orders = null;
	return (orders);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.ordering.OrderQueryInspector getOrderQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  order search record <code> Type. </code> This method must
     *  be used to retrieve an order implementing the requested
     *  record.
     *
     *  @param orderSearchRecordType an order search 
     *         record type 
     *  @return the order search
     *  @throws org.osid.NullArgumentException
     *          <code>orderSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(orderSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.ordering.records.OrderSearchResultsRecord getOrderSearchResultsRecord(org.osid.type.Type orderSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.ordering.records.OrderSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(orderSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(orderSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record order search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addOrderRecord(org.osid.ordering.records.OrderSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "order record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
