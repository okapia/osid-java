//
// AbstractQueryRequisiteLookupSession.java
//
//    A RequisiteQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.course.requisite.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A RequisiteQuerySession adapter.
 */

public abstract class AbstractAdapterRequisiteQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.course.requisite.RequisiteQuerySession {

    private final org.osid.course.requisite.RequisiteQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterRequisiteQuerySession.
     *
     *  @param session the underlying requisite query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterRequisiteQuerySession(org.osid.course.requisite.RequisiteQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeCourseCatalog</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeCourseCatalog Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getCourseCatalogId() {
        return (this.session.getCourseCatalogId());
    }


    /**
     *  Gets the {@codeCourseCatalog</code> associated with this 
     *  session.
     *
     *  @return the {@codeCourseCatalog</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.course.CourseCatalog getCourseCatalog()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getCourseCatalog());
    }


    /**
     *  Tests if this user can perform {@codeRequisite</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchRequisites() {
        return (this.session.canSearchRequisites());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include requisites in course catalogs which are children
     *  of this course catalog in the course catalog hierarchy.
     */

    @OSID @Override
    public void useFederatedCourseCatalogView() {
        this.session.useFederatedCourseCatalogView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this course catalog only.
     */
    
    @OSID @Override
    public void useIsolatedCourseCatalogView() {
        this.session.useIsolatedCourseCatalogView();
        return;
    }
    
      
    /**
     *  The returns from the lookup methods omit sequestered
     *  requisites.
     */

    @OSID @Override
    public void useSequesteredRequisiteView() {
        this.session.useSequesteredRequisiteView();
        return;
    }


    /**
     *  All requisites are returned including sequestered requisites.
     */

    @OSID @Override
    public void useUnsequesteredRequisiteView() {
        this.session.useUnsequesteredRequisiteView();
        return;
    }


    /**
     *  Gets a requisite query. The returned query will not have an
     *  extension query.
     *
     *  @return the requisite query 
     */
      
    @OSID @Override
    public org.osid.course.requisite.RequisiteQuery getRequisiteQuery() {
        return (this.session.getRequisiteQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  requisiteQuery the requisite query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code requisiteQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code requisiteQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.course.requisite.RequisiteList getRequisitesByQuery(org.osid.course.requisite.RequisiteQuery requisiteQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getRequisitesByQuery(requisiteQuery));
    }
}
