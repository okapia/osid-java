//
// AbstractJournalingManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.journaling.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractJournalingManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.journaling.JournalingManager,
               org.osid.journaling.JournalingProxyManager {

    private final Types journalEntryRecordTypes            = new TypeRefSet();
    private final Types journalEntrySearchRecordTypes      = new TypeRefSet();

    private final Types branchRecordTypes                  = new TypeRefSet();
    private final Types branchSearchRecordTypes            = new TypeRefSet();

    private final Types journalRecordTypes                 = new TypeRefSet();
    private final Types journalSearchRecordTypes           = new TypeRefSet();


    /**
     *  Constructs a new <code>AbstractJournalingManager</code>.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractJournalingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any journal federation is exposed. Federation is exposed when 
     *  a specific journal may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  journals appears as a single journal. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests for the availability of a journal entry lookup service. 
     *
     *  @return <code> true </code> if journal entry lookup is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryLookup() {
        return (false);
    }


    /**
     *  Tests if querying journal entries is available. 
     *
     *  @return <code> true </code> if journal entry query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryQuery() {
        return (false);
    }


    /**
     *  Tests if searching for journal entries is available. 
     *
     *  @return <code> true </code> if journal entry search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntrySearch() {
        return (false);
    }


    /**
     *  Tests if searching for journal entries is available. 
     *
     *  @return <code> true </code> if journal entry search is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryAdmin() {
        return (false);
    }


    /**
     *  Tests if journal entry notification is available. 
     *
     *  @return <code> true </code> if journal entry notification is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalEntryNotification() {
        return (false);
    }


    /**
     *  Tests if branch lookup is supported. 
     *
     *  @return <code> true </code> if branch lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchLookup() {
        return (false);
    }


    /**
     *  Tests if branch query is supported. 
     *
     *  @return <code> true </code> if branch query is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchQuery() {
        return (false);
    }


    /**
     *  Tests if branch search is supported. 
     *
     *  @return <code> true </code> if branch search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchSearch() {
        return (false);
    }


    /**
     *  Tests if branch administration is supported. 
     *
     *  @return <code> true </code> if branch administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchAdmin() {
        return (false);
    }


    /**
     *  Tests if branch notification is supported. Messages may be sent when 
     *  branchs are created, modified, or deleted. 
     *
     *  @return <code> true </code> if branch notification is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchNotification() {
        return (false);
    }


    /**
     *  Tests if branch smart journals are available. 
     *
     *  @return <code> true </code> if branch smart journals are supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsBranchSmartJournal() {
        return (false);
    }


    /**
     *  Tests for the availability of an journal lookup service. 
     *
     *  @return <code> true </code> if journal lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalLookup() {
        return (false);
    }


    /**
     *  Tests if querying journals is available. 
     *
     *  @return <code> true </code> if journal query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalQuery() {
        return (false);
    }


    /**
     *  Tests if searching for journals is available. 
     *
     *  @return <code> true </code> if journal search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalSearch() {
        return (false);
    }


    /**
     *  Tests for the availability of a journal administrative service for 
     *  creating and deleting journals. 
     *
     *  @return <code> true </code> if journal administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalAdmin() {
        return (false);
    }


    /**
     *  Tests for the availability of a journal notification service. 
     *
     *  @return <code> true </code> if journal notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalNotification() {
        return (false);
    }


    /**
     *  Tests for the availability of a journal hierarchy traversal service. 
     *
     *  @return <code> true </code> if journal hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalHierarchy() {
        return (false);
    }


    /**
     *  Tests for the availability of a journal hierarchy design service. 
     *
     *  @return <code> true </code> if journal hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalHierarchyDesign() {
        return (false);
    }


    /**
     *  Tests for the availability of a journaling batch service. 
     *
     *  @return <code> true </code> if journaling batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsJournalngBatch() {
        return (false);
    }


    /**
     *  Gets the supported <code> JournalEntry </code> record types. 
     *
     *  @return a list containing the supported journal entry record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJournalEntryRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.journalEntryRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> JournalEntry </code> record type is 
     *  supported. 
     *
     *  @param  journalEntryRecordType a <code> Type </code> indicating a 
     *          <code> JournalEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> journalEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJournalEntryRecordType(org.osid.type.Type journalEntryRecordType) {
        return (this.journalEntryRecordTypes.contains(journalEntryRecordType));
    }


    /**
     *  Adds support for a journal entry record type.
     *
     *  @param journalEntryRecordType a journal entry record type
     *  @throws org.osid.NullArgumentException
     *  <code>journalEntryRecordType</code> is <code>null</code>
     */

    protected void addJournalEntryRecordType(org.osid.type.Type journalEntryRecordType) {
        this.journalEntryRecordTypes.add(journalEntryRecordType);
        return;
    }


    /**
     *  Removes support for a journal entry record type.
     *
     *  @param journalEntryRecordType a journal entry record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>journalEntryRecordType</code> is <code>null</code>
     */

    protected void removeJournalEntryRecordType(org.osid.type.Type journalEntryRecordType) {
        this.journalEntryRecordTypes.remove(journalEntryRecordType);
        return;
    }


    /**
     *  Gets the supported journal entry search record types. 
     *
     *  @return a list containing the supported journal entry search record 
     *          types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJournalEntrySearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.journalEntrySearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given journal entry search record type is supported. 
     *
     *  @param  journalEntrySearchRecordType a <code> Type </code> indicating 
     *          a journal entry record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          journalEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJournalEntrySearchRecordType(org.osid.type.Type journalEntrySearchRecordType) {
        return (this.journalEntrySearchRecordTypes.contains(journalEntrySearchRecordType));
    }


    /**
     *  Adds support for a journal entry search record type.
     *
     *  @param journalEntrySearchRecordType a journal entry search record type
     *  @throws org.osid.NullArgumentException
     *  <code>journalEntrySearchRecordType</code> is <code>null</code>
     */

    protected void addJournalEntrySearchRecordType(org.osid.type.Type journalEntrySearchRecordType) {
        this.journalEntrySearchRecordTypes.add(journalEntrySearchRecordType);
        return;
    }


    /**
     *  Removes support for a journal entry search record type.
     *
     *  @param journalEntrySearchRecordType a journal entry search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>journalEntrySearchRecordType</code> is <code>null</code>
     */

    protected void removeJournalEntrySearchRecordType(org.osid.type.Type journalEntrySearchRecordType) {
        this.journalEntrySearchRecordTypes.remove(journalEntrySearchRecordType);
        return;
    }


    /**
     *  Gets all the branch record types supported. 
     *
     *  @return the list of supported branch record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBranchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.branchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given branch record type is supported. 
     *
     *  @param  branchRecordType the branch type 
     *  @return <code> true </code> if the branch record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> branchRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBranchRecordType(org.osid.type.Type branchRecordType) {
        return (this.branchRecordTypes.contains(branchRecordType));
    }


    /**
     *  Adds support for a branch record type.
     *
     *  @param branchRecordType a branch record type
     *  @throws org.osid.NullArgumentException
     *  <code>branchRecordType</code> is <code>null</code>
     */

    protected void addBranchRecordType(org.osid.type.Type branchRecordType) {
        this.branchRecordTypes.add(branchRecordType);
        return;
    }


    /**
     *  Removes support for a branch record type.
     *
     *  @param branchRecordType a branch record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>branchRecordType</code> is <code>null</code>
     */

    protected void removeBranchRecordType(org.osid.type.Type branchRecordType) {
        this.branchRecordTypes.remove(branchRecordType);
        return;
    }


    /**
     *  Gets all the branch search record types supported. 
     *
     *  @return the list of supported branch search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getBranchSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.branchSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if a given branch search type is supported. 
     *
     *  @param  branchSearchRecordType the branch search type 
     *  @return <code> true </code> if the branch search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> branchSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsBranchSearchRecordType(org.osid.type.Type branchSearchRecordType) {
        return (this.branchSearchRecordTypes.contains(branchSearchRecordType));
    }


    /**
     *  Adds support for a branch search record type.
     *
     *  @param branchSearchRecordType a branch search record type
     *  @throws org.osid.NullArgumentException
     *  <code>branchSearchRecordType</code> is <code>null</code>
     */

    protected void addBranchSearchRecordType(org.osid.type.Type branchSearchRecordType) {
        this.branchSearchRecordTypes.add(branchSearchRecordType);
        return;
    }


    /**
     *  Removes support for a branch search record type.
     *
     *  @param branchSearchRecordType a branch search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>branchSearchRecordType</code> is <code>null</code>
     */

    protected void removeBranchSearchRecordType(org.osid.type.Type branchSearchRecordType) {
        this.branchSearchRecordTypes.remove(branchSearchRecordType);
        return;
    }


    /**
     *  Gets the supported <code> Journal </code> record types. 
     *
     *  @return a list containing the supported journal record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJournalRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.journalRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given <code> Journal </code> record type is supported. 
     *
     *  @param  journalRecordType a <code> Type </code> indicating a <code> 
     *          Journal </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> journalRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJournalRecordType(org.osid.type.Type journalRecordType) {
        return (this.journalRecordTypes.contains(journalRecordType));
    }


    /**
     *  Adds support for a journal record type.
     *
     *  @param journalRecordType a journal record type
     *  @throws org.osid.NullArgumentException
     *  <code>journalRecordType</code> is <code>null</code>
     */

    protected void addJournalRecordType(org.osid.type.Type journalRecordType) {
        this.journalRecordTypes.add(journalRecordType);
        return;
    }


    /**
     *  Removes support for a journal record type.
     *
     *  @param journalRecordType a journal record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>journalRecordType</code> is <code>null</code>
     */

    protected void removeJournalRecordType(org.osid.type.Type journalRecordType) {
        this.journalRecordTypes.remove(journalRecordType);
        return;
    }


    /**
     *  Gets the supported journal search record types. 
     *
     *  @return a list containing the supported journal search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getJournalSearchRecordTypes() {
        return (new net.okapia.osid.jamocha.type.type.ArrayTypeList(this.journalSearchRecordTypes.toCollection()));
    }


    /**
     *  Tests if the given journal search record type is supported. 
     *
     *  @param  journalSearchRecordType a <code> Type </code> indicating a 
     *          journal record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> 
     *          journalEntrySearchRecordType </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsJournalSearchRecordType(org.osid.type.Type journalSearchRecordType) {
        return (this.journalSearchRecordTypes.contains(journalSearchRecordType));
    }


    /**
     *  Adds support for a journal search record type.
     *
     *  @param journalSearchRecordType a journal search record type
     *  @throws org.osid.NullArgumentException
     *  <code>journalSearchRecordType</code> is <code>null</code>
     */

    protected void addJournalSearchRecordType(org.osid.type.Type journalSearchRecordType) {
        this.journalSearchRecordTypes.add(journalSearchRecordType);
        return;
    }


    /**
     *  Removes support for a journal search record type.
     *
     *  @param journalSearchRecordType a journal search record type
     *
     *  @throws org.osid.NullArgumentException
     *  <code>journalSearchRecordType</code> is <code>null</code>
     */

    protected void removeJournalSearchRecordType(org.osid.type.Type journalSearchRecordType) {
        this.journalSearchRecordTypes.remove(journalSearchRecordType);
        return;
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  lookup service. 
     *
     *  @return a <code> JournalEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryLookupSession getJournalEntryLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  lookup service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryLookupSession getJournalEntryLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntryLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  lookup service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @return a <code> JournalEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryLookupSession getJournalEntryLookupSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntryLookupSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  lookup service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryLookupSession getJournalEntryLookupSessionForJournal(org.osid.id.Id journalId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntryLookupSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  query service. 
     *
     *  @return a <code> JournalEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuerySession getJournalEntryQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  query service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuerySession getJournalEntryQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntryQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  query service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @return a <code> JournalEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuerySession getJournalEntryQuerySessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntryQuerySessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  query service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryQuery() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryQuerySession getJournalEntryQuerySessionForJournal(org.osid.id.Id journalId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntryQuerySessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  search service. 
     *
     *  @return a <code> JournalEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchSession getJournalEntrySearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  search service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntrySearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchSession getJournalEntrySearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntrySearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  search service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @return a <code> JournalEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchSession getJournalEntrySearchSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntrySearchSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  search service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntrySearchSession getJournalEntrySearchSessionForJournal(org.osid.id.Id journalId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntrySearchSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  administration service. 
     *
     *  @return a <code> JournalEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryAdminSession getJournalEntryAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryAdmin() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryAdminSession getJournalEntryAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntryAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  administration service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @return a <code> JournalEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryAdminSession getJournalEntryAdminSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntryAdminSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  administration service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryAdminSession getJournalEntryAdminSessionForJournal(org.osid.id.Id journalId, 
                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntryAdminSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  notification service. 
     *
     *  @param  journalEntryReceiver the receiver 
     *  @return a <code> JournalEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> journalEntryReceiver 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryNotificationSession getJournalEntryNotificationSession(org.osid.journaling.JournalEntryReceiver journalEntryReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  notification service. 
     *
     *  @param  journalEntryReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> journalEntryReceiver 
     *          </code> or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryNotification() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryNotificationSession getJournalEntryNotificationSession(org.osid.journaling.JournalEntryReceiver journalEntryReceiver, 
                                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntryNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  notification service for the given journal. 
     *
     *  @param  journalEntryReceiver the receiver 
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @return a <code> JournalEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalEntryReceiver 
     *          </code> or <code> journalId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryNotificationSession getJournalEntryNotificationSessionForJournal(org.osid.journaling.JournalEntryReceiver journalEntryReceiver, 
                                                                                                            org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalEntryNotificationSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal entry 
     *  notification service for the given journal. 
     *
     *  @param  journalEntryReceiver the receiver 
     *  @param  journalId the <code> Id </code> of the <code> Journal </code> 
     *  @param  proxy a proxy 
     *  @return a <code> JournalEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Journal </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> journalEntryReceiver, 
     *          journalId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalEntryNotificationSession getJournalEntryNotificationSessionForJournal(org.osid.journaling.JournalEntryReceiver journalEntryReceiver, 
                                                                                                            org.osid.id.Id journalId, 
                                                                                                            org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalEntryNotificationSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branch lookup 
     *  service. 
     *
     *  @return <code> a BranchLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchLookupSession getBranchLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branch lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BranchLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchLookupSession getBranchLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branch lookup 
     *  service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @return <code> a BranchLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsBranchLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchLookupSession getBranchLookupSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchLookupSessionForJournal not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the branch lookup 
     *  service for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy <code> a proxy </code> 
     *  @return <code> a BranchLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsBranchLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchLookupSession getBranchLookupSessionForJournal(org.osid.id.Id journalId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchLookupSessionForJournal not implemented");
    }


    /**
     *  Gets a branch query session. 
     *
     *  @return <code> a BranchQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuerySession getBranchQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchQuerySession not implemented");
    }


    /**
     *  Gets a branch query session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BranchQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuerySession getBranchQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchQuerySession not implemented");
    }


    /**
     *  Gets a branch query session for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @return <code> a BranchQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuerySession getBranchQuerySessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchQuerySessionForJournal not implemented");
    }


    /**
     *  Gets a branch query session for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return <code> a BranchQuerySession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsBranchQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchQuerySession getBranchQuerySessionForJournal(org.osid.id.Id journalId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchQuerySessionForJournal not implemented");
    }


    /**
     *  Gets a branch search session. 
     *
     *  @return <code> a BranchSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSearchSession getBranchSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchSearchSession not implemented");
    }


    /**
     *  Gets a branch search session. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BranchSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSearchSession getBranchSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchSearchSession not implemented");
    }


    /**
     *  Gets a branch search session for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @return <code> a BranchSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsBranchSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSearchSession getBranchSearchSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchSearchSessionForJournal not implemented");
    }


    /**
     *  Gets a branch search session for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return <code> a BranchSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsBranchSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSearchSession getBranchSearchSessionForJournal(org.osid.id.Id journalId, 
                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchSearchSessionForJournal not implemented");
    }


    /**
     *  Gets a branch administration session for creating, updating and 
     *  deleting branches. 
     *
     *  @return <code> a BranchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchAdminSession getBranchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchAdminSession not implemented");
    }


    /**
     *  Gets a branch administration session for creating, updating and 
     *  deleting branches. 
     *
     *  @param  proxy a proxy 
     *  @return <code> a BranchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchAdminSession getBranchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchAdminSession not implemented");
    }


    /**
     *  Gets a branch administration session for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @return <code> a BranchAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchAdminSession getBranchAdminSessionForJournal(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchAdminSessionForJournal not implemented");
    }


    /**
     *  Gets a branch administration session for the given journal. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return <code> a BranchAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsBranchAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchAdminSession getBranchAdminSessionForJournal(org.osid.id.Id journalId, 
                                                                                  org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchAdminSessionForJournal not implemented");
    }


    /**
     *  Gets the notification session for notifications pertaining to branch 
     *  changes. 
     *
     *  @param  branchReceiver the notification callback 
     *  @return <code> a BranchNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> branchReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchNotificationSession getBranchNotificationSession(org.osid.journaling.BranchReceiver branchReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchNotificationSession not implemented");
    }


    /**
     *  Gets the branch notification session for the given journal. 
     *
     *  @param  branchReceiver notification callback 
     *  @param  proxy a proxy 
     *  @return <code> a BranchNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> branchReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchNotificationSession getBranchNotificationSession(org.osid.journaling.BranchReceiver branchReceiver, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchNotificationSession not implemented");
    }


    /**
     *  Gets the branch notification session for the given journal. 
     *
     *  @param  branchReceiver the notification callback 
     *  @param  journalId the <code> Id </code> of the journal 
     *  @return <code> a BranchNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> branchReceiver </code> 
     *          or <code> journalId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchNotificationSession getBranchNotificationSessionForJournal(org.osid.journaling.BranchReceiver branchReceiver, 
                                                                                                org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchNotificationSessionForJournal not implemented");
    }


    /**
     *  Gets the branch notification session for the given journal. 
     *
     *  @param  branchReceiver notification callback 
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return <code> a BranchNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> branchReceiver, 
     *          journalId </code> or <code> proxy </code> is <code> null 
     *          </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchNotificationSession getBranchNotificationSessionForJournal(org.osid.journaling.BranchReceiver branchReceiver, 
                                                                                                org.osid.id.Id journalId, 
                                                                                                org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchNotificationSessionForJournal not implemented");
    }


    /**
     *  Gets the session for managing dynamic branch journals. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @return a <code> BranchSmartJournalSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchSmartJournal() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSmartJournalSession getBranchSmartJournalSession(org.osid.id.Id journalId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getBranchSmartJournalSession not implemented");
    }


    /**
     *  Gets the session for managing dynamic branch journals. 
     *
     *  @param  journalId the <code> Id </code> of the journal 
     *  @param  proxy a proxy 
     *  @return a <code> BranchSmartJournalSession </code> 
     *  @throws org.osid.NotFoundException <code> journalId </code> not found 
     *  @throws org.osid.NullArgumentException <code> journalId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsBranchSmartJournal() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.BranchSmartJournalSession getBranchSmartJournalSession(org.osid.id.Id journalId, 
                                                                                      org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getBranchSmartJournalSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal lookup 
     *  service. 
     *
     *  @return a <code> JournalLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalLookupSession getJournalLookupSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal lookup 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalLookupSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalLookupSession getJournalLookupSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalLookupSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal query 
     *  service. 
     *
     *  @return a <code> JournalQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalQuerySession getJournalQuerySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal query 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalQuerySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalQuerySession getJournalQuerySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalQuerySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal search 
     *  service. 
     *
     *  @return a <code> JournalSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalSearchSession getJournalSearchSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal search 
     *  service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalSearchSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalSearchSession getJournalSearchSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalSearchSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  administrative service. 
     *
     *  @return a <code> JournalAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalAdminSession getJournalAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  administrative service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsJournalAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalAdminSession getJournalAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  notification service. 
     *
     *  @param  journalReceiver the receiver 
     *  @return a <code> JournalNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> journalReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalNotificationSession getJournalNotificationSession(org.osid.journaling.JournalReceiver journalReceiver)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  notification service. 
     *
     *  @param  journalReceiver the receiver 
     *  @param  proxy a proxy 
     *  @return a <code> JournalNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> journalReceiver </code> 
     *          or <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalNotificationSession getJournalNotificationSession(org.osid.journaling.JournalReceiver journalReceiver, 
                                                                                        org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalNotificationSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  hierarchy service. 
     *
     *  @return a <code> JournalHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalHierarchySession getJournalHierarchySession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  hierarchy service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalHierarchySession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalHierarchySession getJournalHierarchySession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalHierarchySession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  hierarchy design service. 
     *
     *  @return a <code> JournalHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalHierarchyDesignSession getJournalHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalHierarchyDesignSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the journal 
     *  hierarchy design service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> JournalHierarchyDesignSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.journaling.JournalHierarchyDesignSession getJournalHierarchyDesignSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalHierarchyDesignSession not implemented");
    }


    /**
     *  Gets a <code> JournalingBatchManager. </code> 
     *
     *  @return a <code> JournalingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalingBatchManager getJournalingBatchManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingManager.getJournalingBatchManager not implemented");
    }


    /**
     *  Gets a <code> JournalingBatchProxyManager. </code> 
     *
     *  @return a <code> JournalingBatchProxyManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsJournalingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.journaling.batch.JournalingBatchProxyManager getJournalingBatchProxyManager()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.journaling.JournalingProxyManager.getJournalingBatchProxyManager not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        this.journalEntryRecordTypes.clear();
        this.journalEntryRecordTypes.clear();

        this.journalEntrySearchRecordTypes.clear();
        this.journalEntrySearchRecordTypes.clear();

        this.branchRecordTypes.clear();
        this.branchRecordTypes.clear();

        this.branchSearchRecordTypes.clear();
        this.branchSearchRecordTypes.clear();

        this.journalRecordTypes.clear();
        this.journalRecordTypes.clear();

        this.journalSearchRecordTypes.clear();
        this.journalSearchRecordTypes.clear();

        return;
    }
}
