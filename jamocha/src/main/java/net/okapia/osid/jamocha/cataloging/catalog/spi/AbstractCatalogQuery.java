//
// AbstractCatalogQuery.java
//
//     A template for making a Catalog Query.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.catalog.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query for catalogs.
 */

public abstract class AbstractCatalogQuery    
    extends net.okapia.osid.jamocha.spi.AbstractOsidCatalogQuery
    implements org.osid.cataloging.CatalogQuery {

    private final java.util.Collection<org.osid.cataloging.records.CatalogQueryRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Matches an <code> Id </code> in this catalog. Multiple Ids are treated 
     *  as a boolean <code> OR. </code> 
     *
     *  @param  id <code> Id </code> to match 
     *  @param  match <code> true </code> if for a positive match, <code> 
     *          false </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> id </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void matchId(org.osid.id.Id id, boolean match) {
        return;
    }


    /**
     *  Matches catalogs that have any <code> Id </code> mapping. 
     *
     *  @param  match <code> true </code> to match catalogs with any <code> Id 
     *          </code> mapping, <code> false </code> to match catalogs with 
     *          no <code> Id </code> mapping 
     */

    @OSID @Override
    public void matchAnyId(boolean match) {
        return;
    }


    /**
     *  Clears the <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIdTerms() {
        return;
    }


    /**
     *  Sets the catalog <code> Id </code> for this query to match catalogs 
     *  that have the specified catalog as an ancestor. 
     *
     *  @param  catalogId a catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAncestorCatalogId(org.osid.id.Id catalogId, boolean match) {
        return;
    }


    /**
     *  Clears the ancestor <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAncestorCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAncestorCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalog. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsAncestorCatalogQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuery getAncestorCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsAncestorCatalogQuery() is false");
    }


    /**
     *  Matches catalogs with any ancestor. 
     *
     *  @param  match <code> true </code> to match catalogs with any ancestor, 
     *          <code> false </code> to match root catalogs 
     */

    @OSID @Override
    public void matchAnyAncestorCatalog(boolean match) {
        return;
    }


    /**
     *  Clears the ancestor query terms. 
     */

    @OSID @Override
    public void clearAncestorCatalogTerms() {
        return;
    }


    /**
     *  Sets the catalog <code> Id </code> for this query to match catalogs 
     *  that have the specified catalog as an descendant. 
     *
     *  @param  catalogId a catalog <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> catalogId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchDescendantCatalogId(org.osid.id.Id catalogId, 
                                         boolean match) {
        return;
    }


    /**
     *  Clears the descendant <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearDescendantCatalogIdTerms() {
        return;
    }


    /**
     *  Tests if a <code> CatalogQuery </code> is available. 
     *
     *  @return <code> true </code> if a catalog query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsDescendantCatalogQuery() {
        return (false);
    }


    /**
     *  Gets the query for a catalog. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the catalog query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsDescendantCatalogQuery() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQuery getDescendantCatalogQuery() {
        throw new org.osid.UnimplementedException("supportsDescendantCatalogQuery() is false");
    }


    /**
     *  Matches catalogs with any descendant. 
     *
     *  @param  match <code> true </code> to match catalogs with any 
     *          descendant, <code> false </code> to match leaf catalogs 
     */

    @OSID @Override
    public void matchAnyDescendantCatalog(boolean match) {
        return;
    }


    /**
     *  Clears the descendant query terms. 
     */

    @OSID @Override
    public void clearDescendantCatalogTerms() {
        return;
    }



    /**
     *  Gets the record corresponding to the given catalog query
     *  record <code> Type. </code> This method must be used to
     *  retrieve a catalog implementing the requested record.
     *
     *  @param catalogRecordType a catalog record type
     *  @return the catalog query record
     *  @throws org.osid.NullArgumentException
     *          <code>catalogRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.records.CatalogQueryRecord getCatalogQueryRecord(org.osid.type.Type catalogRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.records.CatalogQueryRecord record : this.records) {
            if (record.implementsRecordType(catalogRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogRecordType + " is not supported");
    }


    /**
     *  Adds a record to this catalog query. 
     *
     *  @param catalogQueryRecord catalog query record
     *  @param catalogRecordType catalog record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCatalogQueryRecord(org.osid.cataloging.records.CatalogQueryRecord catalogQueryRecord, 
                                          org.osid.type.Type catalogRecordType) {

        addRecordType(catalogRecordType);
        nullarg(catalogQueryRecord, "catalog query record");
        this.records.add(catalogQueryRecord);        
        return;
    }
}
