//
// AbstractMapPriceScheduleLookupSession
//
//    A simple framework for providing a PriceSchedule lookup service
//    backed by a fixed collection of price schedules.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.ordering.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a PriceSchedule lookup service backed by a
 *  fixed collection of price schedules. The price schedules are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>PriceSchedules</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapPriceScheduleLookupSession
    extends net.okapia.osid.jamocha.ordering.spi.AbstractPriceScheduleLookupSession
    implements org.osid.ordering.PriceScheduleLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.ordering.PriceSchedule> priceSchedules = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.ordering.PriceSchedule>());


    /**
     *  Makes a <code>PriceSchedule</code> available in this session.
     *
     *  @param  priceSchedule a price schedule
     *  @throws org.osid.NullArgumentException <code>priceSchedule<code>
     *          is <code>null</code>
     */

    protected void putPriceSchedule(org.osid.ordering.PriceSchedule priceSchedule) {
        this.priceSchedules.put(priceSchedule.getId(), priceSchedule);
        return;
    }


    /**
     *  Makes an array of price schedules available in this session.
     *
     *  @param  priceSchedules an array of price schedules
     *  @throws org.osid.NullArgumentException <code>priceSchedules<code>
     *          is <code>null</code>
     */

    protected void putPriceSchedules(org.osid.ordering.PriceSchedule[] priceSchedules) {
        putPriceSchedules(java.util.Arrays.asList(priceSchedules));
        return;
    }


    /**
     *  Makes a collection of price schedules available in this session.
     *
     *  @param  priceSchedules a collection of price schedules
     *  @throws org.osid.NullArgumentException <code>priceSchedules<code>
     *          is <code>null</code>
     */

    protected void putPriceSchedules(java.util.Collection<? extends org.osid.ordering.PriceSchedule> priceSchedules) {
        for (org.osid.ordering.PriceSchedule priceSchedule : priceSchedules) {
            this.priceSchedules.put(priceSchedule.getId(), priceSchedule);
        }

        return;
    }


    /**
     *  Removes a PriceSchedule from this session.
     *
     *  @param  priceScheduleId the <code>Id</code> of the price schedule
     *  @throws org.osid.NullArgumentException <code>priceScheduleId<code> is
     *          <code>null</code>
     */

    protected void removePriceSchedule(org.osid.id.Id priceScheduleId) {
        this.priceSchedules.remove(priceScheduleId);
        return;
    }


    /**
     *  Gets the <code>PriceSchedule</code> specified by its <code>Id</code>.
     *
     *  @param  priceScheduleId <code>Id</code> of the <code>PriceSchedule</code>
     *  @return the priceSchedule
     *  @throws org.osid.NotFoundException <code>priceScheduleId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>priceScheduleId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceSchedule getPriceSchedule(org.osid.id.Id priceScheduleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.ordering.PriceSchedule priceSchedule = this.priceSchedules.get(priceScheduleId);
        if (priceSchedule == null) {
            throw new org.osid.NotFoundException("priceSchedule not found: " + priceScheduleId);
        }

        return (priceSchedule);
    }


    /**
     *  Gets all <code>PriceSchedules</code>. In plenary mode, the returned
     *  list contains all known priceSchedules or an error
     *  results. Otherwise, the returned list may contain only those
     *  priceSchedules that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>PriceSchedules</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.ordering.PriceScheduleList getPriceSchedules()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.ordering.priceschedule.ArrayPriceScheduleList(this.priceSchedules.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.priceSchedules.clear();
        super.close();
        return;
    }
}
