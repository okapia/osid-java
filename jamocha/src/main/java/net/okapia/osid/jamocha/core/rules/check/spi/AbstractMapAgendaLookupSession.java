//
// AbstractMapAgendaLookupSession
//
//    A simple framework for providing an Agenda lookup service
//    backed by a fixed collection of agendas.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.rules.check.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an Agenda lookup service backed by a
 *  fixed collection of agendas. The agendas are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Agendas</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapAgendaLookupSession
    extends net.okapia.osid.jamocha.rules.check.spi.AbstractAgendaLookupSession
    implements org.osid.rules.check.AgendaLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.rules.check.Agenda> agendas = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.rules.check.Agenda>());


    /**
     *  Makes an <code>Agenda</code> available in this session.
     *
     *  @param  agenda an agenda
     *  @throws org.osid.NullArgumentException <code>agenda<code>
     *          is <code>null</code>
     */

    protected void putAgenda(org.osid.rules.check.Agenda agenda) {
        this.agendas.put(agenda.getId(), agenda);
        return;
    }


    /**
     *  Makes an array of agendas available in this session.
     *
     *  @param  agendas an array of agendas
     *  @throws org.osid.NullArgumentException <code>agendas<code>
     *          is <code>null</code>
     */

    protected void putAgendas(org.osid.rules.check.Agenda[] agendas) {
        putAgendas(java.util.Arrays.asList(agendas));
        return;
    }


    /**
     *  Makes a collection of agendas available in this session.
     *
     *  @param  agendas a collection of agendas
     *  @throws org.osid.NullArgumentException <code>agendas<code>
     *          is <code>null</code>
     */

    protected void putAgendas(java.util.Collection<? extends org.osid.rules.check.Agenda> agendas) {
        for (org.osid.rules.check.Agenda agenda : agendas) {
            this.agendas.put(agenda.getId(), agenda);
        }

        return;
    }


    /**
     *  Removes an Agenda from this session.
     *
     *  @param  agendaId the <code>Id</code> of the agenda
     *  @throws org.osid.NullArgumentException <code>agendaId<code> is
     *          <code>null</code>
     */

    protected void removeAgenda(org.osid.id.Id agendaId) {
        this.agendas.remove(agendaId);
        return;
    }


    /**
     *  Gets the <code>Agenda</code> specified by its <code>Id</code>.
     *
     *  @param  agendaId <code>Id</code> of the <code>Agenda</code>
     *  @return the agenda
     *  @throws org.osid.NotFoundException <code>agendaId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>agendaId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.Agenda getAgenda(org.osid.id.Id agendaId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.rules.check.Agenda agenda = this.agendas.get(agendaId);
        if (agenda == null) {
            throw new org.osid.NotFoundException("agenda not found: " + agendaId);
        }

        return (agenda);
    }


    /**
     *  Gets all <code>Agendas</code>. In plenary mode, the returned
     *  list contains all known agendas or an error
     *  results. Otherwise, the returned list may contain only those
     *  agendas that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Agendas</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.rules.check.AgendaList getAgendas()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.rules.check.agenda.ArrayAgendaList(this.agendas.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.agendas.clear();
        super.close();
        return;
    }
}
