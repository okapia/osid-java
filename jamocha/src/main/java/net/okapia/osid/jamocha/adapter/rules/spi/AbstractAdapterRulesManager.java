//
// AbstractRulesManager.java
//
//     An adapter for a RulesManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a RulesManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterRulesManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.rules.RulesManager>
    implements org.osid.rules.RulesManager {


    /**
     *  Constructs a new {@code AbstractAdapterRulesManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterRulesManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterRulesManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterRulesManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if any engine federation is exposed. Federation is exposed when 
     *  a specific engine may be identified, selected and used to create a 
     *  lookup or admin session. Federation is not exposed when a set of 
     *  engines appears as a single engine. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if rule evaluation is supported. 
     *
     *  @return <code> true </code> if rule evaluation is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRules() {
        return (getAdapteeManager().supportsRules());
    }


    /**
     *  Tests for the availability of a rule lookup service. 
     *
     *  @return <code> true </code> if rule lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleLookup() {
        return (getAdapteeManager().supportsRuleLookup());
    }


    /**
     *  Tests if querying rules is available. 
     *
     *  @return <code> true </code> if rule query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleQuery() {
        return (getAdapteeManager().supportsRuleQuery());
    }


    /**
     *  Tests if searching for rules is available. 
     *
     *  @return <code> true </code> if rule search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleSearch() {
        return (getAdapteeManager().supportsRuleSearch());
    }


    /**
     *  Tests if managing rules is available. 
     *
     *  @return <code> true </code> if rule admin is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleAdmin() {
        return (getAdapteeManager().supportsRuleAdmin());
    }


    /**
     *  Tests if rule notification is available. 
     *
     *  @return <code> true </code> if rule notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleNotification() {
        return (getAdapteeManager().supportsRuleNotification());
    }


    /**
     *  Tests if rule cataloging is available. 
     *
     *  @return <code> true </code> if rule cataloging is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleEngine() {
        return (getAdapteeManager().supportsRuleEngine());
    }


    /**
     *  Tests if a rule cataloging assignment service is supported. A rule 
     *  cataloging service maps rules to engines. 
     *
     *  @return <code> true </code> if rule cataloging is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleEngineAssignment() {
        return (getAdapteeManager().supportsRuleEngineAssignment());
    }


    /**
     *  Tests if rule smart engines is available. 
     *
     *  @return <code> true </code> if rule smart engines is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRuleSmartEngine() {
        return (getAdapteeManager().supportsRuleSmartEngine());
    }


    /**
     *  Tests for the availability of an engine lookup service. 
     *
     *  @return <code> true </code> if engine lookup is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineLookup() {
        return (getAdapteeManager().supportsEngineLookup());
    }


    /**
     *  Tests for the availability of an engine query service. 
     *
     *  @return <code> true </code> if engine query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineQuery() {
        return (getAdapteeManager().supportsEngineQuery());
    }


    /**
     *  Tests if searching for engines is available. 
     *
     *  @return <code> true </code> if engine search is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineSearch() {
        return (getAdapteeManager().supportsEngineSearch());
    }


    /**
     *  Tests for the availability of a engine administrative service for 
     *  creating and deleting engines. 
     *
     *  @return <code> true </code> if engine administration is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineAdmin() {
        return (getAdapteeManager().supportsEngineAdmin());
    }


    /**
     *  Tests for the availability of an engine notification service. 
     *
     *  @return <code> true </code> if engine notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineNotification() {
        return (getAdapteeManager().supportsEngineNotification());
    }


    /**
     *  Tests for the availability of an engine hierarchy traversal service. 
     *
     *  @return <code> true </code> if engine hierarchy traversal is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineHierarchy() {
        return (getAdapteeManager().supportsEngineHierarchy());
    }


    /**
     *  Tests for the availability of an engine hierarchy design service. 
     *
     *  @return <code> true </code> if engine hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsEngineHierarchyDesign() {
        return (getAdapteeManager().supportsEngineHierarchyDesign());
    }


    /**
     *  Tests for the availability of a rules check service. 
     *
     *  @return <code> true </code> if a rules check service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRulesCheck() {
        return (getAdapteeManager().supportsRulesCheck());
    }


    /**
     *  Gets the supported <code> Rule </code> record types. 
     *
     *  @return a list containing the supported rule record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRuleRecordTypes() {
        return (getAdapteeManager().getRuleRecordTypes());
    }


    /**
     *  Tests if the given <code> Rule </code> record type is supported. 
     *
     *  @param  ruleRecordType a <code> Type </code> indicating a <code> Rule 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ruleRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRuleRecordType(org.osid.type.Type ruleRecordType) {
        return (getAdapteeManager().supportsRuleRecordType(ruleRecordType));
    }


    /**
     *  Gets the supported rule search record types. 
     *
     *  @return a list containing the supported rule search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getRuleSearchRecordTypes() {
        return (getAdapteeManager().getRuleSearchRecordTypes());
    }


    /**
     *  Tests if the given rule search record type is supported. 
     *
     *  @param  ruleSearchRecordType a <code> Type </code> indicating a rule 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> ruleSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsRuleSearchRecordType(org.osid.type.Type ruleSearchRecordType) {
        return (getAdapteeManager().supportsRuleSearchRecordType(ruleSearchRecordType));
    }


    /**
     *  Gets the supported <code> Engine </code> record types. 
     *
     *  @return a list containing the supported engine record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEngineRecordTypes() {
        return (getAdapteeManager().getEngineRecordTypes());
    }


    /**
     *  Tests if the given <code> Engine </code> record type is supported. 
     *
     *  @param  engineRecordType a <code> Type </code> indicating an <code> 
     *          Engine </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> engineRecordType </code> 
     *          is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEngineRecordType(org.osid.type.Type engineRecordType) {
        return (getAdapteeManager().supportsEngineRecordType(engineRecordType));
    }


    /**
     *  Gets the supported engine search record types. 
     *
     *  @return a list containing the supported engine search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getEngineSearchRecordTypes() {
        return (getAdapteeManager().getEngineSearchRecordTypes());
    }


    /**
     *  Tests if the given engine search record type is supported. 
     *
     *  @param  engineSearchRecordType a <code> Type </code> indicating an 
     *          engine record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> engineSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsEngineSearchRecordType(org.osid.type.Type engineSearchRecordType) {
        return (getAdapteeManager().supportsEngineSearchRecordType(engineSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  evaluation service. 
     *
     *  @return a <code> RulesSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRules() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesSession getRulesSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRulesSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  evaluation service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RulesSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRules() </code> 
     *          or <code> supportsVisibleFederation() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.RulesSession getRulesSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRulesSessionForEngine(engineId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule lookup 
     *  service. 
     *
     *  @return a <code> RuleLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleLookupSession getRuleLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule lookup 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleLookupSession getRuleLookupSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleLookupSessionForEngine(engineId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule query 
     *  service. 
     *
     *  @return a <code> RuleQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleQuerySession getRuleQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule query 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleQuerySession getRuleQuerySessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleQuerySessionForEngine(engineId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule search 
     *  service. 
     *
     *  @return a <code> RuleSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSearchSession getRuleSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule search 
     *  service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleSearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSearchSession getRuleSearchSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleSearchSessionForEngine(engineId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  administrative service. 
     *
     *  @return a <code> RuleAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleAdminSession getRuleAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  administrative service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleAdminSession getRuleAdminSessionForEngine(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleAdminSessionForEngine(engineId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  notification service. 
     *
     *  @param  ruleReceiver the receiver 
     *  @return a <code> RuleNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> ruleReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleNotificationSession getRuleNotificationSession(org.osid.rules.RuleReceiver ruleReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleNotificationSession(ruleReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule 
     *  notification service for the given engine. 
     *
     *  @param  ruleReceiver the receiver 
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Rule </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> ruleReceiver </code> or 
     *          <code> engineId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleNotificationSession getRuleNotificationSessionForEngine(org.osid.rules.RuleReceiver ruleReceiver, 
                                                                                      org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleNotificationSessionForEngine(ruleReceiver, engineId));
    }


    /**
     *  Gets the <code> OsidSession </code> to lookup rule/engine mappings. 
     *
     *  @return a <code> RuleEngineSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsRuleEngine() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleEngineSession getRuleEngineSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleEngineSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with assigning rules to 
     *  engines. 
     *
     *  @return a <code> RuleEngineAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleEngineAssignment() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleEngineAssignmentSession getRuleEngineAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleEngineAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the rule smart 
     *  engine service for the given engine. 
     *
     *  @param  engineId the <code> Id </code> of an <code> Engine </code> 
     *  @return a <code> RuleSmartEngineSession </code> 
     *  @throws org.osid.NotFoundException no <code> Engine </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> engineId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRuleSmartEngine() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.RuleSmartEngineSession getRuleSmartEngineSession(org.osid.id.Id engineId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getRuleSmartEngineSession(engineId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine lookup 
     *  service. 
     *
     *  @return an <code> EngineLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineLookupSession getEngineLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine query 
     *  service. 
     *
     *  @return an <code> EngineQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineQuerySession getEngineQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine search 
     *  service. 
     *
     *  @return an <code> EngineSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineSearchSession getEngineSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  administrative service. 
     *
     *  @return an <code> EngineAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsEngineAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineAdminSession getEngineAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  notification service. 
     *
     *  @param  engineReceiver the receiver 
     *  @return an <code> EngineNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> engineReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineNotificationSession getEngineNotificationSession(org.osid.rules.EngineReceiver engineReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineNotificationSession(engineReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy service. 
     *
     *  @return an <code> EngineHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineHierarchySession getEngineHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the engine 
     *  hierarchy design service. 
     *
     *  @return an <code> EngineierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsEngineHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.rules.EngineHierarchyDesignSession getEngineHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getEngineHierarchyDesignSession());
    }


    /**
     *  Gets the <code> RulesCheckManager. </code> 
     *
     *  @return a <code> RulesCheckManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsRulesCheckManager() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.rules.check.RulesCheckManager getRulesCheckManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getRulesCheckManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
