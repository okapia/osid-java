//
// AdapterRequestTransactionLookupSession.java
//
//    A RequestTransaction lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.provisioning;

import org.osid.binding.java.annotation.OSID;


/**
 *  A RequestTransaction lookup session adapter.
 */

public final class AdapterRequestTransactionLookupSession
    extends net.okapia.osid.jamocha.adapter.provisioning.spi.AbstractAdapterRequestTransactionLookupSession
    implements org.osid.provisioning.RequestTransactionLookupSession {


    /**
     *  Constructs a new {@code AdapterRequestTransactionLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AdapterRequestTransactionLookupSession(org.osid.provisioning.RequestTransactionLookupSession session) {
        super(session);
        return;
    }


    /**
     *  Constructs a new AdapterRequestTransactionLookupSession using the proxy
     *  for the session localization methods.
     *
     *  @param session the underlying request transaction lookup session
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code session} or
     *          {@code proxy} is {@code null}
     */

    public AdapterRequestTransactionLookupSession(org.osid.provisioning.RequestTransactionLookupSession session,
                                        org.osid.proxy.Proxy proxy) {
        super(session);
        setSessionProxy(proxy);
        return;
    }
}
