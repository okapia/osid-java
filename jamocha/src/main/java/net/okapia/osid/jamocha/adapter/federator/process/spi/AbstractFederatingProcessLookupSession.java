//
// AbstractFederatingProcessLookupSession.java
//
//     An abstract federating adapter for a ProcessLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.process.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  ProcessLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingProcessLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.process.ProcessLookupSession>
    implements org.osid.process.ProcessLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingProcessLookupSession</code>.
     */

    protected AbstractFederatingProcessLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.process.ProcessLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Process</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupProcesses() {
        for (org.osid.process.ProcessLookupSession session : getSessions()) {
            if (session.canLookupProcesses()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Process</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeProcessView() {
        for (org.osid.process.ProcessLookupSession session : getSessions()) {
            session.useComparativeProcessView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Process</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryProcessView() {
        for (org.osid.process.ProcessLookupSession session : getSessions()) {
            session.usePlenaryProcessView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Process</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Process</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Process</code> and
     *  retained for compatibility.
     *
     *  @param  processId <code>Id</code> of the
     *          <code>Process</code>
     *  @return the process
     *  @throws org.osid.NotFoundException <code>processId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>processId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.Process getProcess(org.osid.id.Id processId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.process.ProcessLookupSession session : getSessions()) {
            try {
                return (session.getProcess(processId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(processId + " not found");
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  processes specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Processes</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  processIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>processIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByIds(org.osid.id.IdList processIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.process.process.MutableProcessList ret = new net.okapia.osid.jamocha.process.process.MutableProcessList();

        try (org.osid.id.IdList ids = processIds) {
            while (ids.hasNext()) {
                ret.addProcess(getProcess(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  process genus <code>Type</code> which does not include
     *  processes of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  processGenusType a process genus type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByGenusType(org.osid.type.Type processGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.process.process.FederatingProcessList ret = getProcessList();

        for (org.osid.process.ProcessLookupSession session : getSessions()) {
            ret.addProcessList(session.getProcessesByGenusType(processGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProcessList</code> corresponding to the given
     *  process genus <code>Type</code> and include any additional
     *  processes with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  processGenusType a process genus type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByParentGenusType(org.osid.type.Type processGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.process.process.FederatingProcessList ret = getProcessList();

        for (org.osid.process.ProcessLookupSession session : getSessions()) {
            ret.addProcessList(session.getProcessesByParentGenusType(processGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProcessList</code> containing the given
     *  process record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  processRecordType a process record type 
     *  @return the returned <code>Process</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>processRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByRecordType(org.osid.type.Type processRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.process.process.FederatingProcessList ret = getProcessList();

        for (org.osid.process.ProcessLookupSession session : getSessions()) {
            ret.addProcessList(session.getProcessesByRecordType(processRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>ProcessList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known processes or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  processes that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Process</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcessesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.process.process.FederatingProcessList ret = getProcessList();

        for (org.osid.process.ProcessLookupSession session : getSessions()) {
            ret.addProcessList(session.getProcessesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Processes</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  processes or an error results. Otherwise, the returned list
     *  may contain only those processes that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Processes</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.process.ProcessList getProcesses()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.process.process.FederatingProcessList ret = getProcessList();

        for (org.osid.process.ProcessLookupSession session : getSessions()) {
            ret.addProcessList(session.getProcesses());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.process.process.FederatingProcessList getProcessList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.process.process.ParallelProcessList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.process.process.CompositeProcessList());
        }
    }
}
