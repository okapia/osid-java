//
// AbstractResourceRelationship.java
//
//     Defines a ResourceRelationship.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.resourcerelationship.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>ResourceRelationship</code>.
 */

public abstract class AbstractResourceRelationship
    extends net.okapia.osid.jamocha.spi.AbstractOsidRelationship
    implements org.osid.resource.ResourceRelationship {

    private org.osid.resource.Resource sourceResource;
    private org.osid.resource.Resource destinationResource;

    private final java.util.Collection<org.osid.resource.records.ResourceRelationshipRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the source resource <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceResourceId() {
        return (this.sourceResource.getId());
    }


    /**
     *  Gets the <code> Resource. </code> 
     *
     *  @return the source resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSourceResource()
        throws org.osid.OperationFailedException {

        return (this.sourceResource);
    }


    /**
     *  Sets the source resource.
     *
     *  @param resource a source resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setSourceResource(org.osid.resource.Resource resource) {
        nullarg(resource, "source resource");
        this.sourceResource = resource;
        return;
    }


    /**
     *  Gets the destination resource <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getDestinationResourceId() {
        return (this.destinationResource.getId());
    }


    /**
     *  Gets the <code> Resource. </code> 
     *
     *  @return the destination resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getDestinationResource()
        throws org.osid.OperationFailedException {

        return (this.destinationResource);
    }


    /**
     *  Sets the destination resource.
     *
     *  @param resource a destination resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setDestinationResource(org.osid.resource.Resource resource) {
        nullarg(resource, "deatination resource");
        this.destinationResource = resource;
        return;
    }


    /**
     *  Tests if this resourceRelationship supports the given record
     *  <code>Type</code>.
     *
     *  @param  resourceRelationshipRecordType a resource relationship record type 
     *  @return <code>true</code> if the resourceRelationshipRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resourceRelationshipRecordType) {
        for (org.osid.resource.records.ResourceRelationshipRecord record : this.records) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ResourceRelationship</code> record <code>Type</code>.
     *
     *  @param resourceRelationshipRecordType the resource
     *         relationship record type
     *  @return the resource relationship record 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceRelationshipRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.ResourceRelationshipRecord getResourceRelationshipRecord(org.osid.type.Type resourceRelationshipRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.resource.records.ResourceRelationshipRecord record : this.records) {
            if (record.implementsRecordType(resourceRelationshipRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceRelationshipRecordType + " is not supported");
    }


    /**
     *  Adds a record to this resource relationship. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resourceRelationshipRecord the resource relationship record
     *  @param resourceRelationshipRecordType resource relationship record type
     *  @throws org.osid.NullArgumentException
     *          <code>resourceRelationshipRecord</code> or
     *          <code>resourceRelationshipRecordTyperesourceRelationship</code> is
     *          <code>null</code>
     */
            
    protected void addResourceRelationshipRecord(org.osid.resource.records.ResourceRelationshipRecord resourceRelationshipRecord, 
                                                 org.osid.type.Type resourceRelationshipRecordType) {
        
        nullarg(resourceRelationshipRecord, "resource relationship record");
        addRecordType(resourceRelationshipRecordType);
        this.records.add(resourceRelationshipRecord);
        
        return;
    }
}
