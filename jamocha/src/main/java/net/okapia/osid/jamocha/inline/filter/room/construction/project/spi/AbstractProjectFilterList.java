//
// AbstractProjectList
//
//     Implements a filter for a ProjectList.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.filter.room.construction.project.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Implements a filter for a ProjectList. Subclasses call this
 *  constructor and implement the <code>pass()</code> method. This
 *  filter is synchronous but can be wrapped in a BufferedProjectList
 *  to improve performance.
 */

public abstract class AbstractProjectFilterList
    extends net.okapia.osid.jamocha.room.construction.project.spi.AbstractProjectList
    implements org.osid.room.construction.ProjectList,
               net.okapia.osid.jamocha.inline.filter.room.construction.project.ProjectFilter {

    private org.osid.room.construction.Project project;
    private final org.osid.room.construction.ProjectList list;
    private org.osid.OsidException error;


    /**
     *  Creates a new <code>AbstractProjectFilterList</code>.
     *
     *  @param projectList a <code>ProjectList</code>
     *  @throws org.osid.NullArgumentException
     *          <code>projectList</code> is <code>null</code>
     */

    protected AbstractProjectFilterList(org.osid.room.construction.ProjectList projectList) {
        nullarg(projectList, "project list");
        this.list = projectList;
        return;
    }    

        
    /**
     *  Tests if there are more elements in this list. 
     *
     *  @return <code> true </code> if more elements are available in
     *          this list, <code> false </code> if the end of the list
     *          has been reached
     *  @throws org.osid.IllegalStateException this list is closed 
     */

    @OSID @Override
    public boolean hasNext() {
        if (hasError()) {
            return (true);
        }

        prime();

        if (this.project == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the next <code> Project </code> in this list. 
     *
     *  @return the next <code> Project </code> in this list. The <code> 
     *          hasNext() </code> method should be used to test that a next 
     *          <code> Project </code> is available before calling this method. 
     *  @throws org.osid.IllegalStateException no more elements available in 
     *          this list or this list is closed
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.room.construction.Project getNextProject()
        throws org.osid.OperationFailedException {

        if (hasError()) {
            throw new org.osid.OperationFailedException(this.error);
        }

        if (hasNext()) {
            org.osid.room.construction.Project project = this.project;
            this.project = null;
            return (project);
        } else {
            throw new org.osid.IllegalStateException("no more elements available in project list");
        }
    }


    /**
     *  Close this list.
     *
     *  @throws org.osid.IllegalStateException this list already closed
     */

    @OSIDBinding @Override
    public void close() {
        this.project = null;
        this.list.close();
        return;
    }

    
    /**
     *  Filters Projects.
     *
     *  @param project the project to filter
     *  @return <code>true</code> if the project passes the filter,
     *          <code>false</code> if the project should be filtered
     */

    public abstract boolean pass(org.osid.room.construction.Project project);


    protected void prime() {
        if (this.project != null) {
            return;
        }

        org.osid.room.construction.Project project = null;

        while (this.list.hasNext()) {
            try {
                project = this.list.getNextProject();
            } catch (org.osid.OsidException oe) {
                error(oe);
                return;
            }

            if (pass(project)) {
                this.project = project;
                return;
            }
        }

        return;
    }


    /**
     *  Set an error to be thrown on the next retrieval.
     *
     *  @param error
     *  @throws org.osid.IllegalStateException this list already closed
     */

    protected void error(org.osid.OsidException error) {
        this.error = error;
        return;
    }


    /**
     *  Tests if an error exists.
     *
     *  @return <code>true</code> if an error has occurred,
     *          <code>false</code> otherwise
     */

    protected boolean hasError() {
        if (this.error == null) {
            return (false);
        } else {
            return (true);
        }
    }
}
