//
// MutableMapProxyDispatchLookupSession
//
//    Implements a Dispatch lookup service backed by a collection of
//    dispatches that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.subscription;


/**
 *  Implements a Dispatch lookup service backed by a collection of
 *  dispatches. The dispatches are indexed only by {@code Id}. This
 *  class can be used for small collections or subclassed to provide
 *  additional indices for faster lookups.
 *
 *  The collection of dispatches can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapProxyDispatchLookupSession
    extends net.okapia.osid.jamocha.core.subscription.spi.AbstractMapDispatchLookupSession
    implements org.osid.subscription.DispatchLookupSession {


    /**
     *  Constructs a new {@code MutableMapProxyDispatchLookupSession}
     *  with no dispatches.
     *
     *  @param publisher the publisher
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher} or
     *          {@code proxy} is {@code null} 
     */

      public MutableMapProxyDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                                  org.osid.proxy.Proxy proxy) {
        setPublisher(publisher);        
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapProxyDispatchLookupSession} with a
     *  single dispatch.
     *
     *  @param publisher the publisher
     *  @param dispatch a dispatch
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher},
     *          {@code dispatch}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                                org.osid.subscription.Dispatch dispatch, org.osid.proxy.Proxy proxy) {
        this(publisher, proxy);
        putDispatch(dispatch);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDispatchLookupSession} using an
     *  array of dispatches.
     *
     *  @param publisher the publisher
     *  @param dispatches an array of dispatches
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher},
     *          {@code dispatches}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                                org.osid.subscription.Dispatch[] dispatches, org.osid.proxy.Proxy proxy) {
        this(publisher, proxy);
        putDispatches(dispatches);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableMapProxyDispatchLookupSession} using a
     *  collection of dispatches.
     *
     *  @param publisher the publisher
     *  @param dispatches a collection of dispatches
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code publisher},
     *          {@code dispatches}, or {@code proxy} is {@code null}
     */

    public MutableMapProxyDispatchLookupSession(org.osid.subscription.Publisher publisher,
                                                java.util.Collection<? extends org.osid.subscription.Dispatch> dispatches,
                                                org.osid.proxy.Proxy proxy) {
   
        this(publisher, proxy);
        setSessionProxy(proxy);
        putDispatches(dispatches);
        return;
    }

    
    /**
     *  Makes a {@code Dispatch} available in this session.
     *
     *  @param dispatch an dispatch
     *  @throws org.osid.NullArgumentException {@code dispatch{@code 
     *          is {@code null}
     */

    @Override
    public void putDispatch(org.osid.subscription.Dispatch dispatch) {
        super.putDispatch(dispatch);
        return;
    }


    /**
     *  Makes an array of dispatches available in this session.
     *
     *  @param dispatches an array of dispatches
     *  @throws org.osid.NullArgumentException {@code dispatches{@code 
     *          is {@code null}
     */

    @Override
    public void putDispatches(org.osid.subscription.Dispatch[] dispatches) {
        super.putDispatches(dispatches);
        return;
    }


    /**
     *  Makes collection of dispatches available in this session.
     *
     *  @param dispatches
     *  @throws org.osid.NullArgumentException {@code dispatch{@code 
     *          is {@code null}
     */

    @Override
    public void putDispatches(java.util.Collection<? extends org.osid.subscription.Dispatch> dispatches) {
        super.putDispatches(dispatches);
        return;
    }


    /**
     *  Removes a Dispatch from this session.
     *
     *  @param dispatchId the {@code Id} of the dispatch
     *  @throws org.osid.NullArgumentException {@code dispatchId{@code  is
     *          {@code null}
     */

    @Override
    public void removeDispatch(org.osid.id.Id dispatchId) {
        super.removeDispatch(dispatchId);
        return;
    }    
}
