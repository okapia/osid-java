//
// AbstractImmutablePeriod.java
//
//     Wraps a mutable Period to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.billing.period.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Period</code> to hide modifiers. This
 *  wrapper provides an immutized Period from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying period whose state changes are visible.
 */

public abstract class AbstractImmutablePeriod
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidObject
    implements org.osid.billing.Period {

    private final org.osid.billing.Period period;


    /**
     *  Constructs a new <code>AbstractImmutablePeriod</code>.
     *
     *  @param period the period to immutablize
     *  @throws org.osid.NullArgumentException <code>period</code>
     *          is <code>null</code>
     */

    protected AbstractImmutablePeriod(org.osid.billing.Period period) {
        super(period);
        this.period = period;
        return;
    }


    /**
     *  Gets a display label for this period which may be less formal than the 
     *  display name. 
     *
     *  @return the period label 
     */

    @OSID @Override
    public org.osid.locale.DisplayText getDisplayLabel() {
        return (this.period.getDisplayLabel());
    }


    /**
     *  Tests if this period has an open date. 
     *
     *  @return <code> true </code> if there is an open date associated with 
     *          this period, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasOpenDate() {
        return (this.period.hasOpenDate());
    }


    /**
     *  Gets the open date. 
     *
     *  @return the open date 
     *  @throws org.osid.IllegalStateException <code> hasOpenDate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getOpenDate() {
        return (this.period.getOpenDate());
    }


    /**
     *  Tests if this period has a close date. 
     *
     *  @return <code> true </code> if there is a close date associated with 
     *          this period, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasCloseDate() {
        return (this.period.hasCloseDate());
    }


    /**
     *  Gets the close date. 
     *
     *  @return the close date 
     *  @throws org.osid.IllegalStateException <code> hasCloseDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getCloseDate() {
        return (this.period.getCloseDate());
    }


    /**
     *  Tests if this period has a billing date. 
     *
     *  @return <code> true </code> if there is a billing date associated with 
     *          this period, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasBillingDate() {
        return (this.period.hasBillingDate());
    }


    /**
     *  Gets the billing date. 
     *
     *  @return the billing date 
     *  @throws org.osid.IllegalStateException <code> hasBillingDate() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getBillingDate() {
        return (this.period.getBillingDate());
    }


    /**
     *  Tests if this period has a due date. 
     *
     *  @return <code> true </code> if there is a due date associated with 
     *          this period, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean hasDueDate() {
        return (this.period.hasDueDate());
    }


    /**
     *  Gets the due date. 
     *
     *  @return the due date 
     *  @throws org.osid.IllegalStateException <code> hasDueDate() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getDueDate() {
        return (this.period.getDueDate());
    }


    /**
     *  Gets the period record corresponding to the given <code> Period 
     *  </code> record <code> Type. </code> This method is used to retrieve an 
     *  object implementing the requested record. The <code> periodRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(periodRecordType) </code> 
     *  is <code> true </code> . 
     *
     *  @param  periodRecordType the type of period record to retrieve 
     *  @return the period record 
     *  @throws org.osid.NullArgumentException <code> periodRecordType </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(periodRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.billing.records.PeriodRecord getPeriodRecord(org.osid.type.Type periodRecordType)
        throws org.osid.OperationFailedException {

        return (this.period.getPeriodRecord(periodRecordType));
    }
}

