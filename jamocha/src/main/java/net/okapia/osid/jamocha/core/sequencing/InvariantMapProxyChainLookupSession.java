//
// InvariantMapProxyChainLookupSession
//
//    Implements a Chain lookup service backed by a fixed
//    collection of chains. 
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.sequencing;


/**
 *  Implements a Chain lookup service backed by a fixed
 *  collection of chains. The chains are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 */

public final class InvariantMapProxyChainLookupSession
    extends net.okapia.osid.jamocha.core.sequencing.spi.AbstractMapChainLookupSession
    implements org.osid.sequencing.ChainLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyChainLookupSession} with no
     *  chains.
     *
     *  @param antimatroid the antimatroid
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code antimatroid} or
     *          {@code proxy} is {@code null}
     */

    public InvariantMapProxyChainLookupSession(org.osid.sequencing.Antimatroid antimatroid,
                                                  org.osid.proxy.Proxy proxy) {
        setAntimatroid(antimatroid);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new {@code
     *  InvariantMapProxyChainLookupSession} with a single
     *  chain.
     *
     *  @param antimatroid the antimatroid
     *  @param chain a single chain
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code antimatroid},
     *          {@code chain} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyChainLookupSession(org.osid.sequencing.Antimatroid antimatroid,
                                                  org.osid.sequencing.Chain chain, org.osid.proxy.Proxy proxy) {

        this(antimatroid, proxy);
        putChain(chain);
        return;
    }


    /**
     *  Constructs a new {@code InvariantMapProxyChainLookupSession} using
     *  an array of chains.
     *
     *  @param antimatroid the antimatroid
     *  @param chains an array of chains
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code antimatroid},
     *          {@code chains} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyChainLookupSession(org.osid.sequencing.Antimatroid antimatroid,
                                                  org.osid.sequencing.Chain[] chains, org.osid.proxy.Proxy proxy) {

        this(antimatroid, proxy);
        putChains(chains);
        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantMapProxyChainLookupSession} using a
     *  collection of chains.
     *
     *  @param antimatroid the antimatroid
     *  @param chains a collection of chains
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code antimatroid},
     *          {@code chains} or {@code proxy} is {@code null}
     */

    public InvariantMapProxyChainLookupSession(org.osid.sequencing.Antimatroid antimatroid,
                                                  java.util.Collection<? extends org.osid.sequencing.Chain> chains,
                                                  org.osid.proxy.Proxy proxy) {

        this(antimatroid, proxy);
        putChains(chains);
        return;
    }
}
