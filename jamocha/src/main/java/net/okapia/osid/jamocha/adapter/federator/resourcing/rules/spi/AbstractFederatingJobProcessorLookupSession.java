//
// AbstractFederatingJobProcessorLookupSession.java
//
//     An abstract federating adapter for a JobProcessorLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resourcing.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  JobProcessorLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingJobProcessorLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.resourcing.rules.JobProcessorLookupSession>
    implements org.osid.resourcing.rules.JobProcessorLookupSession {

    private boolean parallel = false;
    private org.osid.resourcing.Foundry foundry = new net.okapia.osid.jamocha.nil.resourcing.foundry.UnknownFoundry();


    /**
     *  Constructs a new <code>AbstractFederatingJobProcessorLookupSession</code>.
     */

    protected AbstractFederatingJobProcessorLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.resourcing.rules.JobProcessorLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Foundry/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Foundry Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getFoundryId() {
        return (this.foundry.getId());
    }


    /**
     *  Gets the <code>Foundry</code> associated with this 
     *  session.
     *
     *  @return the <code>Foundry</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.foundry);
    }


    /**
     *  Sets the <code>Foundry</code>.
     *
     *  @param  foundry the foundry for this session
     *  @throws org.osid.NullArgumentException <code>foundry</code>
     *          is <code>null</code>
     */

    protected void setFoundry(org.osid.resourcing.Foundry foundry) {
        nullarg(foundry, "foundry");
        this.foundry = foundry;
        return;
    }


    /**
     *  Tests if this user can perform <code>JobProcessor</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupJobProcessors() {
        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            if (session.canLookupJobProcessors()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>JobProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeJobProcessorView() {
        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            session.useComparativeJobProcessorView();
        }

        return;
    }


    /**
     *  A complete view of the <code>JobProcessor</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryJobProcessorView() {
        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            session.usePlenaryJobProcessorView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include job processors in foundries which are children
     *  of this foundry in the foundry hierarchy.
     */

    @OSID @Override
    public void useFederatedFoundryView() {
        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            session.useFederatedFoundryView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this foundry only.
     */

    @OSID @Override
    public void useIsolatedFoundryView() {
        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            session.useIsolatedFoundryView();
        }

        return;
    }


    /**
     *  Only active job processors are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveJobProcessorView() {
        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            session.useActiveJobProcessorView();
        }

        return;
    }


    /**
     *  Active and inactive job processors are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusJobProcessorView() {
        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            session.useAnyStatusJobProcessorView();
        }

        return;
    }
    
     
    /**
     *  Gets the <code>JobProcessor</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>JobProcessor</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>JobProcessor</code> and
     *  retained for compatibility.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorId <code>Id</code> of the
     *          <code>JobProcessor</code>
     *  @return the job processor
     *  @throws org.osid.NotFoundException <code>jobProcessorId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>jobProcessorId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessor getJobProcessor(org.osid.id.Id jobProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            try {
                return (session.getJobProcessor(jobProcessorId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(jobProcessorId + " not found");
    }


    /**
     *  Gets a <code>JobProcessorList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  jobProcessors specified in the <code>Id</code> list, in the
     *  order of the list, including duplicates, or an error results
     *  if an <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible
     *  <code>JobProcessors</code> may be omitted from the list and
     *  may present the elements in any order including returning a
     *  unique set.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>JobProcessor</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByIds(org.osid.id.IdList jobProcessorIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.resourcing.rules.jobprocessor.MutableJobProcessorList ret = new net.okapia.osid.jamocha.resourcing.rules.jobprocessor.MutableJobProcessorList();

        try (org.osid.id.IdList ids = jobProcessorIds) {
            while (ids.hasNext()) {
                ret.addJobProcessor(getJobProcessor(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>JobProcessorList</code> corresponding to the given
     *  job processor genus <code>Type</code> which does not include
     *  job processors of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known job
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorGenusType a jobProcessor genus type 
     *  @return the returned <code>JobProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByGenusType(org.osid.type.Type jobProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessor.FederatingJobProcessorList ret = getJobProcessorList();

        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            ret.addJobProcessorList(session.getJobProcessorsByGenusType(jobProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>JobProcessorList</code> corresponding to the given
     *  job processor genus <code>Type</code> and include any additional
     *  job processors with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known job
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorGenusType a jobProcessor genus type 
     *  @return the returned <code>JobProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByParentGenusType(org.osid.type.Type jobProcessorGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessor.FederatingJobProcessorList ret = getJobProcessorList();

        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            ret.addJobProcessorList(session.getJobProcessorsByParentGenusType(jobProcessorGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>JobProcessorList</code> containing the given
     *  job processor record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known job
     *  processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @param  jobProcessorRecordType a jobProcessor record type 
     *  @return the returned <code>JobProcessor</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>jobProcessorRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessorsByRecordType(org.osid.type.Type jobProcessorRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessor.FederatingJobProcessorList ret = getJobProcessorList();

        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            ret.addJobProcessorList(session.getJobProcessorsByRecordType(jobProcessorRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>JobProcessors</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  job processors or an error results. Otherwise, the returned list
     *  may contain only those job processors that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, job processors are returned that are currently
     *  active. In any status mode, active and inactive job processors
     *  are returned.
     *
     *  @return a list of <code>JobProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.rules.JobProcessorList getJobProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessor.FederatingJobProcessorList ret = getJobProcessorList();

        for (org.osid.resourcing.rules.JobProcessorLookupSession session : getSessions()) {
            ret.addJobProcessorList(session.getJobProcessors());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessor.FederatingJobProcessorList getJobProcessorList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessor.ParallelJobProcessorList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.rules.jobprocessor.CompositeJobProcessorList());
        }
    }
}
