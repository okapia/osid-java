//
// AbstractAdapterResourceRelationshipLookupSession.java
//
//    A ResourceRelationship lookup session adapter.
//
//
// Tom Coppeto
// Okapia
// 5 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.resource.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  A ResourceRelationship lookup session adapter.
 */

public abstract class AbstractAdapterResourceRelationshipLookupSession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.resource.ResourceRelationshipLookupSession {

    private final org.osid.resource.ResourceRelationshipLookupSession session;


    /**
     *  Constructs a new {@code AbstractAdapterResourceRelationshipLookupSession}.
     *
     *  @param session the session to adapt
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterResourceRelationshipLookupSession(org.osid.resource.ResourceRelationshipLookupSession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@code Bin/code> {@code Id} associated
     *  with this session.
     *
     *  @return the {@code Bin Id} associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getBinId() {
        return (this.session.getBinId());
    }


    /**
     *  Gets the {@code Bin} associated with this session.
     *
     *  @return the {@code Bin} associated with this session
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.Bin getBin()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getBin());
    }


    /**
     *  Tests if this user can perform {@code ResourceRelationship} 
     *  lookups.
     *
     *  @return {@code true}
     */

    @OSID @Override
    public boolean canLookupResourceRelationships() {
        return (this.session.canLookupResourceRelationships());
    }


    /**
     *  A complete view of the {@code ResourceRelationship} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeResourceRelationshipView() {
        this.session.useComparativeResourceRelationshipView();
        return;
    }


    /**
     *  A complete view of the {@code ResourceRelationship} returns is desired.
     *  Methods will return what is requested or result in an
     *  error. This view is used when greater precision is desired at
     *  the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryResourceRelationshipView() {
        this.session.usePlenaryResourceRelationshipView();
        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include resource relationships in bins which are children
     *  of this bin in the bin hierarchy.
     */

    @OSID @Override
    public void useFederatedBinView() {
        this.session.useFederatedBinView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this bin only.
     */

    @OSID @Override
    public void useIsolatedBinView() {
        this.session.useIsolatedBinView();
        return;
    }
    

    /**
     *  Only resource relationships whose effective dates are current
     *  are returned by methods in this session.
     */

    public void useEffectiveResourceRelationshipView() {
        this.session.useEffectiveResourceRelationshipView();
        return;
    }
    

    /**
     *  All resource relationships of any effective dates are returned
     *  by all methods in this session.
     */

    public void useAnyEffectiveResourceRelationshipView() {
        this.session.useAnyEffectiveResourceRelationshipView();
        return;
    }

     
    /**
     *  Gets the {@code ResourceRelationship} specified by its {@code
     *  Id}.
     *
     *  In plenary mode, the exact {@code Id} is found or a {@code
     *  NOT_FOUND} results. Otherwise, the returned {@code
     *  ResourceRelationship} may have a different {@code Id} than
     *  requested, such as the case where a duplicate {@code Id} was
     *  assigned to a {@code ResourceRelationship} and retained for
     *  compatibility.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param resourceResourceId {@code Id} of the {@code ResourceRelationship}
     *  @return the resource relationship
     *  @throws org.osid.NotFoundException {@code resourceResourceId} not
     *          found
     *  @throws org.osid.NullArgumentException {@code resourceResourceId} is
     *          {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationship getResourceRelationship(org.osid.id.Id resourceResourceId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationship(resourceResourceId));
    }


    /**
     *  Gets a {@code ResourceRelationshipList} corresponding to the
     *  given {@code IdList}.
     *
     *  In plenary mode, the returned list contains all of the
     *  resourceResources specified in the {@code Id} list, in the
     *  order of the list, including duplicates, or an error results
     *  if an {@code Id} in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible {@code
     *  ResourceRelationships} may be omitted from the list and may
     *  present the elements in any order including returning a unique
     *  set.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  resourceResourceIds the list of {@code Ids} to retrieve 
     *  @return the returned {@code ResourceRelationship} list
     *  @throws org.osid.NotFoundException an {@code Id} was not found
     *  @throws org.osid.NullArgumentException
     *          {@code resourceResourceIds} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByIds(org.osid.id.IdList resourceResourceIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByIds(resourceResourceIds));
    }


    /**
     *  Gets a {@code ResourceRelationshipList} corresponding to the
     *  given resource relationship genus {@code Type} which does not
     *  include resource relationships of types derived from the
     *  specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  resourceResourceGenusType a resourceResource genus type 
     *  @return the returned {@code ResourceRelationship} list
     *  @throws org.osid.NullArgumentException
     *          {@code resourceResourceGenusType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusType(org.osid.type.Type resourceResourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByGenusType(resourceResourceGenusType));
    }


    /**
     *  Gets a {@code ResourceRelationshipList} corresponding to the
     *  given resource relationship genus {@code Type} and include any
     *  additional resource relationships with genus types derived
     *  from the specified {@code Type}.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  resourceResourceGenusType a resourceResource genus type 
     *  @return the returned {@code ResourceRelationship} list
     *  @throws org.osid.NullArgumentException
     *          {@code resourceResourceGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByParentGenusType(org.osid.type.Type resourceResourceGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByParentGenusType(resourceResourceGenusType));
    }


    /**
     *  Gets a {@code ResourceRelationshipList} containing the given
     *  resource relationship record {@code Type}. 
     * 
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  In effective mode, resource relationships are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  resourceResourceRecordType a resourceResource record type 
     *  @return the returned {@code ResourceRelationship} list
     *  @throws org.osid.NullArgumentException
     *          {@code resourceResourceRecordType} is {@code null}
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByRecordType(org.osid.type.Type resourceResourceRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByRecordType(resourceResourceRecordType));
    }


    /**
     *  Gets a {@code ResourceRelationshipList} effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In active mode, resource relationships are returned that are
     *  currently active. In any status mode, active and inactive
     *  resource relationships are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned {@code ResourceRelationship} list 
     *  @throws org.osid.InvalidArgumentException {@code from}
     *          is greater than {@code to}
     *  @throws org.osid.NullArgumentException {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsOnDate(org.osid.calendaring.DateTime from, 
                                                                                     org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsOnDate(from, to));
    }
        

    /**
     *  Gets a list of resource resources corresponding to a
     *  source resource {@code Id}.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource resources that are
     *  accessible through this session.
     *
     *  In effective mode, resource resources are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource resources and those currently expired are
     *  returned.
     *
     *  @param  sourceResourceId the {@code Id} of the source resource
     *  @return the returned {@code ResourceRelationshipList}
     *  @throws org.osid.NullArgumentException {@code sourceResourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForSourceResource(org.osid.id.Id sourceResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsForSourceResource(sourceResourceId));
    }


    /**
     *  Gets a list of resource resources corresponding to a
     *  source resource {@code Id} and effective during the entire
     *  given date range inclusive but not confined to the date range.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource resources that are
     *  accessible through this session.
     *
     *  In effective mode, resource resources are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource resources and those currently expired are
     *  returned.
     *
     *  @param  sourceResourceId the {@code Id} of the source resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ResourceRelationshipList}
     *  @throws org.osid.NullArgumentException {@code sourceResourceId},
     *          {@code from} or {@code to} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForSourceResourceOnDate(org.osid.id.Id sourceResourceId,
                                                                                                      org.osid.calendaring.DateTime from,
                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsForSourceResourceOnDate(sourceResourceId, from, to));
    }





    /**
     *  Gets the {@code ResourceRelationships} of a resource of
     *  relationship genus type that includes any genus type derived
     *  from the given one.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective. In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  sourceResourceId {@code Id} of a {@code Resource}
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException {@code
     *          sourceResourceId} or {@code relationshipGenusType} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForSourceResource(org.osid.id.Id sourceResourceId, 
                                                                                                           org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByGenusTypeForSourceResource(sourceResourceId, relationshipGenusType));
    }


    /**
     *  Gets a list of resource relationships of a given genus type
     *  for a resource and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective in addition to being effective during
     *  the given dates. In any effective mode, effective resource
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceResourceId a resource {@code Id} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code sourceResourceId, 
     *          relationshipGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForSourceResourceOnDate(org.osid.id.Id sourceResourceId, 
                                                                                                                 org.osid.type.Type relationshipGenusType, 
                                                                                                                 org.osid.calendaring.DateTime from, 
                                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByGenusTypeForSourceResourceOnDate(sourceResourceId, relationshipGenusType, from, to));
    }


    /**
     *  Gets a list of resource resources corresponding to a
     *  destination resource {@code Id}.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource resources that are
     *  accessible through this session.
     *
     *  In effective mode, resource resources are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource resources and those currently expired are
     *  returned.
     *
     *  @param  destinationResourceId the {@code Id} of the destination resource
     *  @return the returned {@code ResourceRelationshipList}
     *  @throws org.osid.NullArgumentException {@code destinationResourceId} is
     *          {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForDestinationResource(org.osid.id.Id destinationResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsForDestinationResource(destinationResourceId));
    }


    /**
     *  Gets a list of resource resources corresponding to a
     *  destination resource {@code Id} and effective during the
     *  entire given date range inclusive but not confined to the date
     *  range.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource resources that are
     *  accessible through this session.
     *
     *  In effective mode, resource resources are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource resources and those currently expired are
     *  returned.
     *
     *  @param  destinationResourceId the {@code Id} of the destination resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ResourceRelationshipList}
     *  @throws org.osid.NullArgumentException {@code destinationResourceId}, {@code
     *          from} or {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForDestinationResourceOnDate(org.osid.id.Id destinationResourceId,
                                                                                                           org.osid.calendaring.DateTime from,
                                                                                                           org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsForDestinationResourceOnDate(destinationResourceId, from, to));
    }


    /**
     *  Gets the {@code ResourceRelationships} of a resource of
     *  relationship genus type that includes any genus type derived
     *  from the given one.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective. In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param  destinationResourceId {@code Id} of a {@code Resource}
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException {@code
     *         destinationResourceId} or {@code relationshipGenusType}
     *         is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForDestinationResource(org.osid.id.Id destinationResourceId, 
                                                                                                                org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByGenusTypeForDestinationResource(destinationResourceId, relationshipGenusType));
    }


    /**
     *  Gets a list of resource relationships of a given genus type
     *  for a resource and effective during the entire given date
     *  range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that are 
     *  currently effective in addition to being effective during the given 
     *  dates. In any effective mode, effective resource relationships and 
     *  those currently expired are returned. 
     *
     *  @param  destinationResourceId a resource {@code Id} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code destinationResourceId, 
     *          relationshipGenusType, from} or {@code to} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForDestinationResourceOnDate(org.osid.id.Id destinationResourceId, 
                                                                                                                      org.osid.type.Type relationshipGenusType, 
                                                                                                                      org.osid.calendaring.DateTime from, 
                                                                                                                      org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByGenusTypeForDestinationResourceOnDate(destinationResourceId, relationshipGenusType, from, to));
    }


    /**
     *  Gets a list of resource resources corresponding to source
     *  relationship and destination resource {@code Ids}.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource resources that are
     *  accessible through this session.
     *
     *  In effective mode, resource resources are returned that
     *  are currently effective.  In any effective mode, effective
     *  resource resources and those currently expired are
     *  returned.
     *
     *  @param  sourceResourceId the {@code Id} of the source resource
     *  @param  destinationResourceId the {@code Id} of the destination resource
     *  @return the returned {@code ResourceRelationshipList}
     *  @throws org.osid.NullArgumentException {@code sourceResourceId},
     *          {@code destinationResourceId} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForResources(org.osid.id.Id sourceResourceId,
                                                                                           org.osid.id.Id destinationResourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        return (this.session.getResourceRelationshipsForResources(sourceResourceId, destinationResourceId));
    }


    /**
     *  Gets a list of resource resources corresponding to source
     *  relationship and destination resource {@code Ids} and
     *  effective during the entire given date range inclusive but not
     *  confined to the date range.
     *
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource resources that are
     *  accessible through this session.
     *
     *  In effective mode, resource resources are returned that are currently
     *  effective. In any effective mode, effective resource resources and
     *  those currently expired are returned.
     *
     *  @param destinationResourceId the {@code Id} of the
     *         destination resource
     *  @param  from from date
     *  @param  to to date
     *  @return the returned {@code ResourceRelationshipList}
     *  @throws org.osid.NullArgumentException {@code sourceResourceId},
     *          {@code destinationResourceId}, {@code from} or
     *          {@code to} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsForResourcesOnDate(org.osid.id.Id sourceResourceId,
                                                                                                 org.osid.id.Id destinationResourceId,
                                                                                                 org.osid.calendaring.DateTime from,
                                                                                                 org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsForResourcesOnDate(sourceResourceId, destinationResourceId, from, to));
    }


    /**
     *  Gets the {@code ResourceRelationships} given two
     *  resources and a relationship genus type which includes any
     *  genus types derived from the given genus type.
     *  
     *  In plenary mode, the returned list contains all known
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those relationships that are accessible
     *  through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective. In any effective mode, effective
     *  resource relationships and those currently expired are
     *  returned.
     *
     *  @param sourceResourceId {@code Id} of a {@code Resource}
     *  @param  destinationResourceId {@code Id} of another {@code 
     *          Resource} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @return the relationships 
     *  @throws org.osid.NullArgumentException {@code
     *          sourceResourceId, destinationResourceId,} or {@code
     *          relatonshipGenusType} is {@code null}
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForResources(org.osid.id.Id sourceResourceId, 
                                                                                                      org.osid.id.Id destinationResourceId, 
                                                                                                      org.osid.type.Type relationshipGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByGenusTypeForResources(sourceResourceId, destinationResourceId, relationshipGenusType));
    }


    /**
     *  Gets a list of resource relationships of a given genus type
     *  for a two peer resources and effective during the entire given
     *  date range inclusive but not confined to the date range.
     *  
     *  In plenary mode, the returned list contains all known resource
     *  relationships or an error results. Otherwise, the returned
     *  list may contain only those resource relationships that are
     *  accessible through this session.
     *  
     *  In effective mode, resource relationships are returned that
     *  are currently effective in addition to being effective during
     *  the given dates. In any effective mode, effective resource
     *  relationships and those currently expired are returned.
     *
     *  @param  sourceResourceId a resource {@code Id} 
     *  @param  destinationResourceId {@code Id} of another {@code 
     *          Resource} 
     *  @param  relationshipGenusType a relationship genus type 
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the relationships 
     *  @throws org.osid.InvalidArgumentException {@code from} is 
     *          greater than {@code to} 
     *  @throws org.osid.NullArgumentException {@code sourceResourceId, 
     *          destinationResourceId, relationshipGenusType, from} or 
     *          {@code to} is {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationshipsByGenusTypeForResourcesOnDate(org.osid.id.Id sourceResourceId, 
                                                                                                            org.osid.id.Id destinationResourceId, 
                                                                                                            org.osid.type.Type relationshipGenusType, 
                                                                                                            org.osid.calendaring.DateTime from, 
                                                                                                            org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationshipsByGenusTypeForResourcesOnDate(sourceResourceId, destinationResourceId, relationshipGenusType, from, to));
    }


    /**
     *  Gets all {@code ResourceRelationships}. 
     *
     *  In plenary mode, the returned list contains all known resource
     *  resources or an error results. Otherwise, the returned list
     *  may contain only those resource resources that are accessible
     *  through this session. In both cases, the order of the set is
     *  not specified.
     *
     *  In effective mode, resource resources are returned that are
     *  currently effective.  In any effective mode, effective
     *  resource resources and those currently expired are returned.
     *
     *  @return a list of {@code ResourceRelationships} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resource.ResourceRelationshipList getResourceRelationships()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (this.session.getResourceRelationships());
    }
}
