//
// AbstractAssemblyDirectionQuery.java
//
//     A DirectionQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.recipe.direction.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A DirectionQuery that stores terms.
 */

public abstract class AbstractAssemblyDirectionQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidObjectQuery
    implements org.osid.recipe.DirectionQuery,
               org.osid.recipe.DirectionQueryInspector,
               org.osid.recipe.DirectionSearchOrder {

    private final java.util.Collection<org.osid.recipe.records.DirectionQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recipe.records.DirectionQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.recipe.records.DirectionSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyDirectionQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyDirectionQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Sets the recipe <code> Id </code> for this query. 
     *
     *  @param  recipeId the recipe <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> recipeId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchRecipeId(org.osid.id.Id recipeId, boolean match) {
        getAssembler().addIdTerm(getRecipeIdColumn(), recipeId, match);
        return;
    }


    /**
     *  Clears the recipe <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearRecipeIdTerms() {
        getAssembler().clearTerms(getRecipeIdColumn());
        return;
    }


    /**
     *  Gets the recipe <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRecipeIdTerms() {
        return (getAssembler().getIdTerms(getRecipeIdColumn()));
    }


    /**
     *  Orders the results by recipe. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByRecipe(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getRecipeColumn(), style);
        return;
    }


    /**
     *  Gets the RecipeId column name.
     *
     * @return the column name
     */

    protected String getRecipeIdColumn() {
        return ("recipe_id");
    }


    /**
     *  Tests if a <code> RecipeQuery </code> is available. 
     *
     *  @return <code> true </code> if a recipe query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeQuery() {
        return (false);
    }


    /**
     *  Gets the query for a recipe. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the recipe query 
     *  @throws org.osid.UnimplementedException <code> supportsRecipeQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQuery getRecipeQuery() {
        throw new org.osid.UnimplementedException("supportsRecipeQuery() is false");
    }


    /**
     *  Clears the recipe query terms. 
     */

    @OSID @Override
    public void clearRecipeTerms() {
        getAssembler().clearTerms(getRecipeColumn());
        return;
    }


    /**
     *  Gets the recipe query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.RecipeQueryInspector[] getRecipeTerms() {
        return (new org.osid.recipe.RecipeQueryInspector[0]);
    }


    /**
     *  Tests if a recipe search order is available. 
     *
     *  @return <code> true </code> if a recipe search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsRecipeSearchOrder() {
        return (false);
    }


    /**
     *  Gets the recipe search order. 
     *
     *  @return the recipe search order 
     *  @throws org.osid.IllegalStateException <code> 
     *          supportsRecipeSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.RecipeSearchOrder getRecipeSearchOrder() {
        throw new org.osid.UnimplementedException("supportsRecipeSearchOrder() is false");
    }


    /**
     *  Gets the Recipe column name.
     *
     * @return the column name
     */

    protected String getRecipeColumn() {
        return ("recipe");
    }


    /**
     *  Sets the procedure <code> Id </code> for this query. 
     *
     *  @param  procedureId the procedure <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> procedureId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProcedureId(org.osid.id.Id procedureId, boolean match) {
        getAssembler().addIdTerm(getProcedureIdColumn(), procedureId, match);
        return;
    }


    /**
     *  Clears the procedure <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProcedureIdTerms() {
        getAssembler().clearTerms(getProcedureIdColumn());
        return;
    }


    /**
     *  Gets the procedure query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProcedureIdTerms() {
        return (getAssembler().getIdTerms(getProcedureIdColumn()));
    }


    /**
     *  Gets the ProcedureId column name.
     *
     * @return the column name
     */

    protected String getProcedureIdColumn() {
        return ("procedure_id");
    }


    /**
     *  Tests if a <code> ProcedureQuery </code> is available. 
     *
     *  @return <code> true </code> if a procedure query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProcedureQuery() {
        return (false);
    }


    /**
     *  Gets the query for a procedure. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the procedure query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProcedureQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQuery getProcedureQuery() {
        throw new org.osid.UnimplementedException("supportsProcedureQuery() is false");
    }


    /**
     *  Matches directions with any procedure. 
     *
     *  @param  match <code> true </code> to match directions with any 
     *          procedure, <code> false </code> to match directions with no 
     *          procedures 
     */

    @OSID @Override
    public void matchAnyProcedure(boolean match) {
        getAssembler().addIdWildcardTerm(getProcedureColumn(), match);
        return;
    }


    /**
     *  Clears the procedure query terms. 
     */

    @OSID @Override
    public void clearProcedureTerms() {
        getAssembler().clearTerms(getProcedureColumn());
        return;
    }


    /**
     *  Gets the procedure query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.ProcedureQueryInspector[] getProcedureTerms() {
        return (new org.osid.recipe.ProcedureQueryInspector[0]);
    }


    /**
     *  Gets the Procedure column name.
     *
     * @return the column name
     */

    protected String getProcedureColumn() {
        return ("procedure");
    }


    /**
     *  Sets the ingredient <code> Id </code> for the ingredient. 
     *
     *  @param  ingredientId the ingredient <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> ingredientId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchIngredientId(org.osid.id.Id ingredientId, boolean match) {
        getAssembler().addIdTerm(getIngredientIdColumn(), ingredientId, match);
        return;
    }


    /**
     *  Clears the ingredient <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearIngredientIdTerms() {
        getAssembler().clearTerms(getIngredientIdColumn());
        return;
    }


    /**
     *  Gets the ingredient query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getIngredientIdTerms() {
        return (getAssembler().getIdTerms(getIngredientIdColumn()));
    }


    /**
     *  Gets the IngredientId column name.
     *
     * @return the column name
     */

    protected String getIngredientIdColumn() {
        return ("ingredient_id");
    }


    /**
     *  Tests if an <code> IngredientQuery </code> is available. 
     *
     *  @return <code> true </code> if a stocjk query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsIngredientQuery() {
        return (false);
    }


    /**
     *  Gets the query for an ingredient item. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the ingredient query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsIngredientItemQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.IngredientQuery getIngredientQuery() {
        throw new org.osid.UnimplementedException("supportsIngredientQuery() is false");
    }


    /**
     *  Matches directions with any ingredient. 
     *
     *  @param  match <code> true </code> to match directions with any ingredient, 
     *          <code> false </code> to match directions with no ingredient 
     */

    @OSID @Override
    public void matchAnyIngredient(boolean match) {
        getAssembler().addIdWildcardTerm(getIngredientColumn(), match);
        return;
    }


    /**
     *  Clears the ingredient query terms. 
     */

    @OSID @Override
    public void clearIngredientTerms() {
        getAssembler().clearTerms(getIngredientColumn());
        return;
    }


    /**
     *  Gets the ingredient query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.IngredientQueryInspector[] getIngredientTerms() {
        return (new org.osid.recipe.IngredientQueryInspector[0]);
    }


    /**
     *  Gets the Ingredient column name.
     *
     * @return the column name
     */

    protected String getIngredientColumn() {
        return ("ingredient");
    }


    /**
     *  Matches directions with an estimated duration between the given range 
     *  inclusive. 
     *
     *  @param  start starting duration 
     *  @param  end ending duration 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> start </code> is 
     *          greater than <code> end </code> 
     *  @throws org.osid.NullArgumentException <code> start </code> or <code> 
     *          end </code> is <code> null </code> 
     */

    @OSID @Override
    public void matchEstimatedDuration(org.osid.calendaring.Duration start, 
                                       org.osid.calendaring.Duration end, 
                                       boolean match) {
        getAssembler().addDurationRangeTerm(getEstimatedDurationColumn(), start, end, match);
        return;
    }


    /**
     *  Matches directions with any estimated duration. 
     *
     *  @param  match <code> true </code> to match directions with any 
     *          estimated duration, <code> false </code> to match directions 
     *          with no estimated duration 
     */

    @OSID @Override
    public void matchAnyEstimatedDuration(boolean match) {
        getAssembler().addDurationRangeWildcardTerm(getEstimatedDurationColumn(), match);
        return;
    }


    /**
     *  Clears the duration query terms. 
     */

    @OSID @Override
    public void clearEstimatedDurationTerms() {
        getAssembler().clearTerms(getEstimatedDurationColumn());
        return;
    }


    /**
     *  Gets the estimated duration query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.DurationRangeTerm[] getEstimatedDurationTerms() {
        return (getAssembler().getDurationRangeTerms(getEstimatedDurationColumn()));
    }


    /**
     *  Orders the results by the duration. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByEstimatedDuration(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getEstimatedDurationColumn(), style);
        return;
    }


    /**
     *  Gets the EstimatedDuration column name.
     *
     * @return the column name
     */

    protected String getEstimatedDurationColumn() {
        return ("estimated_duration");
    }


    /**
     *  Sets the asset <code> Id </code> for this query. 
     *
     *  @param  assetId the asset <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> assetId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchAssetId(org.osid.id.Id assetId, boolean match) {
        getAssembler().addIdTerm(getAssetIdColumn(), assetId, match);
        return;
    }


    /**
     *  Clears the asset <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearAssetIdTerms() {
        getAssembler().clearTerms(getAssetIdColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getAssetIdTerms() {
        return (getAssembler().getIdTerms(getAssetIdColumn()));
    }


    /**
     *  Gets the AssetId column name.
     *
     * @return the column name
     */

    protected String getAssetIdColumn() {
        return ("asset_id");
    }


    /**
     *  Tests if an <code> AssetQuery </code> is available. 
     *
     *  @return <code> true </code> if an asset query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsAssetQuery() {
        return (false);
    }


    /**
     *  Gets the query for an asset. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the asset query 
     *  @throws org.osid.UnimplementedException <code> supportsAssetQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.repository.AssetQuery getAssetQuery() {
        throw new org.osid.UnimplementedException("supportsAssetQuery() is false");
    }


    /**
     *  Matches directions with any asset. 
     *
     *  @param  match <code> true </code> to match directions with any asset, 
     *          <code> false </code> to match directions with no assets 
     */

    @OSID @Override
    public void matchAnyAsset(boolean match) {
        getAssembler().addIdWildcardTerm(getAssetColumn(), match);
        return;
    }


    /**
     *  Clears the asset query terms. 
     */

    @OSID @Override
    public void clearAssetTerms() {
        getAssembler().clearTerms(getAssetColumn());
        return;
    }


    /**
     *  Gets the asset query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.repository.AssetQueryInspector[] getAssetTerms() {
        return (new org.osid.repository.AssetQueryInspector[0]);
    }


    /**
     *  Gets the Asset column name.
     *
     * @return the column name
     */

    protected String getAssetColumn() {
        return ("asset");
    }


    /**
     *  Sets the award <code> Id </code> for this query to match directions 
     *  assigned to cookbooks. 
     *
     *  @param  cookbookId a cook book <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cookbookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCookbookId(org.osid.id.Id cookbookId, boolean match) {
        getAssembler().addIdTerm(getCookbookIdColumn(), cookbookId, match);
        return;
    }


    /**
     *  Clears the cook book <code> Id </code> terms. 
     */

    @OSID @Override
    public void clearCookbookIdTerms() {
        getAssembler().clearTerms(getCookbookIdColumn());
        return;
    }


    /**
     *  Gets the cook book <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCookbookIdTerms() {
        return (getAssembler().getIdTerms(getCookbookIdColumn()));
    }


    /**
     *  Gets the CookbookId column name.
     *
     * @return the column name
     */

    protected String getCookbookIdColumn() {
        return ("cookbook_id");
    }


    /**
     *  Tests if a <code> CookbookQuery </code> is available. 
     *
     *  @return <code> true </code> if a cook book query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCookbookQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cookbook. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the cook book query 
     *  @throws org.osid.UnimplementedException <code> supportsCookbookQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQuery getCookbookQuery() {
        throw new org.osid.UnimplementedException("supportsCookbookQuery() is false");
    }


    /**
     *  Clears the cook book query terms. 
     */

    @OSID @Override
    public void clearCookbookTerms() {
        getAssembler().clearTerms(getCookbookColumn());
        return;
    }


    /**
     *  Gets the cook book query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.recipe.CookbookQueryInspector[] getCookbookTerms() {
        return (new org.osid.recipe.CookbookQueryInspector[0]);
    }


    /**
     *  Gets the Cookbook column name.
     *
     * @return the column name
     */

    protected String getCookbookColumn() {
        return ("cookbook");
    }


    /**
     *  Tests if this direction supports the given record
     *  <code>Type</code>.
     *
     *  @param  directionRecordType a direction record type 
     *  @return <code>true</code> if the directionRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type directionRecordType) {
        for (org.osid.recipe.records.DirectionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(directionRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  directionRecordType the direction record type 
     *  @return the direction query record 
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionQueryRecord getDirectionQueryRecord(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.DirectionQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(directionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directionRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  directionRecordType the direction record type 
     *  @return the direction query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionQueryInspectorRecord getDirectionQueryInspectorRecord(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.DirectionQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(directionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directionRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param directionRecordType the direction record type
     *  @return the direction search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>directionRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(directionRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.recipe.records.DirectionSearchOrderRecord getDirectionSearchOrderRecord(org.osid.type.Type directionRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.recipe.records.DirectionSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(directionRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(directionRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this direction. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param directionQueryRecord the direction query record
     *  @param directionQueryInspectorRecord the direction query inspector
     *         record
     *  @param directionSearchOrderRecord the direction search order record
     *  @param directionRecordType direction record type
     *  @throws org.osid.NullArgumentException
     *          <code>directionQueryRecord</code>,
     *          <code>directionQueryInspectorRecord</code>,
     *          <code>directionSearchOrderRecord</code> or
     *          <code>directionRecordTypedirection</code> is
     *          <code>null</code>
     */
            
    protected void addDirectionRecords(org.osid.recipe.records.DirectionQueryRecord directionQueryRecord, 
                                      org.osid.recipe.records.DirectionQueryInspectorRecord directionQueryInspectorRecord, 
                                      org.osid.recipe.records.DirectionSearchOrderRecord directionSearchOrderRecord, 
                                      org.osid.type.Type directionRecordType) {

        addRecordType(directionRecordType);

        nullarg(directionQueryRecord, "direction query record");
        nullarg(directionQueryInspectorRecord, "direction query inspector record");
        nullarg(directionSearchOrderRecord, "direction search odrer record");

        this.queryRecords.add(directionQueryRecord);
        this.queryInspectorRecords.add(directionQueryInspectorRecord);
        this.searchOrderRecords.add(directionSearchOrderRecord);
        
        return;
    }
}
