//
// AbstractStepConstrainerLookupSession.java
//
//    A starter implementation framework for providing a StepConstrainer
//    lookup service.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.workflow.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A starter implementation framework for providing a StepConstrainer
 *  lookup service.
 *
 *  Although this abstract class requires only the implementation of
 *  getStepConstrainers(), this other methods may need to be overridden for
 *  better performance.
 */

public abstract class AbstractStepConstrainerLookupSession
    extends net.okapia.osid.jamocha.spi.AbstractOsidSession
    implements org.osid.workflow.rules.StepConstrainerLookupSession {

    private boolean pedantic      = false;
    private boolean activeonly    = false;
    private boolean federated     = false;
    private org.osid.workflow.Office office = new net.okapia.osid.jamocha.nil.workflow.office.UnknownOffice();
    

    /**
     *  Gets the <code>Office/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Office Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getOfficeId() {
        return (this.office.getId());
    }


    /**
     *  Gets the <code>Office</code> associated with this 
     *  session.
     *
     *  @return the <code>Office</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.Office getOffice()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.office);
    }


    /**
     *  Sets the <code>Office</code>.
     *
     *  @param  office the office for this session
     *  @throws org.osid.NullArgumentException <code>office</code>
     *          is <code>null</code>
     */

    protected void setOffice(org.osid.workflow.Office office) {
        nullarg(office, "office");
        this.office = office;
        return;
    }


    /**
     *  Tests if this user can perform <code>StepConstrainer</code> 
     *  lookups.
     *
     *  @return <code>true</code>
     */

    @OSID @Override
    public boolean canLookupStepConstrainers() {
        return (true);
    }


    /**
     *  A complete view of the <code>StepConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeStepConstrainerView() {
        this.pedantic = false;
        return;
    }


    /**
     *  A complete view of the <code>StepConstrainer</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryStepConstrainerView() {
        this.pedantic = true;
        return;
    }


    /**
     *  Tests if a comparative or plenary view is set.
     *
     *  @return <code>true</code> if comparative</code>,
     *          <code>false</code> if plenary
     */

    protected boolean isComparative() {
        return (!this.pedantic);
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include step constrainers in offices which are children
     *  of this office in the office hierarchy.
     */

    @OSID @Override
    public void useFederatedOfficeView() {
        this.federated = true;
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this office only.
     */

    @OSID @Override
    public void useIsolatedOfficeView() {
        this.federated = false;
        return;
    }
    

    /**
     *  Tests if a federated view is set.
     *
     *  @return <code>true</code> if federated view,
     *          <code>false</code> otherwise
     */

    protected boolean isFederated() {
        return (this.federated);
    }


    /**
     *  Only active step constrainers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveStepConstrainerView() {
        this.activeonly = true;
        return;
    }


    /**
     *  Active and inactive step constrainers are returned by methods in this
     *  session.
     */
    
    @OSID @Override
    public void useAnyStatusStepConstrainerView() {
       this.activeonly = false;
       return;
    }


    /**
     *  Tests if an active or any status view is set.
     *
     *  @return <code>true</code> if active only</code>,
     *          <code>false</code> if both active and inactive
     */
    
    protected boolean isActiveOnly() {
        return (this.activeonly);
    }
    
     
    /**
     *  Gets the <code>StepConstrainer</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>StepConstrainer</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>StepConstrainer</code> and
     *  retained for compatibility.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @param  stepConstrainerId <code>Id</code> of the
     *          <code>StepConstrainer</code>
     *  @return the step constrainer
     *  @throws org.osid.NotFoundException <code>stepConstrainerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>stepConstrainerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainer getStepConstrainer(org.osid.id.Id stepConstrainerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        try (org.osid.workflow.rules.StepConstrainerList stepConstrainers = getStepConstrainers()) {
            while (stepConstrainers.hasNext()) {
                org.osid.workflow.rules.StepConstrainer stepConstrainer = stepConstrainers.getNextStepConstrainer();
                if (stepConstrainer.getId().equals(stepConstrainerId)) {
                    return (stepConstrainer);
                }
            }
        } 

        throw new org.osid.NotFoundException(stepConstrainerId + " not found");
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  stepConstrainers specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>StepConstrainers</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Ids retrieved from <code>getStepConstrainers()</code>.
     *
     *  @param  stepConstrainerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NotFoundException an <code>Id was</code> not found 
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByIds(org.osid.id.IdList stepConstrainerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        java.util.Collection<org.osid.workflow.rules.StepConstrainer> ret = new java.util.ArrayList<>();

        try (org.osid.id.IdList ids = stepConstrainerIds) {
            while (ids.hasNext()) {
                org.osid.id.Id id = ids.getNextId();
                try {
                    ret.add(getStepConstrainer(id));
                } catch (org.osid.NotFoundException nfe) {
                    if (!isComparative()) {
                        throw new org.osid.NotFoundException("step constrainer " + id + " not found");
                    } 
                }
            }
        }
            
        return (new net.okapia.osid.jamocha.workflow.rules.stepconstrainer.LinkedStepConstrainerList(ret));
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the given
     *  step constrainer genus <code>Type</code> which does not include
     *  step constrainers of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  types retrieved from <code>getStepConstrainers()</code>.
     *
     *  @param  stepConstrainerGenusType a stepConstrainer genus type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByGenusType(org.osid.type.Type stepConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepconstrainer.StepConstrainerGenusFilterList(getStepConstrainers(), stepConstrainerGenusType));
    }


    /**
     *  Gets a <code>StepConstrainerList</code> corresponding to the given
     *  step constrainer genus <code>Type</code> and include any additional
     *  step constrainers with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStepConstrainers()</code>.
     *
     *  @param  stepConstrainerGenusType a stepConstrainer genus type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByParentGenusType(org.osid.type.Type stepConstrainerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (getStepConstrainersByGenusType(stepConstrainerGenusType));
    }


    /**
     *  Gets a <code>StepConstrainerList</code> containing the given
     *  step constrainer record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  step constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  The default implementation of this method simply examines the
     *  Types retrieved from <code>getStepConstrainers()</code>.
     *
     *  @param  stepConstrainerRecordType a stepConstrainer record type 
     *  @return the returned <code>StepConstrainer</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>stepConstrainerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.workflow.rules.StepConstrainerList getStepConstrainersByRecordType(org.osid.type.Type stepConstrainerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepconstrainer.StepConstrainerRecordFilterList(getStepConstrainers(), stepConstrainerRecordType));
    }


    /**
     *  Gets all <code>StepConstrainers</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  step constrainers or an error results. Otherwise, the returned list
     *  may contain only those step constrainers that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  In active mode, step constrainers are returned that are currently
     *  active. In any status mode, active and inactive step constrainers
     *  are returned.
     *
     *  @return a list of <code>StepConstrainers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public abstract org.osid.workflow.rules.StepConstrainerList getStepConstrainers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException;


    /**
     *  Filters the step constrainer list for active and effective
     *  views. Should be called by <code>getObjects()</code> if no
     *  filtering is already performed.
     *
     *  @param list the list of step constrainers
     *  @return the filtered list
     *  @throws org.osid.OperationFailedException unable tom complete request
     */

    protected org.osid.workflow.rules.StepConstrainerList filterStepConstrainersOnViews(org.osid.workflow.rules.StepConstrainerList list)
        throws org.osid.OperationFailedException {

        org.osid.workflow.rules.StepConstrainerList ret = list;

        if (isActiveOnly()) {
            ret = new net.okapia.osid.jamocha.inline.filter.workflow.rules.stepconstrainer.ActiveStepConstrainerFilterList(ret);
        }

        return (ret);
    }
}
