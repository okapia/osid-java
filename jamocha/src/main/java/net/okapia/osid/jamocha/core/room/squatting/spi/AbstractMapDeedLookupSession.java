//
// AbstractMapDeedLookupSession
//
//    A simple framework for providing a Deed lookup service
//    backed by a fixed collection of deeds.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.squatting.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Deed lookup service backed by a
 *  fixed collection of deeds. The deeds are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Deeds</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapDeedLookupSession
    extends net.okapia.osid.jamocha.room.squatting.spi.AbstractDeedLookupSession
    implements org.osid.room.squatting.DeedLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.room.squatting.Deed> deeds = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.room.squatting.Deed>());


    /**
     *  Makes a <code>Deed</code> available in this session.
     *
     *  @param  deed a deed
     *  @throws org.osid.NullArgumentException <code>deed<code>
     *          is <code>null</code>
     */

    protected void putDeed(org.osid.room.squatting.Deed deed) {
        this.deeds.put(deed.getId(), deed);
        return;
    }


    /**
     *  Makes an array of deeds available in this session.
     *
     *  @param  deeds an array of deeds
     *  @throws org.osid.NullArgumentException <code>deeds<code>
     *          is <code>null</code>
     */

    protected void putDeeds(org.osid.room.squatting.Deed[] deeds) {
        putDeeds(java.util.Arrays.asList(deeds));
        return;
    }


    /**
     *  Makes a collection of deeds available in this session.
     *
     *  @param  deeds a collection of deeds
     *  @throws org.osid.NullArgumentException <code>deeds<code>
     *          is <code>null</code>
     */

    protected void putDeeds(java.util.Collection<? extends org.osid.room.squatting.Deed> deeds) {
        for (org.osid.room.squatting.Deed deed : deeds) {
            this.deeds.put(deed.getId(), deed);
        }

        return;
    }


    /**
     *  Removes a Deed from this session.
     *
     *  @param  deedId the <code>Id</code> of the deed
     *  @throws org.osid.NullArgumentException <code>deedId<code> is
     *          <code>null</code>
     */

    protected void removeDeed(org.osid.id.Id deedId) {
        this.deeds.remove(deedId);
        return;
    }


    /**
     *  Gets the <code>Deed</code> specified by its <code>Id</code>.
     *
     *  @param  deedId <code>Id</code> of the <code>Deed</code>
     *  @return the deed
     *  @throws org.osid.NotFoundException <code>deedId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>deedId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.Deed getDeed(org.osid.id.Id deedId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.squatting.Deed deed = this.deeds.get(deedId);
        if (deed == null) {
            throw new org.osid.NotFoundException("deed not found: " + deedId);
        }

        return (deed);
    }


    /**
     *  Gets all <code>Deeds</code>. In plenary mode, the returned
     *  list contains all known deeds or an error
     *  results. Otherwise, the returned list may contain only those
     *  deeds that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Deeds</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.squatting.DeedList getDeeds()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.squatting.deed.ArrayDeedList(this.deeds.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.deeds.clear();
        super.close();
        return;
    }
}
