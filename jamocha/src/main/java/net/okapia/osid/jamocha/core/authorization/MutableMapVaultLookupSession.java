//
// MutableMapVaultLookupSession
//
//    Implements a Vault lookup service backed by a collection of
//    vaults that can be modified after instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements a Vault lookup service backed by a collection of
 *  vaults. The vaults are indexed only by
 *  {@code Id}. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *
 *  The collection of vaults can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableMapVaultLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractMapVaultLookupSession
    implements org.osid.authorization.VaultLookupSession {


    /**
     *  Constructs a new {@code MutableMapVaultLookupSession}
     *  with no vaults.
     */

    public MutableMapVaultLookupSession() {
        return;
    }


    /**
     *  Constructs a new {@code MutableMapVaultLookupSession} with a
     *  single vault.
     *  
     *  @param vault a vault
     *  @throws org.osid.NullArgumentException {@code vault}
     *          is {@code null}
     */

    public MutableMapVaultLookupSession(org.osid.authorization.Vault vault) {
        putVault(vault);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapVaultLookupSession}
     *  using an array of vaults.
     *
     *  @param vaults an array of vaults
     *  @throws org.osid.NullArgumentException {@code vaults}
     *          is {@code null}
     */

    public MutableMapVaultLookupSession(org.osid.authorization.Vault[] vaults) {
        putVaults(vaults);
        return;
    }


    /**
     *  Constructs a new {@code MutableMapVaultLookupSession}
     *  using a collection of vaults.
     *
     *  @param vaults a collection of vaults
     *  @throws org.osid.NullArgumentException {@code vaults}
     *          is {@code null}
     */

    public MutableMapVaultLookupSession(java.util.Collection<? extends org.osid.authorization.Vault> vaults) {
        putVaults(vaults);
        return;
    }

    
    /**
     *  Makes a {@code Vault} available in this session.
     *
     *  @param vault a vault
     *  @throws org.osid.NullArgumentException {@code vault{@code  is
     *          {@code null}
     */

    @Override
    public void putVault(org.osid.authorization.Vault vault) {
        super.putVault(vault);
        return;
    }


    /**
     *  Makes an array of vaults available in this session.
     *
     *  @param vaults an array of vaults
     *  @throws org.osid.NullArgumentException {@code vaults{@code 
     *          is {@code null}
     */

    @Override
    public void putVaults(org.osid.authorization.Vault[] vaults) {
        super.putVaults(vaults);
        return;
    }


    /**
     *  Makes collection of vaults available in this session.
     *
     *  @param vaults a collection of vaults
     *  @throws org.osid.NullArgumentException {@code vaults{@code  is
     *          {@code null}
     */

    @Override
    public void putVaults(java.util.Collection<? extends org.osid.authorization.Vault> vaults) {
        super.putVaults(vaults);
        return;
    }


    /**
     *  Removes a Vault from this session.
     *
     *  @param vaultId the {@code Id} of the vault
     *  @throws org.osid.NullArgumentException {@code vaultId{@code 
     *          is {@code null}
     */

    @Override
    public void removeVault(org.osid.id.Id vaultId) {
        super.removeVault(vaultId);
        return;
    }    
}
