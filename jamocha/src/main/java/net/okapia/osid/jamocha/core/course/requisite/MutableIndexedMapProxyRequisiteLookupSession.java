//
// MutableIndexedMapProxyRequisiteLookupSession
//
//    Implements a Requisite lookup service backed by a collection of
//    requisites indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.course.requisite;


/**
 *  Implements a Requisite lookup service backed by a collection of
 *  requisites. The requisites are indexed by {@code Id}, genus
 *  and record types.
 *
 *  The type indices are created from {@code getGenusType()}
 *  and {@code getRecordTypes()}. Some requisites may be compatible
 *  with more types than are indicated through these requisite
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of requisites can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapProxyRequisiteLookupSession
    extends net.okapia.osid.jamocha.core.course.requisite.spi.AbstractIndexedMapRequisiteLookupSession
    implements org.osid.course.requisite.RequisiteLookupSession {


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRequisiteLookupSession} with
     *  no requisite.
     *
     *  @param courseCatalog the course catalog
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog} or
     *          {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRequisiteLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.proxy.Proxy proxy) {
        setCourseCatalog(courseCatalog);
        setSessionProxy(proxy);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRequisiteLookupSession} with
     *  a single requisite.
     *
     *  @param courseCatalog the course catalog
     *  @param  requisite an requisite
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code requisite}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRequisiteLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.requisite.Requisite requisite, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putRequisite(requisite);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRequisiteLookupSession} using
     *  an array of requisites.
     *
     *  @param courseCatalog the course catalog
     *  @param  requisites an array of requisites
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code requisites}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRequisiteLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       org.osid.course.requisite.Requisite[] requisites, org.osid.proxy.Proxy proxy) {

        this(courseCatalog, proxy);
        putRequisites(requisites);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapProxyRequisiteLookupSession} using
     *  a collection of requisites.
     *
     *  @param courseCatalog the course catalog
     *  @param  requisites a collection of requisites
     *  @param  proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code courseCatalog},
     *          {@code requisites}, or {@code [proxy]} is {@code null}
     */

    public MutableIndexedMapProxyRequisiteLookupSession(org.osid.course.CourseCatalog courseCatalog,
                                                       java.util.Collection<? extends org.osid.course.requisite.Requisite> requisites,
                                                       org.osid.proxy.Proxy proxy) {
        this(courseCatalog, proxy);
        putRequisites(requisites);
        return;
    }

    
    /**
     *  Makes a {@code Requisite} available in this session.
     *
     *  @param  requisite a requisite
     *  @throws org.osid.NullArgumentException {@code requisite{@code 
     *          is {@code null}
     */

    @Override
    public void putRequisite(org.osid.course.requisite.Requisite requisite) {
        super.putRequisite(requisite);
        return;
    }


    /**
     *  Makes an array of requisites available in this session.
     *
     *  @param  requisites an array of requisites
     *  @throws org.osid.NullArgumentException {@code requisites{@code 
     *          is {@code null}
     */

    @Override
    public void putRequisites(org.osid.course.requisite.Requisite[] requisites) {
        super.putRequisites(requisites);
        return;
    }


    /**
     *  Makes collection of requisites available in this session.
     *
     *  @param  requisites a collection of requisites
     *  @throws org.osid.NullArgumentException {@code requisite{@code 
     *          is {@code null}
     */

    @Override
    public void putRequisites(java.util.Collection<? extends org.osid.course.requisite.Requisite> requisites) {
        super.putRequisites(requisites);
        return;
    }


    /**
     *  Removes a Requisite from this session.
     *
     *  @param requisiteId the {@code Id} of the requisite
     *  @throws org.osid.NullArgumentException {@code requisiteId{@code  is
     *          {@code null}
     */

    @Override
    public void removeRequisite(org.osid.id.Id requisiteId) {
        super.removeRequisite(requisiteId);
        return;
    }    
}
