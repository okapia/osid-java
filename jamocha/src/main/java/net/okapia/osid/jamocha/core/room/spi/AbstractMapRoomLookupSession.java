//
// AbstractMapRoomLookupSession
//
//    A simple framework for providing a Room lookup service
//    backed by a fixed collection of rooms.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.room.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a Room lookup service backed by a
 *  fixed collection of rooms. The rooms are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Rooms</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapRoomLookupSession
    extends net.okapia.osid.jamocha.room.spi.AbstractRoomLookupSession
    implements org.osid.room.RoomLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.room.Room> rooms = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.room.Room>());


    /**
     *  Makes a <code>Room</code> available in this session.
     *
     *  @param  room a room
     *  @throws org.osid.NullArgumentException <code>room<code>
     *          is <code>null</code>
     */

    protected void putRoom(org.osid.room.Room room) {
        this.rooms.put(room.getId(), room);
        return;
    }


    /**
     *  Makes an array of rooms available in this session.
     *
     *  @param  rooms an array of rooms
     *  @throws org.osid.NullArgumentException <code>rooms<code>
     *          is <code>null</code>
     */

    protected void putRooms(org.osid.room.Room[] rooms) {
        putRooms(java.util.Arrays.asList(rooms));
        return;
    }


    /**
     *  Makes a collection of rooms available in this session.
     *
     *  @param  rooms a collection of rooms
     *  @throws org.osid.NullArgumentException <code>rooms<code>
     *          is <code>null</code>
     */

    protected void putRooms(java.util.Collection<? extends org.osid.room.Room> rooms) {
        for (org.osid.room.Room room : rooms) {
            this.rooms.put(room.getId(), room);
        }

        return;
    }


    /**
     *  Removes a Room from this session.
     *
     *  @param  roomId the <code>Id</code> of the room
     *  @throws org.osid.NullArgumentException <code>roomId<code> is
     *          <code>null</code>
     */

    protected void removeRoom(org.osid.id.Id roomId) {
        this.rooms.remove(roomId);
        return;
    }


    /**
     *  Gets the <code>Room</code> specified by its <code>Id</code>.
     *
     *  @param  roomId <code>Id</code> of the <code>Room</code>
     *  @return the room
     *  @throws org.osid.NotFoundException <code>roomId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>roomId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.Room getRoom(org.osid.id.Id roomId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.room.Room room = this.rooms.get(roomId);
        if (room == null) {
            throw new org.osid.NotFoundException("room not found: " + roomId);
        }

        return (room);
    }


    /**
     *  Gets all <code>Rooms</code>. In plenary mode, the returned
     *  list contains all known rooms or an error
     *  results. Otherwise, the returned list may contain only those
     *  rooms that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>Rooms</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.room.RoomList getRooms()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.room.room.ArrayRoomList(this.rooms.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.rooms.clear();
        super.close();
        return;
    }
}
