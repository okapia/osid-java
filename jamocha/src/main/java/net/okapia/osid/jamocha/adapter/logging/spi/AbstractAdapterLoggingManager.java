//
// AbstractLoggingManager.java
//
//     An adapter for a LoggingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.logging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a LoggingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterLoggingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.logging.LoggingManager>
    implements org.osid.logging.LoggingManager {


    /**
     *  Constructs a new {@code AbstractAdapterLoggingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterLoggingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterLoggingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterLoggingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if visible federation is supported. 
     *
     *  @return <code> true </code> if visible federation is supproted, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if logging is supported. 
     *
     *  @return <code> true </code> if logging is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogging() {
        return (getAdapteeManager().supportsLogging());
    }


    /**
     *  Tests if reading logs is supported. 
     *
     *  @return <code> true </code> if reading logs is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryLookup() {
        return (getAdapteeManager().supportsLogEntryLookup());
    }


    /**
     *  Tests if querying log entries is supported. 
     *
     *  @return <code> true </code> if querying log entries is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryQuery() {
        return (getAdapteeManager().supportsLogEntryQuery());
    }


    /**
     *  Tests if searching log entries is supported. 
     *
     *  @return <code> true </code> if searching log entries is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntrySearch() {
        return (getAdapteeManager().supportsLogEntrySearch());
    }


    /**
     *  Tests if log entry notification is supported,. 
     *
     *  @return <code> true </code> if log entry notification is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryNotification() {
        return (getAdapteeManager().supportsLogEntryNotification());
    }


    /**
     *  Tests if looking up log entry log mappings is supported. 
     *
     *  @return <code> true </code> if log entry logs is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryLog() {
        return (getAdapteeManager().supportsLogEntryLog());
    }


    /**
     *  Tests if managing log entry log mappings is supported. 
     *
     *  @return <code> true </code> if log entry logs mapping assignment is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntryLogAssignment() {
        return (getAdapteeManager().supportsLogEntryLogAssignment());
    }


    /**
     *  Tests if smart logs is supported. 
     *
     *  @return <code> true </code> if smart logs is supported, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogEntrySmartLog() {
        return (getAdapteeManager().supportsLogEntrySmartLog());
    }


    /**
     *  Tests for the availability of a log lookup service. 
     *
     *  @return <code> true </code> if log lookup is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogLookup() {
        return (getAdapteeManager().supportsLogLookup());
    }


    /**
     *  Tests if querying logs is available. 
     *
     *  @return <code> true </code> if log query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogQuery() {
        return (getAdapteeManager().supportsLogQuery());
    }


    /**
     *  Tests if searching for logs is available. 
     *
     *  @return <code> true </code> if log search is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogSearch() {
        return (getAdapteeManager().supportsLogSearch());
    }


    /**
     *  Tests for the availability of a log administrative service for 
     *  creating and deleting logs. 
     *
     *  @return <code> true </code> if log administration is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogAdmin() {
        return (getAdapteeManager().supportsLogAdmin());
    }


    /**
     *  Tests for the availability of a log notification service. 
     *
     *  @return <code> true </code> if log notification is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogNotification() {
        return (getAdapteeManager().supportsLogNotification());
    }


    /**
     *  Tests for the availability of a log hierarchy traversal service. 
     *
     *  @return <code> true </code> if log hierarchy traversal is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogHierarchy() {
        return (getAdapteeManager().supportsLogHierarchy());
    }


    /**
     *  Tests for the availability of a log hierarchy design service. 
     *
     *  @return <code> true </code> if log hierarchy design is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLogHierarchyDesign() {
        return (getAdapteeManager().supportsLogHierarchyDesign());
    }


    /**
     *  Tests for the availability of a logging batch service. 
     *
     *  @return <code> true </code> if loggin batch service is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsLoggingBatch() {
        return (getAdapteeManager().supportsLoggingBatch());
    }


    /**
     *  Gets the supported <code> Log </code> record types. 
     *
     *  @return a list containing the supported log record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogEntryRecordTypes() {
        return (getAdapteeManager().getLogEntryRecordTypes());
    }


    /**
     *  Tests if the given <code> LogEntry </code> record type is supported. 
     *
     *  @param  logEntryRecordType a <code> Type </code> indicating a <code> 
     *          LogEntry </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logEntryRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogEntryRecordType(org.osid.type.Type logEntryRecordType) {
        return (getAdapteeManager().supportsLogEntryRecordType(logEntryRecordType));
    }


    /**
     *  Gets the supported log entry search record types. 
     *
     *  @return a list containing the supported log entry search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogEntrySearchRecordTypes() {
        return (getAdapteeManager().getLogEntrySearchRecordTypes());
    }


    /**
     *  Tests if the given log entry search record type is supported. 
     *
     *  @param  logEntrySearchRecordType a <code> Type </code> indicating a 
     *          log entry record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logEntrySearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogEntrySearchRecordType(org.osid.type.Type logEntrySearchRecordType) {
        return (getAdapteeManager().supportsLogEntrySearchRecordType(logEntrySearchRecordType));
    }


    /**
     *  Gets the supported <code> Log </code> record types. 
     *
     *  @return a list containing the supported log record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogRecordTypes() {
        return (getAdapteeManager().getLogRecordTypes());
    }


    /**
     *  Tests if the given <code> Log </code> record type is supported. 
     *
     *  @param  logRecordType a <code> Type </code> indicating a <code> Log 
     *          </code> record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logRecordType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogRecordType(org.osid.type.Type logRecordType) {
        return (getAdapteeManager().supportsLogRecordType(logRecordType));
    }


    /**
     *  Gets the supported log search record types. 
     *
     *  @return a list containing the supported log search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getLogSearchRecordTypes() {
        return (getAdapteeManager().getLogSearchRecordTypes());
    }


    /**
     *  Tests if the given log search record type is supported. 
     *
     *  @param  logSearchRecordType a <code> Type </code> indicating a log 
     *          record type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> logSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsLogSearchRecordType(org.osid.type.Type logSearchRecordType) {
        return (getAdapteeManager().supportsLogSearchRecordType(logSearchRecordType));
    }


    /**
     *  Gets the priority types supported, in ascending order of the priority 
     *  level. 
     *
     *  @return a list containing the supported priority types 
     */

    @OSID @Override
    public org.osid.type.TypeList getPriorityTypes() {
        return (getAdapteeManager().getPriorityTypes());
    }


    /**
     *  Tests if the priority type is supported. 
     *
     *  @param  priorityType a <code> Type </code> indicating a priority type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> priorityType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsPriorityType(org.osid.type.Type priorityType) {
        return (getAdapteeManager().supportsPriorityType(priorityType));
    }


    /**
     *  Gets the content types supported. 
     *
     *  @return a list containing the supported content types 
     */

    @OSID @Override
    public org.osid.type.TypeList getContentTypes() {
        return (getAdapteeManager().getContentTypes());
    }


    /**
     *  Tests if the content type is supported. 
     *
     *  @param  contentType a <code> Type </code> indicating a content type 
     *  @return <code> true </code> if the given <code> Type </code> is 
     *          supported, <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> contentType </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public boolean supportsContentType(org.osid.type.Type contentType) {
        return (getAdapteeManager().supportsContentType(contentType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging 
     *  service. 
     *
     *  @return a <code> LoggingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogging() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingSession getLoggingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLoggingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging 
     *  service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LoggingSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogging() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LoggingSession getLoggingSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLoggingSessionForLog(logId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log reading 
     *  service. 
     *
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLookupSession getLogEntryLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log reading 
     *  service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryLookupSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLookup() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLookupSession getLogEntryLookupSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryLookupSessionForLog(logId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  query service. 
     *
     *  @return a <code> LogEntryQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryQuerySession getLogEntryQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  query service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryQuerySession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryQuerySession getLogEntryQuerySessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryQuerySessionForLog(logId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  search service. 
     *
     *  @return a <code> LogEntrySearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntrySearch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntrySearchSession getLogEntrySearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntrySearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  search service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntrySearchSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntrySearch() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntrySearchSession getLogEntrySearchSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntrySearchSessionForLog(logId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  administrative service. 
     *
     *  @return a <code> LogEntryAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryAdminSession getLogEntryAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  administrative service for the given log. 
     *
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryAdminSession getLogEntryAdminSessionForLog(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryAdminSessionForLog(logId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the logging entry 
     *  notification service. 
     *
     *  @param  logEntryReceiver the receiver 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryNotificationSession getLogEntryNotificationSession(org.osid.logging.LogEntryReceiver logEntryReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryNotificationSession(logEntryReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log entry 
     *  notification service for the given log. 
     *
     *  @param  logEntryReceiver the receiver 
     *  @param  logId the <code> Id </code> of the <code> Log </code> 
     *  @return a <code> LogEntryNotificationSession </code> 
     *  @throws org.osid.NotFoundException no <code> Log </code> found by the 
     *          given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> logEntryReceiver </code> 
     *          or <code> logId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryNotificationSession getLogEntryNotificationSessionForLog(org.osid.logging.LogEntryReceiver logEntryReceiver, 
                                                                                             org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryNotificationSessionForLog(logEntryReceiver, logId));
    }


    /**
     *  Gets the session for retrieving log entry to log mappings. 
     *
     *  @return a <code> LogEntryLogSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogEntryLog() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLogSession getLogEntryLogSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryLogSession());
    }


    /**
     *  Gets the session for assigning log entry to logs mappings. 
     *
     *  @return a <code> LogEntryLogAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntryLogAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntryLogAssignmentSession getLogEntryLogAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntryLogAssignmentSession());
    }


    /**
     *  Gets the session for managing dynamic logEntry log. 
     *
     *  @param  logId the <code> Id </code> of the log 
     *  @return a <code> LogEntrySmartLogSession </code> 
     *  @throws org.osid.NotFoundException <code> logId </code> not found 
     *  @throws org.osid.NullArgumentException <code> logId </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogEntrySmartLog() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogEntrySmartLogSession getLogEntrySmartLogSession(org.osid.id.Id logId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getLogEntrySmartLogSession(logId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log lookup 
     *  service. 
     *
     *  @return a <code> LogLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogLookupSession getLogLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log query 
     *  service. 
     *
     *  @return a <code> LogQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogQuerySession getLogQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log search 
     *  service. 
     *
     *  @return a <code> LogSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogSearchSession getLogSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogSearchSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log 
     *  administrative service. 
     *
     *  @return a <code> LogAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogAdminSession getLogAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log 
     *  notification service. 
     *
     *  @param  logReceiver the receiver 
     *  @return a <code> LogNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> logReceiver </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogNotificationSession getLogNotificationSession(org.osid.logging.LogReceiver logReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogNotificationSession(logReceiver));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log hierarchy 
     *  service. 
     *
     *  @return a <code> LogHierarchySession </code> for logs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLogHierarchy() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogHierarchySession getLogHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogHierarchySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the log hierarchy 
     *  design service. 
     *
     *  @return a <code> HierarchyDesignSession </code> for logs 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsLogHierarchyDesign() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.LogHierarchyDesignSession getLogHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLogHierarchyDesignSession());
    }


    /**
     *  Gets a <code> LoggingBatchManager. </code> 
     *
     *  @return a <code> LoggingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsLoggingBatch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.logging.batch.LoggingBatchManager getLoggingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getLoggingBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
