//
// AbstractFederatingAgentLookupSession.java
//
//     An abstract federating adapter for an AgentLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.authentication.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AgentLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.</p>
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAgentLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.authentication.AgentLookupSession>
    implements org.osid.authentication.AgentLookupSession {

    private boolean parallel = false;
    private org.osid.authentication.Agency agency = new net.okapia.osid.jamocha.nil.authentication.agency.UnknownAgency();


    /**
     *  Constructs a new <code>AbstractFederatingAgentLookupSession</code>.
     */

    protected AbstractFederatingAgentLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.authentication.AgentLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>Agency/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>Agency Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAgencyId() {
        return (this.agency.getId());
    }


    /**
     *  Gets the <code>Agency</code> associated with this 
     *  session.
     *
     *  @return the <code>Agency</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agency getAgency()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.agency);
    }


    /**
     *  Sets the <code>Agency</code>.
     *
     *  @param  agency the agency for this session
     *  @throws org.osid.NullArgumentException <code>agency</code>
     *          is <code>null</code>
     */

    protected void setAgency(org.osid.authentication.Agency agency) {
        nullarg(agency, "agency");
        this.agency = agency;
        return;
    }


    /**
     *  Tests if this user can perform <code>Agent</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAgents() {
        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            if (session.canLookupAgents()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Agent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAgentView() {
        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            session.useComparativeAgentView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Agent</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAgentView() {
        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            session.usePlenaryAgentView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include agents in agencies which are children
     *  of this agency in the agency hierarchy.
     */

    @OSID @Override
    public void useFederatedAgencyView() {
        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            session.useFederatedAgencyView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this agency only.
     */

    @OSID @Override
    public void useIsolatedAgencyView() {
        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            session.useIsolatedAgencyView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Agent</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Agent</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Agent</code> and
     *  retained for compatibility.
     *
     *  @param  agentId <code>Id</code> of the
     *          <code>Agent</code>
     *  @return the agent
     *  @throws org.osid.NotFoundException <code>agentId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>agentId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.Agent getAgent(org.osid.id.Id agentId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            try {
                return (session.getAgent(agentId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(agentId + " not found");
    }


    /**
     *  Gets an <code>AgentList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  agents specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Agents</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  agentIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Agent</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>agentIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByIds(org.osid.id.IdList agentIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.authentication.agent.MutableAgentList ret = new net.okapia.osid.jamocha.authentication.agent.MutableAgentList();

        try (org.osid.id.IdList ids = agentIds) {
            while (ids.hasNext()) {
                ret.addAgent(getAgent(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AgentList</code> corresponding to the given
     *  agent genus <code>Type</code> which does not include
     *  agents of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agentGenusType an agent genus type 
     *  @return the returned <code>Agent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agentGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByGenusType(org.osid.type.Type agentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.agent.FederatingAgentList ret = getAgentList();

        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            ret.addAgentList(session.getAgentsByGenusType(agentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AgentList</code> corresponding to the given
     *  agent genus <code>Type</code> and include any additional
     *  agents with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agentGenusType an agent genus type 
     *  @return the returned <code>Agent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agentGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByParentGenusType(org.osid.type.Type agentGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.agent.FederatingAgentList ret = getAgentList();

        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            ret.addAgentList(session.getAgentsByParentGenusType(agentGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AgentList</code> containing the given
     *  agent record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  agentRecordType an agent record type 
     *  @return the returned <code>Agent</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>agentRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgentsByRecordType(org.osid.type.Type agentRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.agent.FederatingAgentList ret = getAgentList();

        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            ret.addAgentList(session.getAgentsByRecordType(agentRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Agents</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  agents or an error results. Otherwise, the returned list
     *  may contain only those agents that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Agents</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.authentication.AgentList getAgents()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.authentication.agent.FederatingAgentList ret = getAgentList();

        for (org.osid.authentication.AgentLookupSession session : getSessions()) {
            ret.addAgentList(session.getAgents());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.authentication.agent.FederatingAgentList getAgentList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.authentication.agent.ParallelAgentList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.authentication.agent.CompositeAgentList());
        }
    }
}
