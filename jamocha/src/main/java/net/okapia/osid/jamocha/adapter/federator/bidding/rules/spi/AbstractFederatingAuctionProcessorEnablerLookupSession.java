//
// AbstractFederatingAuctionProcessorEnablerLookupSession.java
//
//     An abstract federating adapter for an AuctionProcessorEnablerLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.bidding.rules.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for an
 *  AuctionProcessorEnablerLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingAuctionProcessorEnablerLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.bidding.rules.AuctionProcessorEnablerLookupSession>
    implements org.osid.bidding.rules.AuctionProcessorEnablerLookupSession {

    private boolean parallel = false;
    private org.osid.bidding.AuctionHouse auctionHouse = new net.okapia.osid.jamocha.nil.bidding.auctionhouse.UnknownAuctionHouse();


    /**
     *  Constructs a new <code>AbstractFederatingAuctionProcessorEnablerLookupSession</code>.
     */

    protected AbstractFederatingAuctionProcessorEnablerLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Gets the <code>AuctionHouse/code> <code>Id</code> associated
     *  with this session.
     *
     *  @return the <code>AuctionHouse Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getAuctionHouseId() {
        return (this.auctionHouse.getId());
    }


    /**
     *  Gets the <code>AuctionHouse</code> associated with this 
     *  session.
     *
     *  @return the <code>AuctionHouse</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.AuctionHouse getAuctionHouse()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.auctionHouse);
    }


    /**
     *  Sets the <code>AuctionHouse</code>.
     *
     *  @param  auctionHouse the auction house for this session
     *  @throws org.osid.NullArgumentException <code>auctionHouse</code>
     *          is <code>null</code>
     */

    protected void setAuctionHouse(org.osid.bidding.AuctionHouse auctionHouse) {
        nullarg(auctionHouse, "auction house");
        this.auctionHouse = auctionHouse;
        return;
    }


    /**
     *  Tests if this user can perform <code>AuctionProcessorEnabler</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupAuctionProcessorEnablers() {
        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            if (session.canLookupAuctionProcessorEnablers()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>AuctionProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeAuctionProcessorEnablerView() {
        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            session.useComparativeAuctionProcessorEnablerView();
        }

        return;
    }


    /**
     *  A complete view of the <code>AuctionProcessorEnabler</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryAuctionProcessorEnablerView() {
        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            session.usePlenaryAuctionProcessorEnablerView();
        }

        return;
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include auction processor enablers in auction houses which are children
     *  of this auction house in the auction house hierarchy.
     */

    @OSID @Override
    public void useFederatedAuctionHouseView() {
        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            session.useFederatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts lookups to this auction house only.
     */

    @OSID @Override
    public void useIsolatedAuctionHouseView() {
        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            session.useIsolatedAuctionHouseView();
        }

        return;
    }


    /**
     *  Only active auction processor enablers are returned by methods in this session. 
     */
     
    @OSID @Override
    public void useActiveAuctionProcessorEnablerView() {
        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            session.useActiveAuctionProcessorEnablerView();
        }

        return;
    }


    /**
     *  Active and inactive auction processor enablers are returned by
     *  methods in this session.
     */
    
    @OSID @Override
    public void useAnyStatusAuctionProcessorEnablerView() {
        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            session.useAnyStatusAuctionProcessorEnablerView();
        }

        return;
    }

     
    /**
     *  Gets the <code>AuctionProcessorEnabler</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>AuctionProcessorEnabler</code> may have a different
     *  <code>Id</code> than requested, such as the case where a
     *  duplicate <code>Id</code> was assigned to a
     *  <code>AuctionProcessorEnabler</code> and retained for
     *  compatibility.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  auctionProcessorEnablerId <code>Id</code> of the
     *          <code>AuctionProcessorEnabler</code>
     *  @return the auction processor enabler
     *  @throws org.osid.NotFoundException <code>auctionProcessorEnablerId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>auctionProcessorEnablerId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnabler getAuctionProcessorEnabler(org.osid.id.Id auctionProcessorEnablerId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            try {
                return (session.getAuctionProcessorEnabler(auctionProcessorEnablerId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(auctionProcessorEnablerId + " not found");
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> corresponding
     *  to the given <code>IdList</code>.
     *
     *  In plenary mode, the returned list contains all of the
     *  auctionProcessorEnablers specified in the <code>Id</code>
     *  list, in the order of the list, including duplicates, or an
     *  error results if an <code>Id</code> in the supplied list is
     *  not found or inaccessible. Otherwise, inaccessible
     *  <code>AuctionProcessorEnablers</code> may be omitted from the
     *  list and may present the elements in any order including
     *  returning a unique set.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  auctionProcessorEnablerIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByIds(org.osid.id.IdList auctionProcessorEnablerIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.MutableAuctionProcessorEnablerList ret = new net.okapia.osid.jamocha.bidding.rules.auctionprocessorenabler.MutableAuctionProcessorEnablerList();

        try (org.osid.id.IdList ids = auctionProcessorEnablerIds) {
            while (ids.hasNext()) {
                ret.addAuctionProcessorEnabler(getAuctionProcessorEnabler(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> corresponding
     *  to the given auction processor enabler genus <code>Type</code>
     *  which does not include auction processor enablers of types
     *  derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  auctionProcessorEnablerGenusType an auctionProcessorEnabler genus type 
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByGenusType(org.osid.type.Type auctionProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessorenabler.FederatingAuctionProcessorEnablerList ret = getAuctionProcessorEnablerList();

        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            ret.addAuctionProcessorEnablerList(session.getAuctionProcessorEnablersByGenusType(auctionProcessorEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> corresponding
     *  to the given auction processor enabler genus <code>Type</code>
     *  and include any additional auction processor enablers with
     *  genus types derived from the specified <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  auctionProcessorEnablerGenusType an auctionProcessorEnabler genus type 
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByParentGenusType(org.osid.type.Type auctionProcessorEnablerGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessorenabler.FederatingAuctionProcessorEnablerList ret = getAuctionProcessorEnablerList();

        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            ret.addAuctionProcessorEnablerList(session.getAuctionProcessorEnablersByParentGenusType(auctionProcessorEnablerGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> containing
     *  the given auction processor enabler record <code>Type</code>.
     * 
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  auctionProcessorEnablerRecordType an auctionProcessorEnabler record type 
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>auctionProcessorEnablerRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersByRecordType(org.osid.type.Type auctionProcessorEnablerRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessorenabler.FederatingAuctionProcessorEnablerList ret = getAuctionProcessorEnablerList();

        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            ret.addAuctionProcessorEnablerList(session.getAuctionProcessorEnablersByRecordType(auctionProcessorEnablerRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets an <code>AuctionProcessorEnablerList</code> effective
     *  during the entire given date range inclusive but not confined
     *  to the date range.
     *  
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session.
     *  
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  from start of date range 
     *  @param  to end of date range 
     *  @return the returned <code>AuctionProcessorEnabler</code> list 
     *  @throws org.osid.InvalidArgumentException <code>from</code>
     *          is greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>from</code> or
     *          <code>to</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */
      
    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersOnDate(org.osid.calendaring.DateTime from, 
                                                                                                org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessorenabler.FederatingAuctionProcessorEnablerList ret = getAuctionProcessorEnablerList();

        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            ret.addAuctionProcessorEnablerList(session.getAuctionProcessorEnablersOnDate(from, to));
        }

        ret.noMore();
        return (ret);
    }
        

    /**
     *  Gets an <code>AuctionProcessorEnablerList </code> which are
     *  effective for the entire given date range inclusive but not
     *  confined to the date range and evaluated against the given
     *  agent.
     *
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @param  agentId an agent Id
     *  @param  from a start date
     *  @param  to an end date
     *  @return the returned <code>AuctionProcessorEnabler</code> list
     *  @throws org.osid.InvalidArgumentException <code>from</code> is
     *          greater than <code>to</code>
     *  @throws org.osid.NullArgumentException <code>agent</code>,
     *          <code>from</code>, or <code>to</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablersOnDateWithAgent(org.osid.id.Id agentId,
                                                                                                         org.osid.calendaring.DateTime from,
                                                                                                         org.osid.calendaring.DateTime to)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessorenabler.FederatingAuctionProcessorEnablerList ret = getAuctionProcessorEnablerList();

        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            ret.addAuctionProcessorEnablerList(session.getAuctionProcessorEnablersOnDateWithAgent(agentId, from, to));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>AuctionProcessorEnablers</code>. 
     *
     *  In plenary mode, the returned list contains all known auction
     *  processor enablers or an error results. Otherwise, the
     *  returned list may contain only those auction processor
     *  enablers that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  In active mode, auction processor enablers are returned that
     *  are currently active. In any status mode, active and inactive
     *  auction processor enablers are returned.
     *
     *  @return a list of <code>AuctionProcessorEnablers</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.bidding.rules.AuctionProcessorEnablerList getAuctionProcessorEnablers()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessorenabler.FederatingAuctionProcessorEnablerList ret = getAuctionProcessorEnablerList();

        for (org.osid.bidding.rules.AuctionProcessorEnablerLookupSession session : getSessions()) {
            ret.addAuctionProcessorEnablerList(session.getAuctionProcessorEnablers());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessorenabler.FederatingAuctionProcessorEnablerList getAuctionProcessorEnablerList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessorenabler.ParallelAuctionProcessorEnablerList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.bidding.rules.auctionprocessorenabler.CompositeAuctionProcessorEnablerList());
        }
    }
}
