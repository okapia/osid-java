//
// AbstractImmutableAuction.java
//
//     Wraps a mutable Auction to hide modifiers.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.bidding.auction.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  Wraps a mutable <code>Auction</code> to hide modifiers. This
 *  wrapper provides an immutized Auction from the point of view
 *  external to the builder. Methods are passed through to the
 *  underlying auction whose state changes are visible.
 */

public abstract class AbstractImmutableAuction
    extends net.okapia.osid.jamocha.builder.spi.AbstractImmutableOsidGovernator
    implements org.osid.bidding.Auction {

    private final org.osid.bidding.Auction auction;


    /**
     *  Constructs a new <code>AbstractImmutableAuction</code>.
     *
     *  @param auction the auction to immutablize
     *  @throws org.osid.NullArgumentException <code>auction</code>
     *          is <code>null</code>
     */

    protected AbstractImmutableAuction(org.osid.bidding.Auction auction) {
        super(auction);
        this.auction = auction;
        return;
    }


    /**
     *  Tests if the current date is within the start end end dates
     *  inclusive.
     *
     *  @return <code> true </code> if this is effective, <code> false
     *          </code> otherwise
     */

    @OSID @Override
    public boolean isEffective() {
        return (this.auction.isEffective());
    }


    /**
     *  Gets the start date. 
     *
     *  @return the start date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getStartDate() {
        return (this.auction.getStartDate());
    }


    /**
     *  Gets the end date. 
     *
     *  @return the end date 
     */

    @OSID @Override
    public org.osid.calendaring.DateTime getEndDate() {
        return (this.auction.getEndDate());
    }


    /**
     *  Gets the currency type used in this auction. 
     *
     *  @return currency type 
     */

    @OSID @Override
    public org.osid.type.Type getCurrencyType() {
        return (this.auction.getCurrencyType());
    }


    /**
     *  Gets the minimum number of bidders required for this auction to 
     *  complete. 
     *
     *  @return the minimum number of bidders 
     */

    @OSID @Override
    public long getMinimumBidders() {
        return (this.auction.getMinimumBidders());
    }


    /**
     *  Tests if bids in this auction are visible. 
     *
     *  @return <code> true </code> if this auction is sealed, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean isSealed() {
        return (this.auction.isSealed());
    }


    /**
     *  Gets the seller <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSellerId() {
        return (this.auction.getSellerId());
    }


    /**
     *  Gets the seller. 
     *
     *  @return a resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getSeller()
        throws org.osid.OperationFailedException {

        return (this.auction.getSeller());
    }


    /**
     *  Gets the item <code> Id. </code> 
     *
     *  @return a resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getItemId() {
        return (this.auction.getItemId());
    }


    /**
     *  Gets the item. 
     *
     *  @return a resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getItem()
        throws org.osid.OperationFailedException {

        return (this.auction.getItem());
    }


    /**
     *  Gets the number of items up for bid. 
     *
     *  @return the initial number of items 
     */

    @OSID @Override
    public long getLotSize() {
        return (this.auction.getLotSize());
    }


    /**
     *  Gets the number of items remaining. 
     *
     *  @return the number of items remaining 
     */

    @OSID @Override
    public long getRemainingItems() {
        return (this.auction.getRemainingItems());
    }


    /**
     *  Tests if this auction has a maxmimum number of items that can be 
     *  bought. 
     *
     *  @return <code> true </code> if the auction has an item limit, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean limitsItems() {
        return (this.auction.limitsItems());
    }


    /**
     *  Gets the limit on the number of items that can be bought. 
     *
     *  @return the item limit 
     *  @throws org.osid.IllegalStateException <code> limitsItems() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public long getItemLimit() {
        return (this.auction.getItemLimit());
    }


    /**
     *  Gets the starting price per item. 
     *
     *  @return the starting price 
     */

    @OSID @Override
    public org.osid.financials.Currency getStartingPrice() {
        return (this.auction.getStartingPrice());
    }


    /**
     *  Gets the minimum increment amount for successive bids. 
     *
     *  @return the increment 
     */

    @OSID @Override
    public org.osid.financials.Currency getPriceIncrement() {
        return (this.auction.getPriceIncrement());
    }


    /**
     *  Tests if a buyout price is available. 
     *
     *  @return <code> true </code> if the auction has a reserve price, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasReservePrice() {
        return (this.auction.hasReservePrice());
    }


    /**
     *  Gets the reserve price. 
     *
     *  @return the reserve price 
     *  @throws org.osid.IllegalStateException <code> hasReservePrice() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getReservePrice() {
        return (this.auction.getReservePrice());
    }


    /**
     *  Tests if a buyout price is available. 
     *
     *  @return <code> true </code> if the item can be bought out, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasBuyoutPrice() {
        return (this.auction.hasBuyoutPrice());
    }


    /**
     *  Gets the buyout price. 
     *
     *  @return the buyout price 
     *  @throws org.osid.IllegalStateException <code> hasBuyoutPrice() </code> 
     *          is <code> false </code> 
     */

    @OSID @Override
    public org.osid.financials.Currency getBuyoutPrice() {
        return (this.auction.getBuyoutPrice());
    }


    /**
     *  Gets the auction record corresponding to the given <code> Auction 
     *  </code> record <code> Type. </code> This method ie used to retrieve an 
     *  object implementing the requested record. The <code> auctionRecordType 
     *  </code> may be the <code> Type </code> returned in <code> 
     *  getRecordTypes() </code> or any of its parents in a <code> Type 
     *  </code> hierarchy where <code> hasRecordType(auctionRecordType) 
     *  </code> is <code> true </code> . 
     *
     *  @param  auctionRecordType the type of auction record to retrieve 
     *  @return the auction record 
     *  @throws org.osid.NullArgumentException <code> auctionRecordType 
     *          </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code> 
     *          hasRecordType(auctionRecordType) </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.bidding.records.AuctionRecord getAuctionRecord(org.osid.type.Type auctionRecordType)
        throws org.osid.OperationFailedException {

        return (this.auction.getAuctionRecord(auctionRecordType));
    }
}

