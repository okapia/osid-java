//
// AbstractCanonicalUnitProcessorQueryInspector.java
//
//     A template for making a CanonicalUnitProcessorQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.offering.rules.canonicalunitprocessor.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for canonical unit processors.
 */

public abstract class AbstractCanonicalUnitProcessorQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidProcessorQueryInspector
    implements org.osid.offering.rules.CanonicalUnitProcessorQueryInspector {

    private final java.util.Collection<org.osid.offering.rules.records.CanonicalUnitProcessorQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the canonical unit <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCanonicalUnitIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the canonical unit query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CanonicalUnitQueryInspector[] getRuledCanonicalUnitTerms() {
        return (new org.osid.offering.CanonicalUnitQueryInspector[0]);
    }


    /**
     *  Gets the catalogue <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogueIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the catalogue query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.offering.CatalogueQueryInspector[] getCatalogueTerms() {
        return (new org.osid.offering.CatalogueQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given canonical unit processor query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a canonical unit processor implementing the requested record.
     *
     *  @param canonicalUnitProcessorRecordType a canonical unit processor record type
     *  @return the canonical unit processor query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>canonicalUnitProcessorRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(canonicalUnitProcessorRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.offering.rules.records.CanonicalUnitProcessorQueryInspectorRecord getCanonicalUnitProcessorQueryInspectorRecord(org.osid.type.Type canonicalUnitProcessorRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.offering.rules.records.CanonicalUnitProcessorQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(canonicalUnitProcessorRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(canonicalUnitProcessorRecordType + " is not supported");
    }


    /**
     *  Adds a record to this canonical unit processor query. 
     *
     *  @param canonicalUnitProcessorQueryInspectorRecord canonical unit processor query inspector
     *         record
     *  @param canonicalUnitProcessorRecordType canonicalUnitProcessor record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCanonicalUnitProcessorQueryInspectorRecord(org.osid.offering.rules.records.CanonicalUnitProcessorQueryInspectorRecord canonicalUnitProcessorQueryInspectorRecord, 
                                                   org.osid.type.Type canonicalUnitProcessorRecordType) {

        addRecordType(canonicalUnitProcessorRecordType);
        nullarg(canonicalUnitProcessorRecordType, "canonical unit processor record type");
        this.records.add(canonicalUnitProcessorQueryInspectorRecord);        
        return;
    }
}
