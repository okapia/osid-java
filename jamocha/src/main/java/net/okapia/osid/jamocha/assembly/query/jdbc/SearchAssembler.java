//
// SearchAssembler.java
//
//     An interface for assembling a search.
//
//
// Tom Coppeto
// OnTapSolutions
// 23 October 2008
//
//
// Copyright (c) 2008 Massachusetts Institute of Technology. All
// Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.jdbc;


/**
 *  A utility interface for assembling SQL search queries.
 */

public class SearchAssembler 
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractSearchAssembler
    implements net.okapia.osid.jamocha.assembly.query.SearchAssembler {


    /**
     *  Gets the resulting search string for limits.
     *
     *  @return the limit string, empty if no limits
     */

    public String getLimitString() {
        StringBuffer sb = new StringBuffer();
        long start = getStartLimit();
        long end   = getEndLimit();

        if (start >= 0) {
            sb.append(Long.toString(start));
            if (end >= start) {
                sb.append(',');
            }
        }

        if (end >= 0) {
            sb.append(Long.toString(end));
        }

        return (sb.toString());
    }
}
