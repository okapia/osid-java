//
// AbstractResourceVelocity.java
//
//     Defines a ResourceVelocity.
//
//
// Tom Coppeto
// Okapia
// 8 October 2012
//
//
// Copyright (c) 2012 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.mapping.path.resourcevelocity.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>ResourceVelocity</code>.
 */

public abstract class AbstractResourceVelocity
    extends net.okapia.osid.jamocha.spi.AbstractOsidCompendium
    implements org.osid.mapping.path.ResourceVelocity {

    private org.osid.resource.Resource resource;
    private org.osid.mapping.Speed speed;
    private org.osid.mapping.Coordinate coordinate;
    private org.osid.mapping.Heading heading;
    private org.osid.mapping.path.Path path;

    private final java.util.Collection<org.osid.mapping.path.records.ResourceVelocityRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the <code> Id </code> of the resource on the route. 
     *
     *  @return the resource <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getResourceId() {
        return (this.resource.getId());
    }


    /**
     *  Gets the resource on the route. 
     *
     *  @return the resource 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.Resource getResource()
        throws org.osid.OperationFailedException {
        
        return (this.resource);
    }


    /**
     *  Sets the resource.
     *
     *  @param resource the resource
     *  @throws org.osid.NullArgumentException <code>resource</code>
     *          is <code>null</code>
     */

    protected void setResource(org.osid.resource.Resource resource) {
        nullarg(resource, "resource");
        this.resource = resource;
        return;
    }


    /**
     *  Gets the current speed.
     *
     *  @return the current speed
     */

    @OSID @Override
    public org.osid.mapping.Speed getSpeed() {
        return (this.speed);
    }


    /**
     *  Sets the speed.
     *
     *  @param speed the speed
     *  @throws org.osid.NullArgumentException <code>speed</code>
     *          is <code>null</code>
     */

    protected void setSpeed(org.osid.mapping.Speed speed) {
        nullarg(speed, "speed");
        this.speed = speed;
        return;
    }


    /**
     *  Gets the current position.
     *
     *  @return the current coordinate
     */

    @OSID @Override
    public org.osid.mapping.Coordinate getPosition() {
        return (this.coordinate);
    }


    /**
     *  Sets the coordinate position.
     *
     *  @param coordinate the position
     *  @throws org.osid.NullArgumentException <code>coordinate</code>
     *          is <code>null</code>
     */

    protected void setPosition(org.osid.mapping.Coordinate coordinate) {
        nullarg(coordinate, "coordinate position");
        this.coordinate = coordinate;
        return;
    }


    /**
     *  Gets the current heading.
     *
     *  @return the current heading
     */

    @OSID @Override
    public org.osid.mapping.Heading getHeading() {
        return (this.heading);
    }


    /**
     *  Sets the heading.
     *
     *  @param heading the heading
     *  @throws org.osid.NullArgumentException <code>heading</code>
     *          is <code>null</code>
     */

    protected void setHeading(org.osid.mapping.Heading heading) {
        nullarg(heading, "heading");
        this.heading = heading;
        return;
    }


    /**
     *  Tests if the resource is on a designated path.
     *
     *  @return <code> true </code> if the resource is on a designated path,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean isOnPath() {
        if (this.path == null) {
            return (false);
        } else {
            return (true);
        }
    }


    /**
     *  Gets the <code> Path </code> <code> Id. </code>
     *
     *  @return the path <code> Id </code>
     *  @throws org.osid.IllegalStateException <code>isOnPath()</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.id.Id getPathId() {
        if (!isOnPath()) {
            throw new org.osid.IllegalStateException("isOnPath() is false");
        }

        return (this.path.getId());
    }


    /**
     *  Gets the <code> Path. </code>
     *
     *  @return the path
     *  @throws org.osid.IllegalStateException <code>isOnPath()</code>
     *          is <code>false</code>
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.mapping.path.Path getPath()
        throws org.osid.OperationFailedException {

        if (!isOnPath()) {
            throw new org.osid.IllegalStateException("isOnPath() is false");
        }

        return (this.path);
    }


    /**
     *  Sets the path.
     *
     *  @param path the path
     *  @throws org.osid.NullArgumentException <code>path</code>
     *          is <code>null</code>
     */

    protected void setPath(org.osid.mapping.path.Path path) {
        nullarg(path, "path");
        this.path = path;
        return;
    }



    /**
     *  Tests if this resource velocity supports the given record
     *  <code>Type</code>.
     *
     *  @param  resourceVelocityRecordType a resource velocity record type 
     *  @return <code>true</code> if the resourceVelocityRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>resourceVelocityRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type resourceVelocityRecordType) {
        for (org.osid.mapping.path.records.ResourceVelocityRecord record : this.records) {
            if (record.implementsRecordType(resourceVelocityRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>ResourceVelocity</code> record <code>Type</code>.
     *
     *  @param resourceVelocityRecordType the resource velocity record
     *         type
     *  @return the resource velocity record 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceVelocityRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(resourceVelocityRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.mapping.path.records.ResourceVelocityRecord getResourceVelocityRecord(org.osid.type.Type resourceVelocityRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.mapping.path.records.ResourceVelocityRecord record : this.records) {
            if (record.implementsRecordType(resourceVelocityRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(resourceVelocityRecordType + " is not supported");
    }


    /**
     *  Adds a record to this resource velocity. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param resourceVelocityRecord the resource velocity record
     *  @param resourceVelocityRecordType resource velocity record type
     *  @throws org.osid.NullArgumentException
     *          <code>resourceVelocityRecord</code> or
     *          <code>resourceVelocityRecordTyperesourceVelocity</code> is
     *          <code>null</code>
     */
            
    protected void addResourceVelocityRecord(org.osid.mapping.path.records.ResourceVelocityRecord resourceVelocityRecord, 
                                             org.osid.type.Type resourceVelocityRecordType) {

        nullarg(resourceVelocityRecord, "resource velocity record");
        addRecordType(resourceVelocityRecordType);
        this.records.add(resourceVelocityRecord);
        
        return;
    }
}
