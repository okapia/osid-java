//
// ProcessingRecurringEventList.java
//
//     Runs a given list through a rules recurringEvent.
//
//
// Tom Coppeto
// Okapia
// 17 March 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.inline.processing.calendaring.recurringevent;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.primordium.type.URNType;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Runs a given list through a rules recurringEvent. The underlying rules
 *  recurringEvent must support the <code>Condition</code> record
 *  (<code>osid.rules.records.ConditionRecord:osid.calendaring.RecurringEvent@okapia.net</code>)
 *  and <code>Result</code>
 *  (<code>osid.rules.records.ResultRecord:osid.calendaring.RecurringEvent@okapia.net</code>)
 *  record for the input and output lists if no condition is
 *  specified.
 */

public final class ProcessingRecurringEventList
    extends net.okapia.osid.jamocha.adapter.federator.calendaring.recurringevent.CompositeRecurringEventList
    implements org.osid.calendaring.RecurringEventList {

    private static final org.osid.type.Type CONDITION_TYPE = URNType.valueOf("urn:inet:osid.org:types:records:rules:Condition:osid.calendaring.RecurringEvent");
    private static final org.osid.type.Type RESULT_TYPE    = URNType.valueOf("urn:inet:osid.org:types:records:rules:Result:osid.calendaring.RecurringEvent");


    /*
     *  Creates a new <code>ProcessingRecurringEventList</code>.
     *
     *  @param recurringEventList an <code>RecurringEventList</code> to filter
     *  @param rulesSession a <code>RulesSession</code> to use for
     *         evaluating rules
     *  @param ruleId the <code>Id</code> of a <code>Rule</code> to evaluate
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventList</code>, <code>rulesSession</code>,
     *          or <code>ruleId</code> is <code>null</code>
     *  @throws org.osid.NotFoundException <code>ruleId</code> not found
     *  @throws org.osid.OperationFailedException 
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    public ProcessingRecurringEventList(org.osid.calendaring.RecurringEventList recurringEventList, 
                                  org.osid.rules.RulesSession rulesSession,
                                  org.osid.id.Id ruleId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.rules.Condition condition = rulesSession.getConditionForRule(ruleId);
        if (!condition.hasRecordType(CONDITION_TYPE)) {
            throw new org.osid.OperationFailedException("record type not supported");
        }

        net.okapia.osid.records.rules.condition.calendaring.RecurringEventConditionRecord crecord = (net.okapia.osid.records.rules.condition.calendaring.RecurringEventConditionRecord) condition.getConditionRecord(CONDITION_TYPE);
        crecord.addRecurringEventList(recurringEventList);
        
        org.osid.rules.Result result = rulesSession.executeRule(ruleId, condition);
        if (!result.hasRecordType(CONDITION_TYPE)) {
            throw new org.osid.OperationFailedException("record type not supported");
        }

        net.okapia.osid.records.rules.result.calendaring.RecurringEventResultRecord rrecord = (net.okapia.osid.records.rules.result.calendaring.RecurringEventResultRecord) result.getResultRecord(RESULT_TYPE);
        addRecurringEventList(rrecord.getRecurringEvents());
        noMore();

        return;
    }


    /**
     *  Creates a new <code>ProcessingRecurringEventList</code>.
     *
     *  @param recurringEventList an <code>RecurringEventList</code> to filter
     *  @param rulesSession a <code>RulesSession</code> to use for
     *         evaluating rules
     *  @param ruleId the <code>Id</code> of a <code>Rule</code> to evaluate
     *  @param condition a condition to use for the rules evaluation
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventList</code>, <code>rulesSession</code>,
     *          or <code>rules</code> is <code>null</code>
     *  @throws org.osid.NotFoundException <code>ruleId</code> not found
     *  @throws org.osid.OperationFailedException 
     *  @throws org.osid.PermissionDeniedException authorization failure
     */

    public ProcessingRecurringEventList(org.osid.calendaring.RecurringEventList recurringEventList, 
                                  org.osid.rules.RulesSession rulesSession,
                                  org.osid.id.Id ruleId, org.osid.rules.Condition condition)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.rules.Result result = rulesSession.executeRule(ruleId, condition);
        if (!result.hasRecordType(RESULT_TYPE)) {
            throw new org.osid.OperationFailedException("record type not supported");
        }

        net.okapia.osid.records.rules.result.calendaring.RecurringEventResultRecord rrecord = (net.okapia.osid.records.rules.result.calendaring.RecurringEventResultRecord) result.getResultRecord(RESULT_TYPE);
        addRecurringEventList(rrecord.getRecurringEvents());
        noMore();

        return;
    }
}

