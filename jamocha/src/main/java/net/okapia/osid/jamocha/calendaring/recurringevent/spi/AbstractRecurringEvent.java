//
// AbstractRecurringEvent.java
//
//     Defines a RecurringEvent.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.recurringevent.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>RecurringEvent</code>.
 */

public abstract class AbstractRecurringEvent
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.calendaring.RecurringEvent {

    private boolean hasSponsors = false;

    private final java.util.Collection<org.osid.calendaring.Schedule> schedules = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.SupersedingEvent> supersedingEvents = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.MeetingTime> specificMeetingTimes = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.Event> events = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.calendaring.DateTimeInterval> blackouts = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.resource.Resource> sponsors = new java.util.LinkedHashSet<>();

    private final java.util.Collection<org.osid.calendaring.records.RecurringEventRecord> records = new java.util.LinkedHashSet<>();

    private final Containable containable = new Containable();


    /**
     *  Tests if this <code> Containable </code> is sequestered in
     *  that it should not appear outside of its aggregated
     *  composition.
     *
     *  @return <code> true </code> if this containable is
     *          sequestered, <code> false </code> if this containable
     *          may appear outside its aggregate
     */

    @OSID @Override
    public boolean isSequestered() {
        return (this.containable.isSequestered());
    }


    /**
     *  Sets the sequestered flag.
     *
     *  @param sequestered <code> true </code> if this containable is
     *         sequestered, <code> false </code> if this containable
     *         may appear outside its aggregate
     */

    protected void setSequestered(boolean sequestered) {
        this.containable.setSequestered(sequestered);
        return;
    }


    /**
     *  Gets the <code> Schedule Id </code> of this composite event. 
     *
     *  @return the recurring schedule <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getScheduleIds() {
        try {
            org.osid.calendaring.ScheduleList schedules = getSchedules();
            return (new net.okapia.osid.jamocha.adapter.converter.calendaring.schedule.ScheduleToIdList(schedules));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the recurring schedule in this composite event. 
     *
     *  @return the recurring schedules 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.ScheduleList getSchedules()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.calendaring.schedule.ArrayScheduleList(this.schedules));
    }


    /**
     *  Adds a schedule.
     *
     *  @param schedule a schedule
     *  @throws org.osid.NullArgumentException <code>schedule</code>
     *          is <code>null</code>
     */

    protected void addSchedule(org.osid.calendaring.Schedule schedule) {
        nullarg(schedule, "schedule");
        this.schedules.add(schedule);
        return;
    }


    /**
     *  Sets all the schedules.
     *
     *  @param schedules a collection of schedules
     *  @throws org.osid.NullArgumentException <code>schedules</code>
     *          is <code>null</code>
     */

    protected void setSchedules(java.util.Collection<org.osid.calendaring.Schedule> schedules) {
        nullarg(schedules, "schedules");

        this.schedules.clear();
        this.schedules.addAll(schedules);

        return;
    }


    /**
     *  Gets the superseding event <code> Ids. </code> A superseding
     *  event is one that replaces another in a series.
     *
     *  @return list of superseding event <code> Ids </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSupersedingEventIds() {
        try {
            org.osid.calendaring.SupersedingEventList supersedingEvents = getSupersedingEvents();
            return (new net.okapia.osid.jamocha.adapter.converter.calendaring.supersedingevent.SupersedingEventToIdList(supersedingEvents));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the superseding events. A superseding event is one that
     *  replaces another in a series.
     *
     *  @return list of superseding events 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.calendaring.SupersedingEventList getSupersedingEvents()
        throws org.osid.OperationFailedException {

        return (new net.okapia.osid.jamocha.calendaring.supersedingevent.ArraySupersedingEventList(this.supersedingEvents));
    }


    /**
     *  Adds a superseding event.
     *
     *  @param event a superseding event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    protected void addSupersedingEvent(org.osid.calendaring.SupersedingEvent event) {
        nullarg(event, "superseding event");
        this.supersedingEvents.add(event);
        return;
    }


    /**
     *  Sets all the superseding events.
     *
     *  @param events a collection of superseding events
     *  @throws org.osid.NullArgumentException <code>events</code> is
     *          <code>null</code>
     */

    protected void setSupersedingEvents(java.util.Collection<org.osid.calendaring.SupersedingEvent> events) {
        nullarg(events, "superseding events");

        this.supersedingEvents.clear();
        this.supersedingEvents.addAll(events);

        return;
    }


    /**
     *  Gets specific dates for this event outside of the schedules. Specific 
     *  dates are excluded from blackouts. 
     *
     *  @return speciifc list of dates 
     */

    @OSID @Override
    public org.osid.calendaring.MeetingTimeList getSpecificMeetingTimes() {
        return (new net.okapia.osid.jamocha.calendaring.meetingtime.ArrayMeetingTimeList(this.specificMeetingTimes));
    }


    /**
     *  Adds a specific meeting time.
     *
     *  @param time a specific meeting time
     *  @throws org.osid.NullArgumentException <code>time</code> is
     *          <code>null</code>
     */

    protected void addSpecificMeetingTime(org.osid.calendaring.MeetingTime time) {
        nullarg(time, "meeting time");
        this.specificMeetingTimes.add(time);
        return;
    }


    /**
     *  Sets all the specific meeting times.
     *
     *  @param times a collection of specific meeting times
     *  @throws org.osid.NullArgumentException <code>times</code> is
     *          <code>null</code>
     */

    protected void setSpecificMeetingTimes(java.util.Collection<org.osid.calendaring.MeetingTime> times) {
        nullarg(times, "meeting times");

        this.specificMeetingTimes.clear();
        this.specificMeetingTimes.addAll(times);

        return;
    }


    /**
     *  Gets the composed event <code> Ids. </code>
     *
     *  @return list of event <code> Ids </code>
     */

    @OSID @Override
    public org.osid.id.IdList getEventIds() {
        try {
            org.osid.calendaring.EventList events = getEvents();
            return (new net.okapia.osid.jamocha.adapter.converter.calendaring.event.EventToIdList(events));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }        
    }


    /**
     *  Gets the composed events.
     *
     *  @return list of events
     *  @throws org.osid.OperationFailedException unable to complete request
     */

    @OSID @Override
    public org.osid.calendaring.EventList getEvents()
        throws org.osid.OperationFailedException {
        
        return (new net.okapia.osid.jamocha.calendaring.event.ArrayEventList(this.events));
    }


    /**
     *  Adds a event.
     *
     *  @param event a event
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    protected void addEvent(org.osid.calendaring.Event event) {
        nullarg(event, "event");

        this.events.add(event);
        return;
    }


    /**
     *  Sets all the events.
     *
     *  @param events a collection of events
     *  @throws org.osid.NullArgumentException <code>events</code>
     *          is <code>null</code>
     */

    protected void setEvents(java.util.Collection<org.osid.calendaring.Event> events) {
        nullarg(events, "events");

        this.events.clear();
        this.events.addAll(events);

        return;
    }

    
    /**
     *  Gets the blackout dates for this recurring event. Recurring events 
     *  overlapping with the time intervals do not appear in their series. 
     *
     *  @return recurring event exceptions 
     */

    @OSID @Override
    public org.osid.calendaring.DateTimeIntervalList getBlackouts() {
        return (new net.okapia.osid.jamocha.calendaring.datetimeinterval.ArrayDateTimeIntervalList(this.blackouts));
    }


    /**
     *  Adds a blackout.
     *
     *  @param blackout a blackout
     *  @throws org.osid.NullArgumentException <code>blackout</code>
     *          is <code>null</code>
     */

    protected void addBlackout(org.osid.calendaring.DateTimeInterval blackout) {
        nullarg(blackout, "blackout");
        this.blackouts.add(blackout);
        return;
    }


    /**
     *  Sets all the blackouts.
     *
     *  @param blackouts a collection of blackouts
     *  @throws org.osid.NullArgumentException
     *          <code>blackouts</code> is <code>null</code>
     */

    protected void setBlackouts(java.util.Collection<org.osid.calendaring.DateTimeInterval> blackouts) {
        nullarg(blackouts, "blackouts");

        this.blackouts.clear();
        this.blackouts.addAll(blackouts);

        return;
    }


    /**
     *  Tests if a sponsor is associated with the overall recurring event. 
     *
     *  @return <code> true </code> if there is an associated sponsor. <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean hasSponsors() {
        return (this.hasSponsors);
    }


    /**
     *  Gets the <code> Id </code> of the recurring event sponsors. 
     *
     *  @return the sponsor <code> Ids </code> 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.id.IdList getSponsorIds() {
        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        try {
            org.osid.resource.ResourceList sponsors = getSponsors();
            return (new net.okapia.osid.jamocha.adapter.converter.resource.resource.ResourceToIdList(sponsors));
        } catch (org.osid.OperationFailedException ofe) {
            return (new net.okapia.osid.jamocha.id.id.ErrorIdList(ofe));
        }
    }


    /**
     *  Gets the <code> Sponsors. </code> 
     *
     *  @return the sponsors 
     *  @throws org.osid.IllegalStateException <code> hasSponsors() </code> is 
     *          <code> false </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.resource.ResourceList getSponsors()
        throws org.osid.OperationFailedException {

        if (!hasSponsors()) {
            throw new org.osid.IllegalStateException("hasSponsors() is false");
        }

        return (new net.okapia.osid.jamocha.resource.resource.ArrayResourceList(this.sponsors));
    }


    /**
     *  Adds a sponsor.
     *
     *  @param sponsor a sponsor
     *  @throws org.osid.NullArgumentException <code>sponsor</code> is
     *          <code>null</code>
     */

    protected void addSponsor(org.osid.resource.Resource sponsor) {
        nullarg(sponsor, "sponsor");

        this.sponsors.add(sponsor);
        this.hasSponsors = true;
        return;
    }


    /**
     *  Sets all the sponsors.
     *
     *  @param sponsors a collection of sponsors
     *  @throws org.osid.NullArgumentException <code>sponsors</code>
     *          is <code>null</code>
     */

    protected void setSponsors(java.util.Collection<org.osid.resource.Resource> sponsors) {
        nullarg(sponsors, "sponsors");

        this.sponsors.clear();
        this.sponsors.addAll(sponsors);
        this.hasSponsors = true;

        return;
    }


    /**
     *  Tests if this recurring event supports the given record
     *  <code>Type</code>.
     *
     *  @param  recurringEventRecordType a recurring event record type 
     *  @return <code>true</code> if the recurringEventRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type recurringEventRecordType) {
        for (org.osid.calendaring.records.RecurringEventRecord record : this.records) {
            if (record.implementsRecordType(recurringEventRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>RecurringEvent</code> record <code>Type</code>.
     *
     *  @param  recurringEventRecordType the recurring event record type 
     *  @return the recurring event record 
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(recurringEventRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.calendaring.records.RecurringEventRecord getRecurringEventRecord(org.osid.type.Type recurringEventRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.calendaring.records.RecurringEventRecord record : this.records) {
            if (record.implementsRecordType(recurringEventRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(recurringEventRecordType + " is not supported");
    }


    /**
     *  Adds a record to this recurring event. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param recurringEventRecord the recurring event record
     *  @param recurringEventRecordType recurring event record type
     *  @throws org.osid.NullArgumentException
     *          <code>recurringEventRecord</code> or
     *          <code>recurringEventRecordType</code> is
     *          <code>null</code>
     */
            
    protected void addRecurringEventRecord(org.osid.calendaring.records.RecurringEventRecord recurringEventRecord, 
                                           org.osid.type.Type recurringEventRecordType) {
        
        nullarg(recurringEventRecord, "recurring event record");
        addRecordType(recurringEventRecordType);
        this.records.add(recurringEventRecord);
        
        return;
    }


    protected class Containable
        extends net.okapia.osid.jamocha.spi.AbstractContainable
        implements org.osid.Containable {


        /**
         *  Sets the sequestered flag.
         *
         *  @param sequestered <code> true </code> if this containable is
         *         sequestered, <code> false </code> if this containable
         *         may appear outside its aggregate
         */

        @Override
        protected void setSequestered(boolean sequestered) {
            super.setSequestered(sequestered);
            return;
        }
    }
}
