//
// MutableIndexedMapPersonLookupSession
//
//    Implements a Person lookup service backed by a collection of
//    persons indexed by their types that can be modified after
//    instantiation.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.personnel;


/**
 *  Implements a Person lookup service backed by a collection of
 *  persons. The persons are indexed by {@code Id}, genus
 *  and record types.</p>
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some persons may be compatible
 *  with more types than are indicated through these person
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 *
 *  The collection of persons can be modified by another provider
 *  thread after handing the session off to a consumer.
 */

public final class MutableIndexedMapPersonLookupSession
    extends net.okapia.osid.jamocha.core.personnel.spi.AbstractIndexedMapPersonLookupSession
    implements org.osid.personnel.PersonLookupSession {


    /**
     *  Constructs a new {@code
     *  MutableIndexedMapPersonLookupSession} with no persons.
     *
     *  @param realm the realm
     *  @throws org.osid.NullArgumentException {@code realm}
     *          is {@code null}
     */

      public MutableIndexedMapPersonLookupSession(org.osid.personnel.Realm realm) {
        setRealm(realm);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPersonLookupSession} with a
     *  single person.
     *  
     *  @param realm the realm
     *  @param  person a single person
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code person} is {@code null}
     */

    public MutableIndexedMapPersonLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.personnel.Person person) {
        this(realm);
        putPerson(person);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPersonLookupSession} using an
     *  array of persons.
     *
     *  @param realm the realm
     *  @param  persons an array of persons
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code persons} is {@code null}
     */

    public MutableIndexedMapPersonLookupSession(org.osid.personnel.Realm realm,
                                                  org.osid.personnel.Person[] persons) {
        this(realm);
        putPersons(persons);
        return;
    }


    /**
     *  Constructs a new
     *  {@code MutableIndexedMapPersonLookupSession} using a
     *  collection of persons.
     *
     *  @param realm the realm
     *  @param  persons a collection of persons
     *  @throws org.osid.NullArgumentException {@code realm} or
     *          {@code persons} is {@code null}
     */

    public MutableIndexedMapPersonLookupSession(org.osid.personnel.Realm realm,
                                                  java.util.Collection<? extends org.osid.personnel.Person> persons) {

        this(realm);
        putPersons(persons);
        return;
    }
    

    /**
     *  Makes a {@code Person} available in this session.
     *
     *  @param  person a person
     *  @throws org.osid.NullArgumentException {@code person{@code  is
     *          {@code null}
     */

    @Override
    public void putPerson(org.osid.personnel.Person person) {
        super.putPerson(person);
        return;
    }


    /**
     *  Makes an array of persons available in this session.
     *
     *  @param  persons an array of persons
     *  @throws org.osid.NullArgumentException {@code persons{@code 
     *          is {@code null}
     */

    @Override
    public void putPersons(org.osid.personnel.Person[] persons) {
        super.putPersons(persons);
        return;
    }


    /**
     *  Makes collection of persons available in this session.
     *
     *  @param  persons a collection of persons
     *  @throws org.osid.NullArgumentException {@code person{@code  is
     *          {@code null}
     */

    @Override
    public void putPersons(java.util.Collection<? extends org.osid.personnel.Person> persons) {
        super.putPersons(persons);
        return;
    }


    /**
     *  Removes a Person from this session.
     *
     *  @param personId the {@code Id} of the person
     *  @throws org.osid.NullArgumentException {@code personId{@code  is
     *          {@code null}
     */

    @Override
    public void removePerson(org.osid.id.Id personId) {
        super.removePerson(personId);
        return;
    }    
}
