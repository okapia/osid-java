//
// AbstractFederatingFoundryLookupSession.java
//
//     An abstract federating adapter for a FoundryLookupSession.
//
//
// Tom Coppeto
// Okapia
// 30 October 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.federator.resourcing.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  An abstract federating adapter for a
 *  FoundryLookupSession. Sessions are added to this session through
 *  <code>addSession()</code>.
 *
 *  Two modes are available. If <code>selectAll()</code> is set, then
 *  the results for retrievals across all the registered sessions are
 *  returned. If <code>selectFirst()</code> is set, then only the
 *  results from the first session to have any results are returned.
 *
 *  In either mode, single returns always use the result from the
 *  first session with a result available.  Federated transactions are
 *  supoported with the selectAll mode.
 *
 *  Control and view methods are passed through to all sessions. The
 *  federating adapter always uses a comparative view.
 *
 *  If any of the underlying sessions are authenticated, the adapter
 *  is also authenticated unless a SessionProxy has been specified. If
 *  no <code>SessionProxy</code> has been specified, the agents from
 *  all underlying sessions are returned in the <code>Agent</code>
 *  list. The default locale types are used for the adapter unless
 *  either a <codeSessionProxy</code> or <code>setLocale()</code> is
 *  supplied.
 */

public abstract class AbstractFederatingFoundryLookupSession
    extends net.okapia.osid.jamocha.adapter.federator.spi.AbstractFederatingOsidSession<org.osid.resourcing.FoundryLookupSession>
    implements org.osid.resourcing.FoundryLookupSession {

    private boolean parallel = false;


    /**
     *  Constructs a new <code>AbstractFederatingFoundryLookupSession</code>.
     */

    protected AbstractFederatingFoundryLookupSession() {
        return;
    }

     
    /**
     *  Adds a session to this federation.
     *
     *  @param session a session to add
     *  @throws org.osid.NullArgumentException <code>session</code> is
     *          <code>null</code>
     */

    protected void addSession(org.osid.resourcing.FoundryLookupSession session) {
        super.addSession(session);
        return;
    }


    /**
     *  Configures this session for parallel or serial federation.
     *
     *  @param parallel <code>true</code> to return results randomly
     *         across all sessions, <code>false</code> to return
     *         results serially in order of the sessions
     */

    protected void setParallel(boolean parallel) {
        this.parallel = false;
        return;
    }


    /**
     *  Tests if this user can perform <code>Foundry</code> 
     *  lookups.
     *
     *  @return <code>true</code> if any session returns
     *          <code>true</code>
     */

    @OSID @Override
    public boolean canLookupFoundries() {
        for (org.osid.resourcing.FoundryLookupSession session : getSessions()) {
            if (session.canLookupFoundries()) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  A complete view of the <code>Foundry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void useComparativeFoundryView() {
        for (org.osid.resourcing.FoundryLookupSession session : getSessions()) {
            session.useComparativeFoundryView();
        }

        return;
    }


    /**
     *  A complete view of the <code>Foundry</code> returns is
     *  desired.  Methods will return what is requested or result in
     *  an error. This view is used when greater precision is desired
     *  at the expense of interoperability.
     */

    @OSID @Override
    public void usePlenaryFoundryView() {
        for (org.osid.resourcing.FoundryLookupSession session : getSessions()) {
            session.usePlenaryFoundryView();
        }

        return;
    }

     
    /**
     *  Gets the <code>Foundry</code> specified by its
     *  <code>Id</code>.
     *
     *  In plenary mode, the exact <code>Id</code> is found or a
     *  <code>NOT_FOUND</code> results. Otherwise, the returned
     *  <code>Foundry</code> may have a different <code>Id</code>
     *  than requested, such as the case where a duplicate
     *  <code>Id</code> was assigned to a <code>Foundry</code> and
     *  retained for compatibility.
     *
     *  @param  foundryId <code>Id</code> of the
     *          <code>Foundry</code>
     *  @return the foundry
     *  @throws org.osid.NotFoundException <code>foundryId</code> not
     *          found
     *  @throws org.osid.NullArgumentException <code>foundryId</code>
     *          is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.Foundry getFoundry(org.osid.id.Id foundryId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        for (org.osid.resourcing.FoundryLookupSession session : getSessions()) {
            try {
                return (session.getFoundry(foundryId));
            } catch (org.osid.NotFoundException nfe) {
                continue;
            }
        }

        throw new org.osid.NotFoundException(foundryId + " not found");
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  <code>IdList</code>. 
     *
     *  In plenary mode, the returned list contains all of the
     *  foundries specified in the <code>Id</code> list, in the order
     *  of the list, including duplicates, or an error results if an
     *  <code>Id</code> in the supplied list is not found or
     *  inaccessible. Otherwise, inaccessible <code>Foundries</code>
     *  may be omitted from the list and may present the elements in
     *  any order including returning a unique set.
     *
     *  @param  foundryIds the list of <code>Ids</code> to retrieve 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NotFoundException an <code>Id</code> was not
     *          found
     *  @throws org.osid.NullArgumentException
     *          <code>foundryIds</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByIds(org.osid.id.IdList foundryIds)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.resourcing.foundry.MutableFoundryList ret = new net.okapia.osid.jamocha.resourcing.foundry.MutableFoundryList();

        try (org.osid.id.IdList ids = foundryIds) {
            while (ids.hasNext()) {
                ret.addFoundry(getFoundry(ids.getNextId()));
            }
        }

        ret.eol();
        return (ret);
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  foundry genus <code>Type</code> which does not include
     *  foundries of types derived from the specified
     *  <code>Type</code>.  
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  foundryGenusType a foundry genus type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByGenusType(org.osid.type.Type foundryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.foundry.FederatingFoundryList ret = getFoundryList();

        for (org.osid.resourcing.FoundryLookupSession session : getSessions()) {
            ret.addFoundryList(session.getFoundriesByGenusType(foundryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FoundryList</code> corresponding to the given
     *  foundry genus <code>Type</code> and include any additional
     *  foundries with genus types derived from the specified
     *  <code>Type</code>.
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  foundryGenusType a foundry genus type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryGenusType</code> is <code></code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByParentGenusType(org.osid.type.Type foundryGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.foundry.FederatingFoundryList ret = getFoundryList();

        for (org.osid.resourcing.FoundryLookupSession session : getSessions()) {
            ret.addFoundryList(session.getFoundriesByParentGenusType(foundryGenusType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FoundryList</code> containing the given
     *  foundry record <code>Type</code>. 
     * 
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @param  foundryRecordType a foundry record type 
     *  @return the returned <code>Foundry</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>foundryRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByRecordType(org.osid.type.Type foundryRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.foundry.FederatingFoundryList ret = getFoundryList();

        for (org.osid.resourcing.FoundryLookupSession session : getSessions()) {
            ret.addFoundryList(session.getFoundriesByRecordType(foundryRecordType));
            if (!useAllResults() && ret.hasNext()) {  
                break;
            }
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets a <code>FoundryList</code> from the given provider. 
     *  
     *  In plenary mode, the returned list contains all known foundries or an 
     *  error results. Otherwise, the returned list may contain only those 
     *  foundries that are accessible through this session. 
     *
     *  @param  resourceId a resource <code>Id</code> 
     *  @return the returned <code>Foundry</code> list 
     *  @throws org.osid.NullArgumentException
     *          <code>resourceId</code> is <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundriesByProvider(org.osid.id.Id resourceId)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        net.okapia.osid.jamocha.adapter.federator.resourcing.foundry.FederatingFoundryList ret = getFoundryList();

        for (org.osid.resourcing.FoundryLookupSession session : getSessions()) {
            ret.addFoundryList(session.getFoundriesByProvider(resourceId));
        }

        ret.noMore();
        return (ret);
    }


    /**
     *  Gets all <code>Foundries</code>. 
     *
     *  In plenary mode, the returned list contains all known
     *  foundries or an error results. Otherwise, the returned list
     *  may contain only those foundries that are accessible through
     *  this session. In both cases, the order of the set is not
     *  specified.
     *
     *  @return a list of <code>Foundries</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.resourcing.FoundryList getFoundries()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        net.okapia.osid.jamocha.adapter.federator.resourcing.foundry.FederatingFoundryList ret = getFoundryList();

        for (org.osid.resourcing.FoundryLookupSession session : getSessions()) {
            ret.addFoundryList(session.getFoundries());
        }

        ret.noMore();
        return (ret);
    }


    protected net.okapia.osid.jamocha.adapter.federator.resourcing.foundry.FederatingFoundryList getFoundryList() {
        if (this.parallel) {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.foundry.ParallelFoundryList());
        } else {
            return (new net.okapia.osid.jamocha.adapter.federator.resourcing.foundry.CompositeFoundryList());
        }
    }
}
