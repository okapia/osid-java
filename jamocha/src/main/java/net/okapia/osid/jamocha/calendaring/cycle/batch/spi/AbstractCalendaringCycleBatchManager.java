//
// AbstractCalendaringCycleBatchManager.java
//
//     Supplies basic information in common throughout the managers
//     and profiles.
//
//
// Tom Coppeto
// Okapia
// 22 May 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.cycle.batch.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.Types;
import net.okapia.osid.torrefacto.collect.TypeRefSet;


/**
 *  Supplies basic information in common throughout the managers and
 *  profiles.
 */

public abstract class AbstractCalendaringCycleBatchManager
    extends net.okapia.osid.jamocha.spi.AbstractOsidManager
    implements org.osid.calendaring.cycle.batch.CalendaringCycleBatchManager,
               org.osid.calendaring.cycle.batch.CalendaringCycleBatchProxyManager {


    /**
     *  Constructs a new
     *  <code>AbstractCalendaringCycleBatchManager</code>.
     *
     *  @param provider the provider
     *  @throws org.osid.NullArgumentException <code>provider</code>
     *          is <code>null</code>
     */

    protected AbstractCalendaringCycleBatchManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is
     *          supported, <code> false </code> otherwise
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (false);
    }


    /**
     *  Tests if bulk administration of cyclic events is available. 
     *
     *  @return <code> true </code> if a cyclic event bulk administrative 
     *          service is available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventBatchAdmin() {
        return (false);
    }


    /**
     *  Tests if bulk administration of cyclic time periods is available. 
     *
     *  @return <code> true </code> if a cyclic time period bulk 
     *          administrative service is available, <code> false </code> 
     *          otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicTimePeriodBatchAdmin() {
        return (false);
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  event administration service. 
     *
     *  @return a <code> CyclicEventBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicEventBatchAdminSession getCyclicEventBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.batch.CalendaringCycleBatchManager.getCyclicEventBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  event administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicEventBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicEventBatchAdminSession getCyclicEventBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.batch.CalendaringCycleBatchProxyManager.getCyclicEventBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  event administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CyclicEventBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicEventBatchAdminSession getCyclicEventBatchAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.batch.CalendaringCycleBatchManager.getCyclicEventBatchAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  event administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicEventBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicEventBatchAdminSession getCyclicEventBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                    org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.batch.CalendaringCycleBatchProxyManager.getCyclicEventBatchAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  time period administration service. 
     *
     *  @return a <code> CyclicTimePeriodBatchAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchAdminSession getCyclicTimePeriodBatchAdminSession()
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.batch.CalendaringCycleBatchManager.getCyclicTimePeriodBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  time period administration service. 
     *
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodBatchAdminSession </code> 
     *  @throws org.osid.NullArgumentException <code> proxy </code> is <code> 
     *          null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodBatchAdmin() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchAdminSession getCyclicTimePeriodBatchAdminSession(org.osid.proxy.Proxy proxy)
        throws org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.batch.CalendaringCycleBatchProxyManager.getCyclicTimePeriodBatchAdminSession not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  time period administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @return a <code> CyclicTimePeriodBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchAdminSession getCyclicTimePeriodBatchAdminSessionForCalendar(org.osid.id.Id calendarId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.batch.CalendaringCycleBatchManager.getCyclicTimePeriodBatchAdminSessionForCalendar not implemented");
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the bulk cyclic 
     *  time period administration service for the given calendar. 
     *
     *  @param  calendarId the <code> Id </code> of the <code> Calendar 
     *          </code> 
     *  @param  proxy a proxy 
     *  @return a <code> CyclicTimePeriodBatchAdminSession </code> 
     *  @throws org.osid.NotFoundException no <code> Calendar </code> found by 
     *          the given <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> calendarId </code> or 
     *          <code> proxy </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicTimePeriodBatchAdmin() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.batch.CyclicTimePeriodBatchAdminSession getCyclicTimePeriodBatchAdminSessionForCalendar(org.osid.id.Id calendarId, 
                                                                                                                              org.osid.proxy.Proxy proxy)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        throw new org.osid.UnimplementedException("org.osid.calendaring.cycle.batch.CalendaringCycleBatchProxyManager.getCyclicTimePeriodBatchAdminSessionForCalendar not implemented");
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        super.close();
        return;
    }
}
