//
// AbstractHoldEnablerSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.hold.rules.holdenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A template for implementing a search results.
 */

public abstract class AbstractHoldEnablerSearchResults
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearchResults
    implements org.osid.hold.rules.HoldEnablerSearchResults {

    private org.osid.hold.rules.HoldEnablerList holdEnablers;
    private final org.osid.hold.rules.HoldEnablerQueryInspector inspector;
    private final java.util.Collection<org.osid.hold.rules.records.HoldEnablerSearchResultsRecord> records = new java.util.ArrayList<>();


    /**
     *  Constructs a new <code>AbstractHoldEnablerSearchResults.
     *
     *  @param holdEnablers the result set
     *  @param holdEnablerQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>holdEnablers</code>
     *          or <code>holdEnablerQueryInspector</code> is
     *          <code>null</code>
     */

    protected AbstractHoldEnablerSearchResults(org.osid.hold.rules.HoldEnablerList holdEnablers,
                                            org.osid.hold.rules.HoldEnablerQueryInspector holdEnablerQueryInspector) {
        nullarg(holdEnablers, "hold enablers");
        nullarg(holdEnablerQueryInspector, "hold enabler query inspectpr");

        this.holdEnablers = holdEnablers;
        this.inspector = holdEnablerQueryInspector;

        return;
    }


    /**
     *  Gets the hold enabler list resulting from a search.
     *
     *  @return a hold enabler list 
     *  @throws org.osid.IllegalStateException list already retrieved
     */

    @OSID @Override
    public org.osid.hold.rules.HoldEnablerList getHoldEnablers() {
        if (this.holdEnablers == null) {
            throw new org.osid.IllegalStateException("list already retrieved");
        }

        org.osid.hold.rules.HoldEnablerList holdEnablers = this.holdEnablers;
        this.holdEnablers = null;
	return (holdEnablers);
    }


    /**
     *  Gets the inspector for the query to examine the terms used in
     *  the search.
     *
     *  @return the query inspector 
     */

    public org.osid.hold.rules.HoldEnablerQueryInspector getHoldEnablerQueryInspector() {
        return (this.inspector);
    }


    /**
     *  Gets the search results record corresponding to the given
     *  hold enabler search record <code> Type. </code> This method must
     *  be used to retrieve a holdEnabler implementing the requested
     *  record.
     *
     *  @param holdEnablerSearchRecordType a holdEnabler search 
     *         record type 
     *  @return the hold enabler search
     *  @throws org.osid.NullArgumentException
     *          <code>holdEnablerSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException <code>
     *          hasRecordType(holdEnablerSearchRecordType) </code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.hold.rules.records.HoldEnablerSearchResultsRecord getHoldEnablerSearchResultsRecord(org.osid.type.Type holdEnablerSearchRecordType)
        throws org.osid.OperationFailedException {
	
	for (org.osid.hold.rules.records.HoldEnablerSearchResultsRecord record : this.records) {
            if (record.implementsRecordType(holdEnablerSearchRecordType)) {
                return (record);
            }
        }

        throw new org.osid.UnsupportedException(holdEnablerSearchRecordType + " is not supported");
    }


    /**
     *  Adds a search results record.
     *
     *  @param record hold enabler search results record
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */

    protected void addHoldEnablerRecord(org.osid.hold.rules.records.HoldEnablerSearchResultsRecord record, org.osid.type.Type recordType) {

        nullarg(record, "hold enabler record");
	addRecordType(recordType);
        this.records.add(record);

	return;
    }
}
