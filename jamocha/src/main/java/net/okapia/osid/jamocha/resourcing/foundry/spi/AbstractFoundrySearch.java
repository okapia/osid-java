//
// AbstractFoundrySearch.java
//
//     A template for making a Foundry Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resourcing.foundry.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing foundry searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractFoundrySearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resourcing.FoundrySearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resourcing.records.FoundrySearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resourcing.FoundrySearchOrder foundrySearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of foundries. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  foundryIds list of foundries
     *  @throws org.osid.NullArgumentException
     *          <code>foundryIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongFoundries(org.osid.id.IdList foundryIds) {
        while (foundryIds.hasNext()) {
            try {
                this.ids.add(foundryIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongFoundries</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of foundry Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getFoundryIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  foundrySearchOrder foundry search order 
     *  @throws org.osid.NullArgumentException
     *          <code>foundrySearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>foundrySearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderFoundryResults(org.osid.resourcing.FoundrySearchOrder foundrySearchOrder) {
	this.foundrySearchOrder = foundrySearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resourcing.FoundrySearchOrder getFoundrySearchOrder() {
	return (this.foundrySearchOrder);
    }


    /**
     *  Gets the record corresponding to the given foundry search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a foundry implementing the requested record.
     *
     *  @param foundrySearchRecordType a foundry search record
     *         type
     *  @return the foundry search record
     *  @throws org.osid.NullArgumentException
     *          <code>foundrySearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(foundrySearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resourcing.records.FoundrySearchRecord getFoundrySearchRecord(org.osid.type.Type foundrySearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resourcing.records.FoundrySearchRecord record : this.records) {
            if (record.implementsRecordType(foundrySearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(foundrySearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this foundry search. 
     *
     *  @param foundrySearchRecord foundry search record
     *  @param foundrySearchRecordType foundry search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addFoundrySearchRecord(org.osid.resourcing.records.FoundrySearchRecord foundrySearchRecord, 
                                           org.osid.type.Type foundrySearchRecordType) {

        addRecordType(foundrySearchRecordType);
        this.records.add(foundrySearchRecord);        
        return;
    }
}
