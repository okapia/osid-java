//
// AbstractGradeQueryInspector.java
//
//     A template for making a GradeQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.grade.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for grades.
 */

public abstract class AbstractGradeQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidObjectQueryInspector
    implements org.osid.grading.GradeQueryInspector {

    private final java.util.Collection<org.osid.grading.records.GradeQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the grade system <code> Id </code> terms. 
     *
     *  @return the grade system <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeSystemIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade system terms. 
     *
     *  @return the grade system terms 
     */

    @OSID @Override
    public org.osid.grading.GradeSystemQueryInspector[] getGradeSystemTerms() {
        return (new org.osid.grading.GradeSystemQueryInspector[0]);
    }


    /**
     *  Gets the input score start range terms. 
     *
     *  @return the input score start range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getInputScoreStartRangeTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the input score end range terms. 
     *
     *  @return the input score end range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getInputScoreEndRangeTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the input score terms. 
     *
     *  @return the input score range terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getInputScoreTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the output score terms. 
     *
     *  @return the output score terms 
     */

    @OSID @Override
    public org.osid.search.terms.DecimalRangeTerm[] getOutputScoreTerms() {
        return (new org.osid.search.terms.DecimalRangeTerm[0]);
    }


    /**
     *  Gets the grade entry <code> Id </code> terms. 
     *
     *  @return the grade entry <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradeEntryIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the grade entry terms. 
     *
     *  @return the grade entry terms 
     */

    @OSID @Override
    public org.osid.grading.GradeEntryQueryInspector[] getGradeEntryTerms() {
        return (new org.osid.grading.GradeEntryQueryInspector[0]);
    }


    /**
     *  Gets the gradebook <code> Id </code> terms. 
     *
     *  @return the gradebook <code> Id </code> terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getGradebookIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the gradebook terms. 
     *
     *  @return the gradebook terms 
     */

    @OSID @Override
    public org.osid.grading.GradebookQueryInspector[] getGradebookTerms() {
        return (new org.osid.grading.GradebookQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given grade query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a grade implementing the requested record.
     *
     *  @param gradeRecordType a grade record type
     *  @return the grade query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>gradeRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.records.GradeQueryInspectorRecord getGradeQueryInspectorRecord(org.osid.type.Type gradeRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.records.GradeQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(gradeRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade query. 
     *
     *  @param gradeQueryInspectorRecord grade query inspector
     *         record
     *  @param gradeRecordType grade record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addGradeQueryInspectorRecord(org.osid.grading.records.GradeQueryInspectorRecord gradeQueryInspectorRecord, 
                                                   org.osid.type.Type gradeRecordType) {

        addRecordType(gradeRecordType);
        nullarg(gradeRecordType, "grade record type");
        this.records.add(gradeQueryInspectorRecord);        
        return;
    }
}
