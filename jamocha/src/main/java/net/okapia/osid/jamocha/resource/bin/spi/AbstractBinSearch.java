//
// AbstractBinSearch.java
//
//     A template for making a Bin Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.resource.bin.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing bin searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractBinSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.resource.BinSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.resource.records.BinSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.resource.BinSearchOrder binSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of bins. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  binIds list of bins
     *  @throws org.osid.NullArgumentException
     *          <code>binIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongBins(org.osid.id.IdList binIds) {
        while (binIds.hasNext()) {
            try {
                this.ids.add(binIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongBins</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of bin Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getBinIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  binSearchOrder bin search order 
     *  @throws org.osid.NullArgumentException
     *          <code>binSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>binSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderBinResults(org.osid.resource.BinSearchOrder binSearchOrder) {
	this.binSearchOrder = binSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.resource.BinSearchOrder getBinSearchOrder() {
	return (this.binSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given bin search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a bin implementing the requested record.
     *
     *  @param binSearchRecordType a bin search record
     *         type
     *  @return the bin search record
     *  @throws org.osid.NullArgumentException
     *          <code>binSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(binSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.resource.records.BinSearchRecord getBinSearchRecord(org.osid.type.Type binSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.resource.records.BinSearchRecord record : this.records) {
            if (record.implementsRecordType(binSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(binSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this bin search. 
     *
     *  @param binSearchRecord bin search record
     *  @param binSearchRecordType bin search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addBinSearchRecord(org.osid.resource.records.BinSearchRecord binSearchRecord, 
                                           org.osid.type.Type binSearchRecordType) {

        addRecordType(binSearchRecordType);
        this.records.add(binSearchRecord);        
        return;
    }
}
