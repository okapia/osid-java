//
// AbstractGradeSystemTransform.java
//
//     Defines a GradeSystemTransform.
//
//
// Tom Coppeto
// OnTapSolutions
// 8 October 2008
//
//
// Copyright (c) 2008,2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.grading.transform.gradesystemtransform.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  Defines a <code>GradeSystemTransform</code>.
 */

public abstract class AbstractGradeSystemTransform
    extends net.okapia.osid.jamocha.spi.AbstractOsidRule
    implements org.osid.grading.transform.GradeSystemTransform {

    private org.osid.grading.GradeSystem sourceGradeSystem;
    private org.osid.grading.GradeSystem targetGradeSystem;
    private boolean normalizesInputScores = false;

    private final java.util.Collection<org.osid.grading.transform.GradeMap> gradeMaps = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.grading.transform.records.GradeSystemTransformRecord> records = new java.util.LinkedHashSet<>();


    /**
     *  Gets the source <code> GradeSystem Id. </code> 
     *
     *  @return the source grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getSourceGradeSystemId() {
        return (this.sourceGradeSystem.getId());
    }


    /**
     *  Gets the source <code> GradeSystem. </code> 
     *
     *  @return the source grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getSourceGradeSystem()
        throws org.osid.OperationFailedException {

        return (this.sourceGradeSystem);
    }


    /**
     *  Sets the source grade system.
     *
     *  @param gradeSystem a source grade system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    protected void setSourceGradeSystem(org.osid.grading.GradeSystem gradeSystem) {
        nullarg(gradeSystem, "source grade system");
        this.sourceGradeSystem = gradeSystem;
        return;
    }


    /**
     *  Gets the target <code> GradeSystem Id. </code> 
     *
     *  @return the target grade system <code> Id </code> 
     */

    @OSID @Override
    public org.osid.id.Id getTargetGradeSystemId() {
        return (this.targetGradeSystem.getId());
    }


    /**
     *  Gets the target <code> GradeSystem. </code> 
     *
     *  @return the target grade system 
     *  @throws org.osid.OperationFailedException unable to complete request 
     */

    @OSID @Override
    public org.osid.grading.GradeSystem getTargetGradeSystem()
        throws org.osid.OperationFailedException {

        return (this.targetGradeSystem);
    }


    /**
     *  Sets the target grade system.
     *
     *  @param gradeSystem a target grade system
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystem</code> is <code>null</code>
     */

    protected void setTargetGradeSystem(org.osid.grading.GradeSystem gradeSystem) {
        nullarg(gradeSystem, "target grade system");
        this.targetGradeSystem = gradeSystem;
        return;
    }


    /**
     *  Tests if this transformation is based on normalization of the input 
     *  scores. 
     *
     *  @return <code> true </code> of a normalization is performed,
     *          <code> false </code> otherwise
     */

    @OSID @Override
    public boolean normalizesInputScores() {
        return (this.normalizesInputScores);
    }


    /**
     *  Sets the normalizes input scores flag.
     *
     *  @param normalizes <code> true </code> of a normalization is
     *         performed, <code> false </code> otherwise
     */

    protected void setNormalizesInputScores(boolean normalizes) {
        this.normalizesInputScores = normalizes;
        return;
    }


    /**
     *  Gets any one-to-one mapping of grades. 
     *
     *  @return the grade map 
     */

    @OSID @Override
    public org.osid.grading.transform.GradeMapList getGradeMaps() {
        return (new net.okapia.osid.jamocha.grading.transform.grademap.ArrayGradeMapList(this.gradeMaps));
    }


    /**
     *  Adds a grade map.
     *
     *  @param gradeMap a grade map
     *  @throws org.osid.NullArgumentException <code>gradeMap</code>
     *          is <code>null</code>
     */

    protected void addGradeMap(org.osid.grading.transform.GradeMap gradeMap) {
        nullarg(gradeMap, "grade map");
        this.gradeMaps.add(gradeMap);
        return;
    }


    /**
     *  Sets all the grade maps.
     *
     *  @param gradeMaps a collection of grade maps
     *  @throws org.osid.NullArgumentException
     *          <code>gradeMaps</code> is <code>null</code>
     */

    protected void setGradeMaps(java.util.Collection<org.osid.grading.transform.GradeMap> gradeMaps) {
        nullarg(gradeMaps, "grade maps");

        this.gradeMaps.clear();
        this.gradeMaps.addAll(gradeMaps);

        return;
    }


    /**
     *  Tests if this gradeSystemTransform supports the given record
     *  <code>Type</code>.
     *
     *  @param  gradeSystemTransformRecordType a grade system transform record type 
     *  @return <code>true</code> if the gradeSystemTransformRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type gradeSystemTransformRecordType) {
        for (org.osid.grading.transform.records.GradeSystemTransformRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemTransformRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>GradeSystemTransform</code> record <code>Type</code>.
     *
     *  @param  gradeSystemTransformRecordType the grade system transform record type 
     *  @return the grade system transform record 
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformRecordType</code> is 
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete
     *          request
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(gradeSystemTransformRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.grading.transform.records.GradeSystemTransformRecord getGradeSystemTransformRecord(org.osid.type.Type gradeSystemTransformRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.grading.transform.records.GradeSystemTransformRecord record : this.records) {
            if (record.implementsRecordType(gradeSystemTransformRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(gradeSystemTransformRecordType + " is not supported");
    }


    /**
     *  Adds a record to this grade system transform. 
     *
     *  This method registers both the record and its type. The type
     *  is only used to fulfill the
     *  <code>getRecordTypes</code>. Additional types may be
     *  registered with this object using
     *  <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param gradeSystemTransformRecord the grade system transform record
     *  @param gradeSystemTransformRecordType grade system transform record type
     *  @throws org.osid.NullArgumentException
     *          <code>gradeSystemTransformRecord</code> or
     *          <code>gradeSystemTransformRecordTypegradeSystemTransform</code> is
     *          <code>null</code>
     */
            
    protected void addGradeSystemTransformRecord(org.osid.grading.transform.records.GradeSystemTransformRecord gradeSystemTransformRecord, 
                                                 org.osid.type.Type gradeSystemTransformRecordType) {
        
        nullarg(gradeSystemTransformRecord, "grade system transform record");
        addRecordType(gradeSystemTransformRecordType);
        this.records.add(gradeSystemTransformRecord);
        
        return;
    }
}
