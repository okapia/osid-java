//
// AbstractMapObjectiveBankLookupSession
//
//    A simple framework for providing an ObjectiveBank lookup service
//    backed by a fixed collection of objective banks.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.learning.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of an ObjectiveBank lookup service backed by a
 *  fixed collection of objective banks. The objective banks are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ObjectiveBanks</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapObjectiveBankLookupSession
    extends net.okapia.osid.jamocha.learning.spi.AbstractObjectiveBankLookupSession
    implements org.osid.learning.ObjectiveBankLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.learning.ObjectiveBank> objectiveBanks = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.learning.ObjectiveBank>());


    /**
     *  Makes an <code>ObjectiveBank</code> available in this session.
     *
     *  @param  objectiveBank an objective bank
     *  @throws org.osid.NullArgumentException <code>objectiveBank<code>
     *          is <code>null</code>
     */

    protected void putObjectiveBank(org.osid.learning.ObjectiveBank objectiveBank) {
        this.objectiveBanks.put(objectiveBank.getId(), objectiveBank);
        return;
    }


    /**
     *  Makes an array of objective banks available in this session.
     *
     *  @param  objectiveBanks an array of objective banks
     *  @throws org.osid.NullArgumentException <code>objectiveBanks<code>
     *          is <code>null</code>
     */

    protected void putObjectiveBanks(org.osid.learning.ObjectiveBank[] objectiveBanks) {
        putObjectiveBanks(java.util.Arrays.asList(objectiveBanks));
        return;
    }


    /**
     *  Makes a collection of objective banks available in this session.
     *
     *  @param  objectiveBanks a collection of objective banks
     *  @throws org.osid.NullArgumentException <code>objectiveBanks<code>
     *          is <code>null</code>
     */

    protected void putObjectiveBanks(java.util.Collection<? extends org.osid.learning.ObjectiveBank> objectiveBanks) {
        for (org.osid.learning.ObjectiveBank objectiveBank : objectiveBanks) {
            this.objectiveBanks.put(objectiveBank.getId(), objectiveBank);
        }

        return;
    }


    /**
     *  Removes an ObjectiveBank from this session.
     *
     *  @param  objectiveBankId the <code>Id</code> of the objective bank
     *  @throws org.osid.NullArgumentException <code>objectiveBankId<code> is
     *          <code>null</code>
     */

    protected void removeObjectiveBank(org.osid.id.Id objectiveBankId) {
        this.objectiveBanks.remove(objectiveBankId);
        return;
    }


    /**
     *  Gets the <code>ObjectiveBank</code> specified by its <code>Id</code>.
     *
     *  @param  objectiveBankId <code>Id</code> of the <code>ObjectiveBank</code>
     *  @return the objectiveBank
     *  @throws org.osid.NotFoundException <code>objectiveBankId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>objectiveBankId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBank getObjectiveBank(org.osid.id.Id objectiveBankId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.learning.ObjectiveBank objectiveBank = this.objectiveBanks.get(objectiveBankId);
        if (objectiveBank == null) {
            throw new org.osid.NotFoundException("objectiveBank not found: " + objectiveBankId);
        }

        return (objectiveBank);
    }


    /**
     *  Gets all <code>ObjectiveBanks</code>. In plenary mode, the returned
     *  list contains all known objectiveBanks or an error
     *  results. Otherwise, the returned list may contain only those
     *  objectiveBanks that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ObjectiveBanks</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.learning.ObjectiveBankList getObjectiveBanks()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.learning.objectivebank.ArrayObjectiveBankList(this.objectiveBanks.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.objectiveBanks.clear();
        super.close();
        return;
    }
}
