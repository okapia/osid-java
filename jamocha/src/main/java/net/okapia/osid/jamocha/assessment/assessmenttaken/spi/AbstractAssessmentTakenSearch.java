//
// AbstractAssessmentTakenSearch.java
//
//     A template for making an AssessmentTaken Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assessment.assessmenttaken.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing assessment taken searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractAssessmentTakenSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.assessment.AssessmentTakenSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.assessment.records.AssessmentTakenSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.assessment.AssessmentTakenSearchOrder assessmentTakenSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of assessments taken. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  assessmentTakenIds list of assessments taken
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongAssessmentsTaken(org.osid.id.IdList assessmentTakenIds) {
        while (assessmentTakenIds.hasNext()) {
            try {
                this.ids.add(assessmentTakenIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongAssessmentsTaken</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of assessment taken Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getAssessmentTakenIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  assessmentTakenSearchOrder assessment taken search order 
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>assessmentTakenSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderAssessmentTakenResults(org.osid.assessment.AssessmentTakenSearchOrder assessmentTakenSearchOrder) {
	this.assessmentTakenSearchOrder = assessmentTakenSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.assessment.AssessmentTakenSearchOrder getAssessmentTakenSearchOrder() {
	return (this.assessmentTakenSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given assessment taken search
     *  record <code> Type. </code> This method must be used to
     *  retrieve an assessment taken implementing the requested record.
     *
     *  @param assessmentTakenSearchRecordType an assessment taken search record
     *         type
     *  @return the assessment taken search record
     *  @throws org.osid.NullArgumentException
     *          <code>assessmentTakenSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(assessmentTakenSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.assessment.records.AssessmentTakenSearchRecord getAssessmentTakenSearchRecord(org.osid.type.Type assessmentTakenSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.assessment.records.AssessmentTakenSearchRecord record : this.records) {
            if (record.implementsRecordType(assessmentTakenSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(assessmentTakenSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this assessment taken search. 
     *
     *  @param assessmentTakenSearchRecord assessment taken search record
     *  @param assessmentTakenSearchRecordType assessmentTaken search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addAssessmentTakenSearchRecord(org.osid.assessment.records.AssessmentTakenSearchRecord assessmentTakenSearchRecord, 
                                           org.osid.type.Type assessmentTakenSearchRecordType) {

        addRecordType(assessmentTakenSearchRecordType);
        this.records.add(assessmentTakenSearchRecord);        
        return;
    }
}
