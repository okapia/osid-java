//
// AbstractMessagingManager.java
//
//     An adapter for a MessagingManager.
//
//
// Tom Coppeto
// Okapia
// 22 February 2014
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.messaging.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;


/**
 *  An adapter for a MessagingManager. The manager must be set for
 *  this adapter to function.
 */

public abstract class AbstractAdapterMessagingManager
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidManager<org.osid.messaging.MessagingManager>
    implements org.osid.messaging.MessagingManager {


    /**
     *  Constructs a new {@code AbstractAdapterMessagingManager} using
     *  the underlying provider.
     */

    protected AbstractAdapterMessagingManager() {
        return;
    }


    /**
     *  Constructs a new {@code AbstractAdapterMessagingManager}.
     *
     *  @param provider the service provider
     *  @throws org.osid.NullArgumentException {@code provider} is
     *          {@code null}
     */

    protected AbstractAdapterMessagingManager(net.okapia.osid.provider.ServiceProvider provider) {
        super(provider);
        return;
    }


    /**
     *  Tests if federation is visible. 
     *
     *  @return <code> true </code> if visible federation is supported <code> 
     *          , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsVisibleFederation() {
        return (getAdapteeManager().supportsVisibleFederation());
    }


    /**
     *  Tests if sending messages is supported. 
     *
     *  @return <code> true </code> if message sending is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessaging() {
        return (getAdapteeManager().supportsMessaging());
    }


    /**
     *  Tests if message lookup is supported. 
     *
     *  @return <code> true </code> if message lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageLookup() {
        return (getAdapteeManager().supportsMessageLookup());
    }


    /**
     *  Tests if querying messages is supported. 
     *
     *  @return <code> true </code> if message query is supported, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageQuery() {
        return (getAdapteeManager().supportsMessageQuery());
    }


    /**
     *  Tests if message search is supported. 
     *
     *  @return <code> true </code> if message search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageSearch() {
        return (getAdapteeManager().supportsMessageSearch());
    }


    /**
     *  Tests if creating, updating and deleting messages is supported. 
     *
     *  @return <code> true </code> if message administration is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageAdmin() {
        return (getAdapteeManager().supportsMessageAdmin());
    }


    /**
     *  Tests if message notification is supported. Messages may be sent when 
     *  messages are created, modified, or deleted. 
     *
     *  @return <code> true </code> if message notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageNotification() {
        return (getAdapteeManager().supportsMessageNotification());
    }


    /**
     *  Tests if retrieving mappings of message and mailboxes is supported. 
     *
     *  @return <code> true </code> if message mailbox mapping retrieval is 
     *          supported <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageMailbox() {
        return (getAdapteeManager().supportsMessageMailbox());
    }


    /**
     *  Tests if managing mappings of messages and mailboxes is supported. 
     *
     *  @return <code> true </code> if message mailbox assignment is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageMailboxAssignment() {
        return (getAdapteeManager().supportsMessageMailboxAssignment());
    }


    /**
     *  Tests if a messaging smart mailbox service is supported. 
     *
     *  @return <code> true </code> if a message smart mailbox service is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessageSmartMailbox() {
        return (getAdapteeManager().supportsMessageSmartMailbox());
    }


    /**
     *  Tests if receipt lookup is supported. 
     *
     *  @return <code> true </code> if receipt lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptLookup() {
        return (getAdapteeManager().supportsReceiptLookup());
    }


    /**
     *  Tests if updating receipts is supported. 
     *
     *  @return <code> true </code> if receipt administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptAdmin() {
        return (getAdapteeManager().supportsReceiptAdmin());
    }


    /**
     *  Tests if receipts notification is supported. Messages may be sent when 
     *  receipts are created, modified, or deleted. 
     *
     *  @return <code> true </code> if receipt notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsReceiptNotification() {
        return (getAdapteeManager().supportsReceiptNotification());
    }


    /**
     *  Tests if mailbox lookup is supported. 
     *
     *  @return <code> true </code> if mailbox lookup is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxLookup() {
        return (getAdapteeManager().supportsMailboxLookup());
    }


    /**
     *  Tests if mailbox search is supported. 
     *
     *  @return <code> true </code> if mailbox search is supported <code> , 
     *          </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxSearch() {
        return (getAdapteeManager().supportsMailboxSearch());
    }


    /**
     *  Tests if mailbox administration is supported. 
     *
     *  @return <code> true </code> if mailbox administration is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxAdmin() {
        return (getAdapteeManager().supportsMailboxAdmin());
    }


    /**
     *  Tests if mailbox notification is supported. Messages may be sent when 
     *  <code> Mailbox </code> objects are created, deleted or updated. 
     *  Notifications for messages within mailboxes are sent via the message 
     *  notification session. 
     *
     *  @return <code> true </code> if mailbox notification is supported 
     *          <code> , </code> <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxNotification() {
        return (getAdapteeManager().supportsMailboxNotification());
    }


    /**
     *  Tests if a mailbox hierarchy traversal is supported. 
     *
     *  @return <code> true </code> if a mailbox hierarchy traversal is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxHierarchy() {
        return (getAdapteeManager().supportsMailboxHierarchy());
    }


    /**
     *  Tests if a mailbox hierarchy design is supported. 
     *
     *  @return <code> true </code> if a mailbox hierarchy design is 
     *          supported, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMailboxHierarchyDesign() {
        return (getAdapteeManager().supportsMailboxHierarchyDesign());
    }


    /**
     *  Tests if a messaging batch service is supported. 
     *
     *  @return <code> true </code> if a messaging batch service is supported, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsMessagingBatch() {
        return (getAdapteeManager().supportsMessagingBatch());
    }


    /**
     *  Gets all the message record types supported. 
     *
     *  @return the list of supported message record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMessageRecordTypes() {
        return (getAdapteeManager().getMessageRecordTypes());
    }


    /**
     *  Tests if a given message record type is supported. 
     *
     *  @param  messageRecordType the message type 
     *  @return <code> true </code> if the message record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> messageRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMessageRecordType(org.osid.type.Type messageRecordType) {
        return (getAdapteeManager().supportsMessageRecordType(messageRecordType));
    }


    /**
     *  Gets all the message search record types supported. 
     *
     *  @return the list of supported message search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMessageSearchRecordTypes() {
        return (getAdapteeManager().getMessageSearchRecordTypes());
    }


    /**
     *  Tests if a given message search type is supported. 
     *
     *  @param  messageSearchRecordType the message search type 
     *  @return <code> true </code> if the message search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> messageSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMessageSearchRecordType(org.osid.type.Type messageSearchRecordType) {
        return (getAdapteeManager().supportsMessageSearchRecordType(messageSearchRecordType));
    }


    /**
     *  Gets all the receipt record types supported. 
     *
     *  @return the list of supported receipt record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getReceiptRecordTypes() {
        return (getAdapteeManager().getReceiptRecordTypes());
    }


    /**
     *  Tests if a given receipt record type is supported. 
     *
     *  @param  receiptRecordType the mesreceiptsage type 
     *  @return <code> true </code> if the receipt record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> receiptRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsReceiptRecordType(org.osid.type.Type receiptRecordType) {
        return (getAdapteeManager().supportsReceiptRecordType(receiptRecordType));
    }


    /**
     *  Gets all the mailbox record types supported. 
     *
     *  @return the list of supported mailbox record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMailboxRecordTypes() {
        return (getAdapteeManager().getMailboxRecordTypes());
    }


    /**
     *  Tests if a given mailbox record type is supported. 
     *
     *  @param  mailboxRecordType the mailbox record type 
     *  @return <code> true </code> if the mailbox record type is supported 
     *          <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mailboxRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMailboxRecordType(org.osid.type.Type mailboxRecordType) {
        return (getAdapteeManager().supportsMailboxRecordType(mailboxRecordType));
    }


    /**
     *  Gets all the mailbox search record types supported. 
     *
     *  @return the list of supported mailbox search record types 
     */

    @OSID @Override
    public org.osid.type.TypeList getMailboxSearchRecordTypes() {
        return (getAdapteeManager().getMailboxSearchRecordTypes());
    }


    /**
     *  Tests if a given mailbox search record type is supported. 
     *
     *  @param  mailboxSearchRecordType the mailbox search record type 
     *  @return <code> true </code> if the mailbox search record type is 
     *          supported <code> , </code> <code> false </code> otherwise 
     *  @throws org.osid.NullArgumentException <code> mailboxSearchRecordType 
     *          </code> is <code> null </code> 
     */

    @OSID @Override
    public boolean supportsMailboxSearchRecordType(org.osid.type.Type mailboxSearchRecordType) {
        return (getAdapteeManager().supportsMailboxSearchRecordType(mailboxSearchRecordType));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message 
     *  sending. 
     *
     *  @return <code> a MessagingSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessaging() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessagingSession getMessagingSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessagingSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message lookup 
     *  service. 
     *
     *  @return <code> a MessageLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageLookupSession getMessageLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message lookup 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a MessageLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsMessageLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageLookupSession getMessageLookupSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageLookupSessionForMailbox(mailboxId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message query 
     *  service. 
     *
     *  @return a <code> MessageQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMessageQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageQuerySession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the message query 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the <code> Mailbox </code> 
     *  @return a <code> MessageQuerySession </code> 
     *  @throws org.osid.NotFoundException no mailbox found by the given 
     *          <code> Id </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageQuery() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMessageQuerySessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageQuerySessionForMailbox(mailboxId));
    }


    /**
     *  Gets a message search session. 
     *
     *  @return <code> a MessageSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSearchSession getMessageSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageSearchSession());
    }


    /**
     *  Gets a message search session for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a MessageSearchSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsMessageSearch() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSearchSession getMessageSearchSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageSearchSessionForMailbox(mailboxId));
    }


    /**
     *  Gets a message administration session for creating, updating and 
     *  deleting messages. 
     *
     *  @return <code> a MessageAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageAdminSession getMessageAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageAdminSession());
    }


    /**
     *  Gets a message administration session for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a MessageAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMessageAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageAdminSession getMessageAdminSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageAdminSessionForMailbox(mailboxId));
    }


    /**
     *  Gets the notification session for notifications pertaining to message 
     *  changes. 
     *
     *  @param  messageReceiver the notification callback 
     *  @return <code> a MessageNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageNotificationSession getMessageNotificationSession(org.osid.messaging.MessageReceiver messageReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageNotificationSession(messageReceiver));
    }


    /**
     *  Gets the message notification session for the given mailbox. 
     *
     *  @param  messageReceiver the notification callback 
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a MessageNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> messageReceiver </code> 
     *          or <code> mailboxId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageNotificationSession getMessageNotificationSessionForMailbox(org.osid.messaging.MessageReceiver messageReceiver, 
                                                                                                 org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageNotificationSessionForMailbox(messageReceiver, mailboxId));
    }


    /**
     *  Gets the session for retrieving message to mailbox mappings. 
     *
     *  @return a <code> MessageMailboxSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageMailbox() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageMailboxSession getMessageMailboxSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageMailboxSession());
    }


    /**
     *  Gets the session for assigning message to mailbox mappings. 
     *
     *  @return a <code> MessageMailboxAssignmentSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageMailboxAssignment() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageMailboxAssignmentSession getMessageMailboxAssignmentSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageMailboxAssignmentSession());
    }


    /**
     *  Gets the <code> OsidSession </code> to manage locatin smart mailboxes. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return a <code> MessageSmartMailboxSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessageSmartMailbox() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageSmartMailboxSession getMessageSmartMailboxSession(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getMessageSmartMailboxSession(mailboxId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt lookup 
     *  service. 
     *
     *  @return <code> a ReceiptLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptLookupSession getReceiptLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptLookupSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt lookup 
     *  service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a ReceiptLookupSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptLookup() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptLookupSession getReceiptLookupSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptLookupSessionForMailbox(mailboxId));
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt 
     *  administrative service. 
     *
     *  @return <code> a ReceiptAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptAdminSession getReceiptAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptAdminSession());
    }


    /**
     *  Gets the <code> OsidSession </code> associated with the receipt 
     *  administrative service for the given mailbox. 
     *
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a ReceiptAdminSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> mailboxId </code> is 
     *          <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> supportsReceiptAdmin() 
     *          </code> or <code> supportsVisibleFederation() </code> is 
     *          <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptAdminSession getReceiptAdminSessionForMailbox(org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptAdminSessionForMailbox(mailboxId));
    }


    /**
     *  Gets the notification session for notifications pertaining to receipt 
     *  changes. 
     *
     *  @param  receiptReceiver the notification callback 
     *  @return <code> a ReceiptNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> receiptReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptNotificationSession getReceiptNotificationSession(org.osid.messaging.ReceiptReceiver receiptReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptNotificationSession(receiptReceiver));
    }


    /**
     *  Gets the receipt notification session for the given mailbox. 
     *
     *  @param  receiptReceiver the notification callback 
     *  @param  mailboxId the <code> Id </code> of the mailbox 
     *  @return <code> a ReceiptNotificationSession </code> 
     *  @throws org.osid.NotFoundException <code> mailboxId </code> not found 
     *  @throws org.osid.NullArgumentException <code> receiptReceiver </code> 
     *          or <code> mailboxId </code> is <code> null </code> 
     *  @throws org.osid.OperationFailedException <code> unable to complete 
     *          request </code> 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsReceiptNotification() </code> or <code> 
     *          supportsVisibleFederation() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.ReceiptNotificationSession getReceiptNotificationSessionForMailbox(org.osid.messaging.ReceiptReceiver receiptReceiver, 
                                                                                                 org.osid.id.Id mailboxId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException {

        return (getAdapteeManager().getReceiptNotificationSessionForMailbox(receiptReceiver, mailboxId));
    }


    /**
     *  Gets the mailbox lookup session. 
     *
     *  @return a <code> MailboxLookupSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxLookup() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxLookupSession getMailboxLookupSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxLookupSession());
    }


    /**
     *  Gets the mailbox query session. 
     *
     *  @return a <code> MailboxQuerySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MessageQuerySession getMailboxQuerySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxQuerySession());
    }


    /**
     *  Gets the mailbox search session. 
     *
     *  @return a <code> MailboxSearchSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxSearch() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxSearchSession getMailboxSearchSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxSearchSession());
    }


    /**
     *  Gets the mailbox administrative session for creating, updating and 
     *  deleteing mailboxes. 
     *
     *  @return a <code> MailboxAdminSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> supportsMailboxAdmin() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxAdminSession getMailboxAdminSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxAdminSession());
    }


    /**
     *  Gets the notification session for subscribing to changes to a mailbox. 
     *
     *  @param  mailboxReceiver the notification callback 
     *  @return a <code> MailboxNotificationSession </code> 
     *  @throws org.osid.NullArgumentException <code> mailboxReceiver </code> 
     *          is <code> null </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxNotification() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxNotificationSession getMailboxNotificationSession(org.osid.messaging.MailboxReceiver mailboxReceiver)
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxNotificationSession(mailboxReceiver));
    }


    /**
     *  Gets the mailbox hierarchy traversal session. 
     *
     *  @return <code> a MailboxHierarchySession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxHierarchy() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxHierarchySession getMailboxHierarchySession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxHierarchySession());
    }


    /**
     *  Gets the mailbox hierarchy design session. 
     *
     *  @return a <code> MailboxHierarchyDesignSession </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMailboxHierarchyDesign() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.messaging.MailboxHierarchyDesignSession getMailboxHierarchyDesignSession()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMailboxHierarchyDesignSession());
    }


    /**
     *  Gets a <code> MessagingBatchManager. </code> 
     *
     *  @return a <code> MessagingBatchManager </code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsMessagingBatch() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.messaging.batch.MessagingBatchManager getMessagingBatchManager()
        throws org.osid.OperationFailedException {

        return (getAdapteeManager().getMessagingBatchManager());
    }


    /**
     * Closes this manager.
     *
     * @throws org.osid.IllegalStateException this manager has been closed
     */

    @OSIDBinding @Override
    public void close() {
        getAdapteeManager().close();
	super.close();

        return;
    }
}
