//
// AbstractTodoProducer.java
//
//     Defines a TodoProducer builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.checklist.mason.todoproducer.spi;


/**
 *  Defines a <code>TodoProducer</code> builder.
 */

public abstract class AbstractTodoProducerBuilder<T extends AbstractTodoProducerBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidRuleBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.checklist.mason.todoproducer.TodoProducerMiter todoProducer;


    /**
     *  Constructs a new <code>AbstractTodoProducerBuilder</code>.
     *
     *  @param todoProducer the todo producer to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractTodoProducerBuilder(net.okapia.osid.jamocha.builder.checklist.mason.todoproducer.TodoProducerMiter todoProducer) {
        super(todoProducer);
        this.todoProducer = todoProducer;
        return;
    }


    /**
     *  Builds the todo producer.
     *
     *  @return the new todo producer
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.checklist.mason.TodoProducer build() {
        (new net.okapia.osid.jamocha.builder.validator.checklist.mason.todoproducer.TodoProducerValidator(getValidations())).validate(this.todoProducer);
        return (new net.okapia.osid.jamocha.builder.checklist.mason.todoproducer.ImmutableTodoProducer(this.todoProducer));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the todo producer miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.checklist.mason.todoproducer.TodoProducerMiter getMiter() {
        return (this.todoProducer);
    }


    /**
     *  Sets the creation rule.
     *
     *  @return the builder
     */

    public T creationRule() {
        getMiter().setCreationRule(true);
        return (self());
    }


    /**
     *  Sets the cyclic event.
     *
     *  @param event a cyclic event
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>event</code> is
     *          <code>null</code>
     */

    public T cyclicEvent(org.osid.calendaring.cycle.CyclicEvent event) {
        getMiter().setCyclicEvent(event);
        return (self());
    }


    /**
     *  Sets the stock level.
     *
     *  @param level a stock level
     *  @return the builder
     *  @throws org.osid.InvalidArgumentException <code>level</code>
     *          is negative
     */

    public T stockLevel(long level) {
        getMiter().setStockLevel(level);
        return (self());
    }


    /**
     *  Sets the stock.
     *
     *  @param stock a stock
     *  @return the builder
     *  @throws org.osid.NullArgumentException
     *          <code>stock</code> is <code>null</code>
     */

    public T stock(org.osid.inventory.Stock stock) {
        getMiter().setStock(stock);
        return (self());
    }


    /**
     *  Adds a TodoProducer record.
     *
     *  @param record a todo producer record
     *  @param recordType the type of todo producer record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.checklist.mason.records.TodoProducerRecord record, org.osid.type.Type recordType) {
        getMiter().addTodoProducerRecord(record, recordType);
        return (self());
    }
}       


