//
// DeedSearchResults.java
//
//     A basic search results container.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.room.squatting.deed;


/**
 *  A template for implementing a search results.
 */

public final class DeedSearchResults
    extends net.okapia.osid.jamocha.room.squatting.deed.spi.AbstractDeedSearchResults
    implements org.osid.room.squatting.DeedSearchResults {


    /**
     *  Constructs a new <code>DeedSearchResults.
     *
     *  @param deeds the result set
     *  @param deedQueryInspector the query inspector
     *  @throws org.osid.NullArgumentException <code>deeds</code>
     *          or <code>deedQueryInspector</code> is
     *          <code>null</code>
     */

    public DeedSearchResults(org.osid.room.squatting.DeedList deeds,
                                 org.osid.room.squatting.DeedQueryInspector deedQueryInspector) {
        super(deeds, deedQueryInspector);
        return;
    }
}
