//
// AbstractIndexedMapSceneLookupSession.java
//
//    A simple framework for providing a Scene lookup service
//    backed by a fixed collection of scenes with indexed types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.control.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.MultiMap;
import net.okapia.osid.torrefacto.collect.TypeMultiHashMap;
import net.okapia.osid.torrefacto.collect.SynchronizedMultiMap;


/**
 *  Simple implementation of a Scene lookup service backed by a
 *  fixed collection of scenes. The scenes are indexed by
 *  <code>Id</code>, genus and record types.
 *
 *  The type indices are created from <code>getGenusType()</code> and
 *  <code>getRecordTypes()</code>. Some scenes may be compatible
 *  with more types than are indicated through these scene
 *  methods. <code>addRecordType()</code>, <code>addGenusType()</code>
 *  can be used to supplement the index.
 *
 *  The backing HashMap is synchronized so that the collection of
 *  <code>Scenes</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractIndexedMapSceneLookupSession
    extends AbstractMapSceneLookupSession
    implements org.osid.control.SceneLookupSession {

    private final MultiMap<org.osid.type.Type, org.osid.control.Scene> scenesByGenus  = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Scene>());
    private final MultiMap<org.osid.type.Type, org.osid.control.Scene> scenesByRecord = new SynchronizedMultiMap<>(new TypeMultiHashMap<org.osid.control.Scene>());


    /**
     *  Makes a <code>Scene</code> available in this session.
     *
     *  @param  scene a scene
     *  @throws org.osid.NullArgumentException <code>scene<code> is
     *          <code>null</code>
     */

    @Override
    protected void putScene(org.osid.control.Scene scene) {
        super.putScene(scene);

        this.scenesByGenus.put(scene.getGenusType(), scene);
        
        try (org.osid.type.TypeList types = scene.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.scenesByRecord.put(types.getNextType(), scene);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }
            
        return;
    }

    
    /**
     *  Makes an array of scenes available in this session.
     *
     *  @param  scenes an array of scenes
     *  @throws org.osid.NullArgumentException <code>scenes<code>
     *          is <code>null</code>
     */

    @Override
    protected void putScenes(org.osid.control.Scene[] scenes) {
        for (org.osid.control.Scene scene : scenes) {
            putScene(scene);
        }

        return;
    }


    /**
     *  Makes a collection of scenes available in this session.
     *
     *  @param  scenes a collection of scenes
     *  @throws org.osid.NullArgumentException <code>scenes<code>
     *          is <code>null</code>
     */

    @Override
    protected void putScenes(java.util.Collection<? extends org.osid.control.Scene> scenes) {
        for (org.osid.control.Scene scene : scenes) {
            putScene(scene);
        }

        return;
    }


    /**
     *  Removes a scene from this session.
     *
     *  @param sceneId the <code>Id</code> of the scene
     *  @throws org.osid.NullArgumentException <code>sceneId</code>
     *          is <code>null</code>
     */

    @Override
    protected void removeScene(org.osid.id.Id sceneId) {
        org.osid.control.Scene scene;
        try {
            scene = getScene(sceneId);
        } catch (org.osid.OsidException e) {
            return;
        }

        this.scenesByGenus.remove(scene.getGenusType());

        try (org.osid.type.TypeList types = scene.getRecordTypes()) {
            while (types.hasNext()) {
                try {
                    this.scenesByRecord.remove(types.getNextType(), scene);
                } catch (org.osid.OperationFailedException oe) { /* error getting record type? */ }
            }
        }

        super.removeScene(sceneId);
        return;
    }


    /**
     *  Gets a <code>SceneList</code> corresponding to the given
     *  scene genus <code>Type</code> which does not include
     *  scenes of types derived from the specified
     *  <code>Type</code>.  In plenary mode, the returned list
     *  contains all known scenes or an error results. Otherwise,
     *  the returned list may contain only those scenes that are
     *  accessible through this session. In both cases, the order of
     *  the set is not specified.
     *
     *  @param  sceneGenusType a scene genus type 
     *  @return the returned <code>Scene</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sceneGenusType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByGenusType(org.osid.type.Type sceneGenusType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.scene.ArraySceneList(this.scenesByGenus.get(sceneGenusType)));
    }


    /**
     *  Gets a <code>SceneList</code> containing the given
     *  scene record <code>Type</code>. In plenary mode, the
     *  returned list contains all known scenes or an error
     *  results. Otherwise, the returned list may contain only those
     *  scenes that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @param  sceneRecordType a scene record type 
     *  @return the returned <code>scene</code> list
     *  @throws org.osid.NullArgumentException
     *          <code>sceneRecordType</code> is <code>null</code>
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.control.SceneList getScenesByRecordType(org.osid.type.Type sceneRecordType)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.control.scene.ArraySceneList(this.scenesByRecord.get(sceneRecordType)));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.scenesByGenus.clear();
        this.scenesByRecord.clear();

        super.close();

        return;
    }
}
