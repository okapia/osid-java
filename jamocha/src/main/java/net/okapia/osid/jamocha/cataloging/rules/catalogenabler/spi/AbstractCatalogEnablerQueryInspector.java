//
// AbstractCatalogEnablerQueryInspector.java
//
//     A template for making a CatalogEnablerQueryInspector.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.cataloging.rules.catalogenabler.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A query inspector template for catalog enablers.
 */

public abstract class AbstractCatalogEnablerQueryInspector
    extends net.okapia.osid.jamocha.spi.AbstractOsidEnablerQueryInspector
    implements org.osid.cataloging.rules.CatalogEnablerQueryInspector {

    private final java.util.Collection<org.osid.cataloging.rules.records.CatalogEnablerQueryInspectorRecord> records = new java.util.ArrayList<>();

    
    /**
     *  Gets the catalog <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getRuledCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the catalog query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQueryInspector[] getRuledCatalogTerms() {
        return (new org.osid.cataloging.CatalogQueryInspector[0]);
    }


    /**
     *  Gets the catalog <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCatalogIdTerms() {
        return (new org.osid.search.terms.IdTerm[0]);
    }


    /**
     *  Gets the catalog query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.cataloging.CatalogQueryInspector[] getCatalogTerms() {
        return (new org.osid.cataloging.CatalogQueryInspector[0]);
    }



    /**
     *  Gets the record corresponding to the given catalog enabler query
     *  inspector record <code> Type. </code> This method must be used
     *  to retrieve a catalog enabler implementing the requested record.
     *
     *  @param catalogEnablerRecordType a catalog enabler record type
     *  @return the catalog enabler query inspsector record
     *  @throws org.osid.NullArgumentException
     *          <code>catalogEnablerRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(catalogEnablerRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.cataloging.rules.records.CatalogEnablerQueryInspectorRecord getCatalogEnablerQueryInspectorRecord(org.osid.type.Type catalogEnablerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.cataloging.rules.records.CatalogEnablerQueryInspectorRecord record : this.records) {
            if (record.implementsRecordType(catalogEnablerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(catalogEnablerRecordType + " is not supported");
    }


    /**
     *  Adds a record to this catalog enabler query. 
     *
     *  @param catalogEnablerQueryInspectorRecord catalog enabler query inspector
     *         record
     *  @param catalogEnablerRecordType catalogEnabler record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addCatalogEnablerQueryInspectorRecord(org.osid.cataloging.rules.records.CatalogEnablerQueryInspectorRecord catalogEnablerQueryInspectorRecord, 
                                                   org.osid.type.Type catalogEnablerRecordType) {

        addRecordType(catalogEnablerRecordType);
        nullarg(catalogEnablerRecordType, "catalog enabler record type");
        this.records.add(catalogEnablerQueryInspectorRecord);        
        return;
    }
}
