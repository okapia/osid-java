//
// AbstractMapParameterProcessorLookupSession
//
//    A simple framework for providing a ParameterProcessor lookup service
//    backed by a fixed collection of parameter processors.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.configuration.rules.spi;

import org.osid.binding.java.annotation.OSID;
import org.osid.binding.java.annotation.OSIDBinding;

import net.okapia.osid.torrefacto.collect.IdHashMap;


/**
 *  Simple implementation of a ParameterProcessor lookup service backed by a
 *  fixed collection of parameter processors. The parameter processors are indexed only by
 *  <code>Id</code>. This class can be used for small collections or
 *  subclassed to provide additional indices for faster lookups.
 *  
 *  The backing HashMap is synchronized so that the collection of
 *  <code>ParameterProcessors</code> may be modified while accessed by a
 *  consumer.
 */

public abstract class AbstractMapParameterProcessorLookupSession
    extends net.okapia.osid.jamocha.configuration.rules.spi.AbstractParameterProcessorLookupSession
    implements org.osid.configuration.rules.ParameterProcessorLookupSession {

    private final java.util.Map<org.osid.id.Id, org.osid.configuration.rules.ParameterProcessor> parameterProcessors = java.util.Collections.synchronizedMap(new IdHashMap<org.osid.configuration.rules.ParameterProcessor>());


    /**
     *  Makes a <code>ParameterProcessor</code> available in this session.
     *
     *  @param  parameterProcessor a parameter processor
     *  @throws org.osid.NullArgumentException <code>parameterProcessor<code>
     *          is <code>null</code>
     */

    protected void putParameterProcessor(org.osid.configuration.rules.ParameterProcessor parameterProcessor) {
        this.parameterProcessors.put(parameterProcessor.getId(), parameterProcessor);
        return;
    }


    /**
     *  Makes an array of parameter processors available in this session.
     *
     *  @param  parameterProcessors an array of parameter processors
     *  @throws org.osid.NullArgumentException <code>parameterProcessors<code>
     *          is <code>null</code>
     */

    protected void putParameterProcessors(org.osid.configuration.rules.ParameterProcessor[] parameterProcessors) {
        putParameterProcessors(java.util.Arrays.asList(parameterProcessors));
        return;
    }


    /**
     *  Makes a collection of parameter processors available in this session.
     *
     *  @param  parameterProcessors a collection of parameter processors
     *  @throws org.osid.NullArgumentException <code>parameterProcessors<code>
     *          is <code>null</code>
     */

    protected void putParameterProcessors(java.util.Collection<? extends org.osid.configuration.rules.ParameterProcessor> parameterProcessors) {
        for (org.osid.configuration.rules.ParameterProcessor parameterProcessor : parameterProcessors) {
            this.parameterProcessors.put(parameterProcessor.getId(), parameterProcessor);
        }

        return;
    }


    /**
     *  Removes a ParameterProcessor from this session.
     *
     *  @param  parameterProcessorId the <code>Id</code> of the parameter processor
     *  @throws org.osid.NullArgumentException <code>parameterProcessorId<code> is
     *          <code>null</code>
     */

    protected void removeParameterProcessor(org.osid.id.Id parameterProcessorId) {
        this.parameterProcessors.remove(parameterProcessorId);
        return;
    }


    /**
     *  Gets the <code>ParameterProcessor</code> specified by its <code>Id</code>.
     *
     *  @param  parameterProcessorId <code>Id</code> of the <code>ParameterProcessor</code>
     *  @return the parameterProcessor
     *  @throws org.osid.NotFoundException <code>parameterProcessorId</code> not 
     *          found 
     *  @throws org.osid.NullArgumentException <code>parameterProcessorId</code> is 
     *          <code>null</code> 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessor getParameterProcessor(org.osid.id.Id parameterProcessorId)
        throws org.osid.NotFoundException,
               org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
        
        org.osid.configuration.rules.ParameterProcessor parameterProcessor = this.parameterProcessors.get(parameterProcessorId);
        if (parameterProcessor == null) {
            throw new org.osid.NotFoundException("parameterProcessor not found: " + parameterProcessorId);
        }

        return (parameterProcessor);
    }


    /**
     *  Gets all <code>ParameterProcessors</code>. In plenary mode, the returned
     *  list contains all known parameterProcessors or an error
     *  results. Otherwise, the returned list may contain only those
     *  parameterProcessors that are accessible through this session. In both
     *  cases, the order of the set is not specified.
     *
     *  @return a list of <code>ParameterProcessors</code> 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.configuration.rules.ParameterProcessorList getParameterProcessors()
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {

        return (new net.okapia.osid.jamocha.configuration.rules.parameterprocessor.ArrayParameterProcessorList(this.parameterProcessors.values()));
    }


    /**
     *  Closes this <code>osid.OsidSession</code>
     *
     *  @throws org.osid.IllegalStateException This session has been closed.
     */

    @OSIDBinding @Override
    public void close() {
        this.parameterProcessors.clear();
        super.close();
        return;
    }
}
