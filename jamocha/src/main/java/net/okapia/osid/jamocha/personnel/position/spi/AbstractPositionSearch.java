//
// AbstractPositionSearch.java
//
//     A template for making a Position Search.
//
//
// Tom Coppeto
// Okapia
// 22 June 2010
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.personnel.position.spi;

import org.osid.binding.java.annotation.OSID;


/**
 *  The search interface for governing position searches. The default
 *  methods store the given information for retrieval from a search
 *  session.
 */

public abstract class AbstractPositionSearch    
    extends net.okapia.osid.jamocha.spi.AbstractOsidSearch
    implements org.osid.personnel.PositionSearch {

    private final java.util.Collection<org.osid.id.Id> ids = new java.util.HashSet<>();
    private final java.util.Collection<org.osid.personnel.records.PositionSearchRecord> records = new java.util.ArrayList<>();
    private org.osid.personnel.PositionSearchOrder positionSearchOrder;
    private Throwable throwable;


    /**
     *  Execute this search among the given list of positions. This
     *  method stores the Id list for later retrieval. This method may
     *  be overridden to stream the Ids.
     *
     *  If an error occurs in retrieving the list of Ids, the error is
     *  stashed and stored 
     *
     *  @param  positionIds list of positions
     *  @throws org.osid.NullArgumentException
     *          <code>positionIds</code> is <code>null</code>
     */

    @OSID @Override
    public void searchAmongPositions(org.osid.id.IdList positionIds) {
        while (positionIds.hasNext()) {
            try {
                this.ids.add(positionIds.getNextId());
            } catch (org.osid.OperationFailedException oe) {
                this.throwable = oe;
            }
        }
        
	return;
    }


    /**
     *  Gets the exception which occurred from traversing the IdList
     *  in <code>searchAmongPositions</code>.
     *
     *  @return the error or <code>null</code> if none occurred
     */

    protected Throwable getError() {
        return (this.throwable);
    }


    /**
     *  Retrieves the list of position Ids stored.
     *
     *  @return list of Ids
     */

    protected java.util.Collection<org.osid.id.Id> getPositionIds() {
        return (java.util.Collections.unmodifiableCollection(this.ids));
    }


    /**
     *  Specify an ordering to the search results. The stored copy is
     *  overwritten with each call.
     *
     *  @param  positionSearchOrder position search order 
     *  @throws org.osid.NullArgumentException
     *          <code>positionSearchOrder</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>positionSearchOrder</code> is not of this
     *          service
     */

    @OSID @Override
    public void orderPositionResults(org.osid.personnel.PositionSearchOrder positionSearchOrder) {
	this.positionSearchOrder = positionSearchOrder;
	return;
    }


    /**
     *  Retrieves the search order specified.
     *
     *  @return the search order or <code>null</code> if none
     *          specified
     */

    protected org.osid.personnel.PositionSearchOrder getPositionSearchOrder() {
	return (this.positionSearchOrder);
    }


    /**
     *  Gets the record corresponding to the given position search
     *  record <code> Type. </code> This method must be used to
     *  retrieve a position implementing the requested record.
     *
     *  @param positionSearchRecordType a position search record
     *         type
     *  @return the position search record
     *  @throws org.osid.NullArgumentException
     *          <code>positionSearchRecordType</code> is
     *          <code>null</code>
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(positionSearchRecordType)</code>
     *          is <code>false</code>
     */

    @OSID @Override
    public org.osid.personnel.records.PositionSearchRecord getPositionSearchRecord(org.osid.type.Type positionSearchRecordType)
        throws org.osid.OperationFailedException {

	for (org.osid.personnel.records.PositionSearchRecord record : this.records) {
            if (record.implementsRecordType(positionSearchRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(positionSearchRecordType + " is not supported");
    }


    /**
     *  Adds a record to this position search. 
     *
     *  @param positionSearchRecord position search record
     *  @param positionSearchRecordType position search record type
     *  @throws org.osid.NullArgumentException <code>record</code>
     *          is <code>null</code>
     */
            
    protected void addPositionSearchRecord(org.osid.personnel.records.PositionSearchRecord positionSearchRecord, 
                                           org.osid.type.Type positionSearchRecordType) {

        addRecordType(positionSearchRecordType);
        this.records.add(positionSearchRecord);        
        return;
    }
}
