//
// AbstractQueryGradeEntryLookupSession.java
//
//    A GradeEntryQuerySession adapter.
//
//
// Tom Coppeto 
// Okapia 
// 15 February 2014
//
//
// Copyright (c) 2014 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.adapter.grading.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A GradeEntryQuerySession adapter.
 */

public abstract class AbstractAdapterGradeEntryQuerySession
    extends net.okapia.osid.jamocha.adapter.spi.AbstractAdapterOsidSession
    implements org.osid.grading.GradeEntryQuerySession {

    private final org.osid.grading.GradeEntryQuerySession session;
    

    /**
     *  Constructs a new AbstractAdapterGradeEntryQuerySession.
     *
     *  @param session the underlying grade entry query session
     *  @throws org.osid.NullArgumentException {@code session} is
     *          {@code null}
     */

    protected AbstractAdapterGradeEntryQuerySession(org.osid.grading.GradeEntryQuerySession session) {
        super(session);
        this.session = session;
        return;
    }


    /**
     *  Gets the {@codeGradebook</code> {@codeId</code> associated
     *  with this session.
     *
     *  @return the {@codeGradebook Id</code> associated with
     *          this session
     *  @throws org.osid.IllegalStateException this session has been
     *          closed
     */

    @OSID @Override
    public org.osid.id.Id getGradebookId() {
        return (this.session.getGradebookId());
    }


    /**
     *  Gets the {@codeGradebook</code> associated with this 
     *  session.
     *
     *  @return the {@codeGradebook</code> associated with 
     *          this session 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.IllegalStateException this session has been closed 
     */

    @OSID @Override
    public org.osid.grading.Gradebook getGradebook()
        throws org.osid.OperationFailedException,
        org.osid.PermissionDeniedException {
        
        return (this.session.getGradebook());
    }


    /**
     *  Tests if this user can perform {@codeGradeEntry</code> 
     *  searches.
     *
     *  @return {@codetrue</code>
     */

    @OSID @Override
    public boolean canSearchGradeEntries() {
        return (this.session.canSearchGradeEntries());
    }


    /**
     *  Federates the view for methods in this session. A federated
     *  view will include grade entries in gradebooks which are children
     *  of this gradebook in the gradebook hierarchy.
     */

    @OSID @Override
    public void useFederatedGradebookView() {
        this.session.useFederatedGradebookView();
        return;
    }


    /**
     *  Isolates the view for methods in this session. An isolated
     *  view restricts queries to this gradebook only.
     */
    
    @OSID @Override
    public void useIsolatedGradebookView() {
        this.session.useIsolatedGradebookView();
        return;
    }
    
      
    /**
     *  Gets a grade entry query. The returned query will not have an
     *  extension query.
     *
     *  @return the grade entry query 
     */
      
    @OSID @Override
    public org.osid.grading.GradeEntryQuery getGradeEntryQuery() {
        return (this.session.getGradeEntryQuery());
    }


    /**
     *  Gets a list of {@code Objects} matching the given resource 
     *  query. 
     *
     *  @param  gradeEntryQuery the grade entry query 
     *  @return the returned {@code [Obect]List} 
     *  @throws org.osid.NullArgumentException {@code gradeEntryQuery} is 
     *          {@code null} 
     *  @throws org.osid.OperationFailedException unable to complete request 
     *  @throws org.osid.PermissionDeniedException authorization failure 
     *  @throws org.osid.UnsupportedException {@code gradeEntryQuery} is
     *          not of this service
     */

    @OSID @Override
    public org.osid.grading.GradeEntryList getGradeEntriesByQuery(org.osid.grading.GradeEntryQuery gradeEntryQuery)
        throws org.osid.OperationFailedException,
               org.osid.PermissionDeniedException {
      
        return (this.session.getGradeEntriesByQuery(gradeEntryQuery));
    }
}
