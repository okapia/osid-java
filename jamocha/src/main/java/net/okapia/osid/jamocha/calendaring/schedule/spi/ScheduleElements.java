//
// ScheduleElements.java
//
//     Pre-generated Ids for form elements.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.calendaring.schedule.spi;


/**
 *  Ids for object elements for use in forms and queries.
 */

public class ScheduleElements
    extends net.okapia.osid.jamocha.spi.OsidObjectElements {


    /**
     *  Gets the ScheduleElement Id.
     *
     *  @return the schedule element Id
     */

    public static org.osid.id.Id getScheduleEntityId() {
        return (makeEntityId("osid.calendaring.Schedule"));
    }


    /**
     *  Gets the ScheduleSlotId element Id.
     *
     *  @return the ScheduleSlotId element Id
     */

    public static org.osid.id.Id getScheduleSlotId() {
        return (makeElementId("osid.calendaring.schedule.ScheduleSlotId"));
    }


    /**
     *  Gets the ScheduleSlot element Id.
     *
     *  @return the ScheduleSlot element Id
     */

    public static org.osid.id.Id getScheduleSlot() {
        return (makeElementId("osid.calendaring.schedule.ScheduleSlot"));
    }


    /**
     *  Gets the TimePeriodId element Id.
     *
     *  @return the TimePeriodId element Id
     */

    public static org.osid.id.Id getTimePeriodId() {
        return (makeElementId("osid.calendaring.schedule.TimePeriodId"));
    }


    /**
     *  Gets the TimePeriod element Id.
     *
     *  @return the TimePeriod element Id
     */

    public static org.osid.id.Id getTimePeriod() {
        return (makeElementId("osid.calendaring.schedule.TimePeriod"));
    }


    /**
     *  Gets the ScheduleStart element Id.
     *
     *  @return the ScheduleStart element Id
     */

    public static org.osid.id.Id getScheduleStart() {
        return (makeElementId("osid.calendaring.schedule.ScheduleStart"));
    }


    /**
     *  Gets the ScheduleEnd element Id.
     *
     *  @return the ScheduleEnd element Id
     */

    public static org.osid.id.Id getScheduleEnd() {
        return (makeElementId("osid.calendaring.schedule.ScheduleEnd"));
    }


    /**
     *  Gets the Limit element Id.
     *
     *  @return the Limit element Id
     */

    public static org.osid.id.Id getLimit() {
        return (makeElementId("osid.calendaring.schedule.Limit"));
    }


    /**
     *  Gets the LocationDescription element Id.
     *
     *  @return the LocationDescription element Id
     */

    public static org.osid.id.Id getLocationDescription() {
        return (makeElementId("osid.calendaring.schedule.LocationDescription"));
    }


    /**
     *  Gets the LocationId element Id.
     *
     *  @return the LocationId element Id
     */

    public static org.osid.id.Id getLocationId() {
        return (makeElementId("osid.calendaring.schedule.LocationId"));
    }


    /**
     *  Gets the Location element Id.
     *
     *  @return the Location element Id
     */

    public static org.osid.id.Id getLocation() {
        return (makeElementId("osid.calendaring.schedule.Location"));
    }


    /**
     *  Gets the TotalDuration element Id.
     *
     *  @return the TotalDuration element Id
     */

    public static org.osid.id.Id getTotalDuration() {
        return (makeElementId("osid.calendaring.schedule.TotalDuration"));
    }


    /**
     *  Gets the ScheduleTime element Id.
     *
     *  @return the ScheduleTime element Id
     */

    public static org.osid.id.Id getScheduleTime() {
        return (makeQueryElementId("osid.calendaring.schedule.ScheduleTime"));
    }


    /**
     *  Gets the ScheduleTimeInclusive element Id.
     *
     *  @return the ScheduleTimeInclusive element Id
     */

    public static org.osid.id.Id getScheduleTimeInclusive() {
        return (makeQueryElementId("osid.calendaring.schedule.ScheduleTimeInclusive"));
    }


    /**
     *  Gets the CalendarId element Id.
     *
     *  @return the CalendarId element Id
     */

    public static org.osid.id.Id getCalendarId() {
        return (makeQueryElementId("osid.calendaring.schedule.CalendarId"));
    }


    /**
     *  Gets the Calendar element Id.
     *
     *  @return the Calendar element Id
     */

    public static org.osid.id.Id getCalendar() {
        return (makeQueryElementId("osid.calendaring.schedule.Calendar"));
    }
}
