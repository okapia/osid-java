//
// AbstractEntry.java
//
//     Defines an Entry builder.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.dictionary.entry.spi;


/**
 *  Defines an <code>Entry</code> builder.
 */

public abstract class AbstractEntryBuilder<T extends AbstractEntryBuilder<T>>
    extends net.okapia.osid.jamocha.builder.spi.AbstractOsidObjectBuilder<T> {
    
    private final net.okapia.osid.jamocha.builder.dictionary.entry.EntryMiter entry;


    /**
     *  Constructs a new <code>AbstractEntryBuilder</code>.
     *
     *  @param entry the entry to build
     *  @throws org.osid.NullArgumentException a <code>null</code>
     *          argument provided
     */

    protected AbstractEntryBuilder(net.okapia.osid.jamocha.builder.dictionary.entry.EntryMiter entry) {
        super(entry);
        this.entry = entry;
        return;
    }


    /**
     *  Builds the entry.
     *
     *  @return the new entry
     *  @throws org.osid.BadLogicException incorrect behavior
     *  @throws org.osid.InvalidReturnException a method return is not valid
     *  @throws org.osid.NullReturnException a method returned <code>null</code>
     *  @throws org.osid.OsidRuntimeException an error occurred in assembly
     */

    @Override
    public org.osid.dictionary.Entry build() {
        (new net.okapia.osid.jamocha.builder.validator.dictionary.entry.EntryValidator(getValidations())).validate(this.entry);
        return (new net.okapia.osid.jamocha.builder.dictionary.entry.ImmutableEntry(this.entry));
    }


    /**
     *  This method is used to get the miter interface for further
     *  updates. Use <code>build()</code> to finalize and validate
     *  construction.
     *
     *  @return the entry miter
     */

    @Override
    public net.okapia.osid.jamocha.builder.dictionary.entry.EntryMiter getMiter() {
        return (this.entry);
    }


    /**
     *  Sets the key type.
     *
     *  @param keyType a key type
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>keyType</code> is
     *          <code>null</code>
     */

    public T keyType(org.osid.type.Type keyType) {
        getMiter().setKeyType(keyType);
        return (self());
    }


    /**
     *  Sets the key.
     *
     *  @param key a key
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>key</code> is
     *          <code>null</code>
     */

    public T key(java.lang.Object key) {
        getMiter().setKey(key);
        return (self());
    }


    /**
     *  Sets the value type.
     *
     *  @param valueType a value type
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>valueType</code>
     *          is <code>null</code>
     */

    public T valueType(org.osid.type.Type valueType) {
        getMiter().setValueType(valueType);
        return (self());
    }


    /**
     *  Sets the value.
     *
     *  @param value a value
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>value</code> is
     *          <code>null</code>
     */

    public T value(java.lang.Object value) {
        getMiter().setValue(value);
        return (self());
    }


    /**
     *  Adds an Entry record.
     *
     *  @param record an entry record
     *  @param recordType the type of entry record
     *  @return the builder
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public T record(org.osid.dictionary.records.EntryRecord record, org.osid.type.Type recordType) {
        getMiter().addEntryRecord(record, recordType);
        return (self());
    }
}       


