//
// TriggerMiter.java
//
//     Defines a Trigger miter interface for use with the builders.
//
//
// Tom Coppeto
// Okapia
// 8 December 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.builder.control.trigger;


/**
 *  Defines a <code>Trigger</code> miter for use with the builders.
 */

public interface TriggerMiter
    extends net.okapia.osid.jamocha.builder.spi.OsidRuleMiter,
            org.osid.control.Trigger {


    /**
     *  Sets the controller.
     *
     *  @param controller a controller
     *  @throws org.osid.NullArgumentException <code>controller</code>
     *          is <code>null</code>
     */

    public void setController(org.osid.control.Controller controller);


    /**
     *  Listen to on events.
     *
     *  @param listen {@code true} to listen to on events, {@code
     *         false} otherwise
     */

    public void setTurnedOn(boolean listen);


    /**
     *  Listen to turned off events.
     *
     *  @param listen {@code true} to listen to off events, {@code
     *         false} otherwise
     */
    
    public void setTurnedOff(boolean listen);

    
    /**
     *  Sets the changed variable amount.
     *
     *  @param listen {@code true} to listen to changed amount events,
     *         {@code false} otherwise
     */

    public void setChangedVariableAmount(boolean listen);


    /**
     *  Listen to changes above the given threshold.
     *
     *  @param max a max threshold
     *  @throws org.osid.NullArgumentException <code>max</code> is
     *          <code>null</code>
     */

    public void setChangedExceedsVariableAmount(java.math.BigDecimal max);


    /**
     *  Listen to changes below the given threshold.
     *
     *  @param min a min threshold
     *  @throws org.osid.NullArgumentException <code>min</code> is
     *          <code>null</code>
     */

    public void setChangedDeceedsVariableAmount(java.math.BigDecimal min);


    /**
     *  Listen for state changes.
     *
     *  @param listen {@code true} to listen to state change events,
     *         {@code false} otherwise
     */

    public void setChangedDiscreetState(boolean listen);

    
    /**
     *  Sets the discreet state.
     *
     *  @param state a discreet state
     *  @throws org.osid.NullArgumentException <code>state</code> is
     *          <code>null</code>
     */

    public void setDiscreetState(org.osid.process.State state);


    /**
     *  Adds an action group.
     *
     *  @param actionGroup an action group
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroup</code> is <code>null</code>
     */

    public void addActionGroup(org.osid.control.ActionGroup actionGroup);


    /**
     *  Sets all the action groups.
     *
     *  @param actionGroups a collection of action groups
     *  @throws org.osid.NullArgumentException
     *          <code>actionGroups</code> is <code>null</code>
     */

    public void setActionGroups(java.util.Collection<org.osid.control.ActionGroup> actionGroups);

    
    /**
     *  Adds a scene.
     *
     *  @param scene a Scene
     *  @throws org.osid.NullArgumentException
     *          <code>scene</code> is <code>null</code>
     */

    public void addScene(org.osid.control.Scene scene);


    /**
     *  Sets all the scenes.
     *
     *  @param scenes a collection of scenes
     *  @throws org.osid.NullArgumentException <code>scenes</code> is
     *          <code>null</code>
     */

    public void setScenes(java.util.Collection<org.osid.control.Scene> scenes);
    

    /**
     *  Adds a setting.
     *
     *  @param setting a setting
     *  @throws org.osid.NullArgumentException
     *          <code>setting</code> is <code>null</code>
     */

    public void addSetting(org.osid.control.Setting setting);


    /**
     *  Sets all the settings.
     *
     *  @param settings a collection of settings
     *  @throws org.osid.NullArgumentException <code>settings</code> is
     *          <code>null</code>
     */

    public void setSettings(java.util.Collection<org.osid.control.Setting> settings);

    
    /**
     *  Adds a Trigger record.
     *
     *  @param record a trigger record
     *  @param recordType the type of trigger record
     *  @throws org.osid.NullArgumentException <code>record</code> or
     *          <code>recordType</code> is <code>null</code>
     */

    public void addTriggerRecord(org.osid.control.records.TriggerRecord record, org.osid.type.Type recordType);
}       


