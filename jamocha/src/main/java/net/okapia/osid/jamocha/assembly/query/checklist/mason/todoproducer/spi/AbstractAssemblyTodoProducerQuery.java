//
// AbstractAssemblyTodoProducerQuery.java
//
//     A TodoProducerQuery that stores terms.
//
//
// Tom Coppeto
// Okapia
// 24 February 2013
//
// Copyright (c) 2013 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom
//      the Software is furnished to do so, subject the following
//      conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the
//      Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY
//      KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//      WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//      PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//      COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
//      OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
//      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.assembly.query.checklist.mason.todoproducer.spi;

import org.osid.binding.java.annotation.OSID;
import static net.okapia.osid.torrefacto.util.MethodCheck.nullarg;


/**
 *  A TodoProducerQuery that stores terms.
 */

public abstract class AbstractAssemblyTodoProducerQuery
    extends net.okapia.osid.jamocha.assembly.query.spi.AbstractAssemblyOsidRuleQuery
    implements org.osid.checklist.mason.TodoProducerQuery,
               org.osid.checklist.mason.TodoProducerQueryInspector,
               org.osid.checklist.mason.TodoProducerSearchOrder {

    private final java.util.Collection<org.osid.checklist.mason.records.TodoProducerQueryRecord> queryRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.checklist.mason.records.TodoProducerQueryInspectorRecord> queryInspectorRecords = new java.util.LinkedHashSet<>();
    private final java.util.Collection<org.osid.checklist.mason.records.TodoProducerSearchOrderRecord> searchOrderRecords = new java.util.LinkedHashSet<>();


    /** 
     *  Constructs a new <code>AbstractAssemblyTodoProducerQuery</code>.
     *
     *  @param assembler the query assembler
     *  @throws org.osid.NullArgumentException <code>assembler</code>
     *          is <code>null</code>
     */

    protected AbstractAssemblyTodoProducerQuery(net.okapia.osid.jamocha.assembly.query.QueryAssembler assembler) {
        super(assembler);
        return;
    }
    

    /**
     *  Matches producers that create todos. 
     *
     *  @param  match <code> true </code> to match creation rules, <code> 
     *          false </code> to match destruction rules 
     */

    @OSID @Override
    public void matchCreationRule(boolean match) {
        getAssembler().addBooleanTerm(getCreationRuleColumn(), match);
        return;
    }


    /**
     *  Clears the creation rule query terms. 
     */

    @OSID @Override
    public void clearCreationRuleTerms() {
        getAssembler().clearTerms(getCreationRuleColumn());
        return;
    }


    /**
     *  Gets the creationquery terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.BooleanTerm[] getCreationRuleTerms() {
        return (getAssembler().getBooleanTerms(getCreationRuleColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the creation 
     *  rule flag. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCreationRule(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCreationRuleColumn(), style);
        return;
    }


    /**
     *  Gets the CreationRule column name.
     *
     * @return the column name
     */

    protected String getCreationRuleColumn() {
        return ("creation_rule");
    }


    /**
     *  Matches producers based on a cyclic event. 
     *
     *  @param  cyclicEventId the cyclic event <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> cyclicEventId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchCyclicEventId(org.osid.id.Id cyclicEventId, boolean match) {
        getAssembler().addIdTerm(getCyclicEventIdColumn(), cyclicEventId, match);
        return;
    }


    /**
     *  Clears the cyclic event <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearCyclicEventIdTerms() {
        getAssembler().clearTerms(getCyclicEventIdColumn());
        return;
    }


    /**
     *  Gets the cyclic event <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getCyclicEventIdTerms() {
        return (getAssembler().getIdTerms(getCyclicEventIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the cyclic 
     *  event. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByCyclicEvent(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getCyclicEventColumn(), style);
        return;
    }


    /**
     *  Gets the CyclicEventId column name.
     *
     * @return the column name
     */

    protected String getCyclicEventIdColumn() {
        return ("cyclic_event_id");
    }


    /**
     *  Tests if a <code> CyclicEventQuery </code> is available. 
     *
     *  @return <code> true </code> if a cyclic event query is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventQuery() {
        return (false);
    }


    /**
     *  Gets the query for a cyclic event. Multiple retrievals produce a 
     *  nested <code> OR </code> term. 
     *
     *  @return the cyclic event query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQuery getCyclicEventQuery() {
        throw new org.osid.UnimplementedException("supportsCyclicEventQuery() is false");
    }


    /**
     *  Matches producers based on any cyclic event. 
     *
     *  @param  match <code> true </code> for producers based on any cyclic 
     *          event, <code> false </code> to match producers based on no 
     *          cyclic event 
     */

    @OSID @Override
    public void matchAnyCyclicEvent(boolean match) {
        getAssembler().addIdWildcardTerm(getCyclicEventColumn(), match);
        return;
    }


    /**
     *  Clears the cyclic event query terms. 
     */

    @OSID @Override
    public void clearCyclicEventTerms() {
        getAssembler().clearTerms(getCyclicEventColumn());
        return;
    }


    /**
     *  Gets the cyclic event query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventQueryInspector[] getCyclicEventTerms() {
        return (new org.osid.calendaring.cycle.CyclicEventQueryInspector[0]);
    }


    /**
     *  Tests if a cyclic event search order is available. 
     *
     *  @return <code> true </code> if a cyclic event search order is 
     *          available, <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsCyclicEventSearchOrder() {
        return (false);
    }


    /**
     *  Gets the cyclic event search order. 
     *
     *  @return the cyclic event search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsCyclicEventSearchOrder() </code> is <code> false 
     *          </code> 
     */

    @OSID @Override
    public org.osid.calendaring.cycle.CyclicEventSearchOrder getCyclicEventSearchOrder() {
        throw new org.osid.UnimplementedException("supportsCyclicEventSearchOrder() is false");
    }


    /**
     *  Gets the CyclicEvent column name.
     *
     * @return the column name
     */

    protected String getCyclicEventColumn() {
        return ("cyclic_event");
    }


    /**
     *  Matches producers based on a stock. 
     *
     *  @param  stockId the stock <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> stockId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchStockId(org.osid.id.Id stockId, boolean match) {
        getAssembler().addIdTerm(getStockIdColumn(), stockId, match);
        return;
    }


    /**
     *  Clears the stock <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearStockIdTerms() {
        getAssembler().clearTerms(getStockIdColumn());
        return;
    }


    /**
     *  Gets the stock <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getStockIdTerms() {
        return (getAssembler().getIdTerms(getStockIdColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the stock. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStock(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStockColumn(), style);
        return;
    }


    /**
     *  Gets the StockId column name.
     *
     * @return the column name
     */

    protected String getStockIdColumn() {
        return ("stock_id");
    }


    /**
     *  Tests if a <code> StockQuery </code> is available. 
     *
     *  @return <code> true </code> if a stock query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockQuery() {
        return (false);
    }


    /**
     *  Gets the query for a stock. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the stock query 
     *  @throws org.osid.UnimplementedException <code> supportsStockQuery() 
     *          </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockQuery getStockQuery() {
        throw new org.osid.UnimplementedException("supportsStockQuery() is false");
    }


    /**
     *  Matches producers based on any stock. 
     *
     *  @param  match <code> true </code> for producers based on any stock, 
     *          <code> false </code> to match producers based on no stock 
     */

    @OSID @Override
    public void matchAnyStock(boolean match) {
        getAssembler().addIdWildcardTerm(getStockColumn(), match);
        return;
    }


    /**
     *  Clears the stock query terms. 
     */

    @OSID @Override
    public void clearStockTerms() {
        getAssembler().clearTerms(getStockColumn());
        return;
    }


    /**
     *  Gets the stock query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.inventory.StockQueryInspector[] getStockTerms() {
        return (new org.osid.inventory.StockQueryInspector[0]);
    }


    /**
     *  Tests if a stock search order is available. 
     *
     *  @return <code> true </code> if a stock search order is available, 
     *          <code> false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsStockSearchOrder() {
        return (false);
    }


    /**
     *  Gets the stock search order. 
     *
     *  @return the stock search order 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsStockSearchOrder() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.inventory.StockSearchOrder getStockSearchOrder() {
        throw new org.osid.UnimplementedException("supportsStockSearchOrder() is false");
    }


    /**
     *  Gets the Stock column name.
     *
     * @return the column name
     */

    protected String getStockColumn() {
        return ("stock");
    }


    /**
     *  Matches producers with a stock level between the given range 
     *  inclusive. 
     *
     *  @param  from start of range 
     *  @param  to end of range 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.InvalidArgumentException <code> from </code> is 
     *          greater than <code> to </code> 
     */

    @OSID @Override
    public void matchStockLevel(long from, long to, boolean match) {
        getAssembler().addCardinalRangeTerm(getStockLevelColumn(), from, to, match);
        return;
    }


    /**
     *  Matches producers with any stock level. 
     *
     *  @param  match <code> true </code> for producers with any stock, <code> 
     *          false </code> to match producers with no stock 
     */

    @OSID @Override
    public void matchAnyStockLevel(boolean match) {
        getAssembler().addCardinalRangeWildcardTerm(getStockLevelColumn(), match);
        return;
    }


    /**
     *  Clears the stock level query terms. 
     */

    @OSID @Override
    public void clearStockLevelTerms() {
        getAssembler().clearTerms(getStockLevelColumn());
        return;
    }


    /**
     *  Gets the stock level query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.CardinalRangeTerm[] getStockLevelTerms() {
        return (getAssembler().getCardinalRangeTerms(getStockLevelColumn()));
    }


    /**
     *  Specifies a preference for ordering the result set by the stock level. 
     *
     *  @param  style search order style 
     *  @throws org.osid.NullArgumentException <code> style </code> is <code> 
     *          null </code> 
     */

    @OSID @Override
    public void orderByStockLevel(org.osid.SearchOrderStyle style) {
        getAssembler().addOrder(getStockLevelColumn(), style);
        return;
    }


    /**
     *  Gets the StockLevel column name.
     *
     * @return the column name
     */

    protected String getStockLevelColumn() {
        return ("stock_level");
    }


    /**
     *  Matches producers producing to the todo. 
     *
     *  @param  todoId the todo <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> todoBookId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchProducedTodoId(org.osid.id.Id todoId, boolean match) {
        getAssembler().addIdTerm(getProducedTodoIdColumn(), todoId, match);
        return;
    }


    /**
     *  Clears the produced todo <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearProducedTodoIdTerms() {
        getAssembler().clearTerms(getProducedTodoIdColumn());
        return;
    }


    /**
     *  Gets the produced <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getProducedTodoIdTerms() {
        return (getAssembler().getIdTerms(getProducedTodoIdColumn()));
    }


    /**
     *  Gets the ProducedTodoId column name.
     *
     * @return the column name
     */

    protected String getProducedTodoIdColumn() {
        return ("produced_todo_id");
    }


    /**
     *  Tests if a <code> TodoBookQuery </code> is available. 
     *
     *  @return <code> true </code> if a todo query is available, <code> false 
     *          </code> otherwise 
     */

    @OSID @Override
    public boolean supportsProducedTodoQuery() {
        return (false);
    }


    /**
     *  Gets the query for a todo. Multiple retrievals produce a nested <code> 
     *  OR </code> term. 
     *
     *  @return the todo query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsProducedTodoQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.TodoQuery getProducedTodoQuery() {
        throw new org.osid.UnimplementedException("supportsProducedTodoQuery() is false");
    }


    /**
     *  Matches producers producing to any todo. 
     *
     *  @param  match <code> true </code> for producers producing any todo, 
     *          <code> false </code> to match producers producing no todo 
     */

    @OSID @Override
    public void matchAnyProducedTodo(boolean match) {
        getAssembler().addIdWildcardTerm(getProducedTodoColumn(), match);
        return;
    }


    /**
     *  Clears the todo query terms. 
     */

    @OSID @Override
    public void clearProducedTodoTerms() {
        getAssembler().clearTerms(getProducedTodoColumn());
        return;
    }


    /**
     *  Gets the produced query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.TodoQueryInspector[] getProducedTodoTerms() {
        return (new org.osid.checklist.TodoQueryInspector[0]);
    }


    /**
     *  Gets the ProducedTodo column name.
     *
     * @return the column name
     */

    protected String getProducedTodoColumn() {
        return ("produced_todo");
    }


    /**
     *  Matches producers mapped to the checklist. 
     *
     *  @param  checklistId the checklist <code> Id </code> 
     *  @param  match <code> true </code> for a positive match, <code> false 
     *          </code> for a negative match 
     *  @throws org.osid.NullArgumentException <code> checklistId </code> is 
     *          <code> null </code> 
     */

    @OSID @Override
    public void matchChecklistId(org.osid.id.Id checklistId, boolean match) {
        getAssembler().addIdTerm(getChecklistIdColumn(), checklistId, match);
        return;
    }


    /**
     *  Clears the checklist <code> Id </code> query terms. 
     */

    @OSID @Override
    public void clearChecklistIdTerms() {
        getAssembler().clearTerms(getChecklistIdColumn());
        return;
    }


    /**
     *  Gets the checklist <code> Id </code> query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.search.terms.IdTerm[] getChecklistIdTerms() {
        return (getAssembler().getIdTerms(getChecklistIdColumn()));
    }


    /**
     *  Gets the ChecklistId column name.
     *
     * @return the column name
     */

    protected String getChecklistIdColumn() {
        return ("checklist_id");
    }


    /**
     *  Tests if an <code> ChecklistQuery </code> is available. 
     *
     *  @return <code> true </code> if a checklist query is available, <code> 
     *          false </code> otherwise 
     */

    @OSID @Override
    public boolean supportsChecklistQuery() {
        return (false);
    }


    /**
     *  Gets the query for a checklist. Multiple retrievals produce a nested 
     *  <code> OR </code> term. 
     *
     *  @return the checklist query 
     *  @throws org.osid.UnimplementedException <code> 
     *          supportsChecklistQuery() </code> is <code> false </code> 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQuery getChecklistQuery() {
        throw new org.osid.UnimplementedException("supportsChecklistQuery() is false");
    }


    /**
     *  Clears the checklist query terms. 
     */

    @OSID @Override
    public void clearChecklistTerms() {
        getAssembler().clearTerms(getChecklistColumn());
        return;
    }


    /**
     *  Gets the checklist query terms. 
     *
     *  @return the query terms 
     */

    @OSID @Override
    public org.osid.checklist.ChecklistQueryInspector[] getChecklistTerms() {
        return (new org.osid.checklist.ChecklistQueryInspector[0]);
    }


    /**
     *  Gets the Checklist column name.
     *
     * @return the column name
     */

    protected String getChecklistColumn() {
        return ("checklist");
    }


    /**
     *  Tests if this todoProducer supports the given record
     *  <code>Type</code>.
     *
     *  @param  todoProducerRecordType a todo producer record type 
     *  @return <code>true</code> if the todoProducerRecordType is
     *          supported, <code>false</code> otherwise
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is 
     *          <code>null</code>
     */

    @OSID @Override
    public boolean hasRecordType(org.osid.type.Type todoProducerRecordType) {
        for (org.osid.checklist.mason.records.TodoProducerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (true);
            }
        }

        return (false);
    }


    /**
     *  Gets the record corresponding to the given
     *  <code>Object]</code> query record <code>Type</code>.
     *
     *  @param  todoProducerRecordType the todo producer record type 
     *  @return the todo producer query record 
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoProducerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerQueryRecord getTodoProducerQueryRecord(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.mason.records.TodoProducerQueryRecord record : this.queryRecords) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoProducerRecordType + " is not supported");
    }


    /**
     *  Gets the query inspector record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param  todoProducerRecordType the todo producer record type 
     *  @return the todo producer query inspector record 
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoProducerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerQueryInspectorRecord getTodoProducerQueryInspectorRecord(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.mason.records.TodoProducerQueryInspectorRecord record : this.queryInspectorRecords) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoProducerRecordType + " is not supported");
    }


    /**
     *  Gets the search order record corresponding to the given
     *  <code>Object]</code> record <code>Type</code>.
     *
     *  @param todoProducerRecordType the todo producer record type
     *  @return the todo producer search order record 
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerRecordType</code> is <code>null</code>
     *  @throws org.osid.UnsupportedException
     *          <code>hasRecordType(todoProducerRecordType)</code> is
     *          <code>false</code>
     */

    @OSID @Override
    public org.osid.checklist.mason.records.TodoProducerSearchOrderRecord getTodoProducerSearchOrderRecord(org.osid.type.Type todoProducerRecordType)
        throws org.osid.OperationFailedException {

        for (org.osid.checklist.mason.records.TodoProducerSearchOrderRecord record : this.searchOrderRecords) {
            if (record.implementsRecordType(todoProducerRecordType)) {
                return (record);
            }
        }
        
        throw new org.osid.UnsupportedException(todoProducerRecordType + " is not supported");
    }


    /**
     *  Adds a record set to this todo producer. 
     *
     *  This method registers the query, query inspector, and search
     *  order records. Additional types may be registered with this
     *  object using <code>addRecordType()</code>.
     *
     *  The registered types are not used for record retrievals or
     *  interoperability tests using
     *  <code>hasRecordType()</code>. When retrieving or testing for
     *  support of a record type, each record is examined using
     *  <code>OsidRecord.implememtsRecordType()</code>. Some types may
     *  be supported in <code>OsidRecords</code> that are not visible
     *  through a retrieval of all record types for the purposes of
     *  compatibility.
     *
     *  @param todoProducerQueryRecord the todo producer query record
     *  @param todoProducerQueryInspectorRecord the todo producer query inspector
     *         record
     *  @param todoProducerSearchOrderRecord the todo producer search order record
     *  @param todoProducerRecordType todo producer record type
     *  @throws org.osid.NullArgumentException
     *          <code>todoProducerQueryRecord</code>,
     *          <code>todoProducerQueryInspectorRecord</code>,
     *          <code>todoProducerSearchOrderRecord</code> or
     *          <code>todoProducerRecordTypetodoProducer</code> is
     *          <code>null</code>
     */
            
    protected void addTodoProducerRecords(org.osid.checklist.mason.records.TodoProducerQueryRecord todoProducerQueryRecord, 
                                      org.osid.checklist.mason.records.TodoProducerQueryInspectorRecord todoProducerQueryInspectorRecord, 
                                      org.osid.checklist.mason.records.TodoProducerSearchOrderRecord todoProducerSearchOrderRecord, 
                                      org.osid.type.Type todoProducerRecordType) {

        addRecordType(todoProducerRecordType);

        nullarg(todoProducerQueryRecord, "todo producer query record");
        nullarg(todoProducerQueryInspectorRecord, "todo producer query inspector record");
        nullarg(todoProducerSearchOrderRecord, "todo producer search odrer record");

        this.queryRecords.add(todoProducerQueryRecord);
        this.queryInspectorRecords.add(todoProducerQueryInspectorRecord);
        this.searchOrderRecords.add(todoProducerSearchOrderRecord);
        
        return;
    }
}
