//
// InvariantIndexedMapProxyAuthorizationLookupSession
//
//    Implements an Authorization lookup service backed by a fixed
//    collection of authorizations indexed by their types.
//
//
// Tom Coppeto
// Okapia
// 5 January 2010
//
//
// Copyright (c) 2010 Okapia. All Rights Reserved.
//
//      Permission is hereby granted, free of charge, to any person
//      obtaining a copy of this software and associated documentation
//      files (the "Software"), to deal in the Software without
//      restriction, including without limitation the rights to use,
//      copy, modify, merge, publish, distribute, sublicesne, and/or
//      sell copies of the Software, and to permit the persons to whom the
//      Software is furnished to do so, subject the following conditions:
//
//      The above copyright notice and this permission notice shall be
//      included in all copies or substantial portions of the Software.
//
//      The Software is provided "AS IS", WITHOUT WARRANTY OF ANY KIND,
//      EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//      OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//      NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//      HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//      WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
//      DEALINGS IN THE SOFTWARE.
//

package net.okapia.osid.jamocha.core.authorization;


/**
 *  Implements a Authorization lookup service backed by a fixed
 *  collection of authorizations. The authorizations are indexed by
 *  {@code Id}, genus and record types.
 *
 *  The type indices are created from {@code getGenusType()} and
 *  {@code getRecordTypes()}. Some authorizations may be compatible
 *  with more types than are indicated through these authorization
 *  methods. {@code addRecordType()}, {@code addGenusType()}
 *  can be used to supplement the index.
 */

public final class InvariantIndexedMapProxyAuthorizationLookupSession
    extends net.okapia.osid.jamocha.core.authorization.spi.AbstractIndexedMapAuthorizationLookupSession
    implements org.osid.authorization.AuthorizationLookupSession {


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyAuthorizationLookupSession}
     *  using an array of authorizations.
     *
     *  @param vault the vault
     *  @param authorizations an array of authorizations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorizations} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                         org.osid.authorization.Authorization[] authorizations, 
                                                         org.osid.proxy.Proxy proxy) {

        setVault(vault);
        setSessionProxy(proxy);
        putAuthorizations(authorizations);

        return;
    }


    /**
     *  Constructs a new
     *  {@code InvariantIndexedMapProxyAuthorizationLookupSession}
     *  using a collection of authorizations.
     *
     *  @param vault the vault
     *  @param authorizations a collection of authorizations
     *  @param proxy a session proxy
     *  @throws org.osid.NullArgumentException {@code vault},
     *          {@code authorizations} or {@code proxy} is {@code null}
     */

    public InvariantIndexedMapProxyAuthorizationLookupSession(org.osid.authorization.Vault vault,
                                                         java.util.Collection<? extends org.osid.authorization.Authorization> authorizations,
                                                         org.osid.proxy.Proxy proxy) {

        setVault(vault);
        setSessionProxy(proxy);
        putAuthorizations(authorizations);

        return;
    }
}
